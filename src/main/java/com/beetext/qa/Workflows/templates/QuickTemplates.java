package com.beetext.qa.Workflows.templates;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.TemplatesWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Template;
import com.beetext.qa.util.TestUtil;

public class QuickTemplates extends TemplatesWFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Template template = new Template();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void template_creation_defaulttemplate_Message() throws Exception {

		// logins(map);

		button(template.pin_conversation, "button").click();
		logger.info("conversation selected");
		button(template.temlate_icon, "button").click();
		logger.info("clicked on template icon");

		button(template.personal_template_tab, "button").click();
		logger.info("clicked on template icon");

		System.out.println(template.noPersonal_template.getText());
		if (template.noPersonal_template.getText().equalsIgnoreCase("Create your first personal template.")) {
			Assert.assertEquals(template.noPersonal_template.getText(), "Create your first personal template.");
			System.out.println("Create your first personal template. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Create your first personal template.selection TestCase");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(template.noPersonal_template.getText(), "Create your first personal template.",
					"Create your first personal template.");
			TestUtil.takeScreenshotAtEndOfTest("Create your first personal template. TestCase is failed");// ScreenShot
																											// capture
		}

		// logout();
	}

	public void template_creation_shareddefaulttemplate_Message() throws Exception {

		//// logins(map);

		button(template.pin_conversation, "button").click();
		logger.info("conversaitn selected");
		button(template.temlate_icon, "button").click();
		logger.info("clicked on template icon");
		button(template.shared_template_button, "button").click();
		logger.info("Clicked on share template button");

		System.out.println(template.noshared_template.getText());
		if (template.noshared_template.getText().equalsIgnoreCase("Create a template for your departments")) {
			Assert.assertEquals(template.noshared_template.getText(), "Create a template for your departments");
			System.out.println("Create a template for your departments case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Create a template for your departments TestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(template.noshared_template.getText(), "Create your first personal template.",
					"Create a template for your departments");
			TestUtil.takeScreenshotAtEndOfTest("Create a template for your departments TestCase is failed");// ScreenShot
																											// capture
		}

		// logout();

	}

	public void sending_personal_Template() throws Exception {

		// login(map);
		selectConversation();
		clickonTemplateIcon();
		String message = "hello world how are you what are you doing hello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are1";
		button(template.personal_template_tab, "button").click();
		logger.info("personal template selected with message" + message);
		waits(1);

		button(template.select_Template, "button").click();
		logger.info("personal template selected with message" + message);
		sendButton();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Personal Template selection case is  TestCase successfully");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("Personal Template is sending  TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void create_personal_Template() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.create_template_button, "button").click();
		logger.info("click on create template button");
		String title = "sample";
		textbox(template.create_template_title).sendKeys(title);
		logger.info("create template title" + title);
		textbox(template.create_template_message).sendKeys(title);
		logger.info("Message:" + title);
		button(template.personal_option, "button").click();
		logger.info("personal template selected with message" + message);
		button(template.create_templates_button, "button").click();
		logger.info("clicked on create template button");
		System.out.println(template.create_template_toaster_msg.getText());
		if (template.create_template_toaster_msg.getText().equalsIgnoreCase("Your template sample has been created.")) {
			Assert.assertEquals(template.create_template_toaster_msg.getText(),
					"Your template sample has been created.");
			System.out.println("Personal Template created is verified");
			TestUtil.takeScreenshotAtEndOfTest("Personal Template creation case is  TestCase successfully");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(template.create_template_toaster_msg.getText(),
					"Your template sample has been created.", "Your template sample has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Personal Template creation TestCase is failed");// ScreenShot capture
		}

		button(template.manage_template, "button").click();
		logger.info("clicked on manage template button");
		button(template.manage_delete_template, "button").click();
		logger.info("clicked on delete template button");

		button(template.manage_confirm_delete_template, "button").click();
		logger.info("clicked on confirm delete button");

		waits(5);
		closetoolspopups();

	}

	public void create_shared_Template() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("clicked on shared template button");
		button(template.create_template_button, "button").click();
		logger.info("clicked on create template button");
		String title = "shared";
		textbox(template.create_template_title).sendKeys(title);
		logger.info("Title:" + title);
		textbox(template.create_template_message).sendKeys("shared template creation checking");
		logger.info("Message shared template creation checking");
		button(template.create_shared_template, "button").click();
		logger.info("click on shared option");
		button(template.shared_template_click_selectall, "button").click();
		logger.info("selected all departments");

		button(template.create_templates_button, "button").click();
		logger.info("clicked on create template button");
		System.out.println(template.create_shared_template_toaster_msg.getText());
		if (template.create_shared_template_toaster_msg.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.create_shared_template_toaster_msg.getText(),
					"Your template shared has been created.");
			System.out.println("shared Template created is verified");
			TestUtil.takeScreenshotAtEndOfTest("shared Template creation case is  TestCase successfully");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(template.create_shared_template_toaster_msg.getText(),
					"Your template shared has been created.", "Your template shared has been created.");
			TestUtil.takeScreenshotAtEndOfTest("shared Template creation TestCase is failed");// ScreenShot capture
		}

		button(template.select_shared_list, "button").click();
		logger.info("clicked on shared tab");
		button(template.manage_shared_template, "button").click();
		logger.info("clicked on manage button");
		button(template.manage_shared_delete_template, "button").click();
		logger.info("click on delete button");

		button(template.manage_shared_confirm_delete_template, "button").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		closetoolspopups();

	}

	public void sending_personal_Template_with_attachment_modify_text() throws Exception {

		// login(map);

		selectConversation();

		clickonTemplateIcon();
		// button(template.personal_template_tab, "button").click();
		logger.info("personal template selected with message" + message);
		button(template.select_attachment_Template, "button").click();
		logger.info("Attachment template is selected");
		textbox(repository.chatInputTextareaTxt).clear();
		logger.info("Message is clearing");

		String message = randomestring();

		textbox(repository.chatInputTextareaTxt).sendKeys(message);
		logger.info("Message typing");

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template with attachments selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template with attachments selection case is  TestCase successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("Personal Template with attachments is sending  TestCase is failed");// ScreenShot
																													// capture
		}

		// logout();

	}

	public void sending_shared_Template_with_attachment_modify_text() throws Exception {

		// login(map);
		selectConversation();

		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("Selected Shared option");

		button(template.select_shared_attachment_Template, "button").click();
		logger.info("selected shared template");

		textbox(repository.chatInputTextareaTxt).clear();
		logger.info("Message is clearing");

		String message = randomestring();

		textbox(repository.chatInputTextareaTxt).sendKeys(message);
		logger.info("Message typing" + message);

		sendButton();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("shared Template with attachments selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"shared Template with attachments selection case is  TestCase successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("shared Template with attachments is sending  TestCase is failed");// ScreenShot
																													// capture
		}

	}

	public void sending_shared_Template() throws Exception {

		// login(map);
		selectConversation();
		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("clicked on shared template button");

		String message = "shared template selected";
		button(template.select_shared_Template, "button").click();
		logger.info("Shared template is selected" + message);

		sendButton();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("shared Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest("shared Template selection case is  TestCase successfully");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("shared Template is sending  TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	// upto above modified

	public void sending_personal_Temp_modify_text() throws Exception {

		// login(map);

		selectConversation();

		clickonTemplateIcon();
		// button(template.personal_template_tab, "button").click();
		logger.info("clicked on template icon");

		button(template.select_Template, "button").click();
		logger.info("Template is selected");

		textbox(repository.chatInputTextareaTxt).clear();
		logger.info("Messsage clearing");

		String message = randomestring();

		textbox(repository.chatInputTextareaTxt).sendKeys(message);
		logger.info("Message Typing" + message);

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Personal Template selection case is  TestCase successfully");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("Personal Template is sending  TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void sending_shared_Temp_modify_text() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("clicked on shared template button");

		button(template.select_shared_Template, "button").click();
		logger.info("Shared template selected");

		textbox(repository.chatInputTextareaTxt).clear();
		logger.info("Message is clearing");

		String message = randomestring();

		textbox(repository.chatInputTextareaTxt).sendKeys(message);
		logger.info("Message is typing" + message);

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("shared Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest("shared Template selection case is  TestCase successfully");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("shared Template is sending  TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void sending_personal_Temp_add_Attachment() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.personal_template_tab, "button").click();
		logger.info("clicked on template icon");

		String message = "hello world how are you what are you doing hello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are1";
		button(template.select_Template, "button").click();
		logger.info("Template selected" + message);

		button(repository.pin_attachment, "file attachment").click();
		logger.info("Attachment icon is clicked");
		waits(7);
		logger.info("wait for 7 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		System.out.println(currentDir + "\\Files\\pdf.pdf");
		logger.info("attachment is selected");
		waits(5);

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template selection case and add attachments is  TestCase successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template is sending case and add attachments TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void sending_shared_Temp_add_Attachment() throws Exception {

		// login(map);
		selectConversation();
		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("Selected Shared template option");

		String message = "shared template creation checking in automation server shared template creation checking in automation servershared template creation checking in automation servershared template creation checking in automation servershared template creation checking in automation servershared template creation checking in automation servershared template creation checking in automation servershared template creation checking in automation servershared template creation checking in automation server";
		button(template.select_shared_Template, "button").click();
		logger.info("selected shared template");

		button(repository.pin_attachment, "file attachment").click();
		logger.info("Clicked on attachment icon");
		waits(7);
		logger.info("wait for 7 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\pdf.pdf");
		logger.info("Attachment added");
		waits(5);
		logger.info("wait for 10 sec");

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template selection case and add attachments is  TestCase successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template is sending case and add attachments TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void selecting_text_oneattach_personal_Temp_addone_more_Attachment() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.personal_template_tab, "button").click();
		logger.info("clicked on template icon");

		String message = "one attachment case checking after that manuvally adding one more attachment in message view page";
		button(template.select_oneattachment_Template, "button").click();
		logger.info("selected one attachment template");

		button(repository.pin_attachment, "file attachment").click();
		logger.info("clicked on attachment icon");
		waits(7);
		logger.info("wait for 7 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\file.mp3");
		logger.info("attachments added ");

		waits(5);
		logger.info("wait for 20 sec");

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template selection case and add attachments is  TestCase successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template is sending case and add attachments TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void selecting_text_oneattach_shared_Temp_addone_more_Attachment() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("click on shared button");

		String message = "one attachment  shared template creation checking";
		button(template.select_shared_oneattachment_Template, "button").click();
		logger.info("selected one attachment shared template");

		button(repository.pin_attachment, "file attachment").click();
		logger.info("clicked on attachment icon");
		waits(7);
		logger.info("wait for 7 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");
		logger.info("Attachment added");

		waits(10);
		logger.info("wait for 10 sec");

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template selection case and add attachments is  TestCase successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template is sending case and add attachments TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void selecting_10attachments_shared_Template() throws Exception {

		// login(map);
		selectConversation();
		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("clicked on shared template button");

		String message = "attachemnt with text message shared  template creation checking";
		button(template.select_shared_attachment_Template, "button").click();
		logger.info("selected 10 attachments shared template");
		waits(5);

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest("shared Template selection 10 attachmetns  TestCase successfully");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("shared Template is sending case 10 attachments TestCase is failed");// ScreenShot
																													// capture
		}

	}

	public void selecting_10attachments_personal_Template() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.personal_template_tab, "button").click();
		logger.info("clicked on template icon");

		String message = "attachment template selected";
		button(template.select_attachment_Template, "button").click();
		logger.info("Tempalte selected" + message);
		waits(5);

		sendButton();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("Personal Template selection case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Personal Template selection 10 attachments case  is  TestCase successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("Personal Template is sending 10 attachments case TestCase is failed");// ScreenShot
																														// capture
		}

	}

	public void selecting_addmorethan10attachments_shared_Template() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();
		button(template.shared_template_button, "button").click();
		logger.info("Clicked on shared tempalte button");

		String message = "attachemnt with text message shared  template creation checking";
		button(template.select_shared_attachment_Template, "button").click();
		logger.info("selected shared with attachmets ");

		button(repository.pin_attachment, "file attachment").click();
		logger.info("Clicked on attachemnt icons");
		waits(7);
		logger.info("wait for 7 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");
		logger.info("11th attachments added showing the max 10 attachment allowed popup");
		waits(5);

		System.out.println(template.morethan_10attachments.getText());
		if (template.morethan_10attachments.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(template.morethan_10attachments.getText(), "Maximum 10 attachments are allowed.");
			template.okbutton.click();
			System.out.println("shared Template selection Maximum 10 attachments are allowed. case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"shared Template selection case Maximum 10 attachments are allowed.  TestCase successfully");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(template.morethan_10attachments.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest(
					"shared Template is sending case Maximum 10 attachments are allowed. TestCase is failed");// ScreenShot
																												// capture
		}

	}

	public void quick_template_popup_opening_headding_validating() throws Exception {

		// login(map);

		selectConversation();

		textbox(template.input_box).sendKeys("/");
		logger.info("Message box typing : /");

		System.out.println(template.quick_template_headding.getText());
		if (template.quick_template_headding.getText().equalsIgnoreCase("Quick Templates")) {
			Assert.assertEquals(template.quick_template_headding.getText(), "Quick Templates");
			System.out.println("Quick Templates case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Quick Templates TestCase successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(template.quick_template_headding.getText(), "quick_template_headding",
					"quick_template_headding");
			TestUtil.takeScreenshotAtEndOfTest("quick_template_headding. TestCase is failed");// ScreenShot capture
		}

		textbox(template.input_box).clear();

	}

	public void quick_template_select_template_modify_text() throws Exception {

		// login(map);

		selectConversation();
		textbox(template.input_box).clear();
		textbox(template.input_box).sendKeys("/");
		logger.info("Message box typed: /");

		button(template.select_quick_template_list, "button").click();
		logger.info("quick tempalte is selected");

		textbox(template.input_box).clear();
		logger.info("Tempalte message is cleared");

		// String message=Char50_randomestring();
		String message = "Quick Template test is modified";

		textbox(repository.chatInputTextareaTxt).sendKeys(message);
		logger.info("Message  Typed" + message);

		sendButton();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("quick tempalte modify text is verified");
			TestUtil.takeScreenshotAtEndOfTest("quick tempalte modify text case is  TestCase successfully");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("quick tempalte modify text  TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void quick_template_select_template_modify_attachments() throws Exception {

		// login(map);

		selectConversation();

		textbox(template.input_box).sendKeys("/");
		logger.info("Message box typed /");
		String message = "hello world how are you what are you doing hello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are you doinghello world how are you what are1";
		button(template.select_quick_template_list, "button").click();
		logger.info("quick template selected " + message);

		button(repository.pin_attachment, "file attachment").click();
		logger.info("attachmetn icon is clicked");
		waits(7);
		logger.info("wait for 7 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");
		logger.info("Attachment is added");
		waits(5);

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("quick tempalte modify text is verified");
			TestUtil.takeScreenshotAtEndOfTest("quick tempalte modify text case is  TestCase successfully");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("quick tempalte modify text  TestCase is failed");// ScreenShot capture
		}

		// logout();

	}

	public void quick_template_select_template_add_morethan_10_attachments() throws Exception {

		// login(map);

		selectConversation();

		textbox(template.input_box).sendKeys("/");
		logger.info("Message box typed /");
		//String message = "attachment template selected";
		button(template.quick_template_10_attachment, "button").click();
		logger.info("Selected 10 attachments quick template");

		button(repository.pin_attachment, "file attachment").click();
		logger.info("clicked on attachment icon");
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");
		logger.info("Attachment is added successfully");
		waits(5);

		System.out.println(template.morethan_10attachments.getText());
		if (template.morethan_10attachments.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(template.morethan_10attachments.getText(), "Maximum 10 attachments are allowed.");
			System.out.println("quick tempalte more than 10 attachments is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"quick tempalte more than 10 attachments case is  TestCase successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(template.morethan_10attachments.getText(), "Maximum 10 attachments are allowed.",
					"Maximum 10 attachments are allowed.");
			TestUtil.takeScreenshotAtEndOfTest("quick tempalte more than 10 attachments TestCase is failed");// ScreenShot
																												// capture
		}
		waits(2);
		template.okbutton.click();
	}

	public void quick_template_select_template__add_gif_attachment() throws Exception {

		// login(map);
		selectConversation();

		clickonTemplateIcon();

		String message = "one attachment case checking after that manuvally adding one more attachment in message view page";
		button(template.select_oneattachment_Template, "button").click();
		logger.info("selected one attachment template with message" + message);

		button(repository.pin_attachment, "file attachment").click();
		logger.info("clicked on attachment icon");
		waits(7);
		logger.info("wait for 7 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");
		logger.info("attachment added");
		waits(5);

		button(repository.giphybttn, "file attachment").click();
		logger.info("clicked on giffy icon");
		waits(7);

		button(template.select_giff_image, "file attachment").click();
		logger.info("selected giffy image");
		waits(7);
		logger.info("wait for 7 sec");

		button(repository.emojibttn, "file attachment").click();
		logger.info("clicked on emoji icon");
		waits(7);
		logger.info("wait for 7 sec");

		button(repository.emoji_like, "file attachment").click();
		logger.info("selected emoji");
		waits(7);
		logger.info("wait for 7 sec");

		sendButton();

		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(repository.message.getText(), message);
			System.out.println("quick tempalte modify add giffy +attachment +emoji text is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"quick tempalte modify add giffy +attachment +emoji text case is  TestCase successfully");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), message, message);
			TestUtil.takeScreenshotAtEndOfTest("quick tempalte add giffy +attachment +emoji text  TestCase is failed");// ScreenShot
																														// capture
		}
		logout();

	}

	public void quick_template_no_quick_template() throws Exception {

		logins();

		button(template.pin_conversation, "button").click();
		logger.info("conversation selected");

		textbox(template.input_box).sendKeys("/");
		logger.info("typed message box /");

		System.out.println(template.no_quick_template.getText());
		if (template.no_quick_template.getText().equalsIgnoreCase("No Templates found!")) {
			Assert.assertEquals(template.no_quick_template.getText(), "No Templates found!");
			System.out.println("Quick Templates No Templates found! case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Quick Templates No Templates found! TestCase successfully");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(template.no_quick_template.getText(), "No Templates found!", "No Templates found!");
			TestUtil.takeScreenshotAtEndOfTest("quick_template_headding.No Templates found! TestCase is failed");// ScreenShot
																													// capture
		}

	}

	public void selecting_morethan10attachments_personal_Template() throws Exception {

		// login(map);

		selectConversation();
		clickonTemplateIcon();

		String message = "attachment template selected";
		button(template.select_attachment_Template, "button").click();
		logger.info("Attachment template is selcted");

		button(repository.pin_attachment, "file attachment").click();
		logger.info("Attachment icon is clicked");
		waits(7);
		logger.info("wait for 7 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");

		logger.info("Attachment is selected");
		waits(5);

		System.out.println(template.morethan_10attachments.getText());
		if (template.morethan_10attachments.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(template.morethan_10attachments.getText(), "Maximum 10 attachments are allowed.");
			System.out.println("personal Template selection Maximum 10 attachments are allowed. case is verified");
			logger.info("personal Template selection Maximum 10 attachments are allowed. case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"personal Template selection case Maximum 10 attachments are allowed.  TestCase successfully");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(template.morethan_10attachments.getText(), message, message);
			logger.info("personal Template selection Maximum 10 attachments are allowed. case is not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"personal Template is sending case Maximum 10 attachments are allowed. TestCase is failed");// ScreenShot
																												// capture
		}
		waits(3);
		template.okbutton.click();

	}

}
