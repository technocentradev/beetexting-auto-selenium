package com.beetext.qa.Workflows.templates;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.ToolsSharedTemplateWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.ToolsTemplate;
import com.beetext.qa.util.TestUtil;

public class ToolsSharedTemplatesWF extends ToolsSharedTemplateWFCM {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	ToolsTemplate template = new ToolsTemplate();

	public static String currentDir = System.getProperty("user.dir");

	public void Managetemplate_selectsharedoption_entertitle_message_selectone_department_clickon_create_template_button()
			throws Exception {

		//// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();

		createTemplateToastermessage();
		wait(5);
		selectsharedtab();

		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managetemplate_selectsharedoption_entertitle_message_select_all_department_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		wait(5);
		selectsharedtab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_entertitle_message_select_all_department_unselect_one_dept_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectallDept();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();
		wait(5);
		selectsharedtab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_entertitle_dontenter_message_select_all_department_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();

		selectsharedoption();
		selectallDept();

		clickoncreateButton();
		drafttemplateToastermessage();
		wait(5);
		selectDrafttab();
		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_entermessage_dontenter_title_select_all_department_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplateMessage();

		selectsharedoption();
		selectallDept();
		selectonlyoneDept();
		clickoncreateButton();
		drafttemplateToastermessage();
		wait(5);
		selectDrafttab();

		notitledraftemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// notitletemplatedeltetoastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_add_attachemnt_select_all_department_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		addAttachments();

		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		wait(5);
		selectsharedtab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_add_attachemnt_select_one_department_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		addAttachments();

		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();
		wait(5);
		selectsharedtab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_add_GIF_attachemnt_select_one_department_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		addGIFAttachments();

		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();
		wait(5);
		selectsharedtab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_add_emoji_attachemnt_select_one_department_clickon_create_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		emoji();

		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();
		wait(5);
		selectsharedtab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_add_GIF_attachemnt_select_one_department_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		addGIFAttachments();

		selectsharedoption();
		selectonlyoneDept();
		saveDraft();
		draftToastermessage();
		wait(5);
		selectDrafttab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_add_emoji_attachemnt_select_one_department_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		emoji();

		selectsharedoption();
		selectonlyoneDept();
		saveDraft();
		draftToastermessage();
		wait(5);
		selectDrafttab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_close_toolspopu_icon_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectallDept();
		closetoolspopup();
		savedraft();
		breadscrumtoastermessage();
		wait(5);

		openTemplatepages();
		selectDrafttab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_close_toolspopup_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectonlyoneDept();
		closetoolspopup();
		discardbuttons();
		openTemplatepages();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_navigationlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();

		breadscrumlink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		wait(5);

		selectDrafttab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_navigationlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();

		breadscrumlink();
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_all_dept_clickon_anyotherlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectallDept();
		tagslink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		wait(5);
		templatelink();
		selectDrafttab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_all_department_clickon_otherlink_clickon_discard_template_button()
			throws Exception {
		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectallDept();
		tagslink();
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_dept_clickon_anyotherlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectonlyoneDept();
		tagslink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		wait(5);
		templatelink();
		selectDrafttab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();
	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_otherlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();
		selectonlyoneDept();
		tagslink();
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_dept_clickon_breadscrumlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();

		breadscrumlink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		waits(5);

		selectDrafttab();

		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_breadscrumlink_clickon_discard_template_button()
			throws Exception {

		// capture
		// login(map);
		openTemplatepage();

		enterTemplatTitle();
		enterTemplateMessage();

		selectsharedoption();

		breadscrumlink();
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_no_title_message_clickon_breadscrumlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		selectsharedoption();
		breadscrumlink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		waits(3);
		selectDrafttab();
		notitledraftemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_no_title_message_clickon_breadscrumlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		selectsharedoption();
		breadscrumlink();
		waits(3);
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_select_some_dept_enter_title_message_clickon_anyotherlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		tagslink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		wait(5);
		templatelink();
		selectDrafttab();
		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_select_some_dept_enter_title_message_clickon_anyotherlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		tagslink();
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_select_all_dept_enter_title_message_clickon_anyotherlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		tagslink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		wait(5);
		templatelink();
		selectDrafttab();
		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_select_all_dept_enter_title_message_clickon_breadscrumlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		tagslink();
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_select_some_dept_dont_give_title_message_clickon_anyotherlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		selectsharedoption();
		selectonlyoneDept();
		tagslink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		wait(5);
		templatelink();
		selectDrafttab();
		notitledraftemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managetemplate_selectsharedoption_select_some_dept_dontenter_title_message_clickon_anohterlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		selectsharedoption();
		selectonlyoneDept();
		tagslink();
		discardbutton();

	}

	public void Managetemplate_selectsharedoption_select_all_dept_dont_give_title_message_clickon_anyotherlink_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		selectsharedoption();
		selectallDept();
		tagslink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		waits(5);
		templatelink();
		selectDrafttab();
		notitledraftemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();
	}

	public void Managetemplate_selectsharedoption_select_all_dept_dontenter_title_message_clickon_anohterlink_clickon_discard_template_button()
			throws Exception {
		// login(map);
		openTemplatepage();
		selectsharedoption();
		selectallDept();
		tagslink();
		discardbutton();

	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_clickon_update_template_button()
			throws Exception {
		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();

		addAttachments();

		updateTemplate();

		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();
	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_clickon_savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();

		addAttachments();

		saveDraft();

		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();
	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_clickon_anyotherlink_clickon_delete_confirm_delete_template_button()
			throws Exception {
		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();

		addAttachments();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_clickon_breadscrumlink_clickon_save_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();
		clearMessage();

		addAttachments();
		breadscrumlink();
		savebutton();

		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_clickon_breadscrumlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();
		clearMessage();

		addAttachments();
		breadscrumlink();
		discardbuttons();

		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteSharedtemplatetoastermessage();

	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_clickon_anyotherlink_clickon_save_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();
		clearMessage();

		addAttachments();
		tagslink();
		waits(3);
		savebutton();
		waits(5);
		templatelink();

		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();
	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_clickon_anyotherlink_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();
		clearMessage();

		addAttachments();
		tagslink();
		discardbuttons();
		templatelink();

		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteSharedtemplatetoastermessage();
	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_close_toolspopup_clickon_save_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();
		clearMessage();

		addAttachments();

		closetoolspopup();

		savebutton();
		opentemplatepage();

		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		// deleteTemplateToastermessage();

	}

	public void Managesharedtemplate__remove_title_message_add_attachemnt_close_toolspopup_clickon_discard_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		clickoncreateButton();
		createTemplateToastermessage();
		selectsharedtab();
		manageSharedTemplate();

		clearTitle();
		clearMessage();

		addAttachments();

		closetoolspopup();
		discardbuttons();
		opentemplatepage();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_title_message_add_onemore_department_clickon_update_template_button()
			throws Exception {
		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();

		addseconddept();
		updateTemplate();

		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_title_message_add_onemore_department_clickon_Savedraft_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();

		addseconddept();
		saveDraft();

		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_title_message_add_onemore_department_clickon_delete_confirm_delete_template_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();

		addseconddept();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changeto_personal_template_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();

		button(template.templat_personal_option, "login submit").click();
		logger.info("clicked on select personal option  button");
		wait(10);
		logger.info("wait for 10 sec");

		updateTemplate();

		personaltab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_addemoji_clickon_update_template_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		emoji();
		updateTemplate();

		selectsharedtab();
		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_change_message_content_addemoji_clickon_update_template_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		enterTemplateMessage();
		emoji();
		updateTemplate();

		selectsharedtab();
		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_selectalldept_unselect_alldept_clickon_update_template_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();

		selectallDept();
		selectallDept();
		updateTemplate();

		selectDrafttab();
		manageSharedTemplate();

		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_unselectonedept_close_popup_clickon_save_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		selectonlyoneDept();
		addseconddept();
		closetoolspopup();
		savebutton();

		openTemplatepages();

		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_unselectonedept_close_popup_clickon_discard_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addseconddept();
		closetoolspopup();
		discardbuttons();
		openTemplatepages();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Managesharedtemplate_unselectonedept_clickon_anyotherlink_clickon_save_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addseconddept();
		tagslink();
		savebutton();
		templatelink();

		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_unselectonedept_clicon_anyotherlink_clickon_discard_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addseconddept();
		tagslink();
		discardbuttons();
		templatelink();

		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changetitle_addgif_emoji_clickon_update_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		textbox(template.Template_title).clear();
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).sendKeys("shared");
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		emoji();

		addGIFAttachments();

		updateTemplate();

		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changetitle_addgif_emoji_close_toolspopup_clickon_save_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		textbox(template.Template_title).clear();
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).sendKeys("shared");
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		emoji();

		addGIFAttachments();

		closetoolspopup();
		savebutton();
		openTemplatepages();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changetitle_addgif_emoji_close_toolspopup_clickon_discard_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		textbox(template.Template_title).clear();
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).sendKeys("shared");
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		emoji();

		addGIFAttachments();

		closetoolspopup();
		discardbuttons();
		openTemplatepages();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changetitle_addgif_emoji_clickon_navigationlink_clickon_save_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		textbox(template.Template_title).clear();
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).sendKeys("shared");
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		emoji();

		addGIFAttachments();

		breadscrumlink();
		savebutton();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changetitle_addgif_emoji_clickon_navigationlink_clickon_discard_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		textbox(template.Template_title).clear();
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).sendKeys("shared");
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		emoji();

		addGIFAttachments();

		breadscrumlink();
		discardbuttons();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changetitle_addgif_emoji_clickon_anyotherlink_clickon_save_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		textbox(template.Template_title).clear();
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).sendKeys("shared");
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		emoji();

		addGIFAttachments();

		tagslink();
		savebutton();
		waits(2);
		templatelink();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_changetitle_addgif_emoji_clickon_anytoherlink_clickon_discard_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		textbox(template.Template_title).clear();
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).sendKeys("shared");
		logger.info("title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		emoji();

		addGIFAttachments();

		tagslink();
		discardbuttons();
		waits(2);
		templatelink();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_addimage_removegif_emoji_close_toolspopup_clickon_save_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addAttachments();
		addGIFAttachments();

		addGIFAttachments();
		logger.info("wait for 30 sec");
		button(template.remove_gif, "login submit").click();
		logger.info("clicked on remove selected gif ");
		wait(10);
		logger.info("wait for 10 sec");

		closetoolspopup();
		savebutton();
		openTemplatepages();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_addimage_removegif_close_toolspopup_clickon_discard_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addAttachments();
		addGIFAttachments();

		addGIFAttachments();
		logger.info("wait for 30 sec");
		button(template.remove_gif, "login submit").click();
		logger.info("clicked on remove selected gif ");
		wait(10);
		logger.info("wait for 10 sec");

		closetoolspopup();
		discardbuttons();
		openTemplatepages();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_addimage_removegif_clickon_breadscrumlink_clickon_save_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addAttachments();
		addGIFAttachments();

		addGIFAttachments();
		logger.info("wait for 30 sec");
		button(template.remove_gif, "login submit").click();
		logger.info("clicked on remove selected gif ");
		wait(10);
		logger.info("wait for 10 sec");

		breadscrumlink();
		savebutton();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_addimage_removegif_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addAttachments();
		addGIFAttachments();

		addGIFAttachments();
		logger.info("wait for 30 sec");
		button(template.remove_gif, "login submit").click();
		logger.info("clicked on remove selected gif ");
		wait(10);
		logger.info("wait for 10 sec");

		breadscrumlink();
		discardbuttons();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_addimage_removegif_clickon_anyotherlink_clickon_save_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addAttachments();
		addGIFAttachments();

		addGIFAttachments();
		logger.info("wait for 30 sec");
		button(template.remove_gif, "login submit").click();
		logger.info("clicked on remove selected gif ");
		wait(10);
		logger.info("wait for 10 sec");

		tagslink();
		savebutton();
		templatelink();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Managesharedtemplate_addimage_removegif_clickon_anyotherlink_clickon_discard_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		addseconddept();
		clickoncreateButton();
		createTemplateToastermessage();

		selectsharedtab();
		manageSharedTemplate();
		addAttachments();
		addGIFAttachments();

		addGIFAttachments();
		logger.info("wait for 30 sec");
		button(template.remove_gif, "login submit").click();
		logger.info("clicked on remove selected gif ");
		wait(10);
		logger.info("wait for 10 sec");

		tagslink();
		discardbuttons();
		templatelink();
		selectsharedtab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_clickon_create_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		clickoncreateButton();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_clickon_saveDraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_Messageclickon_saveDraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_Messageclickon_create_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectpersonaloption();
		clickoncreateButton();
		createTemplateToastermessage();
		selectpersonaltab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_close_toolswindow_clickon_savedraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		closetoolspopup();

		saveDraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_message_close_toolswindow_clickon_savedraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		closetoolspopup();

		saveDraft();
		drafttemplateToastermessage();

		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_breadscrumlink_clickon_savedraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		breadscrumlink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_enteronly_title_message_breadscrumlink_clickon_savedraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		breadscrumlink();
		breadscrumsavedraft();
		breadscrumtoastermessage();

		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_nodata_clickon_create_button() throws Exception {

		// login(map);
		openTemplatepage();

		clickoncreateButton();
		drafttemplateToastermessage();
		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_nodata_clickon_savedraft_button() throws Exception {

		// login(map);
		openTemplatepage();

		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_nodata_close_toolspopup_clickon_savedraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();

	}

	public void Createtemplatepage_nodata_clickon_breadscrumlink_clickon_savedraft_button() throws Exception {

		// login(map);
		openTemplatepage();

		breadscrumlink();
		breadscrumsavedraft();
		breadscrumtoastermessage();
		selectDrafttab();
		notitledraftemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_selectsharedoption_nodept_clickon_create_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		selectsharedoption();
		clickoncreateButton();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_selectsharedoption_nodept_clickon_create_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		clickoncreateButton();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_selectsharedoption_nodept_clickon_saveDraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();

		selectsharedoption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_selectsharedoption_nodept_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_selectsharedoption_nodept_close_window_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_selectsharedoption_selectdept_close_window_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_selectsharedoption_nodept_close_window_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		selectsharedoption();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_selectsharedoption_selectdepts_close_window_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_selectsharedoption_selectdepts_close_window_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		selectsharedoption();
		selectonlyoneDept();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_selectsharedoption_selectdeptss_close_window_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_selectsharedoptions_nodept_close_window_clickon_saveDraft_button()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		selectsharedoption();
		closetoolspopup();
		savedraft();
		drafttemplateToastermessage();
		openTemplatepages();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	// 3-01-2022
	public void Createtemplatepage_entertitle_selectsharedoptions_nodept_clickon_saveDraft_button() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		selectsharedoption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void DraftTemplate_defaultmessage() throws Exception {

		logins();
		clicktoolslink();
		opentemplatepage();
		selectDrafttab();
		logger.info(template.draf_default_message.getText());
		if (template.draft_default_message.getText().equalsIgnoreCase("You can save your templates as drafts.")) {
			Assert.assertEquals(template.draft_default_message.getText(), "You can save your templates as drafts.");
			logger.info("You can save your templates as drafts.");
			TestUtil.takeScreenshotAtEndOfTest("You can save your templates as drafts.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(template.draft_default_message.getText(), "You can save your templates as drafts.",
					"You can save your templates as drafts.");
			TestUtil.takeScreenshotAtEndOfTest("You can save your templates as drafts.");// ScreenShot
			// capture
			// capture
		}
	}

	public void Createtemplatepage_entertitle_message_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_sleect_personal_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectpersonaloption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_sleect_shared_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_sleect_shared_select_alldept_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectallDept();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_message_sleect_shared_select_somedepat_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_attachment_sleect_personaloption_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		addAttachments();
		selectpersonaloption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_attachment_sleect_sharedoption_alldept_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		addAttachments();
		selectsharedoption();
		selectallDept();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle_attachment_sleect_sharedoption_select_somedept_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		addAttachments();
		selectsharedoption();
		selectonlyoneDept();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Createtemplatepage_entertitle2char_message_sleect_personaloption_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle2char();
		enterTemplateMessage();
		selectpersonaloption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate2char();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void Draftlist_Createtemplatepage_entertitle2char_message_sleect_sharedoption_clickon_savedraft_button_tempalte_display_indraftlist()
			throws Exception {

		// login(map);
		opentemplatepage();
		selectDrafttab();
		openaddTemplatePage();
		enterTemplatTitle2char();
		enterTemplateMessage();
		selectsharedoption();
		selectonlyoneDept();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate2char();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
	}

	public void draftlist_managetemplatepage_clickon_delete_confirmedelete_buttons() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectpersonaloption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		manageSharedTemplate();
		clickondeletetemplateButton();
		clickonconfirmdelete_template();
		waits(5);
		closetoolspopup();
		logout();
	}

	public void draftlist_clickon_deletebutton_open_confirmationpopup() throws Exception {

		// login(map);
		openTemplatepage();
		enterTemplatTitle();
		enterTemplateMessage();
		selectsharedoption();
		saveDraft();
		drafttemplateToastermessage();
		selectDrafttab();
		clickondeleteButton();
		logger.info(template.conformationpop_headding.getText());
		if (template.conformationpop_headding.getText().equalsIgnoreCase("Do you want to remove this Draft?")) {
			Assert.assertEquals(template.conformationpop_headding.getText(), "Do you want to remove this Draft?");
			logger.info("Create your first personal template.");
			TestUtil.takeScreenshotAtEndOfTest("Create your first personal template.");// ScreenShot
																						// capture

		} else {
			Assert.assertNotEquals(template.conformationpop_headding.getText(), "Do you want to remove this Draft?",
					"Do you want to remove this Draft?");
			TestUtil.takeScreenshotAtEndOfTest("Create your first personal template.");// ScreenShot
																						// capture
		}
		template.cancelButton.click();

	}

}
