package com.beetext.qa.Workflows.templates;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.ToolsTemplates;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.ToolsTemplate;
import com.beetext.qa.util.TestUtil;

public class ToolsTemplateWF extends ToolsTemplates {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	ToolsTemplate template = new ToolsTemplate();
	public static String currentDir = System.getProperty("user.dir");

	public void personaltemplate_title_3_char_validation() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("pe");

		logger.info("Template Title : pe");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking in web application");
		logger.info("Template Message :personal template creation checking in web application");

		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.Template_title_min_3_char.getText());
		if (template.Template_title_min_3_char.getText().equalsIgnoreCase("Title must be at least 3 characters.")) {
			Assert.assertEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.");
			logger.info("Title must be at least 3 characters. case is working");
			TestUtil.takeScreenshotAtEndOfTest("tempalte title 3 characters validation checking");// ScreenShot
																									// capture

		} else {
			Assert.assertNotEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.",
					"Title must be at least 3 characters.");
			logger.info("tempalte title 3 characters validation  creation failed");
			TestUtil.takeScreenshotAtEndOfTest("tempalte title 3 characters validation creation failed.");// ScreenShot
																											// capture
		}

	}

	public void personaltemplate_title_1_char_validation() throws Exception {

		login();
		logger.info("wait for 10 sec");
		textbox(template.Template_title).sendKeys("p");

		logger.info("Template Title : p");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking in web application");
		logger.info("Template Message :personal template creation checking in web application");

		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.Template_title_min_3_char.getText());
		if (template.Template_title_min_3_char.getText().equalsIgnoreCase("Title must be at least 3 characters.")) {
			Assert.assertEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.");
			logger.info("Title must be at least 3 characters. case is working");
			logger.info("Title must be at least 3 characters.");
			TestUtil.takeScreenshotAtEndOfTest("tempalte title 3 characters validation checking1");// ScreenShot
																									// capture

		} else {
			Assert.assertNotEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.",
					"Title must be at least 3 characters.");
			logger.info("tempalte title 3 characters validation  creation failed");
			TestUtil.takeScreenshotAtEndOfTest("tempalte title 3 characters validation creation failed.1");// ScreenShot
																											// capture
		}

	}

	public void sharedtemplate_title_3_char_validation() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("pe");

		logger.info("Template Title : pe");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking in web application");
		logger.info("Template Message :personal template creation checking in web application");
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.Template_title_min_3_char.getText());
		if (template.Template_title_min_3_char.getText().equalsIgnoreCase("Title must be at least 3 characters.")) {
			Assert.assertEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.");
			logger.info("Title must be at least 3 characters. case is working");
			logger.info("Title must be at least 3 characters.");
			TestUtil.takeScreenshotAtEndOfTest("tempalte title 3 characters validation checking");// ScreenShot
																									// capture

		} else {
			Assert.assertNotEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.",
					"Title must be at least 3 characters.");
			logger.info("tempalte title 3 characters validation  creation failed");
			TestUtil.takeScreenshotAtEndOfTest("tempalte title 3 characters validation creation failed.");// ScreenShot
																											// capture
		}

	}

	public void sharedtemplate_title_already_exit_validation() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("sharedtemplate");

		logger.info("Template already existing Title : sharedtemplate ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking in web application");
		logger.info("Template Message :personal template creation checking in web application");
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.shared_Template_title_alrady_exist.getText());
		if (template.shared_Template_title_alrady_exist.getText()
				.equalsIgnoreCase("Template with name 'sharedtemplate' already exists")) {
			Assert.assertEquals(template.shared_Template_title_alrady_exist.getText(),
					"Template with name 'sharedtemplate' already exists");
			logger.info("Template with name 'sharedtemplate' already exists");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'sharedtemplate' already exists");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(template.shared_Template_title_alrady_exist.getText(),
					"Template with name 'sharedtemplate' already exists",
					"Template with name 'sharedtemplate' already exists");
			logger.info("Template with name 'sharedtemplate' already exists creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'sharedtemplate' already exists creation failed.");// ScreenShot
			// capture
		}

	}

	public void personaltemplate_title_already_exit_validation() throws Exception {

		templatelink();
		//button(template.discard_draft, "button").click();
		addtemplate();
		textbox(template.Template_title).sendKeys("attachments");

		logger.info("Template already existing Title : attachments ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking in web application");
		logger.info("Template Message :personal template creation checking in web application");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_Template_title_alrady_exist.getText());
		if (template.personal_Template_title_alrady_exist.getText()
				.equalsIgnoreCase("Template with name 'attachments' already exists.")) {
			Assert.assertEquals(template.personal_Template_title_alrady_exist.getText(),
					"Template with name 'attachments' already exists.");
			logger.info("Template with name 'attachments' already exists.");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'attachments' already exists.");// ScreenShot
																									// capture

		} else {
			Assert.assertNotEquals(template.personal_Template_title_alrady_exist.getText(),
					"Template with name 'attachments' already exists.",
					"Template with name 'attachments' already exists.");
			logger.info("Template with name 'attachments' already exists. creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'attachments' already exists.");// ScreenShot
																									// capture
		}

	}

	public void personaltemplate_only_title_clickon_create_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'attachments' already exists.");// ScreenShot
																									// capture
		}
		waits(5);
		draftTab();

		button(template.Manage_draft_templates_only_title, "login submit").click();
		logger.info("open in manage template page");
		waits(5);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();

		deleteToastermessage();

	}

	public void sharedtemplate_only_title_clickon_create_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		waits(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'attachments' already exists.");// ScreenShot
																									// capture
		}
		wait(5);
		draftTab();

		button(template.Manage_draft_templates_only_title, "login submit").click();
		logger.info("open in manage template page");
		waits(5);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();
		deleteToastermessage();
	}

	public void sharedtemplate_no_data_clickon_create_button() throws Exception {

		templatelink();
		//button(template.discard_draft, "button").click();
		addtemplate();

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'attachments' already exists.");// ScreenShot
																									// capture
		}
		wait(5);
		draftTab();

		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("open in manage template page");
		waits(5);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted..");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void personaltemplate_no_data_clickon_create_button() throws Exception {

		login();
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Template with name 'attachments' already exists.");// ScreenShot
																									// capture
		}
		waits(5);
		button(template.draft_tab, "login submit").click();
		logger.info("Draft tab is  clicked");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("open in manage template page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete template");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void personaltemplate_creation_deletion() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking in web application");
		logger.info("Template Message :personal template creation checking in web application");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();
		deleteconfirmdelete();

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void personaltemplate_1000char_message_creation_deletion() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("personal tempalte open in manage page");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void sharedtemplate_creation_deletion() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		waits(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template shared has been created.");
			logger.info("shared template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("shared template created");// ScreenShot
																			// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been creation failed");
			logger.info("shared template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void sharedtemplate_1000char_message_creation_deletion() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		waits(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template shared has been created.");
			logger.info("shared template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("shared template created");// ScreenShot
																			// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been creation failed");
			logger.info("shared template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void select_sharedoption_dont_select_dept() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared draft template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(5);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void clickedon_sharedtab_display_shared_template_list() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		waits(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template shared has been created.");
			logger.info("shared template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("shared template created");// ScreenShot
																			// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been creation failed");
			logger.info("shared template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void clickon_personaltab_display_personal_templates() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();
		deleteconfirmdelete();

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void create_draft_template_clickon_draft_tab_display_draft_templates() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared draft template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(5);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void shared_template_give_title_nodept_clickon_breadscrumlink_clickon_savedraft() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);
		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		wait(5);
		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(10);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void shared_template_give_title_message_nodept_clickon_breadscrumlink_clickon_savedraft() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared draft template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);
		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		waits(5);
		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		waits(2);
		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(10);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void shared_template_give_title_message_selectdept_clickon_breadscrumlink_clickon_savedraft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared draft template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		template.breadscrumlinks.click();
		logger.info("clicked on breadscrum link");
		waits(5);
		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(10);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void shared_template_give_title_nodept_clickon_savedraft_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);

		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(10);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void shared_template_give_title_message_nodept_clickon_savedraft_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared draft template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);
		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(10);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void shared_template_give_title_message_select_dept_clickon_savedraft_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared draft template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(2);
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on  add template shared button");
		wait(5);
		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft..");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  draft tempalte tab");
		waits(5);
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("shared tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_delete_toaster_message.getText());
		if (template.draft_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_message.getText(),
					"Your template draft has been deleted.", "Your template draft has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared draft has been deletion failed");// ScreenShot
																										// capture
		}

	}

	public void createpersonaltemplate_manangepage_changeto_shared_tempalte() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("Title edited :shared");
		wait(2);

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title edited :shared");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info(" in manage page changed to shared option");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("All departments are selected");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(1);
		logger.info("wait for 1 sec");
		logger.info(template.shared_template_update_toaster_message.getText());
		if (template.shared_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been updated.")) {
			Assert.assertEquals(template.shared_template_update_toaster_message.getText(),
					"Your template shared has been updated.");
			logger.info("Your template shared has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_update_toaster_message.getText(),
					"Your template shared has been updated.", "Your template shared has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been updation failed");// ScreenShot
																								// capture
		}
		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createpersonaltemplate_manangepage_changeto_shared_tempalte_select_one_department() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		waits(3);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("Title edited :shared");
		wait(2);

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title edited :shared");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info(" in manage page changed to shared option");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.select_specifif_department, "login submit").click();
		logger.info("All departments are selected");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(1);
		logger.info("wait for 1 sec");
		logger.info(template.shared_template_update_toaster_message.getText());
		if (template.shared_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been updated.")) {
			Assert.assertEquals(template.shared_template_update_toaster_message.getText(),
					"Your template shared has been updated.");
			logger.info("Your template shared has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_update_toaster_message.getText(),
					"Your template shared has been updated.", "Your template shared has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been updation failed");// ScreenShot
																								// capture
		}
		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void sharedtemplate_creation_mangepage_unselect_alldepartments() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		waits(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template shared has been created.");
			logger.info("shared template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("shared template created");// ScreenShot
																			// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been creation failed");
			logger.info("shared template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		waits(2);
		button(template.update_template, "login submit").click();
		logger.info("Update Tempalte button clicked");
		wait(2);

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void sharedtemplate_creation_edit_template() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking in web application");
		logger.info("Template Message :shared template creation checking in web application");
		wait(2);

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  add template shared button");
		waits(2);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked select all department option");
		waits(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template shared has been created.");
			logger.info("shared template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("shared template created");// ScreenShot
																			// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been creation failed");
			logger.info("shared template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(10);
		logger.info("wait for 10 sec");
		textbox(template.Template_title).clear();

		logger.info("Template Title : titlecleared");
		wait(2);
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title : shared");
		wait(2);
		button(template.update_template, "login submit").click();
		logger.info("Update Tempalte button clicked");
		wait(2);

		logger.info(template.shared_template_update_toaster_message.getText());
		if (template.shared_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been updated.")) {
			Assert.assertEquals(template.shared_template_update_toaster_message.getText(),
					"Your template shared has been updated.");
			logger.info("Your template shared has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_update_toaster_message.getText(),
					"Your template shared has been updated.", "Your template shared has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been updated");// ScreenShot
																						// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createpersonaltemplate_manangepage_edittitle_save_changes() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("Title edited :shared");
		wait(2);

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title edited :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(1);
		logger.info("wait for 1 sec");
		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template shared has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updation failed");// ScreenShot
																									// capture
		}
		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();
		deleteconfirmdelete();

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void createpersonaltemplate_manangepage_remove_title_save_changes() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(1);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("Title edited :removed");
		wait(2);

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void createpersonaltemplate_manangepage_remove_message_attachments_save_changes() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title : personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal message text");
		logger.info("Message:" + message);

		button(template.attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(5);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("CREATE Tempalte button clicked");
		wait(2);
		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			logger.info("personal template created successfully");
			TestUtil.takeScreenshotAtEndOfTest("perosonal template created");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been creation failed");
			logger.info("personal template creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been creation failed.");// ScreenShot
																									// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		managepersonal();
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);
		logger.info("message is cleared");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.clear();
		button(template.remove_attachment, "login submit").click();
		logger.info("selected attachment is remvoed");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("shared tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");
		deleteconfirmdelete();
		wait(2);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void createpersonaltemplate_manangepage_title_andmessage_clickon_savedarft() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title :personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal template creation checking");

		logger.info("Template message :personal template creation cheking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.draft_Template_btn, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(2);

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template  has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template  has been deleted.");
			logger.info("Your template  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template  has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template  has been deleted.", "Your template  has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void createsharedtemplate_entertitle_selectct_shared_dont_select_department_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.draft_Template_btn, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(2);

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);
		// div[@aria-label='Your template shared has been deleted.']
		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createpersonaltemplate_manangepage_title_andattachments_clickon_create_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("personal");

		logger.info("Template Title :personal");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(5);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("create  Tempalte button clicked");
		wait(2);

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture
		}

		waits(5);
		logger.info("wait for 5 sec");
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template  has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template  has been deleted.");
			logger.info("Your template  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template  has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template  has been deleted.", "Your template  has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}

	}

	public void createsharedtemplate_entertitle_message_selectct_shared__select_department_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Message :shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.selectall_departments, "login submit").click();
		logger.info("department selected");
		wait(10);

		button(template.draft_Template_btn, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(2);

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_message_selectct_shared__dont_select_department_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Message :shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.draft_Template_btn, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(2);

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(5);
		logger.info("wait for 5 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_selectct_shared_dont_select_department_closewindow_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(20);
		logger.info("wait for 30 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_message_selectct_shared__select_department_close_tools_window_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Message :shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.selectall_departments, "login submit").click();
		logger.info("department selected");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}
		wait(20);
		logger.info("wait for 20 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_message_selectct_shared__dont_select_department_close_tools_window_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Message :shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait for 10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_clickon_breadscrumlik_clickon_savedarft() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on bradscrum link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait for 10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_message_clickon_breadscrumlik_clickon_savedarft() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("Template message ");

		logger.info("Template Message :Template message");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on bradscrum link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait for 10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_close_window_clickon_discard() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools pop up button");
		wait(2);
		logger.info("wait for 2 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
		toolslink();

	}

	public void createsharedtemplate_entertitle_message_closewindow_clickon_discard() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("Template message ");

		logger.info("Template Message :Template message");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("close tools pop up");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
		toolslink();
	}

	public void createsharedtemplate_entertitle_clickon_breadscrumlink_clickon_discard() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on close tools pop up button");
		wait(2);
		logger.info("wait for 2 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createsharedtemplate_entertitle_message_clickon_breadscrumlink_clickon_discard() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("Template message ");

		logger.info("Template Message :Template message");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("close tools pop up");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void opencreatetemplate_nodata_close_tools_window_clickon_savedarft() throws Exception {

		login();
		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(2);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}
		wait(20);
		logger.info("wait for 20 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("'Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template  has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.", "'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void opencreatetemplate_nodata_close_tools_window_discard_button() throws Exception {

		login();
		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
		toolslink();
	}

	public void opencreatetemplate_nodata_clickon_breadscrumlik_clickon_savedarft() throws Exception {

		login();
		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on bradscrum link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait for 10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("'Your Template has been deleted")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your Template has been deleted");
			logger.info("'Your Template has been deleted");
			TestUtil.takeScreenshotAtEndOfTest("'Your Template has been deleted");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your Template has been deleted", "'Your Template has been deleted");
			TestUtil.takeScreenshotAtEndOfTest("'Your Template has been deleted");// ScreenShot
																					// capture
		}

	}

	public void opencreatetemplate_nodata_clickon_breadscrumlik_clickon_discard() throws Exception {

		login();

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on bradscrum link");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createsharedtemplate_entertitle__dont_select_department_close_tools_window_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait for 10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_message__dont_select_department_close_tools_window_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(2);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait for 10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_message__select_department_close_tools_window_clickon_savedarft()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("save draft  Tempalte button clicked");
		wait(1);

		logger.info("wait for 1 sec");

		logger.info(template.toastermessage.getText());
		if (template.toastermessage.getText().equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.toastermessage.getText(), "Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.toastermessage.getText(), "Your template has been saved as a draft.",
					"Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait for 10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(2);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared  has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared  has been deleted.", "Your template shared  has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deletion failed");// ScreenShot
																								// capture
		}

	}

	public void createsharedtemplate_entertitle_close_tools_window_clickon_discard() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
		toolslink();
	}

	public void createsharedtemplate_entertitle_Message_no_dept_close_tools_window_clickon_discard() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation message typing");

		logger.info("Template Title :shared template creation message typing");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
		toolslink();
	}

	public void createsharedtemplate_entertitle_Message_select_dept_close_tools_window_clickon_discard()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation message typing");

		logger.info("Template Title :shared template creation message typing");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on  select all department radio button");
		wait(10);

		button(template.close_tools_popup, "login submit").click();
		logger.info("Tools pop up is closed");
		wait(2);
		logger.info("wait for 2 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
		toolslink();
	}

	public void createsharedtemplate_entertitle_clickon_any_other_link_clickon_discard() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.tags_link, "login submit").click();
		logger.info("clikced on tags link");
		wait(10);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createsharedtemplate_entertitle_message_nodept_clickon_any_other_link_clickon_discard()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Title :shared  template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.tags_link, "login submit").click();
		logger.info("clikced on tags link");
		wait(10);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createsharedtemplate_entertitle_message_selectdept_clickon_any_other_link_clickon_discard()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Title :shared  template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.tags_link, "login submit").click();
		logger.info("clikced on tags link");
		wait(10);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createsharedtemplate_entertitle_message_selectdept_clickon_outside_tools_popup_clickon_discard()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");

		logger.info("Template Title :shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");

		logger.info("Template Title :shared  template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on  shared option");
		wait(10);

		button(template.outsideclick, "login submit").click();
		logger.info("clikced on out side of the tools pop up");
		wait(10);
		logger.info("wait for 10 sec");

		driver.close();
		wait(5);
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
		toolslink();
	}

	public void clickon_personaltab_check_default_message_validation() throws Exception {

		textbox(Repository.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id :oneagent@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password :tech1234");
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		wait(10);
		logger.info("wait for 2 min");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");

		logger.info(template.personal_default_message.getText());
		if (template.personal_default_message.getText().equalsIgnoreCase("Create your first personal template.")) {
			Assert.assertEquals(template.personal_default_message.getText(), "Create your first personal template.");
			logger.info("Create your first personal template.");
			TestUtil.takeScreenshotAtEndOfTest("Create your first personal template.");// ScreenShot
																						// capture

		} else {
			Assert.assertNotEquals(template.personal_default_message.getText(), "Create your first personal template.",
					"Create your first personal template.");
			TestUtil.takeScreenshotAtEndOfTest("Create your first personal template.");// ScreenShot
																						// capture
		}

	}

	public void clickon_sharedtab_check_default_message_validation() throws Exception {

		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared link");
		wait(10);
		logger.info("wait for 10 sec");

		logger.info(template.shared_default_message.getText());
		if (template.shared_default_message.getText().equalsIgnoreCase("Create a template for your numbers.")) {
			Assert.assertEquals(template.shared_default_message.getText(), "Create a template for your numbers.");
			logger.info("Create a template for your numbers.");
			TestUtil.takeScreenshotAtEndOfTest("Create a template for your numbers.");// ScreenShot
																						// capture

		} else {
			Assert.assertNotEquals(template.shared_default_message.getText(), "Create a template for your numbers.",
					"Create a template for your numbers.");
			TestUtil.takeScreenshotAtEndOfTest("Create a template for your numbers.");// ScreenShot
																						// capture
		}

	}

	public void clickon_drafttab_check_default_message_validation() throws Exception {

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft link");
		wait(10);
		logger.info("wait for 10 sec");

		logger.info(template.draft_default_message.getText());
		if (template.draft_default_message.getText().equalsIgnoreCase("You can save your templates as drafts.")) {
			Assert.assertEquals(template.draft_default_message.getText(), "You can save your templates as drafts.");
			logger.info("You can save your templates as drafts.");
			TestUtil.takeScreenshotAtEndOfTest("You can save your templates as drafts.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.draft_default_message.getText(), "You can save your templates as drafts.",
					"You can save your templates as drafts.");
			TestUtil.takeScreenshotAtEndOfTest("You can save your templates as drafts.");// ScreenShot
																					// capture
		}

	}

	public void createtempalte_dont_fill_data_clickon_create_button() throws Exception {
		opencreatetemplatepage();

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		waits(3);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("'Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.", "'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void createsharedtempalte_dont_fill_data_clickon_create_button() throws Exception {

		opencreatetemplatepage();

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("'Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.", "'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void createsharedtempalte_dont_fill_data_clickon_draft_template_button() throws Exception {

		login();

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.draft_Template_btn, "login submit").click();
		logger.info("clicked on draft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("'Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.", "'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void createpersonaltempalte_only_message_data_clickon_draft_template_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("tech1234");
		logger.info("message :tech1234");
		wait(2);
		logger.info("wait of 2 ");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.draft_Template_btn, "login submit").click();
		logger.info("clicked on draft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("'Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.", "'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void createpersonaltempalte_title_and_message_clickon_draft_template_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");
		logger.info("message :tech1234");
		wait(2);
		logger.info("wait of 2 ");

		textbox(template.Template_message).sendKeys("tech1234");
		logger.info("message :tech1234");
		wait(2);
		logger.info("wait of 2 ");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.draft_Template_btn, "login submit").click();
		logger.info("clicked on draft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your Template  shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your Template  shared has been deleted.");
			logger.info("'Your Template  shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your Template  shared has been deleted.", "'Your Template  shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your Template  shared has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void createsharedtempalte_nodata_clickon_draft_template_button() throws Exception {

		login();

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on template shared option");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.draft_Template_btn, "login submit").click();
		logger.info("clicked on draft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("'Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template has been deleted.", "'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void createsharedtempalte_fill_title_message_clickon_savedraft_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title:shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_draft, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_sharedtemplates_only_title, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your Template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your Template shared has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your Template shared has been deleted.", "'Your Template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void createtempalte_fill_title_message_close_popup_clickon_savedraft_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title:shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on create template button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save daft button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.tools_link, "login submit").click();
		logger.info("clicked on  tools link");
		wait(10);
		button(template.templateLink, "login submit").click();
		logger.info("clicked on  shared tempalte link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your Template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your Template shared has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your Template shared has been deleted.", "'Your Template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been deleted.");// ScreenShot
																					// capture
		}

	}

	public void createtempalte_dont_fill_data_close_popup_clickon_discard_button() throws Exception {

		login();

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on create template button");
		waits(2);
		logger.info("wait for 10 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createpersonaltempalte_fill_title_message_click_anyother_link_clickon_savedraft_button()
			throws Exception {

		toolslink();
		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on create template button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save daft button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);

		button(template.templateLink, "login submit").click();
		logger.info("clicked on  shared tempalte link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void createtempalte_fill_data_close_popup_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 10 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createsharedtempalte_fill_title_message_click_anyother_link_clickon_savedraft_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title:shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("sahred tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on create template button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save daft button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);

		button(template.templateLink, "login submit").click();
		logger.info("clicked on  shared tempalte link");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void createsharedtempalte_fill_data_close_popup_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title:shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tagslink");
		waits(2);
		logger.info("wait for 10 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			// button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void managepersonaltempalte_change_title_message_click_anyother_link_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("'Your template personal has been created")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created");
			logger.info("'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created", "'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("manage page open the personal tempalte ");
		waits(5);
		logger.info("wait  10 sec");

		textbox(template.Template_title).clear();
		logger.info("Title:clearing");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		waits(10);
		logger.info("wait  10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		logger.info(template.update_toaster_message.getText());
		if (template.update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("Your template has been successfully updated.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.'");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(template.update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.'");// ScreenShot
																								// capture
		}

		wait(10);
		button(template.templateLink, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void managepersonaltempalte_change_title_message_click_anyother_link_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("'Your template personal has been created")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created");
			logger.info("'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created", "'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created");// ScreenShot
																							// capture
		}

		wait(10);

		button(template.personal_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("manage page open the personal tempalte ");
		wait(10);
		logger.info("wait  10 sec");

		textbox(template.Template_title).clear();
		logger.info("Title:clearing");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		waits(2);
		logger.info("wait  2 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		wait(10);

		button(template.personal_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void managepersonaltempalte_change_title_message_close_tools_popup_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("'Your template personal has been created")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created");
			logger.info("'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created", "'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("manage page open the personal tempalte ");
		wait(10);
		logger.info("wait  10 sec");

		textbox(template.Template_title).clear();
		logger.info("Title:clearing");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on tags link");
		wait(10);
		logger.info("wait  10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		logger.info(template.update_toaster_message.getText());
		if (template.update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("Your template has been successfully updated.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.'");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(template.update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.'");// ScreenShot
																								// capture
		}

		wait(10);
		button(template.tools_link, "login submit").click();
		logger.info("clicked on  tools link");
		wait(10);
		button(template.templateLink, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void managepersonaltempalte_change_title_message_close_popup_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("'Your template personal has been created")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created");
			logger.info("'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"'Your template personal has been created", "'Your template personal has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created");// ScreenShot
																							// capture
		}

		wait(10);

		button(template.personal_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("manage page open the personal tempalte ");
		wait(10);
		logger.info("wait  10 sec");

		textbox(template.Template_title).clear();
		logger.info("Title:clearing");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close the popup");
		waits(2);
		logger.info("wait  10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		button(template.tools_link, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.personal_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void managesharedtempalte_changeto_personaltab_click_breadscrumlink_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created");
			logger.info("'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created", "'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");

		textbox(template.Template_title).clear();
		logger.info("Title:clearing");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.templat_personal_option, "login submit").click();
		logger.info("manage page select personal option");
		wait(5);
		logger.info("wait  5 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on navigation link");
		wait(10);
		logger.info("wait  10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		logger.info(template.update_toaster_message.getText());
		if (template.update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("Your template has been successfully updated.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.'");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(template.update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.'");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait for 10 sec");
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void managesharedtempalte_changeto_personaltab_click_breadscrumlink_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created");
			logger.info("'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created", "'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");

		textbox(template.Template_title).clear();
		logger.info("Title:clearing");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title:personal");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message : personal tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.templat_personal_option, "login submit").click();
		logger.info("manage page select personal option");
		wait(5);
		logger.info("wait  5 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on navigation link");
		waits(2);
		logger.info("wait  10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		wait(10);
		logger.info("wait for 10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void managesharedtempalte__addattachments_click_anyotherlink_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created");
			logger.info("'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created", "'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		waits(10);
		logger.info("wait  10 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.tags_link, "login submit").click();
		logger.info("clicked on navigation link");
		waits(5);
		logger.info("wait  10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		logger.info(template.update_toaster_messages.getText());
		if (template.update_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.update_toaster_messages.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(template.update_toaster_messages.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on  template link");
		waits(5);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void managesharedtempalte__addattachments_click_anyotherlink_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been created")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created");
			logger.info("'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created", "'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		waits(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		waits(5);
		logger.info("wait  10 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.tags_link, "login submit").click();
		logger.info("clicked on navigation link");
		waits(2);
		logger.info("wait  10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard tempalte button ");
		waits(5);
		logger.info("wait for 5 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on  template link");
		waits(5);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void managesharedtempalte__removeattachments_click_anyotherlink_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		waits(5);
		logger.info("wait for 5 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		logger.info("wait for 30 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created");
			logger.info("'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created", "'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		waits(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		waits(10);
		logger.info("wait  10 sec");

		button(template.remove_attachment, "login submit").click();
		logger.info("clicked on remove attachment cross mark");
		waits(10);
		logger.info("wait  10 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on navigation link");
		waits(10);
		logger.info("wait  10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save tempalte button ");
		wait(1);
		logger.info("wait  1 sec");

		logger.info(template.update_toaster_messages.getText());
		if (template.update_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.update_toaster_messages.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(template.update_toaster_messages.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on  template link");
		waits(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  personal tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void managesharedtempalte__removeattachments_click_anyotherlink_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("Message : shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been created")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created");
			logger.info("'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"'Your template shared has been created", "'Your template shared has been created");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on personal tab ");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		waits(5);
		logger.info("wait  10 sec");

		button(template.remove_attachment, "login submit").click();
		logger.info("manage page remove attachment ");
		waits(5);
		logger.info("wait  10 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on navigation link");
		waits(2);
		logger.info("wait  10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard tempalte button ");
		waits(2);
		logger.info("wait for 5 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on  template link");
		waits(5);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on  shared tempalte tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("tempalte open in manage page");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_title_click_anyotherlink_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link  button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save button  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_title_click_anyotherlink_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link  button");
		waits(2);
		logger.info("wait for 10 sec");

		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_give_title_close_tools_popup_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools pop up  button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save button  button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link ");
		wait(10);
		button(template.templateLink, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void cratetemplate_give_title_close_tools_popup_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on tags link  button");
		waits(2);
		logger.info("wait for 10 sec");

		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			toolslink();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_give_title_clickon_breascrumlink_savedraft_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void cratetemplate_give_title_clickon_breadscrumlink_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link  button");
		waits(2);
		logger.info("wait for 10 sec");

		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}
	}

	public void createsharedtemplate_give_title_closepopup_savedraft_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared option");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools pop up icon");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void cratetsharedemplate_give_title_closepopup_clickon_discard_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared option");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools pop up icon");
		waits(2);
		logger.info("wait for 10 sec");

		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			toolslink();

			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_give_title_clickon_create_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_title_clickon_savedraft_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_draft, "login submit").click();
		logger.info("clicked on savedraft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'", "'Your template shared has been deleted.'");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_message_clickon_create_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on savedraft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlymessage_clickon_savedraft_button() throws Exception {

		login();
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_draft, "login submit").click();
		logger.info("clicked on savedraft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlymessage_clickon_navigationlink_clickon_savedraft_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum template button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlymessage_clickon_navigationlink_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum template button");
		waits(2);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_give_onlymessage_close_popup_clickon_savedraft_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("close tools popup");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlymessage_close_toolspopup_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		wait(10);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			toolslink();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			toolslink();
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_give_onlymessage_clickon_anyotherlink_clickon_savedraft_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("click on tags link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlymessage_clickon_anyotherlink_clickon_discard_button() throws Exception {

		login();

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message typing : shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tagslink");
		wait(10);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_selectpersonaloption_add_attachment_clickon_create_button() throws Exception {

		login();

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_selectpersonaloption_add_attachment_clickon_saveDraft_button() throws Exception {

		login();

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_selectpersonaloption_add_attachment_clickon_breadscrumlink_clickon_savedraft_button()
			throws Exception {

		login();

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrumlink");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on savedraft button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlyattachment_clickon_breadscrumlink_clickon_discard_button() throws Exception {

		login();

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		wait(10);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_selectpersonaloption_add_attachment_clickon_anyotherlink_clickon_savedraft_button()
			throws Exception {

		login();

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on savedraft button");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlyattachment_clickon_anyotherlink_clickon_discard_button() throws Exception {

		login();

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		waits(5);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "button").click();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void createtemplate_selectpersonaloption_add_attachment_close_tools_popup_clickon_savedraft_button()
			throws Exception {

		login();

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on savedraft button");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft");
			logger.info("Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft", "'Your template has been saved as a draft");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link ");
		waits(5);
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		waits(5);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("manage page open the shared tempalte ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("'Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been deleted.'");// ScreenShot
																							// capture
		}

	}

	public void createtemplate_give_onlyattachment_close_toolspopup_clickon_discard_button() throws Exception {

		login();

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		waits(5);
		logger.info("wait for 10 sec");
		logger.info("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(template.discard_draft));
			button(template.discard_draft, "button").isEnabled();
			button(template.discard_draft, "login submit").click();
			toolslink();
			logger.info("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			logger.info("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_updatetemplate_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(10);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on manage template button");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("'Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"'Your template personal has been updated.");
			logger.info("'Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"'Your template personal has been updated.", "'Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_savedraft_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"'Your template has been saved as a draft.", "'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_deletetemplate_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_breadscrumlink_clickon_savedraft_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_anyotherlink_clickon_save_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_anyotherlink_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_closepopup_clickon_save_button() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on temolate link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_clickon_close_tools_popup_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link button");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link button");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_removetextmessage_add_attachment_clickon_breadscrumlink_clickon_save_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("template message is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonoption_changetitle_removemessage_add_attachment_clickonbreadscrumlink_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_message.sendKeys(Keys.CONTROL + "a");
		// sending DELETE key
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("template message is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_removetextmessage_add_attachment_clickon_closetoolspopup_clickon_save_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("template message is cleared");
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonoption_changetitle_removemessage_add_attachment_clickon_close_tools_popup_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_message.sendKeys(Keys.CONTROL + "a");
		// sending DELETE key
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("template message is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_changetitle_removetextmessage_add_attachment_clickon_anyotherlink_clickon_save_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_message.sendKeys(Keys.CONTROL + "a");
		// sending DELETE key
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("template message is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.tags_link, "login submit").click();
		logger.info("clicked on close tags link");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template has been successfully updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.");
			logger.info("'Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template has been successfully updated.", "Your template has been successfully updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been successfully updated.");// ScreenShot
																								// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonoption_changetitle_removemessage_add_attachment_clickon_anyotherlink_clickon_discard_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("template message is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on tempalte link ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.draft_template_delete_toaster_messages.getText());
		if (template.draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}
		closetoolspopup();
		logout();

	}

	public void Managetemplate_selectpersonopton_removetitle_textmessage_add_attachment_clickon_anyotherlink_clickon_updatetempalte_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.update_template, "login submit").click();
		logger.info("clicked on update  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft..");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase(" Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					" Your template has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_textmessage_add_attachment_clickon_anyotherlink_clickon_savedrafttempalte_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft..");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template has been saved as a draft.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");

		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}
		closetoolspopup();
		logout();
	}

	public void Managetemplate_selectpersonopton_removetitle_textmessage_add_attachment_clickon_delete_confirm_delete_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_update_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked updatetemplate  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_savedraft_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");
		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_draft, "login submit").click();
		logger.info("clicked save draft  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab ");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on Manage button");
		wait(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_navigationlink_clickon_save_template_button()
			throws Exception {

		opencreatetemplatepage();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_navigationlink_clickon_discard_template_button()
			throws Exception {

		// login();
		opencreatetemplatepage();
		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_anyotherlink_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");
		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_anyohterlink_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_closetoolspopupicon_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on cross mark link");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");
		managepersonal();
		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_closetoolspopupicon_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		String message = Char1000_randomestring();
		textbox(template.Template_message).sendKeys(message);
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close toolspopup icon");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard button");
		wait(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_1000char_textmessage_dragthe_firstname_companyname_clickon_update_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		Actions act = new Actions(driver);
		/*
		 * act.dragAndDrop(template.firstnamedrag,template.Template_message).build().
		 * perform(); wait(2); logger.info("first name is dragged");
		 * 
		 * act.dragAndDrop(template.companynamedrag,template.Template_message).build().
		 * perform(); wait(2); logger.info("company name is dragged");
		 */

		// Element which needs to drag.
		WebElement From = driver.findElement(By.xpath("//span[normalize-space()='[firstname]']"));

		// Element on which need to drop.
		WebElement To = driver.findElement(By.xpath("//textarea[@id='template_message']"));

		act.dragAndDrop(From, To).build().perform();

		// Dragged and dropped.
		/*
		 * act.moveToElement(From) .pause(Duration.ofSeconds(2)) .moveToElement(To)
		 * .pause(Duration.ofSeconds(2)) .click().build().perform();
		 */

		wait(10);
		textbox(template.Template_message).sendKeys("personal tempalte creation checking");
		logger.info("Message:" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_Update_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_savedraft_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab button");
		wait(10);
		logger.info("wait  10 sec");

		managepersonaltemp();
		deleteconfirmdelete();

		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_delete_confirmdelete_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(3);
		logger.info("wait for 5 sec");

		wait(10);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_anyohterlink_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(3);
		logger.info("wait for 5 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link button");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link button");
		waits(10);
		logger.info("wait  10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_anyohterlink_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link button");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		waits(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link button");
		waits(5);
		logger.info("wait  10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon__clickonbreadscrumlink_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link button");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_breadscrumlink_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link button");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		waits(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon__close_toolspopup_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup icon");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on save  template button");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_update_toaster_message.getText());
		if (template.personal_template_update_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been updated.")) {
			Assert.assertEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_update_toaster_message.getText(),
					"Your template personal has been updated.", "Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		waits(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on templte link");
		waits(10);
		logger.info("wait  10 sec");
		managepersonal();

		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_closetooslpopup_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();
		textbox(template.Template_title).clear();
		logger.info("title: ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_title).sendKeys("personal");
		logger.info("title: personal ");
		wait(2);
		logger.info("wait for 2 sec");
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

		logger.info("message  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup button");
		waits(2);
		logger.info("wait for 2 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on discard  template button");
		waits(2);
		logger.info("wait for 2 sec");

		wait(10);
		logger.info("wait  10 sec");

		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		waits(5);
		logger.info("wait  10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(5);
		logger.info("wait  10 sec");

		managepersonal();
		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"'Your template personal has been deleted.", "'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_update_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		waits(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
		button(template.update_template, "login submit").click();
		logger.info("clicked on update  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(5);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_savedraft_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");
		button(repository.pin_attachment, "file attachment").click();
		wait(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft  template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(5);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_delete_confirmdelete_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.templat_personal_option, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");

		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(30);

		deleteconfirmdelete();
		wait(2);

		logger.info(template.delete_toaster.getText());
		if (template.delete_toaster.getText().equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.delete_toaster.getText(), "Your template personal has been deleted.");
			logger.info("'Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.delete_toaster.getText(), "Your template personal has been deleted.",
					"Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_navigationlink_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrumlink  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on  save button  ");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		waits(5);
		logger.info("wait  10 sec");

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(5);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_navigationlink_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrumlink  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on  discard button  ");
		waits(5);
		logger.info("wait for 10 sec");

		managepersonal();
		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_anyotherlink_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		waits(5);

		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on  save button  ");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		wait(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_anyotherlink_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on  discard button  ");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link  ");
		waits(5);
		logger.info("wait for 5 sec");

		managepersonal();
		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_closetoolspopup_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		waits(5);
		logger.info("wait for 5 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(30);

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup  ");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on  save button  ");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		waits(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(10);
		logger.info("wait  10 sec");
		deleteconfirmdelete();

		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_close_tooslpopup_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("personal");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("personal template creation checking");
		logger.info("Message: personal template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on  add template personal button");
		wait(2);
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.personal_template_toaster_message.getText());
		if (template.personal_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);

		managepersonal();

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(template.Template_message).sendKeys("personal templater added the extra test");
		logger.info("Message: personal templater added the extra test ");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.emoji_link, "login submit").click();
		logger.info("clicked on emoji link");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.emoji_link_icon, "login submit").click();
		logger.info("clicked on emoji link icon");
		wait(5);
		logger.info("wait for 5 sec");
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools pop up  ");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on  discard button  ");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link  ");
		waits(5);
		logger.info("wait for 5 sec");

		managepersonal();
		deleteconfirmdelete();
		wait(1);

		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managesharedtemplate_selectsharedopton_removetitle_clickon_Update_template_button() throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");
		logger.info("template title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("template Message: shared shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info("template shared option selected");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("template select all departments selected");
		waits(2);
		logger.info("wait for 2 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

		wait(10);

		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab  button");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on Manage shared template  button");
		waits(10);
		logger.info("wait for 10 sec");
		template.Template_title.sendKeys(Keys.CONTROL + "a");

		template.Template_title.sendKeys(Keys.DELETE);
		logger.info("template title remvoed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.update_template, "login submit").click();
		logger.info("clicked on Update template  button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText().equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(), "Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(), "Your template has been deleted.",
					"Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

		wait(10);

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on shared tab  button");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on manage shared   button");
		waits(10);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managesharedtemplate_selectsharedopton_removetitle_clickon_savedraft_template_button()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");
		logger.info("template title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("template Message: shared shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info("template shared option selected");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("template select all departments selected");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

		wait(10);

		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab  button");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on Manage shared template  button");
		waits(5);
		logger.info("wait for 10 sec");
		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);
		logger.info("template title remvoed");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft template  button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText().equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(), "Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(), "Your template has been deleted.",
					"Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

		waits(5);

		button(template.draft_tab, "login submit").click();
		logger.info("clicked on shared tab  button");
		wait(5);
		logger.info("wait for 10 sec");

		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on manage shared   button");
		waits(5);
		logger.info("wait for 10 sec");

		deleteconfirmdelete();

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managesharedtemplate_selectsharedopton_removetitle_clickon_delete_confirm_delete_template_button()
			throws Exception {

		login();
		textbox(template.Template_title).sendKeys("shared");
		logger.info("template title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared tempalte creation checking");
		logger.info("template Message: shared shared tempalte creation checking");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.template_shared_option, "login submit").click();
		logger.info("template shared option selected");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("template select all departments selected");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(1);
		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

		wait(10);

		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab  button");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on Manage shared template  button");
		waits(10);
		logger.info("wait for 10 sec");
		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);
		logger.info("template title remvoed");
		wait(2);
		logger.info("wait for 2 sec");
		deleteconfirmdelete();
		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectsharedopton_removetitl_clickonbreadscrumlink_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared template option");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments radio button");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template shared has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template shared has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on  save button  ");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(5);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		waits(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectsharedoption_removetitle_clickon_breadscrumlink_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: personal");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared option");
		wait(5);
		logger.info("wait for 2 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments radio button");
		wait(5);
		logger.info("wait for 2 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template personal has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link ");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on  discard button  ");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on Manage shared template  ");
		wait(10);
		logger.info("wait for 10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectsharedopton_removetitl_clickonnavigationlink_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(5);
		logger.info("wait for 5 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		wait(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on  save button  ");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(10);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(10);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectsharedoption_removetitle_clickon_navigationlink_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared option");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on shared option");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link ");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on  discard button  ");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on Manage shared template  ");
		waits(5);
		logger.info("wait for 10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectsharedopton_removetitl_close_tools_popup_clickon_save_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(5);
		logger.info("wait for 5 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.save_button, "login submit").click();
		logger.info("clicked on  save button  ");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template personal has been updated.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been updated.");// ScreenShot
																							// capture
		}

		wait(10);
		logger.info("wait  10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on open tools popup");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(5);
		logger.info("wait  10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on Manage button");
		waits(5);
		logger.info("wait  10 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void Managetemplate_selectsharedoption_removetitle_close_toolspopup_clickon_discard_template_button()
			throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared option");
		wait(10);
		logger.info("wait for 10 sec");

		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on shared option");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.", "Your template shared has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);

		logger.info("title  is cleared");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup ");
		waits(5);
		logger.info("wait for 10 sec");

		button(template.discard_button, "login submit").click();
		logger.info("clicked on  discard button  ");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link ");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link ");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab  ");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on Manage shared template  ");
		waits(5);
		logger.info("wait for 10 sec");
		deleteconfirmdelete();
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("'Your template shared has been deleted.'")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"'Your template shared has been deleted.'");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void clickon_sharedtab_dispalysharedlist() throws Exception {

		login();

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(5);
		logger.info("wait for 5 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			logger.info("Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

	public void clickon_sharedtab_clickon_createtemplate_button() throws Exception {

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on add template button");
		wait(10);
		logger.info("wait for 10 sec");

		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(5);
		logger.info("wait for 5 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}
	}

	public void clickon_sharedtab_clickon_createtemplate_button_title_min_3_char_validation() throws Exception {

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		wait(10);
		logger.info("wait for 10 sec");
		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on add template button");
		wait(10);
		logger.info("wait for 10 sec");

		textbox(template.Template_title).sendKeys("sh");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(5);
		logger.info("wait for 5 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		waits(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(template.Template_title_min_3_char.getText());
		if (template.Template_title_min_3_char.getText().equalsIgnoreCase("Title must be at least 3 characters.")) {
			Assert.assertEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.");
			logger.info("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.Template_title_min_3_char.getText(), "Title must be at least 3 characters.",
					"Title must be at least 3 characters.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}
	}

	public void clickon_sharedtab_dont_enter_title_enteronly_message_clickon_createbutton() throws Exception {

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(10);
		logger.info("wait for 10 sec");

		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on add template button");
		waits(10);
		logger.info("wait for 10 sec");

		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");

		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		wait(5);
		logger.info("wait for 5 sec");
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments  button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			logger.info("Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

		wait(10);
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(10);
		logger.info("wait for 10 sec");
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on manage template button");
		waits(2);
		logger.info("wait for 2 sec");

		deleteconfirmdelete();
		wait(1);

		logger.info(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			logger.info("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.", "Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}

	}

}