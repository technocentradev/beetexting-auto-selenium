package com.beetext.qa.Workflows;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Opt_out_WFCM;
import com.beetext.qa.pages.HealthScreen_pages;
import com.beetext.qa.pages.Opt_out_pages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Settings_Payment_Pages;
import com.beetext.qa.util.TestUtil;

public class Opt_Out_WF extends Opt_out_WFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	HealthScreen_pages healthScreen_pages = new HealthScreen_pages();
	Settings_Payment_Pages settings_Payment_Pages = new Settings_Payment_Pages();
	Opt_out_pages Optoutpages = new Opt_out_pages();

	public static String currentDir = System.getProperty("user.dir");

	public void opt_out_stop() throws Exception {
		auto1Login();
		autocontact();
		sendStopText();
		sendButton();
		disabledToasterMessage();
		validateSignOutLink();
		validateSignOut();

	}

	public void opt_out_start() throws Exception {

		auto1Login();
		autocontact();
		subscribeText();
		sendButton();
		enabledToasterMessage();
		validateSignOutLink();
		validateSignOut();

	}

	public void stop_Start_Msg_Block_unblock() throws Exception {

		auto1Login();
		autocontact();
		sendStopText();
		sendButton();
		disabledToasterMessage();

		subscribeText();
		sendButton();
		enabledToasterMessage();

		validateSignOutLink();
		validateSignOut();

		waits(5);

		automationLogin();
		button(Optoutpages.Automation_Contact, "click on autoContact button").click();
		waits(1);

		optMessage();
		validateSignOutLink();
		validateSignOut();

	}

	public void COntact_BLOCK_UNBLOCK_user_recived_Msg() throws Exception {
		automationLogin();
		button(Optoutpages.Automation_Contact, "click on autoContact button").click();
		waits(2);

		optMessage();
		validateSignOutLink();
		validateSignOut();
	}

	public void opt_out_MSG_BM() throws Exception {

		auto1Login();

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waits(4);
		logger.info("wait for 4 sec");

		button(repository.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(repository.MessageInput_box).click();

		button(repository.tools_BM_create_msg_optout_checkbox, "button").click();
		logger.info("click on tools_BM_create_msg_optout_checkbox button");
		waits(1);
		logger.info("wait for 5 sec");

		String text = repository.tools_BM_message.getText();
		if (repository.tools_BM_message.getText().equalsIgnoreCase(text)) {
			Assert.assertEquals(repository.tools_BM_message.getText(), text, "message is matching");
			System.out.println(repository.tools_BM_message.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.tools_BM_message.getText(), text, "message is not  matching");
			System.out.println("failed");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		System.out.println("BM allows Opt-Out");
		/*
		 * closetoolspopups(); validateSignOutLink(); validateSignOut();
		 */

	}

	public void Msgs_received_after_unblock() throws Exception {
		auto1Login();

		button(Optoutpages.autoContact, "Click on autoContact").click();
		logger.info("autoContact clicked");
		waits(1);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Stop");
		logger.info("stop");
		waits(1);
		logger.info("wait for 2 sec");

		JSClick(repository.chatInputsendButton);
		logger.info("click on send");
		waits(1);
		logger.info("wait for 5 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("hello Technocentra");
		logger.info("stop");
		waits(1);
		logger.info("wait for 2 sec");

		JSClick(repository.chatInputsendButton);
		logger.info("click on send");
		waits(1);
		logger.info("wait for 5 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Start");
		logger.info("stop");
		waits(1);
		logger.info("wait for 2 sec");

		JSClick(repository.chatInputsendButton);
		logger.info("click on send");
		waits(1);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(5);

		automationLogin();

		optMessage();

		System.out.println("Messags are allowed after Contact Unblock");
		validateSignOutLink();
		validateSignOut();
	}

	public void user_Can_Block_Contact_Another_User_Cannot_unblock_Cannot() throws Exception {
		auto1Login();

		button(Optoutpages.autoContact, "Click on autoContact").click();
		logger.info("autoContact clicked");
		waits(1);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Stop");
		logger.info("stop");
		waits(1);
		logger.info("wait for 2 sec");

		JSClick(repository.chatInputsendButton);
		logger.info("click on send");
		waits(1);
		logger.info("wait for 5 sec");

		System.out.println(repository.message.getText());

		validateSignOutLink();
		validateSignOut();

		waits(1);

		automationLogin();
		button(Optoutpages.Automation_Contact, "click on Automation_Contact button").click();
		waits(2);
		button(repository.contact_unblock, "contact_unblock").click();
		waits(1);
		logger.info("wait for 2 sec");

		// String text = healthScreen_pages.Block_UnblockTosaster.getText();
		if (healthScreen_pages.Block_UnblockTosaster.getText()
				.equalsIgnoreCase(" Contact is opted out, unblock is not allowed. ")) {
			Assert.assertEquals(healthScreen_pages.Block_UnblockTosaster.getText(),
					" Contact is opted out, unblock is not allowed. ", "message is matching");
			System.out.println(healthScreen_pages.Block_UnblockTosaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.Block_UnblockTosaster.getText(),
					" Contact is opted out, unblock is not allowed. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
		validateSignOutLink();
		validateSignOut();

	}

	public void User_Can_Unblock_Contact() throws Exception {
		auto1Login();
		autocontact();

		subscribeText();
		sendButton();

		logger.info(repository.message.getText());
		validateSignOutLink();
		validateSignOut();

	}

	public void user_receive_msg_After_Unblock() throws Exception {
		auto1Login();
		autocontact();

		sendStopText();
		sendButton();

		textbox(repository.chatInputTextareaTxt).sendKeys("hello Technocentra");
		logger.info("stop");
		waits(3);
		logger.info("wait for 2 sec");

		sendButton();

		startText();
		sendButton();

		validateSignOutLink();
		validateSignOut();

		waits(3);

		automationLogin();
		button(Optoutpages.Automation_Contact, "click on Automation_Contact button").click();
		waits(2);

		textbox(Optoutpages.message).getText();
		textbox(Optoutpages.message1).getText();
		textbox(Optoutpages.message2).getText();
		textbox(Optoutpages.message3).getText();
		validateSignOutLink();
		validateSignOut();

	}

	public void user_Can_Sendstop_BLocked_start_Unblocked() throws Exception {
		auto1Login();

		autocontact();

		sendStopText();

		sendButton();
		validateSignOutLink();
		validateSignOut();

		waits(5);

		automationLogin();
		button(Optoutpages.Automation_Contact, "click on Automation_Contact button").click();
		waits(2);

		if (verifyElementIsEnabled(repository.contact_unblock)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.contact_unblock));
			textbox(repository.contact_unblock).isEnabled();
			System.out.println(repository.contact_unblock.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.contact_unblock));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		validateSignOutLink();
		validateSignOut();
		waits(5);

		auto1Login();

		button(Optoutpages.autoContact, "Click on autoContact").click();
		logger.info("autoContact clicked");
		waits(2);
		logger.info("wait for 2 sec");

		subscribeText();

		sendButton();
		validateSignOutLink();
		validateSignOut();
	}

	public void Stop_Filter_Shows_Blocked_contact() throws Exception {
		auto1Login();

		autocontact();
		sendStopText();
		sendButton();

		validateSignOutLink();
		validateSignOut();

		waits(5);

		automationLogin();
		button(Optoutpages.Automation_Contact, "click on Automation_Contact button").click();
		waits(1);

		if (verifyElementIsEnabled(repository.contact_unblock)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.contact_unblock));
			textbox(repository.contact_unblock).isEnabled();
			System.out.println(repository.contact_unblock.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.contact_unblock));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();

		waits(2);

		auto1Login();

		autocontact();
		subscribeText();

		sendButton();

		validateSignOutLink();
		validateSignOut();

	}
}
