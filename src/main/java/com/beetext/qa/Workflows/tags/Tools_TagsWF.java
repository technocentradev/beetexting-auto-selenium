package com.beetext.qa.Workflows.tags;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Tags;
import com.beetext.qa.pages.Tags_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_TagsWF extends Tags {

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Tags_Pages tagspages = new Tags_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public static void tools_tags_editTag_deleteoption() throws Exception {

		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='delete-tag']")).click();
		waitAndLog(2);
	}

	public static void tools_tags_createtag_createbbtn() throws Exception {

		waitAndLog(5);
		driver.findElement(By.xpath("//button[@id='tag_CreateButton']")).click();
		waitAndLog(5);
	}

	public static void editTag_mnopqrTag() throws Exception {

		waitAndLog(2);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("mnopqr");
		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='mnopqrTagManage']")).click();
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_taginput)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_taginput));
			textbox(tagspages.tools_tags_createtag_taginput).isEnabled();
			logger.info("Tag Input is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_taginput));
			logger.info("Tag Input is not Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_createdisabled() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			textbox(tagspages.tools_tags_createtag_createbbtn).isEnabled();
			logger.info("Tag create button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			logger.info("Tag Create button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_taglimit_30char_errormsg() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(30);
		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("input text to search tag ");

		tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText();
		logger.info(tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText());
		if (tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText()
				.equalsIgnoreCase("Limit Tag length to 30 characters.")) {
			Assert.assertEquals(tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText(),
					"Limit Tag length to 30 characters.", "tools_tags_createtag_taglimit_30char_errormsg is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_taglimit_30char_errormsgTestCase");// ScreenShot
																										// capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText(),
					"Limit Tag length to 30 characters.",
					"tools_tags_createtag_taglimit_30char_errormsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_taglimit_30char_errormsgTestCase");// ScreenShot
																										// capture
		}
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_duplicatetag() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("input text to search tag");
		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		logger.info("click on contact");
		button(tagspages.tools_tags_createtag_select_contact1, "click on button").click();
		logger.info("click on contact");
		tools_tags_createtag_createbbtn();
		logger.info("click on create tag bttn");

		tagspages.tools_tags_createtag_alreadytag_existed.getText();
		logger.info(tagspages.tools_tags_createtag_alreadytag_existed.getText());
		if (tagspages.tools_tags_createtag_alreadytag_existed.getText()
				.equalsIgnoreCase("Tag already exists and cannot be added again.")) {
			Assert.assertEquals(tagspages.tools_tags_createtag_alreadytag_existed.getText(),
					"Tag already exists and cannot be added again.",
					"tools_tags_createtag_alreadytag_existed is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_alreadytag_existedTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createtag_alreadytag_existed.getText(),
					"Tag already exists and cannot be added again.",
					"tools_tags_createtag_alreadytag_existed is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_alreadytag_existedTestCase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_spaecialChar_allow() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).clear();
		logger.info(" Clear input text to search tag");
		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("@&*/");
		logger.info("input text to search contact");
		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		logger.info("click on contact");
		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		logger.info("click on contact ");
		tools_tags_createtag_createbbtn();
		logger.info("click on create bttn");

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			logger.info("Tag Created Successfully using Special Character");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			logger.info("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

		button(tagspages.tools_tags_Special_Char_Tagmanage, "Click on Button").click();
		logger.info("click on Special tag manage");
		waitAndLog(2);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(2);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_count() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("enter text to tag name");
		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		logger.info("click on contact");
		button(tagspages.tools_tags_createtag_select_contact1, "click on button").click();
		logger.info("click on contact");

		String tag_count = tagspages.tools_tags_createtag_displaycount.getText();
		logger.info("Contact Tag Count ----> " + tag_count);
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_applyall() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).clear();
		logger.info(" Clear input text to search tag");

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		String tagcreated = tagspages.tools_tags_createtag_taginput.getText();

		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");

		waitAndLog(2);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagcreated);
		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='" + tagcreated + "TagManage']")).click();
		waitAndLog(2);
		logger.info("click on " + tagcreated + "tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_removeall() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("enter text to tag name");
		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			textbox(tagspages.tools_tags_createtag_select_contact1).isEnabled();
			logger.info("All contacts are remove");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			logger.info("All contacts are apply");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_searchContact_displaySearchresults() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		waitAndLog(3);
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		logger.info("enter text to search contact");

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_Contactdisplay1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_Contactdisplay1));
			textbox(tagspages.tools_tags_createtag_Contactdisplay1).isEnabled();
			logger.info("Search results are Displayed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_Contactdisplay1));
			logger.info("Search results are not Displayed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_searchcontact_NoContactFound() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).clear();
		logger.info("Clear text to search contact");
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("1234567890");
		logger.info("enter text to search contact");

		tagspages.tools_tags_createtag_contact_notfound.getText();
		logger.info(tagspages.tools_tags_createtag_contact_notfound.getText());
		if (tagspages.tools_tags_createtag_contact_notfound.getText().equalsIgnoreCase("No contacts found!")) {
			Assert.assertEquals(tagspages.tools_tags_createtag_contact_notfound.getText(), "No contacts found!",
					"tools_tags_createtag_contact_notfound is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_contact_notfoundTestCase");// ScreenShot
																								// capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createtag_contact_notfound.getText(), "No contacts found!",
					"tools_tags_createtag_contact_notfound is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_contact_notfoundrexistedTestCase");// ScreenShot
																										// capture
		}
		waitAndLog(2);

	}

	public void tools_tags_createtag() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		textbox(tagspages.tools_tags_createtag_taginput).clear();
		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		waitAndLog(2);
		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='tag_CreateButton']")).click();
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			logger.info("Tag Created ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			logger.info("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_applyall_nolimit_forselection() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("enter text to create tag input");
		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		String tag_count = tagspages.tools_tags_createtag_displaycount.getText();
		logger.info("Contact Tag Count ----> " + tag_count);
		logger.info("No Limits for Selection of Contacts in contact list");
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_selectcontact_manually_automatically_applyorremove() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("enter text to create tag");
		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			textbox(tagspages.tools_tags_createtag_select_contact1).isEnabled();
			logger.info("All contacts are apply");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			logger.info("All contacts are not apply");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_clickon_createtag_searchContact_applyallcontacts() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("Enter Tag name");
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		logger.info("Enter the details to Search a Contact");
		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("Click on Apply all");

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			logger.info("All contacts are applied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			logger.info("All contacts are not applied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_clickon_createtag_searchContacts_selectContact() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("Enter tag name");
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		logger.info("Enter the Text to Search a Contact");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			logger.info("One Contact is Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			logger.info("Contact is not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_clickon_createtag_searchContacts_select_applyall() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("abc");
		logger.info("Enter the tag name");
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		logger.info("Enter the text to Search Contact");

		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on applyall");

		String tag_count = tagspages.tools_tags_createtag_displaycount.getText();
		logger.info("Contact Tag Count ----> " + tag_count);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			logger.info("All Contacts are Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			logger.info("Contact is not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_clickon_createtag_createnewtag_redirecttotagPage() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		waitAndLog(2);

		driver.findElement(By.xpath("//button[@id='tag_CreateButton']")).click();
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			logger.info("Tag Created ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			logger.info("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag));
			textbox(tagspages.tools_tags_createtag).isEnabled();
			logger.info("Create Tag is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag));
			logger.info("Create Tag is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_List_listofTags() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		tagspages.tools_tags_select_your_tag_toEdit.getText();
		logger.info(tagspages.tools_tags_select_your_tag_toEdit.getText());
		if (tagspages.tools_tags_select_your_tag_toEdit.getText()
				.equalsIgnoreCase("Select the tag you would like to edit.")) {
			Assert.assertEquals(tagspages.tools_tags_select_your_tag_toEdit.getText(),
					"Select the tag you would like to edit.", "tools_tags_select_your_tag_toEdit is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_select_your_tag_toEditTestCase");// ScreenShot
																							// capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_select_your_tag_toEdit.getText(),
					"Select the tag you would like to edit.", "tools_tags_select_your_tag_toEdit is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_select_your_tag_toEditTestCase");// ScreenShot
																							// capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_abcTagmanage)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_abcTagmanage));
			button(tagspages.tools_tags_abcTagmanage, " Button").isEnabled();
			logger.info("ABC Tag is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_abcTagmanage));
			logger.info("ABC Tag is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_25Tagmanage)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_25Tagmanage));
			button(tagspages.tools_tags_25Tagmanage, " Button").isEnabled();
			logger.info("Tag 25 is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_25Tagmanage));
			logger.info("Tag 25 is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_tags_List_clickonTag_toEdit() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_abcTagmanage, "Click on button").click();
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_editTag_deleteoption)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_editTag_deleteoption));
			button(tagspages.tools_tags_editTag_deleteoption, " Button").isEnabled();
			logger.info("Now you can Edit Tag");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_editTag_deleteoption));
			logger.info("Tag not opened");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_EditTag_Taglimit_30char() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_abcTagmanage, "Click on button").click();

		String tagname = RandomStringUtils.randomAlphabetic(30);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);

		tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText();
		logger.info(tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText());
		if (tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText()
				.equalsIgnoreCase("Tag name can be max 30 characters.")) {
			Assert.assertEquals(tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText(),
					"Tag name can be max 30 characters.", "tools_tags_createtag_taglimit_30char_errormsg is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_taglimit_30char_errormsgTestCase");// ScreenShot
																										// capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createtag_taglimit_30char_errormsg.getText(),
					"Tag name can be max 30 characters.",
					"tools_tags_createtag_taglimit_30char_errormsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_taglimit_30char_errormsgTestCase");// ScreenShot
																										// capture
		}
	}

	public void tools_tags_edittag_duplicatetag() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_abcTagmanage, "Click on button").click();
		textbox(tagspages.tools_tags_createtag_taginput).clear();
		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("dsd");
		waitAndLog(2);

		button(tagspages.tools_tags_editTag_Save, "Click on button").click();
		waitAndLog(2);

		tagspages.tools_tags_createtag_alreadytag_existed.getText();
		logger.info(tagspages.tools_tags_createtag_alreadytag_existed.getText());
		if (tagspages.tools_tags_createtag_alreadytag_existed.getText()
				.equalsIgnoreCase("Tag already exists and cannot be added again.")) {
			Assert.assertEquals(tagspages.tools_tags_createtag_alreadytag_existed.getText(),
					"Tag already exists and cannot be added again.",
					"tools_tags_createtag_alreadytag_existed is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_alreadytag_existedTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createtag_alreadytag_existed.getText(),
					"Tag already exists and cannot be added again.",
					"tools_tags_createtag_alreadytag_existed is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_alreadytag_existedTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_edittag_with_specialChar() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_abcTagmanage, "Click on button").click();
		textbox(tagspages.tools_tags_createtag_taginput).clear();
		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("&$@@");

		if (verifyElementIsEnabled(tagspages.tools_tags_editTag_Save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_editTag_Save));
			button(tagspages.tools_tags_editTag_Save, " Button").isEnabled();
			logger.info("Editing Tag with Specal Character");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_editTag_Save));
			logger.info("Special Characters are not allowed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_edittag_selectContact_ContactsCount() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_abcTagmanage, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(2);
		String tag_count = tagspages.tools_tags_createtag_displaycount.getText();
		logger.info("Contact Tag Count ----> " + tag_count);
	}

	public void tools_tags_Edittag_save_Selct_AllContacts() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		waitAndLog(3);

		button(tagspages.tools_tags_createtag_select_contact1, "click on button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			logger.info("Tag Created ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			logger.info("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			textbox(tagspages.tools_tags_createtag_select_contact2).isEnabled();
			logger.info("Contacts Updated");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			logger.info("Contacts not Updated");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_editTag_Save, "Click on button").click();
		implicitwaitAndLog(120);
		if (verifyElementIsEnabled(tagspages.tools_tags_tagedited_save_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_tagedited_save_toaster));
			textbox(tagspages.tools_tags_tagedited_save_toaster).isEnabled();
			logger.info("Tag Successfully edited ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_tagedited_save_toaster));
			logger.info("Tag not Edited");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void tools_tags_editTag_Donot_selectcontact_errormsg() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_25Tagmanage, "Click on button").click();
		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_editTag_Save, "Click on button").click();

		tagspages.tools_tags_createtag_select_contact_error.getText();
		logger.info(tagspages.tools_tags_createtag_select_contact_error.getText());
		if (tagspages.tools_tags_createtag_select_contact_error.getText()
				.equalsIgnoreCase("Please select contacts from the list to add the tag to.")) {
			Assert.assertEquals(tagspages.tools_tags_createtag_select_contact_error.getText(),
					"Please select contacts from the list to add the tag to.",
					"tools_tags_createtag_select_contact_error is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_select_contact_errorexistedTestCase");// ScreenShot
																											// capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createtag_select_contact_error.getText(),
					"Please select contacts from the list to add the tag to.",
					"tools_tags_createtag_select_contact_error is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_select_contact_errorexistedTestCase");// ScreenShot
																											// capture
		}

	}

	public void tools_tags_editTag_select_allContacts() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_25Tagmanage, "Click on button").click();
		waitAndLog(3);

		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();
		waitAndLog(3);

		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(3);

		String tag_count = tagspages.tools_tags_createtag_displaycount.getText();
		logger.info("Contact Tag Count ----> " + tag_count);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			logger.info("All Contacts are applied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			logger.info("All Contacts are removed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_editTag_select_allContacts_unselectContact_applyDisable() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(5);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(3);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			button(tagspages.tools_tags_createtag_applyall, "button").isEnabled();
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			logger.info("Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void tools_tags_editTag_select_allContacts_automatically_applyEnable() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_25Tagmanage, "Click on button").click();
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("Bee");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on button").click();
		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			textbox(tagspages.tools_tags_createtag_applyall).isEnabled();
			logger.info("Apply Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			logger.info("Apply Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_editTag_searchContact_clickonapply() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_25Tagmanage, "Click on button").click();
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("Bee");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();
		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			textbox(tagspages.tools_tags_createtag_applyall).isEnabled();
			logger.info("Apply Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			logger.info("Apply Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_editTag_searchContact_selectOneContact() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(2);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			logger.info("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			logger.info("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		logger.info("Only Contact Selected");

	}

	public void tools_tags_editTag_selectcontact_manually_automaticallyselect() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(2);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA");
		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			textbox(tagspages.tools_tags_createtag_applyall).isEnabled();
			logger.info("Apply is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void tools_tags_editTag_searchContact_noContactsFound() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_25Tagmanage, "Click on button").click();
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("11111111111111");
		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_contact_notfound)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_contact_notfound));
			textbox(tagspages.tools_tags_createtag_contact_notfound).isEnabled();
			logger.info("Contact Not Found");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_contact_notfound));
			logger.info("Contact Found");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_editTag_seletedContact_onTop() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(2);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			logger.info("Contact Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			textbox(tagspages.tools_tags_createtag_select_contact1).isEnabled();
			logger.info("Contact Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			textbox(tagspages.tools_tags_createtag_select_contact2).isEnabled();
			logger.info("Contact Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		logger.info("Selected Contacts on top");

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void tools_tags_editTag_delete_confirmDeleteText_confirmDelete() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(2);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);
		
		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		String deleteText = tagspages.tools_tags_editTag_deleteoption.getText();
		logger.info("text---->" + deleteText);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void tools_tags_list_createyour_firstTag() throws Exception {

		button(tagspages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(3);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(3);

		textbox(tagspages.user_id).sendKeys("AgentAutomation@yopmail.com");
		textbox(tagspages.Passsword_ID).sendKeys("tech1234");
		button(tagspages.loginbttn, "login submit").submit();
		link(tagspages.tools_bttn, "Click on Tools link").click();
		waitAndLog(3);

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		waitAndLog(3);

		tagspages.tools_tags_createyour_firstTag_Msg.getText();
		logger.info(tagspages.tools_tags_createyour_firstTag_Msg.getText());
		if (tagspages.tools_tags_createyour_firstTag_Msg.getText()
				.equalsIgnoreCase("Create your first tag and manage it here.")) {
			Assert.assertEquals(tagspages.tools_tags_createyour_firstTag_Msg.getText(),
					"Create your first tag and manage it here.", "tools_tags_createyour_firstTag_Msg is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createyour_firstTag_MsgTestCase");// ScreenShot
																								// capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createyour_firstTag_Msg.getText(),
					"Create your first tag and manage it here.", "tools_tags_createyour_firstTag_Msg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createyour_firstTag_MsgTestCase");// ScreenShot
																								// capture
		}
		button(tagspages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(3);
		validateSignOutLink();
		validateSignOut();
		textbox(tagspages.user_id).sendKeys(username1);
		logger.info(username1);
		textbox(tagspages.Passsword_ID).sendKeys(password);
		logger.info("password");
		button(tagspages.loginbttn, "login submit").submit();
		logger.info("click on login");
		link(tagspages.tools_bttn, "Click on Tools link").click();
		logger.info("click on tools link");
		waitAndLog(3);

	}

}