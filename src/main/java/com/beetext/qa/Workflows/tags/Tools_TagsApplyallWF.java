package com.beetext.qa.Workflows.tags;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Tags;
import com.beetext.qa.pages.Tags_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_TagsApplyallWF extends Tags {

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Tags_Pages tagspages = new Tags_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public static void tools_tags_editTag_deleteoption() throws Exception {

		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='delete-tag']")).click();
		waitAndLog(2);
	}

	public static void tools_tags_createtag_createbbtn() throws Exception {

		waitAndLog(5);
		driver.findElement(By.xpath("//button[@id='tag_CreateButton']")).click();
		waitAndLog(5);
	}

	public static void editTag_mnopqrTag() throws Exception {

		waitAndLog(2);
//		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("mnopqr");
//		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='mnopqrTagManage']")).click();
		waitAndLog(2);

	}

	public void tools_tags_clickon_createtag_applyall() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_taginput).clear();
		logger.info(" Clear input text to search tag");
		waitAndLog(2);
		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);
		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		waits(2);
		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");

		implicitwaitAndLog(120);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			logger.info("Tag Created Successfully using Special Character");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			logger.info("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(120);

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);
		logger.info("click on mnopqr tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		implicitwaitAndLog(120);

		if (verifyElementIsEnabled(driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]")))) {
			Assert.assertEquals(true, verifyElementIsEnabled(
					driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]"))));
			driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]")).isEnabled();
			logger.info("Tag has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(
					driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]"))));
			logger.info("Tag has been not deleted.");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_Edittag_save_Selct_AllContacts() throws Exception {

		waitAndLog(2);
		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(2);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		waitAndLog(3);

		button(tagspages.tools_tags_createtag_select_contact1, "click on button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();

		implicitwaitAndLog(120);
		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			logger.info("Tag Created ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			logger.info("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			textbox(tagspages.tools_tags_createtag_select_contact2).isEnabled();
			logger.info("Contacts Updated");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			logger.info("Contacts not Updated");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_editTag_Save, "Click on button").click();

		implicitwaitAndLog(120);
		if (verifyElementIsEnabled(tagspages.tools_tags_tagedited_save_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_tagedited_save_toaster));
			textbox(tagspages.tools_tags_tagedited_save_toaster).isEnabled();
			logger.info("Tag Successfully edited ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_tagedited_save_toaster));
			logger.info("Tag not Edited");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		implicitwaitAndLog(120);

		if (verifyElementIsEnabled(driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]")))) {
			Assert.assertEquals(true, verifyElementIsEnabled(
					driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]"))));
			driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]")).isEnabled();
			logger.info("Tag has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(
					driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]"))));
			logger.info("Tag has been not deleted.");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_tags_editTag_Donot_selectcontact_errormsg() throws Exception {

		waits(2);
		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_25Tagmanage, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(2);
		button(tagspages.tools_tags_createtag_applyall, "Click on button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_editTag_Save, "Click on button").click();

		tagspages.tools_tags_createtag_select_contact_error.getText();
		logger.info(tagspages.tools_tags_createtag_select_contact_error.getText());
		if (tagspages.tools_tags_createtag_select_contact_error.getText()
				.equalsIgnoreCase("Please select contacts from the list to add the tag to.")) {
			Assert.assertEquals(tagspages.tools_tags_createtag_select_contact_error.getText(),
					"Please select contacts from the list to add the tag to.",
					"tools_tags_createtag_select_contact_error is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_select_contact_errorexistedTestCase");// ScreenShot
																											// capture
		} else {
			Assert.assertNotEquals(tagspages.tools_tags_createtag_select_contact_error.getText(),
					"Please select contacts from the list to add the tag to.",
					"tools_tags_createtag_select_contact_error is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_tags_createtag_select_contact_errorexistedTestCase");// ScreenShot
																											// capture
		}

	}

	public void Tags_Apply_All() throws Exception {

		waitAndLog(2);
		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(2);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);
		logger.info("enter text to tag name");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");

		implicitwaitAndLog(120);
		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			logger.info("Tag Created ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			logger.info("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);
		logger.info("click on mnopqr tag");
		waitAndLog(5);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		implicitwaitAndLog(120);

		if (verifyElementIsEnabled(driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]")))) {
			Assert.assertEquals(true, verifyElementIsEnabled(
					driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]"))));
			driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]")).isEnabled();
			logger.info("Tag has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(
					driver.findElement(By.xpath("//div[contains(text(),'tag has been deleted.')]"))));
			logger.info("Tag has been not deleted.");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

}