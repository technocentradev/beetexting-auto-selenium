package com.beetext.qa.Workflows.tags;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Tags;
import com.beetext.qa.pages.Tags_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_TagsWF2 extends Tags {

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Tags_Pages tagspages = new Tags_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public static void editTag_mnopqrTag() throws Exception {

		waitAndLog(2);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("mnopqr");
		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='mnopqrTagManage']")).click();
		waitAndLog(2);

	}

	public static void tools_tags_editTag_deleteoption() throws Exception {

		waitAndLog(2);
		driver.findElement(By.xpath("//button[@id='delete-tag']")).click();
		waitAndLog(2);
	}

	public static void tools_tags_createtag_createbbtn() throws Exception {

		waitAndLog(5);
		driver.findElement(By.xpath("//button[@id='tag_CreateButton']")).click();
		waitAndLog(5);
	}

	public void Tags_Apply_All() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(2);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");
		waitAndLog(2);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);
		waitAndLog(5);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		logger.info("click on delete tag");
		waitAndLog(3);

	}

	public void Tags_Select_Individual_Contact() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		logger.info("Enter text to Search a Contact - QA");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		logger.info("Click on Radio Button 1 to Select Contact");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).clear();
		logger.info("Clear Radio Button 1 to Select Contact");
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("Auto");
		logger.info("Enter textto selectt Contact");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");
		waitAndLog(3);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			textbox(tagspages.tools_tags_createtag_select_contact1).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void Tags_Select_Individual_Contact_shows_Top() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).clear();
		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("Auto");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");
		waitAndLog(3);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.auto2_Contact_IN_Tags)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.auto2_Contact_IN_Tags));
			textbox(tagspages.auto2_Contact_IN_Tags).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.auto2_Contact_IN_Tags));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.QA_Contact_IN_Tags)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.QA_Contact_IN_Tags));
			textbox(tagspages.QA_Contact_IN_Tags).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.QA_Contact_IN_Tags));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			textbox(tagspages.tools_tags_createtag_select_contact1).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			textbox(tagspages.tools_tags_createtag_select_contact2).isEnabled();
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact2));
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);
	}

	public void Tags_Search_With_Text_Apply_All() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			System.out.println("Tag Created Successfully");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			System.out.println("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact4)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact4));
			textbox(tagspages.tools_tags_createtag_select_contact4).isEnabled();
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact4));
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void Tags_Search_With_Text_Apply_All_Remove_Text_Apply() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			System.out.println("Tag Created Successfully");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			System.out.println("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(5);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag_applyall, "click on button").click();
		logger.info("click on apply all");
		waitAndLog(3);

		button(tagspages.tools_tags_editTag_proceed, "Click on button").click();
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			textbox(tagspages.tools_tags_createtag_createbbtn).isEnabled();
			System.out.println("Tag create button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			System.out.println("Tag Create button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();

	}

	public void Tag_Edit_Add_Individual_Contact() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("555");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		logger.info("click on tools_tags_createtag_select_contact");
		waitAndLog(3);

		tools_tags_createtag_createbbtn();
		logger.info("click on create tag");

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			System.out.println("Tag Created Successfully");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			System.out.println("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(5);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("55555");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "click on button").click();
		logger.info("click on tools_tags_createtag_select_contact");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).clear();
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			textbox(tagspages.tools_tags_createtag_select_contact1).isEnabled();
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact1));
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void Tags_Add_Individual_Contact_applyAll_Enable() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		waitAndLog(2);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			button(tagspages.tools_tags_createtag_applyall, "button").isEnabled();
			System.out.println("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_applyall));
			System.out.println("Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		tools_tags_createtag_createbbtn();

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			textbox(tagspages.tools_tags_createtag_tagcreated_toaster).isEnabled();
			System.out.println("Tag Created Successfully");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_tagcreated_toaster));
			System.out.println("Tag not created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void Tags_Edit_remove_Individual_Contact() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		tools_tags_createtag_createbbtn();
		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		tools_tags_createtag_createbbtn();
		waitAndLog(3);
		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void Tags_ApplyAll_Edit_remove_Individual_Contact() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();

		tools_tags_createtag_createbbtn();

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			textbox(tagspages.tools_tags_createtag_createbbtn).isEnabled();
			System.out.println("Tag create button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			System.out.println("Tag Create button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void Tags_Search_ApplyAll_Edit_remove_Individual_Contact() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);
		waitAndLog(5);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		tools_tags_createtag_createbbtn();
		waitAndLog(3);
		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact1, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact2, "Click on Button").click();
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact3, "Click on Button").click();
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			textbox(tagspages.tools_tags_createtag_createbbtn).isEnabled();
			System.out.println("Tag create button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_createbbtn));
			System.out.println("Tag Create button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Tags_Search_deleteTag() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(2);

		waitAndLog(5);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(3);

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

	public void Tags_Create_Tag_Delete_in_MainPage() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

//		String tagname = RandomStringUtils.randomAlphabetic(5);
//		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys("mnopqr");
		logger.info("enter text to tag name");
		waitAndLog(2);

		waitAndLog(5);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("Automation");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(3);

		button(tagspages.tools_crossBtn, "Click on Button").click();
		waitAndLog(3);

		JSClick(tagspages.automation_chat_contact);
		waitAndLog(3);

		button(tagspages.mnopqrTag_delete_MainPage, "Click on Button").click();
		waitAndLog(3);

		button(tagspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);

		link(tagspages.tools_bttn, "Click on Tools link").click();
		logger.info("click on tools link");
		waitAndLog(3);

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		waitAndLog(2);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("mnopqr");
		waitAndLog(2);

		tagspages.NO_Tags_MainPage.getText();
		System.out.println(tagspages.NO_Tags_MainPage.getText());
		if (tagspages.NO_Tags_MainPage.getText().equalsIgnoreCase("No tags found!")) {
			Assert.assertEquals(tagspages.NO_Tags_MainPage.getText(), "No tags found!", "Message is matching");
			TestUtil.takeScreenshotAtEndOfTest("NO_Tags_MainPage");// ScreenShot capture

		} else {
			Assert.assertNotEquals(tagspages.NO_Tags_MainPage.getText(), "No tags found!", "Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("NO_Tags_MainPage");// ScreenShot capture
		}

	}

	public void Tags_Selected_Contact_Shows_Top() throws Exception {

		button(tagspages.tools_tags_bttn, "Click on tools_tags_bttn").click();
		logger.info("click on tags");
		waitAndLog(3);

		button(tagspages.tools_tags_createtag, "Click on tools_tags_createtag Button").click();
		logger.info("click on create tag");
		waitAndLog(3);

		String tagname = RandomStringUtils.randomAlphabetic(5);
		waitAndLog(1);

		textbox(tagspages.tools_tags_createtag_taginput).sendKeys(tagname);
		logger.info("enter text to tag name");
		waitAndLog(5);

		textbox(tagspages.tools_tags_createtag_search_contact).sendKeys("QA10");
		waitAndLog(2);

		button(tagspages.tools_tags_createtag_select_contact, "Click on Button").click();
		waitAndLog(2);

		tools_tags_createtag_createbbtn();
		waitAndLog(3);

		waitAndLog(5);
		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys(tagname);
		waitAndLog(2);
		driver.findElement(
				By.xpath("(//button[@class='btn btn-badge-sm btn-badge-close d-flex align-items-center'])[1]")).click();
		waitAndLog(2);
		logger.info("click on " + tagname + "tag");
		waitAndLog(3);

		if (verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			textbox(tagspages.tools_tags_createtag_select_contact).isEnabled();
			System.out.println("Contact Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tagspages.tools_tags_createtag_select_contact));
			System.out.println("Contact not Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

		tools_tags_editTag_deleteoption();
		waitAndLog(3);

	}

}