package com.beetext.qa.Workflows.automations;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.AutomatiosWFCM;
import com.beetext.qa.pages.Automations_pages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class Automations_WF extends AutomatiosWFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Automations_pages automationspages = new Automations_pages();
	public static String currentDir = System.getProperty("user.dir");

	public void NO_Automations_Created() throws Exception {

		waits(2);

		click_on_Tools_CrossButton();

		validateSignOutLink();
		validateSignOut();

		waits(2);
		automations_block_for_DefaultMessages();
		waits(2);

		if (automationspages.No_Automations_Created.getText()
				.equalsIgnoreCase("No automations have been created for this number.")) {
			Assert.assertEquals(automationspages.No_Automations_Created.getText(),
					"No automations have been created for this number.", "message is matching");
			logger.info(automationspages.No_Automations_Created.getText());
			TestUtil.takeScreenshotAtEndOfTest("No_Automations_Created");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.No_Automations_Created.getText(),
					"No automations have been created for this number.", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("No_Automations_Created");// ScreenShot capture
		}
		waits(2);
		click_on_Tools_CrossButton();
		validateSignOutLink();
		validateSignOut();

		waits(2);
		Automations_autologin();

	}

	public void automations_Dept() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.department, "department").click();
		logger.info("department");

		button(automationspages.department_Auto, "department_Auto").click();
		logger.info("department_Auto");

		logger.info(automationspages.department_Auto.getText());

		button(automationspages.department, "department").click();
		logger.info("department");

		button(automationspages.department_auto2, "department_auto2").click();
		logger.info("department_auto2");

		logger.info(automationspages.department_auto2.getText());

		button(automationspages.department, "department").click();
		logger.info("department");

		button(automationspages.department_Auto, "department_Auto").click();
		logger.info("department_Auto");
	}

	public void create_automations_Open_with_AllFields() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		if (verifyElementIsEnabled(automationspages.add_Condition)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.add_Condition));
			textbox(automationspages.add_Condition).isEnabled();
			logger.info("Add Condition is Enabled");
			logger.info(automationspages.add_Condition.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.add_Condition));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(automationspages.add_Action)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.add_Action));
			textbox(automationspages.add_Action).isEnabled();
			logger.info(automationspages.add_Action.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.add_Action));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(automationspages.Create)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Create));
			textbox(automationspages.Create).isEnabled();
			logger.info(automationspages.Create.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Create));
			logger.info(automationspages.Create.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void create_automations_title_must() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		textbox(automationspages.title).sendKeys("abdc");
		logger.info("enter title name");

		textbox(automationspages.title).clear();
		logger.info("enter title name");

		button(automationspages.add_Condition, "add condition").click();
		logger.info("click on add condition");
		waits(2);

		logger.info("Please enter a title for this auto response.");

	}

	public void create_automations_title_3Char_error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		textbox(automationspages.title).sendKeys("a");
		logger.info("enter title name");

		if (automationspages.title_3Char.getText().equalsIgnoreCase("Title must be at least 3 characters.")) {
			Assert.assertEquals(automationspages.title_3Char.getText(), "Title must be at least 3 characters.",
					"message is matching");
			logger.info(automationspages.title_3Char.getText());
			TestUtil.takeScreenshotAtEndOfTest("title_3Char");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.title_3Char.getText(), "Title must be at least 3 characters.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("title_3Char");// ScreenShot capture
		}
	}

	public void create_automations_title_already_exists() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		textbox(automationspages.title).sendKeys("abc");
		logger.info("enter title name");

		textbox(automationspages.condition_Keyword).sendKeys("1234");
		logger.info("enter condition_Keyword");

		textbox(automationspages.Reply_message).sendKeys("abcdq");
		logger.info("enter Reply_message");

		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		if (automationspages.title_already_exists.getText().equalsIgnoreCase("Title exists already.")) {
			Assert.assertEquals(automationspages.title_already_exists.getText(), "Title exists already.",
					"message is matching");
			logger.info(automationspages.title_already_exists.getText());
			TestUtil.takeScreenshotAtEndOfTest("title_already_exists");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.title_already_exists.getText(), "Title exists already.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("title_already_exists");// ScreenShot capture
		}
	}

	public void Default_Dept() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.department, "department").click();
		logger.info("department");

		button(automationspages.department_Auto, "department_Auto").click();
		logger.info("department_Auto");

		logger.info(automationspages.department_Auto.getText());

		logger.info("It Shows default Department as Auto");

	}

	public void Selecting_Another_Dept() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.department, "department").click();
		logger.info("department");

		button(automationspages.department_auto2, "department_auto2").click();
		logger.info("department_auto2");

		logger.info(automationspages.department_auto2.getText());

	}

	public void manage_Case_Dept_Disabled() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		if (verifyElementIsEnabled(automationspages.department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.department));
			textbox(automationspages.department).isEnabled();
			logger.info("Department is Enabled");
			logger.info(automationspages.department.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.department));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void create_automations_with_Msg_received_Contains_pharse() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.exactlyMatches_Contains_DropDown, "exactlyMatches_Contains_DropDown").click();
		logger.info("click on exactlyMatches_Contains_DropDown");

		button(automationspages.Contains_DropDown, "Contains_DropDown").click();
		logger.info("click on Contains_DropDown");

		textbox(automationspages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		Create_Automations_Contains_Phrase();

	}

	public void create_automations_with_Msg_received_Contains_pharse_CaseSensitive() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.exactlyMatches_Contains_DropDown, "exactlyMatches_Contains_DropDown").click();
		logger.info("click on exactlyMatches_Contains_DropDown");

		button(automationspages.Contains_DropDown, "Contains_DropDown").click();
		logger.info("click on Contains_DropDown");

		textbox(automationspages.condition_Keyword).sendKeys("Hello");
		logger.info("enter condition_Keyword");

		button(automationspages.Case_Sensitive, " click on Case Sensitive").click();
		logger.info("Click on Case_Sensitive");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		Create_Automations_Contains_Phrase();

	}

	public void create_automations_with_Msg_received_Exactly_pharse() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(automationspages.condition_Keyword).sendKeys("Hello");
		logger.info("enter condition_Keyword");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		Create_Automations_Exactly_Phrase();

	}

	public void create_automations_with_Msg_received_Exactly_pharse_CaseSensitive() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(automationspages.condition_Keyword).sendKeys("Hello");
		logger.info("enter condition_Keyword");

		button(automationspages.Case_Sensitive, " click on Case Sensitive").click();
		logger.info("Click on Case_Sensitive");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		Create_Automations_Exactly_Phrase();

	}

	public void create_automations_Search_term_required_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		if (automationspages.Search_Term_required.getText().equalsIgnoreCase("Search term is required.")) {
			Assert.assertEquals(automationspages.Search_Term_required.getText(), "Search term is required.",
					"message is matching");
			logger.info(automationspages.Search_Term_required.getText());
			TestUtil.takeScreenshotAtEndOfTest("Search_Term_required");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Search_Term_required.getText(), "Search term is required.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Search_Term_required");// ScreenShot capture
		}

	}

	public void create_automations_withIn_Create() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		button(automationspages.End_Time, "End_Time").sendKeys("11");
		logger.info("click on End_Time");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Send_Immediately, "Action_Send_Immediately").click();
		logger.info("click on Action_Send_Immediately");
		waits(2);

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		delete_Automations_manage();
	}

	public void create_automations_OutsideOf_Create() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.within_outsideOf_DropDown, "within_outsideOf_DropDown").click();
		logger.info("click on within_outsideOf_DropDown");

		button(automationspages.outsideOf_DropDown, "outsideOf_DropDown").click();
		logger.info("click on outsideOf_DropDown");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		button(automationspages.End_Time, "End_Time").sendKeys("11");
		logger.info("click on End_Time");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Send_Immediately, "Action_Send_Immediately").click();
		logger.info("click on Action_Send_Immediately");
		waits(2);

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		delete_Automations();
	}

	public void create_automations_Weekday_required_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		if (automationspages.weekday_required.getText().equalsIgnoreCase("Weekday is required.")) {
			Assert.assertEquals(automationspages.weekday_required.getText(), "Weekday is required.",
					"message is matching");
			logger.info(automationspages.weekday_required.getText());
			TestUtil.takeScreenshotAtEndOfTest("weekday_required");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.weekday_required.getText(), "Weekday is required.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("weekday_required");// ScreenShot capture
		}

	}

	public void create_automations_select_All_Days() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		button(automationspages.End_Time, "End_Time").sendKeys("11");
		logger.info("click on End_Time");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}

		logger.info("It allows multiple weekday(s) selection");
		waits(2);
		delete_Automations();

	}

	public void create_automations_Deselect_All_Days() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		button(automationspages.End_Time, "End_Time").sendKeys("11");
		logger.info("click on End_Time");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}

		logger.info("It allows De-Selecting multiple weekday(s)");
		waits(2);
		delete_Automations();

	}

	public void create_automations_Start_Item_Should_be_LessThan_EndTime() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.EndTime_Err.getText().equalsIgnoreCase("Start time should be less than end time.")) {
			Assert.assertEquals(automationspages.EndTime_Err.getText(), "Start time should be less than end time.",
					"message is matching");
			logger.info(automationspages.EndTime_Err.getText());
			TestUtil.takeScreenshotAtEndOfTest("EndTime_Err");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.EndTime_Err.getText(), "Start time should be less than end time.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("EndTime_Err");// ScreenShot capture
		}

	}

	public void create_automations_Tag_Added_to_Contact() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		delete_Automations();

	}

	public void Automations_Manage_Tag_Added_to_Contact() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.manage, "manage").click();
		logger.info("click on manage");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		logger.info(automationspages.Tag_Added_to_Contact.getText());

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");

	}

	public void create_automations_Tag_Added_to_Contact_Immediately_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}

		waits(2);
		Automations_From_tools_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Tag_Added_to_Contact_Delayed_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Delayed, "Action_Add_Tag_Contact_Delayed").click();
		logger.info("click on Action_Add_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_Delayed_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_RemoveTag_to_Contact_Immediately_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Immediately, "Action_Remove_Tag_Contact_Immediately").click();
		logger.info("click on Action_Remove_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_Immediately_RemoveTag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_RemoveTag_to_Contact_Delayed_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Delayed, "Action_Remove_Tag_Contact_Delayed").click();
		logger.info("click on Action_Remove_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abcde");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_Delayed_RemoveTag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void Automations_ON_OFF() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.ON_OFF_1, "ON_OFF_1").click();
		logger.info("click on ON_OFF_1");

		if (verifyElementIsEnabled(automationspages.ON_OFF_1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.ON_OFF_1));
			textbox(automationspages.ON_OFF_1).isEnabled();
			logger.info("Automations Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.ON_OFF_1));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.ON_OFF_1, "ON_OFF_1").click();
		logger.info("click on ON_OFF_1");

		if (verifyElementIsEnabled(automationspages.ON_OFF_1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.ON_OFF_1));
			textbox(automationspages.ON_OFF_1).isEnabled();
			logger.info("Automations Diabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.add_Condition));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Automations_Condition_For_Tag_toBe_Added_Contains() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.manage, "manage").click();
		logger.info("click on manage");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		logger.info(automationspages.Tag_Added_to_Contact.getText());

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");

		button(automationspages.exactlyMatches_Contains_DropDown, "exactlyMatches_Contains_DropDown").click();
		logger.info("click on exactlyMatches_Contains_DropDown");

		logger.info(automationspages.Contains_DropDown.getText());

		button(automationspages.Contains_DropDown, "Contains_DropDown").click();
		logger.info("click on Contains_DropDown");

	}

	public void Automations_IF_Condition_Close_Show_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Condition_Close, "Condition_Close").click();
		logger.info("click on Condition_Close");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		if (automationspages.Condition_required.getText().equalsIgnoreCase("A condition is required")) {
			Assert.assertEquals(automationspages.Condition_required.getText(), "A condition is required",
					"message is matching");
			logger.info(automationspages.Condition_required.getText());
			TestUtil.takeScreenshotAtEndOfTest("A condition is required");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Condition_required.getText(), "A condition is required",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Search_Term_required");// ScreenShot capture
		}

	}

	public void Automations_Manage_Existing_Condition_Shown() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.manage, "manage").click();
		logger.info("click on manage");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		logger.info(automationspages.Message_Received.getText());

		logger.info(automationspages.Time_is.getText());

		logger.info(automationspages.Time_is.getText());

	}

	public void Automations_Tag_List_Shown() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.manage, "manage").click();
		logger.info("click on manage");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		String searchtag = repository.tools_BM_create_SearchTag1.getText();
		logger.info("Search Tag name--->" + searchtag);

		String searchtag1 = repository.tools_BM_create_SearchTag2.getText();
		logger.info("Search Tag name--->" + searchtag1);

		String searchtag2 = repository.tools_BM_create_SearchTag3.getText();
		logger.info("Search Tag name--->" + searchtag2);

	}

	public void Automations_oneTag_Allowed_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.manage, "manage").click();
		logger.info("click on manage");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		if (automationspages.one_Tag_Allowed_Error.getText().equalsIgnoreCase("Only one Tag is allowed.")) {
			Assert.assertEquals(automationspages.one_Tag_Allowed_Error.getText(), "Only one Tag is allowed.",
					"message is matching");
			logger.info(automationspages.one_Tag_Allowed_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("one_Tag_Allowed_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.one_Tag_Allowed_Error.getText(), "Only one Tag is allowed.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("one_Tag_Allowed_Error");// ScreenShot capture
		}

	}

	public void Automations_IF_Action_Close_Show_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.manage, "manage").click();
		logger.info("click on manage");

		button(automationspages.Action_Close, "Action_Close").click();
		logger.info("click on Action_Close");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		if (automationspages.Action_required.getText().equalsIgnoreCase("An action is required")) {
			Assert.assertEquals(automationspages.Action_required.getText(), "An action is required",
					"message is matching");
			logger.info(automationspages.Action_required.getText());
			TestUtil.takeScreenshotAtEndOfTest("An action is required");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Action_required.getText(), "An action is required",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Search_Term_required");// ScreenShot capture
		}

	}

	public void Automations_Multiple_Tags_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abcd");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abcde");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		logger.info("In Action Condition we can add mutliple Tags");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		delete_Automations();

	}

	public void Automations_Action_Tag_Already_Added_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");

		if (automationspages.Action_Tag_Already_Added_Error.getText()
				.equalsIgnoreCase("Tag already exists and cannot be added again.")) {
			Assert.assertEquals(automationspages.Action_Tag_Already_Added_Error.getText(),
					"Tag already exists and cannot be added again.", "message is matching");
			logger.info(automationspages.Action_Tag_Already_Added_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Action_Tag_Already_Added_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Action_Tag_Already_Added_Error.getText(),
					"Tag already exists and cannot be added again.", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Action_Tag_Already_Added_Error");// ScreenShot capture
		}

	}

	public void Automations_Action_Tag_List_Shown() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.manage, "manage").click();
		logger.info("click on manage");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		String searchtag = repository.tools_BM_create_SearchTag1.getText();
		logger.info("Search Tag name--->" + searchtag);

		String searchtag1 = repository.tools_BM_create_SearchTag2.getText();
		logger.info("Search Tag name--->" + searchtag1);

		String searchtag2 = repository.tools_BM_create_SearchTag3.getText();
		logger.info("Search Tag name--->" + searchtag2);

	}

	public void Automations_Tag_Required_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Action_Tag_Required_Error.getText().equalsIgnoreCase("Tag is required.")) {
			Assert.assertEquals(automationspages.Action_Tag_Required_Error.getText(), "Tag is required.",
					"message is matching");
			logger.info(automationspages.Action_Tag_Required_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Action_Tag_Required_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Action_Tag_Required_Error.getText(), "Tag is required.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Action_Tag_Required_Error");// ScreenShot capture
		}

	}

	public void create_automations_Tag_Added_to_Contact_Immediately_Action_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Tag_Added_to_Contact_Delayed_Action_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Delayed, "Action_Add_Tag_Contact_Delayed").click();
		logger.info("click on Action_Add_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_Delayed_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_for_Delay_Response_Shows_Min_Hr_Days() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");
		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");
		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);
		waits(2);
		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");
		waits(2);
		button(automationspages.Action_Add_Tag_Contact_Delayed, "Action_Add_Tag_Contact_Delayed").click();
		logger.info("click on Action_Add_Tag_Contact_Delayed");
		waits(2);

		if (verifyElementIsEnabled(automationspages.Input_Delay)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Input_Delay));
			textbox(automationspages.Input_Delay).isEnabled();
			logger.info("Add Condition is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Input_Delay));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.Delay_Unit_Min_Hr_Days, "Delay_Unit_Min_Hr_Days").click();
		logger.info("click on Delay_Unit_Min_Hr_Days");

		if (verifyElementIsEnabled(automationspages.Delay_Unit_Minutes)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Delay_Unit_Minutes));
			textbox(automationspages.Delay_Unit_Minutes).isEnabled();
			logger.info("Delay_Unit_Minutes is Enabled");
			logger.info(automationspages.Delay_Unit_Minutes.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Delay_Unit_Minutes));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(automationspages.Delay_Unit_Hours)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Delay_Unit_Hours));
			textbox(automationspages.Delay_Unit_Hours).isEnabled();
			logger.info("Delay_Unit_Hours is Enabled");
			logger.info(automationspages.Delay_Unit_Hours.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Delay_Unit_Hours));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(automationspages.Delay_Unit_Days)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Delay_Unit_Days));
			textbox(automationspages.Delay_Unit_Days).isEnabled();
			logger.info("Delay_Unit_Days is Enabled");
			logger.info(automationspages.Delay_Unit_Days.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Delay_Unit_Days));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void create_automations_If_we_Give_100_Delay_shows_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Delayed, "Action_Add_Tag_Contact_Delayed").click();
		logger.info("click on Action_Add_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("105");
		logger.info("Input Delay - 01");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Delay_value_cannot_exceed_100.getText()
				.equalsIgnoreCase("Delay value cannot exceed 100.")) {
			Assert.assertEquals(automationspages.Delay_value_cannot_exceed_100.getText(),
					"Delay value cannot exceed 100.", "message is matching");
			logger.info(automationspages.Delay_value_cannot_exceed_100.getText());
			TestUtil.takeScreenshotAtEndOfTest("Delay_value_cannot_exceed_100");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Delay_value_cannot_exceed_100.getText(),
					"Delay value cannot exceed 100.", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Delay_value_cannot_exceed_100");// ScreenShot capture
		}
	}

	public void create_automations_RemoveTag_to_Contact_Immediately_Action_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		Automations_Tag_Add_to_Remove();

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Immediately, "Action_Remove_Tag_Contact_Immediately").click();
		logger.info("click on Action_Remove_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_Immediately_RemoveTag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_RemoveTag_to_Contact_Delayed_Action_Respective_Action() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		Automations_Tag_Add_to_Remove();

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Delayed, "Action_Remove_Tag_Contact_Delayed").click();
		logger.info("click on Action_Remove_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abcde");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_Delayed_RemoveTag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Delayed_Msg_Cancel_If_Contact_Replies() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Send_Delayed_Reply, "Action_Send_Delayed_Reply").click();
		logger.info("click on Action_Send_Delayed_Reply");
		waits(2);

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Delayed_Msg_Cancel_If_Contact_Replies.getText()
				.equalsIgnoreCase("*Any delayed messages will be canceled if the contact replies.")) {
			Assert.assertEquals(automationspages.Delayed_Msg_Cancel_If_Contact_Replies.getText(),
					"*Any delayed messages will be canceled if the contact replies.", "message is matching");
			logger.info(automationspages.Delayed_Msg_Cancel_If_Contact_Replies.getText());
			TestUtil.takeScreenshotAtEndOfTest("Delayed_Msg_Cancel_If_Contact_Replies");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Delayed_Msg_Cancel_If_Contact_Replies.getText(),
					"*Any delayed messages will be canceled if the contact replies.", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Delayed_Msg_Cancel_If_Contact_Replies");// ScreenShot capture
		}
	}

	public void create_automations_Tag_Added_to_Contact_Delayed_response_With_Minute() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Delayed, "Action_Add_Tag_Contact_Delayed").click();
		logger.info("click on Action_Add_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_Delayed_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Message_Tag_Added_to_Contact_Delayed() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(automationspages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Delayed, "Action_Add_Tag_Contact_Delayed").click();
		logger.info("click on Action_Add_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");
		waits(2);
		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		Automations_MSG_Tag_Delayed_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Message_Remove_Tag_Delayed() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");
		validateSignOutLink();
		validateSignOut();
		Automations_Tag_Add_to_Remove_username2();

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(automationspages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Delayed, "Action_Remove_Tag_Contact_Delayed").click();
		logger.info("click on Action_Remove_Tag_Contact_Delayed");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");

		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		Automations_MSG_Remove_Tag_Delayed_tag_close();
		waits(2);
		delete_Automations_From_tools();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");
		validateSignOutLink();
		validateSignOut();
		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);
		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(2);

	}

	public void create_automations_Tag_Added_to_Contact_Multiple_Actions() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		Automations_Tag_Add_to_Remove();

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");
		waits(2);
		button(automationspages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		waits(2);
		button(automationspages.Action_met_UP_Condition1, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Immediately1, "Action_Remove_Tag_Contact_Immediately")
				.click();
		logger.info("click on Action_Remove_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action1).sendKeys("abcde");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Multiple_Actions() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		Automations_Tag_Add_to_Remove();

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");
		waits(2);
		button(automationspages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		waits(2);
		button(automationspages.Action_met_UP_Condition1, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Immediately1, "Action_Remove_Tag_Contact_Immediately")
				.click();
		logger.info("click on Action_Remove_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action1).sendKeys("abcde");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Validate_Case_With_Multiple_Actions() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");
		Automations_Tag_Add_to_Remove();

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Tag_Added_to_Contact, "Tag_Added_to_Contact").click();
		logger.info("click on Tag_Added_to_Contact");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");
		waits(2);
		button(automationspages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		waits(2);
		button(automationspages.Action_met_UP_Condition1, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Remove_Tag_Contact_Immediately1, "Action_Remove_Tag_Contact_Immediately")
				.click();
		logger.info("click on Action_Remove_Tag_Contact_Immediately");
		waits(2);

		textbox(automationspages.input_To_searchTags_Action1).sendKeys("abcde");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");

		if (automationspages.Toaster.getText().equalsIgnoreCase(" Your Auto Response has been successfully added. ")) {
			Assert.assertEquals(automationspages.Toaster.getText(), " Your Auto Response has been successfully added. ",
					"message is matching");
			logger.info(automationspages.Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.Toaster.getText(),
					" Your Auto Response has been successfully added. ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toaster");// ScreenShot capture
		}
		waits(2);
		Automations_From_tools_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_with_Msg_received_Immediate_Response() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.exactlyMatches_Contains_DropDown, "exactlyMatches_Contains_DropDown").click();
		logger.info("click on exactlyMatches_Contains_DropDown");

		button(automationspages.Contains_DropDown, "Contains_DropDown").click();
		logger.info("click on Contains_DropDown");

		textbox(automationspages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		Create_Automations_Contains_Phrase();

	}

	public void create_automations_with_Msg_received_Delayed_Response() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.exactlyMatches_Contains_DropDown, "exactlyMatches_Contains_DropDown").click();
		logger.info("click on exactlyMatches_Contains_DropDown");

		button(automationspages.Contains_DropDown, "Contains_DropDown").click();
		logger.info("click on Contains_DropDown");

		textbox(automationspages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Send_Delayed_Reply, "Action_Send_Delayed_Reply").click();
		logger.info("click on Action_Send_Delayed_Reply");
		waits(2);

		textbox(automationspages.Input_Delay).clear();
		logger.info("Input Delay Clear");
		textbox(automationspages.Input_Delay).sendKeys("01");
		logger.info("Input Delay - 01");
		waits(2);

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		Create_Automations_delay_Response();
		waits(2);

	}

	public void create_automations_Message_Tag_Added_to_Contact_Immediate() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(automationspages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		Automations_MSG_Tag_Immediately_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_Message_Tag_Remove_to_Contact_Immediate() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");
		Automations_Tag_Add_to_Remove();

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(automationspages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		Automations_MSG_RemoveTag_Immediately_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void create_automations_withIn_MSG_Immediate() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("tech1234");
		waitAndLog(2);
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		button(automationspages.End_Time, "End_Time").sendKeys("11");
		logger.info("click on End_Time");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Send_Immediately, "Action_Send_Immediately").click();
		logger.info("click on Action_Send_Immediately");
		waits(2);

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		Create_Automations_MSG_username1();
		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();
		Automations_autologin();
	}

	public void create_automations_withIn_Tag_Add_to_Contact_Immediate() throws Exception {

		waits(2);
		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		button(automationspages.End_Time, "End_Time").sendKeys("11");
		logger.info("click on End_Time");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		Automations_MSG_Tag_Immediately_tag_close();
		waits(2);
		delete_Automations_From_tools();
	}

	public void create_automations_withIn_Tag_Remove_to_Contact_Immediate() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);
		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
		waits(2);
		textbox(automationspages.Conversation_TagInput).sendKeys("abcde");
		textbox(automationspages.Conversation_TagInput).sendKeys(Keys.ENTER);
		logger.info("Enter data in search tags : a ");
		waits(2);

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		button(automationspages.Monday, "Monday").click();
		logger.info("click on Monday");

		button(automationspages.Tuesday, "Tuesday").click();
		logger.info("click on Tuesday");

		button(automationspages.Wednesday, "Wednesday").click();
		logger.info("click on Wednesday");

		button(automationspages.Thursday, "Thursday").click();
		logger.info("click on Thursday");

		button(automationspages.Friday, "Friday").click();
		logger.info("click on Friday");

		button(automationspages.Saturday, "Saturday").click();
		logger.info("click on Saturday");

		button(automationspages.Sunday, "Sunday").click();
		logger.info("click on Sunday");

		button(automationspages.End_Time, "End_Time").sendKeys("11");
		logger.info("click on End_Time");

		button(automationspages.Action_met_UP_Condition, "Action_met_UP_Condition").click();
		logger.info("click on Action_met_UP_Condition");

		button(automationspages.Action_Add_Tag_Contact_Immediately, "Action_Add_Tag_Contact_Immediately").click();
		logger.info("click on Action_Add_Tag_Contact_Immediately");
		waits(2);

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("abc");
		logger.info("Enter data in search tags ");
		waits(2);

		button(repository.tools_BM_create_SearchTag1, "SearchTag1").click();
		logger.info("click on SearchTag1");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);
		Automations_MSG_RemoveTag_Immediately_tag_close();
		waits(2);
		delete_Automations_From_tools();

	}

	public void WithIn_Weekday_required_Error() throws Exception {

		waits(2);
		toolsAutomations_Button();
		waits(2);

		button(automationspages.create_Automations, "create_Automations").click();
		logger.info("click on create_Automations");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(automationspages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		button(automationspages.Msg_Received_Tag_Time_IS_DropDown, "Msg_Received_Tag_Time_IS_DropDown").click();
		logger.info("click on Msg_Received_Tag_Time_IS_DropDown");

		button(automationspages.Time_is, "Time_is").click();
		logger.info("click on Time_is");

		textbox(automationspages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(automationspages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(automationspages.Create, "Create").click();
		logger.info("click on Create");
		waits(2);

		if (automationspages.weekday_required.getText().equalsIgnoreCase("Weekday is required.")) {
			Assert.assertEquals(automationspages.weekday_required.getText(), "Weekday is required.",
					"message is matching");
			logger.info(automationspages.weekday_required.getText());
			TestUtil.takeScreenshotAtEndOfTest("weekday_required");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.weekday_required.getText(), "Weekday is required.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("weekday_required");// ScreenShot capture
		}

	}

}
