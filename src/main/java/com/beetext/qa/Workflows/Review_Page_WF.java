package com.beetext.qa.Workflows;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Reviews_Workflow_CM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Reviews_pages;
import com.beetext.qa.util.TestUtil;

public class Review_Page_WF extends Reviews_Workflow_CM {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Reviews_pages reviews = new Reviews_pages();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	// Reviews Page starts

	// Review Tab availability method

	public void tc_215_Review_Tab_IsEnable_215() throws Exception {

		waitAndLog(2);
		button(reviews.reviews_Tab, "Verify Review tab").isEnabled();
		waitAndLog(2);

		if (verifyElementIsEnabled(reviews.reviews_Tab)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.reviews_Tab));
			button(reviews.reviews_Tab, "Tab").isEnabled();
			logger.info("Reviews Tab is  enabled");

			TestUtil.takeScreenshotAtEndOfTest("Reviews Tab is  enabled case working fine");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.reviews_Tab));
			logger.info("Reviews Tab  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("Reviews Tab disabled case is not working fine");// ScreenShot capture
		}

		logger.info("Verified availability of Review Tab");
	}

	public void Review_NoRecords_CreateYourFirstName_Text_Message_216() throws Exception {

		Click_on_Reviews();
		reviews.Reviewes_NoRecords_TextMessage.getText();

		logger.info(reviews.Reviewes_NoRecords_TextMessage.getText());

		if (reviews.Reviewes_NoRecords_TextMessage.getText().equalsIgnoreCase("Create your first review.")) {
			Assert.assertEquals(reviews.Reviewes_NoRecords_TextMessage.getText(), "Create your first review.",
					"Create your first review.");
			logger.info("Reviewes page Create your first review text message testcase is working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"Reviewes page Create your first review. text message  verifying case is passed");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(reviews.Reviewes_NoRecords_TextMessage.getText(), " Create your first review.",
					"Create your first review.");
			logger.info("Reviewes page Create your first review text message testcase is not working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"Reviewes page Create your first review. text message  verifying case is failed ");// ScreenShot
																										// capture
		}

		logger.info("verified text message for no records");
	}

	// Add Review Site Button

	public void Review_AddReviewSite_button_IsEnabled_217() throws Exception {

		Click_on_Reviews();

		reviews.Review_AddReviewSite_button.getText();
		logger.info(reviews.Review_AddReviewSite_button.getText());
		waitAndLog(2);

		button(reviews.Review_AddReviewSite_button, "Verify Add Review Site button").isEnabled();
		waitAndLog(2);

		if (verifyElementIsEnabled(reviews.Review_AddReviewSite_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Review_AddReviewSite_button));
			button(reviews.Review_AddReviewSite_button, "Button").isEnabled();
			logger.info("Add Review Site button is  enabled");

			TestUtil.takeScreenshotAtEndOfTest("Add Review Site button is  enabled  case is working fine");// ScreenShot
																											// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Review_AddReviewSite_button));
			logger.info("Add Review Site button is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("Add Review Site button disabled case is not working fine");// ScreenShot
																											// capture
		}

		logger.info("Verified availabiity of Add Review site button");
	}

	public void tc_218_Reviewes_Create_Review_Page_Navigation_218() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();

		reviews.Create_Review.getText();
		logger.info(reviews.Create_Review.getText());

		if (reviews.Create_Review.getText().equalsIgnoreCase("Create Rreview")) {
			Assert.assertEquals(reviews.Create_Review.getText(), "Create Rreview", "Create Rreview");
			logger.info("Create Rreview page navigation testcase is working fine");
			TestUtil.takeScreenshotAtEndOfTest("Create Rreview page navigation case is passed");// ScreenShot capture

		} else {
			Assert.assertNotEquals(reviews.Create_Review.getText(), " Create Rreview", "Create Rreview");
			logger.info("Create Rreview page navigation  testcase is not working fine");
			TestUtil.takeScreenshotAtEndOfTest("Create Rreview page navigation case is failed ");// ScreenShot capture
		}

		logger.info("Verified the navigation to Create review page");
	}

	public void tc_219_Reviewes_Create_Review_Page_All_Fields_isEnabled_219() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();

		reviews.Create_Review_Select_Site_DD.click();

		if (verifyElementIsEnabled(reviews.Create_Review_Select_Site_DD)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Create_Review_Select_Site_DD));
			button(reviews.Create_Review_Select_Site_DD, "DD").isEnabled();
			logger.info("Review Select Site DD is  enabled");

			TestUtil.takeScreenshotAtEndOfTest(" Review Select Site DD is  enabled  case working fine");// ScreenShot
																										// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Create_Review_Select_Site_DD));
			logger.info("Review Select Site DD is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("Review Select Site DD disabled case is working fine");// ScreenShot
																										// capture
		}
		logger.info("Verified Site DD field");

		waitAndLog(2);

		if (verifyElementIsEnabled(reviews.Create_Review_Link_textbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Create_Review_Link_textbox));
			textbutton(reviews.Create_Review_Link_textbox, "textbox").isEnabled();
			logger.info("Create Review Link textbox is  enabled");
		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Create_Review_Link_textbox));
			logger.info("Create Review Link textbox is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("Create Review Link textbox disabled case is working fine");// ScreenShot
																											// capture
		}

		logger.info("Verified Link text field");
		waitAndLog(2);

		if (verifyElementIsEnabled(reviews.Create_Review_Help_link)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Create_Review_Help_link));
			textbutton(reviews.Create_Review_Help_link, "link").isEnabled();
			logger.info("Create Review help Link  is  enabled");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Create_Review_Help_link));
			logger.info("Create Review help Link  is disbaled");
		}

		logger.info("Verified Help link");
		waitAndLog(2);

		if (verifyElementIsEnabled(reviews.Create_Review_Create_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Create_Review_Create_button));
			button(reviews.Create_Review_Create_button, "button").isEnabled();
			logger.info("create button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("create btton Is Enabled ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Create_Review_Create_button));
			logger.info("create button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("create btton id disabled");
		}
		logger.info("Verified create button");

	}

	public void tc_221_Create_Review_AddSite_IsDisplay() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click onReviews_Site_Selec_Others").click();
		waitAndLog(2);

		reviews.Create_Review_Add_Site.click();
		waitAndLog(2);

		if (verifyElementIsEnabled(reviews.Create_Review_Add_Site)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Create_Review_Add_Site));
			button(reviews.Create_Review_Add_Site, "textbox").isEnabled();
			logger.info("Add Site textbox is displayed");
			TestUtil.takeScreenshotAtEndOfTest("Add Site textbox Is Enabled ");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Create_Review_Add_Site));
			logger.info("Add Site textbox  is not displayed");
			TestUtil.takeScreenshotAtEndOfTest("Add Site textbox is not available");// ScreenShot capture
		}
		logger.info("Verified Add site text field");

	}

	public void Create_Review_Create_Button_IsEnabled() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Google, "Click Reviews_Site_Selec_Google").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("www.Facebook.com");
		logger.info("Entered link details into link field");
		waitAndLog(2);

		if (verifyElementIsEnabled(reviews.Create_Review_Create_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Create_Review_Create_button));
			button(reviews.Create_Review_Create_button, "button").isEnabled();
			logger.info("Create button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("Create button Is Enabled ");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Create_Review_Create_button));
			logger.info("Create button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("create btuton is disabled");// ScreenShot capture
		}
		logger.info("verified Create button is enabled ");

	}

	public void Create_Review_navigate_to_Reviewlist_230() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Yelp, "Click Reviews_Site_Selec_Yelp").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered link into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		reviews.reviews_Tab.getText();
		logger.info("reviews.reviews_Tab.getText()");

		if (reviews.reviews_Tab.getText().equalsIgnoreCase("Reviews")) {
			Assert.assertEquals(reviews.reviews_Tab.getText(), "Reviews", "Reviews");
			logger.info("Reviews tab is verified");
			TestUtil.takeScreenshotAtEndOfTest("navigation to reviews list page case is passed");// ScreenShot capture

		} else {
			Assert.assertNotEquals(reviews.reviews_Tab.getText(), "Reviews", "Reviews tab is not verified");
			TestUtil.takeScreenshotAtEndOfTest("navigation to reviews list page case is faild");// ScreenShot capture
		}
		logger.info("Verified navigation to reviews main page");
		button(reviews.Yelp_manage_button, "click on Yelp manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();
	}

	// create review displayed in the list

	public void create_review_display_in_list() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Facebook, "Click Reviews_Site_Selec_Facebook").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered dat into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		reviews.Created_Review_name_in_list.getText();
		logger.info("reviews.Created_Review_name_in_list.getText()");

		if (reviews.Created_Review_name_in_list.getText().equalsIgnoreCase("Facebook")) {
			Assert.assertEquals(reviews.Created_Review_name_in_list.getText(), "Facebook", "Facebook");
			logger.info("created review record is displayed in review list is verified");
			TestUtil.takeScreenshotAtEndOfTest("created review record is displayed in review list  case is passed");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(reviews.Created_Review_name_in_list.getText(), "Reviews",
					"created review record is displayed in review list is not verified");
			TestUtil.takeScreenshotAtEndOfTest("created review record is displayed in review list  case is faild");// ScreenShot
																													// capture
		}
		logger.info("Verified created record under review list");
		button(reviews.Facebook_manage_button, "click on facebook manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();
	}

	public void created_review_in_list_is_defaultly_turnOn_verification() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Google, "Click Reviews_Site_Selec_Google").click();
		waitAndLog(2);
		textbox(reviews.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered data into linktext field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		reviews.Review_list_record_default_turn_on_toggle_button.isSelected();
		logger.info("repository.Review_list_record_default_turn_on_check_button.isSelected()");

		if (verifyElementIsEnabled(reviews.Review_list_record_default_turn_on_toggle_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Review_list_record_default_turn_on_toggle_button));
			button(reviews.Review_list_record_default_turn_on_toggle_button, "check button").isSelected();
			logger.info("Review under list is defaultlyturned on is defaultly enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review under list is defaultly turned on button Is Enabled case is passed");// ScreenShot capture

		} else {
			Assert.assertEquals(false,
					verifyElementIsEnabled(reviews.Review_list_record_default_turn_on_toggle_button));
			logger.info("Review under list is defaultly turned on is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review under list is defaultly turned on button Is defaultly Enabled case is faild");// ScreenShot
																											// capture
		}
		logger.info("Verified Toggle button is defaultly selected");
		button(reviews.google_manage_button, "click on google manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();

	}

	public void Functionality_of_Review_tab_in_manage_Layout_popup() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample1");
		logger.info("Entered text into Add site field ");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.Review_Manage_button, "click on Manage button").click();
		logger.info("Clickedon Manage button");
		waitAndLog(2);

		button(reviews.Manage_page_Reviews_button, "click on Review next to Mange review on top").click();
		logger.info("Clicked on review breadcrum link");
		waitAndLog(2);

		reviews.Review_Page_select_All_lable.getText();
		waitAndLog(2);
		logger.info(reviews.Review_Page_select_All_lable.getText());

		if (reviews.Review_Page_select_All_lable.getText().equalsIgnoreCase("Select All")) {
			Assert.assertEquals(reviews.Review_Page_select_All_lable.getText(), "Select All", "Select All");
			logger.info("Select All lable is visible in review List page");
			TestUtil.takeScreenshotAtEndOfTest("Review link button is working fine in Manage Review page");
		} else {

			Assert.assertEquals(reviews.Review_Page_select_All_lable.getText(), "Select All", "Select All");
			logger.info("Select All lable is not visible in review List page");
			TestUtil.takeScreenshotAtEndOfTest("Review link button is not working fine in Manage Review page");

		}
		logger.info("Verified the functionalityof Review breadcrum link");
		button(reviews.Review_record_sample1_manage_button_under_list, "click on Manage button").click();
		logger.info("Clicked on Manage button");
		ManagePage_Delete_Method();
	}

	public void Review_list_Page_Mange_button_isEnable() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		reviews.Review_record_sample1_manage_button_under_list.isEnabled();
		logger.info("Manage button for revive records is enabled");

		if (verifyElementIsEnabled(reviews.Review_record_sample1_manage_button_under_list)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Review_record_sample1_manage_button_under_list));
			button(reviews.Review_record_sample1_manage_button_under_list, "Manage button").isEnabled();
			logger.info("Manage button for review record is enabled");
			TestUtil.takeScreenshotAtEndOfTest("Manage button for review record Enabled case is passed");// ScreenShot
																											// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Review_record_sample1_manage_button_under_list));
			logger.info("Manage button for review record is disabled");
			TestUtil.takeScreenshotAtEndOfTest("Manage button for review record Enabled case is faild");// ScreenShot
																										// capture
		}

		logger.info("Verified the manage button is enable");
		button(reviews.Review_record_sample1_manage_button_under_list, "click on Manage button").click();
		logger.info("Clicked on Manage button");
		ManagePage_Delete_Method();
	}

	// functionality of Manage button TC_241

	public void TC_241_Review_Page_click_MangeButton_Navigates_ManageReviewPage() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered text into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		button(reviews.sample1_review_manage_button, "click on Manage").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		reviews.Manage_Review_Page_lable.getText();
		logger.info("Manage review lable is available in edit manage page");

		if (reviews.Manage_Review_Page_lable.getText().equalsIgnoreCase("Manage Review")) {
			Assert.assertEquals(reviews.Manage_Review_Page_lable.getText(), "Manage Review", "Manage Review");
			logger.info("Navigated to edit Manage review page successfully");
			TestUtil.takeScreenshotAtEndOfTest("Navigation to edit Manage review page scenario is passed");// ScreenShot
																											// capture

		} else {
			Assert.assertEquals(reviews.Manage_Review_Page_lable.getText(), "Manage Review", "Manage Review");
			logger.info("Faild to Navigate to edit Manage review page");
			TestUtil.takeScreenshotAtEndOfTest("Navigation to edit Manage review page scenario is faild");// ScreenShot
																											// capture
		}
		logger.info("Verified the navigation of mangae review page");
		ManagePage_Delete_Method();
	}

	// not yet completed
	// Availability of review information in edit review page TC_242

	public void Edit_Review_Page_Review_record_AllData_isavailable() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample11");
		logger.info("Entered data into Add site fields");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("test");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		button(reviews.Review_record_sample11_manage_button_under_list, "click on Manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		logger.info(reviews.Review_record_sample11_related_SiteDD_data.getText());
		if (reviews.Review_record_sample11_related_SiteDD_data.getText().equalsIgnoreCase("sample11")) {
			Assert.assertEquals(reviews.Review_record_sample11_related_SiteDD_data.getText(), "sample11", "sample11");
			logger.info("Select site DD data for the edit review record is showing correctly");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verification of site DD data for edit review record scenario is passed");// ScreenShot capture

		} else

		{
			Assert.assertEquals(reviews.Review_record_sample11_related_SiteDD_data.getText(), "sample11", "");
			logger.info("Select site DD data for the edit review record is not showing correctly");
			TestUtil.takeScreenshotAtEndOfTest("Verification of site DD data for edit review record scenario is Faild");// ScreenShot
																														// capture
		}

		logger.info("Verfied selected site data ");

		textbox(reviews.Rivew_record_sample11_Link_details).isEnabled();

		if (verifyElementIsEnabled(reviews.Rivew_record_sample11_Link_details)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Rivew_record_sample11_Link_details));
			button(reviews.Rivew_record_sample11_Link_details, "link text box data is available").isEnabled();
			logger.info("link related data of review record is enabled");
			TestUtil.takeScreenshotAtEndOfTest("link related data of review record is enabled case is passed");// ScreenShot
																												// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Rivew_record_sample11_Link_details));
			logger.info("link related data of review record is disabled");
			TestUtil.takeScreenshotAtEndOfTest("link related data of review record is enabled case is faild");// ScreenShot
																												// capture
		}
		logger.info("Verified data in Link field");
		ManagePage_Delete_Method();

	}

	public void Edit_Review_Page_Add_Site_textField_isEnable() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("var1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.edit_review_page_var1_manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		Click_on_SelectSite_DD();

		List<WebElement> allOptions1 = driver.findElements(By.xpath("//select[contains(@id,'definedTitle')]//option"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Others")) {

				allOptions1.get(i).click();
				break;
			}

		}

		logger.info("Selected Others");
		reviews.Create_Review_Add_Site.click();
		waitAndLog(2);

		logger.info(reviews.Create_Review_Add_Site.getText());

		if (reviews.Create_Review_Add_Site.getText().equalsIgnoreCase("Add Site")) {
			Assert.assertEquals(reviews.Create_Review_Add_Site.getText(), "Add Site", "Add Site");
			logger.info("Add Site text fiels is enabled in edit review page");
			TestUtil.takeScreenshotAtEndOfTest("Add Site text field is enabled in edit review page scenario is passes");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(reviews.Create_Review_Add_Site.getText(), "Add Site", "");
			logger.info("Add Site text fiels is not enabled in edit review page");
			TestUtil.takeScreenshotAtEndOfTest("Add Site text field is enabled in edit review page scenario is failed");// ScreenShot
																														// capture
		}
		logger.info("Verified the Add Site field in edit page");
		ManagePage_Delete_Method();
	}

	public void Create_Review_page_AddSite_Duplicate_text_message() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click onReviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample6");
		logger.info("Entered text into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("abcd");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		Click_on_AddReviewSite();

		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click onReviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Add_Site_Review_button).click();
		logger.info("Clicked on Add site review button");
		waitAndLog(2);

		textbox(reviews.Add_Site_Review_button).sendKeys("sample6");
		logger.info("Entered text into Add site ");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("ABCD");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		reviews.Create_Review_page_AddSite_textField_duplicate_error_message.getText();
		waitAndLog(2);
		logger.info(reviews.Create_Review_page_AddSite_textField_duplicate_error_message.getText());

		if (reviews.Create_Review_page_AddSite_textField_duplicate_error_message.getText()
				.equalsIgnoreCase(" Title exists already. ")) {
			Assert.assertEquals(reviews.Create_Review_page_AddSite_textField_duplicate_error_message.getText(),
					" Title exists already. ", " Title exists already. ");
			logger.info("Title exist already. text message is displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Duplicate error message for Add Site text field in create review page scenario is passes");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(reviews.Create_Review_page_AddSite_textField_duplicate_error_message.getText(),
					" Title exists already. ", "");
			logger.info("Title exist already. text message is not displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Duplicate error message for Add Site text field in create review page scenario is failed");// ScreenShot
																												// capture
		}
		logger.info("verified duplicate error message for Add site text field");
		Click_on_Reviews();
		button(reviews.sample6_review_manage_button, "click on manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();
	}

	public void Edit_Review_page_AddSite_Duplicate_text_message() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample9");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Edit_Review_page_Link_textbox).sendKeys("auto");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.edit_review_page_sample9_manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		Click_on_Edit_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("edit2");
		logger.info("Entered data into link field");
		waitAndLog(2);

		ERP_Save_button(); // ERP-Edit Review Page
		waitAndLog(2);
		reviews.Edit_Review_page_AddSite_textField_duplicate_error_message.getText();
		waitAndLog(2);
		logger.info(reviews.Edit_Review_page_AddSite_textField_duplicate_error_message.getText());

		if (reviews.Edit_Review_page_AddSite_textField_duplicate_error_message.getText()
				.equalsIgnoreCase(" Title exists already. ")) {
			Assert.assertEquals(reviews.Edit_Review_page_AddSite_textField_duplicate_error_message.getText(),
					" Title exists already. ", " Title exists already. ");
			logger.info("Title exist already. text message is displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Duplicate error message for Add Site text field in edit review page scenario is passes");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(reviews.Edit_Review_page_AddSite_textField_duplicate_error_message.getText(),
					" Title exists already. ", "");
			logger.info("Title exist already. text message is displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Duplicate error message for Add Site text field in edit review page scenario is failed");// ScreenShot
																												// capture
		}

		logger.info("verified duplicate error message for Add site field");
		Click_on_Reviews();
		button(reviews.edit_review_page_sample9_manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		ManagePage_Delete_Method();
		waitAndLog(2);
		button(reviews.edit2_Manage_button, "click on edit2 manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		ManagePage_Delete_Method();
	}

	// not yet completed
	// Link field mandatory text message TC_252

	public void Edit_Review_page_mandatory_text_Message_for_Link_Textbox() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).click();
		logger.info("Clicked on Add site ");
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample8");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("auto");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.edit_review_page_sample8_manage_button, "click on Manage button").click();
		logger.info("Clicked on Manage button");
		waitAndLog(2);

		textbox(reviews.Edit_Review_page_Link_textbox).click();
		logger.info("Clicked on Link text field");
		waitAndLog(2);

		String s = Keys.chord(Keys.CONTROL, "a");
		reviews.Edit_Review_page_Link_textbox.sendKeys(s);
		// sending DELETE key
		reviews.Edit_Review_page_Link_textbox.sendKeys(Keys.DELETE);
		logger.info("link text is removed");
		waitAndLog(2);

		reviews.Edit_Review_page_Link_textbox.sendKeys(Keys.BACK_SPACE);
		Click_on_Edit_SelectSite_DD();
		waitAndLog(2);
		reviews.Edit_Review_page_LinkTextField_mandatory_text_message.getText();
		waitAndLog(2);
		logger.info(reviews.Edit_Review_page_LinkTextField_mandatory_text_message.getText());

		if (reviews.Edit_Review_page_LinkTextField_mandatory_text_message.getText()
				.equalsIgnoreCase("  Please input link for review. ")) {
			Assert.assertEquals(reviews.Edit_Review_page_LinkTextField_mandatory_text_message.getText(),
					"  Please input link for review. ", "  Please input link for review. ");
			logger.info(" Please input link for review.  text message is displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Duplicate error message for Add Site text field in edit review page scenario is passes");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(reviews.Edit_Review_page_LinkTextField_mandatory_text_message.getText(),
					"  Please input link for review. ", "");
			logger.info(" Please input link for review.  text message is not displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Duplicate error message for Add Site text field in edit review page scenario is failed");// ScreenShot
																												// capture
		}
		logger.info("Verified duplicate error message for Add site field");
		ManagePage_Delete_Method();
	}

	public void edit_Review_page_save_button_isEnable() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).click();
		logger.info("Clicked on Add site");
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("save1");
		logger.info("Entered data into add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("locked");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		button(reviews.edit_review_page_save1_manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		reviews.Edit_Review_page_Save_Button.isEnabled();
		logger.info("Save button is enabled");

		if (verifyElementIsEnabled(reviews.Edit_Review_page_Save_Button)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Edit_Review_page_Save_Button));
			button(reviews.Edit_Review_page_Save_Button, "save button").isEnabled();
			logger.info("Save button for review record is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Save button for review record should be Enabled if all mandatory fields are filled scenario is passed");// ScreenShot
																																// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Edit_Review_page_Save_Button));
			logger.info("Save button for review record is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Save button for review record should be Enabled if all mandatory fields are filled scenario is failed");// ScreenShot
																																// capture
		}

		logger.info("Verified Save button is enabled");
		ManagePage_Delete_Method();
	}

	// on click on save button with valied data should redirect to review list page

	public void edit_Review_page_redirects_to_Reviews_page_by_click_on_Save_button() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).click();
		logger.info("Clicked on Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("Red1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("soke");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		button(reviews.edit_review_page_Red1_manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		ERP_Save_button();

		reviews.Review_AddReviewSite_button.getText();
		logger.info(reviews.Review_AddReviewSite_button.getText());

		if (reviews.Review_AddReviewSite_button.getText().equalsIgnoreCase(" ADD REVIEW SITE")) {
			Assert.assertEquals(reviews.Review_AddReviewSite_button.getText(), " ADD REVIEW SITE ", " ADD REVIEW SITE");
			logger.info(" ADD REVIEW SITE  lable is displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"redirecting to Reviews page by click on save button scenario is passes");// ScreenShot capture

		} else {
			Assert.assertNotEquals(reviews.Review_AddReviewSite_button.getText(), " ADD REVIEW SITE", "");
			logger.info(" ADD REVIEW SITE labele is not displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"redirecting to Reviews page by click on save button scenario is failed");// ScreenShot capture
		}
		logger.info("Verified redirection to Reviews page by click on save");
		button(reviews.Review_Manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();
	}

	// Delete Review button should be enable in manage review page

	public void Manage_review_Page_DeleteReview_button_IsEnabled() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample2");
		logger.info("Entered data into add site field ");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_262");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.sample2_review_manage_button, "click on manage button for record sample2").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		reviews.Manage_review_page_Delete_Review_button.isEnabled();
		logger.info("verify Delete Review button is enabled");

		if (verifyElementIsEnabled(reviews.Manage_review_page_Delete_Review_button)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Manage_review_page_Delete_Review_button));
			button(reviews.Manage_review_page_Delete_Review_button, "Delete Review button").isEnabled();
			logger.info("Delete Review button for review record is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Delete Review button for review record should be Enabled scenario is passed");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Manage_review_page_Delete_Review_button));
			logger.info("Delete Review button for review record is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Delete Review button for review record should be Enabled scenario is failed");// ScreenShot capture
		}

		logger.info("Verified delete button");
		ManagePage_Delete_Method();
	}

	// confirm delete review button is enabled in manage review button

	public void manage_Review_page_Confirm_Delete_Button_IsEnabled() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[contains(@id,'definedTitle')]//option"));
		logger.info(allOptions.size());

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample3");
		logger.info("Entered data into Add site button");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_263");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.sample3_review_manage_button, "click on manage button for record sample3").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		button(reviews.Manage_review_page_Delete_Review_button, "clickon Delete Review button").click();
		logger.info("Clicked on Delete button");
		waitAndLog(2);
		logger.info("waitAndLog  for 2 sec");

		reviews.Manage_review_page_Confirm_Delete_Review_button.isEnabled();
		logger.info("verify Confirm Delete Review button is enabled");

		if (verifyElementIsEnabled(reviews.Manage_review_page_Confirm_Delete_Review_button)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Manage_review_page_Confirm_Delete_Review_button));
			button(reviews.Manage_review_page_Confirm_Delete_Review_button, "Confirm Delete Review button").isEnabled();
			logger.info(" Confirm Delete Review button for review record is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					" Confirm Delete Review button for review record should be Enabled scenario is passed");// ScreenShot
																											// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Manage_review_page_Confirm_Delete_Review_button));
			logger.info("Confirm Delete Review button for review record is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Confirm Delete Review button for review record should be Enabled scenario is failed");// ScreenShot
																											// capture
		}
		logger.info("Verified availability of confirm delete button");
		button(reviews.Manage_review_page_Confirm_Delete_Review_button, "click on confirm delete button").click();
		logger.info("Clicked on Confirm delete button");
	}

	public void ManageReview_page_Click_ConfirmDeleteReview_Button_redirects_Reviews_page() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample4");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_264");
		logger.info("Entered data into Link text field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.sample4_review_manage_button, "click on manage button for record sample4").click();
		logger.info("Clicked on Manage button");
		waitAndLog(2);

		ManagePage_Delete_Method();

		reviews.Review_AddReviewSite_button.getText();
		logger.info(reviews.Review_AddReviewSite_button.getText());

		if (reviews.Review_AddReviewSite_button.getText().equalsIgnoreCase("ADD REVIEW SITE")) {
			Assert.assertEquals(reviews.Review_AddReviewSite_button.getText(), "ADD REVIEW SITE", "ADD REVIEW SITE");
			logger.info(" ADD REVIEW SITE  button is displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"redirecting to Reviews page by click on save button scenario is passes");// ScreenShot capture

		} else {
			Assert.assertNotEquals(reviews.Review_AddReviewSite_button.getText(), "ADD REVIEW SITE", "");
			logger.info(" ADD REVIEW SITE button is not displayed ");
			TestUtil.takeScreenshotAtEndOfTest(
					"redirecting to Reviews page by click on save button scenario is failed");// ScreenShot capture
		}
		logger.info("verified navigation to Reviews page by click on Save");
	}

	// verify deleted review record in review list

	public void Verify_Deleted_record_in_Review_List_page() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("sample4");
		logger.info("Entered data into Add site text field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_264");
		logger.info("Entered data into link field");
		waitAndLog(2);

		button(reviews.Create_Review_Create_button, "click on create button").click();
		logger.info("Clicked on Create button");
		waitAndLog(2);

		button(reviews.sample4_review_manage_button, "click on manage button for record sample4").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		ManagePage_Delete_Method();
	}

	// should show a toaster message "Review <Site Name> has been successfully
	// created."

	public void Verify_Toaster_message_for_Created_review_record_231() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();
		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("create1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_231");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();

		reviews.Toaster_Message_For_create_Record.getText();
		logger.info(reviews.Toaster_Message_For_create_Record.getText());

		if (reviews.Toaster_Message_For_create_Record.getText()
				.equalsIgnoreCase("Review create1 has been successfully created.")) {
			Assert.assertEquals(reviews.Toaster_Message_For_create_Record.getText(),
					"Review create1 has been successfully created.", "Review create1 has been successfully created.");
			logger.info(" Toaster message for created record is displayed ");
			TestUtil.takeScreenshotAtEndOfTest("Toaster message for created record scenario is passes");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(reviews.Toaster_Message_For_create_Record.getText(),
					"Review create1 has been successfully created.", "");
			logger.info(" Toaster message for created record is not displayed ");
			TestUtil.takeScreenshotAtEndOfTest("Toaster message for created record scenario is failed");// ScreenShot
																										// capture
		}

		button(reviews.Create1_Manage_button, "click on Manage button").click();
		logger.info("Clicked on Manage button");
		ManagePage_Delete_Method();
	}

	// verify Add Review site button is enable in renewal page in edit case

	public void Verify_ADDREVIEWSITE_button_Isenabled_in_Review_list_page() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("create1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_235");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.Create1_Manage_button, "click on create1 manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		button(reviews.Manage_page_Reviews_button, "clickon review button").click();
		logger.info("Clicked on review button");
		waitAndLog(2);

		reviews.Review_AddReviewSite_button.getText();
		logger.info(reviews.Review_AddReviewSite_button.getText());

		if (reviews.Review_AddReviewSite_button.getText().equalsIgnoreCase("ADD REVIEW SITE")) {
			Assert.assertEquals(reviews.Review_AddReviewSite_button.getText(), "ADD REVIEW SITE", "ADD REVIEW SITE");
			logger.info(" ADD REVIEW SITE button is enabled in review list page");
			TestUtil.takeScreenshotAtEndOfTest("ADD REVIEW SITE button is enabled scenario is passes");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(reviews.Review_AddReviewSite_button.getText(),
					"Review create1 has been successfully created.", "");
			logger.info(" ADD REVIEW SITE button is not enabled in review list page");
			TestUtil.takeScreenshotAtEndOfTest("ADD REVIEW SITE button is enabled scenario is failed");// ScreenShot
																										// capture
		}
		logger.info("Verified user navigated to reviews main page and Add Review site button");
		button(reviews.Create1_Manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();
	}

	public void Verify_Toaster_message_for_updated_review_record() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();
		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("create4");
		logger.info("Entered data into Add site");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_257");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.Create4_Manage_button, "click on create4 manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		textbox(reviews.Edit_Review_page_Link_textbox).click();
		logger.info("Clicked on link text field");
		waitAndLog(2);

		textbox(reviews.Edit_Review_page_Link_textbox).sendKeys("lucky");
		logger.info("Entered data into link field");
		waitAndLog(2);

		ERP_Save_button();

		reviews.Toaster_Message_For_Updated_Record.getText();
		logger.info(reviews.Toaster_Message_For_Updated_Record.getText());

		if (reviews.Toaster_Message_For_Updated_Record.getText()
				.equalsIgnoreCase("Review create4 has been successfully updated.")) {
			Assert.assertEquals(reviews.Toaster_Message_For_Updated_Record.getText(),
					"Review create4 has been successfully updated.", "Review create4 has been successfully updated.");
			logger.info(" Toaster message for updated record is displayed ");
			TestUtil.takeScreenshotAtEndOfTest("Toaster message for updated record scenario is passes");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(reviews.Toaster_Message_For_Updated_Record.getText(),
					"Review create4 has been successfully updated.", "");
			logger.info(" Toaster message for updated record is not displayed ");
			TestUtil.takeScreenshotAtEndOfTest("Toaster message for updated record scenario is failed");// ScreenShot
																										// capture
		}
		logger.info("Verified Toster message");
		button(reviews.Review_Manage_button, "click on Manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();

	}

	public void functionality_of_SelectAll_toggle_button() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("toggle");
		logger.info("Entered data into Add Site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_239");
		logger.info("Entered data into Link text field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.Select_All_toggle_button, "click on select All toggle button").click();
		logger.info("Clicked on Select All toggle button");
		waitAndLog(2);

		reviews.toggle_On_Off_button_for_All_records_selected.isEnabled();
		logger.info("reviews.toggle_On_Off_button_for_toggle1_record.isEnabled()");

		if (verifyElementIsEnabled(reviews.toggle_On_Off_button_for_All_records_selected)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.toggle_On_Off_button_for_All_records_selected));
			button(reviews.toggle_On_Off_button_for_All_records_selected, "check button").isEnabled();
			logger.info("toggle buttons for all records in the list is disabled by click on SelectAll button");
			TestUtil.takeScreenshotAtEndOfTest(
					"toggle button for the record in the list is disable by click on SelectAll button case is passed");// ScreenShot
																														// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.toggle_On_Off_button_for_All_records_selected));
			logger.info("toggle button for the record in the list is enabled by click on SelectAll button ");
			TestUtil.takeScreenshotAtEndOfTest(
					"toggle buttons for all records in the list is enabled by click on SelectAll button case is faild");// ScreenShot
																														// capture
		}
		logger.info("Verified toggle buttons for all records ");
		button(reviews.toggle_manage_button, "click on manage butoon").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		ManagePage_Delete_Method();
	}

	public void verify_site_information_for_record_in_edit_page() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("site1");
		logger.info("Entered data into Add site");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_245");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.site1_manage_button, "click on manage button of site1 record").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		reviews.site1_record_siteDD_data_in_edit_page.getText();
		logger.info("reviews.site1_record_siteDD_data_in_edit_page.getText()");

		if (reviews.site1_record_siteDD_data_in_edit_page.getText().equalsIgnoreCase("site1")) {
			Assert.assertEquals(reviews.site1_record_siteDD_data_in_edit_page.getText(), "site1", "site1");
			logger.info(" record related text  is available in site DD for record in edit page ");
			TestUtil.takeScreenshotAtEndOfTest(
					"record related text is available in site DD for record in edit page scenario is passes");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(reviews.site1_record_siteDD_data_in_edit_page.getText(), "site1", "");
			logger.info(" record related text  is  available in site DD for record in edit page ");
			TestUtil.takeScreenshotAtEndOfTest(
					"record related text is available in site DD for record in edit page  scenario is failed");// ScreenShot
																												// capture
		}
		logger.info("Verified related data in Select site DD in edit page");
		ManagePage_Delete_Method();

	}

	// not completed
	// functionality of link in create review page TC_228

	public void functionality_of_link_Learnhowtosetupyourreviewsites() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_Link();
		reviews.Beetexting_label_in_review_link_help_page.getText();
		waitAndLog(2);

		logger.info(reviews.Beetexting_label_in_review_link_help_page.getText());

		if (reviews.Beetexting_label_in_review_link_help_page.getText().equalsIgnoreCase("Beetexting")) {
			Assert.assertEquals(reviews.Beetexting_label_in_review_link_help_page.getText(), "Beetexting",
					"Beetexting");
			logger.info(" On click of Link it opened pre-defined blog related to review page ");
			TestUtil.takeScreenshotAtEndOfTest(
					"On click of Link it opened pre-defined blog related to review page  scenario is passes");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(reviews.Beetexting_label_in_review_link_help_page.getText(), "Beetexting", "");
			logger.info(" On click of Link it opened google home page  ");
			TestUtil.takeScreenshotAtEndOfTest(
					"On click of Link it opened pre-defined blog related to review page  scenario is failed");// ScreenShot
																												// capture
		}
		logger.info("Verified navigated to related link page by click on it");
	}

	public void Verify_Toaster_message_for_Deleted_review_record() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("Delete1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_265");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.delete1_Manage_button, "click on Delete1 manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		ManagePage_Delete_Method();

		reviews.Toaster_Message_For_Delete_Record.getText();
		logger.info(reviews.Toaster_Message_For_Delete_Record.getText());

		if (reviews.Toaster_Message_For_Delete_Record.getText().equalsIgnoreCase("Delete1 has been deleted.")) {
			Assert.assertEquals(reviews.Toaster_Message_For_Delete_Record.getText(), "Delete1 has been deleted.",
					"delete1 has been deleted.");
			logger.info(" Toaster message for deleted record is displayed ");
			TestUtil.takeScreenshotAtEndOfTest("Toaster message for deleted record scenario is passes");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(reviews.Toaster_Message_For_Delete_Record.getText(), "Delete1 has been deleted.",
					"");
			logger.info(" Toaster message for deleted record is not displayed ");
			TestUtil.takeScreenshotAtEndOfTest("Toaster message for deleted record scenario is failed");// ScreenShot
																										// capture
		}
		logger.info("Verified Toster message for delete");
	}

	public void create_review_Link_Textbox_allows_5000_char_225() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		waitAndLog(2);
		String randomtext = RandomStringUtils.randomAlphabetic(5000);
		textbox(reviews.Create_Review_Link_textbox).sendKeys(randomtext);
		logger.info("Entered data into Link field");
		waitAndLog(2);

		reviews.create_Review_page_Link_Char_Count.getText();
		waitAndLog(2);
		logger.info(reviews.create_Review_page_Link_Char_Count.getText());
		if (reviews.create_Review_page_Link_Char_Count.getText().equalsIgnoreCase("5000/5000")) {
			Assert.assertEquals(reviews.create_Review_page_Link_Char_Count.getText(), "5000/5000", "5000/5000");
			logger.info("Allowing only 5000 charecters in link text field");
			TestUtil.takeScreenshotAtEndOfTest("Should Allow upto 5000 charecters scenario is working fine");
		} else {

			Assert.assertEquals(reviews.create_Review_page_Link_Char_Count.getText(), "5000/5000", "5000/5000");
			logger.info("Allowing more than 5000 charecters in link text field");
			TestUtil.takeScreenshotAtEndOfTest("Should Allow upto 5000 charecters scenario is not working fine");
		}
		logger.info("Verified error text message for Link");
	}

	public void Edit_review_Link_Textbox_allows_5000_char() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[contains(@id,'definedTitle')]//option"));
		logger.info(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Others")) {

				allOptions.get(i).click();
				break;
			}

		}
		logger.info("Selected Others");
		textbox(reviews.Create_Review_Add_Site).sendKeys("v1");
		logger.info("Entered data into Add site");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_251");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.v1_Manage_button, "click on v1 manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys(
				"5000 hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hellodear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello dear!hello5");
		logger.info("Entered data into link field");
		waitAndLog(2);

		reviews.create_Review_page_Link_Char_Count.getText();
		waitAndLog(2);
		logger.info(reviews.create_Review_page_Link_Char_Count.getText());
		if (reviews.create_Review_page_Link_Char_Count.getText().equalsIgnoreCase("5000/5000")) {
			Assert.assertEquals(reviews.create_Review_page_Link_Char_Count.getText(), "5000/5000", "5000/5000");
			logger.info("Allowing only 5000 charecters in link text field");
			TestUtil.takeScreenshotAtEndOfTest("Should Allow upto 5000 charecters scenario is working fine");
		} else {

			Assert.assertEquals(reviews.create_Review_page_Link_Char_Count.getText(), "5000/5000", "");
			logger.info("Allowing more than 5000 charecters in link text field");
			TestUtil.takeScreenshotAtEndOfTest("Should Allow upto 5000 charecters scenario is not working fine");
		}

		logger.info("Verified validation message");
		ManagePage_Delete_Method();
	}

	public void Edited_review_record_Shows_under_review_list() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("edit1");
		logger.info("Entered data into Add site  field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_258");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.edit1_Manage_button, "click on edit1 manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		textbox(reviews.Edit_Review_page_Link_textbox).sendKeys("testcase");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		ERP_Save_button();

		reviews.edit1_record_under_list.getText();
		waitAndLog(2);
		logger.info(reviews.edit1_record_under_list.getText());

		if (reviews.edit1_record_under_list.getText().equalsIgnoreCase("edit1")) {
			Assert.assertEquals(reviews.edit1_record_under_list.getText(), "edit1", "edit1");
			logger.info("edited review record is displayed in review list is verified");
			TestUtil.takeScreenshotAtEndOfTest("Edited review record is displayed in review list  case is passed");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(reviews.edit1_record_under_list.getText(), "Reviews",
					"Edited review record is displayed in review list is not verified");
			TestUtil.takeScreenshotAtEndOfTest("created review record is displayed in review list  case is faild");// ScreenShot
																													// capture
		}
		logger.info("Verified create record under review list");
		button(reviews.Review_Manage_button, "click on Manage button").click();
		logger.info("Clicked on Manage button");
		ManagePage_Delete_Method();

	}

	public void Edited_review_record_verify_toggle_button_isSelected() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("edit2");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_259");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.edit2_Manage_button, "click on edit2 manage button").click();
		logger.info("Clicked on Manage button");
		waitAndLog(2);

		textbox(reviews.Edit_Review_page_Link_textbox).sendKeys(" testcase");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		ERP_Save_button();

		reviews.edit2_record_default_turn_on_check_button.isSelected();
		logger.info("repository.edit2_record_default_turn_on_check_button.isSelected()");

		if (verifyElementIsEnabled(reviews.edit2_record_default_turn_on_check_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.edit2_record_default_turn_on_check_button));
			button(reviews.edit2_record_default_turn_on_check_button, "check button").isSelected();
			logger.info("edited review record toggle button is default enabled and selected");
			TestUtil.takeScreenshotAtEndOfTest(
					"edited review record toggle button is default enabled and selected case is passed");// ScreenShot
																											// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.edit2_record_default_turn_on_check_button));
			logger.info("edited review record toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"edited review record toggle button is default enabled and selected case is faild");// ScreenShot
																										// capture
		}
		logger.info("Verified toggle button for edited record");
		button(reviews.edit2_Manage_button, "click on edit2 manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

	}

	// Messages-Review link icon related test methods starts

	public void Verify_Review_icon_isdisplayed_in_message_page() throws Exception {

		waitAndLog(2);
		button(reviews.Review_icon_under_message_chat_box, "verify review icon in message page").isEnabled();
		logger.info(reviews.Review_icon_under_message_chat_box.isEnabled());

		if (verifyElementIsEnabled(reviews.Review_icon_under_message_chat_box)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Review_icon_under_message_chat_box));
			button(reviews.Review_icon_under_message_chat_box, "verify review icon in message page").isEnabled();
			logger.info("Review icon is available and enabled in message page");
			TestUtil.takeScreenshotAtEndOfTest("Review icon is available in message page scenario is passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Review_icon_under_message_chat_box));
			logger.info("Review icon is not available in message page ");
			TestUtil.takeScreenshotAtEndOfTest("review icon is  available in message page scenario is failed");
		}
		logger.info("Verified review icon in message page");

	}

	public void Click_on_Review_icon_NoActive_Records_navigates_to_Review_page() throws Exception {

		Click_on_Tools();
		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("edit2");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_259");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.Select_All_toggle_button, "click on SelectAll toggle button").click();
		logger.info("Clicked on SelectAll toggle button");
		waitAndLog(2);

		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		logger.info("Clicked on Close icon in tools page");
		waitAndLog(2);

		button(reviews.conversation_Automation, "click on conversation_Automation").click();
		logger.info("Clicked on conversation_Automation");
		waitAndLog(2);

		button(reviews.Review_icon_under_message_chat_box, "click on review icon next to billing icon").click();
		logger.info("Clicked on review icon in message page");
		waitAndLog(2);

		reviews.toggle_On_Off_button_for_All_records_unselected.isSelected();
		logger.info(reviews.toggle_On_Off_button_for_All_records_unselected.isSelected());
		if (verifyElementIsEnabled(reviews.toggle_On_Off_button_for_All_records_unselected)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.toggle_On_Off_button_for_All_records_unselected));
			button(reviews.toggle_On_Off_button_for_All_records_unselected, "toggle buttons").isSelected();
			logger.info("All review record toggle button is unselected");
			TestUtil.takeScreenshotAtEndOfTest(
					"All review record toggle button is  enabled and selected case is passed");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.toggle_On_Off_button_for_All_records_unselected));
			logger.info("All review record toggle button is selected");
			TestUtil.takeScreenshotAtEndOfTest(
					"All review record toggle button is  enabled and selected case is failed");// ScreenShot capture
		}
		logger.info("verified All review record toggle button");
		button(reviews.edit2_Manage_button, "click on manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		ManagePage_Delete_Method();

		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		waitAndLog(2);
	}

	// verify the message with url for reviews

	public void Verify_Review_message_with_url_in_message_textbox() throws Exception {

		Click_on_Tools();
		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("edit2");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_259");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		waitAndLog(2);
		button(reviews.conversation_Automation, "click on Automation under conversation list").click();
		logger.info("Clicked on Automation contact under conversation");
		waitAndLog(2);

		button(reviews.Review_icon_under_message_chat_box, "verify review icon in message page then click").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		textbox(reviews.message_text_box_field_in_messagees_page).getText();
		waitAndLog(2);
		logger.info(reviews.message_text_box_field_in_messagees_page.getText());

		if (reviews.message_text_box_field_in_messagees_page.getText().equalsIgnoreCase(
				"We are grateful for the opportunity to serve you! We need your help Would you please take 30 seconds to leave us a review using this link? https://automation.beetexting.com/rv?id=cxW0z")) {
			Assert.assertEquals(reviews.message_text_box_field_in_messagees_page.getText(),
					"We are grateful for the opportunity to serve you! We need your help Would you please take 30 seconds to leave us a review using this link? https://automation.beetexting.com/rv?id=cxW0z",
					"We are grateful for the opportunity to serve you! We need your help Would you please take 30 seconds to leave us a review using this link? https://automation.beetexting.com/rv?id=cxW0z");
			logger.info("message with review link is displayed in message textbox area");
			TestUtil.takeScreenshotAtEndOfTest(
					"message with review link should display by click on review icon scenario is passed");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(reviews.message_text_box_field_in_messagees_page.getText(),
					"We are grateful for the opportunity to serve you! We need your help Would you please take 30 seconds to leave us a review using this link? https://automation.beetexting.com/rv?id=cxW0z",
					"Edited review record is displayed in review list is not verified");

			TestUtil.takeScreenshotAtEndOfTest(
					"message with review link should display by click on review icon scenario is faild");// ScreenShot
																											// capture
		}
		button(reviews.messages_send_button, "click on send button").click();
		logger.info("Clicked on send button");
		waitAndLog(5);
		Click_on_Tools();
		Click_on_Reviews();
		waitAndLog(2);
		button(reviews.edit2_manage, "Click on edit2_manage").click();
		logger.info("Click on edit2_manage");
		ManagePage_Delete_Method();

		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		waitAndLog(2);

	}

	// navigation of new window by click on review link in message

	public void functionality_of_review_link_Url_in_message() throws Exception {

		Click_on_Tools();
		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("link2");
		logger.info("Entered data Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_272");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		logger.info("Clicked on Close icon in Tools page");
		waitAndLog(2);

		button(reviews.conversation_Automation, "click on Automation under conversation list").click();
		logger.info("Clicked on contact under conversation");
		waitAndLog(2);
		logger.info("waitAndLog for 2 sec");

		button(reviews.Review_icon_under_message_chat_box, "verify review icon in message page then click").click();
		logger.info("Clicked on review icon in manage button");
		waitAndLog(2);

		button(reviews.messages_send_button, "click on send button").click();
		logger.info("Clicked on Send button");
		waitAndLog(2);

		link(reviews.link_url_in_message_chatbox, "click on url given in message").click();
		logger.info("Clicked on Url given in message");
		waitAndLog(2);

		ArrayList<String> tabL1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabL1.get(1));

		waitAndLog(2);

		reviews.Leave_your_review_on_text.getText();
		logger.info(reviews.Leave_your_review_on_text.getText());

		if (reviews.Leave_your_review_on_text.getText().equalsIgnoreCase("Leave your review on")) {
			Assert.assertEquals(reviews.Leave_your_review_on_text.getText(), "Leave your review on");
			logger.info("Leave your review on text is available in links page");
			TestUtil.takeScreenshotAtEndOfTest(
					"Leave your review on text is available in links page scenario is passed");// ScreenShot capture

		} else {
			Assert.assertNotEquals(reviews.Leave_your_review_on_text.getText(),
					"Leave your review on text is not available in links page");

			TestUtil.takeScreenshotAtEndOfTest(
					"Leave your review on text is available in links page scenario is faild");// ScreenShot capture
		}

		driver.close();
		driver.switchTo().window(tabL1.get(0));
		waitAndLog(2);
		logger.info("Verified navigation of link related page");

		Click_on_Tools();
		Click_on_Reviews();
		button(reviews.link2_manage_button, "click on link2_manage_button").click();
		waitAndLog(2);
		ManagePage_Delete_Method();

		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		waitAndLog(2);
	}

	// This test case is depend on TC-272
	// verify organization name in link related page

	public void Verify_organization_name_in_Url_related_link_page() throws Exception {

		waitAndLog(2);
		button(reviews.conversation_Automation, "click on automation under conversation list").click();
		logger.info("Clicked on contact under conversation list");
		waitAndLog(2);

		link(reviews.link_url_in_message_chatbox, "click on url given in message").click();
		logger.info("Clicked on Url given in message");
		waitAndLog(2);

		ArrayList<String> tabL1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabL1.get(1));
		waitAndLog(2);

		reviews.org_name_in_url_related_page.getText();
		waitAndLog(2);
		logger.info(reviews.org_name_in_url_related_page.getText());

		if (reviews.org_name_in_url_related_page.getText().equalsIgnoreCase("Auto12")) {
			Assert.assertEquals(reviews.org_name_in_url_related_page.getText(), "Auto12", "Auto12");
			logger.info("organization name is showing in link page");
			TestUtil.takeScreenshotAtEndOfTest("organization name should showing in link page scenario is passes");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(reviews.org_name_in_url_related_page.getText(), "Auto12", "");
			logger.info("organization name is not showing in link page");
			TestUtil.takeScreenshotAtEndOfTest("organization name should showing in link page  scenario is failed");// ScreenShot
																													// capture
		}

		logger.info("Verified organization name in link related page");
		driver.close();
		driver.switchTo().window(tabL1.get(0));
		waitAndLog(2);

	}

	// check if message sent with url in message chat box

	public void Verify_message_sent_with_url_in_message_chat_area() throws Exception {

		Click_on_Tools();
		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("link3");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("TC_270");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		logger.info("Clicked on close icon");
		waitAndLog(2);

		button(reviews.Review_icon_under_message_chat_box, "verify review icon in message page then click").click();
		logger.info("Verified reviews icon in message page");
		waitAndLog(2);

		button(reviews.messages_send_button, "click on send button").click();
		logger.info("Clicked on send button");
		waitAndLog(5);
		logger.info("waitAndLog for 5 sec");

		reviews.link_url_in_message_chatbox.isEnabled();
		waitAndLog(2);
		logger.info(reviews.link_url_in_message_chatbox.isEnabled());

		if (verifyElementIsEnabled(reviews.link_url_in_message_chatbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.link_url_in_message_chatbox));
			link(reviews.link_url_in_message_chatbox, "verify url link is enable").isEnabled();
			logger.info("url link is enable in message");
			TestUtil.takeScreenshotAtEndOfTest("url link should be enable in message scenario is passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.link_url_in_message_chatbox));
			logger.info("url link is not enable in message");
			TestUtil.takeScreenshotAtEndOfTest("url link should be enable in message scenario is failed");
		}

		logger.info("Verified URL in mesage");
		Click_on_Tools();
		Click_on_Reviews();
		button(reviews.link3_manage_button, "click on manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();
		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		waitAndLog(2);

	}

	public void functionality_of_review_related_button_in_link_page() throws Exception {

		Click_on_Tools();
		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();
		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("link1");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("https://www.google.com/");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		logger.info("Clicked on close icon in tools page");
		waitAndLog(2);

		button(reviews.Review_icon_under_message_chat_box, "verify review icon in message page then click").click();
		logger.info("Clicked on review icon inmessage page");
		waitAndLog(2);

		button(reviews.messages_send_button, "click on send button").click();
		logger.info("Clicked on Send button");
		waitAndLog(2);

		link(reviews.link_url_in_message_chatbox_276, "click on url in message").click();
		logger.info("Clicked on url in message");
		waitAndLog(2);
		logger.info("waitAndLog for 2  sec");

		ArrayList<String> tabL1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabL1.get(1));
		waitAndLog(2);
		String Servicetext = button(reviews.link1_button_in_link_page, "link1 button is enable").getText();
		if (!(Servicetext.isEmpty())) {
			waitAndLog(2);
			button(reviews.link1_button_in_link_page, "click on link1").click();
			waitAndLog(2);
			logger.info("link1 is enable and navigated to related page by click");
		}
		logger.info("Verified the navigation of url related page");
		driver.close();
		logger.info("Closed the page");
		driver.switchTo().window(tabL1.get(0));
		logger.info("Closed the page");
		waitAndLog(2);
		Click_on_Tools();
		Click_on_Reviews();
		button(reviews.link1_manage_button, "click on manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();
		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		waitAndLog(2);

	}

	// functionality of link"Learn how to set up your review sites"

	public void functionality_of_link_in_create_reviews_page() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		link(reviews.Link_LearnHowToSetUpYourReviewSites, "click on link").click();
		logger.info("Clicked on link");
		waitAndLog(2);

		ArrayList<String> tabL1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabL1.get(1));
		String Servicetext = button(reviews.Features_button_in_link_page, "Features  button is enabled").getText();
		if (!(Servicetext.isEmpty())) {
			button(reviews.Features_button_in_link_page, "verify Features button").getText();
			waitAndLog(2);
			logger.info("Features link button is enable");
		}
		logger.info("Verified the related link buttons are enable in link related page");
		driver.close();
		logger.info("Closed the page");
		driver.switchTo().window(tabL1.get(0));
		logger.info("Switched to previous page");
		waitAndLog(2);
		logger.info("wit for 5 sec");

	}

	// functionality of link"Learn how to set up your review sites for edit page"

	public void functionality_of_link_in_Edit_reviews_page() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("link");
		logger.info("Entered data into Add Site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("www.facebook.com");
		logger.info("Entered data into link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.link_manage_button, "click on manage button").click();
		logger.info("Clicked on manage button");
		waitAndLog(2);

		link(reviews.Link_LearnHowToSetUpYourReviewSites, "click on link").click();
		logger.info("Clicked on link");
		waitAndLog(2);

		ArrayList<String> tabL1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabL1.get(1));
		String Servicetext = button(reviews.Features_button_in_link_page, "Features  button is enabled").getText();
		if (!(Servicetext.isEmpty())) {
			button(reviews.Features_button_in_link_page, "verify Features button").getText();
			waitAndLog(2);
			logger.info("Features link button is enable");
		}
		driver.close();
		driver.switchTo().window(tabL1.get(0));
		waitAndLog(2);

		ManagePage_Delete_Method();
	}

	// verify title ,toagglebutton,manage button is available in list

	public void Title_Toagle_Manage_buttons_Isavailable() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("records");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("www.facebook.com");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		reviews.records_title_under_list.isEnabled();
		waitAndLog(2);
		logger.info("records title is available in the list");
		if (verifyElementIsEnabled(reviews.Review_Manage_button)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.records_title_under_list));
			button(reviews.records_title_under_list, "Manage button").isEnabled();
			logger.info("records title review record is available");
			TestUtil.takeScreenshotAtEndOfTest("records title review record is available case is passed");// ScreenShot
																											// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.records_title_under_list));
			logger.info("records title review record is not available");
			TestUtil.takeScreenshotAtEndOfTest("records title review record is available case is faild");// ScreenShot
																											// capture
		}

		button(reviews.records_toaggle_button, "verify button is enabled").isEnabled();
		if (verifyElementIsEnabled(reviews.records_toaggle_button)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.records_toaggle_button));
			button(reviews.records_toaggle_button, "toaggle button").isEnabled();
			logger.info("records toaggle button of review record is enabled");
			TestUtil.takeScreenshotAtEndOfTest("records toaggle button of review record is enabled case is passed");// ScreenShot
																													// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.records_toaggle_button));
			logger.info("records toaggle button of review record is disabled");
			TestUtil.takeScreenshotAtEndOfTest("records toaggle button of review record is enabled case is faild");// ScreenShot
																													// capture
		}

		button(reviews.records_manage_button, "verify button is enabled").isEnabled();
		if (verifyElementIsEnabled(reviews.records_manage_button)) {

			Assert.assertEquals(true, verifyElementIsEnabled(reviews.records_manage_button));
			button(reviews.records_manage_button, "Manage button").isEnabled();
			logger.info("records manage button of review record is enabled");
			TestUtil.takeScreenshotAtEndOfTest("records manage button of review record is enabled case is passed");// ScreenShot
																													// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.records_manage_button));
			logger.info("records manage button of review record is disabled");
			TestUtil.takeScreenshotAtEndOfTest("records manage button of review record is enabled case is faild");// ScreenShot
																													// capture
		}
		logger.info("Verified all fields");
		button(reviews.records_manage_button, "click on manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();

	}

	public void functionality_of_review_icon_without_active_records() throws Exception {

		waitAndLog(2);
		button(reviews.qa10, "select automation under conversation list").click();
		logger.info("Selected contact under conversation list");
		waitAndLog(2);

		button(reviews.Review_icon_under_message_chat_box, "click on review icon").click();
		logger.info("Clickedon review icon");
		waitAndLog(2);

		reviews.Reviewes_NoRecords_TextMessage.getText();

		logger.info(reviews.Reviewes_NoRecords_TextMessage.getText());

		if (reviews.Reviewes_NoRecords_TextMessage.getText().equalsIgnoreCase("Create your first review.")) {
			Assert.assertEquals(reviews.Reviewes_NoRecords_TextMessage.getText(), "Create your first review.",
					"Create your first review.");
			logger.info("Reviewes page Create your first review text message testcase is working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"Reviewes page Create your first review. text message  verifying case is passed");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(reviews.Reviewes_NoRecords_TextMessage.getText(), " Create your first review.",
					"Create your first review.");
			logger.info("Reviewes page Create your first review text message testcase is not working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"Reviewes page Create your first review. text message  verifying case is failed ");// ScreenShot
																										// capture
		}

		logger.info("Verified text message 'Create your first review.' for no records");
		waitAndLog(2);
		button(reviews.tools_page_close_icon, "click on close iconin top of the tools page").click();
		waitAndLog(2);
	}

	// TC-238

	public void selectAll_toggle_should_Turn_off_by_click_on_any_toggle_button() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("link5");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("https://google.com/");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		waitAndLog(2);
		button(reviews.link5_toggle_button, "click on toggle button").click();
		logger.info("Clicked on toggle button");
		waitAndLog(2);

		reviews.Select_All_toggle_off_button.isEnabled();
		waitAndLog(2);
		if (verifyElementIsEnabled(reviews.Select_All_toggle_off_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(reviews.Select_All_toggle_off_button));
			button(reviews.Select_All_toggle_off_button, "verify button").isEnabled();
			logger.info("Select All toggle button is default disable and off");
			TestUtil.takeScreenshotAtEndOfTest("Select All toggle button is default disable and off case is passed");// ScreenShot
																														// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(reviews.Select_All_toggle_off_button));
			logger.info("Select All toggle button is default enables and on");
			TestUtil.takeScreenshotAtEndOfTest("Select All toggle button is default disabled and off case is faild");// ScreenShot
																														// capture
		}
		logger.info("Verified Select All toggle button is default on");
		button(reviews.link5_toggle_button, "click on toggle button").click();
		logger.info("Clicked on toggle button");
		waitAndLog(2);

		button(reviews.link5_manage_button, "click on link5 manage button").click();
		logger.info("Clicked on manage button");
		ManagePage_Delete_Method();

	}

	// TC-220
	public void verify_All_options_in_SelectSite_DD_220() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		String arr[] = { "Google", "Facebook", "TripAdvisor", "Yelp", "Others" };
		Select select = new Select(driver.findElement(By.xpath("//select[contains(@id,'definedTitle')]")));
		List<WebElement> allOptions = select.getOptions();
		logger.info(allOptions.size());

		for (int i = 0; i < allOptions.size(); i++) {

			Assert.assertEquals(arr[i], allOptions.get(i).getText());

		}

		logger.info("Verified all the options in the select site DD list");
		logger.info("Verified all options in Select site DD");
	}

	// TC-243

	public void verify_All_options_in_SelectSite_DD_Edit_case() throws Exception {

		Click_on_Reviews();
		Click_on_AddReviewSite();
		Click_on_SelectSite_DD();

		button(reviews.Review_SelectSite_DD, "Click on Review_SelectSite_DD").click();
		waitAndLog(2);

		button(reviews.Reviews_Site_Selec_Others, "Click Reviews_Site_Selec_Others").click();
		waitAndLog(2);

		textbox(reviews.Create_Review_Add_Site).sendKeys("like");
		logger.info("Entered data into Add site field");
		waitAndLog(2);

		textbox(reviews.Create_Review_Link_textbox).sendKeys("https://google.com/");
		logger.info("Entered data into Link field");
		waitAndLog(2);

		CRP_Click_on_Create_button();
		button(reviews.like_manage_button, "click on like related manage button").click();
		logger.info("clicked on manage button");
		waitAndLog(2);

		String arr[] = { "Google", "Facebook", "TripAdvisor", "Yelp", "Others", "like" };
		Select select = new Select(driver.findElement(By.xpath("//select[contains(@id,'definedTitle')]")));
		List<WebElement> allOptions1 = select.getOptions();
		logger.info(allOptions1.size());
		logger.info("Get size of the list in DD");

		for (int i = 0; i < allOptions1.size(); i++) {

			Assert.assertEquals(arr[i], allOptions1.get(i).getText());

		}

		logger.info("Verified all the options in the select site DD list");
		logger.info("verified all options in Select site DD");
		ManagePage_Delete_Method();
	}

}

// Reviews Page ends.
