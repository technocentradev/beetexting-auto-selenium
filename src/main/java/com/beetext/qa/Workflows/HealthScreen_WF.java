package com.beetext.qa.Workflows;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.HealthscreenWFCM;
import com.beetext.qa.pages.HealthScreen_pages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Settings_Payment_Pages;
import com.beetext.qa.util.TestUtil;

public class HealthScreen_WF extends HealthscreenWFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	HealthScreen_pages healthScreen_pages = new HealthScreen_pages();
	Settings_Payment_Pages settings_Payment_Pages = new Settings_Payment_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void tools_HealthScreen_Enabled(Map<String, String> map) throws Exception {

		if (verifyElementIsEnabled(healthScreen_pages.healthscreen)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.healthscreen));
			textbox(healthScreen_pages.healthscreen).isEnabled();
			logger.info("Health Screenings is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.healthscreen));
			logger.info("Health Screenings is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_options(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.recurring)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.recurring));
			textbox(healthScreen_pages.recurring).isEnabled();
			logger.info("Health Screenings Recurring is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.recurring));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(healthScreen_pages.upcoming)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.upcoming));
			textbox(healthScreen_pages.upcoming).isEnabled();
			logger.info("Health Screenings upcoming is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.upcoming));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(healthScreen_pages.past)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.past));
			textbox(healthScreen_pages.past).isEnabled();
			logger.info("Health Screenings past is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.past));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_Create_HealthScreen_Enabled(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.create_HealthScreen)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_HealthScreen));
			textbox(healthScreen_pages.create_HealthScreen).isEnabled();
			logger.info("create_HealthScreen is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_HealthScreen));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_preview_settings_create(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.view_preview)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.view_preview));
			textbox(healthScreen_pages.view_preview).isEnabled();
			logger.info("Health Screenings view_preview is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.view_preview));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(healthScreen_pages.settings)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.settings));
			textbox(healthScreen_pages.settings).isEnabled();
			logger.info("Health Screenings settings is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.settings));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(healthScreen_pages.create_HealthScreen)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_HealthScreen));
			textbox(healthScreen_pages.create_HealthScreen).isEnabled();
			logger.info("Health Screenings create HealthScreen is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_HealthScreen));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_preview(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.view_preview, "view_preview").click();
		logger.info("click on view_preview Button");
		wait(5);
		logger.info("wait for 5 sec");

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));
		String Servicetext = link(Repository.Pricing, "Pricing").getText();
		if (!(Servicetext.isEmpty())) {
			link(Repository.Pricing, "Pricing").click();
		}
		driver.close();
		driver.switchTo().window(tabs1.get(0));
	}

	public void tools_HealthScreen_settings(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.settings, "settings").click();
		logger.info("click on settings Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.default_supporting_department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.default_supporting_department));
			textbox(healthScreen_pages.default_supporting_department).isEnabled();
			logger.info("Default_supporting_department is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.default_supporting_department));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_settings_update(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.settings, "settings").click();
		logger.info("click on settings Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.update)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.update));
			textbox(healthScreen_pages.update).isEnabled();
			logger.info("Update");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.update));
			logger.info("update Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_settings_viewPreview_copytext(Map<String, String> map) throws Exception {
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.settings, "settings").click();
		logger.info("click on settings Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.view_preview)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.view_preview));
			textbox(healthScreen_pages.view_preview).isEnabled();
			logger.info("Health Screenings view_preview is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.view_preview));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(healthScreen_pages.copytext)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.copytext));
			textbox(healthScreen_pages.copytext).isEnabled();
			logger.info("Copytext is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.copytext));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HS_Settings_CopyText_PasteText(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(healthScreen_pages.settings, "settings").click();
		logger.info("click on settings Button");
		waits(3);
		logger.info("wait for 3 sec");

		button(healthScreen_pages.copytext, "copytext").click();
		logger.info("click on copytext Button");
		waits(5);
		logger.info("wait for 3 sec");

		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("userid" + username1);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login bttn");
		waits(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Dept_Selection, "click on Dept_Selection").click();
		logger.info("click on Dept_Selection Button");

		button(healthScreen_pages.test_Dept_Selection, "click on test_Dept_Selection").click();
		logger.info("click on test_Dept_Selection Button");
		waits(5);
		logger.info("wait for 5 sec");

		WebElement text = repository.chatInputTextareaTxt;
		text.sendKeys(Keys.SHIFT, Keys.INSERT);
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HealthScreen_supporting_Dept_HealthScreen(Map<String, String> map) throws Exception {

		oneAgentLogin(map);
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.settings, "settings").click();
		logger.info("click on settings Button");
		wait(5);
		logger.info("wait for 5 sec");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Health Screen")) {

				allOptions1.get(i).click();
				break;

			}
		}
		closetoolspopups();
		logout();
	}

	public void tools_HealthScreen_recurring_Message(Map<String, String> map) throws Exception {
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("userid" + username1);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login bttn");
		waits(5);
		logger.info("wait for 5 sec");
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.recurring, "settings").click();
		logger.info("click on recurring Button");
		wait(5);
		logger.info("wait for 5 sec");

		healthScreen_pages.recurring_Msg.getText();
		logger.info(healthScreen_pages.recurring_Msg.getText());
		if (healthScreen_pages.recurring_Msg.getText()
				.equalsIgnoreCase("All your recurring health screenings will appear here.")) {
			Assert.assertEquals(healthScreen_pages.recurring_Msg.getText(),
					"All your recurring health screenings will appear here.", "recurring_Msg is matching");
			TestUtil.takeScreenshotAtEndOfTest("recurring_MsgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.recurring_Msg.getText(),
					"All your recurring health screenings will appear here.", "recurring_Msg is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("recurring_MsgTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_upcoming_Message(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");
		wait(5);
		logger.info("wait for 5 sec");

		healthScreen_pages.upcoming_Msg.getText();
		logger.info(healthScreen_pages.upcoming_Msg.getText());
		if (healthScreen_pages.upcoming_Msg.getText()
				.equalsIgnoreCase("All your upcoming health screenings will appear here.")) {
			Assert.assertEquals(healthScreen_pages.upcoming_Msg.getText(),
					"All your upcoming health screenings will appear here.", "upcoming_Msg is matching");
			TestUtil.takeScreenshotAtEndOfTest("upcoming_MsgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.upcoming_Msg.getText(),
					"All your upcoming health screenings will appear here.", "upcoming_Msg is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("upcoming_MsgTestCase");// ScreenShot capture
		}
	}

	public void tools_HealthScreen_past_Message(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past, "settings").click();
		logger.info("click on past Button");
		wait(5);
		logger.info("wait for 5 sec");

		healthScreen_pages.past_msg.getText();
		logger.info(healthScreen_pages.past_msg.getText());
		if (healthScreen_pages.past_msg.getText()
				.equalsIgnoreCase("All your past health screenings will appear here.")) {
			Assert.assertEquals(healthScreen_pages.past_msg.getText(),
					"All your past health screenings will appear here.", "past_msg is matching");
			TestUtil.takeScreenshotAtEndOfTest("past_msgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.upcoming_Msg.getText(),
					"All your past health screenings will appear here.", "past_msg is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("past_msgTestCase");// ScreenShot capture
		}
		closetoolspopups();
		logout();
	}

	public void tools_HealthScreen_send_Screen_Enabled(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.Send_Screening)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.Send_Screening));
			textbox(healthScreen_pages.Send_Screening).isEnabled();
			logger.info("Send_Screening is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.Send_Screening));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_sendScreen(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");
		wait(3);
		logger.info("wait for 3 sec");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_ScheduleLater_Manage_Send_Scree(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();

		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

		wait(3);
		logger.info("wait for 3 sec");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Doesn't Exist");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");

		button(healthScreen_pages.upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		button(healthScreen_pages.sendNow, "button").click();
		logger.info("click on sendNow button");
		wait(3);
		logger.info("wait for 3 sec");

	}

	public void tools_HealthScreen_upcoming_manage(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();

		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

		waits(5);
		logger.info("wait for 3 sec");

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.upcoming_manage_delete)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.upcoming_manage_delete));
			textbox(healthScreen_pages.upcoming_manage_delete).isEnabled();
			logger.info("Delete HS is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.upcoming_manage_delete));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(healthScreen_pages.upcoming_manage_delete, "button").click();
		logger.info("click on Upcoming manage delete");
		wait(5);
		logger.info("wait for 5 sec");
		button(healthScreen_pages.upcoming_manage_delete, "button").click();
		logger.info("click on Upcoming manage delete");
		wait(3);
		logger.info("wait for 3 sec");

	}

	public void tools_HealthScreen_Edit_HS_Schedule_HSUpdated(Map<String, String> map) throws Exception {

		waits(5);
		logger.info("wait for 5 sec");
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(healthScreen_pages.create_recurring, "button").click();
		logger.info("click on create_recurring button");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.recurring_appDateTime_End, "button").click();
		logger.info("click on recurring_EndDate");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.time_hr_upArrow, "button").click();
		logger.info("click on time_hr_upArrow");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(3);
		button(healthScreen_pages.recurring, "recurring").click();
		logger.info("click on recurring Button");
		waits(3);
		logger.info("wait for 3 sec");

		button(healthScreen_pages.upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_recurring, "button").click();
		logger.info("click on create_recurring button");

		button(healthScreen_pages.ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();

		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		wait(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
		wait(3);
		logger.info("wait for 3 sec");

//		System.out.print(healthScreen_pages.create_toaster.getText());
//
//		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
//			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
//			textbox(healthScreen_pages.create_toaster).isEnabled();
//			logger.info("your health screen request has been successfully updated");
//			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
//
//		} else {
//			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
//			logger.info("Disabled");
//			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
//		}
	}

	public void tools_HealthScreen_upcoming_reuse(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_Reuse, "button").click();
		logger.info("click on upcoming_Reuse button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse_cancel)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse_cancel));
			textbox(healthScreen_pages.upcoming_Reuse_cancel).isEnabled();
			logger.info("Cancel is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse_cancel));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_Reuse_HS_Schedule_HSUpdated(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");
		wait(3);
		logger.info("wait for 3 sec");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_Reuse, "button").click();
		logger.info("click on upcoming Reuse button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();

		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
		wait(3);
		logger.info("wait for 3 sec");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(3);
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_manage_delete, "button").click();
		logger.info("click on Upcoming manage delete");
		wait(5);
		logger.info("wait for 5 sec");
		button(healthScreen_pages.upcoming_manage_delete, "button").click();
		logger.info("click on Upcoming manage delete");
		wait(3);
		logger.info("wait for 3 sec");
	}

	public void tools_HealthScreen_upcoming_manage_delete(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();
		button(healthScreen_pages.time_upArrow, "button").click();

		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

		waits(5);
		logger.info("wait for 3 sec");

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_manage, "button").click();
		logger.info("click on upcoming manage button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_manage_delete, "button").click();
		logger.info("click on upcoming_manage_delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_manage_delete, "button").click();
		logger.info("click on upcoming_manage_delete button");
		wait(3);
		logger.info("wait for 3 sec");

		String text = healthScreen_pages.HS_Delete_Error_Toaster.getText();
		System.out.print(text);

		if (verifyElementIsEnabled(healthScreen_pages.HS_Delete_Error_Toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.HS_Delete_Error_Toaster));
			textbox(healthScreen_pages.HS_Delete_Error_Toaster).isEnabled();
			logger.info(text);
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.HS_Delete_Error_Toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_past_(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming, "settings").click();
		logger.info("click on upcoming Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.upcoming_Reuse, "button").click();
		logger.info("click on upcoming_Reuse button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse_cancel)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse_cancel));
			textbox(healthScreen_pages.upcoming_Reuse_cancel).isEnabled();
			logger.info("Cancel is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse_cancel));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_past_list(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(healthScreen_pages.past, "past").click();
		logger.info("click on past Button");
		waits(2);
		logger.info("wait for 2 sec");

		String title = healthScreen_pages.HS_Past_list_title.getText();
		String DateAndTime = healthScreen_pages.HS_Past_list_DateTime.getText();
		logger.info(title + " " + DateAndTime);

		String title1 = healthScreen_pages.HS_Past_list_title1.getText();
		String DateAndTime1 = healthScreen_pages.HS_Past_list_DateTime1.getText();
		logger.info(title1 + " " + DateAndTime1);

		String title2 = healthScreen_pages.HS_Past_list_title2.getText();
		String DateAndTime2 = healthScreen_pages.HS_Past_list_DateTime2.getText();
		logger.info(title2 + " " + DateAndTime2);

		String title3 = healthScreen_pages.HS_Past_list_title3.getText();
		String DateAndTime3 = healthScreen_pages.HS_Past_list_DateTime3.getText();
		logger.info(title3 + " " + DateAndTime3);

		String title4 = healthScreen_pages.HS_Past_list_title4.getText();
		String DateAndTime4 = healthScreen_pages.HS_Past_list_DateTime4.getText();
		logger.info(title4 + " " + DateAndTime4);

		String title5 = healthScreen_pages.HS_Past_list_title5.getText();
		String DateAndTime5 = healthScreen_pages.HS_Past_list_DateTime5.getText();
		logger.info(title5 + " " + DateAndTime5);

		String title6 = healthScreen_pages.HS_Past_list_title6.getText();
		String DateAndTime6 = healthScreen_pages.HS_Past_list_DateTime6.getText();
		logger.info(title6 + " " + DateAndTime6);

	}

	public void tools_HS_Past_view(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past, "past").click();
		logger.info("click on past Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past, "past").click();
		logger.info("click on past Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past_view, "past_view").click();
		logger.info("click on past_view Button");
		waits(2);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.viewrecipients)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			textbox(healthScreen_pages.viewrecipients).isEnabled();
			logger.info("View Recipients is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			logger.info("View Recipients is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HS_Past_Reuse(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past, "past").click();
		logger.info("click on past Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past, "past").click();
		logger.info("click on past Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past_Reuse, "past_Reuse").click();
		logger.info("click on past_Reuse Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.create_title).clear();

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void tools_HS_Past_Reuse_SendHS(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past, "past").click();
		logger.info("click on past Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.past_Reuse, "past_Reuse").click();
		logger.info("click on past_Reuse Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.create_title).clear();

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waits(2);
		logger.info("wait for 2 sec");

		button(healthScreen_pages.sendNow, "button").click();
		logger.info("click on sendNow button");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_Hs_Allows_one_one_Group_Converstion(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("5550498943");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact_Individual, "button").click();
		logger.info("click on Searchcontact_Individual");
		waits(2);
		logger.info("wait for 5 sec");

		System.out.print("Health Screenings allows one to one Conversatio");

		String arr[] = { "555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7) };

		for (int i = 0; i <= 11; i++) {

			textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys(arr[i]);
			logger.info("Enter data in Individual Recipient ");
			waits(2);
			logger.info("wait for 5 sec");

			button(healthScreen_pages.Searchcontact_Individual, "button").click();
			logger.info("click on Searchcontact_Individual");
			waits(2);
			logger.info("wait for 5 sec");

			System.out.print("Health Screenings allows Group Conversation");

			System.out.print("Health Screenings allows both one-one and Group Conversation");

		}
	}

	public void tools_HealthScreen_recurring_manage_reuse(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_recurring, "button").click();
		logger.info("click on create_recurring button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");
		wait(3);
		logger.info("wait for 3 sec");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(healthScreen_pages.recurring, "recurring").click();
		logger.info("click on recurring Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.upcoming_manage)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.upcoming_manage));
			textbox(healthScreen_pages.upcoming_manage).isEnabled();
			logger.info("Manage is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.upcoming_manage));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse));
			textbox(healthScreen_pages.upcoming_Reuse).isEnabled();
			logger.info("Reuse is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.upcoming_Reuse));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(healthScreen_pages.upcoming_manage, "upcoming_manage").click();
		logger.info("click on upcoming_manage Button");

		button(healthScreen_pages.upcoming_manage_delete, "upcoming_manage_delete").click();
		logger.info("click on upcoming_manage_delete Button");

		button(healthScreen_pages.upcoming_manage_delete, "upcoming_manage_delete").click();
		logger.info("click on upcoming_manage_delete Button");
	}

	public void tools_HealthScreen_tags_list(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		String tag = healthScreen_pages.SearchTag1.getText();
		System.out.print("Tag -->" + tag);

		String tag1 = healthScreen_pages.SearchTag2.getText();
		System.out.print("Tag1 -->" + tag1);

		String tag2 = healthScreen_pages.SearchTag3.getText();
		System.out.print("Tag2 -->" + tag2);

	}

	public void tools_HS_Allows_Optout_feature(Map<String, String> map) throws Exception {
		automationsLogin(map);

		button(healthScreen_pages.HS_Cont, "click on HS Contact").click();
		logger.info("click on HS_Cont Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Stop");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		logger.info("wait for 3 sec");

		oneAgentLogin(map);

		button(healthScreen_pages.HSTech_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(repository.contact_unblock)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.contact_unblock));
			textbox(repository.contact_unblock).isEnabled();
			logger.info(repository.contact_unblock.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.contact_unblock));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();

		automationsLogin(map);

		button(healthScreen_pages.HS_Cont, "click on HS Contact").click();
		logger.info("click on HS_Cont Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("start");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");
		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HealthScreen_addtag_deleteTag(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.tag_crossmark, "button").click();
		logger.info("click on tag_crossmark");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void tools_HealthScreen_addtag_receipentCount(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		wait(5);
		logger.info("wait for 5 sec");

		String recipientCount = healthScreen_pages.recipentCount.getText();
		System.out.print("Recipient Count--->" + recipientCount);
	}

	public void tools_HealthScreen_Notag_receipentdisable(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.viewrecipients)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			textbox(healthScreen_pages.viewrecipients).isEnabled();
			logger.info("View Recipients is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			logger.info("View Recipients is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_HealthScreen_addtag_viewRecipientsList(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.viewrecipients, "button").click();
		logger.info("click on View Recipients");
		waits(2);
		logger.info("wait for 5 sec");

		String recipientlist = healthScreen_pages.recipients_list.getText();
		System.out.print("Recipient list--->" + recipientlist);
	}

	public void tools_HealthScreen_addtag_viewRecipientsList_ClickBack(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.viewrecipients, "button").click();
		logger.info("click on View Recipients");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.viewrecipients_back, "button").click();
		logger.info("click on Back");
		waits(2);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.viewrecipients)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			textbox(healthScreen_pages.viewrecipients).isEnabled();
			logger.info("View Recipients is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_HealthScreen_removetag_receipentdisable(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.tag_crossmark, "button").click();
		logger.info("click on tag_crossmark");
		waits(2);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.viewrecipients)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			textbox(healthScreen_pages.viewrecipients).isEnabled();
			logger.info("View Recipients is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.viewrecipients));
			logger.info("View Recipients is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_HealthScreen_addIndividualRecienpent(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		driver.findElement(By.xpath("//span[contains(text(),' Remove individual recipients')]")).click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");
		wait(3);
		logger.info("wait for 3 sec");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_HealthScreen_addIndividualRecienpent_Count(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		String recipientCount = healthScreen_pages.recipentCount.getText();
		System.out.print("Recipient Count--->" + recipientCount);
	}

	public void tools_HealthScreen_addTag_Individual_Number_Recienpent_Count(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waits(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		String recipientCount = healthScreen_pages.recipentCount.getText();
		System.out.print("Recipient Count--->" + recipientCount);
		waits(2);
		button(healthScreen_pages.viewrecipients, "button").click();
		logger.info("click on viewrecipients");
		waits(2);
		logger.info("wait for 5 sec");
	}

	public void tools_HealthScreen_Recurring_dateTime(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_recurring, "button").click();
		logger.info("click on create_recurring");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.recurring_EndDate, "button").click();
		logger.info("click on recurring_EndDate");
		waits(2);
		logger.info("wait for 5 sec");

		String Start_DateandTime = healthScreen_pages.recurring_appDateTime_Start.getAttribute("value");
		logger.info(" Start Date and Time --->" + Start_DateandTime);

		String End_DateandTime = healthScreen_pages.recurring_appDateTime_End.getAttribute("value");
		logger.info(" End Date and Time --->" + End_DateandTime);

		String What_Time = healthScreen_pages.recurring_appDateTime_WhatTime.getAttribute("value");
		logger.info(" End Date and Time --->" + What_Time);

	}

	public void tools_HealthScreen_Send_Schedule(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String send_screening = healthScreen_pages.Send_Screening.getText();
		logger.info(" Text --->" + send_screening);

		button(healthScreen_pages.ScheduleLater, "button").click();
		logger.info("click on ScheduleLater");
		waits(2);
		logger.info("wait for 5 sec");

		String Schedule_screening = healthScreen_pages.Schedule_BroadCast.getText();
		logger.info(" Text --->" + Schedule_screening);

	}

	public void tools_HealthScreen_ScheduleHS(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.ScheduleLater, "button").click();
		logger.info("click on ScheduleLater");
		wait(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
		wait(5);
		logger.info("wait for 5 sec");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		/*
		 * closetoolspopups(); logout();
		 */
	}

	public void tools_HS_recurring_Manage_Save(Map<String, String> map) throws Exception {
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		button(healthScreen_pages.create_recurring, "button").click();
		logger.info("click on create_recurring button");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(2);
		button(healthScreen_pages.recurring, "settings").click();
		logger.info("click on recurring Button");

		button(healthScreen_pages.upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		button(healthScreen_pages.create_recurring, "button").click();
		logger.info("click on create_recurring button");

		button(healthScreen_pages.sendNow, "button").click();
		logger.info("click on sendNow button");

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

//		System.out.print(healthScreen_pages.create_toaster.getText());
//
//		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
//			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
//			textbox(healthScreen_pages.create_toaster).isEnabled();
//			logger.info(healthScreen_pages.create_toaster.getText());
//			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
//
//		} else {
//			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
//			logger.info("Disabled");
//			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
//		}
	}

	public void tools_HealthScreen_view_Preview(Map<String, String> map) throws Exception {

		// button(repository.tools_bttn, "healthscreen").click();
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.view_preview, "create_HealthScreen").click();
		logger.info("click on view_preview Button");
		waits(2);
		logger.info("wait for 5 sec");

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));
		driver.findElement(By.xpath("//button[@id='simple-banner-close-button']")).click();
		waits(5);
		String text = link(healthScreen_pages.view_preview_Features, "view_preview_Features").getText();
		if (!(text.isEmpty())) {
			link(healthScreen_pages.view_preview_Features, "view_preview_Features").click();
		}
		driver.close();
		driver.switchTo().window(tabs1.get(0));

	}

	public void tools_HealthScreen_activate_back(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 2 sec");

		logger.info(healthScreen_pages.Activate_HS_Page_text1.getText());

		logger.info(healthScreen_pages.Activate_HS_Page_text2.getText() + ""
				+ healthScreen_pages.Activate_HS_Page_text3.getText());

		button(healthScreen_pages.healthscreen_activate, "healthscreen").click();
		logger.info("click on healthscreen_activate Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.activate_back, "healthscreen").click();
		logger.info("click on activate_back Button");
		waits(2);
		logger.info("wait for 5 sec");
	}

	public void tools_HealthScreen_tag_Contact_separeted(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waits(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		String tag = healthScreen_pages.tag_text.getText();
		System.out.print("Tag --->" + tag);

		String contact = healthScreen_pages.Msg_ToSend_text.getText();
		System.out.print("contact ---->" + contact);

	}

	public void tools_HealthScreen_tag(Map<String, String> map) throws Exception {

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waits(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("555");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		closetoolspopups();
		logout();
	}

	public void tools_HealthScreen_Button_inChat(Map<String, String> map) throws Exception {

		textbox(Repository.user_id).sendKeys("AgentAutomation@yopmail.com");
		logger.info("Email id :" + username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		wait(10);
		logger.info("wait for 10 sec");

		if (verifyElementIsEnabled(healthScreen_pages.healthscreen_inChat)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.healthscreen_inChat));
			textbox(healthScreen_pages.healthscreen_inChat).isEnabled();
			logger.info("Healthscreen in Chat is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.healthscreen_inChat));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(2);
		button(healthScreen_pages.healthscreen_inChat, "click").click();
		logout();

	}

	public void tools_HS_actionBttn_Disabled(Map<String, String> map) throws Exception {

		oneAgentLogin(map);

		button(healthScreen_pages.Dept_Selection, "click on Dept_Selection").click();
		logger.info("click on Dept_Selection Button");

		button(healthScreen_pages.HS_Dept_Selection, "click on HS_Dept_Selection").click();
		logger.info("click on HS_Dept_Selection Button");
		waits(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.actionBttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.actionBttn));
			button(healthScreen_pages.actionBttn, "button").isEnabled();
			logger.info("Action Button is Disabled");
			logger.info("Filters & resolved option are not allowed in health screen department");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.actionBttn));
			logger.info("Health Screenings is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		logout();

	}

	public void tools_HealthScreen_Create_yes(Map<String, String> map) throws Exception {
		oneAgentLogin(map);

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(3);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(3);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Yes");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "We look forward to seeing you! In order to have your presence approved during this pandemic, please complete a health screening. To start your screening, please reply BEGIN or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Begin");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg3 = "Thank you for taking this pandemic screening. Please reply with your name.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg3)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is not  matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS - Technocentra");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg4 = "Automation, Have you experienced any of the following symptoms in the past 48 hours: fever or chills, cough, shortness of breath or difficulty breathing, fatigue, muscle or body aches, headache, new loss of taste or smell, sore throat, congestion or runny nose, nausea or vomiting, diarrhea? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg4)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("yes");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg6 = healthScreen_pages.HS_Msg.getText();

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg6)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HS_Create_NO(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		logger.info(healthScreen_pages.HS_Msg.getText());
		button(repository.tools_bttn, "healthscreen").click();
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(3);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		logger.info(healthScreen_pages.HS_Msg.getText());
		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HealthScreen_Create_48Hrs_NO(Map<String, String> map) throws Exception {

		oneAgentLogin(map);
		button(repository.tools_bttn, "healthscreen").click();
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(3);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Yes");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "We look forward to seeing you! In order to have your presence approved during this pandemic, please complete a health screening. To start your screening, please reply BEGIN or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Begin");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg3 = "Thank you for taking this pandemic screening. Please reply with your name.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg3)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS - Technocentra");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg4 = "Automation, Have you experienced any of the following symptoms in the past 48 hours: fever or chills, cough, shortness of breath or difficulty breathing, fatigue, muscle or body aches, headache, new loss of taste or smell, sore throat, congestion or runny nose, nausea or vomiting, diarrhea? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg4)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg5 = "Within the past 14 days, have you been in close physical contact (6 feet or closer for a cumulative total of 15 minutes) with either; Anyone who is known to have laboratory-confirmed COVID-19 or; Anyone who has any symptoms consistent with COVID-19? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg5)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("YES");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg6 = healthScreen_pages.HS_Msg.getText();

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg6)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HealthScreen_Create_48Hrs_14Days_NO(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		button(repository.tools_bttn, "button").click();
		logger.info("click on cross button");
		wait(5);
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(3);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Yes");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "We look forward to seeing you! In order to have your presence approved during this pandemic, please complete a health screening. To start your screening, please reply BEGIN or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Begin");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg3 = "Thank you for taking this pandemic screening. Please reply with your name.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg3)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS - Technocentra");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg4 = "Automation, Have you experienced any of the following symptoms in the past 48 hours: fever or chills, cough, shortness of breath or difficulty breathing, fatigue, muscle or body aches, headache, new loss of taste or smell, sore throat, congestion or runny nose, nausea or vomiting, diarrhea? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg4)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg5 = "Within the past 14 days, have you been in close physical contact (6 feet or closer for a cumulative total of 15 minutes) with either; Anyone who is known to have laboratory-confirmed COVID-19 or; Anyone who has any symptoms consistent with COVID-19? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg5)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg6 = "Are you isolating or quarantining because you may have been exposed to a person with COVID-19 or are worried that you may be sick with COVID-19? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg6)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("YES");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg7 = healthScreen_pages.HS_Msg.getText();

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg7)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg7, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg7, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HS_waiting_For_CovidResults(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		button(repository.tools_bttn, "button").click();
		logger.info("click on cross button");
		wait(5);

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(3);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Yes");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "We look forward to seeing you! In order to have your presence approved during this pandemic, please complete a health screening. To start your screening, please reply BEGIN or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Begin");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg3 = "Thank you for taking this pandemic screening. Please reply with your name.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg3)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS - Technocentra");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg4 = "Automation, Have you experienced any of the following symptoms in the past 48 hours: fever or chills, cough, shortness of breath or difficulty breathing, fatigue, muscle or body aches, headache, new loss of taste or smell, sore throat, congestion or runny nose, nausea or vomiting, diarrhea? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg4)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg5 = "Within the past 14 days, have you been in close physical contact (6 feet or closer for a cumulative total of 15 minutes) with either; Anyone who is known to have laboratory-confirmed COVID-19 or; Anyone who has any symptoms consistent with COVID-19? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg5)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg6 = "Are you isolating or quarantining because you may have been exposed to a person with COVID-19 or are worried that you may be sick with COVID-19? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg6)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(3);
		logger.info("wait for 3 sec");

		String msg7 = "Are you currently waiting on the results of a COVID-19 test? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg7)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg7, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg7, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg8 = healthScreen_pages.HS_Msg.getText();

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg8)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg8, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg8, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HS_NO_Pass_Case(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		logger.info(healthScreen_pages.HS_Msg.getText());
		button(repository.tools_bttn, "button").click();
		logger.info("click on cross button");
		wait(5);
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(3);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		logger.info(healthScreen_pages.HS_Msg.getText());

		validateSignOutLink();
		validateSignOut();

	}

	public void nolimit_contacts(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		logger.info(healthScreen_pages.HS_Msg.getText());
		button(repository.tools_bttn, "button").click();
		logger.info("click on cross button");
		wait(5);
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		String arr[] = { "555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7) };

		for (int i = 0; i <= 11; i++) {

			textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys(arr[i]);
			logger.info("Enter data in Individual Recipient ");
			waits(2);
			logger.info("wait for 5 sec");

			button(healthScreen_pages.Searchcontact_Individual, "button").click();
			logger.info("click on Searchcontact_Individual");
			waits(2);
			logger.info("wait for 5 sec");

			System.out.print("There is no limit for adding n number of contacts.");

		}
		closetoolspopups();
		logout();
	}

	public void tools_HS_fail_Case(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		button(repository.tools_bttn, "button").click();
		logger.info("click on cross button");
		wait(5);
		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(5);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Yes");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "We look forward to seeing you! In order to have your presence approved during this pandemic, please complete a health screening. To start your screening, please reply BEGIN or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Begin");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg3 = "Thank you for taking this pandemic screening. Please reply with your name.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg3)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg3, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS - Technocentra");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg4 = "Automation, Have you experienced any of the following symptoms in the past 48 hours: fever or chills, cough, shortness of breath or difficulty breathing, fatigue, muscle or body aches, headache, new loss of taste or smell, sore throat, congestion or runny nose, nausea or vomiting, diarrhea? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg4)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg4, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg5 = "Within the past 14 days, have you been in close physical contact (6 feet or closer for a cumulative total of 15 minutes) with either; Anyone who is known to have laboratory-confirmed COVID-19 or; Anyone who has any symptoms consistent with COVID-19? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg5)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("YES");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg6 = healthScreen_pages.HS_Msg.getText();

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg6)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg6, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();

	}

	public void tools_HS_report_Pass_Case(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		logger.info(healthScreen_pages.HS_Msg.getText());

		button(repository.tools_bttn, "button").click();
		logger.info("click on cross button");
		wait(5);

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("auto");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Searchcontact1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());
		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		waits(3);
		auto1Login(map);
		waits(5);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);
		logger.info("click on login bttn");

		String msg1 = "Hello from onlyoneAgent. This is a dedicated automated text-only line to help us administer safety protocols during this pandemic. Do you have plans to visit us at our facility today? Please reply, YES, NO or HELP.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg1)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg1, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("NO");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		logger.info(healthScreen_pages.HS_Msg.getText());

		validateSignOutLink();
		validateSignOut();

		waits(3);

	}

	public void HS_Icon_TextMsg_InChatBox(Map<String, String> map) throws Exception {

		textbox(Repository.user_id).sendKeys("AgentAutomation@yopmail.com");
		logger.info("Email id :" + "AgentAutomation@yopmail.com");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.healthscreen_inChat)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.healthscreen_inChat));
			textbox(healthScreen_pages.healthscreen_inChat).isEnabled();
			logger.info("Healthscreen in Chat is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.healthscreen_inChat));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(2);
		button(healthScreen_pages.healthscreen_inChat, "click").click();
		waits(5);
		logger.info("wait for 2 sec");

		JSClick(repository.chatInputsendButton);
		waits(3);

		logger.info(healthScreen_pages.msg.getText());
		logout();

	}

	public void Send_Screen_3Invalid_answers(Map<String, String> map) throws Exception {

		auto1Login(map);
		waits(3);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose bttn");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("Health");
		logger.info("Enter data to search contact");
		waits(3);
		logger.info("wait for 3 sec");
		link(healthScreen_pages.searchcontact_HealthScreen, "Search output").click();
		logger.info("click on login bttn");
		waits(3);

		textbox(repository.chatInputTextareaTxt).sendKeys("Screen");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("abcdef");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("abcdef");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("abcdef");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("abcdef");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg5 = "This automated screening only accepts YES, NO or HELP as responses to questions. This screening has been ended due to a third unapproved response. To restart the screening please reply with SCREEN. If you have questions please call or text us at (555) 158-2508. Thank you.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg5)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg5, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
		// closetoolspopups();
		logout();
	}

	public void Hs_recurring_unselect_currentDate_Error(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_recurring, "button").click();
		logger.info("click on create_recurring button");
		waits(2);
		button(repository.tools_BM_create_recurring_appDateTime, "button").click();
		logger.info("click on recurring App Date and Time button");
		waits(2);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_recurring_appDateTime_DecreaseMonth, "button").click();
		logger.info("click on recurring App Date and Time Decrease Month button");
		waits(2);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_recurring_appDateTime_SelectDate, "button").click();
		logger.info("click on recurring Select App Date and Time button");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(repository.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");
		wait(5);
		logger.info("wait for 5 sec");

		healthScreen_pages.recurring_appDateTime_inFuture_errorMsg.getText();
		logger.info(healthScreen_pages.recurring_appDateTime_inFuture_errorMsg.getText());
		if (healthScreen_pages.recurring_appDateTime_inFuture_errorMsg.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(healthScreen_pages.recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date and time in the future.",
					"recurring_appDateTime_inFuture_errorMsg is matching");
			TestUtil.takeScreenshotAtEndOfTest("recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(healthScreen_pages.recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date and time in the future.",
					"recurring_appDateTime_inFuture_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		}
		closetoolspopups();
		logout();
	}

	public void Hs_wrongAnswer_Accepts_Yes_NO_help(Map<String, String> map) throws Exception {
		auto1Login(map);

		button(healthScreen_pages.HS_Cont, "HS_Cont").click();
		logger.info("click on HS_Cont Button");
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Screen");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS- Tech");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("Help");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "This automated screening only accepts YES or NO as responses to questions. If you have questions, please call or text us at (555) 158-2508. Thank you.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		textbox(repository.chatInputTextareaTxt).sendKeys("YES");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		logout();

	}

	public void Hs_AdhocRequest_secondMsg_reply_YourName(Map<String, String> map) throws Exception {

		auto1Login(map);

		button(healthScreen_pages.HS_Cont, "HS_Cont").click();
		logger.info("click on HS_Cont Button");
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Screen");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "We look forward to seeing you! Thank you for taking this pandemic screening. Please reply with your name.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS- Tech");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("YES");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		logout();

	}

	public void Hs_AdhocRequest_Respondwrong_Give_YourName(Map<String, String> map) throws Exception {
		automationsLogin(map);
		button(healthScreen_pages.HS_Cont, "HS_Cont").click();
		logger.info("click on HS_Cont Button");
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Screen");
		logger.info("enter Message - Yes");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("YES");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		String msg2 = "Please respond with NAME to continue your screening. You can also reply HELP for assistance. Thank you.";

		healthScreen_pages.HS_Msg.getText();
		logger.info(healthScreen_pages.HS_Msg.getText());
		if (healthScreen_pages.HS_Msg.getText().equalsIgnoreCase(msg2)) {
			Assert.assertEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is matching");
			logger.info(healthScreen_pages.HS_Msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(healthScreen_pages.HS_Msg.getText(), msg2, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("HS- Tech");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("YES");
		logger.info("enter Message - Begin");
		waits(2);
		logger.info("wait for 2 sec");
		JSClick(repository.chatInputsendButton);
		waits(3);
		logger.info("wait for 3 sec");
		// closetoolspopups();
		logout();

	}

	public void HS_Reports(Map<String, String> map) throws Exception {
		oneAgentLogin(map);
//		waits(5);
//		button(healthScreen_pages.Dept_Selection, "click on Dept_Selection").click();
//		logger.info("click on Dept_Selection Button");
//		waits(5);
//		button(healthScreen_pages.HS_Dept_Selection, "click on HS_Dept_Selection").click();
//		logger.info("click on HS_Dept_Selection Button");
		waits(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.HS_Reports, "HS_Reports").click();
		logger.info("click on HS_Reports Button");
		waits(3);
		logger.info("wait for 3 sec");

		logger.info(healthScreen_pages.HS_Msg.getText());
	}

	public void HS_System_Stamp(Map<String, String> map) throws Exception {

		button(healthScreen_pages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact Button");
		waits(3);
		logger.info("wait for 3 sec");

		logger.info(healthScreen_pages.System_Stamp.getText());

	}

	public void HS_Report_Contact(Map<String, String> map) throws Exception {

		button(healthScreen_pages.Dept_Selection, "click on Dept_Selection").click();
		logger.info("click on Dept_Selection Button");

		button(healthScreen_pages.HS_Dept_Selection, "click on HS_Dept_Selection").click();
		logger.info("click on HS_Dept_Selection Button");
		waits(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(healthScreen_pages.HS_Reports)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.HS_Reports));
			textbox(healthScreen_pages.HS_Reports).isEnabled();
			logger.info("Health Screenings Reports Contact is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.HS_Reports));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void HS_Contact_US_Format(Map<String, String> map) throws Exception {

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		link(healthScreen_pages.Add_Individual_recipients, "button").click();
		logger.info("click on Add_Individual_recipients");
		waits(2);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.Add_Individual_recipients_input).sendKeys("5550498943");
		logger.info("Enter data in Individual Recipient ");
		waits(2);
		logger.info("wait for 5 sec");

		System.out.print(healthScreen_pages.Searchcontact_Individual.getText());

		button(healthScreen_pages.Searchcontact_Individual, "button").click();
		logger.info("click on Searchcontact_Individual");
		waits(2);
		logger.info("wait for 5 sec");

		System.out.print("Contact is Showing in US format");
		closetoolspopups();

	}

	public void HS_Report_agent_Stamp(Map<String, String> map) throws Exception {

		button(healthScreen_pages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact Button");
		waits(3);
		logger.info("wait for 3 sec");

		System.out.print(healthScreen_pages.System_Stamp.getText());
		// closetoolspopups();
		logout();

	}

	public void Settings_roles_Admin_HS(Map<String, String> map) throws Exception {
		auto1Login(map);

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		waits(3);
		logger.info("wait for 5 sec");

		button(settings_Payment_Pages.setting_roles, "setting_roles").click();
		logger.info("click on setting_roles Button");
		waits(3);
		logger.info("wait for 3 sec");

		String Role = settings_Payment_Pages.setting_roles_admin.getText();
		System.out.print("Text ---->" + Role);

		String Admin_Roles = settings_Payment_Pages.setting_roles_admin.getText();
		System.out.print("Text ---->" + Admin_Roles);
		closesettingspopup();
		logout();

	}

	public void tools_HS_sendScreen(Map<String, String> map) throws Exception {
		oneAgentLogin(map);

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		wait(5);
		logger.info("wait for 5 sec");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(healthScreen_pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(healthScreen_pages.input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");
		wait(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");

		System.out.print(healthScreen_pages.create_toaster.getText());

		if (verifyElementIsEnabled(healthScreen_pages.create_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			textbox(healthScreen_pages.create_toaster).isEnabled();
			logger.info("Health Screen Succesfully Created");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.create_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		closetoolspopups();
		logout();

	}

	public void HS_Icon_TextMsg_InChatBox_retry(Map<String, String> map) throws Exception {

		textbox(Repository.user_id).sendKeys("AgentAutomation@yopmail.com");
		logger.info("Email id :" + "AgentAutomation@yopmail.com");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(5);
		logger.info("wait for 5 sec");

		button(healthScreen_pages.healthscreen_inChat, "click").click();
		waits(7);
		logger.info("wait for 7 sec");

		JSClick(repository.chatInputsendButton);
		waits(3);

		logger.info(healthScreen_pages.msg.getText());

		if (verifyElementIsEnabled(healthScreen_pages.retry)) {
			Assert.assertEquals(true, verifyElementIsEnabled(healthScreen_pages.retry));
			textbox(healthScreen_pages.retry).isEnabled();
			logger.info("Healthscreen retry is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(healthScreen_pages.retry));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		// closetoolspopups();
		logout();
	}
}
