package com.beetext.qa.Workflows;

import java.util.Map;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.OrgSwitch_CMWF;
import com.beetext.qa.pages.Org_Switch_Pages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class Org_Switch_WF extends OrgSwitch_CMWF {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Org_Switch_Pages org_Switch_Pages = new Org_Switch_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void Org_Switch_Add_Org_Arrow_Enabled(Map<String, String> map) throws Exception {

		if (verifyElementIsEnabled(org_Switch_Pages.Arrow)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.Arrow));
			textbox(org_Switch_Pages.Arrow).isEnabled();
			logger.info("Add Orgnaisation arrow is Enabled");
			logger.info("We can Add other Organisation in current ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.Arrow));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Org_Switch_Add_Org(Map<String, String> map) throws Exception {

		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(org_Switch_Pages.add_org)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.add_org));
			textbox(org_Switch_Pages.add_org).isEnabled();
			logger.info("Add Orgnaisation is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.add_org));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		wait(5);
		logger.info("wait for 5 sec");
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void Org_Switch_Add_Org_text(Map<String, String> map) throws Exception {

		waits(3);
		logger.info("wait for 3 sec");
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		String text = org_Switch_Pages.add_org_text.getText();
		logger.info("text ----->" + text);
		waits(2);
		logger.info("wait for 2 sec");
		button(org_Switch_Pages.add_org_Close_Button, "Arrow").click();
		logger.info("click on add_org_Close_Button Button");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void Org_Switch_Add_Org_add(Map<String, String> map) throws Exception {

		waits(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		String text = org_Switch_Pages.add_org_text.getText();
		logger.info("text ----->" + text);

		if (verifyElementIsEnabled(Repository.user_id)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.user_id));
			textbox(Repository.user_id).isEnabled();
			logger.info("Username is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.user_id));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(Repository.Passsword_ID)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Passsword_ID));
			textbox(Repository.Passsword_ID).isEnabled();
			logger.info("Password is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Passsword_ID));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(2);
		logger.info("wait for 2 sec");
		button(org_Switch_Pages.add_org_Close_Button, "Arrow").click();
		logger.info("click on add_org_Close_Button Button");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void Org_Switch_Add_Org_letsgo_disabled(Map<String, String> map) throws Exception {

		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		String text = org_Switch_Pages.add_org_text.getText();
		logger.info("text ----->" + text);

		if (verifyElementIsEnabled(org_Switch_Pages.letsgobttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.letsgobttn));
			textbox(org_Switch_Pages.letsgobttn).isEnabled();
			logger.info("Login Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.letsgobttn));
			logger.info("login button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		wait(5);
		logger.info("wait for 5 sec");
		button(org_Switch_Pages.add_org_Close_Button, "Arrow").click();
		logger.info("click on add_org_Close_Button Button");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void Org_Switch_Add_Org_productname(Map<String, String> map) throws Exception {

		waits(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(org_Switch_Pages.productname)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.productname));
			textbox(org_Switch_Pages.productname).isEnabled();
			logger.info("Product Icon is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.productname));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		wait(5);
		logger.info("wait for 5 sec");
		button(org_Switch_Pages.add_org_Close_Button, "Arrow").click();
		logger.info("click on add_org_Close_Button Button");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void Org_Switch_Add_Org_back_to_MsgPage(Map<String, String> map) throws Exception {

		waits(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(org_Switch_Pages.productname)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.productname));
			textbox(org_Switch_Pages.productname).isEnabled();
			logger.info("Product Icon is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.productname));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		wait(5);
		logger.info("wait for 5 sec");

		driver.navigate().back();

		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(repository.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.tools_bttn));
			textbox(repository.tools_bttn).isEnabled();
			logger.info("Tools Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.tools_bttn));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Org_Switch_Add_Org_validdetails(Map<String, String> map) throws Exception {

		waits(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		if (verifyElementIsEnabled(repository.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.tools_bttn));
			textbox(repository.tools_bttn).isEnabled();
			logger.info("Tools Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.tools_bttn));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();
		waits(2);

	}

	public void Org_Switch_Add_Org_Invalid_Passworddetails(Map<String, String> map) throws Exception {

		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1212121212");
		logger.info("Password:");
		wait(2);
		logger.info("wait for 2 sec");

		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		wait(3);
		logger.info("wait for 3 sec");

		String error_msg = org_Switch_Pages.invalid_Details_error.getText();
		logger.info("Error Msg = " + error_msg);

		if (verifyElementIsEnabled(org_Switch_Pages.invalid_Details_error)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.invalid_Details_error));
			textbox(org_Switch_Pages.invalid_Details_error).isEnabled();
			logger.info("Invalid Email or Password Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.invalid_Details_error));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		button(org_Switch_Pages.add_org_Close_Button, "Arrow").click();
		logger.info("click on add_org_Close_Button Button");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Org_Switch_Add_Org_Invalid_Emaildetails(Map<String, String> map) throws Exception {

		waits(2);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(Repository.user_id).sendKeys("automation.com");
		logger.info("Email id :");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1212121212");
		logger.info("Password:");
		wait(2);
		logger.info("wait for 2 sec");

		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		wait(3);
		logger.info("wait for 3 sec");

		String error_msg = org_Switch_Pages.email_notValid.getText();
		logger.info("Error Msg = " + error_msg);

		if (verifyElementIsEnabled(org_Switch_Pages.email_notValid)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.email_notValid));
			textbox(org_Switch_Pages.email_notValid).isEnabled();
			logger.info("Email not valid");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.email_notValid));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 5 sec");
		button(org_Switch_Pages.add_org_Close_Button, "Arrow").click();
		logger.info("click on add_org_Close_Button Button");
		waits(2);
		logger.info("wait for 5 sec");

	}

	public void Org_Switch_Add_Org_AlreadySignIN_errorMsg(Map<String, String> map) throws Exception {

		waits(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		String error_msg = org_Switch_Pages.alreadySign_in.getText();
		logger.info("Error Msg = " + error_msg);

		if (verifyElementIsEnabled(org_Switch_Pages.alreadySign_in)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.alreadySign_in));
			textbox(org_Switch_Pages.alreadySign_in).isEnabled();
			logger.info("Already Sign In Error msg Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.alreadySign_in));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		button(org_Switch_Pages.add_org_Close_Button, "Arrow").click();
		logger.info("click on add_org_Close_Button Button");
		waits(2);
		logger.info("wait for 2 sec");
		validateSignOutLink();
		validateSignOut();

	}

	public void Org_Switch_Add_Org_Only_OneOrgAllowed(Map<String, String> map) throws Exception {
		waits(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		validateSignOutLink();
		validateSignOut();

		wait(5);
		logger.info("wait for 5 sec");
	}

	public void Org_Switch_Add_Org_validdetails_logout_shows_PreviousOrg(Map<String, String> map) throws Exception {

		waits(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		validateSignOutLink();
		validateSignOut();

		String text = org_Switch_Pages.auto1_login.getText();
		logger.info("text --->" + text);

		if (verifyElementIsEnabled(org_Switch_Pages.auto1_login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.auto1_login));
			textbox(org_Switch_Pages.auto1_login).isEnabled();
			logger.info("After Sign out. By Default it shows previous login");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.auto1_login));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Org_Switch_Add_Org_OrgList(Map<String, String> map) throws Exception {

		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);
		waits(2);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		String text = org_Switch_Pages.addOrg_auto1_login.getText();
		logger.info("text --->" + text);

		if (verifyElementIsEnabled(org_Switch_Pages.addOrg_auto1_login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.addOrg_auto1_login));
			textbox(org_Switch_Pages.addOrg_auto1_login).isEnabled();
			logger.info("Auto1 login");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.addOrg_auto1_login));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String text1 = org_Switch_Pages.addOrg_automation_login.getText();
		logger.info("text --->" + text1);

		if (verifyElementIsEnabled(org_Switch_Pages.addOrg_automation_login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.addOrg_automation_login));
			textbox(org_Switch_Pages.addOrg_automation_login).isEnabled();
			logger.info("Automation login");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.addOrg_automation_login));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		wait(5);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();
	}

	public void Org_Switch_Add_Org_radioButton_underAddButton(Map<String, String> map) throws Exception {

		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(org_Switch_Pages.add_org)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.add_org));
			textbox(org_Switch_Pages.add_org).isEnabled();
			logger.info("Add Button Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.add_org));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(org_Switch_Pages.auto1_radiobttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.auto1_radiobttn));
			textbox(org_Switch_Pages.auto1_radiobttn).isEnabled();
			logger.info("Auto1 checkmark ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.auto1_radiobttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(org_Switch_Pages.automation_radiobttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.automation_radiobttn));
			textbox(org_Switch_Pages.automation_radiobttn).isEnabled();
			logger.info("Automation checkmark ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.automation_radiobttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		wait(5);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

	}

	public void Org_Switch_Add_Org_updateOrg_DeleteOrg(Map<String, String> map) throws Exception {

		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		validateSignOutLink();
		validateSignOut();

		if (verifyElementIsEnabled(org_Switch_Pages.addOrg_auto1_login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(org_Switch_Pages.addOrg_auto1_login));
			textbox(org_Switch_Pages.addOrg_auto1_login).isEnabled();
			logger.info("Auto1 login");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(org_Switch_Pages.addOrg_auto1_login));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		logger.info(" Auto1 Organisation Deleted Successfully");

	}

	public void Batch_Count(Map<String, String> map) throws Exception {

		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		waits(2);
		button(repository.Auto1_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Auto1_Contact Button");
		waits(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Technocentra");
		logger.info("enter text in chat input text area");
		wait(5);
		logger.info("wait for 5 sec");

		button(repository.chatInputsendButton, " click on send").click();
		logger.info("Click on Send Button");
		waits(2);
		logger.info("wait for 2 sec");

		String Batch_Count = org_Switch_Pages.batch_count.getText();
		logger.info("Batch Count ---->" + Batch_Count);
		validateSignOutLink();
		validateSignOut();
	}

	public void login_logout_sameSession(Map<String, String> map) throws Exception {
		waits(3);
		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		wait(5);
		logger.info("wait for 5 sec");

		click_AddORG_automationsLogin(map);

		validateSignOutLink();
		validateSignOut();

	}

}
