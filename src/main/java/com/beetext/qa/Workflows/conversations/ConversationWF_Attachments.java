package com.beetext.qa.Workflows.conversations;

import java.util.Map;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Conversation;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class ConversationWF_Attachments extends Conversation {
	String url, username1, username2, password;
	Repository repository = new Repository();
	public static String currentDir = System.getProperty("user.dir");

	public void conversations_text_message(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);
	}

	public void conversations_1000char_max_text_message(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);
		waits(2);
		textbox(repository.chatInputTextareaTxt).clear();
		String Max_Text = "The accuracy of the information provided to Beetexting,during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are as follows:The accu";
		textbox(repository.chatInputTextareaTxt).sendKeys(Max_Text);
		waits(10);
		SendButton(map);

	}

	public void conversations_textmessage_image(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(15);
		logger.info("wait for 15 sec");

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Image Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 1mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_textmessage_audio(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(15);
		logger.info("wait for 15 sec");

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.audio_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.audio_verification));
			textbox(repository.audio_verification).isEnabled();
			System.out.println("Audio Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.audio_verification));
			System.out.println("Audio not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_textmessage_video(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(
				currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo_1280x720_1mb.mp4");

		waits(15);
		logger.info("wait for 15 sec");

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("Video Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("Video not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void jpg1mb_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");

		waits(15);
		logger.info("wait for 15 sec");

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Jpg 1mb file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 1mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void text_jpg2500kb_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_2.5mb.jpg");

		waits(25);
		logger.info("wait for 25 sec");

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Jpg 2.5mb file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 2.5mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void attachments_10files(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");

		waits(25);

		SendButton(map);

	}

	public void attachments_morethan_10files_popuperror(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		repository.max10attachmentspopup.getText();
		System.out.println(repository.max10attachmentspopup.getText());
		if (repository.max10attachmentspopup.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(repository.max10attachmentspopup.getText(), "Maximum 10 attachments are allowed.",
					"max10attachmentspopup is matching");
			TestUtil.takeScreenshotAtEndOfTest("max10attachmentspopupTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.max10attachmentspopup.getText(), "Maximum 10 attachments are allowed.",
					"max10attachmentspopup is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("max10attachmentspopupTestCase");// ScreenShot capture
		}
		waits(2);

		button(Repository.max_Attachments_Popup_Ok, "Click on OK").click();
		waits(2);

	}

	public void conversations_msg_morethan_2mbfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(15);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.audio_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.audio_verification));
			textbox(repository.audio_verification).isEnabled();
			System.out.println("Audio Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.audio_verification));
			System.out.println("Audio not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_gif(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.giphybttn, "click on gif button").click();
		waits(2);
		button(repository.giphyselection, "clicking on gif ").click();
		waits(2);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Signature_count(Map<String, String> map) throws Exception {

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		waits(2);
		button(Repository.loginbttn, "login submit").submit();
		waits(5);
		button(repository.Auto1_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Auto1_Contact Button");
		waits(2);
		validateSignOutLink();
		validateUser();
		waits(2);
		validatetoggle();
		waits(2);
		button(repository.signature_save, "click on signature_toggle_On_Off").click();
		waits(2);

		button(repository.signature_cancel, "click on signature_cancel").click();
		waits(10);

		String text = "The accuracy of the information provided to Beetexting, both during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are follow";
		textbox(repository.chatInputTextareaTxt).sendKeys(text);
		waits(20);

		String count = repository.signature_count.getText();
		System.out.print("Count --->" + count);

		waits(2);
		validateSignOutLink();
		waits(2);
		validateUser();
		waits(2);
		validatetoggle();
		waits(2);
		button(repository.signature_save, "click on signature_toggle_On_Off").click();
		waits(2);
		button(repository.signature_cancel, "click on signature_cancel").click();
		waits(2);

	}

	public void fileinprogress_sendbttndisable(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");

		if (verifyElementIsEnabled(repository.chatInputsendButton)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.chatInputsendButton));
			button(repository.chatInputsendButton, "click on send button").isEnabled();
			System.out.println("Send button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.chatInputsendButton));
			System.out.println("Send button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void fileinprogress_sendbttnenable(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(5);

		if (verifyElementIsEnabled(repository.chatInputsendButton)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.chatInputsendButton));
			textbox(repository.chatInputsendButton).isEnabled();
			System.out.println("Send button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.chatInputsendButton));
			System.out.println("Send button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void textmessage_emoji(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.emojibttn, "click on Emoji button").click();
		waits(2);
		button(repository.emoji_like, "click on Emoji button").click();
		waits(2);

		SendButton(map);

	}

	public void textmessage_emoji_image(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.emojibttn, "click on Emoji button").click();
		waits(2);
		button(repository.emoji_like, "click on Emoji button").click();
		waits(2);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(10);

		SendButton(map);

	}

	public void textmessage_emoji_image_video(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.emojibttn, "click on Emoji button").click();
		waits(2);
		button(repository.emoji_like, "click on Emoji button").click();
		waits(2);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(20);

		SendButton(map);

	}

	public void emoji(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		button(repository.emojibttn, "click on Emoji button").click();
		waits(2);

		button(repository.emoji_like, "click on Emoji button").click();
		waits(2);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

	}

	public void textmessage_emoji_image_pdf(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.emojibttn, "click on Emoji button").click();
		waits(2);
		button(repository.emoji_like, "click on Emoji button").click();
		waits(2);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(20);

		SendButton(map);

	}

	public void textmessage_hyperlink(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		String hyperlink = "https://automation.beetexting.com/messages";

		textbox(repository.chatInputTextareaTxt).sendKeys(hyperlink);
		waits(2);

		SendButton(map);

	}

	public void textmessage_gif(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.giphybttn, "click on gif button").click();
		waits(2);
		button(repository.giphyselection, "clicking on gif ").click();
		waits(2);

		SendButton(map);

	}

	public void textmessage_morethan_onegif(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.giphybttn, "click on gif button").click();
		waits(2);
		button(repository.giphyselection, "clicking on gif ").click();
		waits(2);

		button(repository.giphybttn, "click on gif button").click();
		waits(2);
		button(repository.giphyselection1, "clicking on gif ").click();
		waits(2);

		button(repository.giphybttn, "click on gif button").click();
		waits(2);
		button(repository.giphyselection2, "clicking on gif ").click();
		waits(2);

		SendButton(map);

	}

	public void textmessage_searchgif(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.giphybttn, "click on gif button").click();
		waits(2);
		textbox(repository.gif_search).sendKeys("goodmorning");
		waits(2);
		button(repository.giphyselection, "clicking on gif ").click();
		waits(2);

		SendButton(map);

	}

	public void textmessage_search_dirtyword_gif(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Team");
		waits(2);
		button(repository.giphybttn, "click on gif button").click();
		waits(5);
		textbox(repository.gif_search).sendKeys("badboys");
		waits(5);

		button(repository.giphyselection, "clicking on gif ").click();
		waits(2);

		SendButton(map);
	}

	public void mp3_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");

		waits(15);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("MP3 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("MP3 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void mp4_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");

		waits(15);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void mov_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mov");

		waits(15);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void webm_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");

		waits(15);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("WEBM file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("WEBM file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void pdf_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");

		waits(5);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void worddocument_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waits(5);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("worddocument file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("worddocument file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void excelsheet_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");

		waits(5);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("excelsheet file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("excelsheet file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void textfileverification(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");

		waits(5);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Text file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Text file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void png_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void jpeg_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void aif_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\aif.aif");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("aif file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("aif file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void avi_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.avi");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("avi file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("avi file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void wmv_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.avi");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("wmv file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("wmv file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void mpg_file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mpg");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("mpg file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("mpg file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void file_vob(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_640x360.vob");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("file vob Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("file vob not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void file_mts(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_640x360.mts");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("file mts Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("file mts not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void file_mpeg(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mpeg");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("file mpeg Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("file mpeg not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void file_3gp(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");

		waits(25);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("file 3gp Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("file 3gp not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void blocked_contact(Map<String, String> map) throws Exception {

		waits(2);

		autologin(map);

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automationblock@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waits(5);
		logger.info("wait for 5 sec");
		button(repository.Automation_Contact, "click on Automation_Contact ").click();
		logger.info("click on Automation_Contact Button");
		waits(3);
		logger.info("wait for 3 sec");

		button(repository.contact_blocked, "contact_blocked").click();
		if (verifyElementIsEnabled(repository.contact_blocked_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.contact_blocked_toaster));
			textbox(repository.contact_blocked_toaster).isEnabled();
			System.out.println("Contact Blocked");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.contact_blocked_toaster));
			System.out.println("Contact not Blocked");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		button(repository.contact_unblock, "click on contact_unblock button").click();
		waits(3);
	}

	public void unblockcontact_messagewillappear(Map<String, String> map) throws Exception {

		waits(2);
		autologin(map);

		button(repository.Automation_Contact, "click on Automation_Contact ").click();
		logger.info("click on Automation_Contact Button");
		waits(3);
		logger.info("wait for 3 sec");
		button(repository.contact_blocked, "contact_blocked").click();
		waits(2);

		changing_Login1_to_Login2(map);

		button(repository.Auto1_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Auto1_Contact Button");
		waits(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Team");
		waits(2);
		SendButton(map);
		waits(2);
		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		waits(2);
		button(Repository.loginbttn, "login submit").submit();
		waits(3);

		button(repository.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waits(2);

		button(repository.contact_unblock, "click on contact_unblock button").click();
		waits(3);
		System.out.println(repository.message.getText());
	}

	public void receivingside_downloadbutton_enable(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(15);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		image(repository.image_verification, "click on image_verification button").click();
		waits(3);

		if (verifyElementIsEnabled(repository.popup_close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.popup_close));
			textbox(repository.popup_close).isEnabled();
			System.out.println("Close Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.popup_close));
			System.out.println("Close Button not Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(2);
		button(repository.popup_close, "click on close").click();
		waits(2);

	}

	public void pinmessage_forinternalorg(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		button(repository.pinIcon, "pinIcon").click();
		waits(3);
		textbox(repository.pinmessage_textarea).sendKeys("Hello Beetexting");
		waits(3);

		JSClick(repository.chatInputsendButton);
		waits(3);

	}

	public void receivingside_downloadfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(15);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		image(repository.image_verification, "click on image_verification button").click();
		waits(2);

		button(repository.download_file, "click on download_file button").click();
		waits(2);

		button(repository.popup_close, "click on close button").click();
		waits(2);

	}

}
