package com.beetext.qa.Workflows.conversations;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.UploadNonMediaAttachmentsWFCM;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.UploadNonMediaAttachment;
import com.beetext.qa.util.TestUtil;

public class UploadNonMediaAttachmentsWF extends UploadNonMediaAttachmentsWFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	UploadNonMediaAttachment pinMessage = new UploadNonMediaAttachment();
	public static String currentDir = System.getProperty("user.dir");

	public void MessageandPDFcombination() throws Exception {
		login();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");

		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + PDF file Recevied");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + PDF file Recevied");// ScreenShot capture
		}
		
	}

	public void MessageandDocumentcombination() throws Exception {
		//logout();
		login();
		button(pinMessage.select_conversations12, "button").click();
		wait(5);
		// System.out.println( repository.select_conversation.getText() );

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		System.out.println(currentDir + "\\Files\\welcome.doc");

		waits(5);
		button(pinMessage.postButton, "button").click();
		wait(3);

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Document is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Document");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Document");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Document file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + Document file Recevied");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + Document file Recevied");// ScreenShot capture
		}
		
	}

	public void MessageandTextDocumentcombination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		System.out.println(currentDir + "\\Files\\welcome.doc");

		waits(5);
		button(pinMessage.postButton, "button").click();
		waits(3);

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Text file is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Text document");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Text Document");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Text file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + Text file Recevied");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + Text file Recevied");// ScreenShot capture
		}
		
	}

	public void MessageandExcel_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		System.out.println(currentDir + "\\Files\\welcome.doc");

		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Excel is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Excel file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + Excel file Recevied");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + Excel file Recevied");// ScreenShot capture
		}
		

	}

	public void MessageandCSV_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv.");
		System.out.println(currentDir + "\\Files\\welcome.doc");

		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus CSV is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + CSV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + CSV file Recevied");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + CSV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Message + CSV file Recevied");// ScreenShot capture
		}
		
	}

	public void MessageandDocumentplusPDFcombination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Doucment and  PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Document +PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and PDF");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Non Media Message plus  Document and PDF not recieved");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and PDF");// ScreenShot capture
		}
		

	}

	public void MessageandDocumentplusExcelcombination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Excel");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Excel");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Document + Excel file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Excel Received");// ScreenShot
																										// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Document + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Excel Received");// ScreenShot
																										// capture
		}
		

	}

	public void MessageandDocumentplusCSVcombination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus document and csv combination is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and CSV");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Excel");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Document + CSV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and CSV");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + document + CSV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and CSV");// ScreenShot capture
		}
		

	}

	public void MessageandDocumentplusText_combination() throws Exception {
		//logout();
		login();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		wait(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Document + Test file is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Test File");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Test File");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Document and Test  file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Test File");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message +  Document plus Test file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus  Document and Test File");// ScreenShot capture
		}
		

	}

	public void Messageand_PDF_plusText_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF  and Test file is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus pdf and test file");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus pdf and test file");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Non Media Message plus pdf and test file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus pdf and test file");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF  + test file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus pdf and test file");// ScreenShot capture
		}
		
	}

	public void Messageand_PDF_plusDocument_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF + Document  is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and Document");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and Document");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF+ Document  file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and Document");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF+ Document  file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and Document");// ScreenShot capture
		}

		
	}

	public void Messageand_PDF_plusExcel_combination() throws Exception {
		//logout();
		login();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF and excel is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and EXCel");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("non Media Message plus PDF and EXCel");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF + Excel file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("non Media Message plus PDF and EXCel");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF + Excel file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("on Media Message plus PDF and EXCel");// ScreenShot capture
		}

		
	}

	public void Messageand_PDF_plusCSV_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);

		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF + Test file is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and test file combination");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and test file combination");// ScreenShot
																										// capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and test file combination");// ScreenShot
																										// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF and test file combination");// ScreenShot
																										// capture
		}
		

	}

	public void Messageand_XLS_plusDocument_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);

		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Excel + Document is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Docuemnt");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Docuemnt");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();

		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Docuemnt");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Docuemnt");// ScreenShot capture
		}
	

	}

	public void Messageand_XLS_plus_PDF_combination() throws Exception {
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Excel + PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Excel + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + PDF");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Excel + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + PDF");// ScreenShot capture
		}
		
	}

	public void Messageand_XLS_plus_CSV_combination() throws Exception {
	//	logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + CSV");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + CSV");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Excel + csv file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + CSV");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Excel + CSV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + CSV");// ScreenShot capture
		}
		
	}

	public void Messageand_XLS_plus_Text_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Excel + Test");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Test");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Test");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Excel+ Test file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Test");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Excel+ Test file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Excel + Test");// ScreenShot capture
		}
		

	}

	public void Messageand_CSV_plus_Document_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus CSV PLUS Document is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Document");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Document");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + CSV+ Document file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Document");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Document + CSV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Document");// ScreenShot capture
		}
		

	}

	public void Messageand_CSV_plus_PDF_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);

		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + CSV+ PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + PDF");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + CSV + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + PDF");// ScreenShot capture
		}
		

	}

	public void Messageand_CSV_plus_XLS_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus CSV + Excel is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Excel");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Excel");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + CSV + Excel file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Excel");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + CSV + Excel file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Excel");// ScreenShot capture
		}
		

	}

	public void Messageand_CSV_plus_Text_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media CSV + Document combination");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Document combination ");// ScreenShot
																										// capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + CSV + Document file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Document combination ");// ScreenShot
																										// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + CSV + Document file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus CSV + Document combination ");// ScreenShot
																										// capture
		}
		

	}

	public void Messageand_Text_plus_Document_combination() throws Exception {
	//	logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus test plues document is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Document ");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Document");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + TEST +Document file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Document");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + TEST + Document file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Document");// ScreenShot capture
		}
		

	}

	public void Messageand_Text_plus_PDF_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus TEST PLUS PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus TEST PLUS PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Test plus PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus TEST PLUS PDF");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus TEST PLUS PDF");// ScreenShot capture
		}

		
	}

	public void Messageand_Text_plus_XLS_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Excel");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Excel");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Excel");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus Excel");// ScreenShot capture
		}
		

	}

	public void Messageand_Text_plus_CSV_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus CSV");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus CSV");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus CSV");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Test plus CSV");// ScreenShot capture
		}
		

	}

	public void Messageand_image_plus_Doucment_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Document ");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Document");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Jpg 1mb file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Document");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 1mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Document");// ScreenShot capture
		}

		

	}

	public void Messageand_image_plus_PDF_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Jpg 1mb file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus PDF");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 1mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus PDF");// ScreenShot capture
		}

		

	}

	public void Messageand_image_plus_XLS_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Excel");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Excel");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Jpg 1mb file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Excel");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 1mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus image plus Excel");// ScreenShot capture
		}

		

	}

	public void Messageand_image_plus_CSV_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plues image plus csv");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plues image plus csv");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Jpg 1mb file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plues image plus csv");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 1mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plues image plus csv");// ScreenShot capture
		}

		
	}

	public void Messageand_image_plus_Text_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Jpg 1mb file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Jpg 1mb file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		
	}

	public void Messageand_Video_plus_Document_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_Video_plus_PDF_combination() throws Exception {
		//logout();
		login();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

		

	}

	public void Messageand_Video_plus_XLS_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

		
	}

	public void Messageand_Video_plus_CSV_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
	

	}

	public void Messageand_Video_plus_Text_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

		

	}

	public void Messageand_Audio_plus_Document_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

		

	}

	public void Messageand_Audio_plus_PDF_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_Audio_plus_XLS_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

	}

	public void Messageand_Audio_plus_CSV_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");
		waits(5);
		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_Audio_plus_Text_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waits(5);
		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

		

	}

	public void Messageand_MP3_Audio() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

	}

	public void Messageand_3GP_Audio() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}

	}

	public void Messageand_AMR_Audio() throws Exception {
		//logout();
		login();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
	}

	public void Messageand_WEBM_Audio() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_mp3_Audio_plus_3gp_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		
	}

	public void Messageand_mp3_Audio_plus_AMR_combination() throws Exception {
		//logout();

		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_mp3_Audio_plus_WEBM_combination() throws Exception {
		//logout();

		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
	

	}

	public void Messageand_3gp_Audio_plus_MP3_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(5);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");

		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}
		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_3Gp_Audio_plus_AMR_combination() throws Exception {
		//logout();
		login();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_3gp_Audio_plus_WEBM_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(15);

		sendButton();
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_AMR_Audio_plus_MP3_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments AMR AND MP3 combination file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND MP3 combination");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments AMR AND MP3 combination file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND MP3 combination");// ScreenShot capture
		}
		

	}

	public void Messageand_AMR_Audio_plus_3GP_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments AMR AND 3Gp combination is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND 3Gp combination");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND 3Gp combination");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments AMR AND 3Gp combination file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND 3Gp combination");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments AMR AND 3Gp combination file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND 3Gp combination");// ScreenShot capture
		}
		

	}

	public void Messageand_AMR_Audio_plus_WEBM_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments AMR AND WEBM combination is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND WEBM combination");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments AMR AND WEBM combination");// ScreenShot capture
		}

		logout();
		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + PDF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + PDF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments");// ScreenShot capture
		}
		

	}

	public void Messageand_WEBM_Audio_plus_MP3_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments WEBM AND MP3 combination is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND MP3 combination");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND MP3 combination");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments WEBM AND MP3 combination file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND MP3 combination");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments WEBM AND MP3 combination file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND MP3 combination");// ScreenShot capture
		}
		

	}

	public void Messageand_WEBM_Audio_plus_3GP_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments WEBM AND 3GP combination is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND 3GP combination");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND 3GP combination");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments WEBM AND 3GP combination file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND 3GP combination");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments WEBM AND 3GP combination file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND 3GP combination");// ScreenShot capture
		}
		
	}

	public void Messageand_WEBM_Audio_plus_AMR_combination() throws Exception {
		//logout();
		login();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments WEBM AND AMR combination is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND AMR combination");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND AMR combination");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments WEBM AND AMR combination file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND AMR combination");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments WEBM AND AMR combination file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments WEBM AND AMR combination");// ScreenShot capture
		}
		

	}

	public void MessageandJpg_Image_plus_MP3_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments image plus MP3 is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus MP3");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus MP3");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments image plus MP3 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus MP3");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments image plus MP3 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus MP3");// ScreenShot capture
		}
		

	}

	public void MessageandJpg_Image_plus_3Gp_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments image plus 3GP is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus 3GP");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus 3GP");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments image plus 3GP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus 3GP");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments image plus 3GP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus 3GP");// ScreenShot capture
		}
		

	}

	public void MessageandJpg_Image_plus_Amr_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");

		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus PDF");// ScreenShot capture
		}

		logout();
		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments image plus AMR file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus AMR");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments image plus AMR file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus AMR");// ScreenShot capture
		}
		
	}

	public void MessageandJpg_Image_plus_Webm_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(5);

		attachmenticon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Message plus Non_Media_Attachments image plus WEBM is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus WEBM");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus WEBM");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments image plus WEBM file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus WEBM");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments image plus WEBM file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments image plus WEBM");// ScreenShot capture
		}
		

	}

	public void MessageandJpg_video_plus_MP3_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments video plus MP3 is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus MP3");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus MP3");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments video plus MP3 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus MP3");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message + Non_Media_Attachments video plus MP3 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus MP3");// ScreenShot capture
		}
		

	}

	public void MessageandJpg_video_plus_3gp_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample_3gp.3gp");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Non_Media_Attachments video plus 3gp is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus 3gp");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus 3gp");// ScreenShot capture
		}

		logout();

		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message + Non_Media_Attachments video plus 3gp file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus 3gp");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Message Non_Media_Attachments video plus 3gp file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus 3gp");// ScreenShot capture
		}
		

	}

	public void MessageandJpg_video_plus_WEBM_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");

		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus PDF is verified");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus WEBM ");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus WEBM");// ScreenShot capture
		}

		logout();
		login2();
		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Message Non_Media_Attachments video plus WEBM file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus WEBM");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Non_Media_Attachments video plus WEBM not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non_Media_Attachments video plus WEBM");// ScreenShot capture
		}
		

	}

	public void MessageandJpg_video_plus_AMR_combination() throws Exception {
		//logout();
		login();

		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);

		attachmenticon();

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);

		attachmenticon();

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Amrfile.amr");
		waits(15);

		sendButton();

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "message is matching");
			System.out.println("Non Media Message plus Video plues AMR file");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Video plues AMR file");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Video plues AMR file");// ScreenShot capture
		}

		logout();

		login2();

		login2conversation();
		if (verifyElementIsEnabled(repository.receviedsidemsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.receviedsidemsg));
			textbox(repository.receviedsidemsg).isEnabled();
			System.out.println("Non Media Message plus Video plues AMR file file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Video plues AMR file");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.receviedsidemsg));
			System.out.println("Non Media Message plus Video plues AMR file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Non Media Message plus Video plues AMR file");// ScreenShot capture
		}

	}

}
