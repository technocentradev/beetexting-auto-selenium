package com.beetext.qa.Workflows.conversations;

import java.util.Map;

import org.testng.Assert;

import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

import com.beetext.qa.WFCommonMethod.Conversation;

public class ConversationWF_Image_Attachments extends Conversation {
	String url, username1, username2, password;
	Repository repository = new Repository();
	public static String currentDir = System.getProperty("user.dir");

	public void message(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

	}

	public void conversations_message_png(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_bmpFile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void conversations_message_jpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_JpegFile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void conversations_message_GifFile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_tiffFile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("Tiff file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("Tiff file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void conversations_message_pngfile_bmpfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_pngfile_jpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_pngfile_jpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_pngfile_giffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_pngfile_tifffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");

		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_bmpfile_pngfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_bmpfile_jpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_bmpfile_jpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_bmpfile_giffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_bmpfile_tifffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpgfile_pngfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpgfile_Bmpfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);
		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpgfile_Jpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpgfile_GIFfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpgfile_TIFFfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		wait(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpegfile_pngfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpegfile_bmpfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpegfile_jpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpegfile_giffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_jpegfile_Tifffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_giffile_pngfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_giffile_bmpfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_giffile_jpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_giffile_jpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_giffile_tifffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_tifffile_pngfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_tifffile_bmpfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\bmpfile.bmp");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("BMP file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("BMP file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_tifffile_JPGfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_tifffile_jpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("JPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("JPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversations_message_tifffile_giffile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\Tiff_file.tif");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");
		waits(10);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("TIFF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("TIFF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("GIF file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("GIF file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

}
