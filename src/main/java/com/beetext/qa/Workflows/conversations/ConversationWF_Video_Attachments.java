package com.beetext.qa.Workflows.conversations;

import java.util.Map;

import org.testng.Assert;

import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.WFCommonMethod.Conversation;

public class ConversationWF_Video_Attachments extends Conversation {
	String url, username1, username2, password;
	Repository repository = new Repository();
	public static String currentDir = System.getProperty("user.dir");

	public void message_avifile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("avi file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("avi file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mp4file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_movfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_avifile_mp4file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("avi file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("avi file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_avifile_movfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("avi file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("avi file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_avifile_mpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("avi file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("avi file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_avifile_mpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("avi file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("avi file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mp4file_avifile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("AVI file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("AVI file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mp4file_movfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mp4file_mpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mp4file_mpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(3);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_movfile_avifile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("AVI file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("AVI file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_movfile_mp4file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_movfile_mpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_movfile_mpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpgfile_avifile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("AVI file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("AVI file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpgfile_mp4file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpgfile_movfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpgfile_mpegfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpegfile_avifile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");
		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("AVI file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("AVI file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpegfile_mp4file(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpegfile_movfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpegfile_mpgfile(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_avifile_image(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropavi.avi");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);
		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("IMAGE file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("IMAGE file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("avi file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("avi file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mp4file_image(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("IMAGE file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("IMAGE file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MP4 file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MP4 file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_movfile_image(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("IMAGE file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("IMAGE file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MOV file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MOV file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpgfile_image(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpg.mpg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("IMAGE file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("IMAGE file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void message_mpegfile_image(Map<String, String> map) throws Exception {

		autologin(map);

		Auto1_Login_Automation_Contact(map);

		Pin_Attachment(map);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waits(5);

		Pin_Attachment(map);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmpeg.mpeg");

		waits(30);

		SendButton(map);

		changing_Login1_to_Login2(map);

		user2_ReceivingSide_message_Validation(map);

		if (verifyElementIsEnabled(repository.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.image_verification));
			textbox(repository.image_verification).isEnabled();
			System.out.println("IMAGE file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.image_verification));
			System.out.println("IMAGE file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(repository.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.video_verification));
			textbox(repository.video_verification).isEnabled();
			System.out.println("MPEG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.video_verification));
			System.out.println("MPEG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

}
