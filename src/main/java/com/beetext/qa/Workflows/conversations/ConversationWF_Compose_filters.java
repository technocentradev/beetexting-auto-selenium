package com.beetext.qa.Workflows.conversations;

import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.WFCommonMethod.Conversation;

public class ConversationWF_Compose_filters extends Conversation {
	String url, username1, username2, password;
	Repository repository = new Repository();
	public static String currentDir = System.getProperty("user.dir");

	public void oneoneconversation(Map<String, String> map) throws Exception {

		autologin(map);
		Auto1_Login_Automation_Contact(map);
		SendButton(map);
	}

	public void group_conversation_maxtencontactsallowed_error(Map<String, String> map) throws Exception {

		automationBlock_login(map);
		waits(2);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose button");
		waits(2);
		logger.info("wait for 2 sec");

		String arr[] = { "555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), "555" + RandomStringUtils.randomNumeric(7),
				"555" + RandomStringUtils.randomNumeric(7), };

		for (int i = 0; i <= 8; i++) {

			textbox(repository.contactSearchInputTxt).sendKeys(arr[i]);
			logger.info("Enter data in contactSearchInputTxt ");
			waits(2);
			logger.info("wait for 2 sec");

			link(repository.contactSearchOutputTxt2, "button").click();
			logger.info("click on contactSearchOutputTxt1");
			waits(2);
			logger.info("wait for 2 sec");
		}

		waits(3);
		logger.info("wait for 3 sec");
		repository.maxtencontactsallowed.getText();
		System.out.println(repository.maxtencontactsallowed.getText());
		if (repository.maxtencontactsallowed.getText().equalsIgnoreCase("Maximum 9 contacts are allowed.")) {
			Assert.assertEquals(repository.maxtencontactsallowed.getText(), "Maximum 9 contacts are allowed.",
					"maxtencontactsallowed is matching");
			TestUtil.takeScreenshotAtEndOfTest("maxtencontactsallowedTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.maxtencontactsallowed.getText(), "Maximum 9 contacts are allowed.",
					"maxtencontactsallowed is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("maxtencontactsallowedTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Techies");
		logger.info("enter message");
		waits(2);
		logger.info("wait for 2 sec");

		SendButton(map);

	}

	public void blocked_contact_err(Map<String, String> map) throws Exception {

		autologin(map);
		waits(2);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose button");
		waits(3);
		logger.info("wait for 3 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("qh");
		logger.info("enter the txt to search contact");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(repository.contactSearchOutputTxt11)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.contactSearchOutputTxt11));
			textbox(repository.contactSearchOutputTxt11).isEnabled();
			System.out.println("contact is Disable because contact is blocked");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.contactSearchOutputTxt11));
			System.out.println("contact is not blocked");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void newnumber_display(Map<String, String> map) throws Exception {

		autologin(map);
		waits(2);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose button");
		waits(2);
		logger.info("wait for 2 sec");

		textbox(repository.contactSearchInputTxt).sendKeys("3098524763");
		logger.info("enter the number to search contact");
		waits(2);
		logger.info("wait for 2 sec");
		link(repository.contactSearchOutputTxt12, "Search output").click();
		logger.info("click on contact");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(repository.starnewconversion)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.starnewconversion));
			textbox(repository.starnewconversion).isEnabled();
			System.out.println("New number - you're starting new conversation");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.message));
			System.out.println("its a old chatt ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void existingnumber_alreadyhavemsg(Map<String, String> map) throws Exception {

		autologin(map);
		button(repository.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waits(2);

		if (verifyElementIsEnabled(repository.message)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.message));
			textbox(repository.message).isEnabled();
			System.out.println("already have messages in this chat");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.message));
			System.out.println("its a new chat ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void placeholder(Map<String, String> map) throws Exception {

		autologin(map);
		waits(2);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on compose button");
		waits(5);
		logger.info("wait for 5 sec");
		String text = repository.placeholder.getAttribute("placeholder");
		System.out.println("text-->" + text);
	}

	public void conversation_transfer_claim(Map<String, String> map) throws Exception {

		autologin(map);
		button(repository.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waits(2);

		button(repository.actionbttninchat, "actionbttninchat").click();
		logger.info("click on action bttn in chat");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.transferlist_inchat, "transferlist_inchat").click();
		logger.info("click on transfer list in chat");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.transferlist_changetoautomation, "transferlist_changetoautomation").click();
		logger.info("click on transfer list");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(repository.transferlist_conversatinclaimedmsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.transferlist_conversatinclaimedmsg));
			textbox(repository.transferlist_conversatinclaimedmsg).isEnabled();
			System.out.println("Conversation claimed by Automation. is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.transferlist_conversatinclaimedmsg));
			System.out.println("Conversation claimed by Automation. is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversation_claim(Map<String, String> map) throws Exception {

		autologin(map);
		button(repository.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waits(2);

		button(repository.actionbttninchat, "actionbttninchat").click();
		logger.info("click on action bttn in chat");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.conversation_claimbttn, "conversation_claimbttn").click();
		logger.info("click on conversation claim bttn");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(repository.conversation_claimtxt)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.conversation_claimtxt));
			textbox(repository.conversation_claimtxt).isEnabled();
			System.out.println("Conversation claimed by You. is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.conversation_claimtxt));
			System.out.println("Conversation claimed by You. is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversation_unclaim(Map<String, String> map) throws Exception {

		autologin(map);
		waits(2);
		button(repository.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waits(2);

		button(repository.actionbttninchat, "actionbttninchat").click();
		logger.info("click on action bttn in chat");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.conversation_unclaimbttn, "conversation_unclaimbttn").click();
		logger.info("click on conversation unclaim bttn");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(repository.conversation_unclaimtxt)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.conversation_unclaimtxt));
			textbox(repository.conversation_unclaimtxt).isEnabled();
			System.out.println("Conversation unclaimed by You. is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.conversation_unclaimtxt));
			System.out.println("Conversation unclaimed by You. is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void conversation_resolved_reopen(Map<String, String> map) throws Exception {

		autologin(map);
		button(repository.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waits(2);

		button(Repository.conversation_resolved, "conversation_resolved").click();
		logger.info("click on conversation resolved");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(Repository.conversation_resolvedtxt)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.conversation_resolvedtxt));
			textbox(Repository.conversation_resolvedtxt).isEnabled();
			System.out.println("Resolved by You. is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.conversation_resolvedtxt));
			System.out.println("Resolved by You. is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.conversation_resolved, "conversation_resolved").click();
		logger.info("click on conversolved");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(Repository.conversation_reopentxt)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.conversation_reopentxt));
			textbox(Repository.conversation_reopentxt).isEnabled();
			System.out.println("Reopened by You. is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.conversation_reopentxt));
			System.out.println("Reopened by You. is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Toll_Not_Allowed_in_Group(Map<String, String> map) throws Exception {

		automationBlock_login(map);
		waits(2);
		button(repository.composebutton, "click on compose button").click();
		logger.info("click on Compose Button");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.contactSearchInputTxt).sendKeys("(555) 573-2467");
		logger.info("Input Text for contact Search");
		waits(2);
		logger.info("wait for 2 sec");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contact Search output");
		waits(2);
		logger.info("wait for 2 sec");

		textbox(repository.contactSearchInputTxt).sendKeys("Toll");
		logger.info("Input Text for contact Search");
		waits(2);
		logger.info("wait for 2 sec");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contact Search output");

		repository.Toll_Not_Allowed_in_Group.getText();
		System.out.println(repository.Toll_Not_Allowed_in_Group.getText());
		if (repository.Toll_Not_Allowed_in_Group.getText()
				.equalsIgnoreCase("Toll Free number is not allowed for group.")) {
			Assert.assertEquals(repository.Toll_Not_Allowed_in_Group.getText(),
					"Toll Free number is not allowed for group.", "Toll_Not_Allowed_in_Group is matching");
			TestUtil.takeScreenshotAtEndOfTest("Toll_Not_Allowed_in_Group");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.Toll_Not_Allowed_in_Group.getText(),
					"Toll Free number is not allowed for group.", "Toll_Not_Allowed_in_Group is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Toll_Not_Allowed_in_Group");// ScreenShot capture
		}

	}

	public void Export_Contact(Map<String, String> map) throws Exception {

		automationBlock_login(map);
		button(repository.actionbttninchat, "actionbttninchat").click();
		logger.info("click on action bttn in chat");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.exportConversation, "click on exportConversation button").click();
		logger.info("click on exportConversation Button");
		waits(2);
		logger.info("wait for 2 sec");
	}

	public void Archive_Cotact(Map<String, String> map) throws Exception {

		automationBlock_login(map);

		button(repository.actionbttninchat, "actionbttninchat").click();
		logger.info("click on action bttn in chat");
		waits(2);
		logger.info("wait for 2 sec");

		button(Repository.conversation_Archive_bttn, "click on conversation_Archive_bttn button").click();
		logger.info("click on conversation_Archive_bttn Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.Filter_Conversation, "click on Filter_Conversation button").click();
		logger.info("click on Filter_Conversation Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.Archived_filter_button, "click on Archived_filter_button button").click();
		logger.info("click on Archived_filter_button Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_apply, "click on filter_apply button").click();
		logger.info("click on filter_apply Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.actionbttninchat, "actionbttninchat").click();
		logger.info("click on action bttn in chat");
		waits(2);
		logger.info("wait for 5 sec");

		button(repository.restore_Conversation, "click on restore_Conversation button").click();
		logger.info("click on restore_Conversation Button");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Apply_all(Map<String, String> map) throws Exception {

		automationBlock_login(map);

		button(repository.Filter_Conversation, "click on Filter_Conversation button").click();
		logger.info("click on Filter_Conversation Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_clear, "click on filter_clear button").click();
		logger.info("click on filter_clear Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_apply, "click on filter_apply button").click();
		logger.info("click on filter_apply Button");
		waits(2);
		logger.info("wait for 2 sec");
	}

	public void ClaimedByMe(Map<String, String> map) throws Exception {

		automationBlock_login(map);

		button(repository.Filter_Conversation, "click on Filter_Conversation button").click();
		logger.info("click on Filter_Conversation Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_claimed_by_me, "click on filter_claimed_by_me button").click();
		logger.info("click on filter_claimed_by_me Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_apply, "click on filter_apply button").click();
		logger.info("click on filter_apply Button");
		waits(2);
		logger.info("wait for 2 sec");
	}

	public void UnClaimed(Map<String, String> map) throws Exception {

		automationBlock_login(map);

		button(repository.Filter_Conversation, "click on Filter_Conversation button").click();
		logger.info("click on Filter_Conversation Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.unclaimed, "click on unclaimed button").click();
		logger.info("click on unclaimed Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_apply, "click on filter_apply button").click();
		logger.info("click on filter_apply Button");
		waits(2);
		logger.info("wait for 2 sec");

		System.out.print(repository.no_Conversation_in_Filter.getText());
	}

	public void Unread(Map<String, String> map) throws Exception {

		automationBlock_login(map);

		button(repository.Filter_Conversation, "click on Filter_Conversation button").click();
		logger.info("click on Filter_Conversation Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.unread, "click on unread button").click();
		logger.info("click on unread Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_apply, "click on filter_apply button").click();
		logger.info("click on filter_apply Button");
		waits(2);
		logger.info("wait for 2 sec");
		System.out.print(repository.no_Conversation_in_Filter.getText());
	}

	public void Unresolved(Map<String, String> map) throws Exception {

		automationBlock_login(map);

		button(repository.Filter_Conversation, "click on Filter_Conversation button").click();
		logger.info("click on Filter_Conversation Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.unresolved, "click on unread button").click();
		logger.info("click on unresolved Button");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.filter_apply, "click on filter_apply button").click();
		logger.info("click on filter_apply Button");
		waits(2);
		logger.info("wait for 2 sec");

	}

}
