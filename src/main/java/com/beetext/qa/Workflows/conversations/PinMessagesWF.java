package com.beetext.qa.Workflows.conversations;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.PinMessageWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Pin_Messages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class PinMessagesWF extends PinMessageWFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Pin_Messages pinMessage = new Pin_Messages();
	TestBase testBase = new TestBase();

	public void Pinmessage() throws Exception {
		// clickonpinicon(map);
		selectConversation();
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		logger.info("pin message is typed");
		wait(2);
		logger.info("wait for 2 sec");
		button(pinMessage.postButton, "button").click();
		logger.info("POST button is clicked");
		wait(3);
		logger.info("wait for 3 sec");
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "pin message is matching");
			System.out.println("pin message is verified");
			logger.warn("Pin message is sending successfully ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			logger.warn("Pin message is not sending");
			TestUtil.takeScreenshotAtEndOfTest("pinmessageTestCase");// ScreenShot capture
		}

		// logout();

	}

	public void Pinmessage_agentselection() throws Exception {
		// clickonpinicon(map);
		selectConversation();
		waits(1);
		textbox(pinMessage.input_box).sendKeys("@");
		logger.info("Message box typing @");
		waits(2);
		logger.info("wait for 2 sec");
		button(pinMessage.selectAgentName, "button").click();
		logger.info("selected agent in agent list pop up");
		wait(3);
		logger.info("wait for 3 sec");
		button(pinMessage.postButton, "button").click();
		logger.info("Clicked on post button");
		wait(3);
		logger.info("wait for 3 sec");
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase("auto1")) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), "auto1", "pin message agentname is matching");
			System.out.println("pin message agent name select case is verified");
			logger.warn("Pin message  along with one agent selection case is sending successfully ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage agent name selection TestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), "auto1", "pin message is not  matching");
			logger.warn("Pin message  along with one agent selection case is failed ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage agent name selection TestCase is failed");// ScreenShot
																										// capture
		}

		// logout();
	}

	public void Pinmessage_no_Agent_Found() throws Exception {

		noAgent();

		pinMessage.no_Agent_Found.getText();
		System.out.println(pinMessage.no_Agent_Found.getText());
		if (pinMessage.no_Agent_Found.getText().equalsIgnoreCase("We didn't find any matches.")) {
			Assert.assertEquals(pinMessage.no_Agent_Found.getText(), "We didn't find any matches.",
					"We didn't find any matches.");
			System.out.println("pin message We didn't find any matches.case is verified");
			logger.warn("No Agents found default message case working fine");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage We didn't find any matches.testCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.no_Agent_Found.getText(), "We didn't find any matches.",
					"pin message We didn't find any matches. is not  matching");
			logger.warn("Defualt message 'No agents found message' is not showing ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage We didn't find any matches. TestCase is failed");// ScreenShot
																											// capture
		}

		logout();

	}

	public void Pinmessage_with_hyperLink() throws Exception {

		// clickonpinicon(map);
		selectConversation();

		String message = "pin message with hyperlink case is checking https://automation.beetexting.com/messages";
		textbox(pinMessage.input_box).sendKeys(message);
		logger.info("typing pin message with hyperlink" + message);
		wait(2);
		logger.info("wait for 2 sec");

		button(pinMessage.postButton, "button").click();
		logger.info("clicked on postButton");
		wait(3);
		logger.info("wait for 3 sec");

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message)) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message, "pin message is matching");
			System.out.println("pin message with hyperlink case is verified");
			logger.warn("Pin message  along with hyperlink case is sending successfully ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage with hyperlink case TestCase success");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message, "message is not  matching");
			logger.warn("Pin message  along with hyperlink case is not sending");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage with hyperlink case TestCase failed");// ScreenShot capture
		}

		// logout();

	}

	public void Pinmessage_with_agentName() throws Exception {
		// clickonpinicon(map);
		selectConversation();
		String message = "pin message with hyperlink case is checking ";
		textbox(pinMessage.input_box).sendKeys(message);
		logger.info("chat input box typed pin message " + message);
		wait(2);

		textbox(pinMessage.input_box).sendKeys("@");
		logger.info("chat input box typed @ symbol");
		wait(2);
		logger.info("wait for 2 sec");

		button(pinMessage.selectAgentName, "button").click();
		logger.info("selected agent in agent list pop up");
		wait(3);
		logger.info("wait for 3 sec");

		button(pinMessage.postButton, "button").click();
		logger.info("Clicked on post button");
		wait(3);
		logger.info("wait for 3 sec");

		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase(message + "automations")) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), message + "automations", "pin message is matching");
			System.out.println("pin message with hyperlink case is verified");
			logger.warn("Pin message  along with agent name selection case working fine ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage with agent name case TestCase success");// ScreenShot capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), message + "automations", "message is not  matching");
			logger.warn("Pin message  along with agent name selection case failed ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage with agent name TestCase failed");// ScreenShot capture
		}

		// logout();

	}

	public void Pinmessage_select_morethanoneagentname() throws Exception {
		// clickonpinicon(map);
		selectConversation();
		wait(1);
		textbox(pinMessage.input_box).sendKeys("@");
		logger.info("chat input typing @");
		waits(2);
		logger.info("wait for 2 sec");
		button(pinMessage.selectAgentName, "button").click();
		logger.info("selected first agent in agent list");
		wait(3);
		logger.info("wait for 3 sec");
		textbox(pinMessage.input_box).sendKeys("@");
		logger.info("chat input typing @");
		waits(2);
		logger.info("wait for 2 sec");
		button(pinMessage.selectsecondAgentName, "button").click();
		logger.info("selecting second agent in agent list");
		wait(3);
		logger.info("wait for 3 sec");
		button(pinMessage.postButton, "button").click();
		logger.info("Post button is clicked");
		wait(3);
		logger.info("wait for 3 sec");
		pinMessage.pinmessage.getText();
		System.out.println(pinMessage.pinmessage.getText());
		if (pinMessage.pinmessage.getText().equalsIgnoreCase("auto1 dummyagent")) {
			Assert.assertEquals(pinMessage.pinmessage.getText(), "auto1 dummyagent",
					"pin message agentname is matching");
			System.out.println("pin message more than one agent name select case is verified");
			logger.warn("Pin message  along with add more than one  agent name selection case working fine ");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage more than one agent name selection TestCase");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(pinMessage.pinmessage.getText(), "auto1 dummyagent", "pin message is not  matching");
			logger.warn("Pin message  along with add more than one  agent name selection case failed");
			TestUtil.takeScreenshotAtEndOfTest("pinmessage more than one agent name selection TestCase is failed");// ScreenShot
																													// capture
		}
        logout();
		

	}

}
