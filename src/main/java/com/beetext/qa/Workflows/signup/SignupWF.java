package com.beetext.qa.Workflows.signup;

import java.util.ArrayList;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.WFCommonMethod.Signup;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class SignupWF extends Signup {
	String org, workmail, confirmEmail, mobileNumber, signuppassword, retypePassword, otptext, Email_otptext, user_id,
			Passsword_ID;

	String Password = "Bee@1234";

	String Name = "Hyderabad";
	
	public void mobile_otp6(String value) throws Exception{
		
		driver.findElement(By.xpath("//div/input[6]")).sendKeys(value);
		waitAndLog(2);
	}

	public void signup_allvalid(Map<String, String> map) throws Exception {

		String org = CommonMethods.exportData("Org", 1);
		map.put("org", org);

		String Workmail = CommonMethods.exportData("WorkEmail", 1);
		map.put("Workmail", Workmail);

		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys(org);
		logger.info("enter the org name");

		textbox(Repository.workEmail).sendKeys(Workmail);
		logger.info("enter workmail");

		textbox(Repository.name).sendKeys(Name);
		logger.info("enter the name");

		textbox(Repository.signuppassword).sendKeys(Password);
		logger.info("enter the signup pasword");
		waitAndLog(2);
		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup checkbox");
		waitAndLog(2);

		button(Repository.letsgobtn, "button").click();
		logger.info("click on letsgo button");
		waitAndLog(5);

	}

	public void orgErrorValidation(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		workmail = (String) map.get("Workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys("");
		logger.info("enter org name");

		textbox(Repository.workEmail).sendKeys("abc@gmail.com");
		logger.info("enter workmail");

		Repository.OrgError.getText();
		System.out.println(Repository.OrgError.getText());
		if (Repository.OrgError.getText().equalsIgnoreCase("Organization Name is required.")) {
			Assert.assertEquals(Repository.OrgError.getText(), "Organization Name is required.",
					"OrgError are matching");
			TestUtil.takeScreenshotAtEndOfTest("OrgErrorTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.OrgError.getText(), "Organization Name is required.",
					"OrgError are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("OrgErrorTestCase");// ScreenShot capture

		}

	}

	public void Orgnamealreadyexist(Map<String, String> map) throws Exception {

		Signup_login_orgnameAlreadyExist(map);

		Repository.Orgnamealreadyexist.getText();
		System.out.println(Repository.Orgnamealreadyexist.getText());
		if (Repository.Orgnamealreadyexist.getText()
				.equalsIgnoreCase("Organization name already exists. Please use another organization name.")) {
			Assert.assertEquals(Repository.Orgnamealreadyexist.getText(),
					"Organization name already exists. Please use another organization name.",
					"Orgnamealreadyexist are matching");
			TestUtil.takeScreenshotAtEndOfTest("Orgnamealreadyexist");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.Orgnamealreadyexist.getText(),
					"Organization name already exists. Please use another organization name.",
					"Orgnamealreadyexist are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Orgnamealreadyexist");// ScreenShot capture

		}

	}

	public void org2charerr(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		logger.info("org");
		workmail = (String) map.get("Workmail");
		logger.info("workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys("ma");
		logger.info("enter orgname");

		textbox(Repository.workEmail).sendKeys("abc@gmail.com");
		logger.info("enter workmail");

		Repository.Orgnameneedmorechar.getText();
		System.out.println(Repository.Orgnameneedmorechar.getText());
		if (Repository.Orgnameneedmorechar.getText()
				.equalsIgnoreCase("Your organization name needs 3 or more characters.")) {
			Assert.assertEquals(Repository.Orgnameneedmorechar.getText(),
					"Your organization name needs 3 or more characters.", "Orgnameneedmorechar is matching");
			TestUtil.takeScreenshotAtEndOfTest("emailvalidationTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.Orgnameneedmorechar.getText(),
					"Your organization name needs 3 or more characters.", "Orgnameneedmorechar is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("OrgnameneedmorecharTestCase");// ScreenShot capture
		}

	}

	public void email_alreadyexisterr(Map<String, String> map) throws Exception {

		Signup_login_emailAlreadyExist(map);

		Repository.EmailalreExisterr.getText();
		System.out.println(Repository.EmailalreExisterr.getText());
		if (Repository.EmailalreExisterr.getText()
				.equalsIgnoreCase("This email ID is already in use. Sign in or try a different email ID.")) {
			Assert.assertEquals(Repository.EmailalreExisterr.getText(),
					"This email ID is already in use. Sign in or try a different email ID.",
					"Orgnamealreadyexist are matching");
			TestUtil.takeScreenshotAtEndOfTest("EmailalreExisterr");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.EmailalreExisterr.getText(),
					"This email ID is already in use. Sign in or try a different email ID.",
					"Orgnamealreadyexist are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("EmailalreExisterr");// ScreenShot capture

		}

		if (verifyElementIsEnabled(Repository.EmailalreExisterr)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.EmailalreExisterr));
			textbox(Repository.EmailalreExisterr).isEnabled();
			System.out.println("EmailalreExisterr is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.EmailalreExisterr));
			System.out.println("EmailalreExisterr is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void workEmailErrorValidation(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		workmail = (String) map.get("Workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("enter the signup link");

		textbox(Repository.orgName).sendKeys("Manipal");
		logger.info("enter org name");

		textbox(Repository.workEmail).sendKeys("");
		logger.info("enter workmail");

		Repository.name.click();
		logger.info("name");

		Repository.workEmailError.getText();
		System.out.println(Repository.workEmailError.getText());
		if (Repository.workEmailError.getText().equalsIgnoreCase("Work Email is required.")) {
			Assert.assertEquals(Repository.workEmailError.getText(), "Work Email is required.",
					"workEmailError are matching");
			TestUtil.takeScreenshotAtEndOfTest("workEmailErrorTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.workEmailError.getText(), "Work Email is required.",
					"workEmailError are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("workEmailErrorTestCase");// ScreenShot capture
		}
	}

	public void emailvalidation(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		workmail = (String) map.get("Workmail");

		link(Repository.signupPage, "signuplink").click();
		logger.info("enter signup link");

		textbox(Repository.orgName).sendKeys("Manipal");
		logger.info("enter org name");

		textbox(Repository.workEmail).sendKeys("abc@gmailcom");
		logger.info("enter workmail");

		Repository.name.sendKeys(Name);
		logger.info("enter name");

		Repository.emailvalidation.getText();
		System.out.println(Repository.emailvalidation.getText());
		if (Repository.emailvalidation.getText().equalsIgnoreCase("Email not valid.")) {
			Assert.assertEquals(Repository.emailvalidation.getText(), "Email not valid.",
					"emailvalidation is matching");
			TestUtil.takeScreenshotAtEndOfTest("emailvalidationTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.emailvalidation.getText(), "Email not valid.",
					"emailvalidation is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("emailvalidationTestCase");// ScreenShot capture
		}

	}

	public void SignuppasswordErrorValidation(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		workmail = (String) map.get("Workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		Repository.signuppassword.sendKeys("");
		logger.info("enter signup password");

		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup checkbox");

		Repository.passwordRequiredError.getText();
		System.out.println(Repository.passwordRequiredError.getText());
		if (Repository.passwordRequiredError.getText().equalsIgnoreCase("Password Number is required.")) {
			Assert.assertEquals(Repository.passwordRequiredError.getText(), "Password Number is required.",
					"passwordRequiredError are matching");
			TestUtil.takeScreenshotAtEndOfTest("passwordRequiredErrorTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.passwordRequiredError.getText(), "Password Number is required.",
					"passwordRequiredError are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("passwordRequiredErrorTestCase");// ScreenShot capture
		}
	}

	public void Passwordmustcharacters(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		workmail = (String) map.get("Workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		Repository.signuppassword.sendKeys("123456");
		logger.info("enter the signup password");

		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup check box");

		Repository.Passwordmustcharacters.getText();
		System.out.println(Repository.Passwordmustcharacters.getText());
		if (Repository.Passwordmustcharacters.getText().equalsIgnoreCase("Password must be at least 8 characters")) {
			Assert.assertEquals(Repository.Passwordmustcharacters.getText(), "Password must be at least 8 characters",
					"Passwordmustcharacters is matching");
			TestUtil.takeScreenshotAtEndOfTest("PasswordmustcharactersTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(Repository.Passwordmustcharacters.getText(),
					"Password must be at least 8 characters", "Passwordmustcharacters is not matching");
			TestUtil.takeScreenshotAtEndOfTest("PasswordmustcharactersTestCase");// ScreenShot capture
		}

	}

	public void Namemust3char(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		workmail = (String) map.get("Workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		Repository.name.sendKeys("ab");
		logger.info("enter the name");

		Repository.signuppassword.sendKeys("123456789");
		logger.info("enter the signup password");

		Repository.Namemust3char.getText();
		System.out.println(Repository.Namemust3char.getText());
		if (Repository.Namemust3char.getText().equalsIgnoreCase("Name must be at least 3 characters.")) {
			Assert.assertEquals(Repository.Namemust3char.getText(), "Name must be at least 3 characters.",
					"Namemust3char is matching");
			TestUtil.takeScreenshotAtEndOfTest("Namemust3charTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(Repository.Namemust3char.getText(), "Name must be at least 3 characters.",
					"Namemust3char is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Namemust3charTestCase");// ScreenShot capture
		}

	}

	public void Plzreadtermsandcond(Map<String, String> map) throws Exception {

		org = (String) map.get("org");
		workmail = (String) map.get("Workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys("Manipal");
		logger.info("Enter the org name");

		textbox(Repository.workEmail).sendKeys("abc@gmail.com");
		logger.info("workmail");

		Repository.name.sendKeys("Nani");
		logger.info("enter the name");

		Repository.signuppassword.sendKeys("123456789");
		logger.info("enter Signup password");
		waitAndLog(2);
		button(Repository.letsgobtn, "button").click();
		logger.info("click on letsgo bttn");

		Repository.Plzreadtermsandcond.getText();
		System.out.println(Repository.Plzreadtermsandcond.getText());
		if (Repository.Plzreadtermsandcond.getText()
				.equalsIgnoreCase("Please read the terms of service and verify that you agree")) {
			Assert.assertEquals(Repository.Plzreadtermsandcond.getText(),
					"Please read the terms of service and verify that you agree", "Plzreadtermsandcond is matching");
			TestUtil.takeScreenshotAtEndOfTest("PlzreadtermsandcondTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(Repository.Plzreadtermsandcond.getText(),
					"Please read the terms of service and verify that you agree",
					"Plzreadtermsandcond is not matching");
			TestUtil.takeScreenshotAtEndOfTest("PlzreadtermsandcondTestCase");// ScreenShot capture
		}

	}

	public void termsCond_Privacypolicy(Map<String, String> map) throws Exception {

		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		org = (String) map.get("org");
		logger.info("org name");

		workmail = (String) map.get("Workmail");
		logger.info("workmail");

		logger.info("Enter name");

		logger.info("password");

		textbox(Repository.orgName).sendKeys(org);
		logger.info("enter org name");

		textbox(Repository.workEmail).sendKeys(workmail);
		logger.info("enter workmail");

		Repository.name.sendKeys(Name);
		logger.info("enter name");

		Repository.signuppassword.sendKeys(Password);
		logger.info("Enter the signup password");

		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on checkbox");

		link(Repository.TermsofService, "terms of service").click();
		logger.info("click on terms and conditions link");

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));
		String Servicetext = link(Repository.Pricing, "Pricing").getText();
		if (!(Servicetext.isEmpty())) {
			link(Repository.Pricing, "Pricing").click();
		}
		driver.close();
		driver.switchTo().window(tabs1.get(0));

		link(Repository.PrivacyPolicy, "Privacy Policy").click();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		String Privacytext = link(Repository.Pricing, "Pricing").getText();
		System.out.println("text---?" + Privacytext);
		if (!(Privacytext.isEmpty()))
			link(Repository.Pricing, "Pricing").click();
	}

	public void letsgobttn_isenabled(Map<String, String> map) throws Exception {
		org = (String) map.get("org");
		logger.info("Enter org name");
		workmail = (String) map.get("Workmail");
		logger.info("Enter workmail");
		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		if (verifyElementIsEnabled(Repository.letsgobtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.letsgobtn));
			button(Repository.letsgobtn, "button").isEnabled();
			System.out.println("letsgobtn is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.letsgobtn));
			System.out.println("letsgobtn is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void Allvalid_details(Map<String, String> map) throws Exception {

		Signup_login_allValid(map);

		if (verifyElementIsEnabled(Repository.orgcresucctoastermsg)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.orgcresucctoastermsg));
			textbox(Repository.orgcresucctoastermsg).isEnabled();
			System.out.println("orgcresucctoastermsg is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.orgcresucctoastermsg));
			System.out.println("orgcresucctoastermsg is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void RedirecttoEmaNum(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		button(Repository.resend, "button").click();
		logger.info("click on resend");

		System.out.println(Repository.Email_ResendToaster.getText());
		if (Repository.Email_ResendToaster.getText().equalsIgnoreCase(
				"Verification resent. Please check junk and add hello@beetexting.com to your contacts.")) {
			Assert.assertEquals(Repository.Email_ResendToaster.getText(),
					"Verification resent. Please check junk and add hello@beetexting.com to your contacts.",
					"Verification resent. Please check junk and add hello@beetexting.com to your contacts.");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotpTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.Email_ResendToaster.getText(),
					"Verification resent. Please check junk and add hello@beetexting.com to your contacts.",
					"Verification resent. Please check junk and add hello@beetexting.com to your contacts.");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotptestcase");// ScreenShot capture
		}

	}

	public void verifybttndisable(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");
		waitAndLog(2);

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");

		if (verifyElementIsEnabled(Repository.verify_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.verify_bttn));
			button(Repository.verify_bttn, "button").isEnabled();
			System.out.println("verify_bttn is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.verify_bttn));
			System.out.println("verify_bttn is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void verifybttnEnable(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("8");

		if (verifyElementIsEnabled(Repository.verify_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.verify_bttn));
			button(Repository.verify_bttn, "button").isEnabled();
			System.out.println("verify_bttn is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.verify_bttn));
			System.out.println("verify_bttn is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void OtpInvalidErr(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("5");

		Repository.OtpInvalidErr.getText();
		System.out.println(Repository.OtpInvalidErr.getText());
		if (Repository.OtpInvalidErr.getText().equalsIgnoreCase("Invalid Code.")) {
			Assert.assertEquals(Repository.OtpInvalidErr.getText(), "Invalid Code.", "OtpInvalidErr is matching");
			TestUtil.takeScreenshotAtEndOfTest("OtpInvalidErrTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.OtpInvalidErr.getText(), "Invalid Code.",
					"OtpInvalidErr is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("OtpInvalidErrTestCase");// ScreenShot capture
		}

	}

	public void Dontallowbelow6digits(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter the mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");

		if (verifyElementIsEnabled(Repository.verify_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.verify_bttn));
			button(Repository.verify_bttn, "button").isEnabled();
			System.out.println("verify_bttn is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.verify_bttn));
			System.out.println("verify_bttn is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Resendotp(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter the mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");

		button(Repository.resend, "button").click();
		logger.info("click on resend");

		if (verifyElementIsEnabled(Repository.MobOtp_ResendToaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.MobOtp_ResendToaster));
			textbox(Repository.MobOtp_ResendToaster).isEnabled();
			System.out.println("MobOtp_ResendToaster is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.MobOtp_ResendToaster));
			System.out.println("ResendotpToastermsg is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void MobOtp_VerifiedToaster(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		if (verifyElementIsEnabled(Repository.MobVerifiedToaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.MobVerifiedToaster));
			textbox(Repository.MobVerifiedToaster).isEnabled();
			System.out.println("MobVerifiedToaster is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.MobVerifiedToaster));
			System.out.println("MobVerifiedToaster is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Email_verifyDisable(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);

		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("enter verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		if (verifyElementIsEnabled(Repository.Email_VerifyBttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Email_VerifyBttn));
			button(Repository.Email_VerifyBttn, "button").isEnabled();
			System.out.println("Email_VerifyBttn is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Email_VerifyBttn));
			System.out.println("Email_VerifyBttn is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Email_verifyEnable(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter the mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("3");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		if (verifyElementIsEnabled(Repository.Email_VerifyBttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Email_VerifyBttn));
			button(Repository.Email_VerifyBttn, "button").isEnabled();
			System.out.println("Email_VerifyBttn is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Email_VerifyBttn));
			System.out.println("Email_VerifyBttn is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void email_Verification(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(3);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

	}

	public void Emailotp_Dontallowbelow6digits(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(3);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("2");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		if (verifyElementIsEnabled(Repository.Email_VerifyBttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Email_VerifyBttn));
			button(Repository.Email_VerifyBttn, "button").isEnabled();
			System.out.println("Email_VerifyBttn is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Email_VerifyBttn));
			System.out.println("Email_VerifyBttn is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Email_OtpInvalidErr(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(3);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("2");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		Repository.Email_OtpInvalidErr.getText();
		System.out.println(Repository.Email_OtpInvalidErr.getText());
		if (Repository.Email_OtpInvalidErr.getText().equalsIgnoreCase("Invalid Code.")) {
			Assert.assertEquals(Repository.Email_OtpInvalidErr.getText(), "Invalid Code.",
					"Email_OtpInvalidErr is matching");
			TestUtil.takeScreenshotAtEndOfTest("Email_OtpInvalidErrTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.Email_OtpInvalidErr.getText(), "Invalid Code.",
					"Email_OtpInvalidErr is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Email_OtpInvalidErrTestCase");// ScreenShot capture
		}

	}

	public void EmailOtp_VerifiedToaster(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(3);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(3);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		if (verifyElementIsEnabled(Repository.Email_VerifiedToaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Email_VerifiedToaster));
			button(Repository.Email_VerifiedToaster, "button").isEnabled();
			System.out.println("Email_VerifiedToaster is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Email_VerifiedToaster));
			System.out.println("Email_VerifiedToaster is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Email_ResendotpToaster(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(5);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(3);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		if (verifyElementIsEnabled(Repository.Email_ResendToaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Email_ResendToaster));
			button(Repository.Email_ResendToaster, "button").isEnabled();
			System.out.println("Email_ResendToaster is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Email_ResendToaster));
			System.out.println("Email_ResendToaster is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void loginwith_usercredentialwithotppending(Map<String, String> map) throws Exception {

		workmail = (String) map.get("Workmail");

		textbox(Repository.user_id).sendKeys(workmail);
		logger.info("enter workmail");

		Repository.Passsword_ID.sendKeys(Password);
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on lets go bttn");

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter the mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("3");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("7");
		waitAndLog(2);
		if (verifyElementIsEnabled(Repository.verify_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.verify_bttn));
			button(Repository.verify_bttn, "button").isEnabled();
			System.out.println("verify_bttn is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.verify_bttn));
			System.out.println("verify_bttn is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void loginwith_usercredentialwithotppending_Refresh(Map<String, String> map) throws Exception {

		workmail = (String) map.get("Workmail");

		textbox(Repository.user_id).sendKeys(workmail);
		logger.info("enter workmail");

		Repository.Passsword_ID.sendKeys(Password);
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on lets go bttn");
		waitAndLog(2);

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("3");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		driver.navigate().refresh();

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");
		waitAndLog(2);
		if (verifyElementIsEnabled(Repository.resend)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.resend));
			button(Repository.resend, "button").isEnabled();
			System.out.println("resend is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.resend));
			System.out.println("resend is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void loginwith_usercredentialwithaccesscodeppending(Map<String, String> map) throws Exception {

		workmail = (String) map.get("Workmail");

		textbox(Repository.user_id).sendKeys(workmail);
		logger.info("enter workmail");

		Repository.Passsword_ID.sendKeys(Password);
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on lets go bttn");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("3");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("7");
		waitAndLog(2);
		if (verifyElementIsEnabled(Repository.verify_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.verify_bttn));
			textbox(Repository.verify_bttn).isEnabled();
			System.out.println("verify_bttn is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.verify_bttn));
			System.out.println("verify_bttn is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void loginwith_usercredentialwithaccesscodeppending_refresh(Map<String, String> map) throws Exception {

		workmail = (String) map.get("Workmail");

		textbox(Repository.user_id).sendKeys(workmail);
		logger.info("enter workmail");

		Repository.Passsword_ID.sendKeys(Password);
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on lets go bttn");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("3");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		driver.navigate().refresh();

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");
		waitAndLog(2);
		if (verifyElementIsEnabled(Repository.Next_PaymentInformation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			button(Repository.Next_PaymentInformation, "button").isEnabled();
			System.out.println("Next_PaymentInformation is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			System.out.println("Next_PaymentInformation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void NextDiable_PaymentInformation(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		if (verifyElementIsEnabled(Repository.Next_PaymentInformation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			button(Repository.Next_PaymentInformation, "button").isEnabled();
			System.out.println("Next_PaymentInformation is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			System.out.println("Next_PaymentInformation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void valid_CreditCardDetails(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		textbox(Repository.name_onthe_card).sendKeys("HBVGU");
		logger.info("ente the name on the credit card");

		textbox(Repository.CardNumber).sendKeys("4242424242424242");
		logger.info("enter the card number");

		textbox(Repository.ExpiryDate).sendKeys("0623");
		logger.info("enter the expiry date");

		textbox(Repository.CC_Code).sendKeys("1234");
		logger.info("enter the cc code");

		checkbox(Repository.Accept_autopayments, "checkbox").click();
		logger.info("click on accept auto payments");

		button(Repository.Next_PaymentInformation, "button").click();
		logger.info("click on next payment");

		if (verifyElementIsEnabled(Repository.Create_Newphonenumber)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Create_Newphonenumber));
			button(Repository.Create_Newphonenumber, "button").isEnabled();
			System.out.println("Create_Newphonenumber is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Create_Newphonenumber));
			System.out.println("Create_Newphonenumber is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Invalid_CreditCardNumber(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		textbox(Repository.CardNumber).sendKeys("424242");
		logger.info("enter the card number");

		Repository.CardNum_Invalid.getText();
		System.out.println(Repository.CardNum_Invalid.getText());
		if (Repository.CardNum_Invalid.getText().equalsIgnoreCase("Card number is invalid.")) {
			Assert.assertEquals(Repository.CardNum_Invalid.getText(), "Card number is invalid.",
					"CardNum_Invalid is matching");
			TestUtil.takeScreenshotAtEndOfTest("CardNum_InvalidTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.CardNum_Invalid.getText(), "Card number is invalid.",
					"CardNum_Invalid is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("CardNum_InvalidTestCase");// ScreenShot capture
		}

	}

	public void Invalid_ExpiryDate(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		textbox(Repository.ExpiryDate).sendKeys("111");
		logger.info("enter the expiry date");

		Repository.ExpDate_Invalid.getText();
		System.out.println(Repository.ExpDate_Invalid.getText());
		if (Repository.ExpDate_Invalid.getText().equalsIgnoreCase("Expiration date is invalid.")) {
			Assert.assertEquals(Repository.ExpDate_Invalid.getText(), "Expiration date is invalid.",
					"ExpDate_Invalid is matching");
			TestUtil.takeScreenshotAtEndOfTest("ExpDate_InvalidTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.CardNum_Invalid.getText(), "Expiration date is invalid.",
					"ExpDate_Invalid is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("ExpDate_InvalidTestCase");// ScreenShot capture
		}

	}

	public void Invalid_CvvCode(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		textbox(Repository.CC_Code).sendKeys("11");
		logger.info("enter the cc code");

		Repository.Cvv_invalid.getText();
		System.out.println(Repository.Cvv_invalid.getText());
		if (Repository.Cvv_invalid.getText().equalsIgnoreCase("Security code is invalid.")) {
			Assert.assertEquals(Repository.Cvv_invalid.getText(), "Security code is invalid.",
					"Cvv_invalid is matching");
			TestUtil.takeScreenshotAtEndOfTest("Cvv_invalidTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.Cvv_invalid.getText(), "Security code is invalid.",
					"Cvv_invalid is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Cvv_invalidTestCase");// ScreenShot capture
		}

	}

	public void Cvv_Mustbe3char(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		textbox(Repository.CC_Code).sendKeys("11");
		logger.info("enter the cc code");

		Repository.Cvv_Mustbe3char.getText();
		System.out.println(Repository.Cvv_Mustbe3char.getText());
		if (Repository.Cvv_Mustbe3char.getText().equalsIgnoreCase("Security code must be at least 3 digits.")) {
			Assert.assertEquals(Repository.Cvv_Mustbe3char.getText(), "Security code must be at least 3 digits.",
					"Cvv_Mustbe3char is matching");
			TestUtil.takeScreenshotAtEndOfTest("Cvv_Mustbe3charTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.Cvv_Mustbe3char.getText(), "Security code must be at least 3 digits.",
					"Cvv_Mustbe3char is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Cvv_Mustbe3charTestCase");// ScreenShot capture
		}

	}

	public void uncheck_autopayments(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		textbox(Repository.name_onthe_card).sendKeys("HBVGU");
		logger.info("enter the name on the card");

		textbox(Repository.CardNumber).sendKeys("4242424242424242");
		logger.info("enter the card number");

		textbox(Repository.ExpiryDate).sendKeys("0623");
		logger.info("enter expiry date");

		textbox(Repository.CC_Code).sendKeys("1234");
		logger.info("enter the cc code");

		if (verifyElementIsEnabled(Repository.Next_PaymentInformation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			button(Repository.Next_PaymentInformation, "button").isEnabled();
			System.out.println("Next_PaymentInformation is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			System.out.println("Next_PaymentInformation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void Validdetail_NextEnablePayments(Map<String, String> map) throws Exception {

		Signup_Randomlogin(map);
		waitAndLog(2);

		String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("enter mobile number");

		button(Repository.send_verfity_code, "button").click();
		logger.info("click on verify code");
		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		waitAndLog(2);

		textbox(Repository.mobile_otp1).sendKeys("1");
		textbox(Repository.mobile_otp2).sendKeys("2");
		textbox(Repository.mobile_otp3).sendKeys("3");
		textbox(Repository.mobile_otp4).sendKeys("4");
		textbox(Repository.mobile_otp5).sendKeys("5");
		mobile_otp6("6");

		textbox(Repository.name_onthe_card).sendKeys("HBVGU");
		logger.info("enter the name on the card");

		textbox(Repository.CardNumber).sendKeys("4242424242424242");
		logger.info("enter the cad number");

		textbox(Repository.ExpiryDate).sendKeys("0623");
		logger.info("enter the expiry date");

		textbox(Repository.CC_Code).sendKeys("1234");
		logger.info("enter cvv");

		checkbox(Repository.Accept_autopayments, "checkbox").click();
		logger.info("click on accept auto payments");

		if (verifyElementIsEnabled(Repository.Next_PaymentInformation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			button(Repository.Next_PaymentInformation, "button").isEnabled();
			System.out.println("Next_PaymentInformation is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			System.out.println("Next_PaymentInformation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void validate_forgotpassword_enteremail(Map<String, String> map) throws Exception {

		waitAndLog(2);

		link(Repository.forgot_password, "link").click();
		logger.info("click on forgot password");

		if (verifyElementIsEnabled(Repository.forgot_passwordemail)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.forgot_passwordemail));
			textbox(Repository.forgot_passwordemail).isEnabled();
			System.out.println("forgot_passwordemail is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.forgot_passwordemail));
			System.out.println("forgot_passwordemail is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		textbox(Repository.forgot_passwordemail).sendKeys("abc@gmail.com");
		logger.info("enter the valid mail get forgot password link");

		button(Repository.forgotpasswordemail_sendbttn, "button").click();
		logger.info("click on send");

		if (verifyElementIsEnabled(Repository.forgotpassword_emailsenttoaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.forgotpassword_emailsenttoaster));
			textbox(Repository.forgotpassword_emailsenttoaster).isEnabled();
			System.out.println("forgotpassword_emailsenttoaster is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.forgotpassword_emailsenttoaster));
			System.out.println("forgotpassword_emailsenttoaster is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void validate_forgotpasswordsendemail_invalidtoaster(Map<String, String> map) throws Exception {

		link(Repository.forgot_password, "link").click();
		logger.info("click on forgot password");

		textbox(Repository.forgot_passwordemail).sendKeys("abcff@gmail.com");
		logger.info("enter the mail");

		button(Repository.forgotpasswordemail_sendbttn, "button").click();
		logger.info("click on send bttn");

		if (verifyElementIsEnabled(Repository.forgotpasswordemail_invalidmailtoaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.forgotpasswordemail_invalidmailtoaster));
			textbox(Repository.forgotpasswordemail_invalidmailtoaster).isEnabled();
			System.out.println("forgotpasswordemail_invalidmailtoaster is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.forgotpasswordemail_invalidmailtoaster));
			System.out.println("forgotpasswordemail_invalidmailtoaster is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void validate_forgotpasswordsendemail_invalidmail(Map<String, String> map) throws Exception {

		Repository.user_id.sendKeys("agh");
		logger.info("enter the username");

		Repository.Passsword_ID.sendKeys("Bee@1234");
		logger.info("enter password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on login bttn");

		Repository.login_invalidmail.getText();
		System.out.println(Repository.login_invalidmail.getText());
		if (Repository.login_invalidmail.getText().equalsIgnoreCase("Email not valid.")) {
			Assert.assertEquals(Repository.login_invalidmail.getText(), "Email not valid.",
					"login_invalidmail are matching");
			TestUtil.takeScreenshotAtEndOfTest("login_invalidmailTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.login_invalidmail.getText(), "Email not valid.",
					"login_invalidmail are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("login_invalidmailTestCase");// ScreenShot capture
		}
	}

	public void logindetailwithinvalidpassword(Map<String, String> map) throws Exception {

		Repository.user_id.sendKeys("agh@gmail.com");
		logger.info("neter the user name");

		Repository.Passsword_ID.sendKeys("147852147");
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on lets go bttn");

		if (verifyElementIsEnabled(Repository.logindetailsinvalid)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.logindetailsinvalid));
			textbox(Repository.logindetailsinvalid).isEnabled();
			System.out.println("logindetailsinvalid is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.logindetailsinvalid));
			System.out.println("logindetailsinvalid is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void login_withInvaliddetails(Map<String, String> map) throws Exception {

		Repository.user_id.sendKeys("asdgh@gmail.com");
		logger.info("enter the username");

		Repository.Passsword_ID.sendKeys("147852147");
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on login bttn");

		if (verifyElementIsEnabled(Repository.logindetailsinvalid)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.logindetailsinvalid));
			textbox(Repository.logindetailsinvalid).isEnabled();
			System.out.println("logindetailsinvalid is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.logindetailsinvalid));
			System.out.println("logindetailsinvalid is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void loginwithNull_letsgo_disable(Map<String, String> map) throws Exception {

		if (verifyElementIsEnabled(Repository.loginbttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.loginbttn));
			textbox(Repository.loginbttn).isEnabled();
			System.out.println("loginbttn is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.loginbttn));
			System.out.println("loginbttn is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void login_withsinglechar_error(Map<String, String> map) throws Exception {

		Repository.user_id.sendKeys("agh");
		logger.info("enter the username");

		Repository.Passsword_ID.sendKeys("14");
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on login bttn");

		Repository.login_invalidmail.getText();
		System.out.println(Repository.login_invalidmail.getText());
		if (Repository.login_invalidmail.getText().equalsIgnoreCase("Email not valid.")) {
			Assert.assertEquals(Repository.login_invalidmail.getText(), "Email not valid.",
					"login_invalidmail are matching");
			TestUtil.takeScreenshotAtEndOfTest("login_invalidmailTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.login_invalidmail.getText(), "Email not valid.",
					"login_invalidmail are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("login_invalidmailTestCase");// ScreenShot capture
		}

		Repository.loginpassword_must8char.getText();
		System.out.println(Repository.loginpassword_must8char.getText());
		if (Repository.loginpassword_must8char.getText().equalsIgnoreCase("Password must be at least 8 characters.")) {
			Assert.assertEquals(Repository.loginpassword_must8char.getText(), "Password must be at least 8 characters.",
					"loginpassword_must8char are matching");
			TestUtil.takeScreenshotAtEndOfTest("loginpassword_must8charTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Repository.loginpassword_must8char.getText(),
					"Password must be at least 8 characters.", "loginpassword_must8char are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("loginpassword_must8charTestCase");// ScreenShot capture
		}
	}

	public void loginwith_pendingpayment(Map<String, String> map) throws Exception {

		workmail = (String) map.get("Workmail");

		textbox(Repository.user_id).sendKeys(workmail);
		logger.info("enter the username");

		Repository.Passsword_ID.sendKeys(Password);
		logger.info("enter the password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on login bttn");
		waitAndLog(3);

		if (verifyElementIsEnabled(Repository.Next_PaymentInformation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			button(Repository.Next_PaymentInformation, "button").isEnabled();
			System.out.println("Next_PaymentInformation is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Next_PaymentInformation));
			System.out.println("Next_PaymentInformation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		textbox(Repository.name_onthe_card).sendKeys("HBVGU");
		logger.info("enter the name on the card");
		waitAndLog(5);

		textbox(Repository.CardNumber).sendKeys("4242424242424242");
		logger.info("enter the card number");

		textbox(Repository.ExpiryDate).sendKeys("0623");
		logger.info("enter the expiry date");

		textbox(Repository.CC_Code).sendKeys("1234");
		logger.info("enter the cc code");
		waitAndLog(3);

		button(Repository.Accept_autopayments, "click on auto check payments").click();
		logger.info("click on accept auto payments");
		waitAndLog(3);

		button(Repository.Next_PaymentInformation, "Next payment button").click();
		logger.info("click on next payment");
		waitAndLog(3);

	}

	public void loginwith_deptpending(Map<String, String> map) throws Exception {

		workmail = (String) map.get("Workmail");

		textbox(Repository.user_id).sendKeys(workmail);
		logger.info("username");

		Repository.Passsword_ID.sendKeys(Password);

		button(Repository.loginbttn, "button").click();
		logger.info("click on login bttn");
		waitAndLog(2);

		if (verifyElementIsEnabled(Repository.Create_Newphonenumber)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Create_Newphonenumber));
			button(Repository.Create_Newphonenumber, "button").isEnabled();
			System.out.println("Create_Newphonenumber is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Create_Newphonenumber));
			System.out.println("Create_Newphonenumber is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(Repository.Create_Newphonenumber, "button").click();
		logger.info("click on create new phone");
		waitAndLog(2);

		textbox(Repository.Create_dept).sendKeys("abcdef");
		logger.info("enter create dept name");
		waitAndLog(2);

		textbox(Repository.Create_dept_areacode).sendKeys("800");
		logger.info("enter area code");
		waitAndLog(2);

		button(Repository.Create_dept_clickon_create, "click on create").click();
		logger.info("click on create");
		waitAndLog(2);

	}

	public void loginwithvaliddetails_conversationpage(Map<String, String> map) throws Exception {

		workmail = (String) map.get("Workmail");

		textbox(Repository.user_id).sendKeys(workmail);
		logger.info("username");

		Repository.Passsword_ID.sendKeys(Password);
		logger.info("password");

		button(Repository.loginbttn, "button").click();
		logger.info("click on login bttn");
		waitAndLog(2);

		if (verifyElementIsEnabled(Repository.Msg_beetextsupport)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.Msg_beetextsupport));
			button(Repository.Msg_beetextsupport, "button").isEnabled();
			System.out.println("Msg_beetextsupport is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.Msg_beetextsupport));
			System.out.println("Msg_beetextsupport is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

}
