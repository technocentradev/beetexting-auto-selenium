package com.beetext.qa.Workflows.signup;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.signup_TCR_Pages;
import com.beetext.qa.util.TestUtil;

public class SignupTCR_WF extends Broadcast {

	String tcr_username1 = "nominee1@yopmail.com";
	String tcr_username2 = "nominee2@yopmail.com";
	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	TestBase testBase = new TestBase();
	signup_TCR_Pages tcr_Pages = new signup_TCR_Pages();

	public void nominee_User_Login_Without_Submitting_Details() throws Exception {

		textbox(tcr_Pages.user_id).sendKeys(tcr_username1);
		logger.info("Email id :" + tcr_username1);
		textbox(tcr_Pages.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);
		button(tcr_Pages.loginbttn, "login submit").click();
		waitAndLog(2);

	}

	public void nominee_User_Login_After_Submitting_Details() throws Exception {

		textbox(tcr_Pages.user_id).sendKeys(tcr_username2);
		logger.info("Email id :" + tcr_username2);
		textbox(tcr_Pages.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(tcr_Pages.loginbttn, "login submit").click();
		waitAndLog(2);

	}

	public void tcr_Message() throws Exception {

		nominee_User_Login_Without_Submitting_Details();
		waitAndLog(8);

		button(tcr_Pages.composebutton, "click on compose button").click();
		logger.info("click on compose button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(tcr_Pages.contactSearchInputTxt).sendKeys("5556216313");
		logger.info("enter the number to search contact");
		waitAndLog(2);

		link(tcr_Pages.contactSearchOutputTxt12, "Search output").click();
		logger.info("click on contact");
		waitAndLog(2);

		textbox(tcr_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(tcr_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tcr_Pages.tcr_Register)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			button(tcr_Pages.tcr_Register, "buton").isEnabled();
			logger.info("TCR Register is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			logger.info("TCR Register is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);
	}

	public void tcr_Broadcast() throws Exception {

		nominee_User_Login_Without_Submitting_Details();
		waitAndLog(2);
		button(tcr_Pages.tools_bttn, "Click on tools_bttn").click();
		waitAndLog(2);
		button(tcr_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(tcr_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(tcr_Pages.tools_BM_input_To_searchTags).sendKeys("TCR");
		logger.info("Enter data in search tags ");

		button(tcr_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(tcr_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(tcr_Pages.tools_BM_message).sendKeys(" Hello Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(tcr_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(tcr_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tcr_Pages.tcr_Register)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			button(tcr_Pages.tcr_Register, "buton").isEnabled();
			logger.info("TCR Register is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			logger.info("TCR Register is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);
	}

	public void tcr_Schedule_Message() throws Exception {

		nominee_User_Login_Without_Submitting_Details();
		waitAndLog(2);
		button(tcr_Pages.tools_bttn, "Click on tools_bttn").click();
		waitAndLog(2);
		button(tcr_Pages.tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(tcr_Pages.tools_create_SM, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(tcr_Pages.input_msg_Sent_TO).sendKeys("5555463772");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(tcr_Pages.select_Contact, "button").click();
		logger.info("select_Contact");

		textbox(tcr_Pages.SM_MSg_Textarea).sendKeys("Hello");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(tcr_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(tcr_Pages.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waitAndLog(2);
		button(tcr_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(tcr_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tcr_Pages.tcr_Register)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			button(tcr_Pages.tcr_Register, "buton").isEnabled();
			logger.info("TCR Register is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			logger.info("TCR Register is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);
	}

	public void tcr_Health_Screening() throws Exception {

		nominee_User_Login_Without_Submitting_Details();
		waitAndLog(2);
		button(tcr_Pages.tools_bttn, "Click on tools_bttn").click();
		waitAndLog(2);
		button(tcr_Pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waitAndLog(2);

		button(tcr_Pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waitAndLog(1);

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(tcr_Pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(1);

		textbox(tcr_Pages.input_To_searchTags).sendKeys("tcr");
		logger.info("Enter data in search tags ");
		waitAndLog(1);

		button(tcr_Pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		button(tcr_Pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(tcr_Pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(tcr_Pages.tcr_Register)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			button(tcr_Pages.tcr_Register, "buton").isEnabled();
			logger.info("TCR Register is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Register));
			logger.info("TCR Register is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);

	}

	public void tcr_After_Submitting_Details_Message() throws Exception {

		nominee_User_Login_After_Submitting_Details();
		waitAndLog(8);

		button(tcr_Pages.composebutton, "click on compose button").click();
		logger.info("click on compose button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(tcr_Pages.contactSearchInputTxt).sendKeys("5556216313");
		logger.info("enter the number to search contact");
		waitAndLog(2);

		link(tcr_Pages.contactSearchOutputTxt12, "Search output").click();
		logger.info("click on contact");
		waitAndLog(2);

		textbox(tcr_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(tcr_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);
	}

	public void tcr_After_Submitting_Details_Broadcast() throws Exception {

		nominee_User_Login_After_Submitting_Details();
		waitAndLog(2);
		button(tcr_Pages.tools_bttn, "Click on tools_bttn").click();
		waitAndLog(2);
		button(tcr_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(tcr_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(tcr_Pages.tools_BM_input_To_searchTags).sendKeys("TCR");
		logger.info("Enter data in search tags ");

		button(tcr_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(tcr_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(tcr_Pages.tools_BM_message).sendKeys(" Hello Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(tcr_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(tcr_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);
	}

	public void tcr_After_Submitting_Details_Schedule_Message() throws Exception {

		nominee_User_Login_After_Submitting_Details();
		waitAndLog(2);
		button(tcr_Pages.tools_bttn, "Click on tools_bttn").click();
		waitAndLog(2);
		button(tcr_Pages.tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(tcr_Pages.tools_create_SM, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(tcr_Pages.input_msg_Sent_TO).sendKeys("5555463772");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(tcr_Pages.select_Contact, "button").click();
		logger.info("select_Contact");

		textbox(tcr_Pages.SM_MSg_Textarea).sendKeys("Hello");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(tcr_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(tcr_Pages.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waitAndLog(2);
		button(tcr_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(tcr_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);
	}

	public void tcr_After_Submitting_Details_Health_Screening() throws Exception {

		nominee_User_Login_After_Submitting_Details();
		waitAndLog(2);
		button(tcr_Pages.tools_bttn, "Click on tools_bttn").click();
		waitAndLog(2);
		button(tcr_Pages.healthscreen, "healthscreen").click();
		logger.info("click on healthscreen Button");
		waitAndLog(2);

		button(tcr_Pages.create_HealthScreen, "create_HealthScreen").click();
		logger.info("click on create_HealthScreen Button");
		waitAndLog(1);

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(tcr_Pages.create_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(1);

		textbox(tcr_Pages.input_To_searchTags).sendKeys("TCR");
		logger.info("Enter data in search tags ");
		waitAndLog(1);

		button(tcr_Pages.SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		button(tcr_Pages.agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(tcr_Pages.Send_Screening, "button").click();
		logger.info("click on Send_Screening");
		waitAndLog(2);

		if (verifyElementIsEnabled(tcr_Pages.tcr_Close)) {
			Assert.assertEquals(true, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			button(tcr_Pages.tcr_Close, "buton").isEnabled();
			logger.info("TCR Close is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(tcr_Pages.tcr_Close));
			logger.info("TCR Close is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(tcr_Pages.tcr_Close, "Click on tcr_Close").click();
		waitAndLog(2);

	}
}
