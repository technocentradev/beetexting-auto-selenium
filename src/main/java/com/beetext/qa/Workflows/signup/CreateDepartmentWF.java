package com.beetext.qa.Workflows.signup;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.CreateDepatmentWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CreateDepartment;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class CreateDepartmentWF extends CreateDepatmentWFCM {
	String org, workmail, confirmEmail, name, mobileNumber, signuppassword, retypePassword;
	Repository repository = new Repository();
	CreateDepartment createDept = new CreateDepartment();
	TestBase testBase = new TestBase();

	// modified

	public void Enabled_CreateDepartment_Button() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();

		if (verifyElementIsEnabled(createDept.createNewNum)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.createNewNum));
			button(createDept.createNewNum, "button").isEnabled();
			System.out.println("create new number button is  enabled");
			System.out.println("create department button is enabled");
			logger.info("Create department button is enabled case working fine");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.createNewNum));
			System.out.println("create new number button  is disbaled");
			logger.info("Create department button is enabled case not working ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Error_Dept_Name() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		createNewNumberButton();

		textbox(createDept.departmentName).sendKeys("");
		logger.info("Department name entered empty");

		wait(5);
		logger.info("wait for 5 sec");

		textbox(createDept.areaCode).sendKeys("888");
		logger.info("Area code entered:888");

		wait(20);
		logger.info("wait for 20 sec");

		System.out.println(createDept.deptNameRequired.getText());
		if (createDept.deptNameRequired.getText().equalsIgnoreCase("Please give your number a name.")) {
			Assert.assertEquals(createDept.deptNameRequired.getText(), "Please give your number a name.",
					"Please give you number a name");
			System.out.println("department name required validation is working");
			logger.info("department name required validation case working fine");
			TestUtil.takeScreenshotAtEndOfTest("DepartmentnameEmpty");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptNameRequired.getText(), "Please give your number a name.", "");
			System.out.println("department name required validation is not working");
			logger.info("department name required validation case not working");
			TestUtil.takeScreenshotAtEndOfTest("DepartmnetNameEmpty");// ScreenShot capture
		}

	}

	public void Error_Min3_Char_Dept_Name() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		createNewNumberButton();

		textbox(createDept.departmentName).sendKeys("12");
		logger.info("Department name entered:12");
		wait(10);
		logger.info("wait for 10 sec");

		createDept.deptName3chars.getText();

		System.out.println(createDept.deptName3chars.getText());
		if (createDept.deptName3chars.getText()
				.equalsIgnoreCase("Your Number name must contain at least 3 character.")) {
			Assert.assertEquals(createDept.deptName3chars.getText(),
					"Your Number name must contain at least 3 character.",
					"Your Number name must contain at least 3 character are matching ");
			System.out.println("department name 3 characters validation is working");
			logger.info("Department name 3 characters validation case checking");
			TestUtil.takeScreenshotAtEndOfTest("Departmentname less than 3 characters");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptName3chars.getText(),
					"Your Number name must contain at least 3 character.",
					"Your Number name must contain at least 3 character are not matching  ");
			System.out.println("department name 3 characters validation is not working");
			logger.info("Department name 3 characters validation case working");
			TestUtil.takeScreenshotAtEndOfTest("Departmentname lessthan 3 characters");// ScreenShot capture
		}

	}

	public void Next_Button_Enabled() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		createNewNumberButton();

		String DeptName = randomestring();
		textbox(createDept.departmentName).sendKeys(DeptName);
		logger.info("Department name entered:" + DeptName);
		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.areaCode).sendKeys(DeptName);
		logger.info("area code entered:" + DeptName);
		wait(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(createDept.createBtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.createBtn));
			button(createDept.createBtn, "button").isEnabled();
			System.out.println("Department name added successfullt");
			logger.info("Next button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.createBtn));
			System.out.println("create new number button  is disbaled");
			logger.info("Next button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void CreateBtn_Enabled() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		createNewNumberButton();

		String DeptName = randomestring();
		textbox(createDept.departmentName).sendKeys(DeptName);

		wait(5);

		textbox(createDept.areaCode).sendKeys("888");

		wait(10);
		if (verifyElementIsEnabled(createDept.createBtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.createBtn));
			button(createDept.createBtn, "button").isEnabled();
			System.out.println("create Department button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.createBtn));
			System.out.println("create department button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void inValidarea_Code() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		createNewNumberButton();

		String DeptName = randomestring();
		textbox(createDept.departmentName).sendKeys(DeptName);

		wait(5);

		textbox(createDept.areaCode).sendKeys("abc");

		wait(10);

		createDept.onlyDigits.getText();

		System.out.println(createDept.onlyDigits.getText());

		if (createDept.onlyDigits.getText().equalsIgnoreCase("Please enter a valid three-digit area code.")) {
			Assert.assertEquals(createDept.onlyDigits.getText(), "Please enter a valid three-digit area code.",
					"Please enter a valid three-digit US area code");
			System.out.println("Please enter a valid three-digit US area code");
			TestUtil.takeScreenshotAtEndOfTest("Please enter a valid three-digit US area code");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.onlyDigits.getText(), "Please enter a valid three-digit area code.",
					"Please enter a valid three-digit area code.");
			System.out.println("department name 3 characters validation is not working");
			TestUtil.takeScreenshotAtEndOfTest("Please enter a valid three-digit US area code");// ScreenShot capture
		}

	}

	public void areaCode_Required() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		createNewNumberButton();

		textbox(createDept.areaCode).sendKeys("");

		String DeptName = randomestring();
		textbox(createDept.departmentName).sendKeys(DeptName);

		wait(5);

		createDept.areaCodeRequited.getText();

		System.out.println(createDept.areaCodeRequited.getText());

		if (createDept.areaCodeRequited.getText().equalsIgnoreCase("Area Code is required.")) {
			Assert.assertEquals(createDept.areaCodeRequited.getText(), "Area Code is required.",
					"Area Code is require");
			System.out.println("Area Code is required");
			TestUtil.takeScreenshotAtEndOfTest("Area Code is required");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.areaCodeRequited.getText(), "Area Code is required.",
					"Area Code is required.");
			System.out.println("Area Code is required");
			TestUtil.takeScreenshotAtEndOfTest("Area Code is required");// ScreenShot capture
		}

	}

	public void Validarea_Code() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		createNewNumberButton();

		String DeptName = randomestring();
		textbox(createDept.departmentName).sendKeys(DeptName);

		wait(5);

		textbox(createDept.areaCode).sendKeys("888");

		wait(10);

		button(createDept.createBtn, "button").click();

		System.out.println(createDept.deptCreated.getText());

		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully");
			System.out.println("Number Created Successfully");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			System.out.println("Number Created not working");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture
		}

	}

	public void Enabled_TextEnabledExistingNumber_Button() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();

		if (verifyElementIsEnabled(createDept.btnTxtEnableExisting)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.btnTxtEnableExisting));
			button(createDept.btnTxtEnableExisting, "button").isEnabled();
			System.out.println("Textenable exisitng number button is  enabled");

			TestUtil.takeScreenshotAtEndOfTest("TestEnableExistingNumber");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.btnTxtEnableExisting));
			System.out.println("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("TestEnableExistingNumber");// ScreenShot capture
		}

	}

	public void Error_TextenableDept_Name() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("");

		wait(5);

		button(createDept.outsideClick, "button").click();
		wait(10);

		createDept.ClerDeptName.getText();

		System.out.println(createDept.ClerDeptName.getText());
		if (createDept.ClerDeptName.getText().equalsIgnoreCase("Please give your number a name.")) {
			Assert.assertEquals(createDept.ClerDeptName.getText(), "Please give your number a name.",
					"Please give you number a name");
			System.out.println("department name required validation is working");
			TestUtil.takeScreenshotAtEndOfTest("TextEnable Follow DepartmentnameEmpty");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.ClerDeptName.getText(), "Please give your number a name..", "");
			System.out.println("TextEnable department name required validation is not working");
			TestUtil.takeScreenshotAtEndOfTest("TextEnable Follow DepartmnetNameEmpty testcase failed");// ScreenShot
																										// capture
		}

	}

	public void Error_Min3Char_Dept_Name() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("12");
		wait(10);

		createDept.Chars_3validation.getText();

		System.out.println(createDept.Chars_3validation.getText());
		if (createDept.Chars_3validation.getText()
				.equalsIgnoreCase("Your Number name must contain at least 3 character.")) {
			Assert.assertEquals(createDept.Chars_3validation.getText(),
					"Your Number name must contain at least 3 character.",
					"Your Number name must contain at least 3 character are matching ");
			System.out.println("department name 3 characters validation is working");
			TestUtil.takeScreenshotAtEndOfTest("Departmentname less than 3 characters");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.Chars_3validation.getText(),
					"Your Number name must contain at least 3 character.",
					"Your Number name must contain at least 3 character are not matching  ");
			System.out.println("department name 3 characters validation is not working");
			TestUtil.takeScreenshotAtEndOfTest("Departmentname lessthan 3 characters");// ScreenShot capture
		}

	}

	public void getPhone_Numberinfo_page() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();
		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();

		System.out.println(createDept.phone_info.getText());

		System.out.println(createDept.phone_info.getText());
		if (createDept.phone_info.getText().equalsIgnoreCase("Phone Number Account Info")) {
			Assert.assertEquals(createDept.phone_info.getText(), "Phone Number Account Info",
					"Phone Number Account Info ");
			System.out.println("Phone Number Account Info title testcase working fine");
			TestUtil.takeScreenshotAtEndOfTest("phone number info page title text is verifying case is passed");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(createDept.phone_info.getText(), "Phone Number Account Info",
					"Phone Number Account Info");
			System.out.println("Phone Number Account Info title testcae is not working");
			TestUtil.takeScreenshotAtEndOfTest("phone number info page title text is  verifying case is failed ");// ScreenShot
																													// capture
		}

	}

	public void getPhone_Numberinfo_CustName() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();

		String phone_Number = "555" + randomeNum();
		textbox(createDept.phoneNumber).sendKeys(phone_Number);
		wait(10);

		textbox(createDept.custName).sendKeys("du");
		wait(10);

		if (createDept.custNamelessthan3.getText().equalsIgnoreCase("Name must be at least 3 characters.")) {
			Assert.assertEquals(createDept.custNamelessthan3.getText(), "Name must be at least 3 characters.",
					"Name must be at least 3 characters.");
			System.out.println("Phone Number Account Info Customer Name minimum 3 characters testcase working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info page  Customer Name minimum 3 characters text is verifying case is passed");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(createDept.custNamelessthan3.getText(), "Name must be at least 3 characters.",
					"Name must be at least 3 characters");
			System.out.println("Phone Number Account Info page customer name mininum 3 characters case is not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info  customer name mininum 3 characters case  is  verifying case is failed ");// ScreenShot
																													// capture
		}

	}

	public void error_zipCode() throws Exception {
		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();

		String phone_Number = "555" + randomeNum();
		textbox(createDept.phoneNumber).sendKeys(phone_Number);
		wait(10);

		textbox(createDept.custName).sendKeys("sample");
		wait(10);

		String street = randomestring();

		textbox(createDept.street).sendKeys(street);
		wait(5);

		String city = randomestring();

		textbox(createDept.city).sendKeys(city);
		wait(5);

		textbox(createDept.zipcode).sendKeys("dfks");
		wait(10);

		System.out.println(createDept.zipcode_allowolyNumber.getText());

		if (createDept.zipcode_allowolyNumber.getText().equalsIgnoreCase("Only numbers are allowed.")) {
			Assert.assertEquals(createDept.zipcode_allowolyNumber.getText(), "Only numbers are allowed.",
					"Only numbers are allowed.");
			System.out.println("Phone Number Account Info page zip code allow only number testcase working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info page zip code allow only number  text is verifying case is passed");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(createDept.zipcode_allowolyNumber.getText(), "Only numbers are allowed.",
					"Only numbers are allowed.");
			System.out.println("Phone Number Account Info page Only numbers are allowed. case is not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info  page Only numbers are allowed. case  is  verifying case is failed ");// ScreenShot
																												// capture
		}

	}

	public void NextButtonEnabled() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();
		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();
		phoneNumberinfoDetails();

		System.out.println("button is enbaled");
		if (verifyElementIsEnabled(createDept.next_Button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.next_Button));
			button(createDept.next_Button, "button").isEnabled();
			System.out.println("Phone Number info page next button is  enabled");

			TestUtil.takeScreenshotAtEndOfTest("Phone Number info page Next button Enabled case working fine");// ScreenShot
																												// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.btnTxtEnableExisting));
			System.out.println("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("Phone Number info page Next button disabled case working fine");// ScreenShot
																												// capture
		}

	}

	public void phoneNumberinfo_validData() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();

		createDept.phone_info.getText();

		phoneNumberinfoDetails();

		button(createDept.next_Button, "button").click();

		wait(30);
		logger.info("wait for 30 sec");

		callmeButton();

		verficationCode();

		if (createDept.dept_created.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.dept_created.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			System.out.println("Text_Enabled department number is created successfully");
			TestUtil.takeScreenshotAtEndOfTest("Text_Enabled department number is created successfully case is passed");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(createDept.dept_created.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			System.out.println("Number Created Successfully.  case is not working");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully case  is failed ");// ScreenShot capture
		}

	}

	public void phoneNumberinfo_callme_Again() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();

		phoneNumberinfoDetails();

		button(createDept.next_Button, "button").click();

		callmeButton();

		invalidOtp();

		callmeagain();
		verficationCode();

		button(Repository.verify_bttn, "button").click();

		if (createDept.dept_created.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.dept_created.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			System.out.println("Text_Enabled department number is created successfully");
			TestUtil.takeScreenshotAtEndOfTest("Text_Enabled department number is created successfully case is passed");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(createDept.dept_created.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			System.out.println("Number Created Successfully.  case is not working");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully case  is failed ");// ScreenShot capture
		}

	}

	public void phoneNumberinfo_mobilealrady_exist() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");
		createDept.phone_info.getText();

		textbox(createDept.phoneNumber).sendKeys("3068016736");
		wait(10);

		textbox(createDept.custName).sendKeys("sample");
		wait(10);

		String street = randomestring();

		textbox(createDept.street).sendKeys(street);
		wait(5);

		String city = randomestring();

		textbox(createDept.city).sendKeys(city);
		wait(5);

		textbox(createDept.zipcode).sendKeys("12345");
		wait(10);

		textbox(createDept.state).click();
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='existingNumberState']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Guam")) {

				allOptions.get(i).click();
				break;

			}
		}

		checkbox(createDept.radioButton, "checkbox").click();

		wait(5);

		button(createDept.next_Button, "button").click();

		System.out.println(createDept.Numberalready_Exist.getText());
		if (createDept.Numberalready_Exist.getText().equalsIgnoreCase(
				"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.")) {
			Assert.assertEquals(createDept.Numberalready_Exist.getText(),
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.",
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.");
			System.out.println("Text_Enabled Phone number is already available with other org,  successfully");
			TestUtil.takeScreenshotAtEndOfTest(
					"Text_Enabled department number Phone number is already available with other org,  is created successfully case is passed");// ScreenShot
																																				// capture

		} else {
			Assert.assertNotEquals(createDept.Numberalready_Exist.getText(),
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.",
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.");
			System.out.println("Phone number is already available with other org, ");
			TestUtil.takeScreenshotAtEndOfTest("Phone number is already available with other org, case  is failed ");// ScreenShot
																														// capture
		}
	}

	public void phoneNumberinfo_cityName_notallow_special_Char() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();
		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");
		createDept.phone_info.getText();

		textbox(createDept.phoneNumber).sendKeys("3068016736");
		wait(10);

		textbox(createDept.custName).sendKeys("sample");
		wait(10);

		String street = randomestring();

		textbox(createDept.street).sendKeys(street);
		wait(5);

		String city = randomestring();

		textbox(createDept.city).sendKeys(city + ",");
		wait(5);

		textbox(createDept.zipcode).sendKeys("12345");
		wait(10);

		wait(5);
		System.out.println(createDept.err_city_name_notallow_specialchar.getText());
		if (createDept.err_city_name_notallow_specialchar.getText()
				.equalsIgnoreCase("Special characters are not allowed.")) {
			Assert.assertEquals(createDept.err_city_name_notallow_specialchar.getText(),
					"Special characters are not allowed.", "Special characters are not allowed.");
			System.out.println("phone number info  city name Special characters are not allowed.   successfully");
			TestUtil.takeScreenshotAtEndOfTest("phone number info  city name specila char not allowed case is passed");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(createDept.err_city_name_notallow_specialchar.getText(),
					"Special characters are not allowed.", "Special characters are not allowed. ");
			System.out.println("phone number info  city name specila char not allowed case failed  ");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info  city name specila char not allowed  case  is failed ");// ScreenShot capture
		}
	}

	public void phoneNumberinfo_uncheck_terms_conditions_check_Mark() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();
		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();

		textbox(createDept.phoneNumber).sendKeys("3068016736");
		wait(10);

		textbox(createDept.custName).sendKeys("sample");
		wait(10);

		String street = randomestring();

		textbox(createDept.street).sendKeys(street);
		wait(5);

		String city = randomestring();

		textbox(createDept.city).sendKeys(city);
		wait(5);

		textbox(createDept.zipcode).sendKeys("12345");
		wait(10);

		textbox(createDept.state).click();
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='existingNumberState']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Guam")) {

				allOptions.get(i).click();
				break;

			}
		}

		checkbox(createDept.radioButton, "checkbox").click();

		wait(5);

		checkbox(createDept.radioButton, "checkbox").click();

		wait(5);
		System.out.println(createDept.err_terms_conditons_check.getText());
		if (createDept.err_terms_conditons_check.getText()
				.equalsIgnoreCase("Please read the terms of service and accept. ")) {
			Assert.assertEquals(createDept.err_terms_conditons_check.getText(),
					"Please read the terms of service and accept. ", "Please read the terms of service and accept. ");
			System.out.println("phone number info  terms and conditions not seelcted   successfully");
			TestUtil.takeScreenshotAtEndOfTest("phone number info  terms and conditions not seelcted case is passed");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(createDept.err_terms_conditons_check.getText(),
					" Please read the terms of service and accept.", "Please read the terms of service and accept. ");
			System.out.println("phone number info  terms and conditions not seelcted  ");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info  terms and conditions not seelcted  case  is failed ");// ScreenShot capture
		}
	}

	public void phoneNumberinfo_required_validation_checking() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();

		textbox(createDept.phoneNumber).sendKeys("");
		wait(10);

		textbox(createDept.custName).sendKeys("");
		wait(10);

		textbox(createDept.street).sendKeys("");
		wait(5);

		textbox(createDept.city).sendKeys("");
		wait(5);

		textbox(createDept.zipcode).sendKeys("");
		wait(10);

		textbox(createDept.state).click();
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='existingNumberState']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Guam")) {

				allOptions.get(i).click();
				break;

			}
		}

		wait(5);
		System.out.println(createDept.err_required_validation.getText());
		if (createDept.err_required_validation.getText().equalsIgnoreCase("City is required. ")) {
			Assert.assertEquals(createDept.err_required_validation.getText(), "City is required. ",
					"City is required. ");
			System.out.println("phone number info  required validation  successfully");
			TestUtil.takeScreenshotAtEndOfTest("phone number info  page required validation  case is passed");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(createDept.err_required_validation.getText(),
					" Please read the terms of service and accept.", "Please read the terms of service and accept. ");
			System.out.println("phone number info page required validation case  failed  ");
			TestUtil.takeScreenshotAtEndOfTest("phone number info page required validation  case  is failed ");// ScreenShot
																												// capture
		}
	}

	public void backto_PhoneNumber_info() throws Exception {

		singuppagedetails();
		phonenumberpage();
		mobileotp();
		emailotp();
		creditcardDetails();
		txtenabledNumber();

		textbox(createDept.deptName).sendKeys("sample");
		wait(10);

		button(createDept.nextButton, "button").click();
		wait(30);
		logger.info("wait for 30 sec");

		createDept.phone_info.getText();

		String phone_Number = "555" + randomeNum();
		textbox(createDept.phoneNumber).sendKeys(phone_Number);
		wait(10);

		textbox(createDept.custName).sendKeys("sample");
		wait(10);

		String street = randomestring();

		textbox(createDept.street).sendKeys(street);
		wait(5);

		String city = randomestring();

		textbox(createDept.city).sendKeys(city);
		wait(5);

		textbox(createDept.zipcode).sendKeys("12345");
		wait(10);

		textbox(createDept.state).click();
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='existingNumberState']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Guam")) {

				allOptions.get(i).click();
				break;

			}
		}

		checkbox(createDept.radioButton, "checkbox").click();

		wait(5);

		button(createDept.next_Button, "button").click();

		waits(5);
		logger.info("wait for 30 sec");

		button(createDept.back_Button, "button").click();

		wait(30);

		createDept.phone_info.getText();

		System.out.println(createDept.phone_info.getText());
		if (createDept.phone_info.getText().equalsIgnoreCase("Phone Number Account Info")) {
			Assert.assertEquals(createDept.phone_info.getText(), "Phone Number Account Info",
					"Phone Number Account Info ");
			System.out.println("Phone Number Account Info title testcase working fine");
			TestUtil.takeScreenshotAtEndOfTest("phone number info page title text is verifying case is passed");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(createDept.phone_info.getText(), "Phone Number Account Info",
					"Phone Number Account Info");
			System.out.println("Phone Number Account Info title testcae is not working");
			TestUtil.takeScreenshotAtEndOfTest("phone number info page title text is  verifying case is failed ");// ScreenShot
																													// capture
		}

	}

}
