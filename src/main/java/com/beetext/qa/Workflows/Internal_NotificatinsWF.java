package com.beetext.qa.Workflows;

import java.util.Map;

import org.testng.Assert;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.internal_notifications_pages;
import com.beetext.qa.util.TestUtil;

public class Internal_NotificatinsWF extends CommonMethods {
	String url, username1, username2, password;
	Repository repository = new Repository();
	internal_notifications_pages INP = new internal_notifications_pages();
	public static String currentDir = System.getProperty("user.dir");

	public void Notifications_CM(Map<String, String> map) throws Exception {
		username1 = (String) map.get("username1");
		username2 = (String) map.get("username2");
		password = (String) map.get("password");

		textbox(Repository.user_id).sendKeys(username2);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		logger.info("validateSignOutLink");
		waits(2);
		logger.info("wait for 2 sec");

		validateUser();
		logger.info("validateUser");
		waits(2);
		logger.info("wait for 3 sec");

		button(INP.notification, "notification").click();
		logger.info("notification");

	}

	public void Notification_All_States_Enable(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.new_Msgs_email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.new_Msgs_email));
			textbox(INP.new_Msgs_email).isEnabled();
			logger.info("New Message Email is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.new_Msgs_email));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(INP.new_Msgs_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.new_Msgs_web_banner));
			textbox(INP.new_Msgs_web_banner).isEnabled();
			logger.info("New Message Web Banner is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.new_Msgs_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(INP.internal_Mentions_email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.internal_Mentions_email));
			textbox(INP.internal_Mentions_email).isEnabled();
			logger.info("Internal mentions Email is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.internal_Mentions_email));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(INP.internal_mentions_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.internal_mentions_web_banner));
			textbox(INP.internal_mentions_web_banner).isEnabled();
			logger.info("Internal mentions Web Banner is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.internal_mentions_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(INP.Conversation_transfered_email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.Conversation_transfered_email));
			textbox(INP.Conversation_transfered_email).isEnabled();
			logger.info("Conversation transferred to you Email is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.Conversation_transfered_email));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(INP.Conversation_transfered_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.Conversation_transfered_web_banner));
			textbox(INP.Conversation_transfered_web_banner).isEnabled();
			logger.info("Conversation transferred to you Web Banner is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.Conversation_transfered_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Notification_ON_Off_Update_Toaster(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_email, "new_Msgs_email").click();
		logger.info("new_Msgs_email");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_email, "new_Msgs_email").click();
		logger.info("new_Msgs_email");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Notification_Email_WebBanner_Both_ON(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.new_Msgs_email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.new_Msgs_email));
			textbox(INP.new_Msgs_email).isEnabled();
			logger.info("New Message Email is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.new_Msgs_email));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(INP.new_Msgs_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.new_Msgs_web_banner));
			textbox(INP.new_Msgs_web_banner).isEnabled();
			logger.info("New Message Web Banner is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.new_Msgs_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Notification_Email_WebBanner_Both_OFF(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_email, "new_Msgs_email").click();
		logger.info("new_Msgs_email");
		waits(3);
		logger.info("wait for 3 sec");

		button(INP.new_Msgs_web_banner, "new_Msgs_web_banner").click();
		logger.info("new_Msgs_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_email, "new_Msgs_email").click();
		logger.info("new_Msgs_email");
		waits(3);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_web_banner, "new_Msgs_web_banner").click();
		logger.info("new_Msgs_web_banner");
		waits(3);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Notification_Email_ON_WebBanner_OFF(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.new_Msgs_email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.new_Msgs_email));
			textbox(INP.new_Msgs_email).isEnabled();
			logger.info("New Message Email is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.new_Msgs_email));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(INP.new_Msgs_web_banner, "new_Msgs_web_banner").click();
		logger.info("new_Msgs_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_web_banner, "new_Msgs_web_banner").click();
		logger.info("new_Msgs_web_banner");
		waits(3);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Notification_Email_OFF_WebBanner_ON(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_email, "new_Msgs_email").click();
		logger.info("new_Msgs_email");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.new_Msgs_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.new_Msgs_web_banner));
			textbox(INP.new_Msgs_web_banner).isEnabled();
			logger.info("New Message Web Banner is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.new_Msgs_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.new_Msgs_web_banner, "new_Msgs_web_banner").click();
		logger.info("new_Msgs_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Notification_Internal_Mentions_Both_ON(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.internal_Mentions_email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.internal_Mentions_email));
			textbox(INP.internal_Mentions_email).isEnabled();
			logger.info("internal_Mentions_email is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.internal_Mentions_email));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(INP.internal_mentions_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.internal_mentions_web_banner));
			textbox(INP.internal_mentions_web_banner).isEnabled();
			logger.info("internal_mentions_web_banner is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.internal_mentions_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Notification_Internal_Mentions_Both_OFF(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.internal_Mentions_email, "internal_Mentions_email").click();
		logger.info("internal_Mentions_email");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.internal_mentions_web_banner, "internal_mentions_web_banner").click();
		logger.info("internal_mentions_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(INP.internal_Mentions_email, "internal_Mentions_email").click();
		logger.info("internal_Mentions_email");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.internal_mentions_web_banner, "internal_mentions_web_banner").click();
		logger.info("internal_mentions_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Notification_Internal_Mentions_Email_ON_WebBanner_OFF(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.internal_Mentions_email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.internal_Mentions_email));
			textbox(INP.internal_Mentions_email).isEnabled();
			logger.info("internal_Mentions_email is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.internal_Mentions_email));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(INP.internal_mentions_web_banner, "internal_mentions_web_banner").click();
		logger.info("internal_mentions_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(INP.internal_mentions_web_banner, "internal_mentions_web_banner").click();
		logger.info("internal_mentions_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Notification_Internal_Mentions_Email_OFF_WebBanner_ON(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.internal_Mentions_email, "internal_Mentions_email").click();
		logger.info("internal_Mentions_email");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.internal_mentions_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.internal_mentions_web_banner));
			textbox(INP.internal_mentions_web_banner).isEnabled();
			logger.info("internal_mentions_web_banner is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.internal_mentions_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(INP.internal_Mentions_email, "internal_Mentions_email").click();
		logger.info("internal_Mentions_email");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Notification_Conversation_trasfered_ON(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.Conversation_transfered_web_banner)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.Conversation_transfered_web_banner));
			textbox(INP.Conversation_transfered_web_banner).isEnabled();
			logger.info("Conversation_transfered_web_banner is ON");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.Conversation_transfered_web_banner));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Notification_Conversation_trasfered_OFF(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		button(INP.Conversation_transfered_web_banner, "Conversation_transfered_web_banner").click();
		logger.info("Conversation_transfered_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(INP.notification_updated_Successfully)) {
			Assert.assertEquals(true, verifyElementIsEnabled(INP.notification_updated_Successfully));
			textbox(INP.notification_updated_Successfully).isEnabled();
			logger.info(INP.notification_updated_Successfully.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(INP.notification_updated_Successfully));
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);

		button(INP.Conversation_transfered_web_banner, "Conversation_transfered_web_banner").click();
		logger.info("Conversation_transfered_web_banner");
		waits(2);
		logger.info("wait for 2 sec");

		button(INP.update_notifications, "update_notifications").click();
		logger.info("update_notifications");
		waits(2);
		logger.info("wait for 2 sec");

	}

}
