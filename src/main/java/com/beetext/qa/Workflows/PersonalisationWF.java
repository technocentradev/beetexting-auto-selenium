package com.beetext.qa.Workflows;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Personalisation;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class PersonalisationWF extends Broadcast {

	Repository repository = new Repository();
	TestBase testBase = new TestBase();

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Personalisation P_pages = new Personalisation();

	public void auto1_Login() throws Exception {

		textbox(Personalisation.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(Personalisation.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waits(2);
		button(Personalisation.loginbttn, "login submit").submit();
		waits(2);
		logger.info("wait for 2 sec");
	}

	public void Persoanlisation_BM() throws Exception {

		waits(2);

		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(P_pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(3);
		logger.info("wait for 3 sec");

		P_pages.BM_Persoanlisation_Message.getText();
		System.out.println(P_pages.BM_Persoanlisation_Message.getText());
		if (P_pages.BM_Persoanlisation_Message.getText()
				.equalsIgnoreCase("If name isn't available, text will be removed")) {
			Assert.assertEquals(P_pages.BM_Persoanlisation_Message.getText(),
					"If name isn't available, text will be removed", "message is matching");
			System.out.println("Content is verified");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(P_pages.BM_Persoanlisation_Message.getText(),
					"If name isn't available, text will be removed", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		wait(2);
		logger.info("wait for 2 sec");
		button(P_pages.tools_crossBtn, "click on cross button").click();
		waits(2);

	}

	public void Persoanlisation_BM_title_Below_PM() throws Exception {

		waits(2);
		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(P_pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(3);
		logger.info("wait for 3 sec");

		P_pages.firstname.getText();
		System.out.println(P_pages.firstname.getText());
		if (P_pages.firstname.getText().equalsIgnoreCase(" [firstname] ")) {
			Assert.assertEquals(P_pages.firstname.getText(), " [firstname] ", "message is matching");
			System.out.println("Firstname is Compared and verified");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(P_pages.firstname.getText(), " [firstname] ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		P_pages.companyname.getText();
		System.out.println(P_pages.companyname.getText());
		if (P_pages.companyname.getText().equalsIgnoreCase(" [companyname] ")) {
			Assert.assertEquals(P_pages.companyname.getText(), " [companyname] ", "message is matching");
			System.out.println("Companyname is Compared and verified");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(P_pages.companyname.getText(), " [companyname] ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		wait(2);
		logger.info("wait for 2 sec");
		button(P_pages.tools_crossBtn, "click on cross button").click();
		waits(2);

	}

	public void Persoanlisation_questionMark_Content() throws Exception {

		waits(2);
		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(P_pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(3);
		logger.info("wait for 3 sec");

		button(P_pages.Question_Mark, "button").click();
		logger.info("click on Question_Mark button");
		waits(3);
		logger.info("wait for 3 sec");

		String text = P_pages.Question_Mark_Content1.getText();
		String text1 = P_pages.Question_Mark_Content2.getText();

		System.out.println(text + "" + text1);

		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		wait(2);
		logger.info("wait for 2 sec");
		button(P_pages.tools_crossBtn, "click on cross button").click();
		waits(2);

	}

	public void Persoanlisation_firstname_Companyname_Content() throws Exception {

		waits(2);
		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(P_pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(3);
		logger.info("wait for 3 sec");

		driver.findElement(By.xpath("//span[contains(text(),'[firstname]')]")).click();
		logger.info("click on firstname button");
		waitAndLog(2);

		driver.findElement(By.xpath("//span[contains(text(),'[companyname]')]")).click();
		logger.info("click on companyname button");
		waitAndLog(2);

		String message_text = driver.findElement(By.xpath("//textarea[@id='broadcast_message']")).getText();

		logger.info(message_text);
		waitAndLog(2);
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");
		button(P_pages.tools_crossBtn, "click on cross button").click();
		waits(2);

	}

	public void PM_BM_doesnot_first_Comapany() throws Exception {

		waits(2);
		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(repository.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(2);
		logger.info("wait for 2 sec");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("no name");
		logger.info("Enter data in search tags ");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(repository.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(repository.tools_BM_message).sendKeys("[firstname][companyname]");
		logger.info(" Enter your messeage");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on tools_BM_create_Send_BroadCast");
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		wait(2);
		logger.info("wait for 2 sec");
		button(P_pages.tools_crossBtn, "click on cross button").click();
		waits(2);

	}

	public void PM_BM_first_Comapany() throws Exception {

		waits(2);
		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(repository.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(2);
		logger.info("wait for 2 sec");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("first");
		logger.info("Enter data in search tags ");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(repository.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(repository.tools_BM_message).sendKeys("[firstname][companyname]");
		logger.info(" Enter your messeage");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on tools_BM_create_Send_BroadCast");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.contentContact, "click on content contact").click();
		logger.info("click on contentContact button");
		waits(2);
		logger.info("wait for 2 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("ContentBeetexting")) {
			Assert.assertEquals(repository.message.getText(), "ContentBeetexting", "message is matching");
			System.out.println("message is verified with firstname and companyname ");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "ContentBeetexting", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void PM_BM_onlyfirst() throws Exception {

		waits(2);
		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(repository.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("onlyname");
		logger.info("Enter data in search tags ");
		waits(2);
		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(repository.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(repository.tools_BM_message).sendKeys("[firstname][companyname]");
		logger.info(" Enter your messeage");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on tools_BM_create_Send_BroadCast");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.onlyname_Contact, "click on onlyname_Contact button").click();
		waits(2);

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("Onlyname")) {
			Assert.assertEquals(repository.message.getText(), "Onlyname", "message is matching");
			System.out.println("message is verified with firstname");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "Onlyname", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
	}

	public void PM_BM_onlycompanyname() throws Exception {

		waits(2);
		button(P_pages.tools_bttn, "click on tools").click();
		waits(2);
		button(P_pages.BM, "button").click();
		logger.info("click on BM button");
		waits(3);
		logger.info("wait for 3 sec");

		button(repository.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waits(2);
		logger.info("wait for 2 sec");

		textbox(repository.tools_BM_input_To_searchTags).sendKeys("onlycompany");
		logger.info("Enter data in search tags ");

		button(repository.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		System.out.print("Random Title--->" + randomtitle);

		textbox(repository.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(repository.tools_BM_message).sendKeys("[firstname][companyname]");
		logger.info(" Enter your messeage");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on tools_BM_create_Send_BroadCast");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.companyname_Contact, "click on companyname contact");
		logger.info("click on companyname_Contact button");
		waits(3);
		logger.info("wait for 3 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("beetex")) {
			Assert.assertEquals(repository.message.getText(), "beetex", "message is matching");
			System.out.println("message is verified with Companyname");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "beetex", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void PM_firstname_SpecialChar_Comapany() throws Exception {

		waits(2);
		button(repository.composebutton, "click on compose button").click();
		textbox(repository.contactSearchInputTxt).sendKeys("Special");
		logger.info("enter text to search : Auto1");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");
		waits(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("[firstname]");
		logger.info("[firstname]");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("[companyname]");
		logger.info("[companyname]");
		waits(3);
		logger.info("wait for 3 sec");
		button(repository.chatInputsendButton, "button").click();
		waits(2);
		logger.info("wait for 2 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("special$%#&Tech-Bee")) {
			Assert.assertEquals(repository.message.getText(), "special$%#&Tech-Bee", "message is matching");
			System.out.println("message is verified with Special Char firstname and companyname ");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "special$%#&Tech-Bee", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void Persoanlisation_Msg_Templates() throws Exception {

		waits(2);
		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.Templates, "button").click();
		logger.info("click on Templates button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.Create_Template, "button").click();
		logger.info("click on Create_Template BroadCast button");
		waits(2);
		logger.info("wait for 2 sec");

		P_pages.Templates_Persoanlisation_Message.getText();
		System.out.println(P_pages.Templates_Persoanlisation_Message.getText());
		if (P_pages.Templates_Persoanlisation_Message.getText()
				.equalsIgnoreCase("If name isn't available, text will be removed")) {
			Assert.assertEquals(P_pages.Templates_Persoanlisation_Message.getText(),
					"If name isn't available, text will be removed", "message is matching");
			System.out.println("Content is verified");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(P_pages.Templates_Persoanlisation_Message.getText(),
					"If name isn't available, text will be removed", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
		waits(2);
		button(P_pages.Templates, "button").click();
		logger.info("click on Templates button");
		waits(2);
		logger.info("wait for 2 sec");
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void Persoanlisation_Templates_title_Below_PM() throws Exception {

		waits(2);
		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.Templates, "button").click();
		logger.info("click on Templates button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.Create_Template, "button").click();
		logger.info("click on Create_Template BroadCast button");
		waits(2);
		logger.info("wait for 2 sec");

		P_pages.firstname.getText();
		System.out.println(P_pages.firstname.getText());
		if (P_pages.firstname.getText().equalsIgnoreCase(" [firstname] ")) {
			Assert.assertEquals(P_pages.firstname.getText(), " [firstname] ", "message is matching");
			System.out.println("Firstname is Compared and verified");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(P_pages.firstname.getText(), " [firstname] ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		P_pages.companyname.getText();
		System.out.println(P_pages.companyname.getText());
		if (P_pages.companyname.getText().equalsIgnoreCase(" [companyname] ")) {
			Assert.assertEquals(P_pages.companyname.getText(), " [companyname] ", "message is matching");
			System.out.println("Companyname is Compared and verified");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(P_pages.companyname.getText(), " [companyname] ", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
		waits(2);
		button(P_pages.Templates, "button").click();
		logger.info("click on Templates button");
		waits(2);
		logger.info("wait for 2 sec");
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void PM_Templates_In_Conversation_firstname_Comapany() throws Exception {

		waits(2);
		button(P_pages.contentContact, "button").click();
		logger.info("click on contentContact button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.Templates_In_Conversation, "button").click();
		logger.info("click on Templates_In_Conversation button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.personal_Templates_In_Conversation, "button").click();
		logger.info("click on personal_Templates_In_Conversation");

		button(P_pages.Use_personal_Templates_In_Conversation, "button").click();
		logger.info("click on use personal_Templates_In_Conversation");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on chatInputsendButton");
		waits(3);
		logger.info("wait for 3 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("ContentBeetexting")) {
			Assert.assertEquals(repository.message.getText(), "ContentBeetexting", "message is matching");
			System.out.println("message is verified with firstname and companyname ");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "ContentBeetexting", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void PM_Templates_In_Single_Conversation_firstname_Comapany() throws Exception {

		waits(2);

		button(P_pages.contentContact, "button").click();
		logger.info("click on contentContact button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.Templates_In_Conversation, "button").click();
		logger.info("click on Templates_In_Conversation button");
		waits(2);
		logger.info("wait for 2 sec");

		button(P_pages.personal_Templates_In_Conversation, "button").click();
		logger.info("click on personal_Templates_In_Conversation");

		button(P_pages.Use_personal_Templates_In_Conversation, "button").click();
		logger.info("click on use personal_Templates_In_Conversation");
		waits(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on chatInputsendButton");
		waits(3);
		logger.info("wait for 3 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("ContentBeetexting")) {
			Assert.assertEquals(repository.message.getText(), "ContentBeetexting", "message is matching");
			System.out.println("message is verified with firstname and companyname ");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "ContentBeetexting", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void PM_Group_Conversation_PM_Template_disabled() throws Exception {

		waits(3);

		button(repository.composebutton, "click on compose button").click();
		textbox(repository.contactSearchInputTxt).sendKeys("QA10");
		logger.info("enter text to search : Auto1");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");
		textbox(repository.contactSearchInputTxt).sendKeys("QA123");
		logger.info("enter text to search : Auto1");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");
		textbox(repository.contactSearchInputTxt).sendKeys("QA");
		logger.info("enter text to search : Auto1");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");

		button(P_pages.Templates_In_Conversation, "button").click();
		logger.info("click on Templates_In_Conversation button");

		button(P_pages.personal_Templates_In_Conversation, "button").click();
		logger.info("click on personal_Templates_In_Conversation");

		if (verifyElementIsEnabled(P_pages.Use_personal_Templates_In_Conversation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(P_pages.Use_personal_Templates_In_Conversation));
			button(P_pages.Use_personal_Templates_In_Conversation, "button").isEnabled();
			System.out.println("Personalisation Message Template is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(P_pages.Use_personal_Templates_In_Conversation));
			System.out.println("Personalisation Message Template is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void PM_compose_add_2Contacts_PM_Template_disabled() throws Exception {

		waits(5);
		logger.info("wait for 5 sec");

		button(repository.composebutton, "click on compose button").click();
		textbox(repository.contactSearchInputTxt).sendKeys("QA10");
		logger.info("enter text to search : Auto1");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");
		textbox(repository.contactSearchInputTxt).sendKeys("QA123");
		logger.info("enter text to search : Auto1");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");

		button(P_pages.Templates_In_Conversation, "button").click();
		logger.info("click on Templates_In_Conversation button");

		button(P_pages.personal_Templates_In_Conversation, "button").click();
		logger.info("click on personal_Templates_In_Conversation");

		if (verifyElementIsEnabled(P_pages.Use_personal_Templates_In_Conversation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(P_pages.Use_personal_Templates_In_Conversation));
			button(P_pages.Use_personal_Templates_In_Conversation, "button").isEnabled();
			System.out.println("Personalisation Message Template is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(P_pages.Use_personal_Templates_In_Conversation));
			System.out.println("Personalisation Message Template is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void PM_doesnot_first_Comapany() throws Exception {

		waits(3);
		logger.info("wait for 3 sec");

		button(repository.composebutton, "click on compose button").click();
		waitAndLog(2);
		textbox(repository.contactSearchInputTxt).sendKeys("5550026788");
		logger.info("enter text to search : 5550026788");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");
		waits(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("[firstname]");
		logger.info("[firstname]");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("[companyname]");
		logger.info("[companyname]");
		waits(3);
		logger.info("wait for 3 sec");
		button(repository.chatInputsendButton, "button").click();
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void PM_first_Comapany() throws Exception {

		waits(2);
		button(P_pages.contentContact, "click on contact").click();
		;
		logger.info("click on contentContact button");
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("[firstname]");
		logger.info("[firstname]");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("[companyname]");
		logger.info("[companyname]");
		waits(3);
		logger.info("wait for 3 sec");
		JSClick(repository.chatInputsendButton);
		waits(2);
		logger.info("wait for 2 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("ContentBeetexting")) {
			Assert.assertEquals(repository.message.getText(), "ContentBeetexting", "message is matching");
			System.out.println("message is verified with firstname and companyname ");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "ContentBeetexting", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void PM_only_firstname() throws Exception {

		waits(3);
		logger.info("wait for 3 sec");

		button(repository.composebutton, "click on compose button").click();
		textbox(repository.contactSearchInputTxt).sendKeys("onlyname");
		logger.info("enter text to search : onlyname");
		link(repository.contactSearch_OP_Txt1, "Search output").click();
		logger.info("click on contactSearch_OP_Txt1");
		waits(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("[firstname]");
		logger.info("[firstname]");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("[companyname]");
		logger.info("[companyname]");
		waits(3);
		logger.info("wait for 3 sec");
		button(repository.chatInputsendButton, "button").click();
		waits(2);
		logger.info("wait for 2 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("Onlyname")) {
			Assert.assertEquals(repository.message.getText(), "Onlyname", "message is matching");
			System.out.println("message is verified with firstname");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "Onlyname", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void PM_only_Companyname() throws Exception {

		waits(3);
		logger.info("wait for 3 sec");

		button(P_pages.companyname_Contact, "click on contact").click();
		logger.info("click on companyname_Contact button");
		waits(3);
		logger.info("wait for 3 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("[firstname]");
		logger.info("[firstname]");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(repository.chatInputTextareaTxt).sendKeys("[companyname]");
		logger.info("[companyname]");
		waits(3);
		logger.info("wait for 3 sec");
		JSClick(repository.chatInputsendButton);
		waits(2);
		logger.info("wait for 2 sec");

		repository.message.getText();
		System.out.println(repository.message.getText());
		if (repository.message.getText().equalsIgnoreCase("beetex")) {
			Assert.assertEquals(repository.message.getText(), "beetex", "message is matching");
			System.out.println("message is verified with Companyname");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "beetex", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

}
