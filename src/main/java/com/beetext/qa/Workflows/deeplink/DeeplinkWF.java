package com.beetext.qa.Workflows.deeplink;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Deeplink;
import com.beetext.qa.pages.Org_Switch_Pages;
import com.beetext.qa.pages.Pin_Messages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class DeeplinkWF extends Broadcast {

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Repository repository = new Repository();
	Pin_Messages pinMessage = new Pin_Messages();
	TestBase testBase = new TestBase();
	Org_Switch_Pages org_Switch_Pages = new Org_Switch_Pages();
	Deeplink dlink = new Deeplink();

	public void Redirect_Same_User_Org() throws Exception {

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");

		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(2);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1Login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1Login));
			textbox(dlink.Auto1Login).isEnabled();
			logger.info("Redirected to Same User and Same department");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1Login));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
	}

	public void MultiOrg_Redirect_Same_User_Org() throws Exception {

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1Login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1Login));
			textbox(dlink.Auto1Login).isEnabled();
			logger.info("Redirected to Same User and Same department");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1Login));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
		validateSignOutLink();
		validateSignOut();
		waitAndLog(3);

	}

	public void invalid_deeplink() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys(username2);
		logger.info("Email id :" + username2);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);
		logger.info("wait for  sec");

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		logger.info(dlink.Invalid_Deeplink.getText());
		if (dlink.Invalid_Deeplink.getText().equalsIgnoreCase("Invalid deeplink Id.")) {
			Assert.assertEquals(dlink.Invalid_Deeplink.getText(), "Invalid deeplink Id.", "message is matching");
			logger.info(dlink.Invalid_Deeplink.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(dlink.Invalid_Deeplink.getText(), "Invalid deeplink Id.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));

	}

	public void Redirect_OneOne_Conversation() throws Exception {

		waitAndLog(3);

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(2);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));
		waitAndLog(3);

		if (verifyElementIsEnabled(repository.chatInputTextareaTxt)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.chatInputTextareaTxt));
			textbox(repository.chatInputTextareaTxt).isEnabled();
			logger.info("Redirected to one-one conversation Page");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.chatInputTextareaTxt));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));

	}

	public void MultiOrg_Redirect_Same_User_Auto1_1stuser() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1Login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1Login));
			textbox(dlink.Auto1Login).isEnabled();
			logger.info("Redirected to Same User and Same department - Auto1 1st login");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1Login));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
		validateSignOutLink();
		validateSignOut();
		waitAndLog(3);

	}

	public void Dept2_Redirect_to_Dept1() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.Auto2_department, "button").click();
		logger.info("click on Auto2_department button");
		waitAndLog(2);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1_department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1_department));
			textbox(dlink.Auto1_department).isEnabled();
			logger.info("Redirected to Auto1 Department from Auto2 Department");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1_department));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));

	}

	public void Redirect_TollFreeDept_To_Auto1Dept() throws Exception {

		waitAndLog(3);

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.TollFree_Button_department, "button").click();
		logger.info("click on TollFree_Button_department button");
		waitAndLog(2);

		button(dlink.TollFree_department, "button").click();
		logger.info("click on TollFree_department button");
		waitAndLog(2);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(2);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		if (verifyElementIsEnabled(dlink.Auto1_department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1_department));
			textbox(dlink.Auto1_department).isEnabled();
			logger.info("Redirected to Auto1 Department from Toll Free Department");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1_department));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(3);
	}

	public void MultiOrg_Active_In_TollFree_2nd_Redirect_Same_User_Auto1_1stuser_() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.TollFree_Button_department, "button").click();
		logger.info("click on TollFree_Button_department button");
		waitAndLog(2);

		button(dlink.TollFree_department, "button").click();
		logger.info("click on TollFree_department button");
		waitAndLog(2);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1Login)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1Login));
			textbox(dlink.Auto1Login).isEnabled();
			logger.info("Redirected to Same User and Same department - Auto1 1st login form 2nd Org");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1Login));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(dlink.Auto1_department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1_department));
			textbox(dlink.Auto1_department).isEnabled();
			logger.info("Redirected to Same User and Same department - Auto1 1st login form 2nd Org");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1_department));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
		validateSignOutLink();
		validateSignOut();
		waitAndLog(5);
	}

	public void PinMsg_Dept_switch() throws Exception {

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		waitAndLog(2);

		button(dlink.PinIcon_auto2, "button").click();
		logger.info("click on PinIcon_auto2 button");
		waitAndLog(2);

		JSClick(repository.chatInputsendButton);
		logger.info("click on send button");
		waitAndLog(5);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id :auto2@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("auto2@yopmail.com");
		logger.info("entering the mail in text box : auto2@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1_department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1_department));
			textbox(dlink.Auto1_department).isEnabled();
			logger.info("Redirected to Auto1 Department from Auto2 Department");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1_department));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
		validateSignOutLink();
		validateSignOut();
		waitAndLog(5);

	}

	public void PinMsg_org_switch() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		waitAndLog(2);

		button(dlink.PinIcon_auto2, "button").click();
		logger.info("click on PinIcon_auto2 button");
		waitAndLog(2);

		JSClick(repository.chatInputsendButton);
		logger.info("click on send button");
		waitAndLog(5);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id :auto2@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.auto1_radiobttn, "Arrow").click();
		logger.info("click on auto1_radiobttn Button");
		waitAndLog(2);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("auto2@yopmail.com");
		logger.info("entering the mail in text box : auto2@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1_department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1_department));
			textbox(dlink.Auto1_department).isEnabled();
			logger.info("Redirected to Same User and Same department - Auto1 1st login form 2nd Org");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1_department));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
		validateSignOutLink();
		validateSignOut();
		waitAndLog(5);
	}

	public void PinMsg_org_switch_from_tollFree() throws Exception {

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		waitAndLog(2);

		button(dlink.PinIcon_auto2, "button").click();
		logger.info("click on PinIcon_auto2 button");
		waitAndLog(2);

		JSClick(repository.chatInputsendButton);
		logger.info("click on send button");
		waitAndLog(5);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id :auto2@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.TollFree_Button_department, "button").click();
		logger.info("click on TollFree_Button_department button");
		waitAndLog(2);

		button(dlink.TollFree_department, "button").click();
		logger.info("click on TollFree_department button");
		waitAndLog(2);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("auto2@yopmail.com");
		logger.info("entering the mail in text box : auto2@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(dlink.Auto1_department)) {
			Assert.assertEquals(true, verifyElementIsEnabled(dlink.Auto1_department));
			textbox(dlink.Auto1_department).isEnabled();
			logger.info("Redirected to Same User and Same department - Auto1 1st login form 2nd Org Toll Free");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(dlink.Auto1_department));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
		validateSignOutLink();
		validateSignOut();
		waitAndLog(5);
	}

	public void PinMsg_DepartmentRemoved() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.Dummy_department, "button").click();
		logger.info("click on Dummy_department button");
		waitAndLog(2);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		button(Deeplink.AutomationDummy_pin, "button").click();
		logger.info("click on AutomationDummy_pin button");
		waitAndLog(2);

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on send button");
		waitAndLog(5);

		button(Deeplink.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(Deeplink.settings_Agents, "click on settings_Agents button").click();
		logger.info("click on settings_Agents Button");
		waitAndLog(2);

		button(Deeplink.Automation_dummy_Agent_Manage, "click on Automation_dummy_Agent_Manage button").click();
		logger.info("click on Automation_dummy_Agent_Manage Button");
		waitAndLog(2);

		button(Deeplink.Department_Dummy_Checkbox, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Department_Dummy_Checkbox Button");

		button(Deeplink.Save_Agent_Changes, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Save_Agent_Changes Button");
		waitAndLog(2);

		button(dlink.Settings_Close, "button").click();
		logger.info("click on Settings_Close button");
		waitAndLog(2);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("automationdummy@yopmail.com");
		logger.info("entering the mail in text box : automationdummy@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		logger.info(dlink.Dept_Not_Found.getText());
		if (dlink.Dept_Not_Found.getText().equalsIgnoreCase("Department not found.")) {
			Assert.assertEquals(dlink.Dept_Not_Found.getText(), "Department not found.", "Dept_Not_Found is matching");
			logger.info(dlink.Dept_Not_Found.getText());
			TestUtil.takeScreenshotAtEndOfTest("Dept_Not_Found");// ScreenShot capture

		} else {
			Assert.assertNotEquals(dlink.Dept_Not_Found.getText(), "Department not found.",
					"Dept_Not_Found is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Dept_Not_Found");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(2);
		button(Deeplink.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(Deeplink.settings_Agents, "click on settings_Agents button").click();
		logger.info("click on settings_Agents Button");
		waitAndLog(2);

		button(Deeplink.Automation_dummy_Agent_Manage, "click on Automation_dummy_Agent_Manage button").click();
		logger.info("click on Automation_dummy_Agent_Manage Button");
		waitAndLog(2);

		button(Deeplink.Department_Dummy_Checkbox, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Department_Dummy_Checkbox Button");

		button(Deeplink.Save_Agent_Changes, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Save_Agent_Changes Button");
		waitAndLog(2);

		button(dlink.Settings_Close, "button").click();
		logger.info("click on Settings_Close button");
		waitAndLog(5);

	}

	public void PinMsg_DepartmentDeleted() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.Dummy_department, "button").click();
		logger.info("click on Dummy_department button");
		waitAndLog(2);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		button(Deeplink.AutomationDummy_pin, "button").click();
		logger.info("click on AutomationDummy_pin button");
		waitAndLog(2);

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on send button");
		waitAndLog(5);

		validateSignOutLink();
		validateSignOut();

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("automationdummy@yopmail.com");
		logger.info("entering the mail in text box : automationdummy@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		if (verifyElementIsEnabled(Repository.user_id)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.user_id));
			textbox(Repository.user_id).isEnabled();
			logger.info("Login Page Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.user_id));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

	}

	public void MultiOrg_PinMsg_DepartmentRemoved() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.Dummy_department, "button").click();
		logger.info("click on Dummy_department button");
		waitAndLog(2);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		button(Deeplink.AutomationDummy_pin, "button").click();
		logger.info("click on AutomationDummy_pin button");
		waitAndLog(2);

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on send button");
		waitAndLog(5);

		button(Deeplink.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(Deeplink.settings_Agents, "click on settings_Agents button").click();
		logger.info("click on settings_Agents Button");
		waitAndLog(2);

		button(Deeplink.Automation_dummy_Agent_Manage, "click on Automation_dummy_Agent_Manage button").click();
		logger.info("click on Automation_dummy_Agent_Manage Button");
		waitAndLog(2);

		button(Deeplink.Department_Dummy_Checkbox, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Department_Dummy_Checkbox Button");

		button(Deeplink.Save_Agent_Changes, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Save_Agent_Changes Button");
		waitAndLog(2);

		button(dlink.Settings_Close, "button").click();
		logger.info("click on Settings_Close button");
		waitAndLog(2);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id :auto2@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("automationdummy@yopmail.com");
		logger.info("entering the mail in text box : automationdummy@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		logger.info(dlink.Invalid_Deeplink.getText());
		if (dlink.Invalid_Deeplink.getText().equalsIgnoreCase("Invalid deeplink Id.")) {
			Assert.assertEquals(dlink.Invalid_Deeplink.getText(), "Invalid deeplink Id.", "message is matching");
			logger.info(dlink.Invalid_Deeplink.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(dlink.Invalid_Deeplink.getText(), "Invalid deeplink Id.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
		validateSignOutLink();
		validateSignOut();
		waitAndLog(3);
		button(Deeplink.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(Deeplink.settings_Agents, "click on settings_Agents button").click();
		logger.info("click on settings_Agents Button");
		waitAndLog(2);

		button(Deeplink.Automation_dummy_Agent_Manage, "click on Automation_dummy_Agent_Manage button").click();
		logger.info("click on Automation_dummy_Agent_Manage Button");
		waitAndLog(2);

		button(Deeplink.Department_Dummy_Checkbox, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Department_Dummy_Checkbox Button");

		button(Deeplink.Save_Agent_Changes, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Save_Agent_Changes Button");
		waitAndLog(2);

		button(dlink.Settings_Close, "button").click();
		logger.info("click on Settings_Close button");
		waitAndLog(5);

	}

	public void MultiOrg_PinMsg_DepartmentDeleted() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.Dummy_department, "button").click();
		logger.info("click on Dummy_department button");
		waitAndLog(2);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		button(Deeplink.AutomationDummy_pin, "button").click();
		logger.info("click on AutomationDummy_pin button");
		waitAndLog(2);

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on send button");
		waitAndLog(5);

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("automationdummy@yopmail.com");
		logger.info("entering the mail in text box : automationdummy@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		logger.info(dlink.Invalid_Deeplink.getText());
		if (dlink.Invalid_Deeplink.getText().equalsIgnoreCase("Invalid deeplink Id.")) {
			Assert.assertEquals(dlink.Invalid_Deeplink.getText(), "Invalid deeplink Id.", "message is matching");
			logger.info(dlink.Invalid_Deeplink.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(dlink.Invalid_Deeplink.getText(), "Invalid deeplink Id.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);

	}

	public void Msg_DepartmentRemoved() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.Dummy_department, "button").click();
		logger.info("click on Dummy_department button");
		waitAndLog(2);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		button(Deeplink.AutomationDummy_pin, "button").click();
		logger.info("click on AutomationDummy_pin button");
		waitAndLog(2);

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on send button");
		waitAndLog(5);

		button(Deeplink.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(Deeplink.settings_Agents, "click on settings_Agents button").click();
		logger.info("click on settings_Agents Button");
		waitAndLog(2);

		button(Deeplink.Automation_dummy_Agent_Manage, "click on Automation_dummy_Agent_Manage button").click();
		logger.info("click on Automation_dummy_Agent_Manage Button");
		waitAndLog(2);

		button(Deeplink.Department_Dummy_Checkbox, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Department_Dummy_Checkbox Button");

		button(Deeplink.Save_Agent_Changes, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Save_Agent_Changes Button");
		waitAndLog(2);

		button(dlink.Settings_Close, "button").click();
		logger.info("click on Settings_Close button");
		waitAndLog(2);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("automationdummy@yopmail.com");
		logger.info("entering the mail in text box : automationdummy@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		logger.info(dlink.Dept_Not_Found.getText());
		if (dlink.Dept_Not_Found.getText().equalsIgnoreCase("Department not found.")) {
			Assert.assertEquals(dlink.Dept_Not_Found.getText(), "Department not found.", "Dept_Not_Found is matching");
			logger.info(dlink.Dept_Not_Found.getText());
			TestUtil.takeScreenshotAtEndOfTest("Dept_Not_Found");// ScreenShot capture

		} else {
			Assert.assertNotEquals(dlink.Dept_Not_Found.getText(), "Department not found.",
					"Dept_Not_Found is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Dept_Not_Found");// ScreenShot capture
		}
		waitAndLog(3);

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));

		button(Deeplink.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(Deeplink.settings_Agents, "click on settings_Agents button").click();
		logger.info("click on settings_Agents Button");
		waitAndLog(2);

		button(Deeplink.Automation_dummy_Agent_Manage, "click on Automation_dummy_Agent_Manage button").click();
		logger.info("click on Automation_dummy_Agent_Manage Button");
		waitAndLog(2);

		button(Deeplink.Department_Dummy_Checkbox, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Department_Dummy_Checkbox Button");

		button(Deeplink.Save_Agent_Changes, "click on Department_Dummy_Checkbox button").click();
		logger.info("click on Save_Agent_Changes Button");
		waitAndLog(2);

		button(dlink.Settings_Close, "button").click();
		logger.info("click on Settings_Close button");
		waitAndLog(5);
//		validateSignOutLink();
//		validateSignOut();
//		waitAndLog(3);
	}

	public void Msg_DepartmentDeleted() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(dlink.department_Head, "button").click();
		logger.info("click on department_Head button");
		waitAndLog(2);

		button(dlink.Dummy_department, "button").click();
		logger.info("click on Dummy_department button");
		waitAndLog(2);

		button(dlink.PinIcon, "button").click();
		logger.info("click on PinIcon button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("@");
		logger.info("enter the message");
		button(Deeplink.AutomationDummy_pin, "button").click();
		logger.info("click on AutomationDummy_pin button");
		waitAndLog(2);

		button(repository.chatInputsendButton, "button").click();
		logger.info("click on send button");
		waitAndLog(5);

		validateSignOutLink();
		validateSignOut();

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("automationdummy@yopmail.com");
		logger.info("entering the mail in text box : automationdummy@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));

		waitAndLog(3);

		if (verifyElementIsEnabled(Repository.user_id)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.user_id));
			textbox(Repository.user_id).isEnabled();
			logger.info("Login Page Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.user_id));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(5);

	}

	public void Msg_received_Block_Contact() throws Exception {
		waitAndLog(3);
		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(5);

		button(repository.Automation_Contact, "click on Automation_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		button(repository.chatInputsendButton2, "button").click();
		waitAndLog(3);

		button(org_Switch_Pages.Arrow, "Arrow").click();
		logger.info("click on Arrow Button");
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");

		textbox(Repository.user_id).sendKeys(username2);
		logger.info("Email id :" + username2);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		waitAndLog(3);

		button(repository.Auto1_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Auto1_Contact Button");
		waitAndLog(2);

		button(repository.contact_blocked, "contact_blocked").click();
		logger.info("click on contact_blocked Button");
		waitAndLog(2);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://www.yopmail.com/");
		textbox(dlink.mail_textbar).clear();
		textbox(dlink.mail_textbar).sendKeys("automations");
		logger.info("entering the mail in text box : automations@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(3);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(2));

		waitAndLog(3);

		button(repository.Auto1_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Auto1_Contact Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(repository.contact_unblock)) {
			Assert.assertEquals(true, verifyElementIsEnabled(repository.contact_unblock));
			textbox(repository.contact_unblock).isEnabled();
			logger.info("contact_unblock button Enabled");
			TestUtil.takeScreenshotAtEndOfTest("contact_unblock");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(repository.contact_unblock));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("contact_unblock");// ScreenShot capture
		}

		waitAndLog(3);

		button(repository.contact_unblock, "click on contact_unblock button").click();
		waitAndLog(3);

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(5);
//		validateSignOutLink();
//		validateSignOut();
//		waitAndLog(3);

	}

}
