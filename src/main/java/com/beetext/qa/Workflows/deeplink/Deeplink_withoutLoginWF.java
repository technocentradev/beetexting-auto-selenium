package com.beetext.qa.Workflows.deeplink;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Deeplink;
import com.beetext.qa.pages.Pin_Messages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class Deeplink_withoutLoginWF extends Broadcast {
	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Repository repository = new Repository();
	Pin_Messages pinMessage = new Pin_Messages();
	TestBase testBase = new TestBase();
	Deeplink dlink = new Deeplink();

	public void Deepplink_without_login() throws Exception {

		waitAndLog(3);
		textbox(dlink.mail_textbar).sendKeys(username1);
		logger.info("entering the mail in text box : auto1@yopmail.com");
		waitAndLog(2);

		button(dlink.mail_Submit, "button").click();
		logger.info("click on mail_Submit button");
		waitAndLog(5);

		int size = driver.findElements(By.tagName("iframe")).size();
		System.out.println("size" + size);
		driver.switchTo().frame(2);
		waitAndLog(3);

		button(dlink.reply_Link, "button").click();
		logger.info("click on reply_Link button");
		waitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waitAndLog(3);

		if (verifyElementIsEnabled(Repository.user_id)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Repository.user_id));
			textbox(Repository.user_id).isEnabled();
			System.out.println("userid is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Repository.user_id));
			System.out.println("Userid is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

}
