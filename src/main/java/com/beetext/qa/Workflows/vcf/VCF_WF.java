package com.beetext.qa.Workflows.vcf;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;

public class VCF_WF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void Vcard_toggle() throws Exception {

		waitAndLog(2);

		VCard_Automations();

		if (verifyElementIsEnabled(vcf_Pages.VCard_toggle)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			button(vcf_Pages.VCard_toggle, "buton").isEnabled();
			logger.info("VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		settingsclose();
	}

	public void Vcard_toggle_Enable_Disable_Click_on_CrossMark() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		if (verifyElementIsEnabled(vcf_Pages.VCard_toggle)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			button(vcf_Pages.VCard_toggle, "buton").isEnabled();
			logger.info("VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			logger.info("VCard toggle Disabled");
			logger.info("By Default VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_NamePrefix)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_NamePrefix));
			button(vcf_Pages.Vcard_NamePrefix, "buton").isEnabled();
			logger.info("Vcard NamePrefix is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_NamePrefix));
			logger.info("Vcard NamePrefix Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_FirstName)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_FirstName));
			button(vcf_Pages.Vcard_FirstName, "buton").isEnabled();
			logger.info("Vcard_FirstName is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_FirstName));
			logger.info("Vcard_FirstName Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_MiddleName)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_MiddleName));
			button(vcf_Pages.Vcard_MiddleName, "buton").isEnabled();
			logger.info("Vcard_MiddleName is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_MiddleName));
			logger.info("Vcard_MiddleName Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_LastName)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_LastName));
			button(vcf_Pages.Vcard_LastName, "buton").isEnabled();
			logger.info("Vcard_LastName is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_LastName));
			logger.info("Vcard_LastName Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_NameSuffix)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_NameSuffix));
			button(vcf_Pages.Vcard_NameSuffix, "buton").isEnabled();
			logger.info("Vcard_NameSuffix is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_NameSuffix));
			logger.info("Vcard_NameSuffix Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Email)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Email));
			button(vcf_Pages.Vcard_Email, "buton").isEnabled();
			logger.info("Vcard_Email is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Email));
			logger.info("Vcard_NameSuffix Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_CompanyName)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_CompanyName));
			button(vcf_Pages.Vcard_CompanyName, "buton").isEnabled();
			logger.info("Vcard_CompanyName is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_CompanyName));
			logger.info("Vcard_CompanyName Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_PhoneNumber)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_PhoneNumber));
			button(vcf_Pages.Vcard_PhoneNumber, "buton").isEnabled();
			logger.info("Vcard_PhoneNumber is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_PhoneNumber));
			logger.info("Vcard_CompanyName Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_WebsiteURL)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_WebsiteURL));
			button(vcf_Pages.Vcard_WebsiteURL, "buton").isEnabled();
			logger.info("Vcard_WebsiteURL is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_WebsiteURL));
			logger.info("Vcard_WebsiteURL Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Description)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Description));
			button(vcf_Pages.Vcard_Description, "buton").isEnabled();
			logger.info("Vcard_Description is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Description));
			logger.info("Vcard_Description Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		settingsclose();

		logger.info("Setings Window Closed");

		if (verifyElementIsEnabled(vcf_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.tools_bttn));
			textbox(vcf_Pages.tools_bttn).isEnabled();
			logger.info("Tools Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.tools_bttn));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.test_Manage, "click on test_Manage button").click();
		logger.info("click on test_Manage Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");
		implicitwaitAndLog(2);
		if (verifyElementIsEnabled(vcf_Pages.VCard_toggle)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			button(vcf_Pages.VCard_toggle, "buton").isEnabled();
			logger.info("VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			logger.info("VCard toggle Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");
		waitAndLog(2);

		settingsclose();

	}

	public void Vcard_Available_Department() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.VCard_File_Name)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_File_Name));
			textbox(vcf_Pages.VCard_File_Name).isEnabled();
			logger.info("VCard_File_Name is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("VCard_File_Name");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_File_Name));
			logger.info("VCard_File_Name is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("VCard_File_Name");// ScreenShot capture
		}

	}

	public void Vcard_Valid_Details_Names_Description_Allows_Special_Characers_CloseWindow() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		VCard_Automations_Fill_All_Details();
		waitAndLog(2);
		VCard_Automations_Fill_All_Details_With_SpecialCharacters();

		settingsclose();

		logger.info("Setings Window Closed");

		if (verifyElementIsEnabled(vcf_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.tools_bttn));
			textbox(vcf_Pages.tools_bttn).isEnabled();
			logger.info("Tools Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.tools_bttn));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Vcard_Valid_Details_Click_Save() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		VCard_Automations_Fill_All_Details();

		button(vcf_Pages.Vcard_Save, "click on Vcard_Save button").click();
		logger.info("click on Vcard_Save Button");

		vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText();
		if (vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText()
				.equalsIgnoreCase("Vcard successfully updated.")) {
			Assert.assertEquals(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText(),
					"Vcard successfully updated.", "Vcard_Status_Successfully_Updated_Toaster is matching");
			logger.info(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText());
			logger.info("First Name Allows Special Characters and Numbers");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Status_Successfully_Updated_Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText(),
					"Vcard successfully updated.", "Vcard_Status_Successfully_Updated_Toaster is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Status_Successfully_Updated_Toaster");// ScreenShot capture
		}

		settingsclose();

	}

	public void Vcard_Valid_Details_Click_Cancel() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		VCard_Automations_Fill_All_Details();

		button(vcf_Pages.Vcard_Cancel, "click on Vcard_Cancel button").click();
		logger.info("click on Vcard_Cancel Button");
		waitAndLog(3);

		settingsclose();

	}

	public void Vcard_If_Any_UI_Error_Save_in_Disable() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		textbox(vcf_Pages.Vcard_WebsiteURL).sendKeys("automation.com");
		logger.info("Text the Details of Vcard_WebsiteURL");

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Save));
			textbox(vcf_Pages.Vcard_Save).isEnabled();
			logger.info("VCard Save is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Save));
			logger.info("VCard Save is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		settingsclose();

	}

	public void Vcard_Valid_Details_Click_Outside() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		VCard_Automations_Fill_All_Details();

		settingsclose();

		if (verifyElementIsEnabled(vcf_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.tools_bttn));
			textbox(vcf_Pages.tools_bttn).isEnabled();
			logger.info("Tools Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.tools_bttn));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Vcard_PhoneNUmber_US_Format_10_Digits() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		String Phone_Number = vcf_Pages.Vcard_PhoneNumber.getAttribute("value");
		if (Phone_Number.equalsIgnoreCase("(555) 236-4873")) {
			Assert.assertEquals(Phone_Number, "(555) 236-4873", "Message is matching");
			logger.info("Phone_Number" + '\n');
			logger.info("Mobile Number is in US Format " + '\n');
			logger.info("Mobile Number is with 10 Digits" + '\n');
			logger.info("By Defaultly Mobile Number is Showing Department Number." + '\n');
			TestUtil.takeScreenshotAtEndOfTest("Vcard_PhoneNumber");// ScreenShot capture

		} else {
			Assert.assertNotEquals(Phone_Number, "(555) 236-4873", "Message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_PhoneNumber");// ScreenShot capture
		}
		settingsclose();
	}

	public void Vcard_Email_Error() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		textbox(vcf_Pages.Vcard_Email).sendKeys("qa");
		logger.info("Text the Details of Vcard_Email");

		vcf_Pages.Vcard_Email_Error.getText();
		if (vcf_Pages.Vcard_Email_Error.getText().equalsIgnoreCase("Email not valid.")) {
			Assert.assertEquals(vcf_Pages.Vcard_Email_Error.getText(), "Email not valid.",
					"Vcard_Email_Error is matching");
			logger.info(vcf_Pages.Vcard_Email_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Email_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_Email_Error.getText(), "Email not valid.",
					"Vcard_Email_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Email_Error");// ScreenShot capture
		}
		settingsclose();
	}

	public void Vcard_Valid_Email() throws Exception {

		VCard_Auto1_Login();

		textbox(vcf_Pages.Vcard_Email).sendKeys("auto1@yopmail.com");
		logger.info("Text the Details of auto1@yopmail.com");

		button(vcf_Pages.Vcard_Save, "click on Vcard_Save button").click();
		logger.info("click on Vcard_Save Button");
		waitAndLog(3);

		settingsclose();

	}

	public void Vcard_Company_Allows_50_Char_Error() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		textbox(vcf_Pages.Vcard_CompanyName).clear();
		String CompanyName = RandomStringUtils.randomAlphabetic(50);
		textbox(vcf_Pages.Vcard_CompanyName).sendKeys(CompanyName);
		logger.info("enter the Vcard_CompanyName");

		vcf_Pages.Vcard_Company_Error.getText();
		if (vcf_Pages.Vcard_Company_Error.getText().equalsIgnoreCase("Limit Company Name length to 50 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_Company_Error.getText(), "Limit Company Name length to 50 characters.",
					"Vcard_Company_Error is matching");
			logger.info(vcf_Pages.Vcard_Company_Error.getText());
			logger.info("Company Name Allows Special Characters and Numbers");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Company_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_Company_Error.getText(),
					"Limit Company Name length to 50 characters.", "Vcard_Company_Error is not  matching");
			logger.info("Failed" + vcf_Pages.Vcard_Company_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Company_Error");// ScreenShot capture
		}
		settingsclose();
	}

	public void Vcard_Company_Allows_Note_message() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		textbox(vcf_Pages.Vcard_Description).sendKeys("Hello Team" + "\n" + " Technocentra - BeeTexting");
		logger.info("enter the Vcard_Description");
		settingsclose();
	}

	public void Vcard_Profile_Pic_Upload() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		WebElement uploadElement = driver.findElement(By.xpath("//input[@class='d-none']"));

		uploadElement.sendKeys(currentDir + "\\Files\\SamplePng.png");

		settingsclose();
	}

	public void Vcard_Profile_Pic_Upload_Error() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		WebElement uploadElement = driver.findElement(By.xpath("//input[@class='d-none']"));

		uploadElement.sendKeys(currentDir + "\\Files\\pdf.pdf");

		vcf_Pages.Vcard_File_Format_Not_Supported.getText();
		if (vcf_Pages.Vcard_File_Format_Not_Supported.getText().equalsIgnoreCase("File format is not supported.")) {
			Assert.assertEquals(vcf_Pages.Vcard_File_Format_Not_Supported.getText(), "File format is not supported.",
					"Vcard_File_Format_Not_Supported is matching");
			logger.info(vcf_Pages.Vcard_File_Format_Not_Supported.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_File_Format_Not_Supported");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_File_Format_Not_Supported.getText(), "File format is not supported.",
					"Vcard_File_Format_Not_Supported is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_File_Format_Not_Supported");// ScreenShot capture
		}
		settingsclose();

	}

	public void Vcard_Admin_Can_Send_Vcard() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Validation();

	}

	public void Vcard_Admin_Can_Send_Vcard_Along_With_Msg_Attachment() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);
		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		if (verifyElementIsEnabled(vcf_Pages.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.image_verification));
			textbox(vcf_Pages.image_verification).isEnabled();
			logger.info("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.image_verification));
			logger.info("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void Vcard_Admin_Can_Send_Vcard_Attachment() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		if (verifyElementIsEnabled(vcf_Pages.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.image_verification));
			textbox(vcf_Pages.image_verification).isEnabled();
			logger.info("PNG file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.image_verification));
			logger.info("PNG file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void Send_Vcard_Validation_Vcard_For_Single_Contact() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Validation();

	}

	public void VCard_Send_wih_Emoji() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_For_Group_Conversation() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + username2);
		waitAndLog(2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(3);

		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");

		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		waitAndLog(2);
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_Agent_Can_Sent_Vcard_For_Single_Conversation() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void Vcard_Agent_Can_Send_Vcard_Along_With_Msg_Emoji_Attachment() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);
		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");

	}

	public void VCard_Agent_Can_Send_Group_Conversation() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();
		waitAndLog(2);

		button(vcf_Pages.groupConversation, "click on groupConversation button").click();
		logger.info("click on groupConversation Button");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_Manage() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		String CompanyName = RandomStringUtils.randomAlphabetic(5);
		textbox(vcf_Pages.Vcard_CompanyName).sendKeys(CompanyName);
		logger.info("enter the Vcard_CompanyName");

		settingsclose();
	}

	public void Click_On_VCard_Navigate_To_ThatPage() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		if (verifyElementIsEnabled(vcf_Pages.Vcard_PhoneNumber)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_PhoneNumber));
			vcf_Pages.Vcard_PhoneNumber.isEnabled();
			logger.info("After clicking on VCard, It Navigates to VCard Page is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_PhoneNumber));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		settingsclose();
	}

	public void Vcard_By_Default_DeptNumber_taken_as_Firstname() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		String FirstName = vcf_Pages.Vcard_FirstName.getText();
		if (FirstName.equalsIgnoreCase("Dummy")) {
			Assert.assertEquals(FirstName, "Dummy", "Message is matching");
			logger.info("By Default Depart Number is Taken as First Name" + '\n');
			TestUtil.takeScreenshotAtEndOfTest("Vcard_FirstName");// ScreenShot capture

		} else {
			Assert.assertNotEquals(FirstName, "Dummy", "Message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("FirstName");// ScreenShot capture
		}

		settingsclose();
	}

	public void Vcard_firstname_50_Char_Error() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		String FirstName = RandomStringUtils.randomAlphabetic(40);
		textbox(vcf_Pages.Vcard_FirstName).sendKeys("1@$%*!()^&" + FirstName);
		logger.info("enter the Vcard_FirstName");

		vcf_Pages.Vcard_FirstName_Error.getText();
		if (vcf_Pages.Vcard_FirstName_Error.getText().equalsIgnoreCase("Limit First Name length to 50 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_FirstName_Error.getText(), "Limit First Name length to 50 characters.",
					"Vcard_FirstName_Error is matching");
			logger.info(vcf_Pages.Vcard_FirstName_Error.getText());
			logger.info("Limit First Name Allows Special Characters and Numbers");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_FirstName_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_FirstName_Error.getText(), "First Name can be max 50 characters.",
					"Vcard_FirstName_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_FirstName_Error");// ScreenShot capture
		}
		settingsclose();
	}

	public void Vcard_Alldetails_Cancel() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		VCard_Automations_Fill_All_Details();

		button(vcf_Pages.Vcard_Cancel, "click on Vcard_Cancel button").click();
		logger.info("click on Vcard_Cancel Button");
		waitAndLog(3);

		settingsclose();

	}

	public void Vcard_Alldetails_Save() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("automationblock@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waitAndLog(5);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);

		button(vcf_Pages.automationBlock_Manage, "click on test_Manage button").click();
		logger.info("click on automationBlock_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(3);

		VCard_Automations_Fill_All_Details();

		button(vcf_Pages.Vcard_Save, "click on Vcard_Save button").click();
		logger.info("click on Vcard_Save Button");

		vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText();
		if (vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText()
				.equalsIgnoreCase("Vcard successfully updated.")) {
			Assert.assertEquals(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText(),
					"Vcard successfully updated.", "Vcard_Status_Successfully_Updated_Toaster is matching");
			logger.info(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText());
			logger.info("First Name Allows Special Characters and Numbers");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Status_Successfully_Updated_Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText(),
					"Vcard successfully updated.", "Vcard_Status_Successfully_Updated_Toaster is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Status_Successfully_Updated_Toaster");// ScreenShot capture
		}

		settingsclose();

	}

	public void VCard_Enable_in_Conversation() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon in Conversation is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon in Conversation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void VCard_Agent_Can_See_Vcard_Details_not_able_to_Edit() throws Exception {

		AgentAutomation_Dummy_Agent_Account();

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);

		button(vcf_Pages.testdeptManage, "click on testdeptManage button").click();
		logger.info("click on testdeptManage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Save));
			textbox(vcf_Pages.Vcard_Save).isEnabled();
			logger.info("VCard Save is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Save));
			logger.info("VCard Save is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Cancel)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Cancel));
			textbox(vcf_Pages.Vcard_Cancel).isEnabled();
			logger.info("VCard Cancel is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Cancel));
			logger.info("VCard Cancel is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		settingsclose();
	}

	public void VCard_Selection() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void Vcard_Sending_Validation_at_ReceivingSide() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Validation();

	}

	public void Vcard_Msg_validation() throws Exception {

		VCard_Auto1_Automations_Contact();

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();
		VCard_Validation();

	}

	public void VCard_9_Attachments() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);
		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_1000_CharMsg() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		waitAndLog(2);

		String generatedstring = "The accuracy of information provided to Beetexting, both during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are as follows:The accu";
		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys(generatedstring);
		waitAndLog(5);
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);
		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void VCard_Emoji() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void VCard_For_Group_Conversation_with_attachment() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(2);
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");

		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);
		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_Compose_Contact_VCard_send() throws Exception {

		VCard_Automations_Compose_Auto1_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_Compose_Contact_VCard_send_msg_Attachment() throws Exception {

		VCard_Automations_Compose_Auto1_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);
		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_Compose_Group_Conversation() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(2);
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");

		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		waitAndLog(2);
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

	}

	public void VCard_Compose_Group_Conversation_with_Msg_attachment() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(2);
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number - Automation");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("QA");
		logger.info("text the Number - Automation");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		waitAndLog(2);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);
		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void VCard_Disable_After_Clicking() throws Exception {

		AgentAutomation_Dummy_Agent_Account();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

	}

	public void Vcard_Attaches_Department_Only() throws Exception {

		AgentAutomation_Dummy_Agent_Account();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.VCard_File_Name)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_File_Name));
			textbox(vcf_Pages.VCard_File_Name).isEnabled();
			logger.info("VCard_File_Name is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("VCard_File_Name");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_File_Name));
			logger.info("VCard_File_Name is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("VCard_File_Name");// ScreenShot capture
		}
	}

	public void Compose_Number_VCard_Msg_Multiple_Attachmens() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Vcar");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");
		waitAndLog(5);
		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Compose_Number_VCard_1000_Char() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(2);
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Vcar");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");

		String generatedstring = "The accuracy of information provided to Beetexting, both during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are as follows:The accu";
		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys(generatedstring);
		waitAndLog(5);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Compose_Number_VCard_Emoji() throws Exception {

		AgentAutomation_Dummy_Agent_Account();
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Vcar");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		waitAndLog(2);
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Admin_Group_Conversation_VCard() throws Exception {

		AgentAutomation_Admin_Account();
		waitAndLog(10);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(2);
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Vcar");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Dum");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		waitAndLog(2);
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Admin_Group_Conversation_VCard_Msg_Attachment() throws Exception {

		AgentAutomation_Admin_Account();
		waitAndLog(10);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(2);
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Vcar");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Dum");
		logger.info("text the Number");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		waitAndLog(2);
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\gif_file.gif");
		waitAndLog(5);
		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void VCard_Available_In_BM_SM_Templates_Automations() throws Exception {

		AgentAutomation_Admin_Account();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon is Avaiable in Broadcast ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "Click on Tools_SM").click();
		logger.info("click on Tools_SM");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "Click on Tools_SM_Create").click();
		logger.info("click on Tools_SM_Create");
		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon is Avaiable in Schedule Messages ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.Tools_SM, "Click on Tools_SM").click();
		logger.info("click on Tools_SM");
		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on create_templates");

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon is Avaiable in Templates ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon is Avaiable in Automations ");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "Click on tools_crossBtn").click();
		logger.info("click on tools_crossBtn");
		waitAndLog(2);

	}

	public void BM_Vcard_Enable() throws Exception {

		AgentAutomation_Admin_Account();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "click on create_broadcast button").click();
		logger.info("click on create_broadcast Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon in Conversation is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon in Conversation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(vcf_Pages.broadCast_msg, "button").click();
		logger.info("click on broadCast ");
		waitAndLog(2);
		button(vcf_Pages.tools_crossBtn, "click on tools_crossBtn button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

	}

	public void Vcard_Prefix_Suffix_10_Char_Error() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		String prefix_Suffix = RandomStringUtils.randomAlphabetic(10);
		textbox(vcf_Pages.Vcard_NamePrefix).sendKeys(prefix_Suffix);
		logger.info("enter the Vcard_NamePrefix");

		vcf_Pages.Vcard_PrefixName_Error.getText();
		if (vcf_Pages.Vcard_PrefixName_Error.getText().equalsIgnoreCase("Limit Name Prefix length to 10 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_PrefixName_Error.getText(),
					"Limit Name Prefix length to 10 characters.", "Vcard_Company_Error is matching");
			logger.info(vcf_Pages.Vcard_PrefixName_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_PrefixName_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_PrefixName_Error.getText(),
					"Limit Name Prefix length to 10 characters.", "Vcard_PrefixName_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_PrefixName_Error");// ScreenShot capture
		}

		textbox(vcf_Pages.Vcard_NameSuffix).sendKeys(prefix_Suffix);
		logger.info("enter the Vcard_NameSuffix");

		vcf_Pages.Vcard_SuffixName_Error.getText();
		if (vcf_Pages.Vcard_SuffixName_Error.getText().equalsIgnoreCase("Limit Name Suffix length to 10 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_SuffixName_Error.getText(),
					"Limit Name Suffix length to 10 characters.", "Vcard_SuffixName_Error is matching");
			logger.info(vcf_Pages.Vcard_SuffixName_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_SuffixName_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_SuffixName_Error.getText(),
					"Limit Name Suffix length to 10 characters.", "Vcard_SuffixName_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_SuffixName_Error");// ScreenShot capture
		}
		settingsclose();
	}

	public void Vcard_Company_First_Middle_Last_Allows_50_Char_Error() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();

		String CompanyName = RandomStringUtils.randomAlphabetic(50);

		textbox(vcf_Pages.Vcard_FirstName).sendKeys(CompanyName);
		logger.info("enter the Vcard_FirstName");

		vcf_Pages.Vcard_FirstName_Error.getText();
		if (vcf_Pages.Vcard_FirstName_Error.getText().equalsIgnoreCase("Limit First Name length to 50 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_FirstName_Error.getText(), "Limit First Name length to 50 characters.",
					"Vcard_FirstName_Error is matching");
			logger.info(vcf_Pages.Vcard_FirstName_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Company_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_FirstName_Error.getText(),
					"Limit First Name length to 50 characters.", "Vcard_FirstName_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_FirstName_Error");// ScreenShot capture
		}

		textbox(vcf_Pages.Vcard_MiddleName).sendKeys(CompanyName);
		logger.info("enter the Vcard_MiddleName");

		vcf_Pages.Vcard_MiddleName_Error.getText();
		if (vcf_Pages.Vcard_MiddleName_Error.getText().equalsIgnoreCase("Limit Middle Name length to 50 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_MiddleName_Error.getText(),
					"Limit Middle Name length to 50 characters.", "Vcard_MiddleName_Error is matching");
			logger.info(vcf_Pages.Vcard_MiddleName_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_MiddleName_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_MiddleName_Error.getText(),
					"Limit Middle Name length to 50 characters.", "Vcard_MiddleName_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_MiddleName_Error");// ScreenShot capture
		}

		textbox(vcf_Pages.Vcard_LastName).sendKeys(CompanyName);
		logger.info("enter the Vcard_LastName");

		vcf_Pages.Vcard_LastName_Error.getText();
		if (vcf_Pages.Vcard_LastName_Error.getText().equalsIgnoreCase("Limit Last Name length to 50 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_LastName_Error.getText(), "Limit Last Name length to 50 characters.",
					"Vcard_LastName_Error is matching");
			logger.info(vcf_Pages.Vcard_LastName_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_LastName_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_LastName_Error.getText(), "Limit Last Name length to 50 characters.",
					"Vcard_LastName_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_LastName_Error");// ScreenShot capture
		}

		textbox(vcf_Pages.Vcard_CompanyName).sendKeys(CompanyName);
		logger.info("enter the Vcard_CompanyName");

		vcf_Pages.Vcard_Company_Error.getText();
		if (vcf_Pages.Vcard_Company_Error.getText().equalsIgnoreCase("Limit Company Name length to 50 characters.")) {
			Assert.assertEquals(vcf_Pages.Vcard_Company_Error.getText(), "Limit Company Name length to 50 characters.",
					"Vcard_Company_Error is matching");
			logger.info(vcf_Pages.Vcard_Company_Error.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Company_Error");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_Company_Error.getText(),
					"Limit Company Name length to 50 characters.", "Vcard_Company_Error is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Company_Error");// ScreenShot capture
		}

		settingsclose();
	}

	public void BM_VCard_9_Attachments() throws Exception {

		AgentAutomation_Admin_Account();

		AgentAutomation_Dummy_Agent_TestDept();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "click on create_broadcast button").click();
		logger.info("click on create_broadcast Button");
		waitAndLog(2);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");
		waitAndLog(5);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);
		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.BM_pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on tools_crossBtn button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

	}

	public void BM_Click_Review_VCard_Disable() throws Exception {

		AgentAutomation_Admin_Account();
		AgentAutomation_Dummy_Agent_TestDept();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		implicitwaitAndLog(2);

		button(vcf_Pages.create_broadcast, "click on create_broadcast button").click();
		logger.info("click on create_broadcast Button");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_ReviewIcon, "click on tools_BM_ReviewIcon button").click();
		logger.info("click on tools_BM_ReviewIcon Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on tools_crossBtn button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

	}

	public void VCF_Conversation_Click_on_VCard_Switch_to_ContactsTab_Andback_to_Conversation() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.ContactsTab, "click on ContactsTab button").click();
		logger.info("click on ContactsTab Button");
		waitAndLog(2);

		button(vcf_Pages.ConversationTab, "click on ConversationTab button").click();
		logger.info("click on ConversationTab Button");
		waitAndLog(2);

		button(vcf_Pages.Automation_Contact, "click on Automation_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.Conversation_Msg_textbox_Department_VCF_File)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Conversation_Msg_textbox_Department_VCF_File));
			button(vcf_Pages.Conversation_Msg_textbox_Department_VCF_File, "buton").isEnabled();
			logger.info("Department VCard is available in Message Textbox ");
			TestUtil.takeScreenshotAtEndOfTest("Conversation_Msg_textbox_Department_VCF_File");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Conversation_Msg_textbox_Department_VCF_File));
			logger.info("Department VCard is not available in Message Textbox ");
			TestUtil.takeScreenshotAtEndOfTest("Conversation_Msg_textbox_Department_VCF_File");// ScreenShot capture
		}

	}

	public void tollFree_Dept_VCard_Available() throws Exception {

		auto1_TollFree();
		waitAndLog(2);
		selectTollFreeDept();

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);

		button(vcf_Pages.TollFree_Manage, "click on TollFree_Manage button").click();
		logger.info("click on TollFree_Manage Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.VCard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard));
			button(vcf_Pages.VCard, "buton").isEnabled();
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		settingsclose();
		TollFreeDept_to_LocalDept();

	}

	public void tollFree_Dept_VCard__Emoji_Msg() throws Exception {

		auto1_to_TollFree_Dept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		TollFreeDept_to_LocalDept();

	}

	public void tollFree_Dept_VCard_Single_Contact() throws Exception {

		auto1_to_TollFree_Dept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		TollFreeDept_to_LocalDept();

	}

	public void tollFree_Dept_VCard_9Attachments() throws Exception {

		auto1_to_TollFree_Dept();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.selectGif, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(5);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(3);

		TollFreeDept_to_LocalDept();

	}

	public void VCard_Save_Updated_Toggle_in_Same_Position() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();
		waitAndLog(2);
		button(vcf_Pages.Vcard_Save, "click on Vcard_Save button").click();
		logger.info("click on Vcard_Save Button");

		waitAndLog(5);

		settingsclose();

	}

	public void Vcard_BM_VCF_9Attachments_Manage_Unable_add_2ndVCF() throws Exception {

		auto1();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		implicitwaitAndLog(2);

		button(vcf_Pages.create_broadcast, "click on create_broadcast button").click();
		logger.info("click on create_broadcast Button");
		waitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Clear the Save As Draft");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(vcf_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Disable because already available");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "Click on tools_crossBtn").click();
		logger.info("click on tools_crossBtn");
		waitAndLog(2);
	}

	public void Vcard_SM_VCF_9Attachments_Manage_Unable_add_2ndVCF() throws Exception {

		auto1();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		implicitwaitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		waitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("input_msg_Sent_TO");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("SM_MSg_Textarea");
		logger.info(" Enter your Message");

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.Tools_SM_SaveDraft, "click on button").click();
		logger.info("Clear the Save As Draft");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(vcf_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Disable because already available");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.Tools_SM_Manage_Delete, "Click on Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Manage_Delete, "Click Tools_SM_Manage_Delete ").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "Click on tools_crossBtn").click();
		logger.info("click on tools_crossBtn");
		waitAndLog(2);
	}

	public void Tempalates_Shared_10Attachments_Error_For_11th_Attachment() throws Exception {

		auto1();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		String title = RandomStringUtils.randomAlphabetic(5);
		textbox(vcf_Pages.templates_title).sendKeys(title);
		textbox(vcf_Pages.templates_message).sendKeys(title);
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.GifIcon_Tools, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(3);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(3);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (vcf_Pages.error_for_10Attachments.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is matching");
			logger.info(vcf_Pages.error_for_10Attachments.getText());
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture
		}

		waitAndLog(2);
		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "Click on tools_crossBtn").click();
		logger.info("click on tools_crossBtn");
		waitAndLog(2);

	}

	public void Select_VCF_Switch_Other_Contact_then_to_PreviousContact() throws Exception {

		auto1();

		button(vcf_Pages.Automation_Contact, "click on Automation_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.QA10_Contact, "click on Automation_Contact button").click();
		logger.info("click on QA10_Contact Button");
		waitAndLog(2);

		button(vcf_Pages.Automation_Contact, "click on Automation_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.VCard_File_Name)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_File_Name));
			textbox(vcf_Pages.VCard_File_Name).isEnabled();
			logger.info("VCard_File_Name is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("VCard_File_Name");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_File_Name));
			logger.info("VCard_File_Name is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("VCard_File_Name");// ScreenShot capture
		}

	}

	public void Select_VCard_Click_ON_PinIcon_Deselect_PinIcon_VCardIcon_Enable() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.Automation_Contact, "click on Automation_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.PinIcon, "click on PinIcon button").click();
		logger.info("click on PinIcon Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon in Conversation is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon in Conversation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Settings_Roles_VCard_Details() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.setting_roles, "click on setting_roles button").click();
		logger.info("click on setting_roles Button");
		waitAndLog(2);
		logger.info(vcf_Pages.setting_roles_VCard_Details.getText());
		settingsclose();

	}

	public void Converastion_Select_VCard_Use_oneTemplate_VCard_Enabled() throws Exception {

		AgentAutomation_Admin_Account();

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		button(vcf_Pages.conversation_Templates, "Click on conversation_Templates").click();
		logger.info("Click on conversation_Templates");
		waitAndLog(2);

		button(vcf_Pages.conversation_Templates_Personal, "Click on conversation_Templates_Personal").click();
		logger.info("Click on conversation_Templates_Personal");
		waitAndLog(2);

		button(vcf_Pages.conversation_Templates_Select_Template, "Click on conversation_Templates_Select_Template")
				.click();
		logger.info("Click on conversation_Templates_Select_Template");

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon in Conversation is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon in Conversation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void VCard_toggle_Off_Save_toaster_toggleON_changeData_click_Outside() throws Exception {

		Vcard_Agent_Automations_Dummy_Account();
		waitAndLog(2);
		button(vcf_Pages.VCard_toggle, "Click on toggle").click();
		logger.info("Click on toggle");

		VCard_Automations_Fill_All_Details();

		button(vcf_Pages.Vcard_Save, "click on Vcard_Save button").click();
		logger.info("click on Vcard_Save Button");

		vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText();
		if (vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText()
				.equalsIgnoreCase("Vcard successfully updated.")) {
			Assert.assertEquals(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText(),
					"Vcard successfully updated.", "Vcard_Status_Successfully_Updated_Toaster is matching");
			logger.info(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Status_Successfully_Updated_Toaster");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.Vcard_Status_Successfully_Updated_Toaster.getText(),
					"Vcard successfully updated.", "Vcard_Status_Successfully_Updated_Toaster is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("Vcard_Status_Successfully_Updated_Toaster");// ScreenShot capture
		}
		waitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.test_Manage, "click on test_Manage button").click();
		logger.info("click on test_Manage Button");
		waitAndLog(2);
		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);
		button(vcf_Pages.VCard_toggle, "Click on toggle").click();
		logger.info("Click on toggle");

		VCard_Automations_Fill_All_Details();

		settingsclose();
	}

	public void VCF_BM_Dep1_Have_VCard_Dep2_Donthave_VCard_Switch_VCFEnable() throws Exception {

		VCard_Auto1_Automations_Contact();

		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "click on create_broadcast button").click();
		logger.info("click on create_broadcast Button");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");

		List<WebElement> allOptions = driver
				.findElements(By.xpath("//select[@class='form-control ng-pristine ng-valid ng-touched']"));
		logger.info(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Tollfree")) {

				allOptions.get(i).click();
				break;
			}

		}

		List<WebElement> allOptions1 = driver
				.findElements(By.xpath("//select[@class='form-control ng-pristine ng-valid ng-touched']"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Auto1")) {

				allOptions1.get(i).click();
				break;
			}

		}

		if (verifyElementIsEnabled(vcf_Pages.Vcard_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			button(vcf_Pages.Vcard_Icon, "buton").isEnabled();
			logger.info("VCard Icon in Conversation is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Vcard_Icon));
			logger.info("VCard Icon in Conversation is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(vcf_Pages.broadCast_msg, "button").click();
		logger.info("click on broadCast ");
		waitAndLog(2);
		button(vcf_Pages.tools_crossBtn, "click on tools_crossBtn button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

	}

}
