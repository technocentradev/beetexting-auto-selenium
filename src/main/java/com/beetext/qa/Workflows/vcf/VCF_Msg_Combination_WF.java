package com.beetext.qa.Workflows.vcf;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;

public class VCF_Msg_Combination_WF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void VCF_Msg() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		VCard_Validation();

	}

	public void VCF_Gif() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		gif_Validation();

		VCard_Validation();

	}

	public void VCF_Msg_Gif() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		gif_Validation();

		VCard_Validation();

	}

	public void VCF_Msg_Emoji() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		VCard_Validation();

	}

	public void VCF_Emoji() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Validation();

	}

	public void VCF_Msg_Image() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		Image_Validation();

		VCard_Validation();

	}

	public void VCF_Image() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		Image_Validation();

		VCard_Validation();

	}

	public void VCF_Msg_Video() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		Video_Validation();

		VCard_Validation();

	}

	public void VCF_Msg_audio() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		audio_File_Validation();

		VCard_Validation();

	}

	public void VCF_Video() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		Video_Validation();

		VCard_Validation();

	}

	public void VCF_audio() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		audio_File_Validation();

		VCard_Validation();

	}

	public void VCF_Msg_pdf() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		if (verifyElementIsEnabled(vcf_Pages.pdf_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.pdf_File_Verification));
			textbox(vcf_Pages.pdf_File_Verification).isEnabled();
			logger.info("pdf_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("pdf_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.pdf_File_Verification));
			logger.info("pdf_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("pdf_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_pdf() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		if (verifyElementIsEnabled(vcf_Pages.pdf_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.pdf_File_Verification));
			textbox(vcf_Pages.pdf_File_Verification).isEnabled();
			logger.info("pdf_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("pdf_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.pdf_File_Verification));
			logger.info("pdf_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("pdf_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_Msg_Excel() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		if (verifyElementIsEnabled(vcf_Pages.Excel_xls_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Excel_xls_File_Verification));
			textbox(vcf_Pages.Excel_xls_File_Verification).isEnabled();
			logger.info("Excel_xls_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Excel_xls_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Excel_xls_File_Verification));
			logger.info("Excel_xls_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Excel_xls_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_Excel() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		if (verifyElementIsEnabled(vcf_Pages.Excel_xls_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.Excel_xls_File_Verification));
			textbox(vcf_Pages.Excel_xls_File_Verification).isEnabled();
			logger.info("Excel_xls_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Excel_xls_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.Excel_xls_File_Verification));
			logger.info("Excel_xls_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("Excel_xls_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_Msg_WordDoc() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		if (verifyElementIsEnabled(vcf_Pages.wordDoc_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.wordDoc_File_Verification));
			textbox(vcf_Pages.wordDoc_File_Verification).isEnabled();
			logger.info("wordDoc_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("wordDoc_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.wordDoc_File_Verification));
			logger.info("wordDoc_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("wordDoc_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_WordDoc() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		if (verifyElementIsEnabled(vcf_Pages.wordDoc_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.wordDoc_File_Verification));
			textbox(vcf_Pages.wordDoc_File_Verification).isEnabled();
			logger.info("wordDoc_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("wordDoc_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.wordDoc_File_Verification));
			logger.info("wordDoc_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("wordDoc_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_Msg_TxtDoc() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		if (verifyElementIsEnabled(vcf_Pages.txt_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.txt_File_Verification));
			textbox(vcf_Pages.txt_File_Verification).isEnabled();
			logger.info("txt_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("txt_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.txt_File_Verification));
			logger.info("txt_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("txt_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_TxtDoc() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		if (verifyElementIsEnabled(vcf_Pages.txt_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.txt_File_Verification));
			textbox(vcf_Pages.txt_File_Verification).isEnabled();
			logger.info("txt_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("txt_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.txt_File_Verification));
			logger.info("txt_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("txt_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_Msg_MOV() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		if (verifyElementIsEnabled(vcf_Pages.mov_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.mov_File_Verification));
			textbox(vcf_Pages.mov_File_Verification).isEnabled();
			logger.info("mov_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("mov_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.mov_File_Verification));
			logger.info("mov_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("mov_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_MOV() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		if (verifyElementIsEnabled(vcf_Pages.mov_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.mov_File_Verification));
			textbox(vcf_Pages.mov_File_Verification).isEnabled();
			logger.info("mov_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("mov_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.mov_File_Verification));
			logger.info("mov_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("mov_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_Msg_WEBM() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		if (verifyElementIsEnabled(vcf_Pages.webm_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.webm_File_Verification));
			textbox(vcf_Pages.webm_File_Verification).isEnabled();
			logger.info("webm_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("webm_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.webm_File_Verification));
			logger.info("webm_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("webm_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_WEBM() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		if (verifyElementIsEnabled(vcf_Pages.webm_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.webm_File_Verification));
			textbox(vcf_Pages.webm_File_Verification).isEnabled();
			logger.info("webm_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("webm_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.webm_File_Verification));
			logger.info("webm_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("webm_File_Verification");// ScreenShot capture
		}

		VCard_Validation();

	}

	public void VCF_Msg_GIF_Emoji_MP3_MOV_PDF_Image_Video() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		VCard_Validation();

		gif_Validation();

		Image_Validation();

		Video_Validation();

		audio_File_Validation();

		if (verifyElementIsEnabled(vcf_Pages.pdf_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.pdf_File_Verification));
			textbox(vcf_Pages.pdf_File_Verification).isEnabled();
			logger.info("pdf_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("pdf_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.pdf_File_Verification));
			logger.info("pdf_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("pdf_File_Verification");// ScreenShot capture
		}

		if (verifyElementIsEnabled(vcf_Pages.mov_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.mov_File_Verification));
			textbox(vcf_Pages.mov_File_Verification).isEnabled();
			logger.info("mov_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("mov_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.mov_File_Verification));
			logger.info("mov_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("mov_File_Verification");// ScreenShot capture
		}

	}

	public void VCF_Msg_Mp3() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		VCard_Message_Validation();

		audio_File_Validation();

		VCard_Validation();

	}

	public void VCF_Mp3() throws Exception {

		VCard_Auto1_Automations_Contact();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

		VCard_Validation();

		VCard_Automations_Auto1_Contact();

		audio_File_Validation();

		VCard_Validation();

	}

}
