package com.beetext.qa.Workflows.vcf;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;

public class vcf_TemplatesWF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void VCard_Available_in_SharedTemplates() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Available");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Available");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void In_SharedTemplates_Bydefault_Vcard_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void In_SharedTemplates_Select_Dept_Vcard_Enable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void In_SharedTemplates_Select_Dept_and_Vcard_Select_AnotherDept_Vcard_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void In_SharedTemplates_Select_TwoDepts_Vcard_Will_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void In_SharedTemplates_Select_AllDepts_Vcard_Will_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(vcf_Pages.selectAll_Dept_in_Templates, "Click on selectAll_Dept_in_Templates").click();
		logger.info("click on selectAll_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void In_SharedTemplates_Select_no_VCF_Dept_Vcard_Will_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void In_SharedTemplates_Select_Dept_and_Vcard_DeSelect_Dept_Vcard_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			logger.info("VCard is Deleted");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Templates_Select_NoVcfDept_selectVcfDept_Deselect_NoVcfDept_VCardEnable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		implicitwaitAndLog(2);

		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Templates_Select_VcfDept_selectNoVcfDept_Deselect_VcfDept_VCardDisable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		implicitwaitAndLog(2);

		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Templates_9Attachments_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		String title = RandomStringUtils.randomAlphabetic(5);
		textbox(vcf_Pages.templates_title).sendKeys(title);

		textbox(vcf_Pages.templates_message).sendKeys(title);

		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.GifIconTemplates, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(3);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);

		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");

	}

	public void Templates_Attachment_Emoji_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		String title = RandomStringUtils.randomAlphabetic(5);
		textbox(vcf_Pages.templates_title).sendKeys(title);
		textbox(vcf_Pages.templates_message).sendKeys(title);
		implicitwaitAndLog(2);

		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		waitAndLog(2);
		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");

	}

	public void Templates_Emoji_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		textbox(vcf_Pages.templates_title).sendKeys("Template VCF");
		implicitwaitAndLog(2);
		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);
		button(vcf_Pages.create_Template, "Click on Create Template").click();
		waitAndLog(2);

		button(vcf_Pages.sharedtemplates, "click on sharedtemplates button").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.manageTemplate, "click on manageTemplate button").click();
		logger.info("Click manageTemplate");

		button(vcf_Pages.DeleteTemplate, "click on DeleteTemplate button").click();
		logger.info("click on DeleteTemplate");
		waitAndLog(2);
		button(vcf_Pages.DeleteTemplate, "click on DeleteTemplate button").click();
		logger.info("click on DeleteTemplate");
	}

	public void Templates_Message_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		textbox(vcf_Pages.templates_title).sendKeys("Template VCF");
		textbox(vcf_Pages.templates_message).sendKeys("Hello Techies");
		implicitwaitAndLog(2);

		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.create_Template, "Click on Create Template").click();
		waitAndLog(2);

		button(vcf_Pages.sharedtemplates, "click on sharedtemplates button").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.manageTemplate, "click on manageTemplate button").click();
		logger.info("Click manageTemplate");
		waitAndLog(2);
		button(vcf_Pages.DeleteTemplate, "click on DeleteTemplate button").click();
		logger.info("click on DeleteTemplate");
		waitAndLog(2);
		button(vcf_Pages.DeleteTemplate, "click on DeleteTemplate button").click();
		logger.info("click on DeleteTemplate");
	}

	public void Templates_Message_Emoji_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		textbox(vcf_Pages.templates_title).sendKeys("Template VCF");
		textbox(vcf_Pages.templates_message).sendKeys("Hello Techies");
		implicitwaitAndLog(2);

		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);
		button(vcf_Pages.create_Template, "Click on Create Template").click();
		waitAndLog(2);

		button(vcf_Pages.sharedtemplates, "click on sharedtemplates button").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.manageTemplate, "click on manageTemplate button").click();
		logger.info("Click manageTemplate");
		waitAndLog(2);
		button(vcf_Pages.DeleteTemplate, "click on DeleteTemplate button").click();
		logger.info("click on DeleteTemplate");
		waitAndLog(2);
		button(vcf_Pages.DeleteTemplate, "click on DeleteTemplate button").click();
		logger.info("click on DeleteTemplate");
	}

	public void Templates_Select_Vcard_Click_On_Personal_Vcard_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");

		implicitwaitAndLog(2);

		button(vcf_Pages.auto1_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.templates_Personal, "click on templates_Personal button").click();
		logger.info("click on templates_Personal Button");
		logger.info("VCard not available for Personal Templates");

	}

	public void Templates_manage_Already_Vcf_Available_Vacrd_Willbe_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.sharedtemplates, "Click on sharedtemplates").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.Template_For_ManageCase, "Click on Template_For_ManageCase").click();
		logger.info("click on Template_For_ManageCase");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Templates_manage_Already_Vcf_Available_Select_NoVCFDept_Vacrd_Willbe_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.sharedtemplates, "Click on sharedtemplates").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.Template_For_ManageCase, "Click on Template_For_ManageCase").click();
		logger.info("click on Template_For_ManageCase");
		implicitwaitAndLog(2);
		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto1_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Templates_manage_Already_Vcf_Available_Select_AllDept_Vacrd_Willbe_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.sharedtemplates, "Click on sharedtemplates").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.Template_For_ManageCase, "Click on Template_For_ManageCase").click();
		logger.info("click on Template_For_ManageCase");
		implicitwaitAndLog(2);
		button(vcf_Pages.selectAll_Dept_in_Templates, "Click on selectAll_Dept_in_Templates").click();
		logger.info("click on selectAll_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void ManageTemplates_Select_NoVcfDept_selectVcfDept_Deselect_NoVcfDept_VCardEnable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.sharedtemplates, "Click on sharedtemplates").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.Template_For_ManageCase, "Click on Template_For_ManageCase").click();
		logger.info("click on Template_For_ManageCase");
		implicitwaitAndLog(2);
		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void ManageTemplates_Select_VcfDept_selectNoVcfDept_Deselect_VcfDept_VCardDisable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);
		button(vcf_Pages.sharedtemplates, "Click on sharedtemplates").click();
		logger.info("click on sharedtemplates");
		waitAndLog(2);
		button(vcf_Pages.Template_For_ManageCase, "Click on Template_For_ManageCase").click();
		logger.info("click on Template_For_ManageCase");
		implicitwaitAndLog(2);

		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.auto2_Dept_in_Templates, "Click on auto1_Dept_in_Templates").click();
		logger.info("click on auto2_Dept_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

}
