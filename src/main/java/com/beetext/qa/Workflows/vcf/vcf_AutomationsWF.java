package com.beetext.qa.Workflows.vcf;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;

public class vcf_AutomationsWF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	String randomtitle = RandomStringUtils.randomAlphabetic(5);
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void Vcf_Available_in_Automations() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Available");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Available");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void VCF_Automations_Admin_Able_to_Enable_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on Tools_SM button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(vcf_Pages.user_id).sendKeys("automationblock@yopmail.com");
		logger.info("Email id :" + "automationblock@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.oneAgent_Sample1_Manage, "click on oneAgent_Sample1_Manage button").click();
		logger.info("click on oneAgent_Sample1_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");

		settingsclose();

		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on Tools_SM button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.oneAgent_Sample1_Manage, "click on oneAgent_Sample1_Manage button").click();
		logger.info("click on oneAgent_Sample1_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");

		settingsclose();
		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);
		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);

	}

	public void VcfAutomations_Message_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		textbox(vcf_Pages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_Message_Emoji_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		textbox(vcf_Pages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");

		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_Message_Attachment_Emoji_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		textbox(vcf_Pages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");

		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_1000Message_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

//		String message = "The accuracy of the information provided to Beetexting, both during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are as follows:The accu";
		String message = RandomStringUtils.randomAlphabetic(1000);
		textbox(vcf_Pages.Reply_message).sendKeys(message);
		logger.info("enter Reply_message");

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_1000Message_Emoji_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

//		String message = "The accuracy of the information provided to Beetexting, both during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are as follows:The accu";
		String message = RandomStringUtils.randomAlphabetic(1000);
		textbox(vcf_Pages.Reply_message).sendKeys(message);
		logger.info("enter Reply_message");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_9Attachments_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_10Attachments_VCF_Shows_Error() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello");
		logger.info("enter condition_Keyword");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.GifIconTemplates, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(3);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.GifIconTemplates, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(3);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (vcf_Pages.error_for_10Attachments.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is matching");
			logger.info(vcf_Pages.error_for_10Attachments.getText());
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture
		}

	}

	public void Select_VCF_then_VCF_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Select_Attachment_VCF_then_VCF_Disable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		waitAndLog(2);
		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void VCF_Can_Addin_Multiple_Actions() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		implicitwaitAndLog(2);

		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");

	}

	public void VcfAutomations_9Attachments_and2ndAction_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(10);

		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		waitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		waitAndLog(2);
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waitAndLog(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");
	}

	public void VcfAutomations_9Attachments_and2ndAction_9Attachments_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waitAndLog(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

	}

	public void VcfAutomations_Message_9Attachments_and2ndAction_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		implicitwaitAndLog(2);
		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_Message_9Attachments_and2ndAction_Message_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		implicitwaitAndLog(2);
		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_Emoji_Message_9Attachments_and2ndAction_Message_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		implicitwaitAndLog(2);
		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_1000CharMessage_9Attachments_and2ndAction_1000CharMessage_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
//		String message = "The accuracy of the information provided to Beetexting, both during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are as follows:The accu";
		String message = RandomStringUtils.randomAlphabetic(1000);
		textbox(vcf_Pages.Reply_message).sendKeys(message);
		logger.info("enter Reply_message");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		textbox(vcf_Pages.Reply_message).sendKeys(message);
		logger.info("enter Reply_message");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		implicitwaitAndLog(2);
		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void VcfAutomations_1000CharMessage_Emoji_9Attachments_and2ndAction_1000CharMessage_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
//		String message = "The accuracy of the information provided to Beetexting, both during registration and communicated to Beetexting via any channel, must be valid and accurate. Failure to provide accurate information or providing false information can lead to non-compliance fees assessed by mobile network carriers. Any non-compliance fees assessed by carriers to a brand or number are the responsibility of said brand or the individual that prompted number-creation within Beetexting.All information submitted into the Beetexting must be accurate. Fraudulent or inaccurate information can lead to the termination of your account without notice. Following your Beetexting registration, you will be contacted by a Beetexting representative to confirm your SMS/MMS use cases. Failure to provide accurate details regarding your usage of SMS and MMS messages can lead to message failing to deliver and/or non-compliance fees; neither of which are the responsibility of Beetexting.Non-Compliance Fees are as follows:The accu";
		String message = RandomStringUtils.randomAlphabetic(1000);
		textbox(vcf_Pages.Reply_message).sendKeys(message);
		logger.info("enter Reply_message");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		textbox(vcf_Pages.Reply_message).sendKeys(message);
		logger.info("enter Reply_message");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		implicitwaitAndLog(2);
		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");
		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}

	public void In_2nd_Action_Select_VCF_then_Disabled() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard Button is Disable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard Button is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Automaations_Manage_removeVCF_AddVCF_AddAction_9Attachments_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);
		button(vcf_Pages.manage, "Click on manage").click();
		logger.info("click on manage");
		waitAndLog(2);
		button(vcf_Pages.VCF_Close, "Click on VCF_Close").click();
		logger.info("click on VCF_Close");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		implicitwaitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		waitAndLog(2);
		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.VCF_PinAttachent, "file attachment").click();
		waitAndLog(5);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
	}

	public void Automations_Manage_removeVCF_addVCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);
		button(vcf_Pages.manage, "Click on manage").click();
		logger.info("click on manage");
		waitAndLog(2);
		button(vcf_Pages.VCF_Close, "Click on VCF_Close").click();
		logger.info("click on VCF_Close");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
	}

	public void Automations_Manage_removeVCF_andAttachment_addVCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);
		button(vcf_Pages.manage, "Click on manage").click();
		logger.info("click on manage");
		waitAndLog(2);
		button(vcf_Pages.VCF_Close, "Click on VCF_Close").click();
		logger.info("click on VCF_Close");
		waitAndLog(2);
		button(vcf_Pages.VCF_Attachent_Close, "Click on VCF_Attachent_Close").click();
		logger.info("click on VCF_Attachent_Close");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
	}

	public void Automations_Manage_removeVCF_Attachmnet_andMsg_addVCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);
		button(vcf_Pages.manage, "Click on manage").click();
		logger.info("click on manage");
		waitAndLog(2);
		button(vcf_Pages.VCF_Close, "Click on VCF_Close").click();
		logger.info("click on VCF_Close");
		waitAndLog(2);
		button(vcf_Pages.VCF_Attachent_Close, "Click on VCF_Attachent_Close").click();
		logger.info("click on VCF_Attachent_Close");
		textbox(vcf_Pages.Reply_message).clear();
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

	}

	public void Automations_Manage_removeVCF_Attachmnet_andMsg_addVCF_WithAttachment() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);
		button(vcf_Pages.manage, "Click on manage").click();
		logger.info("click on manage");
		waitAndLog(2);
		button(vcf_Pages.VCF_Close, "Click on VCF_Close").click();
		logger.info("click on VCF_Close");
		waitAndLog(2);
		button(vcf_Pages.VCF_Attachent_Close, "Click on VCF_Attachent_Close").click();
		logger.info("click on VCF_Attachent_Close");
		textbox(vcf_Pages.Reply_message).clear();
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.add_Action, "add_Action").click();
		logger.info("click on add_Action");
		waitAndLog(2);
		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);
		button(vcf_Pages.VCard_Button2, "VCard_Button2").click();
		logger.info("click on VCard_Button2");
		implicitwaitAndLog(2);
	}

	public void Automations_Manage_Message_Emoji_and_VCF_deleteAutomations() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);

		button(vcf_Pages.create_Automations, "Click on create_Automations").click();
		logger.info("click on create_Automations");

		textbox(vcf_Pages.title).sendKeys(randomtitle);
		logger.info("enter title name");

		textbox(vcf_Pages.condition_Keyword).sendKeys("Hello ");
		logger.info("enter condition_Keyword");

		textbox(vcf_Pages.Reply_message).sendKeys("Hello - Technocentra");
		logger.info("enter Reply_message");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(3);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");

		button(vcf_Pages.automation_ON_OFF, "automation_ON_OFF").click();
		logger.info("click on automation_ON_OFF");
		waits(2);
		button(vcf_Pages.Create, "Create").click();
		logger.info("click on Create");

		VCF_Automations_Delete();
	}
}
