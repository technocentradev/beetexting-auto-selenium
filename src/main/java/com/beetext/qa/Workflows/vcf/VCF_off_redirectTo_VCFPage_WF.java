package com.beetext.qa.Workflows.vcf;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;

public class VCF_off_redirectTo_VCFPage_WF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void VCF_off_redircts_to_VCFPage_From_BM_Page() throws Exception {

		waitAndLog(2);
		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);
		button(vcf_Pages.broadCast_msg, "click on broadCast_msg button").click();
		logger.info("click on broadCast_msg Button");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "click on create_broadcast button").click();
		logger.info("click on create_broadcast Button");
		waitAndLog(2);

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.VCard_toggle)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			button(vcf_Pages.VCard_toggle, "buton").isEnabled();
			logger.info("VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		settingsclose();

	}

	public void VCF_off_redircts_to_VCFPage_From_SM_Page() throws Exception {

		waitAndLog(2);
		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		if (verifyElementIsEnabled(vcf_Pages.VCard_toggle)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			button(vcf_Pages.VCard_toggle, "buton").isEnabled();
			logger.info("VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		settingsclose();

	}

	public void VCF_off_redircts_to_VCFPage_From_Templates_Page() throws Exception {

		waitAndLog(2);
		button(vcf_Pages.tools_bttn, "click on tools_bttn button").click();
		logger.info("click on tools_bttn Button");
		waitAndLog(2);
		waitAndLog(2);

		button(vcf_Pages.templates, "Click on templates").click();
		logger.info("click on templates");
		waitAndLog(2);

		button(vcf_Pages.create_templates, "Click on create_templates").click();
		logger.info("click on templates");

		waitAndLog(2);
		button(vcf_Pages.templates_shared, "Click on templates_shared").click();
		logger.info("click on templates_shared");
		waitAndLog(2);

		button(vcf_Pages.selectAll_Dept_in_Templates, "Click on nominee123_Dept_in_Templates").click();
		logger.info("click on nominee123_Dept_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		if (verifyElementIsEnabled(vcf_Pages.VCard_toggle)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			button(vcf_Pages.VCard_toggle, "buton").isEnabled();
			logger.info("VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		settingsclose();

	}

	public void VCF_off_redircts_to_VCFPage_From_Conv_Page() throws Exception {

		waitAndLog(2);
		vcard_Icon_in_Conversation();
		waitAndLog(2);
		if (verifyElementIsEnabled(vcf_Pages.VCard_toggle)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			button(vcf_Pages.VCard_toggle, "buton").isEnabled();
			logger.info("VCard toggle is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCard_toggle));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		settingsclose();

	}
}
