package com.beetext.qa.Workflows.vcf;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;

public class vcf_SM_WF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void VCF_SM_1000_TextMsg_Vcf() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys(RandomStringUtils.randomAlphabetic(1000));
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_9Attachments_Vcf() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);
		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_Attachment_Vcf_Non_Recurring() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);
		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_Attachment_Vcf_Recurring() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Recurring_Button, "Tools_SM_Recurring_Button").click();
		logger.info("click on Tools_SM_Recurring_Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_Attachment_Vcf_textMsg_Emoji_Non_Recurring() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);
		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_Attachment_Vcf_textMsg_Emoji() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);
		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_Emoji_Vcf() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);
		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_Vcf_textMsg_Emoji() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);
		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_admin_enable_Disable_VCF_also_For_Mange_Dept_File_Enabled() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on Tools_SM button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(vcf_Pages.user_id).sendKeys("automationblock@yopmail.com");
		logger.info("Email id :" + "automationblock@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.oneAgent_Sample1_Manage, "click on oneAgent_Sample1_Manage button").click();
		logger.info("click on oneAgent_Sample1_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");

		settingsclose();

		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		waitAndLog(2);
		button(vcf_Pages.Tools_SM, "Click on Tools_SM").click();
		logger.info("click on Tools_SM");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Upcoming, "Tools_SM_Upcoming").click();
		logger.info("click on Tools_SM_Upcoming");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage, "Tools_SM_Manage").click();
		logger.info("click on Tools_SM_Manage");

		if (verifyElementIsEnabled(vcf_Pages.managePage_Department_VCF_File)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.managePage_Department_VCF_File));
			button(vcf_Pages.managePage_Department_VCF_File, "buton").isEnabled();
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.managePage_Department_VCF_File));
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on Tools_SM button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.oneAgent_Sample1_Manage, "click on oneAgent_Sample1_Manage button").click();
		logger.info("click on oneAgent_Sample1_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");

		settingsclose();

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);
		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);

	}

	public void VCF_SM_After_Selecting_Vcard_it_Should_Be_in_Disabled() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void VCF_SM_Attachment_Vcf_TextMsg_Emoji() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);
		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_10Attachments_Vcf_Shows_Error() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (vcf_Pages.error_for_10Attachments.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is matching");
			logger.info(vcf_Pages.error_for_10Attachments.getText());
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture
		}

	}

	public void VCF_SM_VCF_For_group_Conversation() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_VCF_For_TollFree() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("8884121895");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_VCF_For_one_to_one_Conversation() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_addVCF_manage_SM_Remove_add_Again_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "Click on Tools_SM").click();
		logger.info("click on Tools_SM");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Upcoming, "Tools_SM_Upcoming").click();
		logger.info("click on Tools_SM_Upcoming");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage, "Tools_SM_Manage").click();
		logger.info("click on Tools_SM_Manage");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_crossMark_of_Contact, "Tools_SM_crossMark_of_Contact").click();
		logger.info("click on Tools_SM_crossMark_of_Contact");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);

	}

	public void VCF_SM_addVCF_manage_SM_Remove_add_Again_VCF_Emoji_text() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "Click on Tools_SM").click();
		logger.info("click on Tools_SM");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Upcoming, "Tools_SM_Upcoming").click();
		logger.info("click on Tools_SM_Upcoming");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage, "Tools_SM_Manage").click();
		logger.info("click on Tools_SM_Manage");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_crossMark_of_Contact, "Tools_SM_crossMark_of_Contact").click();
		logger.info("click on Tools_SM_crossMark_of_Contact");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);
		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);

	}

	public void Creat_SM_with_1Attachment_and_VCF() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.PinAttachent, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");

		VCF_Tools_SM_DeleteSM();

	}

	public void VCF_SM_addVCF_manage_SM_try_to_AddVCF_it_isIn_Disabled() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.input_msg_Sent_TO).sendKeys("QA");
		logger.info("Enter data input_msg_Sent_TO search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(vcf_Pages.SM_MSg_Textarea).sendKeys("Techenocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waitAndLog(2);

		button(vcf_Pages.Uparrow_button_Time_mint, "Click on Uparrow button for Minute Time").click();
		logger.info("Clicked on Uparrow of Minute time");
		waitAndLog(2);
		button(vcf_Pages.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waitAndLog(2);

		button(vcf_Pages.Tools_Sm_Create_SM, "button").click();
		logger.info("click on Tools_Sm_Create_SM button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "Click on Tools_SM").click();
		logger.info("click on Tools_SM");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Upcoming, "Tools_SM_Upcoming").click();
		logger.info("click on Tools_SM_Upcoming");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage, "Tools_SM_Manage").click();
		logger.info("click on Tools_SM_Manage");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);

	}

	public void VCF_SM_addVCF_then_it_Should_Be_in_Disabled() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void VCF_SM_addVCF_RemoveVCF_then_it_Should_Be_in_Enable() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Tools_SM, "click on Tools_SM button").click();
		logger.info("click on Tools_SM Button");
		waitAndLog(2);

		button(vcf_Pages.Tools_SM_Create, "click on Tools_SM_Create button").click();
		logger.info("click on Tools_SM_Create Button");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.VCF_Attachent_Close, "Tools_SM_crossMark_of_Contact").click();
		logger.info("click on VCF_Attachent_Close");
		waitAndLog(2);
		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Enable");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Diabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

}
