package com.beetext.qa.Workflows.vcf;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;

public class VCF_Conv_SM_Msg_Combinaion_WF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void Conv_SM_Msg_VCF() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_1Min_Time();

		vcard_Icon_in_Conversation();

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Conv_SM_VCF_Gif() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_1Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Conv_SM_VCF_Msg_Gif() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_1Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);
	}

	public void Conv_SM_VCF_Msg_Emoji() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_1Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Emoji() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_1Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);
	}

	public void Con_SM_VCF_Msg_Image() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Image() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_Video() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_audio() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Video() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_audio() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_pdf() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_pdf() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_Excel() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Excel() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_WordDoc() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_WordDoc() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_TxtDoc() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_TxtDoc() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_MOV() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_MOV() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_WEBM() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);
	}

	public void Con_SM_VCF_WEBM() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_GIF_Emoji_MP3_MOV_PDF_Image_Video() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();

		waitAndLog(2);

		button(vcf_Pages.Conv_SM, "Conv_SM ").click();
		logger.info("click on Conv_SM Button");
		waitAndLog(2);

		button(vcf_Pages.increase_1Min_Time_in_Conv_SM, "Click on increase_1Min_Time_in_Conv_SM").click();
		logger.info("click on increase_1Min_Time_in_Conv_SM");
		button(vcf_Pages.increase_1Min_Time_in_Conv_SM, "Click on increase_1Min_Time_in_Conv_SM").click();
		logger.info("click on increase_1Min_Time_in_Conv_SM");
		button(vcf_Pages.increase_1Min_Time_in_Conv_SM, "Click on increase_1Min_Time_in_Conv_SM").click();
		logger.info("click on increase_1Min_Time_in_Conv_SM");
		waitAndLog(2);

		button(vcf_Pages.conv_SM_Time_Set_button, "conv_SM_Time_Set_button ").click();
		logger.info("click on conv_SM_Time_Set_button Button");
		waitAndLog(2);

		vcard_Icon_in_Conversation();

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		waitAndLog(2);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(5);

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Msg_Mp3() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		textbox(vcf_Pages.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waitAndLog(2);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}

	public void Con_SM_VCF_Mp3() throws Exception {

		waitAndLog(2);
		Click_on_QA10_Contact();
		Click_on_Conv_SM_Increase_2Min_Time();

		vcard_Icon_in_Conversation();

		button(vcf_Pages.pin_attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		waitAndLog(8);

		button(vcf_Pages.chatInputsendButton, "click on chatInputsendButton button").click();
		logger.info("click on chatInputsendButton Button");
		waitAndLog(2);

	}
}
