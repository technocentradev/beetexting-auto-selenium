package com.beetext.qa.Workflows.vcf;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;

public class vcf_BroadcastWF extends VCF_WFCM {
	String url, username1, username2, password;
	TestBase testBase = new TestBase();
	VCF vcf_Pages = new VCF();
	public static String currentDir = System.getProperty("user.dir");

	public void VCF_9Attachments() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_10Attachments_Error() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.png");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");

		if (vcf_Pages.error_for_10Attachments.getText().equalsIgnoreCase("Maximum 10 attachments are allowed.")) {
			Assert.assertEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is matching");
			logger.info(vcf_Pages.error_for_10Attachments.getText());
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.error_for_10Attachments.getText(), "Maximum 10 attachments are allowed.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("error_for_10Attachments");// ScreenShot capture
		}

	}

	public void VCF_BM_After_Selecting_Vcard_it_Should_Be_in_Disabled() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.vcard_in_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			button(vcf_Pages.vcard_in_Templates, "buton").isEnabled();
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.vcard_in_Templates));
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void VCF_BM_1000_TextMsg_Vcf() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys(RandomStringUtils.randomAlphabetic(1000));
		logger.info(" Enter your messeage");

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_TextMsg_Vcf_Emoji() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_TextMsg_Vcf_Emoji_Attachment() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_TextMsg_Vcf_Attachment_Recurring() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_TextMsg_Vcf_Attachment_non_Recurring() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_TextMsg_Vcf_Emoji_Attachment_Non_Recurring() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_TextMsg_Vcf_Emoji_Attachment_Recurring() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);
		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.emojibttn, "click on Emoji button").click();
		logger.info("click on Emoji Button");
		waitAndLog(2);

		button(vcf_Pages.emoji_like, "click on Emoji button").click();
		logger.info("Select Like Emoji");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_Past_Reuse_add_Vcf() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		implicitwaitAndLog(2);

		button(vcf_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		implicitwaitAndLog(2);
		textbox(vcf_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_Past_Reuse_text_add_Vcf() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		implicitwaitAndLog(2);

		button(vcf_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		implicitwaitAndLog(2);
		textbox(vcf_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		textbox(vcf_Pages.tools_BM_message).clear();
		logger.info(" Clear message");
		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your message");
		implicitwaitAndLog(2);
		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_Create_BM_with_VCF_Delete_BM() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.BM_Pin_Attachment, "file attachment").click();
		waitAndLog(2);
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waitAndLog(5);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_time_upArrow, "button").click();
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void VCF_BM_Create_BM_click_Outside() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_PopUp_Discard, "tools_BM_create_PopUp_Discard").click();
		logger.info("click on tools_BM_create_PopUp_Discard button");
		waitAndLog(2);

		link(vcf_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
	}

	public void VCF_BM_Save_BM_as_Draft() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on tools_BM_create_Save_Draft");

	}

	public void VCF_BM_admin_enable_Disable_VCF_also_For_Mange_Dept_File_Enabled() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on Tools_SM button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(vcf_Pages.user_id).sendKeys("automationblock@yopmail.com");
		logger.info("Email id :" + "automationblock@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.oneAgent_Sample1_Manage, "click on oneAgent_Sample1_Manage button").click();
		logger.info("click on oneAgent_Sample1_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");

		settingsclose();

		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "Click on broadCast_msg").click();
		logger.info("click on broadCast_msg");
		waitAndLog(2);

		button(vcf_Pages.create_broadcast, "Click on create_broadcast").click();
		logger.info("click on create_broadcast");
		waitAndLog(2);

		implicitwaitAndLog(2);

		textbox(vcf_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(vcf_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(vcf_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(vcf_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(vcf_Pages.vcard_in_Templates, "vcard_in_Templates").click();
		logger.info("click on vcard_in_Templates");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(vcf_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");
		waitAndLog(2);

		button(vcf_Pages.broadCast_msg, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(vcf_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(vcf_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		waitAndLog(2);

		if (verifyElementIsEnabled(vcf_Pages.managePage_Department_VCF_File)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.managePage_Department_VCF_File));
			button(vcf_Pages.managePage_Department_VCF_File, "buton").isEnabled();
			logger.info("VCard is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.managePage_Department_VCF_File));
			logger.info("VCard is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(vcf_Pages.tools_BM_create_upcoming_manage_delete, "button").click();
		logger.info("click on upcoming manage delete Bm ");
		waitAndLog(2);

		button(vcf_Pages.tools_BM_create_upcoming_manage_delete, "button").click();
		logger.info("click on upcoming manage delete Bm ");
		waitAndLog(2);

		button(vcf_Pages.tools_crossBtn, "click on Tools_SM button").click();
		logger.info("click on tools_crossBtn Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.oneAgent_Sample1_Manage, "click on oneAgent_Sample1_Manage button").click();
		logger.info("click on oneAgent_Sample1_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

		button(vcf_Pages.VCard_toggle, "click on VCard_toggle button").click();
		logger.info("click on VCard_toggle Button");

		settingsclose();

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);
		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);

	}
}
