package com.beetext.qa.Workflows;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.ProfileWFCM;
import com.beetext.qa.pages.Profileupdate;
import com.beetext.qa.util.TestUtil;

public class ProfileWF extends ProfileWFCM {

	Profileupdate profile=new Profileupdate();
	public void successfully_update_agent_profile_details() {
		waits(3);
		clickonthreedotslink();
		profileLink();
		profilepage();
		profile_name();
		signature();
		toggle_Buttons();
		save_Button();
		if (profile.toaster_message.getText().equalsIgnoreCase("'Profile updated successfully.")) {
			Assert.assertEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile Updated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile updation  is failed");// ScreenShot capture
		}

	}
	
	public void update_agent_profile_name() {
		profilepage();
		profile_name();
		save_Button();
		
		if (profile.toaster_message.getText().equalsIgnoreCase("'Profile updated successfully.")) {
			Assert.assertEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile updated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile updation is failed");// ScreenShot capture
		}

	}
	
	public void update_agent_profile_name_3char_required_validation() {
		profilepage();
		profile.profile_name.click();
		profile.profile_name.clear();
		profile.profile_name.sendKeys( "a");
		
		if (profile.profile_name_3_char_required.getText().equalsIgnoreCase("Name must be at least 3 characters.")) {
			Assert.assertEquals(profile.profile_name_3_char_required.getText(), "Name must be at least 3 characters.",
					"Name must be at least 3 characters.");
			TestUtil.takeScreenshotAtEndOfTest("Agent Name required validation Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(profile.profile_name_3_char_required.getText(), "Name must be at least 3 characters.",
					"Name must be at least 3 characters.");
			TestUtil.takeScreenshotAtEndOfTest("Agent Name required validation is failed");// ScreenShot capture
		}
		
		profile_name();
		save_Button();
	}
	
	
	public void update_agent_profile_Moblile_number() {
		profilepage();
		profile_name();
		profile.profile_mobile_num.sendKeys("5555555555");
		save_Button();
		
		if (profile.toaster_message.getText().equalsIgnoreCase("'Profile updated successfully.")) {
			Assert.assertEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile Mobile Number updated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile Mobile Number updation is failed");// ScreenShot capture
		}

	}
	
	public void update_agent_profile__Mobile_Number_invalid_validation() {
		profilepage();
		profile.profile_mobile_num.clear();
		profile.profile_mobile_num.sendKeys("55");
		save_Button();
		
		if (profile.profile_mobile_num_required.getText().equalsIgnoreCase("'Mobile number must be a valid 10-digit phone numbe'")) {
			Assert.assertEquals(profile.profile_mobile_num_required.getText(), "'Mobile number must be a valid 10-digit phone numbe'",
					"'Mobile number must be a valid 10-digit phone numbe'");
			TestUtil.takeScreenshotAtEndOfTest("Agent Mobile Numebr invalidation Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(profile.profile_mobile_num_required.getText(), "'Mobile number must be a valid 10-digit phone numbe'",
					"'Mobile number must be a valid 10-digit phone numbe'");
			TestUtil.takeScreenshotAtEndOfTest("Agent Mobile Number invalidation is failed");// ScreenShot capture
		}
		profile.profile_mobile_num.clear();
	}
	
	public void update_agent_profile_singature_enable() {
		profilepage();
		profile.signature_text.clear();
		signature();
		save_Button();
		
		if (profile.toaster_message.getText().equalsIgnoreCase("'Profile updated successfully.")) {
			Assert.assertEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile signature is updated  Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile signature updation is failed");// ScreenShot capture
		}

	}
	
	public void clickon_Cancel_button() throws Exception {
		profilepage();
		profile.cancel_Button.click();
		
		waits(1);
		if (verifyElementIsEnabled(profile.threedots)) {
			Assert.assertEquals(true, verifyElementIsEnabled(profile.threedots));
			button(profile.threedots, "button").isDisplayed();
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case passed");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(profile.threedots));
			
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case failed");// ScreenShot capture
		}
	}
	
	public void clickon_Close_profile_popup_button() throws Exception {
		clickonthreedotslink();
		profileLink();
		profilepage();
		profile.close_profile_popup.click();
		
		waits(1);
		if (verifyElementIsEnabled(profile.threedots)) {
			Assert.assertEquals(true, verifyElementIsEnabled(profile.threedots));
			button(profile.threedots, "button").isDisplayed();
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case passed");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(profile.threedots));
			
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case failed");// ScreenShot capture
		}
	}
	
	public void update_agent_profile_100_char_singature_enable() {
		profilepage();
		signature_Clear();
		signature_100_char();
		save_Button();
		
		if (profile.toaster_message.getText().equalsIgnoreCase("'Profile updated successfully.")) {
			Assert.assertEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile signature is updated  Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(profile.toaster_message.getText(), "'Profile updated successfully.",
					"'Profile updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Profile signature updation is failed");// ScreenShot capture
		}

	}
}
