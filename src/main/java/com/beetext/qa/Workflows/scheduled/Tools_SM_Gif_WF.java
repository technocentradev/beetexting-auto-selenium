package com.beetext.qa.Workflows.scheduled;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Tools_SM_pages;
import com.beetext.qa.util.TestUtil;

public class Tools_SM_Gif_WF extends Tools_SM_WFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Tools_SM_pages SM = new Tools_SM_pages();
	TestUtil testUtil = new TestUtil();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void TC_001_Verify_Giphy_popup() throws Exception {
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.SM_GIF_icon, "Click on Gif icon").click();
		logger.info("Clicked on Gif icon ");

		if (verifyElementIsEnabled(SM.SM_GIF_popup)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_popup));
			System.out.println("Gif  popup with all options is opened");
			TestUtil.takeScreenshotAtEndOfTest("Gif  popup with all options is opened case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_popup));
			System.out.println("Gif  popup with all options is not opened");
			TestUtil.takeScreenshotAtEndOfTest("Gif  popup with all options is opened case is not working as expected");
		}
		waits(3);
//		

	}

	public void TC_002_Verify_Giphy_Attachment() throws Exception {
		SM.SM_GIF_image1.click();
		logger.info("Clicked on first image in popup page");
		waits(2);
		if (verifyElementIsEnabled(SM.SM_GIF_Attachment1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_Attachment1));
			System.out.println("Selected Gif1 attachment is added");
			TestUtil.takeScreenshotAtEndOfTest("Add GIF attachment to message case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_Attachment1));
			System.out.println("Selected Gif1 attachment is added");
			TestUtil.takeScreenshotAtEndOfTest("Add GIF attachment to message case is not working as expected");
		}
		waits(2);

	}

	public void TC_003_Verify_Giphy_images_Using_Search() throws Exception {

		button(SM.SM_GIF_icon, "Click on Gif icon").click();
		logger.info("Clicked on Gif icon and popup is opened");
		waits(5);
		logger.info("wait for 5 sec");
		textbox(SM.SM_Gif_Search).sendKeys("a");
		logger.info("entered 'a' into popup search");
		waits(5);
		logger.info("wait for 5 sec");

		if (verifyElementIsEnabled(SM.SM_GIF_image1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_image1));
			System.out.println("Verified image1 related to search");
			TestUtil.takeScreenshotAtEndOfTest("Verified images related to search case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_image1));
			System.out.println("Verified image1 related to search");
			TestUtil.takeScreenshotAtEndOfTest("Verified images related to search case is not working as expected");
		}

		if (verifyElementIsEnabled(SM.SM_GIF_image2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_image2));
			System.out.println("Verified image2 related to search");
			TestUtil.takeScreenshotAtEndOfTest("Verified images related to search case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_image2));
			System.out.println("Verified image2 related to search");
			TestUtil.takeScreenshotAtEndOfTest("Verified images related to search case is working as expected");
		}

		if (verifyElementIsEnabled(SM.SM_GIF_image3)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_image3));
			System.out.println("Verified image3 related to search");
			TestUtil.takeScreenshotAtEndOfTest("Verified images related to search case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_image3));
			System.out.println("Verified image3 related to search");
			TestUtil.takeScreenshotAtEndOfTest("Verified images related to search case is working as expected");
		}
		waits(2);

	}

	public void TC_004_Verify_Giphy_error_message_for_invalid_Search() throws Exception {

		textbox(SM.SM_Gif_Search).clear();
		waits(2);
		textbox(SM.SM_Gif_Search).sendKeys("$");
		logger.info("entered '$' into popup search");
		waits(5);
		logger.info("wait for 5 sec");
		SM.SM_Gif_Search_Error_Message.getText();
		System.out.println(SM.SM_Gif_Search_Error_Message.getText());
		if (SM.SM_Gif_Search_Error_Message.getText().equalsIgnoreCase(" We didn't find any matches. ")) {
			Assert.assertEquals(" We didn't find any matches. ", " We didn't find any matches. ");
			System.out.println(" Error message for invalid search is available");
			TestUtil.takeScreenshotAtEndOfTest("Error message for invalid search case is working as expected");
		} else {
			Assert.assertEquals("We didn't find any matches. ", "We didn't find any matches. ");
			System.out.println(" Error message for invalid search is not available");
			TestUtil.takeScreenshotAtEndOfTest("Error message for invalid search case is not working as expected");

		}
		waits(2);
	}

	public void TC_005_Verify_Functionality_of_Gif_Close_icon() throws Exception {

		textbox(SM.SM_Gif_Search).clear();
		textbox(SM.SM_Gif_Search).sendKeys("a");
		logger.info("entered 'a' into popup search");
		waits(5);
		logger.info("wait for 5 sec");
		button(SM.SM_Gif_Close_icon, "Click on Close icon").click();
		logger.info("Clicked on Close icon ");
		waits(2);
		logger.info("waits for 2 sec");

		if (verifyElementIsEnabled(SM.SM_GIF_image1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_image1));
			System.out.println("Verified basic image1 ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified basic images after click on Close icon case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_image1));
			System.out.println("Verified image1 related to search");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified basic images after click on Close icon case is not working as expected");
		}

		if (verifyElementIsEnabled(SM.SM_GIF_image2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_image2));
			System.out.println("Verified image2");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified basic images after click on Close icon case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_image2));
			System.out.println("Verified image2 ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified basic images after click on Close icon case is working as expected");
		}

		if (verifyElementIsEnabled(SM.SM_GIF_image3)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_image3));
			System.out.println("Verified image3");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified basic images after click on Close icon case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_image3));
			System.out.println("Verified image3 ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified basic images after click on Close icon case is working as expected");
		}
		waits(2);

	}

	public void TC_006_Verify_Functionality_of_Gif_ViewTerms() throws Exception {

		waits(5);
		logger.info("wait for 5 sec");
		link(SM.SM_Gif_ViewTerms_link, "Click on View Terms").click();
		logger.info("Clicked on View Terms link");
		waits(10);
		logger.info("waits for 10 sec");

		ArrayList<String> tabL1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabL1.get(1));

		waits(10);
		driver.close();
		logger.info("Closed the page");
		driver.switchTo().window(tabL1.get(0));
		logger.info("Switched to previous page");
		waits(5);
		logger.info("wait for 5 sec");

	}

	public void TC_007_Verify_Functionality_of_Gif_popup_by_OutsideClick() throws Exception {

		waits(5);
		logger.info("wait for 5 sec");
		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click()", element);

		logger.info("Clicked on outside of popup");
		wait(5);
		logger.info("Waits for 5 sec");

		if (verifyElementIsEnabled(SM.SM_GIF_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_icon));
			System.out.println("Gif icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest("Close Gif popup on outside click case is working as expected");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_popup));
			System.out.println("Gif icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("Close Gif popup on outside click case is not working as expected");
		}
		waits(2);

	}

	public void TC_008_Verify_Record_with_Gif_attachments_under_upcoming() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		Click_on_CreateScheduledMessage_button();
		String arr[] = { "4255058829", "7273039977", "5555555555", "5555555550", "5555555552", "5555555553",
				"5555555554", "5555555556", "5555555557" };
		waits(2);
		for (int i = 0; i <= 8; i++) {

			waits(2);
			textbox(SM.Message_sent_to_textbox).sendKeys(arr[i]);
			logger.info("Entered contact number");
			waits(2);
			logger.info("wait for 2 seconds");
			driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
			logger.info("Added contact number to message sent textbox");
			waits(2);
			logger.info("wait for 2 seconds");

		}
		waits(2);

		button(SM.SM_GIF_icon, "Click on Gif icon").click();
		logger.info("Clicked on Gif icon and popup is opened");
		waits(5);
		logger.info("wait for 5 sec");
		SM.SM_GIF_image1.click();
		logger.info("Clicked on image 1 and added as attachment");
		waits(2);
		logger.info("Wait for 2 sec");

		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);

		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(3);
		logger.info("wait for 3 sec");
		button(SM.SM_selected_Upcoming_button, "click on upcoming button which is already selected").click();
		waits(2);
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with Gif attachments is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with Gif attachments is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with Gif attachments is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with Gif attachments is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		waits(2);
		DeleteSM();
	}

	public void TC_009_Verify_Record_with_Gif_attachments_under_upcoming() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		Click_on_CreateScheduledMessage_button();
		String arr[] = { "4255058829", "7273039977", "5555555555", "5555555550", "5555555552", "5555555553",
				"5555555554", "5555555556", "5555555557" };
		waits(2);
		for (int i = 0; i <= 8; i++) {

			waits(2);
			textbox(SM.Message_sent_to_textbox).sendKeys(arr[i]);
			logger.info("Entered contact number");
			waits(2);
			logger.info("wait for 2 seconds");
			driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
			logger.info("Added contact number to message sent textbox");
			waits(2);
			logger.info("wait for 2 seconds");

		}
		waits(2);
		Enter_message();
		button(SM.SM_GIF_icon, "Click on Gif icon").click();
		logger.info("Clicked on Gif icon and popup is opened");
		waits(5);
		logger.info("wait for 5 sec");
		SM.SM_GIF_image1.click();
		logger.info("Clicked on image 1 and added as attachment");
		waits(2);
		logger.info("Wait for 2 sec");

		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(3);
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(3);
		logger.info("wait for 3 sec");
		button(SM.SM_selected_Upcoming_button, "click on upcoming button which is already selected").click();
		waits(2);
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with Gif attachments is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with Gif attachments is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with Gif attachments is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with Gif attachments is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		waits(2);
		DeleteSM();
	}

	public void TC_010_Verify_Record_with_Gif_attachments_under_upcoming() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		button(SM.SM_GIF_icon, "Click on Gif icon").click();
		logger.info("Clicked on Gif icon and popup is opened");
		waits(5);
		logger.info("wait for 5 sec");
		SM.SM_GIF_image1.click();
		logger.info("Clicked on image 1 and added as attachment");
		waits(2);
		logger.info("Wait for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_Drafts_Button, "click on Drafts tab button").click();
		logger.info("Clickedon Drafts tab button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified saved record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved record under Drafts list case is pass");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified saved record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved record under Drafts list case is not working  fine");
		}
		logger.info("Verified saved record under Drafts list");

		delete_draft();

	}

	public void TC_011_Verify_Record_with_Gif_attachments_under_upcoming() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(4);
		logger.info("wait for 4 sec");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(4);
		logger.info("wait for 4 sec");
		button(SM.SM_Upcoming_tab, "click on upcomming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("wait for 2 sec");

		button(SM.SM_GIF_icon, "Click on Gif icon").click();
		logger.info("Clicked on Gif icon and popup is opened");
		waits(5);
		logger.info("wait for 5 sec");
		SM.SM_GIF_image1.click();
		logger.info("Clicked on image 1 and added as attachment");
		waits(2);
		logger.info("Wait for 2 sec");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(3);

		WebElement element1 = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_selected_Upcoming_button, "click on upcoming button which is already selected").click();
		waits(2);
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with Gif attachments is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with Gif attachments is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with Gif attachments is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with Gif attachments is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		waits(2);
		DeleteSM();
	}

}
