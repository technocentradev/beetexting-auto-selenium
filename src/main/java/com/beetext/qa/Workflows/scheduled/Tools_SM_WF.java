package com.beetext.qa.Workflows.scheduled;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Tools_SM_pages;
import com.beetext.qa.util.TestUtil;

public class Tools_SM_WF extends Tools_SM_WFCM {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Tools_SM_pages SM = new Tools_SM_pages();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	// Create cases for SM Starts

	public void tc_001_functionality_of_Create_Scheduled_message_button() throws Exception

	{
		button(SM.Create_Scheduled_Message_button, "click on create scheduled message button").click();
		logger.info("clicked on create scheduled message button");
		waits(2);
		logger.info("waits  for 10 sec");

		SM.Scheduled_message_link_button.isEnabled();
		logger.info("scheduled message button is verified");
		System.out.println(SM.Scheduled_message_link_button.isEnabled());
		if (verifyElementIsEnabled(SM.Scheduled_message_link_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Scheduled_message_link_button));
			System.out.println("scheduled message link is available and enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"click on create scheduled message button redirects to Create SM page case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Scheduled_message_link_button));
			System.out.println("scheduled message link is available and not enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"click on create scheduled message button redirects to Create SM page case is not working  fine");
		}

		button(SM.Scheduled_message_link_button, "click on scheduled message link").click();
		logger.info("clicked on scheduled message link button");
		waits(2);
		logger.info("waits for 10 sec");
		logger.info("popup message opened");
		button(SM.popup_Discard_button, "click on discard button in popup").click();
		logger.info("clicked on discard button then popup closed");
		waits(2);
		logger.info("waits for 5 seconds");

	}

	public void tc_002_functionality_of_Select_Department_DD() throws Exception

	{
		textbox(SM.select_department_DD).click();
		logger.info("clicked on select department DD");
		waits(2);
		logger.info("waits for 2 seconds");

		List<WebElement> allOptions = driver
				.findElements(By.xpath("//select[@class='form-control ng-pristine ng-valid ng-touched']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Department-2")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Department-2 is selected under select department DD");

	}

	public void tc_004_Should_allow_only_9_contacts_validation() throws Exception

	{
		waits(2);
		Click_on_CreateScheduledMessage_button();

		String arr[] = { "4255058829", "7273039977", "5555555555", "5555555551", "5555555552", "5555555553",
				"5555555554", "5555555556", "5555555557" };

		for (int i = 0; i <= 8; i++) {

			textbox(SM.Message_sent_to_textbox).sendKeys(arr[i]);
			logger.info("Entered contact number");
			waits(2);
			logger.info("waits for 5 seconds");
			driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
			logger.info("Added contact number to message sent textbox");
			waits(2);
			logger.info("waits for 2 seconds");

		}

		waits(2);
		SM.Max_9_Contact_allowed_text_message.getText();
		logger.info("verified text message for Max 10 contact");
		waits(2);
		logger.info("waits for 5 seconds");
		System.out.println(SM.Max_9_Contact_allowed_text_message.getText());
		waits(2);
		logger.info("waits for 5 seconds");

		if (SM.Max_9_Contact_allowed_text_message.getText().equalsIgnoreCase("Maximum 9 contacts are allowed.")) {
			Assert.assertEquals(SM.Max_9_Contact_allowed_text_message.getText(), "Maximum 9 contacts are allowed.",
					"Maximum 9 contacts are allowed.");
			System.out.println("Maximum 9 contacts are allowed. text message is available and verified");
			TestUtil.takeScreenshotAtEndOfTest("text validation message for 10 contacts case is passed");

		} else {
			Assert.assertEquals(SM.Max_9_Contact_allowed_text_message.getText(), "Maximum 9 contacts are allowed.");
			System.out.println("Maximum 9 contacts are allowed. text message is not available and not identified");
			TestUtil.takeScreenshotAtEndOfTest("validation text message for contacts case is failed");
		}

		button(SM.Scheduled_message_link_button, "click on scheduled message link").click();
		logger.info("clicked on scheduled message link button");
		waits(2);
		logger.info("waits for 2 seconds");
		logger.info("popup page is opened");
		button(SM.popup_Discard_button, "click on discard button in popup").click();
		logger.info("clicked on discard buton in popup");
		waits(2);
		logger.info("waits for 2 seconds");

	}

	public void tc_006_functionality_of_Close_mark_for_contact() throws Exception

	{
		Click_on_CreateScheduledMessage_button();

		Enter_Contact_number();
		button(SM.close_mark_button_for_contact, "click on close button related to contact").click();
		logger.info("clicked on close icon");
		waits(2);
		logger.info("waits for 5 seconds");

		System.out.println("Contact have been deleted successfully");
		logger.info("contact is deleted and remains in create SM page");

	}

	public void tc_007_Validation_message_for_1000_char_in_message_textbox() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("cleared text in the message text box");
		waits(2);
		logger.info("waits for 5 seconds");

		String generatedstring = RandomStringUtils.randomAlphabetic(1000);
		waits(2);
		textbox(SM.Create_SM_Message_textbox).sendKeys(generatedstring);
		logger.info("entered text into the message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		SM.Create_SM_Message_char_count.getText();
		logger.info("verified char count of the text");
		waits(2);
		logger.info("waits for 5 seconds");
		System.out.println(SM.Create_SM_Message_char_count.getText());
		logger.info("verified the char count of the text");
		waits(2);
		logger.info("waits for 2 seconds");

		if (SM.Create_SM_Message_char_count.getText().equalsIgnoreCase("1000/1000")) {
			Assert.assertEquals(SM.Create_SM_Message_char_count.getText(), "1000/1000", "1000/1000");
			System.out.println("1000/1000 char count is available");
			TestUtil.takeScreenshotAtEndOfTest("message text box should allow 1000 char case is passed");
		} else {

			Assert.assertEquals(SM.Create_SM_Message_char_count, "1000/1000");
			System.out.println("1000/1000 char count is not available");
			TestUtil.takeScreenshotAtEndOfTest("message text box should allow 1000 char case is failed");

		}

	}

	public void tc_012_Allows_Max_10_attachments() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");

		System.out.println("Allowed all attachments");

	}

	public void tc_013_create_SM_with_attachment() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with image is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with image is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with image is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with image is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record is created and shown under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void tc_014_create_SM_with_textmessage() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record  is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record  is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record  is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record is created and shown under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void tc_015_create_SM_with_pastdate() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Downarrow_button_Time_hour, "Click on Downarrow button for Time").click();
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.Validation_message_for_previous_date_time.getText();
		waits(2);
		System.out.println(SM.Validation_message_for_previous_date_time.getText());

		if (SM.Validation_message_for_previous_date_time.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "Please select a date and time in the future.");
			System.out.println("Please select a date and time in the future. is available");
			logger.info("validation text message for previous date and time is available");
			TestUtil.takeScreenshotAtEndOfTest("Validation message for previous time test case is passed");
		} else {

			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "");
			System.out.println("Please select a date and time in the future. text message is not available");
			logger.info("validation text message for previous date and time is not available");
			TestUtil.takeScreenshotAtEndOfTest("Validation message for previous time test case is Failed");

		}
		logger.info("Validation message for previous date and time is verified");
		waits(2);

	}

	public void tc_016_Save_SM_with_attachments() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(2);
		logger.info("waits for 2 seconds");

		WebElement element = driver.findElement(By.id("schedule_DraftButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on save draft button");
		waits(5);
		logger.info("wait for 5 sec");
		button(SM.SM_Drafts_Button, "click on Drafts tab button").click();
		logger.info("Clicked on Drafts button");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with attachments is saved and available in drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with attachments is saved and available in drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with attachments is not saved and  not available in drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with attachments is saved and available in drafts list case is not working  fine");
		}
		logger.info("Verified the record is saved and shown under drafts  list");
		waits(2);
		delete_draft();

	}

	public void tc_017_functionality_of_Sceduled_button() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("recurring button is checked");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("clicked on Scheduled message button");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_Upcoming_tab, "click on Upcomming tab button").click();
		waits(2);
		SM.Recurring_Record_under_upcoming_list.isEnabled();

		System.out.println(SM.Recurring_Record_under_upcoming_list.isEnabled());
		if (verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			System.out.println("created contact is added under upcomming list");
			TestUtil.takeScreenshotAtEndOfTest("created contact is added under upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			System.out.println("created contact is not added under upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"created contact is added under upcomming list case is not working  fine");
		}
		logger.info("Record is verified under upcoming list");
		DeleteSM();

	}

	public void tc_018_functionality_of_Sceduled_button_with_attachments() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("recurring button is checked");
		waits(2);
		logger.info("waits for 2 seconds");

		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Recurring_Record_under_upcoming_list.isEnabled();

		System.out.println(SM.Recurring_Record_under_upcoming_list.isEnabled());
		if (verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			System.out.println("created record is added under upcomming list");
			TestUtil.takeScreenshotAtEndOfTest("created record is added under upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			System.out.println("created record is not added under upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"created record is added under upcomming list case is not working  fine");
		}
		logger.info("Record is verified under upcoming list");
		DeleteSM();

	}

	public void tc_019_Save_SM_with_message_attachments() throws Exception

	{

		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_message();

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(2);
		logger.info("waits for 2 seconds");

		WebElement element = driver.findElement(By.id("schedule_DraftButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on save draft button");
		waits(3);
		logger.info("wait for 3 sec");
		button(SM.SM_Drafts_Button, "click on Drafts tab button").click();
		logger.info("Clicked on Drafts button");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with attachments is saved and available in drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with attachments is saved and available in drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with attachments is not saved and  not available in drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with attachments is saved and available in drafts list case is not working  fine");
		}
		logger.info("Verified the record is saved and shown under drafts  list");
		waits(2);
		delete_draft();
	}

	public void tc_020_verify_SM_without_contact_under_Draft_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("cleared text in the message text field");
		waits(2);
		logger.info("waits for 5 seconds");

		Enter_message();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the edited information under draft");
		DeleteSM();
	}

	public void tc_021_verify_SM_with_only_contact_under_Draft_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("waits for 10 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("clicked on Draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("verified the saved record under draft");
		DeleteSM();
	}

	public void tc_022_Functionality_of_Delete_attachments() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Close_icon_of_Attachment, "click on close icon of the attachment").click();
		logger.info("Clicked on close icon of attachment");
		logger.info("attachement was deleted");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("Verified the attachment was deleted and  not available");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		delete_draft();
	}

	public void tc_023_Functionality_of_save_Draft_button_in_CSM() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		button(SM.Save_Draft_button_in_CSM, "click on save draft button").click();
		logger.info("Clicked on Save draft button");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("clicked on Drafts button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the saved draft under drafts list");
		DeleteSM();
	}

	public void tc_024_Functionality_of_save_Draft_button_in_CSM() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.Save_Draft_button_in_CSM, "click on save draft button").click();
		logger.info("licked on Save draft button");
		waits(2);
		logger.info("waits for 10 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Drafts button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("verified saved draft under draft list");
		DeleteSM();
	}

	public void tc_025_Functionality_of_SecheduleMessage_button_in_CSM_without_data() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on scheduled message button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified saved draft under drafts list");
		DeleteSM();

	}

	public void tc_026_Functionality_of_SaveDraft_button_in_CSM_without_data() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Save_Draft_button_in_CSM, "click on save draft button").click();
		logger.info("Clicked on Save draft button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified saved data under drafts list ");
		DeleteSM();

	}

	public void tc_027_Functionality_of_ScheduledMessage_button_in_CSM_with_only_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the saved record in drafts list");
		DeleteSM();

	}

	public void tc_028_Functionality_of_ScheduledMessage_button_in_CSM_with_message_and_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		Click_Recurring_button();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the saved data in drafts list");
		DeleteSM();

	}

	public void tc_029_functionality_of_SaveDraft_with_allData() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("deleted the text in message text field");
		waits(2);
		logger.info("waits for 5 seconds");
		Enter_message();
		Click_Recurring_button();
		button(SM.Save_Draft_button_in_CSM, "click on save draft button").click();
		logger.info("Clicked on save draft button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record in drafts list");
		delete_draft();
	}

	public void tc_030_Functionality_of_SaveDraft_button_in_CSM_with_only_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();
		button(SM.Save_Draft_button_in_CSM, "click on Save Draft button").click();
		logger.info("clicked on save draft button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		DeleteSM();

	}

	public void tc_031_Functionality_of_SaveDraft_button_in_CSM_with_message_and_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		Click_Recurring_button();
		button(SM.Save_Draft_button_in_CSM, "click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified saved draft under drafts list");
		DeleteSM();

	}

	public void tc_032_functionality_of_ScheduledMessage_button_in_CSM_with_Reciepient() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		DeleteSM();
	}

	public void tc_033_functionality_of_ScheduledMessage_button_in_CSM_with_message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		delete_draft();
	}

	public void tc_034_functionality_of_ScheduledMessage_button_in_CSM_with_attachment() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		delete_draft();
	}

	public void tc_036_functionality_of_ScheduledMessage_button_in_CSM_with_Reciepient_and_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Click_Recurring_button();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}

		logger.info("Verified saved record under drafts list");
		DeleteSM();

	}

	public void tc_038_functionality_of_ScheduledMessage_button_with_attachment_recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(2);
		logger.info("waits for 2 seconds");
		Click_Recurring_button();
		button(SM.create_SM_ScheduledMessage_button, "click on scheduled message button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		delete_draft();
	}

	public void tc_041_functionality_of_SaveDraft_button_with_attachments() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Save_Draft_button_in_CSM, "click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		delete_draft();
	}

	public void tc_042_functionality_of_SaveDraft_button_in_CSM_with_Reciepient_and_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Click_Recurring_button();
		button(SM.Save_Draft_button_in_CSM, "click on Save Draft button").click();
		logger.info("Clicked on save draft button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}

		logger.info("Verified saved draft under Drafts list");
		DeleteSM();

	}

	public void tc_043_functionality_of_SaveDraft_button_in_CSM_with_message_and_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		Click_Recurring_button();
		button(SM.Save_Draft_button_in_CSM, "click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}

		logger.info("Verified the saved draft under drafts list");
		DeleteSM();

	}

	public void tc_044_functionality_of_SaveDraft_button_in_CSM_with_Attachment_and_Recurring() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		Click_Recurring_button();
		button(SM.Save_Draft_button_in_CSM, "click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}

		logger.info("Verified the saved draft under drafts list");
		delete_draft();

	}

	public void tc_046_Functionality_of_SaveDraft_button_in_Confermation_popup() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}

		logger.info("Verified saved record under drafts list");
		DeleteSM();

	}

	public void tc_045_Functionality_of_Breadscrum_link_in_CSM() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.text_in_Breadcrum_popup_page.getText();
		waits(2);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());

		if (SM.text_in_Breadcrum_popup_page.getText()
				.equalsIgnoreCase("Do you want to save scheduled text as draft?")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opend ");
			TestUtil.takeScreenshotAtEndOfTest("Functionality of Breadcrum test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?", "");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("Functionality of Breadcrum test case is Failed");

		}

		logger.info("popup  is opened");
		button(SM.Breadcrum_popup_discard_button, "click on discard button in popup").click();
		logger.info("Clicked on discard button");
		waits(2);

	}

	public void tc_047_Functionality_of_Discard_button_conformation_popup() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");

		button(SM.Breadcrum_popup_discard_button, "click on discard button in popup").click();
		logger.info("clicked on discard button in popup page");
		waits(5);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 5 seconds");
		SM.YouCanSaveDraftScheduledMessages_text_under_Drafts_list.getText();
		System.out.println(SM.YouCanSaveDraftScheduledMessages_text_under_Drafts_list.getText());
		waits(2);
		if (SM.YouCanSaveDraftScheduledMessages_text_under_Drafts_list.getText()
				.equalsIgnoreCase("You can save your draft scheduled texts.")) {
			Assert.assertEquals(SM.YouCanSaveDraftScheduledMessages_text_under_Drafts_list.getText(),
					"You can save your draft scheduled texts.", "You can save your draft scheduled texts.");
			System.out.println("Record is deleted and not saved in Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Functionality of SaveDraft button in popup is working as expected");

		} else {

			Assert.assertEquals(SM.YouCanSaveDraftScheduledMessages_text_under_Drafts_list.getText(),
					"You can save your draft scheduled texts.", "");
			System.out.println("Record is not deleted and saved in Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Functionality of SaveDraft button in popup is not working as expected");

		}
		logger.info("verified the deleted record under drafts list");

	}

	public void tc_048_Scenario_of_close_popup_by_click_on_outside() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 10 seconds");

		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on outside of popup page");
		waits(2);
		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());

		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("Create Scheduled message button is available and enabled");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is working as expected");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("Create Scheduled message button is not available and enabled");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is not working as expected");
		}

		logger.info("Verified that user is in main SM page");
	}

	public void tc_049_Functionality_of_Breadcrum_link_with_Recipient() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.text_in_Breadcrum_popup_page.getText();
		waits(5);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());

		if (SM.text_in_Breadcrum_popup_page.getText().equalsIgnoreCase(" Do you want to save scheduled message ")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opened ");
			TestUtil.takeScreenshotAtEndOfTest("popup is opend test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("popup is opened test case is Failed");

		}
		logger.info("Verified popup is opened with SaveDraft and Discard buttons");
		button(SM.Breadcrum_popup_discard_button, "click on Discard button").click();
		waits(5);

	}

	public void tc_053_Functionality_of_Breadcrum_link_with_Message() throws Exception

	{

		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 5 seconds");
		SM.text_in_Breadcrum_popup_page.getText();
		waits(2);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());

		if (SM.text_in_Breadcrum_popup_page.getText()
				.equalsIgnoreCase("Do you want to save scheduled text as draft?")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opened ");
			TestUtil.takeScreenshotAtEndOfTest("popup is opend test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?", "");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("popup is opened test case is Failed");

		}
		logger.info("Verified popup page is opened");
		button(SM.Breadcrum_popup_discard_button, "click on Discard button").click();

	}

	public void tc_050_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Recipient() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link button");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on Save draft button");
		waits(5);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}

		logger.info("Verified the saved draft under drafts list");
		delete_draft();

	}

	public void tc_051_Functionality_of_Discard_button_in_Confermation_popup_with_Recipient() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link ");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println(
					"popup closed and Navigated to main SM page and verified 'Create Scheduled message button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("verified it redirectd to SM main page");

	}

	public void tc_052_Scenario_of_close_popup_by_click_on_outside_with_Recipient() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 2 seconds");

		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on outside of popuppage");
		waits(5);
		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());

		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("Create Scheduled message button is available and enabled");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is working as expected");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("Create Scheduled message button is not available and enabled");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is not working as expected");
		}

		logger.info(" verified popup closed and redirected to SM main page");
	}

	public void tc_054_Functionality_of_SaveDraft_button_in_Confermation_popup_with_message() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link ");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on Save draft button in popup page");
		waits(5);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified saved draft under drafts list");
		delete_draft();

	}

	public void tc_055_Functionality_of_Discard_button_in_Confermation_popup_with_message() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println(
					"popup closed and Navigated to main SM page and verified 'Create Scheduled message button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified navigated to SM main page");

	}

	public void tc_056_Scenario_of_close_popup_by_click_on_outside_with_message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("CLicked on SM link button");
		waits(5);
		logger.info("waits for 2 seconds");

		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on outside of the popup page");
		waits(5);
		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());

		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup is closed and Create Scheduled message button is verified");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is working as expected");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup is not closed and Create Scheduled message button is not verified");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is not working as expected");
		}
		logger.info("Verified popup closed and navigated to SM main page");

	}

	public void tc_057_and_tc_059_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment()
			throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");

		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println(
					"popup closed and Navigated to main SM page and verified 'Create Scheduled message button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified navigated to SM main page");

	}

	public void tc_058_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachments() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("verified saved draft under drafts list");
		delete_draft();
		Close_Tools_page();
		logout();

	}

	public void tc_060_Functionality_of_Breadcrum_link_with_Message_Recipient() throws Exception

	{
		Login2_Tools_SM();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.text_in_Breadcrum_popup_page.getText();
		waits(2);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());

		if (SM.text_in_Breadcrum_popup_page.getText()
				.equalsIgnoreCase("Do you want to save scheduled text as draft?")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opened ");
			TestUtil.takeScreenshotAtEndOfTest("popup is opend test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?", "");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("popup is opened test case is Failed");

		}
		logger.info("Verified popup is opened");
		button(SM.Breadcrum_popup_discard_button, "click on discard button").click();
		waits(5);

	}

	public void tc_061_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Recipient_message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft in popup");
		waits(5);
		logger.info("waits for 5 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("verified saved draft under drafts list");
		DeleteSM();

	}

	public void tc_062_Functionality_of_Discard_button_in_Confermation_popup_with_Recipient_Message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println(
					"popup closed and Navigated to main SM page and verified 'Create Scheduled message button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified navigated to SM main page");

	}

	public void tc_063_Scenario_of_close_popup_by_click_on_outside_with_Recipient_Message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link button");
		waits(5);
		logger.info("waits for 2 seconds");

		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on outside of the popup");
		waits(5);
		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());

		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("Create Scheduled message button is available and enabled");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is working as expected");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("Create Scheduled message button is not available and enabled");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is not working as expected");
		}
		logger.info("verified navigated to SM main page");

	}

	public void tc_064_and_tc_066_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment_and_Recipent()
			throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println(
					"popup closed and Navigated to main SM page and verified 'Create Scheduled message button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified navigated to SM main page");

	}

	public void tc_067_Scenario_of_close_popup_by_click_on_outside_with_message() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		waits(5);
		logger.info("Clicked on outside of the popup page");
		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());

		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup is closed and Create Scheduled message button is verified");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is working as expected");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup is not closed and Create Scheduled message button is not verified");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is not working as expected");
		}
		logger.info("Verified popup closed and navigated to SM main page");

	}

	public void tc_068_and_tc_070_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment_message_Recipent()
			throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println(
					"popup closed and Navigated to main SM page and verified 'Create Scheduled message button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified navigated to SM main page");

	}

	public void tc_065_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachments_Recipient()
			throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("verified saved draft under drafts list");
		delete_draft();

	}

	public void tc_069_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachments_message_Recipient()
			throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("verified saved draft under drafts list");
		delete_draft();

	}

	public void tc_071_Scenario_of_close_popup_by_click_on_outside_with_message_recipient_attachment() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.schedule_breadcrumId_link_CSM, "click on scheduled message link").click();
		logger.info("Clicked on SM breadcrum link");
		waits(5);
		logger.info("waits for 2 seconds");
		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on outside of the popup page");
		waits(5);
		SM.Create_Scheduled_Message_button.isEnabled();
		System.out.println(SM.Create_Scheduled_Message_button.isEnabled());
		if (verifyElementIsEnabled(SM.Create_Scheduled_Message_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup is closed and Create Scheduled message button is verified");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is working as expected");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.Create_Scheduled_Message_button));
			System.out.println("popup is not closed and Create Scheduled message button is not verified");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is not working as expected");
		}
		logger.info("Verified popup closed and navigated to SM main page");

	}

	public void tc_072_Functionality_of_other_link_link_with_Message() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		link(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(5);
		logger.info("waits for 5 seconds");
		SM.text_in_Breadcrum_popup_page.getText();
		waits(5);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());
		if (SM.text_in_Breadcrum_popup_page.getText()
				.equalsIgnoreCase("Do you want to save scheduled text as draft?")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opened ");
			TestUtil.takeScreenshotAtEndOfTest("popup is opend test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?", "");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("popup is opened test case is Failed");

		}

		logger.info("verified popup page");
		button(SM.Discard_button_in_other_Link_conformation_popup, "click on Discard button").click();
		waits(5);
	}

	public void tc_073_Functionality_of_SaveDraft_button_in_Confermation_popup_with_message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft button in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified record in Drafts list ");
		delete_draft();

	}

	public void tc_074_Functionality_of_Discard_button_in_Confermation_popup_with_Message() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(4);
		logger.info("waits for 2 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		SM.CreateTemplate_button.isEnabled();
		System.out.println(SM.CreateTemplate_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.CreateTemplate_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out
					.println("popup closed and Navigated to main Templates page and verified 'Create Templet button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified navigation to Template main page");
		waits(2);

	}

	public void tc_075_Scenario_of_close_popup_by_click_on_outside_with_Message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_message();
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(3);
		logger.info("waits for 3 seconds");
		WebElement element = driver.findElement(By.xpath("//div[@class='model-bg']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		waits(5);
		logger.info("Clicked on outside of the popup page");
		SM.CreateTemplate_button.isEnabled();
		System.out.println(SM.CreateTemplate_button.isEnabled());
		if (verifyElementIsEnabled(SM.CreateTemplate_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out.println("navigated to Templets page and verified create templet button");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is working as expected");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out.println("popup didn't closed and not navigated to Templets page ");
			TestUtil.takeScreenshotAtEndOfTest("Click on outside of popup scenario is not working as expected");
		}

		logger.info("Verified popup is closed and navigated to Templets main page");
	}

	public void tc_076_Functionality_of_other_link_link_with_Recipient() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		link(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(3);
		logger.info("waits for 3 seconds");
		SM.text_in_Breadcrum_popup_page.getText();
		waits(2);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());

		if (SM.text_in_Breadcrum_popup_page.getText()
				.equalsIgnoreCase("Do you want to save scheduled text as draft?")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opened ");
			TestUtil.takeScreenshotAtEndOfTest("popup is opend test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?", "");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("popup is opened test case is Failed");

		}

		logger.info("Verified popup message");
		button(SM.Discard_button_in_other_Link_conformation_popup, "Click on Discard button").click();
		waits(5);
	}

	public void tc_077_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Recipient() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft button in popup");
		waits(5);
		logger.info("waits for 2 seconds");

		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 10 seconds");
		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		delete_draft();
	}

	public void tc_078_Functionality_of_Discard_button_in_Confermation_popup_with_Recipient() throws Exception

	{
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(3);
		logger.info("waits for 3 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		SM.CreateTemplate_button.isEnabled();
		System.out.println(SM.CreateTemplate_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.CreateTemplate_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out
					.println("popup closed and Navigated to main Templates page and verified 'Create Templet button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified popup closed and navigated to Templets page");

	}

	public void tc_079_Functionality_of_other_link_link_with_Attachments() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waits(5);
		logger.info("waits for 5 seconds");
		link(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(5);
		logger.info("waits for 5 seconds");
		SM.text_in_Breadcrum_popup_page.getText();
		waits(5);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());

		if (SM.text_in_Breadcrum_popup_page.getText()
				.equalsIgnoreCase("Do you want to save scheduled text as draft?")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opened ");
			TestUtil.takeScreenshotAtEndOfTest("popup is opend test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?", "");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("popup is opened test case is Failed");

		}

		logger.info("Verified popup message");
		waits(3);
		button(SM.Discard_button_in_other_Link_conformation_popup, "clcik on Discard button").click();
		waits(5);
	}

	public void tc_080_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachment() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft button in popup");
		waits(5);
		logger.info("waits for 5 seconds");

		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on drafts");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified the record under drafts list");
		delete_draft();
	}

	public void tc_081_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup");
		waits(5);
		logger.info("waits for 5 seconds");
		SM.CreateTemplate_button.isEnabled();
		System.out.println(SM.CreateTemplate_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.CreateTemplate_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out
					.println("popup closed and Navigated to main Templates page and verified 'Create Templet button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified popup closed and navigated to Templets page");
		waits(2);

	}

	public void tc_082_Functionality_of_other_link_link_with_AllData() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		link(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab ");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.text_in_Breadcrum_popup_page.getText();
		waits(5);
		System.out.println(SM.text_in_Breadcrum_popup_page.getText());

		if (SM.text_in_Breadcrum_popup_page.getText()
				.equalsIgnoreCase("Do you want to save scheduled text as draft?")) {
			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?",
					"Do you want to save scheduled text as draft?");
			System.out.println("popup with SaveDraft and Discard buttons is opened ");
			TestUtil.takeScreenshotAtEndOfTest("popup is opend test case is passed");
		} else {

			Assert.assertEquals(SM.text_in_Breadcrum_popup_page.getText(),
					"Do you want to save scheduled text as draft?", "");
			System.out.println("popup with SaveDraft and Discard buttons is not opened");
			TestUtil.takeScreenshotAtEndOfTest("popup is opened test case is Failed");

		}

		logger.info("Verified popup page");
		button(SM.Discard_button_in_other_Link_conformation_popup, "Click on Discard button").click();
		waits(5);
	}

	public void tc_083_Functionality_of_SaveDraft_button_in_Confermation_popup_with_AllData() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked on Templets tab");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SaveDraft_button_in_Breadcrum_popup, "click on SaveDraft button in popup").click();
		logger.info("Clicked on save draft button in popup page");
		waits(5);
		logger.info("waits for 5 seconds");

		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.SM_Drafts_Button, "click on draft button").click();
		logger.info("Clicked on Drafts");
		waits(5);
		logger.info("waits for 2 seconds");
		SM.SM_first_saved_draft.isEnabled();
		System.out.println(SM.SM_first_saved_draft.isEnabled());
		if (verifyElementIsEnabled(SM.SM_first_saved_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_first_saved_draft));
			System.out.println("saved SM is not added under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("saved SM is added under Drafts list case is not working  fine");
		}
		logger.info("Verified reord under drafts list");
		delete_draft();

	}

	public void tc_084_Functionality_of_Discard_button_in_Confermation_popup_with_AllData() throws Exception

	{
		waits(2);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Templates_tab, "click on Templets tab").click();
		logger.info("Clicked  on Templets tab");
		waits(5);
		logger.info("waits for 5 seconds");

		button(SM.popup_Discard_button, "click on Discard button in popup").click();
		logger.info("Clicked on discard button in popup page");
		waits(5);
		logger.info("waits for 5 seconds");

		SM.CreateTemplate_button.isEnabled();
		System.out.println(SM.CreateTemplate_button.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.CreateTemplate_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out
					.println("popup closed and Navigated to main Templates page and verified 'Create Templet button '");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case passed");

		} else {

			Assert.assertEquals(true, verifyElementIsEnabled(SM.CreateTemplate_button));
			System.out.println("popup didn't closed ");
			TestUtil.takeScreenshotAtEndOfTest("popup closed by click on discard button case Failed");
		}

		logger.info("Verified popupis closed and navigated to Templets page");
		waits(2);

	}

	public void tc_100_FeatureTime_Date_Warning_message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Calendar_icon, "click on calendar icon").click();
		logger.info("Clicked on Calendar icon");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.left_arrow_for_month_in_calander, "Click on < icon for previous month").click();
		logger.info("Selected previous month");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Previous_date_in_calendar_popup, "select on  previous date").click();
		logger.info("selected the previous date");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button in calender popup");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		SM.Error_text_message_for_Date_Time_in_CSM.getText();
		System.out.println(SM.Error_text_message_for_Date_Time_in_CSM.getText());
		waits(2);
		if (SM.Error_text_message_for_Date_Time_in_CSM.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(SM.Error_text_message_for_Date_Time_in_CSM.getText(),
					"Please select a date and time in the future.", "Please select a date and time in the future.");
			System.out.println("Error text message for previous date and Time is working as expected");
			TestUtil.takeScreenshotAtEndOfTest("Error text message for previous Date and Time case is passed");
		} else {
			Assert.assertEquals(SM.Error_text_message_for_Date_Time_in_CSM.getText(),
					"Please select a date and time in the future.", "");
			System.out.println("Error text message for previous date and Time is not working as expected");
			TestUtil.takeScreenshotAtEndOfTest("Error text message for previous Date and Time case is Failed");
		}

		logger.info("Verified the error text message for previous date and time");
		waits(2);

	}

	public void tc_101_Functionality_of_Deselecting_Recurring_button() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();

		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Deselected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");

		if (SM.recurring_check_button.isSelected()) {

			System.out.println("Recurring check button is selected and all related options are open");
			TestUtil.takeScreenshotAtEndOfTest(
					"Recurring check button is deselected and all related options are closed case is Fail");
		} else {

			System.out.println("Recurring check button is deselected and all related options are closed");
			TestUtil.takeScreenshotAtEndOfTest(
					"Recurring check button is deselected and all related options are closed case is pass");
		}

		logger.info("Verified Recurring check button is deselected and all related options are hide");
		waits(2);

	}

	public void tc_103_Functionality_of_Recurring_Check_button() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();
		SM.Repeat_label_under_Recurring.getText();
		System.out.println(SM.Repeat_label_under_Recurring.getText());
		waits(2);
		if (SM.Repeat_label_under_Recurring.getText().equalsIgnoreCase("Repeat")) {
			Assert.assertEquals(SM.Repeat_label_under_Recurring.getText(), "Repeat", "Repeat");
			System.out.println("Recurring check button is selected and all related options are available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Recurring check button is selected and all related options are available case is pass");
		} else {
			Assert.assertEquals(SM.Repeat_label_under_Recurring.getText(), "Repeat", "Repeat");
			System.out.println("Recurring check button is deselected and all related options are closed");
			TestUtil.takeScreenshotAtEndOfTest(
					"Recurring check button is selected and all related options are available case is Fail");
		}

		logger.info("Verified  all options related to recurring is available");

	}

	public void tc_104_Availability_of_daily_once_recurring_option() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());
		for (int i = 0; i <= allOptions.size(); i++) {
			if (allOptions.get(i).getText().contains("Daily at this time")) {
				allOptions.get(i).click();
				waits(2);
				break;
			}

		}

		logger.info("Selected Daily at this time option");

	}

	// not completed
	public void tc_107_Verifying_Toster_message_for_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Toaster_message_Reccuring_SM_record.getText();
		System.out.println(SM.Toaster_message_Reccuring_SM_record.getText());

		if (verifyElementIsEnabled(SM.Toaster_message_Reccuring_SM_record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Toaster_message_Reccuring_SM_record));
			System.out.println("Verified the tostar message for recurring record");
			TestUtil.takeScreenshotAtEndOfTest("Verified the tostar message for recurring record");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Toaster_message_Reccuring_SM_record));
			System.out.println("Verified the tostar message for recurring record is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified the tostar message for recurring record case is not working  fine");
		}
		logger.info("Verified toaster message for created Reccuring SM");
		DeleteSM();

	}

	public void tc_109_Verifying_UI_error_for_previous_Date() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		button(SM.Calender_icon_under_reccuring, "Click on calender ").click();
		logger.info("Clicked on calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Left_arrow_button_in_End_Date_calender, "click on < button").click();
		button(SM.Left_arrow_button_in_End_Date_calender, "click on < button").click();
		logger.info("Selected previous month");
		waits(2);
		logger.info("waits for 2 sec");

		SM.Previous_date_for_End_Date_calender.click();
		logger.info("selected the previous date");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on Secheduled message button");
		waits(2);
		logger.info("waits for 2 sec");

		SM.validation_error_message_for_Date_in_EndDate_calender.getText();
		System.out.println(SM.validation_error_message_for_Date_in_EndDate_calender.getText());

		if (SM.validation_error_message_for_Date_in_EndDate_calender.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(SM.validation_error_message_for_Date_in_EndDate_calender.getText(),
					"Please select a date in the future.", "Please select a date in the future.");
			System.out.println("Verified validation error message for previous date");
			TestUtil.takeScreenshotAtEndOfTest("Verified validation error message for previous date case is passed");
		} else {

			Assert.assertEquals(SM.validation_error_message_for_Date_in_EndDate_calender.getText(),
					"Please select a date in the future.", "");
			System.out.println("Verified validation error message for previous date is not available");
			TestUtil.takeScreenshotAtEndOfTest("Verified validation error message for previous date is failed");
		}
		logger.info("Verified the error message for previous date for end date");
		waits(2);

	}

	public void tc_108_Verifying_UI_error_for_previous_Date_Time() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Calendar_icon, "Click on calender ").click();
		logger.info("Clicked on calender");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Downarrow_button_Time_hour.click();
		SM.Downarrow_button_Time_hour.click();
		logger.info("selected the past time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "click on set button").click();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 30 sec");

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {
			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {
				allOptions.get(i).click();
				break;
			}
		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on Secheduled message button");
		waits(2);
		logger.info("waits for 5 sec");

		SM.Validation_message_for_previous_date_time.getText();
		System.out.println(SM.Validation_message_for_previous_date_time.getText());

		if (SM.Validation_message_for_previous_date_time.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "Please select a date and time in the future.");
			System.out.println("Verified validation error message for previous dateand time");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified validation error message for previous date and time case is passed");
		} else {

			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "");
			System.out.println("Verified validation error message for previous date and time is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified validation error message for previous date and time is failed");
		}
		logger.info("Verified the error message for previous date and time ");

	}

	public void tc_111_Verifying_UI_error_for_previous_Date_Time() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Calendar_icon, "Click on calender ").click();
		logger.info("Clicked on calender");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Downarrow_button_Time_hour.click();
		SM.Downarrow_button_Time_hour.click();
		logger.info("selected the past time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "click on set button").click();
		logger.info("Clicked on Set button");
		Click_Recurring_button();

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Monthly on the (Date) option");
		waits(2);
		logger.info("waits for 30 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on Secheduled message button");
		waits(2);
		logger.info("waits for 2 sec");

		SM.Validation_message_for_previous_date_time.getText();
		System.out.println(SM.Validation_message_for_previous_date_time.getText());

		if (SM.Validation_message_for_previous_date_time.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "Please select a date and time in the future.");
			System.out.println("Verified validation error message for previous dateand time");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified validation error message for previous date and time case is passed");
		} else {

			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "");
			System.out.println("Verified validation error message for previous date and time is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified validation error message for previous date and time is failed");
		}
		logger.info("Verified the error message for previous date and time ");

	}

	public void tc_112_Verifying_UI_error_for_previous_Date() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Date) option");
		button(SM.Calender_icon_under_reccuring, "Click on calender").click();
		logger.info("Clicked on calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Left_arrow_button_in_End_Date_calender, "click on < button").click();
		logger.info("Selected previous month");
		button(SM.Left_arrow_button_in_End_Date_calender, "click on < button").click();
		logger.info("Selected previous month");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Previous_date_for_End_Date_calender.click();
		logger.info("selected the previous date");
		waits(2);
		logger.info("waits for 5 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on Secheduled message button");
		waits(2);
		logger.info("waits for 5 sec");

		SM.validation_error_message_for_Date_in_EndDate_calender.getText();
		System.out.println(SM.validation_error_message_for_Date_in_EndDate_calender.getText());

		if (SM.validation_error_message_for_Date_in_EndDate_calender.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(SM.validation_error_message_for_Date_in_EndDate_calender.getText(),
					"Please select a date in the future.", "Please select a date in the future.");
			System.out.println("Verified validation error message for previous date");
			TestUtil.takeScreenshotAtEndOfTest("Verified validation error message for previous date case is passed");
		} else {

			Assert.assertEquals(SM.validation_error_message_for_Date_in_EndDate_calender.getText(),
					"Please select a date in the future.", "");
			System.out.println("Verified validation error message for previous date is not available");
			TestUtil.takeScreenshotAtEndOfTest("Verified validation error message for previous date is failed");
		}
		logger.info("Verified the error message for previous date for end date");
		waits(2);

	}

	public void tc_116_verify_error_message_for_contacts_for_TollFree() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab Button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Tollfree(1)")) {

				allOptions.get(i).click();
				break;
			}
		}

		logger.info("Seleted Tollfree department");
		Enter_Contact_number();
		SM.Message_sent_to_textbox.isEnabled();
		System.out.println(SM.Message_sent_to_textbox.isEnabled());

		if (verifyElementIsEnabled(SM.Message_sent_to_textbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Message_sent_to_textbox));
			textbox(SM.Message_sent_to_textbox).isEnabled();
			System.out.println("Message sent text field is  displayed with added contact");
			TestUtil.takeScreenshotAtEndOfTest("Message text box should not available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Message_sent_to_textbox));
			System.out.println("Message sent text field is not available");
			TestUtil.takeScreenshotAtEndOfTest("Message text box should not available case failed ");
		}

		logger.info(
				"Verified Member sent to text field is displays with added contact number  and not allowing to add another contact for Tollfree department");
	}

	public void tc_114_Verifying_UI_error_for_previous_Date_Time() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		button(SM.Calendar_icon, "Click on calender ").click();
		logger.info("Clicked on calender");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Downarrow_button_Time_hour.click();
		SM.Downarrow_button_Time_hour.click();
		logger.info("selected the past time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "click on set button").click();
		Click_Recurring_button();

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Yearly on (Date)(Month) option");
		waits(2);
		logger.info("waits for 30 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on Secheduled message button");
		waits(2);
		logger.info("waits for 5 sec");

		SM.Validation_message_for_previous_date_time.getText();
		System.out.println(SM.Validation_message_for_previous_date_time.getText());

		if (SM.Validation_message_for_previous_date_time.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "Please select a date and time in the future.");
			System.out.println("Verified validation error message for previous dateand time");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified validation error message for previous date and time case is passed");
		} else {

			Assert.assertEquals(SM.Validation_message_for_previous_date_time.getText(),
					"Please select a date and time in the future.", "");
			System.out.println("Verified validation error message for previous date and time is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified validation error message for previous date and time is failed");
		}
		logger.info("Verified the error message for previous date and time ");

	}

	public void tc_115_Verifying_UI_error_for_previous_Date() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM Tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Yearly on (Date) (Month) option");
		button(SM.Calender_icon_under_reccuring, "Click on calender").click();
		logger.info("Clicked on calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Down_arrow_icon_for_Month_and_Year_under_Recurring, "Click on down arrow for year").click();
		logger.info("Clicked on down arrow button of years");
		button(SM.Previous_Year_under_recurring, "Select previous year").click();
		logger.info("Selected previous year");
		button(SM.Month_for_previous_year_under_recurring, "Select previous month").click();
		logger.info("Selected previous month");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Previous_date_for_End_Date_calender.click();
		logger.info("selected the previous date");
		waits(2);
		logger.info("waits for 5 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on Secheduled message button");
		waits(2);
		logger.info("waits for 5 sec");

		SM.validation_error_message_for_Date_in_EndDate_calender.getText();
		System.out.println(SM.validation_error_message_for_Date_in_EndDate_calender.getText());

		if (SM.validation_error_message_for_Date_in_EndDate_calender.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(SM.validation_error_message_for_Date_in_EndDate_calender.getText(),
					"Please select a date in the future.", "Please select a date in the future.");
			System.out.println("Verified validation error message for previous date");
			TestUtil.takeScreenshotAtEndOfTest("Verified validation error message for previous date case is passed");
		} else {

			Assert.assertEquals(SM.validation_error_message_for_Date_in_EndDate_calender.getText(),
					"Please select a date in the future.", "");
			System.out.println("Verified validation error message for previous date is not available");
			TestUtil.takeScreenshotAtEndOfTest("Verified validation error message for previous date is failed");
		}
		logger.info("Verified the error message for previous date for end date");

	}

	public void tc_117_Verify_all_related_department_records_under_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on SM tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		textbox(SM.select_department_DD).click();
		logger.info("clicked on select department DD");
		waits(2);
		logger.info("waits for 5 seconds");

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Tollfree(1)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("sample is selected under select department DD");
		Click_Recurring_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.select_department_DD).click();
		logger.info("clicked on select department DD");
		waits(2);
		logger.info("waits for 2 seconds");

		List<WebElement> allOption = driver.findElements(By.xpath("//select[1]//option"));
		System.out.println(allOption.size());

		for (int i = 0; i <= allOption.size() - 1; i++) {

			if (allOption.get(i).getText().contains("Tollfree(1)")) {

				allOption.get(i).click();
				break;
			}

		}

		logger.info("Selected sample department");
		button(SM.SM_Upcoming_tab, "click on upcomming tab button").click();
		waits(2);
		SM.Recurring_Record_under_upcoming_list.isEnabled();
		System.out.println(SM.Recurring_Record_under_upcoming_list.isEnabled());

		if (verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			textbox(SM.Recurring_Record_under_upcoming_list).isEnabled();
			System.out.println("Record related to selected department is available under list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record related to selected department is available under list is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			System.out.println("Record related to selected department is not available under list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record related to selected department is available under list case failed ");
		}

		logger.info("Verified Record related to selected department is available under list");
		DeleteSM();

	}

	public void tc_123_Verifying_text_message_for_no_records() throws Exception

	{

		waits(2);
		List<WebElement> allOption = driver.findElements(By.xpath("//select[1]//option"));
		System.out.println(allOption.size());

		for (int i = 0; i <= allOption.size() - 1; i++) {

			if (allOption.get(i).getText().contains("Auto2")) {

				allOption.get(i).click();
				break;
			}

		}
		logger.info("Selected Auto2 department");
		waits(2);
		button(SM.SM_selected_Upcoming_button, "click on upcoming button which is already selected").click();
		waits(2);
		SM.text_message_for_no_upcoming_records.getText();
		System.out.println(SM.text_message_for_no_upcoming_records.getText());

		if (SM.text_message_for_no_upcoming_records.getText()
				.equalsIgnoreCase("All your scheduled texts will appear here.")) {
			Assert.assertEquals(SM.text_message_for_no_upcoming_records.getText(),
					"All your scheduled texts will appear here.", "All your scheduled texts will appear here.");
			System.out.println("Verified text message for no upcoming records");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no upcoming records case is passed");
		} else {

			Assert.assertEquals(SM.text_message_for_no_upcoming_records.getText(),
					"All your scheduled texts will appear here.", "");
			System.out.println("Verified text message for no upcoming records is not exist");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no upcoming records is failed");
		}
		logger.info("Verified text message for no upcoming records");

	}

	public void tc_124_Verifying_text_message_for_no_draft_records() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);

		List<WebElement> allOption = driver.findElements(By.xpath("//select[1]//option"));
		System.out.println(allOption.size());

		for (int i = 0; i <= allOption.size() - 1; i++) {

			if (allOption.get(i).getText().contains("sample")) {

				allOption.get(i).click();
				break;
			}

		}
		logger.info("Selected sample department");
		button(SM.SM_Drafts_Button, "click on Drafts").click();
		logger.info("Clicked on Drafts");

		SM.text_message_for_no_Drafts_records.getText();
		System.out.println(SM.text_message_for_no_Drafts_records.getText());

		if (SM.text_message_for_no_Drafts_records.getText()
				.equalsIgnoreCase("You can save your draft scheduled texts.")) {
			Assert.assertEquals(SM.text_message_for_no_Drafts_records.getText(),
					"You can save your draft scheduled texts.", "You can save your draft scheduled texts.");
			System.out.println("Verified text message for no Drafts records");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no Drafts records case is passed");
		} else {

			Assert.assertEquals(SM.text_message_for_no_upcoming_records.getText(),
					"You can save your draft scheduled texts.", "");
			System.out.println("Verified text message for no Drafts records is not exist");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no Drafts records is failed");
		}
		logger.info("Verified text message for no Drafts records");

	}

	public void tc_125_Verifying_record_under_Upcoming_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		waits(2);
		logger.info("waits for 2 sec");
		driver.findElement(By.xpath("//button[@id='schedule_SaveButton']")).click();

		waits(5);
		button(SM.Templates_tab, "click on templets tab").click();
		waits(5);
		button(SM.Scheduled_Message_Tab_button, "click on SM tab").click();
		waits(2);
		button(SM.SM_selected_Upcoming_button, "click on upcoming button which is already selected").click();
		waits(2);
		DeleteSM();
		waits(5);
		SM.text_message_for_no_upcoming_records.getText();
		System.out.println(SM.text_message_for_no_upcoming_records.getText());

		if (SM.text_message_for_no_upcoming_records.getText()
				.equalsIgnoreCase("All your scheduled texts will appear here.")) {
			Assert.assertEquals(SM.text_message_for_no_upcoming_records.getText(),
					"All your scheduled texts will appear here.", "All your scheduled texts will appear here.");
			System.out.println("Verified text message for no upcoming records");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no upcoming records case is passed");
		} else {

			Assert.assertEquals(SM.text_message_for_no_upcoming_records.getText(),
					"All your scheduled texts will appear here.", "");
			System.out.println("Verified text message for no upcoming records is not exist");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no upcoming records is failed");
		}

		logger.info("Verified Record  is not available under list");

	}

	public void tc_126_Verifying_recurring_icon_under_upcoming_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		SM.recurring_icon_under_list.isEnabled();
		System.out.println(SM.recurring_icon_under_list.isEnabled());

		if (verifyElementIsEnabled(SM.recurring_icon_under_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.recurring_icon_under_list));
			textbox(SM.recurring_icon_under_list).isEnabled();
			System.out.println("Recurring icon is enable under upcoming list");
			TestUtil.takeScreenshotAtEndOfTest("Recurring icon is enable under upcoming list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.recurring_icon_under_list));
			System.out.println("Recurring icon is not available under upcoming list");
			TestUtil.takeScreenshotAtEndOfTest("Recurring icon is enable under upcoming list case failed ");
		}

		logger.info("Verified Recurring icon is enable under upcoming list");
		DeleteSM();

	}

	public void tc_127_Verifying_navigation_to_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Click_Recurring_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(5);
		logger.info("waits for 2 sec");

		button(SM.recurring_manage_button_under_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");

		SM.Manage_SM_Breadcrum_button.isEnabled();
		System.out.println(SM.Manage_SM_Breadcrum_button.isEnabled());

		if (verifyElementIsEnabled(SM.Manage_SM_Breadcrum_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Manage_SM_Breadcrum_button));
			textbox(SM.Manage_SM_Breadcrum_button).isEnabled();
			System.out.println("Manage SM breadcrum button is enable in manage SM page");
			TestUtil.takeScreenshotAtEndOfTest("Manage SM breadcrum button is enable in manage SM page case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Manage_SM_Breadcrum_button));
			System.out.println("Manage SM breadcrum button is enable in manage SM page");
			TestUtil.takeScreenshotAtEndOfTest("Manage SM breadcrum button is enable in manage SM page case failed ");
		}

		logger.info("Verified Manage SM breadcrum button is enable in manage SM page");

		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clickedondelete SM button");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm delete SM button").click();
		logger.info("Clicked on Confirm Delete SM button");

	}

	public void tc_133_Verifying_records_under_Drafts_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified records under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified records under Drafts list case failed ");
		}

		logger.info("Verified not-completed records under Drafts list");

		delete_draft();

	}

	// Drafts cases WF starts from here//

	public void tc_134_Functionality_of_Delete_button_under_Drafts_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		waits(2);
		SM.Text_in_Drafts_conformation_popup.isEnabled();
		System.out.println(SM.Text_in_Drafts_conformation_popup.isEnabled());

		if (verifyElementIsEnabled(SM.Text_in_Drafts_conformation_popup)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Text_in_Drafts_conformation_popup));
			textbox(SM.Text_in_Drafts_conformation_popup).isEnabled();
			System.out.println("Verified the drafts conformation popup is opened");
			TestUtil.takeScreenshotAtEndOfTest("Verified the drafts conformation popup is opened case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Text_in_Drafts_conformation_popup));
			System.out.println("Verified the drafts conformation popup is opened");
			TestUtil.takeScreenshotAtEndOfTest("Verified the drafts conformation popup is opened case failed ");
		}

		logger.info("Drafts conformation popup is opened with options Discard and Cancel");
		waits(2);
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");
		waits(2);

	}

	public void tc_135_Verify_deleted_record_under_Drafts_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		waits(3);
		logger.info("Drafts conformation popup is opened with options Discard and Cancel");
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");
		waits(4);
		SM.text_message_for_no_Drafts_records.getText();
		System.out.println(SM.text_message_for_no_Drafts_records.getText());

		if (SM.text_message_for_no_Drafts_records.getText()
				.equalsIgnoreCase("You can save your draft scheduled texts.")) {
			Assert.assertEquals(SM.text_message_for_no_Drafts_records.getText(),
					"You can save your draft scheduled texts.", "You can save your draft scheduled texts.");
			System.out.println("Verified text message for no Drafts records");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no Drafts records case is passed");
		} else {

			Assert.assertEquals(SM.text_message_for_no_upcoming_records.getText(),
					"You can save your draft scheduled texts.", "");
			System.out.println("Verified text message for no Drafts records is not exist");
			TestUtil.takeScreenshotAtEndOfTest("Verified text message for no Drafts records is failed");
		}
		logger.info("Verified deleted records under Drafts list is not available");
		waits(2);

	}

	public void tc_136_Functionality_of_Cancel_button_in_conformation_popup() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		waits(3);
		logger.info("Drafts conformation popup is opened with options Discard and Cancel");
		waits(4);
		button(SM.Cancel_button_in_conformation_Drafts_popup, "Clicked on Cancel button in popup").click();
		logger.info("Clicked on Cancel button button in popup page");
		logger.info("popup is closed and redirected to SM page");
		waits(4);
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified records under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified records under Drafts list case failed ");
		}

		logger.info("Verified records under Drafts list is not deleted");

		delete_draft();

	}

	public void tc_137_Functionality_of_Manage_button_under_Drafts_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");

		logger.info("Verified Navigation to manage page by click on manage button");
		waits(2);
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clickedondelete SM button");
		waits(2);
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm delete SM button").click();
		logger.info("Clicked on Confirm Delete SM button");
		waits(2);

	}

	public void tc_138_Draft_save_to_Upcoming_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing2");
		logger.info("Entered textinto message textbox field");
		waits(2);
		button(SM.recurring_check_button, "Click on reccuring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		button(SM.Calendar_icon, "Click on Calender").click();
		logger.info("Clickedon Calender");
		button(SM.Uparrow_button_Time_hour, "Click on uparrow button").click();
		logger.info("Clicked on uparrow of time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on  Set button").click();
		logger.info("Clicked on Set button");

		waits(2);
		logger.info("waits for 2 sec");

		WebElement element = driver.findElement(By.xpath("//button[@id='schedule_SaveButton']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		waits(3);
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());

		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			textbox(SM.Upcoming_Record).isEnabled();
			System.out.println("Record  is available under list");
			TestUtil.takeScreenshotAtEndOfTest("Record  is available under list is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record  is not available under list");
			TestUtil.takeScreenshotAtEndOfTest("Record  is available under list case failed ");
		}

		logger.info("Verified Record  is available under list");
		DeleteSM();

	}

	public void tc_139_Verify_updated_record_under_Drafts_list() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		button(SM.SaveDraft_button_in_Manage_page, "Click on Save Draft button").click();
		logger.info("Clicked on Save Draft button in manage page");
		waits(2);
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_140_Functionality_of_SaveDraft_button_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		button(SM.SaveDraft_button_in_Manage_page, "Click on Save Draft button").click();
		logger.info("Clicked on Save Draft button in manage page");
		waits(2);
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_141_Functionality_of_Breadscrum_link_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(4);
		logger.info("waits for 4 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		SM.text_in_breadcrum_conformation_popup.getText();
		System.out.println("SM.text_in_breadcrum_conformation_popup.getText()");

		if (SM.text_in_breadcrum_conformation_popup.getText()
				.equalsIgnoreCase("Do you want to save scheduled text ?")) {
			Assert.assertEquals(SM.text_in_breadcrum_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "Do you want to save scheduled text ?");
			System.out.println("Verified conformation popup for Breadcrum link");
			TestUtil.takeScreenshotAtEndOfTest("TC_141 scenario is passed");
		} else {
			Assert.assertEquals(SM.text_in_breadcrum_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "");
			System.out.println("Verified conformation popup for Breadcrum link");
			TestUtil.takeScreenshotAtEndOfTest("TC_141 scenario is faied");
		}

		button(SM.Breadcrum_popup_discard_button, "Click on Discard button").click();
		logger.info("Clicked on Discard button in breadcrum conformation popup");
		waits(2);
		logger.info("waits for 2sec");
		delete_draft();
	}

	public void tc_142_Functionality_of_Save_button_in_conformation_popup() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(4);
		logger.info("waits for 4 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		waits(4);
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(4);
		logger.info("waits for 4 sec");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_143_Functionality_of_Breadscrum_link_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(3);
		logger.info("waits for 3 seconds");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		SM.text_in_breadcrum_conformation_popup.getText();
		System.out.println("SM.text_in_breadcrum_conformation_popup.getText()");

		if (SM.text_in_breadcrum_conformation_popup.getText()
				.equalsIgnoreCase("Do you want to save scheduled text ?")) {
			Assert.assertEquals(SM.text_in_breadcrum_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "Do you want to save scheduled text ?");
			System.out.println("Verified conformation popup for Breadcrum link");
			TestUtil.takeScreenshotAtEndOfTest("TC_143 scenario is passed");
		} else {
			Assert.assertEquals(SM.text_in_breadcrum_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "");
			System.out.println("Verified conformation popup for Breadcrum link");
			TestUtil.takeScreenshotAtEndOfTest("TC_143 scenario is faied");
		}

		button(SM.Breadcrum_popup_discard_button, "Click on Discard button").click();
		logger.info("Clicked on Discard button in breadcrum conformation popup");
		waits(4);
		logger.info("waits for 4sec");
		delete_draft();
		waits(2);

	}

	public void tc_144_Functionality_of_Save_button_in_conformation_popup() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		waits(4);
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_145_Functionality_of_Save_button_in_conformation_popup_for_other_Link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 10 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 10 seconds");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Templates_tab, "Click on Templetes tab").click();
		logger.info("Clicked on Templet tab");
		waits(3);
		logger.info("waits for 3 sec");

		logger.info("conformation popup is opened with Save and Discard buttons");
		waits(4);
		button(SM.Save_button_in_other_Link_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on Save button in conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(3);
		logger.info("waits for 3 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_146_Functionality_of_Delete_button_in_Draft_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Click on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Click on Confirm Delete SM button").click();
		logger.info("Clicked on Confirm Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");

		SM.text_message_for_no_Drafts_records.isEnabled();
		System.out.println(SM.text_message_for_no_Drafts_records.isEnabled());

		if (verifyElementIsEnabled(SM.text_message_for_no_Drafts_records)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.text_message_for_no_Drafts_records));
			textbox(SM.text_message_for_no_Drafts_records).isEnabled();
			System.out.println("Verified no records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified deleted record under Drafts list is not available case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.text_message_for_no_Drafts_records));
			System.out.println("Verified no records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified deleted record under Drafts list is not available case failed ");
		}

	}

	public void tc_147_Functtionality_by_Click_on_other_Links_in_Draft_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Templates_tab, "Click on Templetes tab").click();
		logger.info("Clicked on Templet tab");
		waits(3);
		logger.info("waits for 3 sec");

		SM.text_in_other_Link_conformation_popup.getText();
		System.out.println(SM.text_in_other_Link_conformation_popup.getText());

		if (SM.text_in_other_Link_conformation_popup.getText()
				.equalsIgnoreCase("Do you want to save scheduled text ?")) {
			Assert.assertEquals(SM.text_in_other_Link_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "Do you want to save scheduled text ?");
			System.out.println("Verified conformation popup for other links");
			TestUtil.takeScreenshotAtEndOfTest("TC_147 scenario is passed");
		} else {
			Assert.assertEquals(SM.text_in_other_Link_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "");
			System.out.println("Verified conformation popup for other links");
			TestUtil.takeScreenshotAtEndOfTest("TC_147 scenario is faied");
		}

		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_other_Link_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on Save button in conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(3);
		logger.info("waits for 3 seconds");

		delete_draft();

	}

	public void tc_148_Functtionality_of_Save_button_by_Click_on_other_Links() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Templates_tab, "Click on Templetes tab").click();
		logger.info("Clicked on Templet tab");
		waits(3);
		logger.info("waits for 3 sec");

		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_other_Link_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on Save button in conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_149_Functtionality_of_Discard_button_by_Click_on_other_Links() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Templates_tab, "Click on Templetes tab").click();
		logger.info("Clicked on Templet tab");
		waits(4);
		logger.info("waits for 4 sec");

		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Discard_button_in_other_Link_conformation_popup, "Click on Discard button").click();
		logger.info("Clicked on discard button in conformation popup");
		waits(4);
		logger.info("waits for 4 sec");
		logger.info("popup is closed and stayed in templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");

		delete_draft();

	}

	public void tc_150_Functionality_of_other_links_from_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(3);
		button(SM.Templates_tab, "Click on Templetes tab").click();
		logger.info("Clicked on Templet tab");
		waits(3);
		logger.info("waits for 3 sec");

		logger.info("conformation popup is opened with Save and Discard buttons");
		SM.text_in_other_Link_conformation_popup.getText();
		System.out.println(SM.text_in_other_Link_conformation_popup.getText());

		if (SM.text_in_other_Link_conformation_popup.getText()
				.equalsIgnoreCase("Do you want to save scheduled text ?")) {
			Assert.assertEquals(SM.text_in_other_Link_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "Do you want to save scheduled text ?");
			System.out.println("Verified conformation popup for other links");
			TestUtil.takeScreenshotAtEndOfTest("TC_150 scenario is passed");
		} else {
			Assert.assertEquals(SM.text_in_other_Link_conformation_popup.getText(),
					"Do you want to save scheduled text ?", "not");
			System.out.println("Verified conformation popup for Breadcrum link");
			TestUtil.takeScreenshotAtEndOfTest("TC_150 scenario is faied");
		}
		waits(3);
		button(SM.Discard_button_in_other_Link_conformation_popup, "Click on Discard button").click();
		logger.info("Clicked on discard button in conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("popup is closed and stayed in templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");

		delete_draft();

	}

	public void tc_151_Functionality_of_Save_button_in_other_links_conformation_popup() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on Recurring check button");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");

		WebElement element = driver.findElement(By.id("templatesTab"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Templet tab");
		waits(3);
		logger.info("waits for 3 sec");

		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_other_Link_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on Save button in conformation popup");
		waits(4);
		logger.info("waits for 4 sec");
		logger.info("popup is closed and stayed in templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_152_Functionality_of_Discard_button_in_other_links_conformation_popup() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("clicked on SMbutton");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
		logger.info("Clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on Recurring check button");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		WebElement element = driver.findElement(By.id("templatesTab"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Templet tab");
		waits(3);
		logger.info("waits for 3 sec");

		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Discard_button_in_other_Link_conformation_popup, "Click on Discard button").click();
		logger.info("Clicked on Discard button in conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("popup is closed and stayed in templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Clicked on SM tab button");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified records under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited record under Drafts list case failed ");
		}

		logger.info("Verified edited record under Drafts list is available");

		delete_draft();

	}

	public void tc_158_UI_Error_message_for_max_Contacts() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");

		String arr[] = { "4255058829", "7273039977", "5555555555", "5555555550", "5555555552", "5555555553",
				"5555555554", "5555555556" };

		for (int i = 0; i <= 7; i++) {

			waits(2);
			logger.info("waits for 2 sec");
			textbox(SM.Message_sent_to_textbox).sendKeys(arr[i]);
			logger.info("Entered contact number");
			waits(2);
			logger.info("waits for 2 seconds");
			driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
			logger.info("Added contact number to message sent textbox");
			waits(2);
			logger.info("waits for 2 seconds");

		}
		waits(2);
		SM.Max_9_Contact_allowed_text_message.isEnabled();
		waits(2);
		System.out.println(SM.Max_9_Contact_allowed_text_message.isEnabled());

		if (verifyElementIsEnabled(SM.Max_9_Contact_allowed_text_message)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Max_9_Contact_allowed_text_message));
			System.out.println("Verified errortext message for more than 9 contacts");
			TestUtil.takeScreenshotAtEndOfTest("Verified errortext message for more than 9 contacts case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Max_9_Contact_allowed_text_message));
			System.out.println("Verified errortext message for more than 9 contacts");
			TestUtil.takeScreenshotAtEndOfTest("Verified errortext message for more than 9 contacts case failed ");
		}

		logger.info("Verified errortext message for more than 9 contacts is available");

		WebElement element1 = driver.findElement(By.id("schedule_SaveButton"));
		jse2.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("Verified Recurring icon is enable under upcoming list");
		DeleteSM();
	}

	public void tc_159_verify_error_message_for_multiple_contacts_for_TollFree() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Tollfree(1)")) {

				allOptions.get(i).click();
				break;
			}
		}

		logger.info("Seleted Tollfree department");
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);

		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

//		button(SM.SM_Drafts_Button, "Click on Drafts button").click();
//		logger.info("Clicked on Drafts");
//		waits(2);
//		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");

		SM.Message_sent_to_textbox.isEnabled();
		System.out.println(SM.Message_sent_to_textbox.isEnabled());

		if (verifyElementIsEnabled(SM.Message_sent_to_textbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Message_sent_to_textbox));
			textbox(SM.Message_sent_to_textbox).isEnabled();
			System.out.println("Message sent text field is not displayed with added contact");
			TestUtil.takeScreenshotAtEndOfTest("Message text field should not available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Message_sent_to_textbox));
			System.out.println("Message sent text field is  available");
			TestUtil.takeScreenshotAtEndOfTest("Message text field should not available case failed ");
		}

		logger.info("Verified 'Member sent to' text field is not available to add another contact  for Tollfree");

		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);
	}

	public void tc_160_Functtionality_of_Delete_icon_of_contact_in_Manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();

		Enter_Contact_number();
		Enter_message();

		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");

		button(SM.Cancel_Icon_For_Contact_in_Manage_page, "Click on Delete icon for contact").click();
		logger.info("Contact is deleted");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");

		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_161_Validation_message_for_1000_char_in_message_textbox_for_Edit_case() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(1);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		SM.Create_SM_Message_textbox.sendKeys(Keys.CONTROL + "a");
		SM.Create_SM_Message_textbox.sendKeys(Keys.DELETE);
		waits(5);
		textbox(SM.Create_SM_Message_textbox).sendKeys(
				"shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared template creation checking in automation server shared tem");
		logger.info("entered text into the message textbox field");
		waits(10);
		logger.info("waits for 2 seconds");
		SM.Create_SM_Message_char_count.getText();
		logger.info("verified char count of the text");
		waits(2);
		logger.info("waits for 2 seconds");
		System.out.println(SM.Create_SM_Message_char_count.getText());
		logger.info("verified the char count of the text");
		waits(2);
		logger.info("waits for 2 seconds");

		if (SM.Create_SM_Message_char_count.getText().equalsIgnoreCase("1000/1000")) {
			Assert.assertEquals(SM.Create_SM_Message_char_count.getText(), "1000/1000", "1000/1000");
			System.out.println("1000/1000 char count is available");
			TestUtil.takeScreenshotAtEndOfTest("message text box should allow 1000 char case is passed");
		} else {

			Assert.assertEquals(SM.Create_SM_Message_char_count, "1000/1000");
			System.out.println("1000/1000 char count is not available");
			TestUtil.takeScreenshotAtEndOfTest("message text box should allow 1000 char case is failed");

		}
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");

		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_162_Allows_Max_10_attachments() throws Exception

	{

		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		waits(2);
		driver.findElement(By.xpath("//input[@id='appDateTimeId']")).click();
		waits(2);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(2);

		driver.findElement(By
				.xpath("(//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'])[2]"))
				.click();
		waits(2);
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		waits(2);
		logger.info("SM was created");
		button(SM.Manage_button_under_Upcomming_list, "click on Manage button").click();
		logger.info("Click on manage button and redirected to edit page");
		waits(2);

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");

		System.out.println("Allowed all 10  attachments");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element2 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element2);
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		waits(2);
		DeleteSM();

	}

	public void tc_166_Allows_Max_10_attachments() throws Exception

	{

		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		waits(2);
		driver.findElement(By.xpath("//input[@id='appDateTimeId']")).click();
		waits(2);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(1);
		driver.findElement(By.xpath("(//span[@class='owl-dt-control-button-content'])[3]")).click();
		waits(2);

		driver.findElement(By
				.xpath("(//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'])[2]"))
				.click();
		waits(2);
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		waits(2);
		logger.info("SM was created");
		button(SM.First_contact_manage_button, "click on manage button").click();
		waits(2);
		logger.info("clicked on manage button under upcomming list");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\samplecsv.csv");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");

		waits(5);
		logger.info("waits for 5 seconds");
		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");

		System.out.println("Allowed all mp4,mp3,jpg,csv,mov,pdf,doc,xls attachments");
		logger.info("Allowed mp3,mp4,jpg,csv,mov,pdf,doc,xls attachments");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element2 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element2);
		waits(2);
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		waits(2);
		DeleteSM();

	}

	public void tc_167_Verify_SM_scheduled_with_attachment() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");
		logger.info("Attachment was added to message");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element2 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element2);
		waits(2);
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		waits(2);
		SM.Created_SM_under_upcomming.isEnabled();

		if (verifyElementIsEnabled(SM.Created_SM_under_upcomming)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Created_SM_under_upcomming));
			textbox(SM.Created_SM_under_upcomming).isEnabled();
			System.out.println("created record is verified under upcoming list");
			TestUtil.takeScreenshotAtEndOfTest("SM was created with attachment and contact case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Created_SM_under_upcomming));
			System.out.println("created record under upcoming list is not available");
			TestUtil.takeScreenshotAtEndOfTest("SM was created with attachment and contact case failed ");
		}
		logger.info("SM record was created with attachment and contact");
		DeleteSM();

	}

	public void tc_168_FeatureTime_Date_Warning_message() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");

		logger.info("Attachment was added to message");
		waits(5);
		logger.info("wait for 5 sec");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Downarrow_button_Time_hour, "Click on downarrow button for Time").click();
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element2 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element2);
		waits(5);
		button(SM.create_SM_ScheduledMessage_button, "Click on SM button").click();
		logger.info("click on SM button");
		waits(2);
		SM.Error_text_message_for_Date_Time_in_CSM.getText();
		System.out.println(SM.Error_text_message_for_Date_Time_in_CSM.getText());

		if (SM.Error_text_message_for_Date_Time_in_CSM.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(SM.Error_text_message_for_Date_Time_in_CSM.getText(),
					"Please select a date and time in the future.", "Please select a date and time in the future.");
			System.out.println("error text message for previous time is available");
			TestUtil.takeScreenshotAtEndOfTest("Error text message for previous TimeZone case is pass");
		} else {
			Assert.assertEquals(SM.Error_text_message_for_Date_Time_in_CSM.getText(),
					"Please select a date and time in the future.", "Please select a date and time in the future.");
			System.out.println("Error text message for previous TimeZone is not available");
			TestUtil.takeScreenshotAtEndOfTest("Error text message for previous TimeZone case is Fail");
		}
		logger.info("Verified the error text message for previous time ");

	}

	public void tc_169_Functionality_of_SM_button_add_recipent_in_Manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);

		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Toster_message_for_edited_upcomming_record.getText();
		System.out.println(SM.Toster_message_for_edited_upcomming_record.getText());

		if (verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			textbox(SM.Toster_message_for_edited_upcomming_record).isEnabled();
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case failed ");
		}

		logger.info("Toster message for updated and saved record is displayed");
		DeleteSM();

	}

	public void tc_170_Verify_Edited_message() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(4);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("this message");
		logger.info("entered text into message textbox");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(4);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.text_in_message_field.getText();
		System.out.println(SM.text_in_message_field.getText());

		if (driver.getPageSource().contains("test message please ignorethis message")) {
			System.out.println("Edited text is available");
			TestUtil.takeScreenshotAtEndOfTest("Edited text message for Updated record is available case passed ");
		} else {
			System.out.println("Edited text is not available");
			TestUtil.takeScreenshotAtEndOfTest("Edited text message for Updated record is not available case passed ");
		}
		logger.info("Verified edited text in message text field");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_172_Functionality_of_Recurring_check_button_in_Manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element1 = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits  for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		SM.Repeat_label_under_Recurring.getText();
		System.out.println(SM.Repeat_label_under_Recurring.getText());
		waits(2);

		if (SM.Repeat_label_under_Recurring.getText().equalsIgnoreCase("Repeat")) {
			Assert.assertEquals(SM.Repeat_label_under_Recurring.getText(), "Repeat", "Repeat");
			System.out.println("Recurring check button is selected and all related options are available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Recurring check button is selected and all related options are available case is pass");
		} else {
			Assert.assertEquals(SM.Repeat_label_under_Recurring.getText(), "Repeat", "Repeat");
			System.out.println("Recurring check button is deselected and all related options are closed");
			TestUtil.takeScreenshotAtEndOfTest(
					"Recurring check button is selected and all related options are available case is Fail");
		}

		logger.info("Verified  all options related to recurring is available");

		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");

		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_173_verify_Recurring_icon_in_upcoming_list_for_Edit_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element1 = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");

		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits  for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.recurring_icon_under_list.isEnabled();
		System.out.println(SM.recurring_icon_under_list.isEnabled());
		waits(2);
		logger.info("waits for 2 sec");

		if (verifyElementIsEnabled(SM.recurring_icon_under_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.recurring_icon_under_list));
			textbox(SM.recurring_icon_under_list).isEnabled();
			System.out.println("Verified recurring icon for edit record under Upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified recurring icon for edit record under Upcomming list case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.recurring_icon_under_list));
			System.out.println("Verified recurring icon for edit record under Upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified recurring icon for edit record under Upcomming list case failed ");
		}

		logger.info("Verified recurring icon for edit record under Upcomming list");
		DeleteSM();

	}

	public void tc_174_Verify_Recurring_Record_as_normal_record_in_upcoming_list_for_Edit_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on recurring checkbutton");
		WebElement element1 = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");

		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits  for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits of 2 sec");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Recurring_Record_under_upcoming_list.isEnabled();
		System.out.println(SM.Recurring_Record_under_upcoming_list.isEnabled());
		waits(2);
		logger.info("waits for 2 sec");

		if (verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			textbox(SM.Recurring_Record_under_upcoming_list).isEnabled();
			System.out.println("Verified saved record under Upcomming list");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved record under Upcomming list case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Recurring_Record_under_upcoming_list));
			System.out.println("Verified saved record under Upcomming list");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved record under Upcomming list case failed ");
		}

		waits(2);
		logger.info("waits for 2 sec");
		logger.info("Verified saved record under Upcomming list");
		button(SM.Manage_button_under_Upcomming_list, "Click on manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_176_Verify_Edit_message_for_Saved_record_for_Edit_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		WebElement element1 = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");

		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits  for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits of 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("sample");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits of 2 sec");

		WebElement text = driver.findElement(By.xpath("//textarea[@id='schedule_message']"));
		String attribute = text.getAttribute("value");

		System.out.println("Edited text:" + attribute);

		if (attribute.contains("testingsample")) {

			System.out.println("Edited text for updated record is available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		else {

			System.out.println("Edited text for updated record is not available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		logger.info("Verified edited message for saved record");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_177_Verify_edited_attachment() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("Selected Daily at this time option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_178_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);

		textbox(SM.Cancel_Icon_For_Contact_in_Manage_page).click();
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 5 seconds");
		textbox(SM.Create_SM_Message_textbox).sendKeys("1");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Toster_message_for_edited_upcomming_record.getText();
		System.out.println(SM.Toster_message_for_edited_upcomming_record.getText());

		if (verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			textbox(SM.Toster_message_for_edited_upcomming_record).isEnabled();
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case failed ");
		}

		logger.info("Toster message for updated and saved record is displayed");
		waits(2);
		button(SM.SM_Drafts_Button, "click on drafts ").click();
		delete_draft();

	}

	public void tc_179_Verify_edited_record_information() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("Selected Daily at this time option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("Deleted the message");
		waits(2);
		logger.info("wait for 2 sec");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_180_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();

		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Toster_message_for_edited_upcomming_record.getText();
		System.out.println(SM.Toster_message_for_edited_upcomming_record.getText());

		if (verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			textbox(SM.Toster_message_for_edited_upcomming_record).isEnabled();
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case failed ");
		}
		logger.info("Toster message for updated and saved record is displayed");
		waits(2);
		DeleteSM();

	}

	public void tc_181_Verify_validation_message_for_previous_date() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("Selected Daily at this time option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.recurring_manage_button_under_list, "click on Manage button of record ").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waited for 2 sec");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.previous_Month, "click on previous month < icon").click();
		logger.info("click on previous month < button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Previous_date_in_calendar_popup, "select previous date").click();
		logger.info("selected previous date");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Set_button_in_calendar_popup, "click on set button").click();
		logger.info("click on set button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");

		SM.Error_text_message_for_Date_Time_in_CSM.isEnabled();
		if (verifyElementIsEnabled(SM.Error_text_message_for_Date_Time_in_CSM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Error_text_message_for_Date_Time_in_CSM));
			textbox(SM.Error_text_message_for_Date_Time_in_CSM).isEnabled();
			System.out.println("error message for date and time is displayed");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Error_text_message_for_Date_Time_in_CSM));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case failed ");
		}
		logger.info("verified validation message for previous date");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(5);
		logger.info("wait for 5 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_183_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list as general SM ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list as general SM case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list as general SM");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list as general SM case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		DeleteSM();

	}

	public void tc_184_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");

		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");

		SM.New_recipent_in_manage_page.isEnabled();
		System.out.println(SM.New_recipent_in_manage_page.isEnabled());
		if (verifyElementIsEnabled(SM.New_recipent_in_manage_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page is not available");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is not working  fine");
		}

		logger.info("Verified the added recipient in Manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_185_Verifying_Updated_text_message_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing1");
		logger.info("Edited text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		WebElement text = driver.findElement(By.xpath("//textarea[@id='schedule_message']"));
		String attribute = text.getAttribute("value");

		System.out.println("Edited text:" + attribute);

		if (attribute.contains("testingtesting1")) {

			System.out.println("Edited text for updated record is available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		else {

			System.out.println("Edited text for updated record is not available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		logger.info("Verified edited text message in manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_186_Verify_edited_attachment() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_187_Verifying_Updated_text_message_recipent_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 2 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing1");
		logger.info("Edited text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		WebElement text = driver.findElement(By.xpath("//textarea[@id='schedule_message']"));
		String attribute = text.getAttribute("value");

		System.out.println("Edited text:" + attribute);

		if (attribute.contains("testingtesting1")) {
			System.out.println("Edited text for updated record is available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		else {
			System.out.println("Edited text for updated record is not available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		logger.info("Verified edited text message in manage page");
		SM.New_recipent_in_manage_page.isEnabled();
		System.out.println(SM.New_recipent_in_manage_page.isEnabled());
		if (verifyElementIsEnabled(SM.New_recipent_in_manage_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page is not available");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is not working  fine");
		}

		logger.info("Verified the added recipient in Manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_188_Verify_edited_record_information() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}
		}
		logger.info("Selected Weekly on (Selected day) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("Deleted the message");
		waits(2);
		logger.info("wait for 2 sec");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_189_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}
		}
		logger.info("selected Weekly on (Select day) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		List<WebElement> allOption = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOption.size());

		for (int i = 0; i <= allOption.size(); i++) {

			if (allOption.get(i).getText().contains("Monthly on the (Date)")) {

				allOption.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Date) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Toster_message_for_edited_upcomming_record.getText();
		System.out.println(SM.Toster_message_for_edited_upcomming_record.getText());

		if (verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			textbox(SM.Toster_message_for_edited_upcomming_record).isEnabled();
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case failed ");
		}
		logger.info("Toster message for updated and saved record is displayed");
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");

		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_190_Verify_validation_message_for_previous_date() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());
		for (int i = 0; i <= allOptions.size(); i++) {
			if (allOptions.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Daily at this time option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.recurring_manage_button_under_list, "click on Manage button of record ").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waited for 2 sec");
		WebElement element = driver
				.findElement(By.xpath("//app-date-time[@class='date-margin']//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.previous_Month, "click on previous month < icon").click();
		waits(2);
		button(SM.previous_Month, "click on previous month < icon").click();
		logger.info("click on previous month < button");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element1 = driver.findElement(By.xpath("//span[normalize-space()='26']"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click()", element1);

		// button(SM.Previous_date_in_calendar_popup,"select previous date").click();
		logger.info("selected previous date");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");

		SM.validation_error_message_for_Date_in_EndDate_calender.isEnabled();
		if (verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender));
			textbox(SM.validation_error_message_for_Date_in_EndDate_calender).isEnabled();
			System.out.println("error message for date and time is displayed");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case passed ");

		} else {
			Assert.assertEquals(false,
					verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case failed ");
		}
		logger.info("verified validation message for previous date");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(5);
		logger.info("wait for 5 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_191_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(4);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(4);
		logger.info("waits for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list as general SM ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list as general SM case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list as general SM");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list as general SM case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		DeleteSM();
	}

	public void tc_192_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Date) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");

		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");

		SM.New_recipent_in_manage_page.isEnabled();
		System.out.println(SM.New_recipent_in_manage_page.isEnabled());
		if (verifyElementIsEnabled(SM.New_recipent_in_manage_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page is not available");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is not working  fine");
		}

		logger.info("Verified the added recipient in Manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_193_Verifying_Updated_text_message_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();

		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Date) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing1");
		logger.info("Edited text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		WebElement text = driver.findElement(By.xpath("//textarea[@id='schedule_message']"));
		String attribute = text.getAttribute("value");

		System.out.println("Edited text:" + attribute);

		if (attribute.contains("testingtesting1")) {

			System.out.println("Edited text for updated record is available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		else {

			System.out.println("Edited text for updated record is not available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		logger.info("Verified edited text message in manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_194_Verify_edited_attachment() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Date) option");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_195_Verifying_Updated_text_message_recipent_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Date) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 2 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing1");
		logger.info("Edited text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		WebElement text = driver.findElement(By.xpath("//textarea[@id='schedule_message']"));
		String attribute = text.getAttribute("value");

		System.out.println("Edited text:" + attribute);

		if (attribute.contains("testingtesting1")) {
			System.out.println("Edited text for updated record is available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		else {
			System.out.println("Edited text for updated record is not available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		logger.info("Verified edited text message in manage page");
		SM.New_recipent_in_manage_page.isEnabled();
		System.out.println(SM.New_recipent_in_manage_page.isEnabled());
		if (verifyElementIsEnabled(SM.New_recipent_in_manage_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page is not available");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is not working  fine");
		}

		logger.info("Verified the added recipient in Manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_196_Verify_edited_record_information() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}
		}
		logger.info("Selected Monthly on the (Date) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("Deleted the message");
		waits(2);
		logger.info("wait for 2 sec");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_197_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}
		}
		logger.info("selected Monthly on the (Date) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		List<WebElement> allOption = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOption.size());

		for (int i = 0; i <= allOption.size(); i++) {

			if (allOption.get(i).getText().contains("Weekly on (Selected day)")) {

				allOption.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day)");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Toster_message_for_edited_upcomming_record.getText();
		System.out.println(SM.Toster_message_for_edited_upcomming_record.getText());

		if (verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			textbox(SM.Toster_message_for_edited_upcomming_record).isEnabled();
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case failed ");
		}
		logger.info("Toster message for updated and saved record is displayed");
		waits(2);
		DeleteSM();

	}

	public void tc_198_Verify_validation_message_for_previous_date() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());
		for (int i = 0; i <= allOptions.size(); i++) {
			if (allOptions.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Monthly on the (Date) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("click on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_manage_button_under_list, "click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waited for 2 sec");
		WebElement element = driver
				.findElement(By.xpath("//app-date-time[@class='date-margin']//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.previous_Month, "click on < icon for previous month").click();
		button(SM.previous_Month, "click on < icon for previous month").click();
		button(SM.Previous_date_for_End_Date_calender, "select previous date").click();
		logger.info("selected previous date");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");

		SM.validation_error_message_for_Date_in_EndDate_calender.isEnabled();
		if (verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender));
			textbox(SM.validation_error_message_for_Date_in_EndDate_calender).isEnabled();
			System.out.println("error message for date and time is displayed");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case passed ");

		} else {
			Assert.assertEquals(false,
					verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case failed ");
		}
		logger.info("verified validation message for previous date");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(5);
		logger.info("wait for 5 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_199_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list as general SM ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list as general SM case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list as general SM");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list as general SM case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void tc_200_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");

		SM.New_recipent_in_manage_page.isEnabled();
		System.out.println(SM.New_recipent_in_manage_page.isEnabled());
		if (verifyElementIsEnabled(SM.New_recipent_in_manage_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page is not available");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is not working  fine");
		}

		logger.info("Verified the added recipient in Manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_201_Verifying_Updated_text_message_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing1");
		logger.info("Edited text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		WebElement text = driver.findElement(By.xpath("//textarea[@id='schedule_message']"));
		String attribute = text.getAttribute("value");

		System.out.println("Edited text:" + attribute);

		if (attribute.contains("testingtesting1")) {

			System.out.println("Edited text for updated record is available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		else {

			System.out.println("Edited text for updated record is not available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		logger.info("Verified edited text message in manage page");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_202_Verify_edited_attachment() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Yearly on (Date)(Month) option");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_203_Verifying_Updated_text_message_recipent_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Yearly on (Date)(Month) option");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();

		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 2 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing1");
		logger.info("Edited text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		WebElement text = driver.findElement(By.xpath("//textarea[@id='schedule_message']"));
		String attribute = text.getAttribute("value");

		System.out.println("Edited text:" + attribute);

		if (attribute.contains("testingtesting1")) {
			System.out.println("Edited text for updated record is available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		else {
			System.out.println("Edited text for updated record is not available in manage page");
			TestUtil.takeScreenshotAtEndOfTest("Verified edited text message in manage page scenario is passed");
		}

		logger.info("Verified edited text message in manage page");
		SM.New_recipent_in_manage_page.isEnabled();
		System.out.println(SM.New_recipent_in_manage_page.isEnabled());
		if (verifyElementIsEnabled(SM.New_recipent_in_manage_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.New_recipent_in_manage_page));
			System.out.println("verified added recipient in manage page is not available");
			TestUtil.takeScreenshotAtEndOfTest("verified added recipient in manage page case is not working  fine");
		}

		logger.info("Verified the added recipient in Manage page");
		waits(2);
		logger.info("wit for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_204_Verify_edited_record_information() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Selected recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}
		}
		logger.info("Selected Yearly on (Date)(Month) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("Deleted the message");
		waits(2);
		logger.info("wait for 2 sec");

		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.SM_button_in_Manage_page, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.attachment.isEnabled();
		if (verifyElementIsEnabled(SM.attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.attachment));
			textbox(SM.attachment).isEnabled();
			System.out.println("attachment is available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.attachment));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("attachment for Updated record is available case failed ");
		}
		logger.info("verified attachment in edit page");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_205_Verifying_Updated_recurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size(); i++) {

			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}
		}
		logger.info("selected Yearly on (Date)(Month) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		List<WebElement> allOption = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOption.size());

		for (int i = 0; i <= allOption.size(); i++) {

			if (allOption.get(i).getText().contains("Weekly on (Selected day)")) {

				allOption.get(i).click();
				break;
			}

		}

		logger.info("Selected Weekly on (Selected day)");
		waits(2);
		logger.info("waits 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");

		SM.Toster_message_for_edited_upcomming_record.getText();
		System.out.println(SM.Toster_message_for_edited_upcomming_record.getText());

		if (verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			textbox(SM.Toster_message_for_edited_upcomming_record).isEnabled();
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case passed ");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Toster_message_for_edited_upcomming_record));
			System.out.println("Toster message for Updated record is available");
			TestUtil.takeScreenshotAtEndOfTest("Toster message for Updated record is available case failed ");
		}
		logger.info("Toster message for updated and saved record is displayed");
		waits(2);
		DeleteSM();

	}

	public void tc_206_Verify_validation_message_for_previous_date() throws Exception {
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		List<WebElement> allOptions = driver.findElements(By.xpath("//select[1]/option"));
		System.out.println(allOptions.size());
		for (int i = 0; i <= allOptions.size(); i++) {
			if (allOptions.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Selected Yearly on (Date) (Month) option");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("click on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_manage_button_under_list, "click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waited for 2 sec");
		WebElement element = driver
				.findElement(By.xpath("//app-date-time[@class='date-margin']//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Down_arrow_icon_for_Month_and_Year_under_Recurring, "click on yearly icon").click();
		logger.info("clicked on years icon");
		waits(2);
		button(SM.Previous_Year_under_recurring, "click on previous year").click();
		logger.info("selected previous year");
		waits(2);
		button(SM.Month_for_previous_year_under_recurring, "click on month for previous year").click();
		logger.info("selected  month for previous year");
		waits(2);
		button(SM.Previous_date_for_End_Date_calender, "select previous date").click();
		logger.info("selected previous date");
		waits(2);
		logger.info("wait for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");

		SM.validation_error_message_for_Date_in_EndDate_calender.isEnabled();
		if (verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender));
			// textbox(SM.validation_error_message_for_Date_in_EndDate_calender).isEnabled();
			System.out.println("error message for date and time is displayed");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case passed ");

		} else {
			Assert.assertEquals(false,
					verifyElementIsEnabled(SM.validation_error_message_for_Date_in_EndDate_calender));
			System.out.println("attachment is not available");
			TestUtil.takeScreenshotAtEndOfTest("validation message for previous date is available case failed ");
		}
		logger.info("verified validation message for previous date");
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(5);
		logger.info("wait for 5 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");

	}

	public void tc_207_Functionality_of_SaveDraft_button_in_manage_page_for_nonrecurring_record() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicled on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "click on Drafts tab button").click();
		logger.info("Clickedon Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified the edited non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified the edited non recurring record under Drafts list case is pass");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified the edited non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified the edited non recurring record under Drafts list case is not working  fine");
		}
		logger.info("Verified edited non recurring record under Drafts list");

		button(SM.Draft_delete_button, "click on delete button").click();
		logger.info("clicked on delete button");
		waits(2);
		logger.info("waits for 5 seconds");
		logger.info("popup is opened");
		button(SM.Draft_delete_discard_popup_button, "click on discard button in popup").click();
		logger.info("clicked on discard button");
		waits(2);

	}

	public void tc_208_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "Click on Upcoming tab").click();
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(4);
		logger.info("waits for 4 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_209_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Cancel_Icon_For_Contact_in_Manage_page, "click on cancel icon").click();
		logger.info("Contact is deleted");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		logger.info("popup is opened with options Discard and Cancel");
		waits(4);
		logger.info("waits for 4 sec");
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");
		waits(4);

	}

	public void tc_210_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element2 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element2);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys(Keys.CONTROL, "a");
		textbox(SM.Create_SM_Message_textbox).sendKeys(Keys.DELETE);
		logger.info("Message is deleted");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Close_icon_of_Attachment, "click on close icon of the attachment").click();
		logger.info("Clicked on close icon of attachment");
		logger.info("attachement was deleted");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		logger.info("popup is opened with options Discard and Cancel");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");
		waits(3);

	}

	public void tc_211_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");

		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(1);
		logger.info("waits for 1 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");
		DeleteSM();
	}

	public void tc_212_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		textbox(SM.Create_SM_Message_textbox).sendKeys("app");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(4);
		logger.info("waits for 4 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		waits(3);
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void tc_213_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.webm");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element2 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element2);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Close_icon_of_Attachment, "click on close icon of the attachment").click();
		logger.info("Clicked on close icon of attachment");
		logger.info("attachement was deleted");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		waits(3);
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");

		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());

		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			textbox(SM.Upcoming_Record).isEnabled();
			System.out.println("Verified edited and saved non recurring record under upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under upcomming list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Verified edited and saved non recurring record under upcomming list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under upcomming list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under upcomming list is available");
		DeleteSM();

	}

	public void tc_214_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(1);
		logger.info("waits for 1 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");
		waits(2);
		DeleteSM();
	}

	public void tc_216_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.close_mark_button_for_contact, "click on close icon of the added contact").click();
		logger.info("Clicked on Close icon of added recipent");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		logger.info("popup is opened with options Discard and Cancel");
		waits(3);
		logger.info("waits for 3 sec");
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");
		waits(2);

	}

	public void tc_217_Functionality_of_Save_button_in_popup_for_Other_links() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element2 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element2);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys(Keys.CONTROL, "a");
		textbox(SM.Create_SM_Message_textbox).sendKeys(Keys.DELETE);
		logger.info("Message is deleted");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Close_icon_of_Attachment, "click on close icon of the attachment").click();
		logger.info("Clicked on close icon of attachment");
		logger.info("attachement was deleted");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_218_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 2 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(1);
		logger.info("waits for 1 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited non recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void tc_219_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		textbox(SM.Create_SM_Message_textbox).sendKeys("app");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(1);
		logger.info("waits for 1 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited message for non recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited message for non recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out
					.println("Edited message for non recurring record is not saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited message for non recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the edited message and saved record under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void tc_220_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();

		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleVideo.mp4");

		waits(5);
		logger.info("waits for 5 seconds");
		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(1);
		logger.info("waits for 1 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited message for non recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited message for non recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out
					.println("Edited message for non recurring record is not saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited message for non recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the edited message and saved record under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void tc_228_Functionality_of_SaveDraft_button_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "click on save Draft button").click();
		logger.info("clicked on Save Draft button");

		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified saved recurring record under Drafts list is available");
		waits(2);
		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		logger.info("popup is opened with options Discard and Cancel");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");
		waits(2);

	}

	public void tc_229_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the edited record under upcoming  list");
		DeleteSM();

	}

	public void tc_230_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.close_mark_button_for_contact, "click on close icon of recipent").click();
		logger.info("Clicked on close icon of reciepent");
		waits(2);
		logger.info("waitsfor 2 sec");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(1);
		logger.info("waits for 1 sec");
		button(SM.SM_Drafts_Button, "click on drafts button").click();
		logger.info("Clicked on Drafts button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest("Verified saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified saved recurring record under Drafts list is available");

		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		logger.info("popup is opened with options Discard and Cancel");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");

	}

	public void tc_232_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		waits(3);
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the edited record under upcoming  list");
		DeleteSM();

	}

	public void tc_233_Functionality_of_Save_button_in_popup_for_breadcrum_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on Recurring check button").click();
		logger.info("Clicked on Recurring check button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("SM");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Breadcrum_link_button, "Click on breadcrum link button").click();
		logger.info("Clicked on breadcrum link button");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(1);
		logger.info("waits for 1 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the edited record under upcoming  list");
		DeleteSM();
	}

	public void tc_235_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "Click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits if 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		waits(3);
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Delete_SM_button_in_Edit_SM_page, "Clicked on Delete SM button").click();
		logger.info("Clicked on Delete SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Confirm_Delete_SM_button_in_Edit_SM_page, "Clicked on Confirm Delete SM button in popup").click();
		logger.info("Clicked on Confirm Delete SM button in popup page");
		waits(2);

	}

	public void tc_237_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555551");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 2 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");

		waits(2);
		logger.info("waits for 2 seconds");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");

		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.close_mark_button_for_contact, "click on close icon of the added contact").click();
		logger.info("Clicked on Close icon of added recipent");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_239_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 2 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");
		waits(2);
		logger.info("waits for 2 seconds");
		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");
		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(3);
		logger.info("waits for 3 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");

		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited non recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");

		DeleteSM();

	}

	public void tc_240_Functionality_of_Save_button_in_popup_for_any_other_link() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		textbox(SM.Create_SM_Message_textbox).sendKeys("record");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 2 seconds");

		button(SM.Templates_tab, "Click on Templates button").click();
		logger.info("Clicked on Templates");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("conformation popup is opened with Save and Discard buttons");

		button(SM.Save_button_in_breadcrum_conformation_popup, "Click on Save button").click();
		logger.info("Clicked on save button in breadcrum conformation popup");
		waits(2);
		logger.info("waits for 2 sec");
		logger.info("user is in Templets page");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("Scheduled message tab");
		waits(2);
		logger.info("waits for 2 seconds");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and updated in upcomming list ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Edited recurring record is saved and not updated in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Edited recurring record is saved and updated in upcomming list case is not working  fine");
		}
		logger.info("Verified the saved record under upcoming  list");

		DeleteSM();

	}

	public void tc_251_Functionality_of_SM_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.close_mark_button_for_contact, "Click on close icon for recipient").click();
		logger.info("Clicked on Close icon for recipient");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_253_Functionality_of_Save_Draft_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "Click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_254_Functionality_of_Save_Draft_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.close_mark_button_for_contact, "Click on Close icon for recepent").click();
		logger.info("Clicked on close icon of reciepient");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");
		delete_draft();

	}

	public void tc_259_Functionality_of_SM_button_for_NonRecurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.close_mark_button_for_contact, "Click on Close icon for recepent").click();
		logger.info("Clicked on close icon of reciepient");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_261_Functionality_of_Save_Draft_button_for_NonRecurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "Click on Save Draft button").click();
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_262_Functionality_of_SM_button_for_NonRecurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.close_mark_button_for_contact, "Click on cancel icon of the recipent").click();
		logger.info("Clicked on close icon of the recipient");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_264_Functionality_of_Save_Draft_button_for_NonRecurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		textbox(SM.Message_sent_to_textbox).sendKeys("5555555551");
		logger.info("Entered contact number");
		waits(2);
		logger.info("waits for 2 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added contact number to message sent textbox");

		waits(2);
		logger.info("waits for 2 seconds");
		textbox(SM.Create_SM_Message_textbox).sendKeys("testing");
		logger.info("enetered text into message textbox field");
		waits(2);
		logger.info("waits for 5 seconds");

		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);

		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming button").click();
		waits(2);
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Clicked on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.close_mark_button_for_contact, "Click on cancel icon of the recipent").click();
		logger.info("Clicked on close icon of the recipient");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "Click on Save Draft button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved non recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved non recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved non recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved non recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_252_Functionality_of_SM_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(3);
		logger.info("waits for 3 sec");
		WebElement element1 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming tab").click();
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(5);
		logger.info("waits for 2 sec");
		SM.Create_SM_Message_textbox.sendKeys(Keys.CONTROL + "a");
		SM.Create_SM_Message_textbox.sendKeys(Keys.DELETE);
		/*
		 * textbox(SM.Create_SM_Message_textbox).clear(); waits(2);
		 */
		button(SM.Close_icon_of_Attachment, "Click on close icon for attachment").click();
		logger.info("Clicked on Close icon for attachment");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_255_Functionality_of_SM_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(3);
		logger.info("waits for 3 sec");
		WebElement element1 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming tab").click();
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		SM.Create_SM_Message_textbox.sendKeys(Keys.CONTROL + "a");
		SM.Create_SM_Message_textbox.sendKeys(Keys.DELETE);
		/*
		 * textbox(SM.Create_SM_Message_textbox).clear(); waits(2);
		 */
		button(SM.Close_icon_of_Attachment, "Click on close icon for attachment").click();
		logger.info("Clicked on Close icon for attachment");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_257_Functionality_of_SaveDraft_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(3);
		logger.info("waits for 3 sec");
		WebElement element1 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Click on recurring check button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming tab").click();
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		SM.Create_SM_Message_textbox.sendKeys(Keys.CONTROL + "a");
		SM.Create_SM_Message_textbox.sendKeys(Keys.DELETE);
		/*
		 * textbox(SM.Create_SM_Message_textbox).clear(); waits(2);
		 */
		button(SM.Close_icon_of_Attachment, "Click on close icon for attachment").click();
		logger.info("Clicked on Close icon for attachment");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "Click on SaveDraft button").click();
		logger.info("Clicked on SaveDraft button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_260_Functionality_of_SM_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(3);
		logger.info("waits for 3 sec");
		WebElement element1 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming tab").click();
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Create_SM_Message_textbox.sendKeys(Keys.CONTROL + "a");
		SM.Create_SM_Message_textbox.sendKeys(Keys.DELETE);
		/*
		 * textbox(SM.Create_SM_Message_textbox).clear(); waits(2);
		 */
		button(SM.Close_icon_of_Attachment, "Click on close icon for attachment").click();
		logger.info("Clicked on Close icon for attachment");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_265_Functionality_of_SaveDraft_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(3);
		logger.info("waits for 3 sec");
		WebElement element1 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming tab").click();
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		SM.Create_SM_Message_textbox.sendKeys(Keys.CONTROL + "a");
		SM.Create_SM_Message_textbox.sendKeys(Keys.DELETE);
		/*
		 * textbox(SM.Create_SM_Message_textbox).clear(); waits(2);
		 */
		button(SM.Close_icon_of_Attachment, "Click on close icon for attachment").click();
		logger.info("Clicked on Close icon for attachment");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SaveDraft_button_in_Manage_page, "Click on SaveDraft button").click();
		logger.info("Clicked on SaveDraft button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

	public void tc_263_Functionality_of_SM_button_for_Recurring_in_manage_page() throws Exception

	{
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		waits(2);
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on attachments paper clip icon").click();
		logger.info("Clicked on paper clip icon");

		waits(5);
		logger.info("waits for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\sample.mp3");

		waits(5);
		logger.info("waits for 5 seconds");
		WebElement element = driver.findElement(By.xpath("//input[@id='appDateTimeId']"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		waits(2);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		waits(3);
		logger.info("waits for 3 sec");
		WebElement element1 = driver.findElement(
				By.xpath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]"));
		jse1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.create_SM_ScheduledMessage_button, "click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Upcoming_tab, "click on upcoming tab").click();
		button(SM.Manage_button_under_Upcomming_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button");
		waits(2);
		logger.info("waits for 2 sec");
		Click_Recurring_button();
		SM.Create_SM_Message_textbox.sendKeys(Keys.CONTROL + "a");
		SM.Create_SM_Message_textbox.sendKeys(Keys.DELETE);
		/*
		 * textbox(SM.Create_SM_Message_textbox).clear(); waits(2);
		 */
		button(SM.Close_icon_of_Attachment, "Click on close icon for attachment").click();
		logger.info("Clicked on Close icon for attachment");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_button_in_Manage_page, "Click on SM button").click();
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("waits for 2 sec");

		button(SM.SM_Drafts_Button, "Click on Drafts").click();
		logger.info("Clicked on Drafts tab button");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());

		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			textbox(SM.Record_under_drafts_list).isEnabled();
			System.out.println("Verified edited and saved recurring record under Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case is passed");

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Verified edited and saved recurring record under Drafts list is not available");
			TestUtil.takeScreenshotAtEndOfTest(
					"Verified edited and saved recurring record under Drafts list case failed ");
		}

		logger.info("Verified edited and saved recurring record under Drafts list is available");

		delete_draft();

	}

}
