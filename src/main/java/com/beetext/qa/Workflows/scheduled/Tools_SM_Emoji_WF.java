package com.beetext.qa.Workflows.scheduled;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Tools_SM_pages;
import com.beetext.qa.util.TestUtil;

public class Tools_SM_Emoji_WF extends Tools_SM_WFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Tools_SM_pages SM = new Tools_SM_pages();
	TestUtil testUtil = new TestUtil();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void TC_013_Verify_emoji_added_record_under_upcoming() throws Exception {

		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_Contact_number();
		button(SM.SM_emoji_icon, "Click on emoji icon").click();
		logger.info("Clicked on emoji icon ");
		button(SM.SM_emoji_image1, "Click on emoji emage1").click();
		logger.info("Clicked on image1 and it was added to message box");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(1);
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with emoji is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with emoji is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		waits(2);
		DeleteSM();

	}

	public void TC_014_Verify_emoji_added_record_under_Drafts() throws Exception {

		Click_on_CreateScheduledMessage_button();
		String arr[] = { "4255058829", "7273039977", "5555555555", "5555555550", "5555555552", "5555555553",
				"5555555554", "5555555556", "5555555557" };
		waits(2);
		for (int i = 0; i <= 8; i++) {

			waits(2);
			textbox(SM.Message_sent_to_textbox).sendKeys(arr[i]);
			logger.info("Entered contact number");
			waits(2);
			logger.info("wait for 2 seconds");
			driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
			logger.info("Added contact number to message sent textbox");
			waits(2);
			logger.info("wait for 2 seconds");

		}
		waits(2);
		Enter_message();
		button(SM.SM_emoji_icon, "Click on emoji icon").click();
		logger.info("Clicked on emoji icon ");
		button(SM.SM_emoji_image1, "Click on emoji emage1").click();
		logger.info("Clicked on image1 and it was added to message box");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("wait for 2 sec");

		WebElement element = driver.findElement(By.id("schedule_DraftButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_Drafts_Button, "clicked on Drafts").click();
		logger.info("clicked on Drafts");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with emoji is saved and available in Draft list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is saved and available in Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with emoji is saved and  not available in Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is saved and available in Drafts list case is not working  fine");
		}
		logger.info("Verified the record under Drafts  list");
		waits(2);
		delete_draft();
	}

	public void TC_016_Verify_emoji_added_record_under_upcoming() throws Exception {

		Click_on_CreateScheduledMessage_button();
		String arr[] = { "4255058829", "7273039977" };
		waits(2);
		for (int i = 0; i <= 1; i++) {

			waits(2);
			textbox(SM.Message_sent_to_textbox).sendKeys(arr[i]);
			logger.info("Entered contact number");
			waits(2);
			logger.info("wait for 2 seconds");
			driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
			logger.info("Added contact number to message sent textbox");
			waits(2);
			logger.info("wait for 2 seconds");

		}
		waits(2);
		Enter_message();
		button(SM.Paper_Clip_icon_for_Attachments, "Click on paper clip icon").click();
		logger.info("Clicked on paper clip icon");
		waits(2);
		logger.info("waits for 2 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");
		waits(5);
		logger.info("waits for 5 sec");
		button(SM.SM_emoji_icon, "Click on emoji icon").click();
		logger.info("Clicked on emoji icon ");
		button(SM.SM_emoji_image1, "Click on emoji image1").click();
		logger.info("Clicked on image1 and it was added to message box");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("wait for 2 sec");

		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_Upcoming_tab, "click on Upcoming").click();
		wait(2);
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with emoji is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with emoji is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		waits(2);
		DeleteSM();
	}

	public void TC_018_Verify_emoji_added_record_under_Drafts() throws Exception {

		Click_on_CreateScheduledMessage_button();

		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_emoji_icon, "Click on emoji icon").click();
		logger.info("Clicked on emoji icon ");
		button(SM.SM_emoji_image1, "Click on emoji emage1").click();
		logger.info("Clicked on image1 and it was added to message box");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element = driver.findElement(By.id("schedule_DraftButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on Save Draft button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_Drafts_Button, "clicked on Drafts").click();
		logger.info("clicked on Drafts");
		SM.Record_under_drafts_list.isEnabled();
		System.out.println(SM.Record_under_drafts_list.isEnabled());
		if (verifyElementIsEnabled(SM.Record_under_drafts_list)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with emoji is saved and available in Draft list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is saved and available in Drafts list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Record_under_drafts_list));
			System.out.println("Record with emoji is saved and  not available in Drafts list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is saved and available in Drafts list case is not working  fine");
		}
		logger.info("Verified the record under Drafts  list");
		waits(2);
		delete_draft();
	}

	public void TC_019_Verify_emoji_added_record_under_Upcoming() throws Exception {

		Click_on_CreateScheduledMessage_button();
		Enter_message();
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Save_Draft_button_in_CSM, "click on save draft button").click();
		logger.info("Clicked on save draft button");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_Drafts_Button, "clicked on Drafts").click();
		logger.info("clicked on Drafts");
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.Manage_button_under_drafts_List, "click on manage button of the saved record").click();
		logger.info("Clicked on manage button");
		waits(2);
		logger.info("wait for 2 sec");
		Enter_Contact_number();
		waits(2);
		logger.info("waits for 2 sec");
		button(SM.SM_emoji_icon, "Click on emoji icon").click();
		logger.info("Clicked on emoji icon ");
		button(SM.SM_emoji_image1, "Click on emoji emage1").click();
		logger.info("Clicked on image1 and it was added to message box");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Calendar_icon, "Click on calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		button(SM.Uparrow_button_Time_hour, "Click on Uparrow button for Time").click();
		logger.info("Clicked on Uparrow of hour time");
		button(SM.Set_button_in_calendar_popup, "Click on Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("wait for 2 sec");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.SM_Upcoming_tab, "Click on Upcoming tab").click();
		logger.info("Clicked on upcoming tab");
		waits(2);
		logger.info("waits for 2 sec");
		SM.Upcoming_Record.isEnabled();
		System.out.println(SM.Upcoming_Record.isEnabled());
		if (verifyElementIsEnabled(SM.Upcoming_Record)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with emoji is created and available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is created and available in upcomming list case is working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.Upcoming_Record));
			System.out.println("Record with emoji is created and  not available in upcomming list");
			TestUtil.takeScreenshotAtEndOfTest(
					"Record with emoji is created and available in upcomming list case is not working  fine");
		}
		logger.info("Verified the record under upcoming  list");
		waits(2);
		DeleteSM();
	}

}
