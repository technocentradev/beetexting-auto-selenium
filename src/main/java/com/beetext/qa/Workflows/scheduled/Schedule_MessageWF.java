package com.beetext.qa.Workflows.scheduled;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.PinMessageWFCM;
import com.beetext.qa.WFCommonMethod.ScheduleMessagWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.util.TestUtil;

public class Schedule_MessageWF extends ScheduleMessagWFCM {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	PinMessageWFCM pin = new PinMessageWFCM();
	public static String currentDir = System.getProperty("user.dir");

	public void create_Schedule_Message() throws Exception {

		// login();
		CreateSM();
		entermessage();
		ScheduleButton();
		logger.info(sm.schdule_toaster_message.getText());
		if (sm.schdule_toaster_message.getText().equalsIgnoreCase("Your text for QA10 is scheduled to send on'")) {
			Assert.assertEquals(sm.schdule_toaster_message.getText(), "Your text for QA10 is scheduled to send on'");
			logger.info("Your message has been successfully scheduled to QA123. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("create_Schedule_Message");// ScreenShot
																			// capture

		} else {
			Assert.assertNotEquals(sm.schdule_toaster_message.getText(),
					"Your message has been successfully scheduled to QA123.",
					"Your text for QA10 is scheduled to send on'");
			TestUtil.takeScreenshotAtEndOfTest("create_Schedule_Message  TestCase is failed");// ScreenShot capture
		}

	}

	public void create_compose_new_Schedule_Message() throws Exception {

		// login();
		clickonComposeIcon();
		selectQA10_Conversation();
		composeNewcluckonSM();

		entermessage();
		ScheduleButton();

		logger.info(sm.schdule_toaster_message.getText());
		if (sm.schdule_toaster_message.getText().equalsIgnoreCase("Your text for QA10 is scheduled to send on'")) {
			Assert.assertEquals(sm.schdule_toaster_message.getText(), "Your text for QA10 is scheduled to send on'");
			logger.info("Your message has been successfully scheduled to QA123. case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase successfully");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(sm.schdule_toaster_message.getText(),
					"Your text for QA10 is scheduled to send on'", "Your text for QA10 is scheduled to send on'");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase is failed");// ScreenShot capture
		}

	}

	public void create_compose_new_Schedule_Message_add_attachments() throws Exception {

		// login();
		waits(2);
		clickonComposeIcon();
		selectQA10_Conversation();
		composeNewcluckonSM();

		entermessage();

		attachments();
		ScheduleButton();
		logger.info(sm.schdule_toaster_message.getText());
		if (sm.schdule_toaster_message.getText().equalsIgnoreCase("Your text for QA10 is scheduled to send on'")) {
			Assert.assertEquals(sm.schdule_toaster_message.getText(), "Your text for QA10 is scheduled to send on'");
			logger.info("Your message has been successfully scheduled to QA123. case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase successfully");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(sm.schdule_toaster_message.getText(),
					"Your text for QA10 is scheduled to send on'", "Your text for QA10 is scheduled to send on'");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase is failed");// ScreenShot capture
		}

	}

	public void create_compose_new_group_Schedule_Message_add_attachments() throws Exception {
		composenew_groupSM();
		selectgroupcontacts();
		entermessage();

		attachments();
		ScheduleButton();
		logger.info(sm.group_schdule_toaster_message.getText());
		if (sm.group_schdule_toaster_message.getText()
				.equalsIgnoreCase("Your text for this Group is scheduled to send on")) {
			Assert.assertEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on");
			logger.info("Your message has been successfully scheduled to group compose new . case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to group.  TestCase successfully");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on",
					"Your text for this Group is scheduled to send on");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to Group.  TestCase is failed");// ScreenShot capture
		}

	}

	public void create_exisitng_group_Schedule_Messae_add_attachments() throws Exception {

		// login();
		selectgroupconversation();

		entermessage();
		waits(5);
		attachments();

		ScheduleButton();
		logger.info(sm.group_schdule_toaster_message.getText());
		if (sm.group_schdule_toaster_message.getText()
				.equalsIgnoreCase("Your text for this Group is scheduled to send on")) {
			Assert.assertEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on");
			logger.info("Your message has been successfully scheduled to group compose new . case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to group.  TestCase successfully");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on",
					"Your text for this Group is scheduled to send on");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to Group.  TestCase is failed");// ScreenShot capture
		}

	}

	public void create_exisitng_onetoone_Schedule_Messae_add_attachments() throws Exception {

		// login();
		CreateSM();
		entermessage();
		attachments();
		ScheduleButton();
		logger.info(sm.schdule_toaster_message.getText());
		if (sm.schdule_toaster_message.getText().equalsIgnoreCase("Your text for QA10 is scheduled to send on'")) {
			Assert.assertEquals(sm.schdule_toaster_message.getText(), "Your text for QA10 is scheduled to send on'");
			logger.info("Your message has been successfully scheduled to QA123. case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase successfully");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(sm.schdule_toaster_message.getText(),
					"Your message has been successfully scheduled to QA123.",
					"Your text for QA10 is scheduled to send on'");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase is failed");// ScreenShot capture
		}

	}

	public void create_compose_new_group_Schedule_Message() throws Exception {

		// login();
		composeGroup();

		entermessage();
		ScheduleButton();

		logger.info(sm.group_schdule_toaster_message.getText());
		if (sm.group_schdule_toaster_message.getText()
				.equalsIgnoreCase("Your text for this Group is scheduled to send on")) {
			Assert.assertEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on");
			logger.info("Your message has been successfully scheduled to group. case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to group.  TestCase successfully");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on",
					"Your text for this Group is scheduled to send on");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase is failed");// ScreenShot capture
		}

	}

	public void pastDate_Time_compose_new_Schedule_Message() throws Exception {

		// login();
		composenew();
		ScheduleButton();

		logger.info(sm.past_DateTime.getText());
		if (sm.past_DateTime.getText().equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(sm.past_DateTime.getText(), "Please select a date and time in the future.");
			logger.info("Please select a date and time in the future. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Please select a date and time in the future.  TestCase successfully");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(sm.past_DateTime.getText(), "Please select a date and time in the future.",
					"Please select a date and time in the future.");
			TestUtil.takeScreenshotAtEndOfTest("Please select a date and time in the future.  TestCase is failed");// ScreenShot
																													// capture
		}
		button(sm.okbutton, "okButton").click();

	}

	public void pastDate_Time_group_compose_new_Schedule_Message() throws Exception {

		// login();
		// waits(1);
		
		waitAndLog(10);
		composenew_group();

		entermessage();

		ScheduleButton();

		logger.info(sm.past_DateTime.getText());
		if (sm.past_DateTime.getText().equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(sm.past_DateTime.getText(), "Please select a date and time in the future.");
			logger.info("Please select a date and time in the future. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Please select a date and time in the future.  TestCase successfully");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(sm.past_DateTime.getText(), "Please select a date and time in the future.",
					"Please select a date and time in the future.");
			TestUtil.takeScreenshotAtEndOfTest("Please select a date and time in the future.  TestCase is failed");// ScreenShot
																													// capture
		}
		button(sm.okbutton, "okButton").click();
	}

	public void composenew_add_maxlimit_contacts_createSM() throws Exception {

		// login();
		composenew_group_maxlimit_Contacts_create_SM();

		entermessage();

		ScheduleButton();

		logger.info(sm.group_schdule_toaster_message.getText());
		if (sm.group_schdule_toaster_message.getText()
				.equalsIgnoreCase("Your text for this Group is scheduled to send on")) {
			Assert.assertEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on");

			logger.info("Your message has been successfully scheduled to group. case is verified");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to group.  TestCase successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(sm.group_schdule_toaster_message.getText(),
					"Your text for this Group is scheduled to send on",
					"Your text for this Group is scheduled to send on");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase is failed");// ScreenShot capture
		}

	}

	public void composenew_add_maxlimit_contacts_errrormessagecase() throws Exception {

		// login();

		composenew_group_maxlimit_Contacts_erorrmessage_checking();

		logger.info(sm.maxtencontactsallowed.getText());
		if (sm.maxtencontactsallowed.getText().equalsIgnoreCase("Maximum 9 contacts are allowed.")) {
			Assert.assertEquals(sm.maxtencontactsallowed.getText(), "Maximum 9 contacts are allowed.");
			logger.info("Your message has been successfully scheduled to group. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("composenew contact error limit  TestCase successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(sm.maxtencontactsallowed.getText(), "Maximum 9 contacts are allowed.",
					"Maximum 9 contacts are allowed.");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your message has been successfully scheduled to QA123.  TestCase is failed");// ScreenShot capture
		}

	}

}
