package com.beetext.qa.Workflows.scheduled;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Reviews_Workflow_CM;
import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Reviews_pages;
import com.beetext.qa.pages.Tools_SM_pages;
import com.beetext.qa.util.TestUtil;

public class Tools_SM_Reviews_WF extends Tools_SM_WFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Tools_SM_pages SM = new Tools_SM_pages();
	Reviews_Workflow_CM RCM = new Reviews_Workflow_CM();
	Reviews_pages RP = new Reviews_pages();
	TestUtil testUtil = new TestUtil();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void tc_001_Functionalty_of_Reviews_icon() throws Exception {

		Create_Review();
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("clicked on sceduled message tab");
		Click_on_CreateScheduledMessage_button();
		button(SM.Review_link_icon, "Click on review icon").click();
		logger.info("Clicked on review icon");
		waits(2);
		System.out.println(SM.SM_reviews_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_reviews_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_reviews_icon));
			System.out.println("Reviews icons is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_reviews_icon));
			System.out.println("Reviews icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}

		System.out.println(SM.SM_emoji_icon.isEnabled());
		if (verifyElementIsEnabled(SM.SM_emoji_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_emoji_icon));
			System.out.println("emoji icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_emoji_icon));
			System.out.println("emoji icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}
		System.out.println(SM.SM_GIF_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_GIF_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_icon));
			System.out.println("GIF icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_icon));
			System.out.println("GIF icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}
		System.out.println(SM.SM_pin_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_pin_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_pin_icon));
			System.out.println("pin icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_pin_icon));
			System.out.println("pin icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}
		RCM.Click_on_Reviews();
		waits(2);
		button(SM.Discard_button_in_other_Link_conformation_popup, "click on discard button in conformation popup page")
				.click();
		waits(2);
		button(RP.Yelp_manage_button, "click on Yelp manage button").click();
		logger.info("Clicked on manage button");
		waits(2);
		WebElement element = driver.findElement(By.xpath("//button[@id='delete-review-message']"));
		JavascriptExecutor jr = (JavascriptExecutor) driver;
		jr.executeScript("arguments[0].click()", element);
		logger.info("Clickedon Delete button");
		waits(2);
		WebElement element1 = driver.findElement(By.xpath("//button[contains(text(),'Confirm Delete Review ')]"));
		JavascriptExecutor jr1 = (JavascriptExecutor) driver;
		jr1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on confirm delete button");

	}

	public void tc_002_Verify_all_icons_disable() throws Exception {

		Create_Review();
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("clicked on sceduled message tab");
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Enter_message();
		Click_Recurring_button();
		waits(2);
		button(SM.Calendar_icon, "Click on Calender").click();
		logger.info("Clickedon Calender");
		button(SM.Uparrow_button_Time_hour, "Click on uparrow button").click();
		logger.info("Clicked on uparrow of time");
		button(SM.Set_button_in_calendar_popup, "Click on  Set button").click();
		logger.info("Clicked on Set button");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jr = (JavascriptExecutor) driver;
		jr.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		button(SM.recurring_manage_button_under_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button under upcoming list");
		textbox(SM.Create_SM_Message_textbox).clear();
		logger.info("cleared text in the message text box");
		button(SM.Review_link_icon, "Click on review icon").click();
		logger.info("Clicked on review icon");
		waits(2);
		System.out.println(SM.SM_reviews_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_reviews_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_reviews_icon));
			System.out.println("Reviews icons is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_reviews_icon));
			System.out.println("Reviews icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}

		System.out.println(SM.SM_emoji_icon.isEnabled());
		if (verifyElementIsEnabled(SM.SM_emoji_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_emoji_icon));
			System.out.println("emoji icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_emoji_icon));
			System.out.println("emoji icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}
		System.out.println(SM.SM_GIF_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_GIF_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_icon));
			System.out.println("GIF icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_icon));
			System.out.println("GIF icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}
		System.out.println(SM.SM_pin_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_pin_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_pin_icon));
			System.out.println("pin icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_pin_icon));
			System.out.println("pin icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}

		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		waits(2);
		logger.info("wait for 2 seconds");
//			WebElement  element1= driver.findElement(By.id("delete-scheduled-message"));
//			JavascriptExecutor jse= (JavascriptExecutor)driver;
//			jse.executeScript("arguments[0].click()",element1);

		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 sec");
		delete_Review();

	}

	public void tc_003_Verify_all_icons_disable() throws Exception {
		Create_Review();
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("clicked on sceduled message tab");
		waits(3);
		logger.info("wait for 3 sec");
		Click_on_CreateScheduledMessage_button();
		Enter_Contact_number();
		Click_Recurring_button();
		waits(2);
		button(SM.Review_link_icon, "Click on review icon").click();
		logger.info("Clicked on review icon");
		button(SM.Calendar_icon, "Click on Calender").click();
		logger.info("Clicked on Calender");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Uparrow_button_Time_hour, "Click on uparrow button").click();
		logger.info("Clicked on uparrow of time");
		waits(2);
		logger.info("wait for 2 sec");
		button(SM.Set_button_in_calendar_popup, "Click on  Set button").click();
		logger.info("Clicked on Set button");
		waits(2);
		logger.info("waits for 2 sec");
		WebElement element = driver.findElement(By.id("schedule_SaveButton"));
		JavascriptExecutor jr = (JavascriptExecutor) driver;
		jr.executeScript("arguments[0].click()", element);
		logger.info("Clicked on SM button");
		waits(2);
		button(SM.recurring_manage_button_under_list, "Click on Manage button").click();
		logger.info("Clicked on Manage button under upcoming list");
		waits(2);
		System.out.println(SM.SM_reviews_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_reviews_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_reviews_icon));
			System.out.println("Reviews icons is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_reviews_icon));
			System.out.println("Reviews icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}

		System.out.println(SM.SM_emoji_icon.isEnabled());
		if (verifyElementIsEnabled(SM.SM_emoji_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_emoji_icon));
			System.out.println("emoji icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_emoji_icon));
			System.out.println("emoji icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}
		System.out.println(SM.SM_GIF_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_GIF_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_GIF_icon));
			System.out.println("GIF icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_GIF_icon));
			System.out.println("GIF icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}
		System.out.println(SM.SM_pin_icon.isEnabled());
		waits(2);
		if (verifyElementIsEnabled(SM.SM_pin_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(SM.SM_pin_icon));
			System.out.println("pin icon is enabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is not working  fine");

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(SM.SM_pin_icon));
			System.out.println("pin icon is disabled");
			TestUtil.takeScreenshotAtEndOfTest(
					"Review link added to message field and all icons are disabled case is working  fine");
		}

		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		waits(2);
		logger.info("wait for 2 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 2 seconds");
		delete_Review();

	}

}
