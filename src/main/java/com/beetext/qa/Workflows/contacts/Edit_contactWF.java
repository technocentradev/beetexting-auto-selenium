package com.beetext.qa.Workflows.contacts;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.EditContactWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Edit_contact;
import com.beetext.qa.util.TestUtil;

public class Edit_contactWF extends EditContactWFCM {

	Edit_contact create = new Edit_contact();
	TestBase testBase = new TestBase();

	public void editContact_Name_With_specialChar() throws Exception {
		// login(map);
		selectconversations();
		clearName();
		textbox(create.edit_name).sendKeys("QA123@$#%&*?_()");
		logger.info("Edit Contact Enterign Nmae QA123@$#%&*?_()");
		wait(5);

		savebutton();

		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
	}

	public void editContact_Name_With_singe_Char() throws Exception {
		// login(map);
		searchQA123contact();
		clearName();
		textbox(create.edit_name).sendKeys("a");
		logger.info("edit contact page , name is Entering singel character");
		wait(5);
		logger.info("wait for 5 sec");

		savebutton();

		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
		editName();
	}

	public void editContact_Companyname_allow_special_char() throws Exception {
		// login(map);
		selectconversations();
		clearCompanyName();
		textbox(create.edit_company_name).sendKeys("@#$^%$%^&%^&%&^%&");
		logger.info("Company name entering @#$^%$%^&%^&%&^%& ");
		wait(5);
		logger.info("wait for 5 sec");
		savebutton();
		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
	}

	public void editcontatName_givespaceatend() throws Exception {

		// login(map);
		selectconversations();
		textbox(create.edit_name).sendKeys("  ");
		logger.info("Edit contact page , contact giving at the end space");
		wait(5);
		logger.info("wait for 5 sec");

		savebutton();

		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
	}

	public void editcontatCompanyName_givespaceatend() throws Exception {

		// login(map);
		selectconversations();

		textbox(create.edit_company_name).sendKeys(" tech  ");
		logger.info("Edit contact page , company name beging and ending giving  spaces ");
		wait(5);
		logger.info("wait for 5 sec");

		savebutton();

		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
	}

	public void editContact_toastermessagevalidation() throws Exception {
		// login(map);
		selectconversations();
		savebutton();
		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
	}

	public void editContact_vaid_avathar_logo() throws Exception {
		// login(map);
		selectconversations();
		avatharlogo();

		savebutton();

		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
	}

	public void editContact_invaid_avathar_logo() throws Exception {
		// login(map);
		selectconversations();
		invalidavatharlogo();

		logger.info("wait for 20 sec");
		logger.info(create.edit_invalidavathar_file.getText());
		if (create.edit_invalidavathar_file.getText().equalsIgnoreCase("File format is not supported.")) {
			Assert.assertEquals(create.edit_invalidavathar_file.getText(), "File format is not supported.",
					"File format is not supported.");

			logger.info("contact avathar File format is not supported.");
			TestUtil.takeScreenshotAtEndOfTest("edit contact File format is not supported. case working fine.");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(create.edit_invalidavathar_file.getText(), "File format is not supported.",
					"File format is not supported.");
			logger.info("edit contact File format is not supported.not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact File format is not supported.  not working");// ScreenShot
																											// capture
		}
		waits(3);
		create.okbutton.click();

		savebutton();
	}

	public void editContact_enter_invalid_email() throws Exception {
		// login(map);
		selectconversations();

		textbox(create.edit_email).sendKeys("abc");
		logger.info("Edit contact page ,enterring invalid email");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.edit_company_name).click();
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(create.edit_invalid_email.getText());
		if (create.edit_invalid_email.getText().equalsIgnoreCase("Enter valid email.")) {
			Assert.assertEquals(create.edit_invalid_email.getText(), "Enter valid email.", "Enter valid email.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact invalid email case working fine.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.edit_invalid_email.getText(), "Enter valid email.", "Enter valid email.");
			logger.info("edit contact invalid email case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contac invalid  email case  not working");// ScreenShot capture
		}
	}

	public void editContact_all_valid_data() throws Exception {
		// login(map);
		selectconversations();
		textbox(create.edit_company_name).sendKeys("technocentra");
		logger.info("Edit contact page company name entering technocentra ");
		wait(5);
		logger.info("wait for 5 sec ");
		textbox(create.edit_company_name).sendKeys("technocentra@yopmail.com");
		logger.info("Edit contact page email  entering technocentra@yopmail.com ");
		wait(5);
		logger.info("wait for 5 sec ");

		savebutton();

		logger.info(create.edit_toaster_message.getText());
		if (create.edit_toaster_message.getText().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.edit_toaster_message.getText(), "Contact successfully updated.",
					"Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case  not working");// ScreenShot
																											// capture
		}
	}

	public void editContact_tag_allow_special_Char() throws Exception {
		// login(map);
		selectconversation();
		clickonaddtagTextbox();

		String tag = "@$%&*/-";
		textbox(create.type_tag).clear();
		textbox(create.type_tag).sendKeys(tag);
		logger.info("create tag text entering:" + tag);
		wait(5);
		logger.info("wait for 5 sec");

		addTagbutton();
		logger.info(create.verify_tag_special_char.getText());
		if (create.verify_tag_special_char.getText().equalsIgnoreCase("@$%&*/-")) {
			Assert.assertEquals(create.verify_tag_special_char.getText(), "@$%&*/-", "@$%&*/-");
			logger.info("edit contact tag is adding case working fine");
			logger.info("edit contact tag is adding case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag is adding  case working fine.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.verify_tag_special_char.getText(), "@$%&*/-", "@$%&*/-");
			logger.info("edit contact tag is adding case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag is adding  case  not working");// ScreenShot capture
		}
	}

	public void editContact_delete_tag() throws Exception {
		// login(map);
		selectconversation();
		clickoncrossmark();
		deletetag();

		logger.info(create.tag_toaster.getText());
		if (create.tag_toaster.getText().equalsIgnoreCase("@$%&*/- tag has been deleted.")) {
			Assert.assertEquals(create.tag_toaster.getText(), "@$%&*/- tag has been deleted.",
					"@$%&*/- tag has been deleted.");
			logger.info("edit contact tag is deleting case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag is deleting  case working fine.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.tag_toaster.getText(), "@$%&*/- tag has been deleted.",
					"@$%&*/- tag has been deleted.");
			logger.info("edit contact tag is deleting case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact deleting is adding  case  not working");// ScreenShot
																										// capture
		}
	}

	public void editContact_select50tags_contact_deleteone_tag() throws Exception {
		// login(map);
		searchHellocontact();

		button(create.delete43_tag, "button").click();
		logger.info("deleting 43 tag in tag list");
		wait(5);
		logger.info("wait for 5 sec");

		deletetag();

		logger.info(create.delete_43tag_toaster.getText());
		if (create.delete_43tag_toaster.getText().equalsIgnoreCase("43 tag has been deleted.")) {
			Assert.assertEquals(create.delete_43tag_toaster.getText(), "43 tag has been deleted.",
					"@$%&*/- tag has been deleted.");
			logger.info("edit contact tag is deleting case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag is deleting and same tag adding  case working fine.");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(create.delete_43tag_toaster.getText(), "43 tag has been deleted.",
					"43 tag has been deleted.");
			logger.info("edit contact tag is deleting case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact deleting and same tag adding  case  not working");// ScreenShot
																												// capture
		}
		addTag();

	}

	public void editContact_morethan_50_tag_validating() throws Exception {
		// login(map);
		selectconversation();

		clickonaddtagTextbox();
		textbox(create.type_tag).clear();
		button(create.type_tag, "button").sendKeys(
				"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51");
		logger.info("Create tag  text box entering 50 tags");
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(create.validation_50tag_error.getText());
		if (create.validation_50tag_error.getText()
				.equalsIgnoreCase("This contact has reached the maximum number of tag")) {
			Assert.assertEquals(create.validation_50tag_error.getText(),
					"This contact has reached the maximum number of tag",
					"This contact has reached the maximum number of tag");
			logger.info("edit contact tag is deleting case working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"edit contact tag is This contact has reached the maximum number of tag  case working fine.");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(create.validation_50tag_error.getText(),
					"This contact has reached the maximum number of tag",
					"This contact has reached the maximum number of tag.");
			logger.info("edit contact tag This contact has reached the maximum number of tag not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"edit contact This contact has reached the maximum number of tag  case  not working");// ScreenShot
																											// capture
		}

	}

	public void editContact_close_delete_tag_popup() throws Exception {
		// login(map);
		selectconversation();

		clickoncrossmark();
		logger.info(create.open_delete_popup.getText());
		if (create.open_delete_popup.getText().equalsIgnoreCase("Do you want to remove this tag from")) {
			Assert.assertEquals(create.open_delete_popup.getText(), "Do you want to remove this tag from",
					"Do you want to remove this tag from");
			logger.info("edit contact tag conformation popup click on cancel buttoncase working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"edit contact tag conformation popup click on cancel button case working fine.");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(create.open_delete_popup.getText(), "Do you want to remove this tag from",
					"Do you want to remove this tag from");
			logger.info("edit contact tag conformation popup click on cancel buttoncase not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"edit contact conformation popup click on cancel button case  not working");// ScreenShot capture
		}

		button(create.cancel_button, "button").click();
		logger.info("click on cancel button ");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void editContact_tag_allow_invalid_special_Char() throws Exception {
		// login(map);
		selectconversation();
		clickonaddtagTextbox();
		String tag = "()#!:<,.";
		textbox(create.type_tag).clear();
		textbox(create.type_tag).sendKeys(tag);
		logger.info("Typing invalid specials tag" + tag);
		wait(5);
		logger.info("wait for 5 sec");

		addTagbutton();

		logger.info(create.invalid_special_char.getText());
		if (create.invalid_special_char.getText().equalsIgnoreCase("Special characters not allowed.")) {
			Assert.assertEquals(create.invalid_special_char.getText(), "Special characters not allowed.",
					"Special characters not allowed.");
			logger.info("edit contact tag Special characters not allowed. working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Special characters not allowed.  case working fine.");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(create.invalid_special_char.getText(), "Special characters not allowed.",
					"Special characters not allowed.");
			logger.info("edit contact tag Special characters not allowed. case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Special characters not allowed. case  not working");// ScreenShot
																														// capture
		}
	}

	public void editContact_tag_duplicate_tag_validation() throws Exception {
		// login(map);
		selectconversation();
		clickonaddtagTextbox();
		textbox(create.type_tag).clear();
		String tag = "1,1";
		textbox(create.type_tag).sendKeys(tag);
		logger.info("Enter the duplicate tags " + tag);
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(create.duplicate_tag_validation.getText());
		if (create.duplicate_tag_validation.getText().equalsIgnoreCase("Duplicate tags not allowed.")) {
			Assert.assertEquals(create.duplicate_tag_validation.getText(), "Duplicate tags not allowed.",
					"Special characters not allowed.");
			logger.info("edit contact tag Duplicate tags not allowed. working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Duplicate tags not allowed..  case working fine.");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(create.duplicate_tag_validation.getText(), "Duplicate tags not allowed.",
					"Duplicate tags not allowed.");
			logger.info("edit contact tag Duplicate tags not allowed. case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Duplicate tags not allowed.. case  not working");// ScreenShot
																													// capture
		}
	}

	public void editContact_tag_duplicate_tag_validation_lower_upper_char() throws Exception {
		// login(map);
		selectconversation();

		clickonaddtagTextbox();
		textbox(create.type_tag).clear();
		String tag = "aws,AWS";
		textbox(create.type_tag).sendKeys(tag);
		logger.info("Typing lower and upper case duplicate Tags" + tag);
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(create.duplicate_tag_validation.getText());
		if (create.duplicate_tag_validation.getText().equalsIgnoreCase("Duplicate tags not allowed.")) {
			Assert.assertEquals(create.duplicate_tag_validation.getText(), "Duplicate tags not allowed.",
					"Special characters not allowed.");
			logger.info("edit contact tag Duplicate tags not allowed. working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Duplicate tags not allowed..  case working fine.");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(create.duplicate_tag_validation.getText(), "Duplicate tags not allowed.",
					"Duplicate tags not allowed.");
			logger.info("edit contact tag Duplicate tags not allowed. case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Duplicate tags not allowed.. case  not working");// ScreenShot
																													// capture
		}
	}

	public void editContact_tag_duplicate_tag_validation_upper_char() throws Exception {
		// login(map);
		selectconversation();
		clickonaddtagTextbox();
		textbox(create.type_tag).clear();
		String tag = "AWS,AWS";
		textbox(create.type_tag).sendKeys(tag);
		logger.info("Entering uppercase duplicate tags" + tag);
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(create.duplicate_tag_validation.getText());
		if (create.duplicate_tag_validation.getText().equalsIgnoreCase("Duplicate tags not allowed.")) {
			Assert.assertEquals(create.duplicate_tag_validation.getText(), "Duplicate tags not allowed.",
					"Special characters not allowed.");
			logger.info("edit contact tag Duplicate tags not allowed. working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Duplicate tags not allowed..  case working fine.");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(create.duplicate_tag_validation.getText(), "Duplicate tags not allowed.",
					"Duplicate tags not allowed.");
			logger.info("edit contact tag Duplicate tags not allowed. case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact tag Duplicate tags not allowed.. case  not working");// ScreenShot
																													// capture
		}
	}

	public void editContact__tag_uppercase_letters() throws Exception {
		// login(map);
		selectconversation();
		clickonaddtagTextbox();
		textbox(create.type_tag).clear();
		// String tag=randomecapitalstring();
		String tag = "HELLO";
		logger.info("capital tag" + tag);
		textbox(create.type_tag).sendKeys(tag);
		logger.info("create tag text box entering uppercase tag" + tag);
		wait(5);
		logger.info("wait for 5 sec");
		addTagbutton();
		logger.info(create.add_tag_serachresults.getText());
		if (create.add_tag_serachresults.getText().equalsIgnoreCase(tag)) {
			Assert.assertEquals(create.add_tag_serachresults.getText(), tag, tag);
			logger.info("edit contact uppcase tag is adding case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact uppercase tag is adding  case working fine.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(create.add_tag_serachresults.getText(), tag, tag);
			logger.info("edit contact tag uppercase tag is adding case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact uppercase  tag is adding  case  not working");// ScreenShot
																											// capture
		}

		button(create.search_crossmark, "button").click();
		logger.info("click on cross mark");
		wait(3);
		logger.info("wait for 3 sec");
		deletetag();

	}

	public void editContact__add_note() throws Exception {
		// login(map);
		selectconversation();
		typeNote();
		String note = Char50_randomestring();
		textbox(create.type_note).sendKeys(note);
		logger.info("Note text box entering " + note);
		wait(5);
		logger.info("wait for 5 sec");
		addNoteButton();
		waits(3);
		textbox(create.type_note).sendKeys("helo");
		logger.info("Typing note helo");
		wait(5);
		logger.info("wait for 5 sec");
		addNoteButton();
		logger.info(create.edit_note.getText());
		if (create.edit_note.getText().equalsIgnoreCase("helo")) {
			Assert.assertEquals(create.edit_note.getText(), "helo", "helo");
			logger.info("edit contact add note  case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact   adding note  case working fine.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.edit_note.getText(), "hello", "helo");
			logger.info("edit contact adding note case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact adding  note  case  not working");// ScreenShot capture
		}

	}

	public void editContact__edit_note() throws Exception {
		// login(map);
		selectconversation();

		textbox(create.edit_note).click();
		logger.info("click on  edit note text box");
		wait(5);
		logger.info("wait for 5 sec");
		clearNote();
		textbox(create.edit_type_note).sendKeys("helo");
		logger.info("edit note text box entering helo");
		wait(5);
		logger.info("wait for 5 sec");
		savenotebutton();
		logger.info(create.edit_note.getText());
		if (create.edit_note.getText().equalsIgnoreCase("helo")) {
			Assert.assertEquals(create.edit_note.getText(), "helo", "helo");
			logger.info("edit contact add note  case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact edit note case is   working fine.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.edit_note.getText(), "helo", "helo");
			logger.info("edit contact tag search tag is adding case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact edit note tag   case  not working");// ScreenShot capture
		}

	}

	public void editContact__edit_not_update_note_text() throws Exception {
		// login(map);
		selectconversation();

		textbox(create.edit_note).click();
		logger.info("note opening in edit mode");
		wait(5);
		logger.info("wait for 5 sec");

		savenotebutton();
		logger.info(create.edit_note.getText());
		if (create.edit_note.getText().equalsIgnoreCase("helo")) {
			Assert.assertEquals(create.edit_note.getText(), "helo", "helo");
			logger.info("edit contact add note  case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact edit note case is   working fine.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.edit_note.getText(), "helo", "helo");
			logger.info("edit contact tag search tag is adding case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact edit note  case  not working");// ScreenShot capture
		}

	}

	public void editContact__edit_note_click_cancel_button() throws Exception {
		// login(map);
		selectconversation();

		textbox(create.edit_note).click();
		logger.info("click on edit note text box");
		wait(5);
		logger.info("wait for 5 sec");

		button(create.cancel_cross_mark, "button").click();
		logger.info("click on cross mark");
		wait(5);
		logger.info("wait for 5 sec");
		logger.info(create.edit_note.getText());
		if (create.edit_note.getText().equalsIgnoreCase("helo")) {
			Assert.assertEquals(create.edit_note.getText(), "helo", "helo");
			logger.info("edit contact click on  cancel butotn  case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact note edit cancel button  case is   working fine.");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(create.edit_note.getText(), "helo", "helo");
			logger.info("edit contact note edit cancel button case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact note edit cancel button case  not working");// ScreenShot
																											// capture
		}

	}

	public void editContact__delete_Note() throws Exception {
		// login(map);
		selectconversation();

		deleteNote();
		logger.info(create.delete_note_toaster_message.getText());
		if (create.delete_note_toaster_message.getText().equalsIgnoreCase("Note has been deleted.")) {
			Assert.assertEquals(create.delete_note_toaster_message.getText(), "Note has been deleted.",
					"Note has been deleted.");
			logger.info("edit contact delete note  case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact delete note case is   working fine.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.delete_note_toaster_message.getText(), "helo", "helo");
			logger.info("edit contact delete note  case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact delete note  case  not working");// ScreenShot capture
		}

		addNote();

	}

	public void editContact__cancel_Note() throws Exception {
		// login(map);
		selectconversation();
		textbox(create.edit_note).click();
		logger.info("click on edit note text box");
		wait(5);
		logger.info("wait for 5 sec");

		button(create.click_delete_icon, "button").click();
		logger.info("click on note delete icon");
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(create.pop_headding.getText());
		if (create.pop_headding.getText().equalsIgnoreCase("Do you want to delete this note from")) {
			Assert.assertEquals(create.pop_headding.getText(), "Do you want to delete this note from",
					"Do you want to delete this note from");
			logger.info("edit contact note conformation popup click on cancel buttoncase working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"edit contact note conformation popup click on cancel button case working fine.");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(create.pop_headding.getText(), "Do you want to delete this note from",
					"Do you want to delete this note from");
			logger.info("edit contact note conformation popup click on cancel buttoncase not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"edit contact conformation popup click on cancel button case  not working");// ScreenShot capture
		}

		button(create.click_cancel_button, "button").click();
		logger.info("click on cancel button");
		wait(5);
		logger.info("wait for 5 sec");
	}
}
