package com.beetext.qa.Workflows.contacts;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.CreateContactWFCM;
import com.beetext.qa.pages.Create_Contact;
import com.beetext.qa.util.TestUtil;

public class CreateContactWF extends CreateContactWFCM {

	Create_Contact create = new Create_Contact();

	public void Createcontat() throws Exception {

		openCreateContact();
		onlyName();
		enterEmailandPhonenumber();
		comapanynameandTagsAndNotes();
		createButton();
		if (create.create_contact_Successfully.getText().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact not successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");// ScreenShot capture
		}

	}

	public void CreatecontatName_allowSpecialChar() throws Exception {
		openCreateContact();
		waits(2);
		enterEmailandPhonenumber();

		textbox(create.create_contact_Name).sendKeys("@#$%^&*?_!@@#$$%%^^?___");
		logger.info("Contact name entered @#$%^&*?_!@@#$$%%^^?___");
		wait(5);
		logger.info("wait for 5 sec");
		comapanynameandTagsAndNotes();
		createButton();

		if (create.create_contact_Successfully.getText().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");

			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact not successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");// ScreenShot capture
		}

	}

	public void CreatecontatName_allowSingleChar() throws Exception {

		openCreateContact();
		enterEmailandPhonenumber();

		textbox(create.create_contact_Name).sendKeys("1");
		logger.info("contact name entered : 1");
		wait(5);
		logger.info("wait for 5 sec");

		createButton();
		if (create.create_contact_Successfully.getText().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact not successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");// ScreenShot capture
		}

	}

	public void Createcontat_valid_avathar_image_upload() throws Exception {

		openCreateContact();
		validavatharlogo();
		onlyName();
		onlyMobileNumber();
		comapanynameandTagsAndNotes();
		createButton();
		if (create.create_contact_Successfully.getText().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact not successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");// ScreenShot capture
		}

	}

	public void Createcontat_invalid_avathar_image_upload() throws Exception {

		openCreateContact();

		invalidavatharlogo();
		if (create.create_invlaid_avathar_logo.getText().equalsIgnoreCase("File format is not supported.")) {
			Assert.assertEquals(create.create_invlaid_avathar_logo.getText(), "File format is not supported.",
					"File format is not supported.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated File format is not supported.");// ScreenShot
																									// capture

		} else {
			Assert.assertNotEquals(create.create_invlaid_avathar_logo.getText(), "File format is not supported..",
					"File format is not supported.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation File format is not supported. is failed");// ScreenShot
																												// capture
		}

		closeicon();

	}

	public void CreatecontatName_nameandMobilenumber() throws Exception {

		openCreateContact();

		onlyName();
		onlyMobileNumber();
		createButton();
		if (create.create_contact_Successfully.getText().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact not successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");// ScreenShot capture
		}

	}

	public void Createcontat_invalidEmail() throws Exception {

		openCreateContact();

		onlyName();
		onlyMobileNumber();
		textbox(create.create_contac_Email).sendKeys("123");
		logger.info("Email id :123");
		wait(5);
		logger.info("wait for 5 sec");

		createButton();
		if (create.create_contac_invalid_Email.getText().equalsIgnoreCase("Enter valid email.")) {
			Assert.assertEquals(create.create_contac_invalid_Email.getText(), "Enter valid email.",
					"Enter valid email.");
			TestUtil.takeScreenshotAtEndOfTest("Enter valid email.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contac_invalid_Email.getText(), "Enter valid email.",
					"Enter valid email..");
			TestUtil.takeScreenshotAtEndOfTest("Enter valid email. is failed");// ScreenShot capture
		}

		closeicon();

	}

	public void CreatecontatName_Morethan50tags() throws Exception {

		openCreateContact();

		createcontactdetails();
		if (create.create_contac_morethan_50tags.getText()
				.equalsIgnoreCase("This contact has reached the maximum number of tag")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(),
					"This contact has reached the maximum number of tag",
					"This contact has reached the maximum number of tag");
			TestUtil.takeScreenshotAtEndOfTest("This contact has reached the maximum number of tag");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(create.create_contac_morethan_50tags.getText(),
					"This contact has reached the maximum number of tag",
					"This contact has reached the maximum number of tag");
			TestUtil.takeScreenshotAtEndOfTest("This contact has reached the maximum number of tag");// ScreenShot
																										// capture
		}

		closeicon();

	}

	public void CreatecontatName_MobileNumber_Already_Exist() throws Exception {

		openCreateContact();

		onlyName();

		textbox(create.create_contact_phonenum).sendKeys("3068016736");
		logger.info("Existing Mobile Number 3068016736");
		wait(5);
		logger.info("wait for 5 sec");

		createButton();
		if (create.create_contac_phonenum_alreayExist.getText()
				.equalsIgnoreCase("Contact with same phone number already exists in o")) {
			Assert.assertEquals(create.create_contac_phonenum_alreayExist.getText(),
					"Contact with same phone number already exists in o",
					"Contact with same phone number already exists in o");
			TestUtil.takeScreenshotAtEndOfTest("Contact with same phone number already exists in o");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(create.create_contac_phonenum_alreayExist.getText(),
					"Contact with same phone number already exists in o",
					"Contact with same phone number already exists in o");
			TestUtil.takeScreenshotAtEndOfTest("Contact with same phone number already exists in o case is failed");// ScreenShot
																													// capture
		}

		closeicon();

	}

	public void Createcontat_Tagsnotallowing_Special_Characters() throws Exception {

		openCreateContact();

		onlyName();
		onlyMobileNumber();

		textbox(create.create_contac_tags).sendKeys("-!~:;)(");
		wait(5);
		if (create.special_char_notallowed.getText().equalsIgnoreCase("Special characters not allowed.")) {
			Assert.assertEquals(create.special_char_notallowed.getText(), "Special characters not allowed.",
					"Special characters not allowed.");
			TestUtil.takeScreenshotAtEndOfTest("Special characters not allowed.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.special_char_notallowed.getText(), "Special characters not allowed.",
					"Special characters not allowed.");
			TestUtil.takeScreenshotAtEndOfTest("Special characters not allowed. case failed");// ScreenShot capture
		}
		closeicon();

	}

	public void Createcontat_DuplicateTAGS() throws Exception {

		openCreateContact();

		createcontactNameandMobilenumber();

		textbox(create.create_contac_tags).sendKeys("1,1");
		logger.info("Dulicate tags entered 1,1");
		wait(5);

		logger.info("wait for 5 sec");
		if (create.duplicate_tags.getText().equalsIgnoreCase("Duplicate tags are not allowed.")) {
			Assert.assertEquals(create.duplicate_tags.getText(), "Duplicate tags are not allowed.",
					"Duplicate tags are not allowed.");
			TestUtil.takeScreenshotAtEndOfTest("Duplicate tags are not allowed.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.duplicate_tags.getText(), "Duplicate tags are not allowed.",
					"Duplicate tags are not allowed.");
			TestUtil.takeScreenshotAtEndOfTest("Duplicate tags are not allowed.case failed");// ScreenShot capture
		}

		closeicon();

	}

	public void CreatecontatName_givespaceatend() throws Exception {

		openCreateContact();

		textbox(create.create_contact_Name).sendKeys("sample ");
		logger.info("Contact Name at the end given one space");
		wait(5);
		onlyMobileNumber();
		createButton();
		if (create.create_contact_Successfully.getText().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");// ScreenShot capture
		}

	}

	public void CreatecontatCompanyName_givespaceatend() throws Exception {

		openCreateContact();
		textbox(create.create_contact_Name).sendKeys("sample ");
		logger.info("Contact Name at the end given one space");
		wait(5);
		onlyMobileNumber();
		textbox(create.create_contac_companyName).sendKeys("tech  ");
		logger.info("Company name at the end giving one space");
		wait(5);
		logger.info("wait for 5 sec");

		createButton();
		if (create.create_contact_Successfully.getText().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(create.create_contact_Successfully.getText(), "Created contact successfully.",
					"Created contact successfully.");
			logger.info("Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");// ScreenShot capture
		}
	}

}
