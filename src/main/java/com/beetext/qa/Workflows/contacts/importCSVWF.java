package com.beetext.qa.Workflows.contacts;


import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.ImportCSVWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.importCSV;
import com.beetext.qa.util.TestUtil;

public class importCSVWF  extends ImportCSVWFCM {
	
	String url, username1, username2, password;
	Repository repository = new Repository();
	importCSV importCSV= new importCSV();
	TestBase testBase =new TestBase();

	
	public void importValidCSV() throws Exception {
	
		//login(map);
		clickonimportcsvicon();
		
		
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\importcsv.csv");
		System.out.println(currentDir + "\\Files\\importcsv.csv");
		logger.info("csv file is added");
		waits(19);
		logger.info("wait for 19 sec");
		
		button(importCSV.importcontacts_button, "import SVG").click();
		logger.info("clicked on import contacts button");
		wait(12);
		
		logger.info("wait for 12 sec");
		importCSV.import_contact_headder.getText();
		
		if (importCSV.import_contact_headder.getText().equalsIgnoreCase("Import Contacts Report")) {
			Assert.assertEquals(importCSV.import_contact_headder.getText(), "Import Contacts Report", "Import Contacts Report");
			
			logger.info("Valid CSV case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Import Contacts Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.import_contact_headder.getText(), "Import Contacts Report", "Import Contacts Report");
			logger.warn("Valid CSV case  not Working");
			TestUtil.takeScreenshotAtEndOfTest("Import Contacts Report");// ScreenShot capture
		}
		
		
		importCSV.totalimportscontacts_text.getText();
		
		if (importCSV.totalimportscontacts_text.getText().equalsIgnoreCase("Total Contacts Imported :")) {
			Assert.assertEquals(importCSV.totalimportscontacts_text.getText(), "Total Contacts Imported :", "Total Contacts Imported :");
			
			logger.info("Valid CSV case  Total contacts text is matched");
			TestUtil.takeScreenshotAtEndOfTest("Total Contacts Imported : Contacts Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.totalimportscontacts_text.getText(), "Total Contacts Imported :", "Total Contacts Imported :");
			logger.warn("Valid CSV case  Total contacts text is not matched");
			TestUtil.takeScreenshotAtEndOfTest("Total Contacts Imported :Import Contacts Report");// ScreenShot capture
		}
		
		
		////	button(importCSV.totalimportscontacts_count1,"Button").click();
		wait(2);
		importCSV.totalimportscontacts_count.getText();
		
		if (importCSV.totalimportscontacts_count.getText().equalsIgnoreCase("19")) {
			Assert.assertEquals(importCSV.totalimportscontacts_count.getText(), "19", "19");
			
			logger.info("Valid CSV case  Total count is matched");
			TestUtil.takeScreenshotAtEndOfTest("Total Import Contacts  count Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.totalimportscontacts_count.getText(), "19", "19");
			logger.warn("Valid CSV case  Total count is not matched");
			TestUtil.takeScreenshotAtEndOfTest("Total Import Contacts count Report");// ScreenShot capture
		}
		
		
		importCSV.successimportscontacts_text.getText();
		
		if (importCSV.successimportscontacts_text.getText().equalsIgnoreCase("Successfully Imported :")) {
			Assert.assertEquals(importCSV.successimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			logger.info("Valid CSV case  successfully imported Text is matched");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported  Contacts Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.successimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			logger.warn("Valid CSV case  successfully imported Text is not matched");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report");// ScreenShot capture
		}
		
		////	button(importCSV.totalimportscontacts_count1,"Button").click();
		wait(2);
		importCSV.successimportscontacts_count.getText();
		
		if (importCSV.successimportscontacts_count.getText().equalsIgnoreCase("12")) {
			Assert.assertEquals(importCSV.successimportscontacts_count.getText(), "12", "12");
			logger.info("Valid CSV case  successfully imported coount  is matched");
			
			TestUtil.takeScreenshotAtEndOfTest("successfully Import Contacts  count Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.successimportscontacts_count.getText(), "12", "12");
			logger.warn("Valid CSV case  successfully imported coount  is not matched");
			TestUtil.takeScreenshotAtEndOfTest("successfuly Import Contacts count Report");// ScreenShot capture
		}
		
		
		importCSV.failureimportscontacts_text.getText();
		
		if (importCSV.failureimportscontacts_text.getText().equalsIgnoreCase("Successfully Imported :")) {
			Assert.assertEquals(importCSV.failureimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			logger.info("Valid CSV case failure imported text  is matched");
			
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported  Contacts Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.failureimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			logger.warn("Valid CSV case  failure imported text  is not matched");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report");// ScreenShot capture
		}
		
	//	//	button(importCSV.totalimportscontacts_count1,"Button").click();
		wait(2);
		
		importCSV.failureimportscontacts_count.getText();
		
		if (importCSV.failureimportscontacts_count.getText().equalsIgnoreCase("7")) {
			Assert.assertEquals(importCSV.failureimportscontacts_count.getText(), "7", "7");
			
			logger.info("Valid CSV case failure imported count  is matched");
			TestUtil.takeScreenshotAtEndOfTest("failure Imported  Contacts Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.failureimportscontacts_count.getText(), "7", "7");
			logger.info("Valid CSV case failure imported count  is not matched");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report");// ScreenShot capture
		}
		
		button(importCSV.closeButton,"Button").click();
		wait(2);
		
		
	}
	
	
	public void importinValidCSV() throws Exception {
		//login(map);
		clickonimportcsvicon();
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");
		System.out.println(currentDir + "\\Files\\importcsv.csv");
		logger.info("file selected");

		importCSV.invalid_CSV.getText();
		
		if (importCSV.invalid_CSV.getText().equalsIgnoreCase("Please import your contacts using a CSV file.")) {
			Assert.assertEquals(importCSV.invalid_CSV.getText(), "Please import your contacts using a CSV file.");
			
			logger.info("Upload In valid CSV  Format file case is working fine");
			TestUtil.takeScreenshotAtEndOfTest("Please import your contacts using a CSV file.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.invalid_CSV.getText(), "Please import your contacts using a CSV file.", "Please import your contacts using a CSV file.");
			logger.warn("Upload In valid CSV  Format file case is not working ");
			TestUtil.takeScreenshotAtEndOfTest("Please import your contacts using a CSV file.");// ScreenShot capture
		}
		
		
	}

	
	public void DownloadsampleCSV() throws Exception {
		//login(map);
		clickonimportcsvicon();
		
		
		button(importCSV.download_sample_CSV, "import SVG").click();
		logger.info("improt contacts button is clicked");
		waits(19);
		logger.info("wait for 19 sec");
		

		/*importCSV.invalid_CSV.getText();
		System.out.println(importCSV.invalid_CSV.getText());
		if (importCSV.invalid_CSV.getText().equalsIgnoreCase("Please import your contacts using a CSV file.")) {
			Assert.assertEquals(importCSV.invalid_CSV.getText(), "Please import your contacts using a CSV file.");
			System.out.println("Please import your contacts using a CSV file. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Please import your contacts using a CSV file.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.invalid_CSV.getText(), "Please import your contacts using a CSV file.", "Please import your contacts using a CSV file.");
			TestUtil.takeScreenshotAtEndOfTest("Please import your contacts using a CSV file.");// ScreenShot capture
		}
		*/
		button(importCSV.crossMark,"closeButton").click();
		
	}
	
	
	
	public void import_empty_CSV() throws Exception {
		//login(map);
		clickonimportcsvicon();
		
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\emptycsv.csv");
		System.out.println(currentDir + "\\Files\\emptycsv.csv");
		logger.info("Empty csv file is selected");
		waits(19);
		logger.info("wait for 19 sec");
		
		button(importCSV.importcontacts_button, "import SVG").click();
		logger.info("clicked on import contacts button");
		wait(12);
		
		logger.info("wait for 12 sec");
		importCSV.import_contact_headder.getText();
		
		if (importCSV.import_contact_headder.getText().equalsIgnoreCase("Import Contacts Report")) {
			Assert.assertEquals(importCSV.import_contact_headder.getText(), "Import Contacts Report", "Import Contacts Report");
			
			logger.info("Upload Empty CSV  Format file case is working fine");
			TestUtil.takeScreenshotAtEndOfTest("Import Empty Contacts Report");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.import_contact_headder.getText(), "Import Contacts Report", "Import Contacts Report");
			logger.warn("Upload Empty CSV  Format file case is  not working");
			TestUtil.takeScreenshotAtEndOfTest("Import Empty Contacts Report");// ScreenShot capture
		}
		
		
		importCSV.totalimportscontacts_text.getText();
		
		if (importCSV.totalimportscontacts_text.getText().equalsIgnoreCase("Total Contacts Imported :")) {
			Assert.assertEquals(importCSV.totalimportscontacts_text.getText(), "Total Contacts Imported :", "Total Contacts Imported :");
			
			TestUtil.takeScreenshotAtEndOfTest("Total Contacts Imported : Contacts Report2");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.totalimportscontacts_text.getText(), "Total Contacts Imported :", "Total Contacts Imported :");
			TestUtil.takeScreenshotAtEndOfTest("Total Contacts Imported :Import Contacts Report2");// ScreenShot capture
		}
		
		
	////	button(importCSV.totalimportscontacts_count1,"Button").click();
		wait(2);
		importCSV.totalimportscontacts_count1.getText();
		
		if (importCSV.totalimportscontacts_count1.getText().equalsIgnoreCase("0")) {
			Assert.assertEquals(importCSV.totalimportscontacts_count1.getText(), "0", "0");
			
			TestUtil.takeScreenshotAtEndOfTest("Total Import Contacts  count Report2");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.totalimportscontacts_count1.getText(), "0", "0");
			TestUtil.takeScreenshotAtEndOfTest("Total Import Contacts count Report2");// ScreenShot capture
		}
		
		
		importCSV.successimportscontacts_text.getText();
		
		if (importCSV.successimportscontacts_text.getText().equalsIgnoreCase("Successfully Imported :")) {
			Assert.assertEquals(importCSV.successimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported  Contacts Report2");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.successimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report2");// ScreenShot capture
		}
		
		button(importCSV.successimportscontacts_count1,"Button").click();
		wait(2);
		importCSV.successimportscontacts_count1.getText();
		
		if (importCSV.successimportscontacts_count1.getText().equalsIgnoreCase("0")) {
			Assert.assertEquals(importCSV.successimportscontacts_count1.getText(), "0", "0");
			
			TestUtil.takeScreenshotAtEndOfTest("successfully Import Contacts  count Report2");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.successimportscontacts_count1.getText(), "0", "0");
			TestUtil.takeScreenshotAtEndOfTest("successfuly Import Contacts count Report2");// ScreenShot capture
		}
		
		
		importCSV.failureimportscontacts_text.getText();
		
		if (importCSV.failureimportscontacts_text.getText().equalsIgnoreCase("Successfully Imported :")) {
			Assert.assertEquals(importCSV.failureimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported  Contacts Report2");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.failureimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report2");// ScreenShot capture
		}
		
		
		button(importCSV.failureimportscontacts_count1,"Button").click();
		wait(2);
		importCSV.failureimportscontacts_count1.getText();
	
		if (importCSV.failureimportscontacts_count1.getText().equalsIgnoreCase("0")) {
			Assert.assertEquals(importCSV.failureimportscontacts_count1.getText(), "0", "0");
			
			TestUtil.takeScreenshotAtEndOfTest("failure Imported  Contacts Report2");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.failureimportscontacts_count1.getText(), "0", "0");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report2");// ScreenShot capture
		}
		button(importCSV.closeButton,"Button").click();
		wait(2);
	}

	
	
	
	public void import_inValidCSV_validCSV() throws Exception {
		//login(map);
		clickonimportcsvicon();
		
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.mp3");
		System.out.println(currentDir + "\\Files\\importcsv.csv");
		logger.info("invalid csv format file is uploaded");

		importCSV.invalid_CSV.getText();
		
		if (importCSV.invalid_CSV.getText().equalsIgnoreCase("Please import your contacts using a CSV file.")) {
			Assert.assertEquals(importCSV.invalid_CSV.getText(), "Please import your contacts using a CSV file.");
			
			TestUtil.takeScreenshotAtEndOfTest("Please import your contacts using a CSV file.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.invalid_CSV.getText(), "Please import your contacts using a CSV file.", "Please import your contacts using a CSV file.");
			TestUtil.takeScreenshotAtEndOfTest("Please import your contacts using a CSV file.");// ScreenShot capture
		}
		
		wait(7);
		logger.info("wait for 2 sec");
		button(importCSV.select_file, "import SVG").click();
		logger.info("Again click on select file button");
		waits(12);
		logger.info("wait for 19 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\importcsv.csv");
		System.out.println(currentDir + "\\Files\\importcsv.csv");
		
		logger.info("uploading valid csv ");
		
        waits(19);
        logger.info("wait for 19 sec");
		
		button(importCSV.importcontacts_button, "import SVG").click();
		logger.info("clickon import contacts button");
		wait(12);
		
		logger.info("wait for 12 sec");
		importCSV.import_contact_headder.getText();
		
		if (importCSV.import_contact_headder.getText().equalsIgnoreCase("Import Contacts Report")) {
			Assert.assertEquals(importCSV.import_contact_headder.getText(), "Import Contacts Report", "Import Contacts Report");
			logger.info("Upload valid CSV  Format file case is working fine");
			TestUtil.takeScreenshotAtEndOfTest("Import Contacts Report1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.import_contact_headder.getText(), "Import Contacts Report", "Import Contacts Report");
			logger.info("Upload valid CSV  Format file case is Not working");
			TestUtil.takeScreenshotAtEndOfTest("Import Contacts Report1");// ScreenShot capture
		}
		
		
		importCSV.totalimportscontacts_text.getText();
		if (importCSV.totalimportscontacts_text.getText().equalsIgnoreCase("Total Contacts Imported :")) {
			Assert.assertEquals(importCSV.totalimportscontacts_text.getText(), "Total Contacts Imported :", "Total Contacts Imported :");
			logger.info("Total Contacts Imported test is matching");
			TestUtil.takeScreenshotAtEndOfTest("Total Contacts Imported : Contacts Report1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.totalimportscontacts_text.getText(), "Total Contacts Imported :", "Total Contacts Imported :");
			logger.warn("Total Contacts Imported test is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("Total Contacts Imported :Import Contacts Report1");// ScreenShot capture
		}
		
		
		//	button(importCSV.totalimportscontacts_count1,"Button").click();
		wait(2);
		importCSV.totalimportscontacts_count.getText();
		if (importCSV.totalimportscontacts_count.getText().equalsIgnoreCase("19")) {
			Assert.assertEquals(importCSV.totalimportscontacts_count.getText(), "19", "19");
			logger.info("Total Contacts Imported count is matching");
			TestUtil.takeScreenshotAtEndOfTest("Total Import Contacts  count Report1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.totalimportscontacts_count.getText(), "19", "19");
			logger.warn("Total Contacts Imported count is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Total Import Contacts count Report1");// ScreenShot capture
		}
		
		
		importCSV.successimportscontacts_text.getText();
		
		if (importCSV.successimportscontacts_text.getText().equalsIgnoreCase("Successfully Imported :")) {
			Assert.assertEquals(importCSV.successimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			
			logger.info("successfully  Imported test is matching");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported  Contacts Report1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.successimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			logger.info("successfully  Imported test is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report1");// ScreenShot capture
		}
		
		//	button(importCSV.totalimportscontacts_count1,"Button").click();
		wait(2);
		importCSV.successimportscontacts_count.getText();
		if (importCSV.successimportscontacts_count.getText().equalsIgnoreCase("12")) {
			Assert.assertEquals(importCSV.successimportscontacts_count.getText(), "12", "12");
			logger.info("successfully  Imported count is matching");
			TestUtil.takeScreenshotAtEndOfTest("successfully Import Contacts  count Report1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.successimportscontacts_count.getText(), "12", "12");
			logger.warn("successfully  Imported count is not matching");
			TestUtil.takeScreenshotAtEndOfTest("successfuly Import Contacts count Report1");// ScreenShot capture
		}
		
		
		importCSV.failureimportscontacts_text.getText();
		System.out.println(importCSV.failureimportscontacts_text.getText());
		if (importCSV.failureimportscontacts_text.getText().equalsIgnoreCase("Successfully Imported :")) {
			Assert.assertEquals(importCSV.failureimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			System.out.println("Successfully Imported :case is verified");
			logger.info("failure  Imported test is matching");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported  Contacts Report1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.failureimportscontacts_text.getText(), "Successfully Imported :", "Successfully Imported :");
			logger.warn("failure   Imported test is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report1");// ScreenShot capture
		}
		
		//	button(importCSV.totalimportscontacts_count1,"Button").click();
		wait(2);
		
		importCSV.failureimportscontacts_count.getText();
		if (importCSV.failureimportscontacts_count.getText().equalsIgnoreCase("7")) {
			Assert.assertEquals(importCSV.failureimportscontacts_count.getText(), "7", "7");
			logger.info("failure  Imported count is matching");
			TestUtil.takeScreenshotAtEndOfTest("failure Imported  Contacts Report1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(importCSV.failureimportscontacts_count.getText(), "7", "7");
			logger.warn("failure  Imported test is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Successfully Imported Import Contacts Report1");// ScreenShot capture
		}
		button(importCSV.closeButton,"Button").click();
		wait(2);
		
	}

}
