package com.beetext.qa.Workflows.settings;


import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.SettingsAgentinviteWFCM;
import com.beetext.qa.WFCommonMethod.SettingsCreateDepartmentWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.SettingsCreateDepartment;
import com.beetext.qa.util.TestUtil;

public class SettingsCreateDepartmentWF extends SettingsCreateDepartmentWFCM {
	String url, username1, username2, password;
	Repository repository = new Repository();
	SettingsCreateDepartment createDept = new SettingsCreateDepartment();
	TestBase testBase = new TestBase();
	SettingsAgentinviteWFCM sa = new SettingsAgentinviteWFCM();

	public void DeptName_required_validation() throws Exception {

		login();
		selectcreatenumberoption();
		createbutton();

		textbox(createDept.deptName).sendKeys("");
		logger.info("Department name is sending empty");

		wait(5);
		logger.info("wait for 5 sec");

		textbox(createDept.areacode).sendKeys("888");
		logger.info("Aread code entering : 888");

		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.deptName_required.getText());
		if (createDept.deptName_required.getText().equalsIgnoreCase("Please give your number a name.")) {
			Assert.assertEquals(createDept.deptName_required.getText(), "Please give your number a name.",
					"Please give you number a name");
			logger.info("department name required validation is working");
			logger.info("Department name Required validation case working fine");
			TestUtil.takeScreenshotAtEndOfTest("SettingsDepartmentnameEmpty");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptName_required.getText(), "Please give your number a name.",
					"Please give your number a name.");
			logger.info("department name required validation is not working");
			logger.warn("Department name Required validation case  is not working");
			TestUtil.takeScreenshotAtEndOfTest("settingsDepartmnetNameEmpty");// ScreenShot capture
		}

	}

	public void DeptName_required_3char_validation() throws Exception {

		login();
		selectcreatenumberoption();
		createbutton();

		textbox(createDept.deptName).sendKeys("12");
		logger.info("Department Name text box entering 12");

		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.deptName_required_min3_char.getText());
		if (createDept.deptName_required_min3_char.getText()
				.equalsIgnoreCase("Your Number name must contain at least 3 character")) {
			Assert.assertEquals(createDept.deptName_required_min3_char.getText(),
					"Your Number name must contain at least 3 character",
					"Your Number name must contain at least 3 character");
			logger.info("department name required min 3 char validation is working");
			logger.info("Department Name minimum 3 char validation checking");
			TestUtil.takeScreenshotAtEndOfTest("SettingsDepartmentname min 3 char validation");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptName_required_min3_char.getText(),
					"Your Number name must contain at least 3 character",
					"Your Number name must contain at least 3 character");
			logger.info("department name required min 3 char validation is not working");
			logger.info("Department name minimum 3 char validation case not working");
			TestUtil.takeScreenshotAtEndOfTest("settingsDepartmnetName min 3 char validation");// ScreenShot capture
		}

	}

	public void areaCode_required_validation() throws Exception {

		login();
		selectcreatenumberoption();
		createbutton();

		textbox(createDept.areacode).sendKeys("");
		logger.info("aread code entering empty");

		wait(5);

		logger.info("wait for 2 sec");
		textbox(createDept.deptName).sendKeys("");
		logger.info("department name entering empty");

		wait(5);
		logger.info("wait for 2 sec");

		logger.info(createDept.areacode_required.getText());
		if (createDept.areacode_required.getText().equalsIgnoreCase("Area Code is required.")) {
			Assert.assertEquals(createDept.areacode_required.getText(), "Area Code is required.",
					"Area Code is required.");
			logger.info("area code is required validation is working");
			logger.info("Area code required validation case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Settings area code empty");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.areacode_required.getText(), "Area Code is required.",
					"Area Code is required.");
			logger.info("Area Code is required validation is not working");
			logger.warn("Area code required validation case  not working");
			TestUtil.takeScreenshotAtEndOfTest("settingsDepartmnetNameEmpty");// ScreenShot capture
		}

	}

	public void invalidareaCode_required_validation() throws Exception {

		login();
		selectcreatenumberoption();
		createbutton();

		textbox(createDept.areacode).sendKeys("abcd");
		logger.info("area code entered abcd");

		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.areacode_mustbe_letters.getText());
		if (createDept.areacode_mustbe_letters.getText()
				.equalsIgnoreCase("Please enter a valid three-digit area code.")) {
			Assert.assertEquals(createDept.areacode_mustbe_letters.getText(),
					"Please enter a valid three-digit area code.", "Please enter a valid three-digit area code.");
			logger.info("Please enter a valid three-digit area code. validation is working");
			logger.info("invalid area code case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Settings Please enter a valid three-digit area code.");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(createDept.areacode_mustbe_letters.getText(),
					"Please enter a valid three-digit area code.", "Please enter a valid three-digit area code.");
			logger.info("Please enter a valid three-digit area code. is not working");
			logger.info("invalid area code case is not working");
			TestUtil.takeScreenshotAtEndOfTest("settings Please enter a valid three-digit area code.");// ScreenShot
																										// capture
		}

	}

	public void createTollfreeDept() throws Exception {

		login();
		selectcreatenumberoption();
		createbutton();

		textbox(createDept.deptName).sendKeys("Tollfree");
		logger.info("Department name is entering : Tollfree");

		wait(5);
		logger.info("wait for 2 sec");
		textbox(createDept.areacode).sendKeys("888");
		logger.info("Area code : 888");

		wait(5);
		logger.info("wait for 2 sec");

		button(createDept.createbtn, "button").click();
		logger.info("create button is clicked");

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Number Created Successfully. validation is working");
			logger.info("Tollfree numebr created successfully");
			TestUtil.takeScreenshotAtEndOfTest("Settings Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Settings Tollfree department creation  is not working");
			logger.warn("Tollfree numebr  not created successfully");
			TestUtil.takeScreenshotAtEndOfTest("ettings Tollfree department creation ");// ScreenShot capture
		}

		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.ManageBtn1, "button").click();
		logger.info("clicked on manage button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.DeleteDept, "button").click();
		logger.info("clicked on delete dept button");
		waits(2);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("clicked on radio button one");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn2, "button").click();
		logger.info("Clicked on radio button 2");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn3, "button").click();
		logger.info("clicked on radio button 3");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn4, "button").click();
		logger.info("clicked on radio button 4");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn5, "button").click();
		logger.info("clicked on radio button 5");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn6, "button").click();
		logger.info("clicked on radio button6");
		waits(2);
		logger.info("wait for 5 sec");
		button(createDept.confirmDelete, "button").click();
		logger.info("clicked on confirm delete button");
		waits(3);
		logger.info("wait for 5 sec");

	}

	public void CreateBtn_Enabled() throws Exception {

		login();
		selectcreatenumberoption();
		createbutton();
		textbox(createDept.deptName).sendKeys("sample");
		logger.info("Deaprtment name entered : sample");

		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.areacode).sendKeys("888");
		logger.info("Area code: 888");

		waits(2);
		if (verifyElementIsEnabled(createDept.createbtn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.createbtn));
			button(createDept.createbtn, "button").isEnabled();
			logger.info("create Department button is enabled");
			logger.info("create button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.createbtn));
			logger.info("create department button is disabled");
			logger.info("create button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void TextEnabledExistingDepartment_CreateBtn_Enabled() throws Exception {

		login();
		selectcreatenumberoption();

		logger.info("wait for 5 sec");
		if (verifyElementIsEnabled(createDept.txtEnabled_Existing_Number_Btn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.txtEnabled_Existing_Number_Btn));
			button(createDept.txtEnabled_Existing_Number_Btn, "button").isEnabled();
			logger.info("create Department TestEnabled button is enabled");
			logger.info("TextEnabled button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("TestEnableButtonIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.txtEnabled_Existing_Number_Btn));
			logger.info("create department Testenabled  button is disabled");
			logger.warn("txtenabledbutton is disbaled state");
			TestUtil.takeScreenshotAtEndOfTest("TestEnabledButtonisEnabledTestCase");// ScreenShot capture
		}

	}

	public void TextEnabledExistingDepartment_Entervalid_department_Name() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		textbox(createDept.DeptName).sendKeys("Dummy");
		logger.info("Department name entering Dummy");
		wait(2);
		logger.info("wait for 2 sec");

		if (verifyElementIsEnabled(createDept.NextButton)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.NextButton));
			button(createDept.NextButton, "button").isEnabled();
			logger.info("create Department TestEnabled button is enabled");
			logger.info("Next button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("TestEnabledDepartment Next ButtonIsEnabledTestCase");// ScreenShot
																										// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.NextButton));
			logger.info("create department next   button is disabled");
			logger.info("next button is disbaled state");
			TestUtil.takeScreenshotAtEndOfTest("TestEnabledDepartment Next ButtonisEnabledTestCase");// ScreenShot
																										// capture
		}

	}

	public void TextEnabledExistingDepartment_Required_validation() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		textbox(createDept.DeptName).sendKeys("");
		logger.info("Department name entering empty");
		wait(2);
		button(createDept.outsideClick, "button").click();
		logger.info("click on outside of the pop up");
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.department_required_valid.getText());
		if (createDept.department_required_valid.getText().equalsIgnoreCase("Please give your number a name.")) {
			Assert.assertEquals(createDept.department_required_valid.getText(), "Please give your number a name.",
					"Please give your number a name.");
			logger.info("department Please give your number a name. is working");
			logger.info("Department name required validation working fine");
			TestUtil.takeScreenshotAtEndOfTest("Settings Please give your number a name. Successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(createDept.department_required_valid.getText(), "Please give your number a name.",
					"Please give your number a name.");
			logger.info("Settings Exsting  Please give your number a name. is not working");
			logger.info("Department name required validation not working");
			TestUtil.takeScreenshotAtEndOfTest("existings Please give your number a name. required creation ");// ScreenShot
																												// capture
		}

	}

	public void TextEnabledExistingDepartment_3charRequired_validation() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		textbox(createDept.DeptName).sendKeys("12");
		logger.info("Department name entering: 12");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(createDept.department_required_3char_valid.getText());
		if (createDept.department_required_3char_valid.getText()
				.equalsIgnoreCase("Your Number name must contain at least 3 character")) {
			Assert.assertEquals(createDept.department_required_3char_valid.getText(),
					"Your Number name must contain at least 3 character",
					"Your Number name must contain at least 3 character");
			logger.info("department Please give your number a name. is working");
			logger.info("Department name minimum 3 chars validation checking");
			TestUtil.takeScreenshotAtEndOfTest("Settings Please give your number a name. Successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(createDept.department_required_3char_valid.getText(),
					"Your Number name must contain at least 3 character",
					"Your Number name must contain at least 3 character");
			logger.info("Settings Exsting  Your Number name must contain at least 3 characteris not working");
			logger.warn("Department name minimum 3 characters validation case not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"existings Your Number name must contain at least 3 character required creation ");// ScreenShot
																										// capture
		}

	}

	public void phoneNumberinfo_customername_3charRequired_validation() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		textbox(createDept.DeptName).sendKeys("sample");
		logger.info("Department name entering sample");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.NextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.cname).sendKeys("sa");
		logger.info("customer name : sa");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(createDept.cname_3char_required_validation.getText());
		if (createDept.cname_3char_required_validation.getText()
				.equalsIgnoreCase("Name must be at least 3 characters.")) {
			Assert.assertEquals(createDept.cname_3char_required_validation.getText(),
					"Name must be at least 3 characters.", "Name must be at least 3 characters.");
			logger.info("phone number info  Name must be at least 3 characters. is working");
			logger.info("customer name minimum 3 chars validation checking case working fine");
			TestUtil.takeScreenshotAtEndOfTest("phone number info  Name must be at least 3 characters.  Successfully");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(createDept.cname_3char_required_validation.getText(),
					"Name must be at least 3 characters", "Name must be at least 3 characters");
			logger.info("phone number info  Name must be at least 3 characters not working");
			logger.warn("customer name minimum 3 chars validation checking case not working");
			TestUtil.takeScreenshotAtEndOfTest("phone number info  Name must be at least 3 characters failed ");// ScreenShot
																												// capture
		}

	}

	public void phoneNumberinfo_zipcode_allowonly_numbers_validation() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		textbox(createDept.DeptName).sendKeys("sample");
		logger.info("Department name :sample");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.NextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.zipcode).sendKeys("sa");
		logger.info("zip code enter : sa");
		wait(2);
		logger.info("wait for 2 sec");

		logger.info(createDept.zipcode_allow_only_numbers.getText());
		if (createDept.zipcode_allow_only_numbers.getText().equalsIgnoreCase("Only numbers are allowed.")) {
			Assert.assertEquals(createDept.zipcode_allow_only_numbers.getText(), "Only numbers are allowed.",
					"Only numbers are allowed.");
			logger.info("phone number info  Only numbers are allowed. is working");
			logger.info("invalid zip code case working fine");
			TestUtil.takeScreenshotAtEndOfTest("phone number info  zipcode Only numbers are allowed.  Successfully");// ScreenShot
																														// capture

		} else {
			Assert.assertNotEquals(createDept.zipcode_allow_only_numbers.getText(), "Only numbers are allowed.",
					"Only numbers are allowed.");
			logger.info("phone number info Only numbers are allowed. not working");
			logger.warn("invalid zip code case not working");
			TestUtil.takeScreenshotAtEndOfTest("phone number info zipcode  Only numbers are allowed. failed ");// ScreenShot
																												// capture
		}

	}

	public void phoneNumberinfo_checkanduncheck_radiobutton_validation() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		textbox(createDept.DeptName).sendKeys("sample789");
		logger.info("Department Name: sample789");
		wait(2);
		logger.info("wait for 5 sec");
		button(createDept.NextButton, "button").click();
		logger.info("click on next button");
		wait(10);
		logger.info("wait for 10 sec");

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("click on radio button");

		wait(5);
		logger.info("wait for 5 sec");

		checkbox(createDept.radioButton, "checkbox").click();

		logger.info("un click on radio button");
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.checkradiobtn_validation.getText());
		if (createDept.checkradiobtn_validation.getText()
				.equalsIgnoreCase("Please read the terms of service and accept.")) {
			Assert.assertEquals(createDept.checkradiobtn_validation.getText(),
					"Please read the terms of service and accept.", "Please read the terms of service and accept.");
			logger.info("phone number info  Please read the terms of service and accept. is working");
			logger.info("check and uncheck of radio button case working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info Please read the terms of service and accept.  Successfully");// ScreenShot
																									// capture

		} else {
			Assert.assertNotEquals(createDept.checkradiobtn_validation.getText(),
					"Please read the terms of service and accept.", "Please read the terms of service and accept.");
			logger.info("phone number info Please read the terms of service and accept. not working");
			logger.warn("check and uncheck of radio button case not working");
			TestUtil.takeScreenshotAtEndOfTest(
					"phone number info Please read the terms of service and accept. failed ");// ScreenShot capture
		}

	}

//up to above working fine

	public void phoneNumberinfo_required_validation() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		textbox(createDept.DeptName).sendKeys("sample");
		logger.info("Department Name: sample");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.NextButton, "button").click();
		logger.info("Clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(createDept.phonenumber).sendKeys("");
		logger.info("phone number info page , phone number text field sending empty data");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.cname).sendKeys("");
		logger.info("Customer name sending empty");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(createDept.cstreet).sendKeys("");
		logger.info("Steet name empty ");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(createDept.ccity).sendKeys("");
		wait(2);

		textbox(createDept.zipcode).sendKeys("");
		logger.info("Zip code sending empty");
		wait(2);
		logger.info("wait for 2 sec");

		selectStage();

		logger.info(createDept.zipcode_required.getText());
		if (createDept.zipcode_required.getText().equalsIgnoreCase("Zip is required.")) {
			Assert.assertEquals(createDept.zipcode_required.getText(), "Zip is required.", "Zip is required.");
			logger.info("phone number info  required validation case is working");
			logger.info("Phone number info page required validation case working fine");
			TestUtil.takeScreenshotAtEndOfTest("phone number info required validation  Successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(createDept.zipcode_required.getText(), "Zip is required.", "Zip is required.");
			logger.info("phone number info Please read the terms of service and accept. not working");
			logger.info("Phone number info page required validation case not working");
			TestUtil.takeScreenshotAtEndOfTest("phone number info required validation case failed ");// ScreenShot
																										// capture
		}

	}

	public void phoneNumberinfo_valid_data_nextbutton_enabled() throws Exception {
		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();

		textbox(createDept.DeptName).sendKeys("sample");
		logger.info("Department Name : sample");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.NextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		String mobile = "555" + randomeNum();
		textbox(createDept.phonenumber).sendKeys(mobile);
		logger.info("Mobile numberr entered" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.cname).sendKeys("sample90");
		logger.info("cusotmer name entered: sample90");
		wait(10);
		logger.info("wait for 10 sec");

		String street = randomestring();

		textbox(createDept.cstreet).sendKeys(street);
		logger.info("street name entered:" + street);
		wait(5);

		logger.info("wait for 5 sec");
		String city = randomestring();

		textbox(createDept.ccity).sendKeys(city);
		logger.info("City name entered:" + city);
		wait(5);
		logger.info("wait for 5 sec");

		textbox(createDept.zipcode).sendKeys("12345");
		logger.info("zip code entered: 12345");
		wait(10);
		logger.info("wait for 10 sec");

		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on terms and conditions radio button");

		wait(5);
		logger.info("wait for 5 sec");

		logger.info("button is enbaled");
		if (verifyElementIsEnabled(createDept.nextButton)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.nextButton));
			button(createDept.nextButton, "button").isEnabled();
			logger.info("Phone Number info page next button is  enabled");
			logger.info("Phone number info page next button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("Phone Number info page Next button Enabled case working fine");// ScreenShot
																												// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.nextButton));
			logger.info("TestEnable Existing button  is disbaled");
			logger.info("Phone numebr info page next button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("Phone Number info page Next button disabled case working fine");// ScreenShot
																												// capture
		}

	}

	public void phoneNumberinfo_valid_data_clickon_callme_button() throws Exception {

		login();
		selectcreatenumberoption();

		selectTxtenabledNumberoption();
		departmentName();
		phonenumberinfopagedetails();

		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on selected  terms and conditions");

		waits(1);
		logger.info("wait for 1 sec");
		button(createDept.nextButton, "button").click();
		logger.info("click on next button");
		waits(3);
		logger.info("wait for 3 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("click on call me button");
		waits(3);
		logger.info("wait for 5 sec");

		logger.info(createDept.verify_page.getText());
		if (createDept.verify_page.getText().equalsIgnoreCase("Verify Your Number")) {
			Assert.assertEquals(createDept.verify_page.getText(), "Verify Your Number", "Verify Your Number");
			logger.info("verification page case is working");
			logger.info("verification page is opening");
			TestUtil.takeScreenshotAtEndOfTest(" varification page open   Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.verify_page.getText(), "Verify Your Number", "Verify Your Number");
			logger.info("verification page is  not working");
			logger.info("verification page is not opening");
			TestUtil.takeScreenshotAtEndOfTest("verfication page opening  case failed ");// ScreenShot capture
		}
		waits(5);
		button(createDept.agent_link, "button").click();
		waits(3);

		button(createDept.Numbers_link, "button").click();
		waits(5);
		button(createDept.sample1_dept, "button").click();
		waits(5);
		button(createDept.click_delete_button, "button").click();

		waits(3);
		button(createDept.click_confirm_delete_button, "button").click();
		waits(2);

	}

	public void valid_Verification_Code() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		sampledepartmentName();
		phonenumberinfopagedetails();

		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on terms and conditonns radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("Clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("clicked on call me button");
		wait(5);
		logger.info("wait for 5 sec");
		waits(3);
		Repository.mobile_otp1.sendKeys("1");
		wait(1);
		Repository.mobile_otp2.sendKeys("2");
		wait(1);
		Repository.mobile_otp3.sendKeys("3");
		wait(1);
		Repository.mobile_otp4.sendKeys("4");
		wait(1);
		Repository.mobile_otp5.sendKeys("5");
		wait(1);
		Repository.mobile_otp6.sendKeys("6");
		wait(2);
		logger.info("Otp entered :123456");
		button(createDept.verify_button, "button").click();
		logger.info("clicked on verify button");
		wait(1);

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully");
			logger.info("Number Created Successfully");
			logger.info("Number created successfully");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Number Created not working");
			logger.info("Number creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture
		}

	}

	public void invalid_Verification_Code() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		departmentName();
		phonenumberinfopagedetails();

		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("Clicked on check terms and conditions radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("Clicked on call me button");
		wait(5);
		logger.info("wait for 5 sec");
		waits(3);
		Repository.mobile_otp1.sendKeys("1");
		wait(1);
		Repository.mobile_otp2.sendKeys("2");
		wait(1);
		Repository.mobile_otp3.sendKeys("3");
		wait(1);
		Repository.mobile_otp4.sendKeys("4");
		wait(1);
		Repository.mobile_otp5.sendKeys("5");
		wait(1);
		Repository.mobile_otp6.sendKeys("9");
		wait(2);
		logger.info("otp entered: 123459");
		button(createDept.verify_button, "button").click();
		logger.info("clicked on verifiy button");

		logger.info(createDept.invalid_otp.getText());
		if (createDept.invalid_otp.getText()
				.equalsIgnoreCase("The 6 digit verification you entered is invalid, please try again.")) {
			Assert.assertEquals(createDept.invalid_otp.getText(),
					"The 6 digit verification you entered is invalid, please try again.",
					"The 6 digit verification you entered is invalid, please try again.");
			logger.info("The 6 digit verification you entered is invalid, please try again.");
			logger.info("invlaid otp case working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"The 6 digit verification you entered is invalid, please try again.Successfully");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(createDept.invalid_otp.getText(),
					"The 6 digit verification you entered is invalid, please try again.",
					"The 6 digit verification you entered is invalid, please try again.");
			logger.info("The 6 digit verification you entered is invalid, please try again. not working");
			logger.info("invlaid otp case failed");
			TestUtil.takeScreenshotAtEndOfTest("The 6 digit verification you entered is invalid, please try again.");// ScreenShot
																														// capture
		}

		waits(5);
		button(createDept.agent_link, "button").click();
		waits(5);

		button(createDept.Numbers_link, "button").click();
		waits(5);
		button(createDept.sample1_dept, "button").click();
		waits(3);
		button(createDept.click_delete_button, "button").click();

		waits(2);
		button(createDept.click_confirm_delete_button, "button").click();
		waits(2);

	}

	public void callME_Again() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		departmentName();
		phonenumberinfopagedetails();

		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on terms and conditions check box");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("Click on next button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("clicked on callme button");
		waits(5);
		logger.info("wait for 5 sec");
		waits(3);
		Repository.mobile_otp1.sendKeys("1");
		wait(1);
		Repository.mobile_otp2.sendKeys("2");
		wait(1);
		Repository.mobile_otp3.sendKeys("3");
		wait(1);
		Repository.mobile_otp4.sendKeys("4");
		wait(1);
		Repository.mobile_otp5.sendKeys("5");
		wait(1);
		Repository.mobile_otp6.sendKeys("9");
		wait(2);
		logger.info("otp entered : 123459");
		button(createDept.verify_button, "button").click();
		logger.info("clicked on verify button");

		waits(10);
		button(createDept.resendcode, "button").click();
		logger.info("clicked on resend code button");

		logger.info(createDept.callme_again.getText());
		if (createDept.callme_again.getText().equalsIgnoreCase(
				"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you.")) {
			Assert.assertEquals(createDept.callme_again.getText(),
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you.",
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you. please try again.");
			logger.info(
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you. please try again.");
			logger.info("Resend otp is sending successfully");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you.Successfully");// ScreenShot
																																															// capture

		} else {
			Assert.assertNotEquals(createDept.callme_again.getText(),
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you.",
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you.");
			logger.info(
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you. not working");
			logger.info("Resend otp is not sending");
			TestUtil.takeScreenshotAtEndOfTest(
					"Your phone number text enablement request was succesfully created. Keep an eye on your phone, if we need any more information we will reach out to you.");// ScreenShot
																																												// capture
		}

		waits(5);
		button(createDept.agent_link, "button").click();
		waits(5);

		button(createDept.Numbers_link, "button").click();
		waits(5);
		button(createDept.sample1_dept, "button").click();
		waits(3);
		button(createDept.click_delete_button, "button").click();

		waits(5);
		button(createDept.click_confirm_delete_button, "button").click();
		wait(10);

	}

	public void clickon_back_button() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		departmentName();
		phonenumberinfopagedetails();

		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on terms and conditions radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("click on call me button ");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.BackBtn, "button").click();
		logger.info("clicked on back button");
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.phonenumberinfo_page.getText());
		if (createDept.phonenumberinfo_page.getText().equalsIgnoreCase("Phone Number Account Info")) {
			Assert.assertEquals(createDept.phonenumberinfo_page.getText(), "Phone Number Account Info",
					"Phone Number Account Info");
			logger.info("back button case working fine Phone Number Account Info");
			logger.info("backbutton case is working fine");
			TestUtil.takeScreenshotAtEndOfTest("Phone Number Account Info.Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.phonenumberinfo_page.getText(), "Phone Number Account Info",
					"Phone Number Account Info");
			logger.info("back butotn case Phone Number Account Info not working");
			logger.info("back button case is not working");
			TestUtil.takeScreenshotAtEndOfTest("Phone Number Account Info");// ScreenShot capture
		}

		waits(5);
		button(createDept.agent_link, "button").click();
		waits(3);

		button(createDept.Numbers_link, "button").click();
		waits(4);
		button(createDept.sample1_dept, "button").click();
		waits(5);
		button(createDept.click_delete_button, "button").click();

		waits(5);
		button(createDept.click_confirm_delete_button, "button").click();
		waits(5);

	}

	public void Alreadyexisting_department_number() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();

		textbox(createDept.DeptName).sendKeys("sample");
		logger.info("Department Name: sample");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.NextButton, "button").click();
		logger.info("Clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		//String mobile = "555" + randomeNum();
		textbox(createDept.phonenumber).sendKeys("2342552999");
		logger.info("Existing mobile number: 2342552999");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.cname).sendKeys("sample");
		logger.info("customer name: sample");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(createDept.cstreet).sendKeys("dummy");
		logger.info("Street Name: dummy");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(createDept.ccity).sendKeys("dummy");
		logger.info("City name: dummy");
		wait(2);
		logger.info("wait for 2 sec");

		textbox(createDept.zipcode).sendKeys("89045");
		logger.info("zip code : 89045");
		wait(2);
		logger.info("wait for 2 sec");

		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on terms and conditions radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("clicked on next button");

		logger.info(createDept.Numberalready_Exist.getText());
		if (createDept.Numberalready_Exist.getText().equalsIgnoreCase(
				"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.")) {
			Assert.assertEquals(createDept.Numberalready_Exist.getText(),
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.",
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.");
			logger.info("Text_Enabled Phone number is already available with other org,  successfully");
			logger.info("Already existing mobile numebr case is working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"Text_Enabled department number Phone number is already available with other org,  is created successfully case is passed");// ScreenShot
																																				// capture

		} else {
			Assert.assertNotEquals(createDept.Numberalready_Exist.getText(),
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.",
					"Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.");
			logger.info("Phone number is already available with other org, ");
			logger.info("Already existing mobile number case not working fine");
			TestUtil.takeScreenshotAtEndOfTest("Phone number is already available with other org, case  is failed ");// ScreenShot
																														// capture
		}

	}

	public void tollfree_clickon_backbutton() throws Exception {

		login();
		selectcreatenumberoption();

		createbutton();

		wait(2);
		button(createDept.BackButton, "button").click();
		logger.info("clicked on back button");
		waits(1);
		logger.info("wait for 1 sec");

		logger.info(createDept.createNumber.getText());
		if (createDept.createNumber.getText()
				.equalsIgnoreCase("Would you like to create a new phone number or text enable an existing landline?")) {
			Assert.assertEquals(createDept.createNumber.getText(),
					"Would you like to create a new phone number or text enable an existing landline?",
					"Would you like to create a new phone number or text enable an existing landline?");
			logger.info("Would you like to create a new phone number or tex  successfully");
			logger.info("click on back button case is working fine");
			TestUtil.takeScreenshotAtEndOfTest(
					"Would you like to create a new phone number or texsuccessfully case is passed");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(createDept.createNumber.getText(),
					"Would you like to create a new phone number or text enable an existing landline?",
					"Would you like to create a new phone number or text enable an existing landline?");
			logger.info("Would you like to create a new phone number or tex");
			logger.info("click on back button case is working fine");
			TestUtil.takeScreenshotAtEndOfTest("Would you like to create a new phone number or tex  is failed ");// ScreenShot
																													// capture
		}

	}

	public void edit_departmentname_min3char_validation() throws Exception {

		login();

		button(createDept.ManageBtn, "button").click();
		logger.info("clicked on manage button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.dept_name_edit, "button").clear();
		logger.info("department name clearing");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.dept_name_edit, "button").sendKeys("sa");
		logger.info("Department name entering : sa");
		wait(5);
		logger.info("wait for 5 sec");
		logger.info(createDept.dept_name_edit_3char_validation.getText());
		if (createDept.dept_name_edit_3char_validation.getText()
				.equalsIgnoreCase("Your Number name must contain at least 3 character.")) {
			Assert.assertEquals(createDept.dept_name_edit_3char_validation.getText(),
					"Your Number name must contain at least 3 character.",
					"Your Number name must contain at least 3 character.");
			logger.info("department name required min 3 char validation is working");
			logger.info("department name required min 3 char validation is working");
			TestUtil.takeScreenshotAtEndOfTest("SettingsDepartmentname min 3 char validation");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.dept_name_edit_3char_validation.getText(),
					"Your Number name must contain at least 3 character",
					"Your Number name must contain at least 3 character");
			logger.info("department name required min 3 char validation is not working");
			logger.info("department name required min 3 char validation is not working");
			TestUtil.takeScreenshotAtEndOfTest("settingsDepartmnetName min 3 char validation");// ScreenShot capture
		}

	}

	public void edit_departmentname() throws Exception {

		login();

		button(createDept.ManageBtn, "button").click();
		logger.info("Clicked on manage button");
		waits(5);
		logger.info("wait for 5 sec");
		textbox(createDept.dept_name_edit).clear();
		logger.info("Department name clearting");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.dept_name_edit).sendKeys("sampleedit");
		logger.info("Department name entered : sampleedit");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.savebutton, "button").click();
		logger.info("clicked on save button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.confirm_save, "button").click();
		logger.info("clicked on confirm save");

		logger.info(createDept.toaster_message_edit.getText());
		if (createDept.toaster_message_edit.getText().equalsIgnoreCase("Updated sampleedit successfully.")) {
			Assert.assertEquals(createDept.toaster_message_edit.getText(), "Updated sampleedit successfully.",
					"Updated sampleedit successfully.");
			logger.info("update department name is working");
			logger.info("Department name updated succcessfully");
			TestUtil.takeScreenshotAtEndOfTest("Settings update Departmentname case working.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.toaster_message.getText(), "Updated sampleedit successfully.",
					"Updated sampleedit successfully.");
			logger.info("settings update DepartmnetName case not working");
			logger.info("department name updation failed");
			TestUtil.takeScreenshotAtEndOfTest("settings update DepartmnetName case not working");// ScreenShot capture
		}
		button(createDept.ManageEditBtn, "button").click();
		logger.info("Clicked on manage button");
		waits(5);
		logger.info("wait for 5 sec");
		textbox(createDept.dept_name_edit).clear();
		logger.info("Department name clearting");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.dept_name_edit).sendKeys("sample");
		logger.info("Department name entered : sampleedit");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.savebutton, "button").click();
		logger.info("clicked on save button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.confirm_save, "button").click();
		logger.info("clicked on confirm save");


	}

	public void edit_departmentpage_inviteagent() throws Exception {

		login();

		button(createDept.ManageBtn, "button").click();
		logger.info("clicked on manage button");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.addagentBtn, "button").click();
		logger.info("clicked on add new numebr button");
		waits(5);
		logger.info("wait for 5 sec");

		button(createDept.inviteagent_button, "button").click();
		logger.info("clicked on invite agent button");
		waits(5);
		logger.info("wait for 10 sec");

		textbox(createDept.agent_name).sendKeys("dummy123");
		logger.info("Agent name entered : dummy123");
		wait(2);
		logger.info("wait for 2 sec");
		String deptName = randomestring() + "@yopmail.com";
		textbox(createDept.agent_email).sendKeys(deptName);
		logger.info("Email:dummy123@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");

		sa.selectrole();

		button(createDept.email_radiobutton, "button").click();
		logger.info("email radio button selected");
		wait(10);
		logger.info("wait for 10 sec");

		button(createDept.sendbutton, "button").click();
		logger.info("clicked on send button");

		logger.info(createDept.invite_agent_toaster_message.getText());
		if (createDept.invite_agent_toaster_message.getText()
				.equalsIgnoreCase("Your invitation to dummy123 has been sent.")) {
			Assert.assertEquals(createDept.invite_agent_toaster_message.getText(),
					"Your invitation to dummy123 has been sent.", "Your invitation to dummy123 has been sent.");
			logger.info("Your invitation to dummy123 has been sent.");
			logger.info("agent invitation sending successfully");
			TestUtil.takeScreenshotAtEndOfTest(
					"Settings update Department Your invitation to dummy123 has been sent. case working.");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(createDept.invite_agent_toaster_message.getText(),
					"Your invitation to dummy123 has been sent.", "Your invitation to dummy123 has been sent.");
			logger.info("settings update DepartmnetName case not working");
			logger.info("agent invitation sending failed");
			TestUtil.takeScreenshotAtEndOfTest(
					"settings update Departmnet Your invitation to dummy123 has been sent. case not working");// ScreenShot
																												// capture
		}

		waits(2);
		logger.info("wait for 2 sec");
		button(createDept.agent_link, "button").click();
		logger.info("clicked on agent link");
		waits(5);
		logger.info("wait for 10 sec");
		button(createDept.ManageButton, "button").click();
		logger.info("clicked on manage button ");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.delete_Agent, "button").click();
		logger.info("clicked on delte agent button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.confirm_delete_Agent, "button").click();
		logger.info("clicked on confirm delete agent button");
		waits(3);
		logger.info("wait for 3 sec");

	}

	public void edit_departmentpage_remove_agent_add_agian_same_agent() throws Exception {

		login();

		button(createDept.Auto1ManageBtn, "button").click();
		logger.info("clicked on manage button");
		waits(5);
		logger.info("wait for 5 sec");

		button(createDept.department_remvoe_agent, "button").click();
		logger.info("Clicked on remove agent button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.department_confirm_remvoe_agent, "button").click();
		logger.info("Clicked on confirm remove agent button");

		logger.info(createDept.delete_Agent_toastermessage1.getText());
		if (createDept.delete_Agent_toastermessage1.getText()
				.equalsIgnoreCase("Auto1.All of their claimed threads are now unclaimed within the Auto1.")) {
			Assert.assertEquals(createDept.delete_Agent_toastermessage1.getText(),
					"Auto1.All of their claimed threads are now unclaimed within the Auto1.",
					"Auto1.All of their claimed threads are now unclaimed within the Auto1.");
			logger.info("sample.All of their claimed threads are now unclaimed within the sample.");
			logger.info("agent remove working fine ");
			TestUtil.takeScreenshotAtEndOfTest(
					"Settings update Department sample.All of their claimed threads are now unclaimed within the sample. case working.");// ScreenShot
																																			// capture

		} else {
			Assert.assertNotEquals(createDept.delete_Agent_toastermessage1.getText(),
					"Auto1.All of their claimed threads are now unclaimed within the Auto1.",
					"Auto1.All of their claimed threads are now unclaimed within the Auto1.");
			logger.info(
					"settings update Departmnet sample.All of their claimed threads are now unclaimed within the sample.case not working");
			logger.info("agent remove case not working");
			TestUtil.takeScreenshotAtEndOfTest("settings delete agent in department list case not working");// ScreenShot
																											// capture
		}

		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.addagentBtn, "button").click();
		logger.info("clicked on add agent button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.select_radiobutton, "button").click();
		logger.info("clicked on readio button ");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.addButton, "button").click();
		logger.info("Clicked on add button");

		logger.info(createDept.addagentToaster.getText());
		if (createDept.addagentToaster.getText().equalsIgnoreCase("Successfully added agent(s)")) {
			Assert.assertEquals(createDept.addagentToaster.getText(), "Successfully added agent(s)",
					"Successfully added agent(s)");
			logger.info("Successfully added agent(s)");
			logger.info("add agent case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Settings Successfully added agent(s)case working.");// ScreenShot
																									// capture

		} else {
			Assert.assertNotEquals(createDept.addagentToaster.getText(), "Successfully added agent(s)",
					"Successfully added agent(s)");
			logger.info("settings update Departmnet Successfully added agent(s)");
			logger.info("add agent case not working");
			TestUtil.takeScreenshotAtEndOfTest("settings Successfully added agent(s) case not working");// ScreenShot
																										// capture
		}
		
		
	}

	public void Delete_Department() throws Exception {

		oneAgentLogindetails();

		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		departmentName();
		phonenumberinfopagedetails();
		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("selected terms and conditions radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("call me button");
		waits(5);
		logger.info("wait for 5 sec");

		waits(3);
		Repository.mobile_otp1.sendKeys("1");
		waits(1);
		Repository.mobile_otp2.sendKeys("2");
		waits(1);
		Repository.mobile_otp3.sendKeys("3");
		waits(1);
		Repository.mobile_otp4.sendKeys("4");
		waits(1);
		Repository.mobile_otp5.sendKeys("5");
		waits(1);
		Repository.mobile_otp6.sendKeys("6");
		wait(2);
		logger.info("opt entered : 123456");
		button(createDept.verify_button, "button").click();
		logger.info("clicked on verify button");
		waits(1);
		logger.info("wait for 1 sec");

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully");
			logger.info("Number Created Successfully");
			logger.info("Numebr created successfully");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Number Created not working");
			logger.info("Number creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture
		}

		button(createDept.sample1_dept, "button").click();
		logger.info("clicked on manage button");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.DeleteDept, "button").click();
		logger.info("click on delete department button");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("selected radio button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn2, "button").click();
		logger.info("clicked on radio button 2");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn3, "button").click();
		logger.info("selected radio button 3");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn4, "button").click();
		logger.info("seleccted radio button 4");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn5, "button").click();
		logger.info("selected radio button 5");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn6, "button").click();
		logger.info("selected radio button 6");
		waits(2);
		logger.info("wait for 5 sec");
		button(createDept.confirmDelete, "button").click();
		logger.info("clicked on confirm delete button");
		waits(3);
		logger.info("wait for 5 sec");

	}

	public void Delete_Department_NextButton1_Enabled() throws Exception {

		// oneAgentLogindetails();

		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		sampledepartmentName();
		phonenumberinfopagedetails();
		selectStage();
		checkbox(createDept.radioButton, "checkbox").click();

		wait(5);
		button(createDept.nextButton, "button").click();
		wait(5);

		button(createDept.callme_Button, "button").click();
		waits(3);
		Repository.mobile_otp1.sendKeys("1");
		waits(1);
		Repository.mobile_otp2.sendKeys("2");
		waits(1);
		Repository.mobile_otp3.sendKeys("3");
		waits(1);
		Repository.mobile_otp4.sendKeys("4");
		waits(1);
		Repository.mobile_otp5.sendKeys("5");
		waits(1);
		Repository.mobile_otp6.sendKeys("6");
		wait(2);
		button(createDept.verify_button, "button").click();
		wait(1);

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully");
			logger.info("Number Created Successfully");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Number Created not working");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture
		}
		waits(5);
		button(createDept.ManageBtn, "button").click();
		waits(5);
		button(createDept.DeleteDept, "button").click();
		waits(3);
		button(createDept.radiobtn1, "button").click();
		waits(2);

		if (verifyElementIsEnabled(createDept.NEXTBTN1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.NEXTBTN1));
			button(createDept.NEXTBTN1, "button").isEnabled();
			logger.info("Delete Department Nextbutton button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("NextButton is enabled");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.NEXTBTN1));
			logger.info("Delete department Next  button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("NextButton is enabled");// ScreenShot capture
		}

		waits(5);
		button(createDept.NEXTBTN1, "button").click();
		wait(5);
		button(createDept.radiobtn2, "button").click();
		wait(5);
		button(createDept.NEXTBTN1, "button").click();
		wait(5);
		button(createDept.radiobtn3, "button").click();
		wait(5);
		button(createDept.NEXTBTN1, "button").click();
		wait(5);
		button(createDept.radiobtn4, "button").click();
		wait(5);
		button(createDept.radiobtn5, "button").click();
		wait(5);
		button(createDept.radiobtn6, "button").click();
		waits(3);
		button(createDept.confirmDelete, "button").click();
		waits(2);

	}

	public void Delete_Department_NextButton2_Enabled() throws Exception {

		// oneAgentLogindetails();
		login();
		selectcreatenumberoption();

		selectTxtenabledNumberoption();
		sampledepartmentName();
		phonenumberinfopagedetails();
		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on terms and conditions radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("clicked on call me button");
		wait(5);
		logger.info("wait for 5 sec");

		waits(2);
		Repository.mobile_otp1.sendKeys("1");
		waits(1);
		Repository.mobile_otp2.sendKeys("2");
		wait(1);
		Repository.mobile_otp3.sendKeys("3");
		waits(1);
		Repository.mobile_otp4.sendKeys("4");
		waits(1);
		Repository.mobile_otp5.sendKeys("5");
		waits(1);
		Repository.mobile_otp6.sendKeys("6");
		waits(2);
		logger.info("otp Entered 123456");
		button(createDept.verify_button, "button").click();
		logger.info("clicked on verify button");
		wait(1);
		logger.info("wait for 1 sec");

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully");
			logger.info("Number Created Successfully");
			logger.info("Number created successfully");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Number Created not working");
			logger.info("Number creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture
		}

		wait(1);
		button(createDept.ManageBtn, "button").click();
		logger.info("clicked on manage button");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.DeleteDept, "button").click();
		logger.info("clicked on delete department button");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("clicked on radio button one");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn2, "button").click();
		logger.info("clicked on radio button 2 ");
		wait(5);

		if (verifyElementIsEnabled(createDept.NEXTBTN1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(createDept.NEXTBTN1));
			button(createDept.NEXTBTN1, "button").isEnabled();
			logger.info("Delete Department Nextbutton button is enabled");
			logger.info("Next button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("NextButton is enabled2");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(createDept.NEXTBTN1));
			logger.info("Delete department Next  button is disabled");
			logger.info("Next button is diabled");
			TestUtil.takeScreenshotAtEndOfTest("NextButton is enabled2");// ScreenShot capture
		}
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn3, "button").click();
		logger.info("clicked on radio button 3");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn4, "button").click();
		logger.info("radio button 4 selected");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn5, "button").click();
		logger.info("selected radio button 5");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn6, "button").click();
		logger.info("selected radio button 6");
		waits(2);
		logger.info("wait for 5 sec");
		button(createDept.confirmDelete, "button").click();
		logger.info("clicked on confirm delete button");
		waits(3);
		logger.info("wait for 5 sec");

	}

	public void edit_departmentpage_Manage_agent_click_on_cancel_button() throws Exception {

		login();

		waits(5);
		button(createDept.ManageBtn, "button").click();
		logger.info("clicked on manage button");
		waits(2);
		logger.info("wait for 5 sec");

		button(createDept.manageagentButton, "button").click();
		logger.info("clicked on manage agent button");
		waits(3);
		logger.info("wait for 5 sec");

		button(createDept.managedeleteagentButton, "button").click();
		logger.info("clicked on delete agent button");
		waits(3);
		logger.info("wait for 5 sec");

		button(createDept.cancelbutton, "button").click();
		logger.info("clicked on cancel button");
		waits(2);
		logger.info("wait for 5 sec");

		logger.info(createDept.ManageAgent_Headding.getText());
		if (createDept.ManageAgent_Headding.getText().equalsIgnoreCase("Manage Agent")) {
			Assert.assertEquals(createDept.ManageAgent_Headding.getText(), "Manage Agent", "Manage Agent");
			logger.info("Manage Agent");
			logger.info("Back to manage agent page");
			TestUtil.takeScreenshotAtEndOfTest("Settings Manage Agent case working.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.ManageAgent_Headding.getText(), "Manage Agent", "Manage Agent");
			logger.info("settings Manage Agent case not working");
			logger.info("Back to manage agent page");
			TestUtil.takeScreenshotAtEndOfTest("settings Manage Agent case not working");// ScreenShot capture
		}

		
	}

	public void createTollfreeDeptName_min_3_char_validation() throws Exception {

		login();
		selectcreatenumberoption();

		createbutton();

		textbox(createDept.deptName).sendKeys("Tollfree");
		logger.info("Department name entered:Tollfree");

		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.areacode).sendKeys("888");
		logger.info("area code entered: 888");

		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.createbtn, "button").click();
		logger.info("clicked on create button");

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Numebr created successfully");
			logger.info("Number Created Successfully. validation is working");
			TestUtil.takeScreenshotAtEndOfTest("Settings Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Settings Tollfree department creation  is not working");
			logger.info("Number creation failed");
			TestUtil.takeScreenshotAtEndOfTest("ettings Tollfree department creation ");// ScreenShot capture
		}

		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.Manage_tollfree_Dept, "button").click();
		logger.info("open in  mangage tollfree page");
		waits(3);
		logger.info("wait for 5 sec");
		textbox(createDept.dept_name_edit).clear();
		logger.info("clearing the department name");

		wait(5);
		logger.info("wait for 5 sec");

		textbox(createDept.dept_name_edit).sendKeys("to");
		logger.info("department name entered : to");

		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.Manage_tollfree_Dept_min_3char_validation.getText());
		if (createDept.Manage_tollfree_Dept_min_3char_validation.getText()
				.equalsIgnoreCase("Your Number name must contain at least 3 character.")) {
			Assert.assertEquals(createDept.Manage_tollfree_Dept_min_3char_validation.getText(),
					"Your Number name must contain at least 3 character.",
					"Your Number name must contain at least 3 character.");
			logger.info("department name required min 3 char validation is working");
			logger.info("deaprtment name required min 3 char validation case is working");
			TestUtil.takeScreenshotAtEndOfTest("SettingsDepartmentname min 3 char validation1");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.Manage_tollfree_Dept_min_3char_validation.getText(),
					"Your Number name must contain at least 3 character",
					"Your Number name must contain at least 3 character");
			logger.info("department name required min 3 char validation is not working");
			logger.info("department name required min 3 char validation case is not working");
			TestUtil.takeScreenshotAtEndOfTest("settingsDepartmnetName min 3 char validation1");// ScreenShot capture
		}

		waits(2);
		button(createDept.DeleteDept, "button").click();
		logger.info("clicked on delete department button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("selected radio button one");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button ");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn2, "button").click();
		logger.info("clicked on radio button 2");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn3, "button").click();
		logger.info("clicked on radio button 3");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn4, "button").click();
		logger.info("clicked on radio button 4");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn5, "button").click();
		logger.info("clicked on radio button 5");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn6, "button").click();
		logger.info("clicked on radio button 6");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.confirmDelete, "button").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 5 sec");
		
		closetoolspopups();
		logout();

	}

	public void createTollfreeDeptName_update_department_name() throws Exception {

		login();

		selectcreatenumberoption();

		createbutton();

		textbox(createDept.deptName).sendKeys("Tollfree");
		logger.info("Department name entered: Tollfree");

		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.areacode).sendKeys("888");
		logger.info("Area code : 888");

		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.createbtn, "button").click();
		logger.info("clicked on create button ");

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Numebr created successfully");
			logger.info("Number Created Successfully. validation is working");
			TestUtil.takeScreenshotAtEndOfTest("Settings Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Settings Tollfree department creation  is not working");
			logger.info("Number creation failed");
			TestUtil.takeScreenshotAtEndOfTest("ettings Tollfree department creation ");// ScreenShot capture
		}

		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.Manage_tollfree_Dept, "button").click();
		logger.info("tollfree department open in manage page");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.dept_name_edit).clear();
		logger.info("Department name clearing");

		wait(5);
		logger.info("wait for 5 sec");
		textbox(createDept.dept_name_edit).sendKeys("Tollfree");
		logger.info("Department name entered: Tollfree");

		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.savebutton, "button").click();
		logger.info("clicked on save button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.confirm_save, "button").click();
		logger.info("clicked on confirm save button");

		logger.info(createDept.tollfreeDept_updated.getText());
		if (createDept.tollfreeDept_updated.getText().equalsIgnoreCase("Updated Tollfree successfully.")) {
			Assert.assertEquals(createDept.tollfreeDept_updated.getText(), "Updated Tollfree successfully.",
					"Updated Tollfree successfully.");
			logger.info("Toll free department name updated is working");
			logger.info("Toll free department name updated case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Toll free department name updated is working");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.tollfreeDept_updated.getText(), "Updated Tollfree successfully.",
					"Updated Tollfree successfully.");
			logger.info("department name required min 3 char validation is not working");
			logger.info("Tollfree department case case not working");
			TestUtil.takeScreenshotAtEndOfTest("settings Updated Tollfree123 successfully.");// ScreenShot capture
		}

		button(createDept.Manage_tollfree_Dept, "button").click();
		logger.info("tollfree department open in manage page");
		waits(5);
		button(createDept.DeleteDept, "button").click();
		logger.info("clicked on delete department button");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("selected radio button one");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button ");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn2, "button").click();
		logger.info("clicked on radio button 2");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn3, "button").click();
		logger.info("clicked on radio button 3");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn4, "button").click();
		logger.info("clicked on radio button 4");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn5, "button").click();
		logger.info("clicked on radio button 5");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn6, "button").click();
		logger.info("clicked on radio button 6");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.confirmDelete, "button").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 5 sec");

	}

	public void verifystatus_department_clickoncallmebutton_entervalid_verification_Code() throws Exception {

		login();

		selectcreatenumberoption();

		selectTxtenabledNumberoption();
		sample2departmentName();
		phonenumberinfopagedetails();
		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("selected terms and conditions radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("Clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callme_Button, "button").click();
		logger.info("Clicked on call me button");
		waits(5);
		logger.info("wait for 5 sec");

		button(createDept.agent_link, "button").click();
		logger.info("clicked on agent link");
		waits(5);
		logger.info("wait for 10 sec");

		button(createDept.Numbers_link, "button").click();
		logger.info("Clicked on numbers link");
		waits(5);
		logger.info("wait for 10 sec");

		button(createDept.Verify_button2, "button").click();
		logger.info("clicked on verify button");
		wait(5);
		logger.info("wait for 5 sec");

		button(createDept.callmeButton, "button").click();
		logger.info("clicked on call me button");
		waits(5);
		logger.info("wait for 5 sec");
		waits(3);
		Repository.mobile_otp1.sendKeys("1");
		wait(1);
		Repository.mobile_otp2.sendKeys("2");
		wait(1);
		Repository.mobile_otp3.sendKeys("3");
		wait(1);
		Repository.mobile_otp4.sendKeys("4");
		wait(1);
		Repository.mobile_otp5.sendKeys("5");
		wait(1);
		Repository.mobile_otp6.sendKeys("6");
		wait(2);
		logger.info("otp entered 123456");
		button(createDept.verify_button_click, "button").click();

		logger.info(createDept.deptCreated.getText());
		if (createDept.deptCreated.getText().equalsIgnoreCase("Number Created Successfully.")) {
			Assert.assertEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully");
			logger.info("Number Created Successfully");
			logger.info("Number created successfully");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.deptCreated.getText(), "Number Created Successfully.",
					"Number Created Successfully.");
			logger.info("Number Created not working");
			logger.info("Number creation failed");
			TestUtil.takeScreenshotAtEndOfTest("Number Created Successfully");// ScreenShot capture
		}

		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.sample2ManageBtn, "button").click();
		logger.info("clicked on manage button");
		waits(5);
		logger.info("wait for 5 sec");
		button(createDept.DeleteDept, "button").click();
		logger.info("clicked on delete dept button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("clicked on radio button one");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn2, "button").click();
		logger.info("Clicked on radio button 2");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn3, "button").click();
		logger.info("clicked on radio button 3");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn4, "button").click();
		logger.info("clicked on radio button 4");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn5, "button").click();
		logger.info("clicked on radio button 5");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn6, "button").click();
		logger.info("clicked on radio button6");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.confirmDelete, "button").click();
		logger.info("clicked on confirm delete button");
		waits(2);
		logger.info("wait for 5 sec");

	}

	public void verifystatus_departmen_Page_clickon_back_button() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		departmentName();
		phonenumberinfopagedetails();
		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("clicked on terms and conditions radio button ");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("Clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.callme_Button, "button").click();
		logger.info("clicked on call me button");
		waits(5);
		logger.info("wait for 5 sec");

		button(createDept.agent_link, "button").click();
		logger.info("clicked on agent link");
		waits(5);
		logger.info("wait for 10 sec");

		button(createDept.Numbers_link, "button").click();
		logger.info("clicked on number link");
		waits(5);
		logger.info("wait for 10 sec");

		button(createDept.Verify_button1, "button").click();
		logger.info("clicked on verfiy button ");
		waits(3);
		logger.info("wait for 5 sec");

		button(createDept.Back_Button, "button").click();
		logger.info("click on back button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.Verify_button1, "button").click();
		logger.info("click on verify button");
		waits(3);
		logger.info("wait for 5 sec");

		button(createDept.DeleteButton, "button").click();
		logger.info("click on delete button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("clicked on radio button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.CofirmDeleteButton, "button").click();
		logger.info("click on confirm delete button");

		logger.info(createDept.delete_toaster_message.getText());
		if (createDept.delete_toaster_message.getText().equalsIgnoreCase("")) {
			Assert.assertEquals(createDept.delete_toaster_message.getText(), "sample has been deleted.",
					"sample has been deleted.");
			logger.info("sample has been deleted.");
			logger.info("verify status department deleted successfully");
			TestUtil.takeScreenshotAtEndOfTest("sample has been deleted.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createDept.delete_toaster_message.getText(), "sample has been deleted.",
					"sample has been deleted.");
			logger.info("sample has been deleted.");
			logger.info("Verify status departmetn deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("sample has been deleted.");// ScreenShot capture
		}

	}

	public void phoneNumberinfo_phonenumber_mustbe_10digits_validation_checking() throws Exception {

		login();
		selectcreatenumberoption();
		selectTxtenabledNumberoption();
		phonenumberinfopagedetailsinvalid();
		selectStage();

		checkbox(createDept.radioButton, "checkbox").click();
		logger.info("selected terms and conditons radio button");

		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.nextButton, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");

		logger.info(createDept.phonenumber_10digits_validation.getText());
		if (createDept.phonenumber_10digits_validation.getText()
				.equalsIgnoreCase("Your contact must have a valid 10-digit phone numb")) {
			Assert.assertEquals(createDept.phonenumber_10digits_validation.getText(),
					"Your contact must have a valid 10-digit phone numb",
					"Your contact must have a valid 10-digit phone numb");
			logger.info("Your contact must have a valid 10-digit phone numb");
			logger.info("mobile number have vaid 10-digit phone number validation is working fine");
			TestUtil.takeScreenshotAtEndOfTest("Your contact must have a valid 10-digit phone numb");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(createDept.phonenumber_10digits_validation.getText(),
					"Your contact must have a valid 10-digit phone numb",
					"Your contact must have a valid 10-digit phone numb");
			logger.info("Your contact must have a valid 10-digit phone numb not working");
			logger.info("Your contact must have a valid 10-digit phone numb not working");
			TestUtil.takeScreenshotAtEndOfTest("Your contact must have a valid 10-digit phone numb  case failed ");// ScreenShot
																													// capture
		}
		closetoolspopups();
		logout();
	}
	
	public void deleteSampleDepartment() throws Exception {
		Auto2Logindetails();
		button(createDept.ManageBtn, "button").click();
		logger.info("click on verify button");
		waits(3);
		logger.info("wait for 5 sec");
		button(createDept.DeleteDept, "button").click();
		logger.info("clicked on delete dept button");
		waits(2);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn1, "button").click();
		logger.info("clicked on radio button one");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn2, "button").click();
		logger.info("Clicked on radio button 2");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn3, "button").click();
		logger.info("clicked on radio button 3");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.NEXTBTN1, "button").click();
		logger.info("clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn4, "button").click();
		logger.info("clicked on radio button 4");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn5, "button").click();
		logger.info("clicked on radio button 5");
		wait(5);
		logger.info("wait for 5 sec");
		button(createDept.radiobtn6, "button").click();
		logger.info("clicked on radio button6");
		waits(2);
		logger.info("wait for 5 sec");
		button(createDept.confirmDelete, "button").click();
		logger.info("clicked on confirm delete button");
		waits(3);
		logger.info("wait for 5 sec");

		
	}

}
