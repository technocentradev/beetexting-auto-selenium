package com.beetext.qa.Workflows.settings;

import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.SettingsEditorganizationName;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.SettingsOrganizationNameEdit;
import com.beetext.qa.util.TestUtil;

public class SettignsOrganizationNameEditWF extends SettingsEditorganizationName {
	String url, username1, username2, password;
	Repository repository = new Repository();
	TestBase testBase = new TestBase();
	SettingsOrganizationNameEdit editOrg = new SettingsOrganizationNameEdit();

	public void Settings_OrganizatioName_contain_single_char() throws Exception {
		editorganizationLink();
		clearorganizationname();

		textbox(editOrg.organization_name_edit).sendKeys("A");
		logger.info("org Name Entering : A");

		logger.info(editOrg.organization_name_min_3char.getText());
		threecharrequired();

	}

	public void Settings_OrganizatioName_lessthan_3char_validation() throws Exception {
		editorganizationLink();
		clearorganizationname();

		textbox(editOrg.organization_name_edit).sendKeys("Au");
		logger.info("organization name entering two characters : Au");
		logger.info(editOrg.organization_name_min_3char.getText());
		threecharrequired();

	}

	public void Settings_OrganizatioName_required_validation() throws Exception {
		editorganizationLink();
		textbox(editOrg.organization_name_edit).click();
		logger.info("org Name text box clicked");
		editOrg.organization_name_edit.sendKeys(Keys.CONTROL + "a");
		editOrg.organization_name_edit.sendKeys(Keys.DELETE);
		logger.info("organization name is cleared");
		logger.info(editOrg.organization_name_required.getText());
		if (editOrg.organization_name_required.getText().equalsIgnoreCase("Organization Name is required.")) {
			Assert.assertEquals(editOrg.organization_name_required.getText(), "Organization Name is required.",
					"Organization Name is required.");
			logger.info("Organization Name is required.");
			TestUtil.takeScreenshotAtEndOfTest("Settings Organization Name is required.validation checking");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(editOrg.organization_name_required.getText(), "Organization Name is required.",
					"Organization Name is required.");
			logger.info("Organization Name is required. validation case field");
			TestUtil.takeScreenshotAtEndOfTest("Settings Organization Name is required. validation case");// ScreenShot
																											// capture
		}

	}

	public void Settings_OrganizatioName_Update() throws Exception {

		editorganizationLink();
		clearorganizationname();
		textbox(editOrg.organization_name_edit).sendKeys("Auto12");
		logger.info("Org Name Entering Auto12");
		saveButton();

		logger.info(editOrg.ToasterMessage.getText());
		if (editOrg.ToasterMessage.getText().equalsIgnoreCase("Updated organization successfully.")) {
			Assert.assertEquals(editOrg.ToasterMessage.getText(), "Updated organization successfully.",
					"Updated organization successfully.");
			logger.info("Updated organization successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Settings Updated organization successfully. validation checking");// ScreenShot
																													// capture

		} else {
			Assert.assertNotEquals(editOrg.ToasterMessage.getText(), "Updated organization successfully.",
					"Updated organization successfully.");
			logger.info("Updated organization successfully. validation case field");
			TestUtil.takeScreenshotAtEndOfTest("Settings Updated organization successfully. validation case");// ScreenShot
																												// capture
		}

	}

	public void Settings_OrganizatioName_Button_Disabled() throws Exception {

		editorganizationLink();
		textbox(editOrg.organization_name_edit).click();
		logger.info("org Name text box clicked");
		wait(2);
		// sending Ctrl+a by Keys.Chord()
		String s = Keys.chord(Keys.CONTROL, "a");
		editOrg.organization_name_edit.sendKeys(s);
		// sending DELETE key
		editOrg.organization_name_edit.sendKeys(Keys.DELETE);

		logger.info("organization name is cleared");

		if (verifyElementIsEnabled(editOrg.saveButton)) {
			Assert.assertEquals(true, verifyElementIsEnabled(editOrg.saveButton));

			logger.info("create Department button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(editOrg.saveButton));
			button(editOrg.saveButton, "button").isEnabled();
			logger.info("create department button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		wait(10);

	}

	public void Settings_OrganizatioName_Duplicate_Validation_checking() throws Exception {

		editorganizationLink();
		clearorganizationname();
		textbox(editOrg.organization_name_edit).sendKeys("Technocentra");
		logger.info("Organization name Typing Technocentra");
		saveButton();

		logger.info(editOrg.Duplicate_orgname.getText());
		if (editOrg.Duplicate_orgname.getText().equalsIgnoreCase("Duplicate Organization Name")) {
			Assert.assertEquals(editOrg.Duplicate_orgname.getText(), "Duplicate Organization Name",
					"Duplicate Organization Name");
			logger.info("Duplicate Organization Name");
			TestUtil.takeScreenshotAtEndOfTest("Settings Duplicate Organization Name validation checking");// ScreenShot
																											// capture

		} else {
			Assert.assertNotEquals(editOrg.Duplicate_orgname.getText(), "Duplicate Organization Name",
					"Duplicate Organization Name");
			logger.info("Duplicate Organization Name validation case field");
			TestUtil.takeScreenshotAtEndOfTest("Settings Duplicate Organization Name validation case");// ScreenShot
																										// capture
		}

	}

}
