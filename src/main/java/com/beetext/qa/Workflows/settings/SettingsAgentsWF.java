package com.beetext.qa.Workflows.settings;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.SettingsAgentinviteWFCM;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.SettingsAgents;
import com.beetext.qa.util.TestUtil;

public class SettingsAgentsWF extends SettingsAgentinviteWFCM {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	SettingsAgents agent = new SettingsAgents();
	public static String currentDir = System.getProperty("user.dir");

	public void Agent_name_required_validation() throws Exception {

		login();

		textbox(agent.agent_name).sendKeys("");
		logger.info("text box entered agent name empty");
		textbox(agent.agent_email).sendKeys("");
		logger.info("Clicked on email text box");
		logger.info(agent.agent_name_required.getText());
		if (agent.agent_name_required.getText().equalsIgnoreCase("Name is required.")) {
			Assert.assertEquals(agent.agent_name_required.getText(), "Name is required.");
			logger.info("Name is required.. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Name is required.");// ScreenShot
																	// capture

		} else {
			Assert.assertNotEquals(agent.agent_name_required.getText(), "Name is required.", "Name is required.");
			logger.info("agent name is required validation case not working");
			TestUtil.takeScreenshotAtEndOfTest("Name is required. TestCase is failed");// ScreenShot
																						// capture
		}

	}

	public void Agent_name_required_3char_validation() throws Exception {

		login();
		textbox(agent.agent_name).sendKeys("12");
		logger.info("agent name entered:12");

		logger.info(agent.agent_name_required_3_char.getText());
		if (agent.agent_name_required_3_char.getText().equalsIgnoreCase("Name must be at least 3 characters.")) {
			Assert.assertEquals(agent.agent_name_required_3_char.getText(), "Name must be at least 3 characters.");
			logger.info("Name must be at least 3 characters. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Name must be at least 3 characters.");// ScreenShot
																						// capture

		} else {
			Assert.assertNotEquals(agent.agent_name_required_3_char.getText(), "Name must be at least 3 characters.",
					"Name must be at least 3 characters.");
			logger.info("Name must be at least 3 characters case is not working");
			TestUtil.takeScreenshotAtEndOfTest("Name must be at least 3 characters. TestCase is failed");// ScreenShot
																											// capture
		}

	}

	public void Agent_email_required_validation() throws Exception {

		login();

		textbox(agent.agent_email).sendKeys("");
		logger.info("clicked on email text box");
		textbox(agent.agent_name).sendKeys("");
		logger.info("Clicked on name text box");
		logger.info(agent.agent_email_required.getText());
		if (agent.agent_email_required.getText().equalsIgnoreCase("Email is required.")) {
			Assert.assertEquals(agent.agent_email_required.getText(), "Email is required.");
			logger.info("Email is required. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Email is required.");// ScreenShot
																		// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_required.getText(), "Email is required.", "Email is required.");
			logger.info("Email is required case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email is required.");// ScreenShot
																		// capture
		}

	}

	public void Agent_email_not_valid() throws Exception {

		login();

		textbox(agent.agent_email).sendKeys("abc");
		logger.info("agent email typed: abc");
		textbox(agent.agent_name).sendKeys("");
		logger.info("agent name text box typed");
		logger.info(agent.agent_email_not_valid.getText());
		if (agent.agent_email_not_valid.getText().equalsIgnoreCase("Email not valid.")) {
			Assert.assertEquals(agent.agent_email_not_valid.getText(), "Email not valid.");
			logger.info("Email not valid. case is verified");
			TestUtil.takeScreenshotAtEndOfTest("Email not valid.");// ScreenShot
																	// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_not_valid.getText(), "Email not valid.", "Email not valid.");
			logger.info("invalid email case not working ");
			TestUtil.takeScreenshotAtEndOfTest("Email not valid.");// ScreenShot
																	// capture
		}

	}

	public void Agent_email_duplicate_validation() throws Exception {

		login();
		enteralreadyexistingemail();
		logger.info(agent.agent_email_duplicate.getText());
		if (agent.agent_email_duplicate.getText()
				.equalsIgnoreCase("Email address already in use. Re-invite the user or try a new email address.")) {
			Assert.assertEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the user or try a new email address.");
			logger.info("Email already exists.Please try again.");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the user or try a new email address.",
					"Email address already in use. Re-invite the user or try a new email address.");
			logger.info("Email already exists case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture
		}

	}

	public void Agent_invite_uncheck_email() throws Exception {

		login();
		enteralreadyexistingemail();
		button(agent.emailradiobutton, "login submit").click();
		logger.info("unselected the email option");

		logger.info(agent.agent_email_check_uncheck.getText());
		if (agent.agent_email_check_uncheck.getText()
				.equalsIgnoreCase("Please select how you would like to send your invi")) {
			Assert.assertEquals(agent.agent_email_check_uncheck.getText(),
					"Please select how you would like to send your invi");
			logger.info("Please select how you would like to send your invi");
			TestUtil.takeScreenshotAtEndOfTest("Please select how you would like to send your invi.");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_check_uncheck.getText(),
					"Please select how you would like to send your invi",
					"Please select how you would like to send your invi");
			logger.info("Please select how you would like to send your invi case not working");
			TestUtil.takeScreenshotAtEndOfTest("Please select how you would like to send your invi");// ScreenShot
																										// capture
		}

	}

	public void Agent_invite_send_button_enabled() throws Exception {

		login();
		textbox(agent.agent_name).sendKeys("sample");
		logger.info("agent name: sample");
		textbox(agent.agent_email).sendKeys("auto2@yopmail.com");
		logger.info("Email: auto2@yopmail.com");

		selectrole();

		button(agent.emailradiobutton, "login submit").click();
		logger.info("Email option is selected");
		if (verifyElementIsEnabled(agent.send_button)) {
			Assert.assertEquals(true, verifyElementIsEnabled(agent.send_button));
			button(agent.send_button, "button").isEnabled();
			logger.info("send button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase89");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(agent.send_button));
			logger.info("send  button is disabled");

			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase89");// ScreenShot capture
		}

	}

	public void Agent_invite_sending() throws Exception {

		login();
		sentsampleagentinvitation();

		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		button(agent.agent_reviewinvite_button, "login submit").click();
		logger.info("clicked on reinvite button");

		logger.info(agent.reinvitation_toaster_message.getText());
		if (agent.reinvitation_toaster_message.getText().equalsIgnoreCase("User has been re-invited.")) {
			Assert.assertEquals(agent.reinvitation_toaster_message.getText(), "User has been re-invited.");
			logger.info("User has been re-invited.");
			logger.info("User has been re-invited.");
			TestUtil.takeScreenshotAtEndOfTest("re invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.reinvitation_toaster_message.getText(), "User has been re-invited.",
					"User has been re-invited.");
			logger.info("Not clicked on reinvite button");
			TestUtil.takeScreenshotAtEndOfTest("re invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

	public void Manage_Agent_check_uncheck_department() throws Exception {

		agentLink();

		button(agent.manage_btn1, "login submit").click();
		logger.info("clicked on Manage button");
		button(agent.check_uncheck_dept, "login submit").click();

		button(agent.save_agent, "login submit").click();

		logger.info(agent.update_toaster_message.getText());
		if (agent.update_toaster_message.getText().equalsIgnoreCase("User updated successfully.")) {
			Assert.assertEquals(agent.update_toaster_message.getText(), "User updated successfully.");
			logger.info("User updated successfully.");
			logger.info("User updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("User updated successfully.");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(agent.update_toaster_message.getText(), "User updated successfully.",
					"User updated successfully.");
			logger.info("User updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("User updated successfully.");// ScreenShot
																				// capture
		}

		button(agent.manage_btn1, "login submit").click();
		logger.info("clicked on Manage button");
		wait(10);
		button(agent.check_uncheck_dept, "login submit").click();
		button(agent.save_agent, "login submit").click();

		logger.info(agent.update_toaster_message.getText());
		if (agent.update_toaster_message.getText().equalsIgnoreCase("User updated successfully.")) {
			Assert.assertEquals(agent.update_toaster_message.getText(), "User updated successfully.");
			logger.info("User updated successfully.");
			logger.info("User updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Agent updated successfully1.");// ScreenShot
																				// capture

		} else {
			Assert.assertNotEquals(agent.update_toaster_message.getText(), "User updated successfully.",
					"User updated successfully.");
			logger.info("User updated successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Agent updated successfully1.");// ScreenShot
																				// capture
		}

	}

	public void Manageagent_click_agent_delete_and_then_click_on_cancel_button() throws Exception {

		login();
		sampleAgent();
		sampleagentinvitetoastermessage();
		manageButton();
		button(agent.Delete_agent, "login submit").click();
		logger.info("clicked on delete  button");

		button(agent.cancel_button, "login submit").click();
		wait(5);
		logger.info("clicked on cancel button");

//		logger.info(agent.manage_agent_headding.getText());
//		if (agent.manage_agent_headding.getText().equalsIgnoreCase("Manage Agent")) {
//			Assert.assertEquals(agent.manage_agent_headding.getText(), "Manage Agent");
//			logger.info("Back to Manage Agent page");
//			TestUtil.takeScreenshotAtEndOfTest("Manage Agent heading.");// ScreenShot
//																		// capture
//
//		} else {
//			Assert.assertNotEquals(agent.manage_agent_headding.getText(), "Manage Agent", "Manage Agent");
//			logger.info("back to Manage Agent page case failed ");
//			TestUtil.takeScreenshotAtEndOfTest("Manage Agent");// ScreenShot
//																// capture
//		}
		waits(3);
		deleteConfirmagent();

		sampleagentdeletemessage();

	}

	public void User_Agent_invite_sending() throws Exception {

		login();
		sentsampleagentinvitation();

		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		button(agent.agent_reviewinvite_button, "login submit").click();
		logger.info("clicked on reinvite button");

		logger.info(agent.reinvitation_toaster_message.getText());
		if (agent.reinvitation_toaster_message.getText().equalsIgnoreCase("User has been re-invited.")) {
			Assert.assertEquals(agent.reinvitation_toaster_message.getText(), "User has been re-invited.");
			logger.info("User has been re-invited.");
			TestUtil.takeScreenshotAtEndOfTest("re invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.reinvitation_toaster_message.getText(), "User has been re-invited.",
					"User has been re-invited.");
			logger.info("Not clicked on reinvite button");
			TestUtil.takeScreenshotAtEndOfTest("re invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
		manageButton();
		deleteConfirmagent();
		logger.info(agent.Delete_Toaster.getText());
		sampleagentdeletemessage();

	}

	public void Agent_invite_phone_number_validation() throws Exception {

		login();
		textbox(agent.agent_name).sendKeys("sampleagent");
		logger.info("agent name : sampleagent");
		textbox(agent.agent_email).sendKeys("auto9999@yopmail.com");
		logger.info("Email: auto9999@yopmail.com");

		textbox(agent.phone_number_input_box).sendKeys("555");
		logger.info("MobileNumber entered: 555");

		selectrole();

		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected radio button");
		clickonsendButton();

		logger.info(agent.phone_number_validation.getText());
		if (agent.phone_number_validation.getText()
				.equalsIgnoreCase("Your contact must have a valid 10-digit phone numb")) {
			Assert.assertEquals(agent.phone_number_validation.getText(),
					"Your contact must have a valid 10-digit phone numb");
			logger.info("Your contact must have a valid 10-digit phone numb");
			TestUtil.takeScreenshotAtEndOfTest("Your contact must have a valid 10-digit phone numb");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(agent.phone_number_validation.getText(),
					"Your contact must have a valid 10-digit phone numb",
					"Your contact must have a valid 10-digit phone numb");
			TestUtil.takeScreenshotAtEndOfTest("Your contact must have a valid 10-digit phone numb");// ScreenShot
																										// capture
		}

	}

	public void User_Agent_invite_sending_accept_the_invitation() throws Exception {

		login();
		sentsampleagentinvitation();
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(3);
		driver.manage().deleteAllCookies();
		waits(5);
		
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		waits(5);

		driver.switchTo().window(tabs.get(1));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		agentName();
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);

		link(agent.accept_button, "accept Link").click();
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		waits(10);
		textbox(agent.password).sendKeys("tech1234");
		logger.info("passwordEntered: tech1234");
		textbox(agent.cpassword).sendKeys("tech1234");
		logger.info("confirm passwordEntered: tech1234");

		button(agent.setpassword_button, "login submit").submit();
		waits(10);
		validateSignOutLink();
		validateSignOut();
		wait(30);

		logins();
		settingsagentslink();

		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

	public void User_Agent_invite_sending_reject_the_invitation() throws Exception {

		login();
		sentsampleagentinvitation();
		wait(1);
		logger.info(agent.agent_invite_toaster.getText());
		if (agent.agent_invite_toaster.getText().equalsIgnoreCase("Your invitation to sampleagent has been sent.")) {
			Assert.assertEquals(agent.agent_invite_toaster.getText(), "Your invitation to sampleagent has been sent.");
			logger.info("Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.agent_invite_toaster.getText(),
					"Your invitation to sampleagent has been sent.", "Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing settings pop up");

		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(3);
		driver.manage().deleteAllCookies();
		waits(5);
		
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		waits(5);

		driver.switchTo().window(tabs.get(1));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		agentName();
		button(agent.gobutton, "login submit").submit();
		waits(5);
		driver.switchTo().frame("ifmail");
		waits(5);
		link(agent.reject_button, "Reject Link").click();
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		waits(10);
		logger.info(agent.reject_page.getText());
		if (agent.reject_page.getText().equalsIgnoreCase("Your invitation is rejected successfully.")) {
			Assert.assertEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.");
			logger.info("Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.",
					"Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture
		}

	}

	public void Agentemail_duplicate_validation() throws Exception {
		login();
		textbox(agent.agent_name).sendKeys("sample");
		logger.info("agent name typed: sample");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(agent.agent_email).sendKeys("auto2@yopmail.com");
		logger.info("email text box typed: auto2@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");
		selectrole();

		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected the email option");
		wait(2);
		logger.info("wait for 2 sec");
		clickonsendButton();
		System.out.println(agent.agent_email_duplicate.getText());
		if (agent.agent_email_duplicate.getText()
				.equalsIgnoreCase("Email address already in use. Re-invite the agent or try a new email address.")) {
			Assert.assertEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.");
			System.out.println("Email already exists.Please try again.");
			logger.info("Email already exists case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.",
					"Email address already in use. Re-invite the agent or try a new email address.");
			logger.info("Email already exists case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture
		}

	}

	public void delete_existing_agent_add_same_email() throws Exception {

		agent_links();
		waits(5);
		button(agent.addsame_agent_email_manage_btn, "login submit").click();
		logger.info("clicked on manage agent button");
		waits(5);
		logger.info("wait for 5 sec");

		button(agent.Delete_agent, "login submit").click();
		logger.info("clicked on delete agent button");
		wait(10);
		logger.info("wait for 10 sec");

		button(agent.Confirm_Delete_agent, "login submit").click();
		logger.info("clicked on confirm delete agent button");

		System.out.println(agent.Delete_Toasters.getText());
		if (agent.Delete_Toasters.getText()
				.equalsIgnoreCase("deleteagentaddsameagent has been deleted from the organization.")) {
			Assert.assertEquals(agent.Delete_Toasters.getText(),
					"deleteagentaddsameagent has been deleted from the organization.");
			System.out.println("sampleagent has been deleted from the organization.");
			logger.info("sampleagent has been deleted from the organization.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.Delete_Toasters.getText(),
					"deleteagentaddsameagent has been deleted from the organization.",
					"sampleagent has been deleted from the organization.");
			logger.info("sampleagent has been deleted from the organization.");
			TestUtil.takeScreenshotAtEndOfTest("sampleagent has been deleted from the organization..");// ScreenShot
																										// capture
		}

		waits(5);
		login();
		textbox(agent.agent_name).sendKeys("deleteagentaddsameagent");
		logger.info("agent name : sampleagent");
		wait(2);
		logger.info("wait for 2 min");
		String agentName1 = "auto14@yopmail.com";
		textbox(agent.agent_email).sendKeys(agentName1);
		logger.info("Email: auto9999@yopmail.com");
		wait(2);
		logger.info("wait for 2 min");

		selectrole();

		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected radio button");
		wait(2);
		logger.info("wait for 2 sec");
		clickonsendButton();

		System.out.println(agent.agent_invite_toasters.getText());
		if (agent.agent_invite_toasters.getText()
				.equalsIgnoreCase("Your invitation to deleteagentaddsameagent has been sent.")) {
			Assert.assertEquals(agent.agent_invite_toasters.getText(),
					"Your invitation to deleteagentaddsameagent has been sent.");
			System.out.println("Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.agent_invite_toasters.getText(),
					"Your invitation to deleteagentaddsameagent has been sent.",
					"Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);

		waits(10);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get("https://yopmail.com/");
		waits(5);
		addsameagentName();
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);
		link(agent.accept_button, "accept Link").click();
		waits(10);
//		driver.close();
//		waits(10);
//		driver.switchTo().window(tabs.get(0));
//		driver.findElement(By.xpath("//a[contains(text(),'automation.beetexting.com.')]")).click();
//		waits(5);
//		validateSignOutLink();
//		waits(3);
//		validateSignOut();
//		waits(10);
//		driver.manage().deleteAllCookies();
//		waits(3);
//		driver.close();
//		waits(2);
//		driver.switchTo().window(tabs.get(0));
//		waits(5);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(2));
		waits(10);
		textbox(agent.password).sendKeys("tech1234");
		logger.info("passwordEntered: tech1234");
		textbox(agent.cpassword).sendKeys("tech1234");
		logger.info("confirm passwordEntered: tech1234");

		button(agent.setpassword_button, "login submit").submit();
		waits(10);

	}

	public void Agent_invite_sending_accept_the_invitation() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(3);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		waits(5);

		driver.switchTo().window(tabs.get(1));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);

		link(agent.accept_button, "accept Link").click();
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		waits(10);
		textbox(agent.password).sendKeys("tech1234");
		logger.info("passwordEntered: tech1234");
		textbox(agent.cpassword).sendKeys("tech1234");
		logger.info("confirm passwordEntered: tech1234");

		button(agent.setpassword_button, "login submit").submit();
		waits(10);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(3);

		logins();
		settingsagentslink();

		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(3);
		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(3);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		waits(5);

		driver.switchTo().window(tabs1.get(2));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		textbox(agent.email).clear();
		waits(2);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);

		link(agent.accept_button, "accept Link").click();
//		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(3));
		waits(10);
		textbox(agent.password).sendKeys("tech1234");
		logger.info("passwordEntered: tech1234");
		textbox(agent.cpassword).sendKeys("tech1234");
		logger.info("confirm passwordEntered: tech1234");

		button(agent.setpassword_button, "login submit").submit();
		waits(10);
		validateSignOutLink();
		validateSignOut();
		wait(30);

		logins();
		settingsagentslink();

		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

	public void Agent_invite_sending_reject_the_invitation_add_same_agent() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		waits(5);

		driver.switchTo().window(tabs.get(1));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		textbox(agent.email).clear();
		waits(2);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);

		link(agent.accept_button, "accept Link").click();
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		waits(10);
		textbox(agent.password).sendKeys("tech1234");
		logger.info("passwordEntered: tech1234");
		textbox(agent.cpassword).sendKeys("tech1234");
		logger.info("confirm passwordEntered: tech1234");

		button(agent.setpassword_button, "login submit").submit();
		waits(10);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(5);

		logins();
		settingsagentslink();

		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		if (agent.agent_invite_toaster.getText().equalsIgnoreCase("Your invitation to sampleagent has been sent.")) {
			Assert.assertEquals(agent.agent_invite_toaster.getText(), "Your invitation to sampleagent has been sent.");
			logger.info("Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.agent_invite_toaster.getText(),
					"Your invitation to sampleagent has been sent.", "Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing settings pop up");
		waits(3);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);
		waits(10);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		waits(5);
		driver.switchTo().window(tabs1.get(2));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		textbox(agent.email).clear();
		waits(2);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(5);
		driver.switchTo().frame("ifmail");
		waits(5);
		link(agent.reject_button, "Reject Link").click();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(3));
		waits(10);
		logger.info(agent.reject_page.getText());
		if (agent.reject_page.getText().equalsIgnoreCase("Your invitation is rejected successfully.")) {
			Assert.assertEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.");
			logger.info("Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.",
					"Agent Invitation is rejected not successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture
		}

	}

	public void Agent_invite_sending_again_add_same_agent_email() throws Exception {

		login();
		sentsampleagentinvitation();

		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		login();
		sentsampleagentinvitation();
		System.out.println(agent.agent_email_duplicate.getText());
		if (agent.agent_email_duplicate.getText()
				.equalsIgnoreCase("Email address already in use. Re-invite the agent or try a new email address.")) {
			Assert.assertEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.");
			System.out.println("Email already exists.Please try again.");
			logger.info("Email already exists case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.",
					"Email address already in use. Re-invite the agent or try a new email address.");
			logger.info("Email already exists case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture
		}
		waits(5);
		agent_links();
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

	public void Agent_invite_sending_reject_the_invitation_add_same_agent_one_more_time() throws Exception {

		login();
		sampleAgents();
		wait(1);
		logger.info(agent.agent_invite_toaster.getText());
		if (agent.agent_invite_toaster.getText().equalsIgnoreCase("Your invitation to sampleagent has been sent.")) {
			Assert.assertEquals(agent.agent_invite_toaster.getText(), "Your invitation to sampleagent has been sent.");
			logger.info("Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.agent_invite_toaster.getText(),
					"Your invitation to sampleagent has been sent.", "Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing settings pop up");
		waits(3);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);
//		driver.close();
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		waits(5);

		driver.switchTo().window(tabs1.get(1));

		waits(5);
		driver.get("https://yopmail.com/en/");
		waits(5);
		textbox(agent.email).clear();
		waits(2);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		waits(2);
		button(agent.gobutton, "login submit").submit();
		waits(5);
		driver.switchTo().frame("ifmail");
		waits(5);
		link(agent.reject_button, "Reject Link").click();
//		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(2));
		waits(10);
		logger.info(agent.reject_page.getText());
		if (agent.reject_page.getText().equalsIgnoreCase("Your invitation is rejected successfully.")) {
			Assert.assertEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.");
			logger.info("Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.",
					"Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture
		}
	}

	public void addrejectemail_id() throws Exception {
		login();
		sampleAgents();
		wait(1);
		logger.info(agent.agent_invite_toaster.getText());
		if (agent.agent_invite_toaster.getText().equalsIgnoreCase("Your invitation to sampleagent has been sent.")) {
			Assert.assertEquals(agent.agent_invite_toaster.getText(), "Your invitation to sampleagent has been sent.");
			logger.info("Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.agent_invite_toaster.getText(),
					"Your invitation to sampleagent has been sent.", "Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
		waits(5);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

	public void Agent_invite_sending_delete_agent_try_to_accept() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		validateSignOut();
		waits(5);
		driver.manage().deleteAllCookies();
		waits(5);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waits(5);

		driver.switchTo().window(tabs1.get(1));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);

		link(agent.accept_button, "accept Link").click();
		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(1));
		waits(10);
		if (agent.delete_agent_popup.getText()
				.equalsIgnoreCase("Your invite has expired or invalid, please contact your administrator.")) {
			Assert.assertEquals(agent.delete_agent_popup.getText(),
					"Your invite has expired or invalid, please contact your administrator.");
			logger.info("Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.delete_agent_popup.getText(),
					"Your invite has expired or invalid, please contact your administrator.", "Message not matching");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture
		}

	}

	public void Agent_invite_sending_delete_agent_try_to_reject() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waits(3);
		driver.get("https://yopmail.com/en/");
		waits(10);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);

		link(agent.reject_button, "reject Link").click();
//		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(2));

		waits(5);
		if (agent.delete_agent_popup1.getText()
				.equalsIgnoreCase("Your account is not available. Please check with your administrator.")) {
			Assert.assertEquals(agent.delete_agent_popup1.getText(),
					"Your account is not available. Please check with your administrator.");
			logger.info("Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.delete_agent_popup1.getText(),
					"Your account is not available. Please check with your administrator.",
					"Your account is not available. Please check with your administrator.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture
		}

	}

	public void Agent_is_inactive_tryto_add_same_agent_email_one_more_time() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(5);
		login();
		sampleAgents();
		waits(1);
		if (agent.agent_email_duplicate.getText()
				.equalsIgnoreCase("Email address already in use. Re-invite the agent or try a new email address.")) {
			Assert.assertEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.");
			System.out.println("Email already exists.Please try again.");
			logger.info("Email already exists case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.",
					"Email address already in use. Re-invite the agent or try a new email address.");
			logger.info("Email already exists case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture
		}
		waits(7);
		agent_links();
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);

	}

	public void alreadydeletedagent_add_same_agent_email_one_more_time() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(5);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);
		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);

	}

	public void alreadydeletedagent_add_same_agent_email_one_more_time_user_role() throws Exception {

		login();
		usersampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(5);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);
		login();
		usersampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);

	}

	public void mulitorg_send_agent_invitation_otherorg_try_to_add_same_agent() throws Exception {

		login();
		sampleAgents();
		waits(2);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		validateSignOut();
		waits(10);
		automations_login();
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
		login();
		sampleAgents();
		waits(1);
		if (agent.agent_email_duplicate.getText()
				.equalsIgnoreCase("Email address already in use. Re-invite the agent or try a new email address.")) {
			Assert.assertEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.");
			System.out.println("Email already exists.Please try again.");
			logger.info("Email already exists case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.",
					"Email address already in use. Re-invite the agent or try a new email address.");
			logger.info("Email already exists case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture
		}

		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		validateSignOut();
		waits(5);
		auto2_login();
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
		agent_links();
		waits(5);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

	public void mulitorg_send_agent_invitation_reject_otherorg_try_to_add_same_agent() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(3);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(3);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		waits(5);

		driver.switchTo().window(tabs1.get(1));

		waits(5);
		driver.get("https://yopmail.com/en/");
		waits(5);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(5);
		driver.switchTo().frame("ifmail");
		waits(5);
		link(agent.reject_button, "Reject Link").click();
		waits(3);
//		driver.close();
		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(2));
		waits(10);
		logger.info(agent.reject_page.getText());
		if (agent.reject_page.getText().equalsIgnoreCase("Your invitation is rejected successfully.")) {
			Assert.assertEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.");
			logger.info("Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.reject_page.getText(), "Your invitation is rejected successfully.",
					"Your invitation is rejected successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation is rejected successfully.");// ScreenShot
																								// capture
		}

		/*
		 * waits(5); button(agent.close_settings_popup, "login submit").click();
		 * logger.info("closing tools pop up"); waits(5);
		 * 
		 * validateSignOutLink(); validateSignOut(); waits(5); auto2_login();
		 * driver.findElement(By.xpath("//span[@class='s-title']")).click();
		 * agent_links(); manageButton(); deleteConfirmagent(); deleteagenttoaster();
		 */

	}

	public void addsameagent() throws Exception {
		login();
		usersampleAgents();
		waits(1);
		if (agent.agent_email_duplicate.getText()
				.equalsIgnoreCase("Email address already in use. Re-invite the agent or try a new email address.")) {
			Assert.assertEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.");
			System.out.println("Email already exists.Please try again.");
			logger.info("Email already exists case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.",
					"Email address already in use. Re-invite the agent or try a new email address.");
			logger.info("Email already exists case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture
		}
	}

	public void Agent_invite_sending_accept_the_invitation_otherorg_try_to_add_same_agent() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		waits(3);
		validateSignOut();
		waits(10);
		driver.manage().deleteAllCookies();
		waits(5);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		waits(5);

		driver.switchTo().window(tabs.get(1));

		waits(10);
		driver.get("https://yopmail.com/en/");
		waits(10);
		textbox(agent.email).sendKeys("sampleagent@yopmail.com");
		button(agent.gobutton, "login submit").submit();
		waits(10);
		driver.switchTo().frame("ifmail");
		waits(5);

		link(agent.accept_button, "accept Link").click();
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		waits(10);
		textbox(agent.password).sendKeys("tech1234");
		logger.info("passwordEntered: tech1234");
		textbox(agent.cpassword).sendKeys("tech1234");
		logger.info("confirm passwordEntered: tech1234");

		button(agent.setpassword_button, "login submit").submit();
		waits(10);
		validateSignOutLink();
		waits(3);
		validateSignOut();
		wait(10);
		automations_login();
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
		login();
		sampleAgents();
		waits(1);
		if (agent.agent_email_duplicate.getText()
				.equalsIgnoreCase("Email address already in use. Re-invite the agent or try a new email address.")) {
			Assert.assertEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.");
			System.out.println("Email already exists.Please try again.");
			logger.info("Email already exists case working fine");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture

		} else {
			Assert.assertNotEquals(agent.agent_email_duplicate.getText(),
					"Email address already in use. Re-invite the agent or try a new email address.",
					"Email address already in use. Re-invite the agent or try a new email address.");
			logger.info("Email already exists case not working");
			TestUtil.takeScreenshotAtEndOfTest("Email already exists.Please try again.");// ScreenShot
			// capture
		}
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);
		validateSignOutLink();
		validateSignOut();
		wait(30);
		auto2_login();
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
		agent_links();
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);

	}

	public void Agent_invite_sending_delete_agent_otherorg_try_to_add_same_agent() throws Exception {

		login();
		sampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		validateSignOut();
		waits(10);
		automations_login();
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
		login();
		automations_usersampleAgents();
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

	public void userAgent_invite_sending_delete_agent_otherorg_try_to_add_same_agent() throws Exception {

		login();
		usersampleAgents();
		waits(1);
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();
		waits(5);
		button(agent.close_settings_popup, "login submit").click();
		logger.info("closing tools pop up");
		waits(5);

		validateSignOutLink();
		validateSignOut();
		waits(10);
		automations_login();
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
		login();
		automations_usersampleAgents();
		logger.info(agent.agent_invite_toaster.getText());
		sampleagentinvitetoastermessage();
		waits(7);
		manageButton();
		deleteConfirmagent();
		deleteagenttoaster();

	}

}
