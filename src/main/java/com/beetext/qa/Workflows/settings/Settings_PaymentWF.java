package com.beetext.qa.Workflows.settings;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Settings_Payment_Pages;
import com.beetext.qa.util.TestUtil;

public class Settings_PaymentWF extends CommonMethods {

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Settings_Payment_Pages settings_Payment_Pages = new Settings_Payment_Pages();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void Login_Settings() throws Exception {

		textbox(Settings_Payment_Pages.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);

		textbox(Settings_Payment_Pages.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Settings_Payment_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_defaultPayment() throws Exception {

		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment));
			textbox(settings_Payment_Pages.planBilling_addPayment).isEnabled();
			logger.info("Default payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_TwoPayments() throws Exception {

		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment, "planBilling_addPayment").click();
		logger.info("click on planBilling_addPayment Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment_creditCard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment_creditCard));
			textbox(settings_Payment_Pages.planBilling_addPayment_creditCard).isEnabled();
			logger.info("Credit card payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false,
					verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment_creditCard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment_AchLink)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment_AchLink));
			textbox(settings_Payment_Pages.planBilling_addPayment_AchLink).isEnabled();
			logger.info("ACH payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment_AchLink));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_Select_creditcard_and_Validations() throws Exception {
		logger.info("Defaultlt save is in Disabled, Valid & Invalid CreditcardNumber, Valid & Invalid_expiryDate,"
				+ "Valid & Invalid Cvv, AllValidDetails Select PrimaryPayment-SaveEnabled ");
		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment, "planBilling_addPayment").click();
		logger.info("click on planBilling_addPayment Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment_creditCard, "planBilling_addPayment_creditCard").click();
		logger.info("click on planBilling_addPayment_creditCard Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_cardnumber)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_cardnumber));
			textbox(settings_Payment_Pages.planBilling_creditcard_cardnumber).isEnabled();
			logger.info("Credit card Number is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false,
					verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_cardnumber));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment, "planBilling_addPayment").click();
		logger.info("click on planBilling_addPayment Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment_creditCard, "planBilling_addPayment_creditCard").click();
		logger.info("click on planBilling_addPayment_creditCard Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_save));
			textbox(settings_Payment_Pages.planBilling_creditcard_save).isEnabled();
			logger.info("Save is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_save));
			logger.info("Save is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		textbox(settings_Payment_Pages.planBilling_creditcard_cardnumber).sendKeys("42424242");
		logger.info("enter the credit card number");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.planBilling_creditcard_expDate).sendKeys("01");
		logger.info("enter the expiry date");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.planBilling_creditcard_cvvcode).sendKeys("11");
		logger.info("enter the cvv date");
		implicitwaitAndLog(5);

		if (settings_Payment_Pages.planBilling_creditcard_invalidCard.getText()
				.equalsIgnoreCase("Card number is invalid.")) {
			Assert.assertEquals(settings_Payment_Pages.planBilling_creditcard_invalidCard.getText(),
					"Card number is invalid.", "planBilling_creditcard_invalidCard are matching");
			logger.info(settings_Payment_Pages.planBilling_creditcard_invalidCard.getText());
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_invalidCardTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.planBilling_creditcard_invalidCard.getText(),
					"Card number is invalid.", "planBilling_creditcard_invalidCard are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_invalidCardTestCase");// ScreenShot capture

		}

		if (settings_Payment_Pages.planBilling_creditcard_InvalidExpDate.getText()
				.equalsIgnoreCase("Expiration date is invalid.")) {
			Assert.assertEquals(settings_Payment_Pages.planBilling_creditcard_InvalidExpDate.getText(),
					"Expiration date is invalid.", "planBilling_creditcard_InvalidExpDate are matching");
			logger.info(settings_Payment_Pages.planBilling_creditcard_InvalidExpDate.getText());
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_InvalidExpDateTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.planBilling_creditcard_InvalidExpDate.getText(),
					"Expiration date is invalid.", "planBilling_creditcard_InvalidExpDate are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_InvalidExpDateTestCase");// ScreenShot capture

		}

		if (settings_Payment_Pages.planBilling_creditcard_invalidCvv.getText()
				.equalsIgnoreCase("Security code is invalid.")) {
			Assert.assertEquals(settings_Payment_Pages.planBilling_creditcard_invalidCvv.getText(),
					"Security code is invalid.", "planBilling_creditcard_invalidCvv are matching");
			logger.info(settings_Payment_Pages.planBilling_creditcard_invalidCvv.getText());
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_invalidCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.planBilling_creditcard_invalidCvv.getText(),
					"Security code is invalid.", "planBilling_creditcard_invalidCvv are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_invalidCvvTestCase");// ScreenShot capture

		}

		if (settings_Payment_Pages.planBilling_creditcard_3charerror.getText()
				.equalsIgnoreCase("Security code must be at least 3 digits.")) {
			Assert.assertEquals(settings_Payment_Pages.planBilling_creditcard_3charerror.getText(),
					"Security code must be at least 3 digits.", "planBilling_creditcard_3charerror are matching");
			logger.info(settings_Payment_Pages.planBilling_creditcard_3charerror.getText());
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_3charerrorTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.planBilling_creditcard_3charerror.getText(),
					"Security code must be at least 3 digits.", "planBilling_creditcard_3charerror are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("planBilling_creditcard_3charerrorTestCase");// ScreenShot capture

		}
		waitAndLog(2);
		textbox(settings_Payment_Pages.planBilling_creditcard_cardnumber).clear();
		logger.info("enter the credit card number");

		textbox(settings_Payment_Pages.planBilling_creditcard_cardnumber).sendKeys("4242424242424242");
		logger.info("enter the credit card number");

		textbox(settings_Payment_Pages.planBilling_creditcard_expDate).clear();
		logger.info("enter the expiry date");

		textbox(settings_Payment_Pages.planBilling_creditcard_expDate).sendKeys("0123");
		logger.info("enter the expiry date");

		textbox(settings_Payment_Pages.planBilling_creditcard_cvvcode).clear();
		logger.info("enter the cvv");

		textbox(settings_Payment_Pages.planBilling_creditcard_cvvcode).sendKeys("1234");
		logger.info("enter the cvv");
		waitAndLog(2);
		button(settings_Payment_Pages.planBilling_creditcard_PrimaryPayments, "planBilling_creditcard_PrimaryPayments")
				.click();
		logger.info("click on planBilling_creditcard_PrimaryPayments Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_save));
			textbox(settings_Payment_Pages.planBilling_creditcard_save).isEnabled();
			logger.info("Save is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.planBilling_creditcard_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(5);
		button(settings_Payment_Pages.planBilling_creditcard_save, "click on save").click();
		waitAndLog(5);
		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_creditCard_Back() throws Exception {

		waitAndLog(3);

		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment, "planBilling_addPayment").click();
		logger.info("click on planBilling_addPayment Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment_creditCard, "planBilling_addPayment_creditCard").click();
		logger.info("click on planBilling_addPayment_creditCard Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_creditcard_back, "planBilling_creditcard_back").click();
		logger.info("click on planBilling_creditcard_back Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment));
			textbox(settings_Payment_Pages.planBilling_addPayment).isEnabled();
			logger.info("Add payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.planBilling_addPayment));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_select_Achpayment_and_ACH_Validations() throws Exception {

		logger.info(
				" Select Achpayment,Click Achcontinue, Select Bank, Don't fillusername & password, Invalidusername & password"
						+ "tryanother, Acct_Routing");

		waitAndLog(3);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment, "planBilling_addPayment").click();
		logger.info("click on planBilling_addPayment Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment_AchLink, "planBilling_addPayment_AchLink").click();
		logger.info("click on planBilling_addPayment_AchLink Button");
		implicitwaitAndLog(2);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		implicitwaitAndLog(5);

		if (verifyElementIsEnabled(Settings_Payment_Pages.achContinue)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Settings_Payment_Pages.achContinue));
			button(Settings_Payment_Pages.achContinue, "click").isEnabled();
			logger.info("ACH continue is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Settings_Payment_Pages.achContinue));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(Settings_Payment_Pages.achContinue, "achContinue").click();
		logger.info("click on achContinue Button");
		waitAndLog(2);
		textbox(settings_Payment_Pages.ach_Bank_Search).sendKeys("Bank of America");
		logger.info("click on ach_Bank_Search Button");
		waitAndLog(2);
		button(Settings_Payment_Pages.ach_bankselection, "ach_bankselection").click();
		logger.info("click on ach_bankselection Button");
		waitAndLog(2);

		button(Settings_Payment_Pages.ach_bankselection1, "ach_bankselection").click();
		logger.info("click on ach_bankselection Button");
		implicitwaitAndLog(2);

		textbox(Settings_Payment_Pages.ach_username).sendKeys("");
		logger.info("enter the ach_username");
		implicitwaitAndLog(2);

		textbox(Settings_Payment_Pages.ach_password).sendKeys("");
		logger.info("enter the ach_password");
		implicitwaitAndLog(2);

		button(Settings_Payment_Pages.ach_submitbttn, "ach_submitbttn").click();
		logger.info("click on ach_submitbttn Button");
		implicitwaitAndLog(2);

		if (Settings_Payment_Pages.ach_usernamerequired.getText().equalsIgnoreCase("Username is required")) {
			Assert.assertEquals(Settings_Payment_Pages.ach_usernamerequired.getText(), "Username is required",
					"ach_usernamerequired is matching");
			logger.info(Settings_Payment_Pages.ach_usernamerequired.getText());
			TestUtil.takeScreenshotAtEndOfTest("ach_usernamerequiredTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(Settings_Payment_Pages.ach_usernamerequired.getText(), "Username is required",
					"ach_usernamerequired is not matching");
			TestUtil.takeScreenshotAtEndOfTest("ach_usernamerequiredTestCase");// ScreenShot capture
		}

		if (Settings_Payment_Pages.ach_passworedrequried.getText().equalsIgnoreCase("Password is required")) {
			Assert.assertEquals(Settings_Payment_Pages.ach_passworedrequried.getText(), "Password is required",
					"ach_passworedrequried is matching");
			logger.info(Settings_Payment_Pages.ach_passworedrequried.getText());
			TestUtil.takeScreenshotAtEndOfTest("ach_passworedrequriedTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(Settings_Payment_Pages.ach_passworedrequried.getText(), "Password is required",
					"ach_passworedrequried is not matching");
			TestUtil.takeScreenshotAtEndOfTest("ach_passworedrequriedTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		textbox(Settings_Payment_Pages.ach_username).clear();
		waitAndLog(2);
		textbox(Settings_Payment_Pages.ach_password).clear();
		waitAndLog(2);
		Settings_Payment_Pages.ach_username.sendKeys("pass_good");
		waitAndLog(2);

		Settings_Payment_Pages.ach_password.sendKeys("user_good");
		waitAndLog(2);

		button(Settings_Payment_Pages.ach_submitbttn, "ach_submitbttn").click();
		implicitwaitAndLog(5);

		if (Settings_Payment_Pages.ach_details_incorrect.getText().equalsIgnoreCase("Your answers were incorrect")) {
			Assert.assertEquals(Settings_Payment_Pages.ach_details_incorrect.getText(), "Your answers were incorrect",
					"ach_details_incorrect is matching");
			logger.info(Settings_Payment_Pages.ach_details_incorrect.getText());
			TestUtil.takeScreenshotAtEndOfTest("ach_details_incorrectTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(Settings_Payment_Pages.ach_details_incorrect.getText(),
					"Your answers were incorrect", "ach_details_incorrect is not matching");
			TestUtil.takeScreenshotAtEndOfTest("ach_details_incorrectTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);

		if (Settings_Payment_Pages.ach_invaliddetails_errormsg.getText().equalsIgnoreCase(
				"For security reasons, your account may be locked after several unsuccessful attempts")) {
			Assert.assertEquals(Settings_Payment_Pages.ach_invaliddetails_errormsg.getText(),
					"For security reasons, your account may be locked after several unsuccessful attempts",
					"ach_invaliddetails_errormsg is matching");
			logger.info(Settings_Payment_Pages.ach_invaliddetails_errormsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("ach_invaliddetils_errormsgTestCase");// ScreenShot capture
		} else {
			Assert.assertNotEquals(Settings_Payment_Pages.ach_invaliddetails_errormsg.getText(),
					"For security reasons, your account may be locked after several unsuccessful attempts",
					"ach_invaliddetails_errormsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("ach_invaliddetails_errormsgTestCase");// ScreenShot capture
		}

		waitAndLog(5);
		button(Settings_Payment_Pages.ach_retryanswer, "Click on retry").click();
		;
		waitAndLog(5);
		Settings_Payment_Pages.ach_username.sendKeys("user_good");
		implicitwaitAndLog(2);

		Settings_Payment_Pages.ach_password.sendKeys("pass_good");
		implicitwaitAndLog(2);

		button(Settings_Payment_Pages.ach_submitbttn, "ach_submitbttn").click();
		implicitwaitAndLog(5);

		if (verifyElementIsEnabled(Settings_Payment_Pages.ach_acct1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Settings_Payment_Pages.ach_acct1));
			button(Settings_Payment_Pages.ach_acct1, "click").isEnabled();
			logger.info("ACH Bank Acct 1 is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Settings_Payment_Pages.ach_acct1));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		implicitwaitAndLog(2);

		String AccountDetails = Settings_Payment_Pages.ach_acct1_Description.getText();
		logger.info("Account Details ---->  " + AccountDetails);

		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(Settings_Payment_Pages.ach_acct2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Settings_Payment_Pages.ach_acct2));
			button(Settings_Payment_Pages.ach_acct2, "click").isEnabled();
			logger.info("ACH Bank Acct 2 is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Settings_Payment_Pages.ach_acct2));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		implicitwaitAndLog(5);

		String AccountDetails1 = Settings_Payment_Pages.ach_acct2_Description.getText();
		logger.info("Account Details ---->  " + AccountDetails1);

		button(Settings_Payment_Pages.ach_acct1, "ach_acct1").click();
		logger.info("click on Account 1");
		implicitwaitAndLog(2);

		button(Settings_Payment_Pages.achContinue, "achContinue").click();
		logger.info("click on achContinue Button");
		implicitwaitAndLog(2);

		textbox(Settings_Payment_Pages.ach_secretCode).sendKeys("1234");
		logger.info("Enter the SecretCode");
		implicitwaitAndLog(2);

		button(Settings_Payment_Pages.ach_secretcode_submit, "ach_secretcode_submit").click();
		logger.info("click on ach_secretcode_submit Button");
		implicitwaitAndLog(5);

		if (verifyElementIsEnabled(Settings_Payment_Pages.ach_plaidChecking)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Settings_Payment_Pages.ach_plaidChecking));
			button(Settings_Payment_Pages.ach_plaidChecking, "click").isEnabled();
			logger.info("ACH Bank Plaid Checking is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Settings_Payment_Pages.ach_plaidChecking));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		implicitwaitAndLog(2);

		String plaidChecking = Settings_Payment_Pages.ach_plaidChecking_Description.getText();
		logger.info("Plaid Checking Details ---->  " + plaidChecking);

		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(Settings_Payment_Pages.ach_plaidsaving)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Settings_Payment_Pages.ach_plaidsaving));
			button(Settings_Payment_Pages.ach_plaidsaving, "click").isEnabled();
			logger.info("ACH Bank Plaid Saving is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Settings_Payment_Pages.ach_plaidsaving));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		implicitwaitAndLog(2);

		String plaidSavings = Settings_Payment_Pages.ach_plaidsaving_Description.getText();
		logger.info("Plaid Savings Details ---->  " + plaidSavings);

		button(Settings_Payment_Pages.ach_plaidsaving_Description, "ach_acct1").click();
		logger.info("click on ach_plaidsaving");
		waitAndLog(5);

		JSClick(Settings_Payment_Pages.achContinue);
		logger.info("click on achContinue Button");
		waitAndLog(5);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://automation.beetexting.com/");
		driver.switchTo().window(tabs.get(0));
		driver.close();
		driver.switchTo().window(tabs.get(1));
		waitAndLog(5);
	}

	public void Settings_planBilling_autoPayments() throws Exception {

		waitAndLog(3);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		JSClick(settings_Payment_Pages.settings_planBilling);
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(Settings_Payment_Pages.Auto_payment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Settings_Payment_Pages.Auto_payment));
			button(Settings_Payment_Pages.Auto_payment, "click").isEnabled();
			logger.info("Auto Payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Settings_Payment_Pages.Auto_payment));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_default_Payment() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(Settings_Payment_Pages.default_payment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(Settings_Payment_Pages.default_payment));
			button(Settings_Payment_Pages.default_payment, "click").isEnabled();
			logger.info("Default Payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(Settings_Payment_Pages.default_payment));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_Select_creditcard_AllValidDetails_Save() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment, "planBilling_addPayment").click();
		logger.info("click on planBilling_addPayment Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment_creditCard, "planBilling_addPayment_creditCard").click();
		logger.info("click on planBilling_addPayment_creditCard Button");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.planBilling_creditcard_cardnumber).sendKeys("4242424242424242");
		logger.info("enter the cvv date");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.planBilling_creditcard_expDate).sendKeys("1234");
		logger.info("enter the cvv");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.planBilling_creditcard_cvvcode).sendKeys("1234");
		logger.info("enter the cvv");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_creditcard_save, "planBilling_creditcard_save").click();
		logger.info("click on planBilling_creditcard_save Button");
		implicitwaitAndLog(2);

		String Credit_Card_Toaster = settings_Payment_Pages.creditCard_toaster.getText();
		if (verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster));
			textbox(settings_Payment_Pages.creditCard_toaster).isEnabled();
			logger.info(Credit_Card_Toaster);
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_Ach_AllValidDetails() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment, "planBilling_addPayment").click();
		logger.info("click on planBilling_addPayment Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.planBilling_addPayment_AchLink, "planBilling_addPayment_AchLink").click();
		logger.info("click on planBilling_addPayment_AchLink Button");
		implicitwaitAndLog(2);

		int size = driver.findElements(By.tagName("iframe")).size();
		logger.info("size" + size);
		driver.switchTo().frame(2);
		implicitwaitAndLog(2);

		button(Settings_Payment_Pages.achContinue, "achContinue").click();
		logger.info("click on achContinue Button");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.ach_Bank_Search).clear();
		textbox(settings_Payment_Pages.ach_Bank_Search).sendKeys("Bank of America");
		logger.info("click on ach_Bank_Search Button");
		waitAndLog(2);
		button(Settings_Payment_Pages.ach_bankselection, "ach_bankselection").click();
		logger.info("click on ach_bankselection Button");
		waitAndLog(2);

		button(Settings_Payment_Pages.ach_bankselection1, "ach_bankselection").click();
		logger.info("click on ach_bankselection Button");
		waitAndLog(2);

		Settings_Payment_Pages.ach_username.sendKeys("user_good");
		implicitwaitAndLog(2);

		Settings_Payment_Pages.ach_password.sendKeys("pass_good");
		implicitwaitAndLog(2);

		button(Settings_Payment_Pages.ach_submitbttn, "ach_submitbttn").click();
		implicitwaitAndLog(5);

		button(Settings_Payment_Pages.ach_acct1, "ach_acct1").click();
		logger.info("click on Account 1");
		implicitwaitAndLog(5);

		button(Settings_Payment_Pages.achContinue, "achContinue").click();
		logger.info("click on achContinue Button");
		implicitwaitAndLog(5);

		textbox(Settings_Payment_Pages.ach_secretCode).sendKeys("1234");
		logger.info("Enter the SecretCode");
		implicitwaitAndLog(5);

		button(Settings_Payment_Pages.ach_secretcode_submit, "ach_secretcode_submit").click();
		logger.info("click on ach_secretcode_submit Button");
		implicitwaitAndLog(5);

		button(Settings_Payment_Pages.ach_plaidsaving_Description, "ach_acct1").click();
		logger.info("click on ach_plaidsaving");
		implicitwaitAndLog(5);

		JSClick(Settings_Payment_Pages.achContinue);
		logger.info("click on achContinue Button");
		waitAndLog(5);

		JSClick(Settings_Payment_Pages.achContinue);
		logger.info("click on achContinue Button");
		waitAndLog(5);

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to("https://automation.beetexting.com/");
		driver.switchTo().window(tabs.get(0));
		driver.close();
		driver.switchTo().window(tabs.get(1));
		waitAndLog(5);
	}

	public void Settings_planBilling_manage_primaryPayment() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.manage_billing, "manage_billing").click();
		logger.info("click on manage_billing Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.save, "save").click();
		logger.info("click on save Button");
		implicitwaitAndLog(2);

		String toaster = settings_Payment_Pages.creditCard_toaster.getText();

		if (verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster));
			textbox(settings_Payment_Pages.creditCard_toaster).isEnabled();
			logger.info(toaster);
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_planBilling_manage_changeExpiryDate() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_planBilling, "settings_planBilling").click();
		logger.info("click on settings_planBilling Button");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.manage_billing, "manage_billing").click();
		logger.info("click on manage_billing Button");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.planBilling_creditcard_expDate).clear();
		logger.info("enter the cvv");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.planBilling_creditcard_expDate).sendKeys("1245");
		logger.info("enter the cvv");
		implicitwaitAndLog(2);

		button(settings_Payment_Pages.save, "save").click();
		logger.info("click on save Button");
		implicitwaitAndLog(2);

		String toaster = settings_Payment_Pages.creditCard_toaster.getText();
		logger.info("Credit card Toaster ----->" + toaster);

		if (verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster));
			textbox(settings_Payment_Pages.creditCard_toaster).isEnabled();
			logger.info("Credit card added Successfully");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.creditCard_toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_roles() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_roles, "setting_roles").click();
		logger.info("click on setting_roles Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.setting_roles_roleCapbilities)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.setting_roles_roleCapbilities));
			button(settings_Payment_Pages.setting_roles_roleCapbilities, "click").isEnabled();
			logger.info("setting_roles RoleCapbilities is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.setting_roles_roleCapbilities));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(settings_Payment_Pages.setting_roles_admin)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.setting_roles_admin));
			button(settings_Payment_Pages.setting_roles_admin, "click").isEnabled();
			logger.info("setting_roles Admin is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.setting_roles_admin));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(settings_Payment_Pages.setting_roles_agent)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.setting_roles_agent));
			button(settings_Payment_Pages.setting_roles_agent, "click").isEnabled();
			logger.info("setting_roles Agent is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.setting_roles_agent));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String Role = settings_Payment_Pages.setting_roles_admin.getText();
		logger.info("Text ---->" + Role);

		String Admin_Roles = settings_Payment_Pages.setting_roles_admin.getText();
		logger.info("Text ---->" + Admin_Roles);

		String Agent_Roles = settings_Payment_Pages.setting_roles_agent.getText();
		logger.info("Text ---->" + Agent_Roles);
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_roles_mySellerAccount() throws Exception {
		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		if (verifyElementIsEnabled(settings_Payment_Pages.setting_mysellerAcct)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.setting_mysellerAcct));
			button(settings_Payment_Pages.setting_mysellerAcct, "click").isEnabled();
			logger.info("setting My Seller Account is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.setting_mysellerAcct));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_roles_mySellerAccount_stripeDetails() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_roles_mySellerAccount_mockup_text() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(2);

		String text = settings_Payment_Pages.sellerAcct_Page.getText();
		logger.info("text ----> " + text);

		String text1 = settings_Payment_Pages.sellerAcct_Page_enableTips.getText();
		logger.info("text ----> " + text1);
		waitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_roles_mySellerAccount_Click_StripeAcct_updateLink() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.stripe_Acct_Updatelink)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.stripe_Acct_Updatelink));
			link(settings_Payment_Pages.stripe_Acct_Updatelink, "link").isEnabled();
			logger.info("Click here to update Stripe Acct Details");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.stripe_Acct_Updatelink));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		link(settings_Payment_Pages.stripe_Acct_Updatelink, "link").click();
		logger.info("click on stripe_Acct_Updatelink");
		waitAndLog(2);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		driver.switchTo().window(tabs2.get(0));
		waitAndLog(2);
		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_roles_mySellerAccount_enableTips_DisableTips() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.settings, "Click on Settings link").click();
		logger.info("click on settings Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		waitAndLog(2);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		implicitwaitAndLog(2);

		String toaster = settings_Payment_Pages.tips_on_off_toaster.getText();
		logger.info("Toaster Message-----> " + toaster);
		waitAndLog(2);
		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		String toaster1 = settings_Payment_Pages.tips_on_off_toaster.getText();
		logger.info("Toaster Message-----> " + toaster1);
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.settings_Close, "click on settings_Close").click();
		logger.info("click on settings_Close Button");
		waitAndLog(3);

	}

	public void Settings_roles_mySellerAccount_dollarSymbol() throws Exception {

		waitAndLog(2);

		if (verifyElementIsEnabled(settings_Payment_Pages.msg_payment_Icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_Icon));
			button(settings_Payment_Pages.msg_payment_Icon, "click").isEnabled();
			logger.info("Dollar Symbol is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_Icon));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Settings_roles_errorMsg_sellerAcct_NotAdded() throws Exception {

		waitAndLog(2);
		validateSignOutLink();
		validateSignOut();

		textbox(Settings_Payment_Pages.user_id).sendKeys("Automationblock@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(Settings_Payment_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Settings_Payment_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(3);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		waitAndLog(2);

		settings_Payment_Pages.msg_payment_errorMsg.getText();
		logger.info(settings_Payment_Pages.msg_payment_errorMsg.getText());
		if (settings_Payment_Pages.msg_payment_errorMsg.getText().equalsIgnoreCase(
				"Seller Payment Account is not added, go to Settings Page and use this feature to get payments thru text message quickly.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_errorMsg.getText(),
					"Seller Payment Account is not added, go to Settings Page and use this feature to get payments thru text message quickly.",
					"msg_payment_errorMsg are matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_errorMsgCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_errorMsg.getText(),
					"Seller Payment Account is not added, go to Settings Page and use this feature to get payments thru text message quickly.",
					"msg_payment_errorMsg are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_errorMsgCvvTestCase");// ScreenShot capture

		}
		waitAndLog(2);
		validateSignOutLink();
		validateSignOut();

		textbox(Settings_Payment_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(Settings_Payment_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Settings_Payment_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(3);

	}

	public void Settings_roles_amount_Dollar_Symbol() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(2);

		settings_Payment_Pages.msg_payment_amounttext.getText();
		logger.info(settings_Payment_Pages.msg_payment_amounttext.getText());
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
	}

	public void Settings_roles_Bill_Description_100Char_ErrorMsg() throws Exception {

		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		String generatedstring = RandomStringUtils.randomAlphabetic(100);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys(generatedstring);
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		settings_Payment_Pages.msg_payment_billDescription_lengtherror.getText();
		logger.info(settings_Payment_Pages.msg_payment_billDescription_lengtherror.getText());
		if (settings_Payment_Pages.msg_payment_billDescription_lengtherror.getText()
				.equalsIgnoreCase("Only 100 characters allowed.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_billDescription_lengtherror.getText(),
					"Only 100 characters allowed.", "msg_payment_billDescription_lengtherror are matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_billDescription_lengtherrorCvvTestCase");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_billDescription_lengtherror.getText(),
					"Only 100 characters allowed.", "msg_payment_billDescription_lengtherror are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_billDescription_lengtherrorCvvTestCase");// ScreenShot
																										// capture

		}
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");

	}

	public void Settings_roles_Amount_ErrorMsg() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("999999");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(2);

		settings_Payment_Pages.msg_payment_Amount_Maxerror.getText();
		logger.info(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText());
		if (settings_Payment_Pages.msg_payment_Amount_Maxerror.getText()
				.equalsIgnoreCase("Amount cannot exceed $100000.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror are matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorerrorCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorCvvTestCase");// ScreenShot capture

		}
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_crossmark, "click").click();
		logger.info("click on msg_payment_crossmark Button");

	}

	public void Settings_roles_2DecimalAmount() throws Exception {

		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		waitAndLog(2);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_crossmark, "click").click();
		logger.info("click on msg_payment_crossmark Button");
		waitAndLog(2);

	}

	public void Settings_billDescription_errorMsg() throws Exception {

		waitAndLog(3);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		waitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		waitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).clear();
		logger.info("Enter data in msg_payment_BillDescription ");
		waitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		logger.info("Bill description is required.");

		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_crossmark, "click").click();
		logger.info("click on msg_payment_crossmark Button");
		waitAndLog(2);

	}

	public void Settings_bill_crossmark() throws Exception {

		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_crossmark, "click").click();
		logger.info("click on msg_payment_crossmark Button");
		implicitwaitAndLog(5);

	}

	public void Settings_bill_send_payment() throws Exception {

		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		implicitwaitAndLog(5);

	}

	public void Settings_bill_send_payment_link_withText() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(2);

		String text = settings_Payment_Pages.msg_payment_textWithLink.getText();
		String link = settings_Payment_Pages.msg_payment_link.getText();

		logger.info(text + link);

	}

	public void Settings_click_linkToredirect_paymentPage_FailedPayment() throws Exception {

		logger.info(" Click on link Redirect to PaymentPage,BackEnable, Click on back - PaymentFailed");
		waitAndLog(2);
//		link(settings_Payment_Pages.settings, "link").click();
//		logger.info("click on settings Button");
//		implicitwaitAndLog(5);
//
//		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
//		logger.info("click on setting_mysellerAcct Button");
//		implicitwaitAndLog(5);
//
//		settings_Payment_Pages.sellerAcct_tips.click();
//		logger.info("click on sellerAcct_tips Button");
//		waitAndLog(2);
//
//		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
//		logger.info("click on setting_crossIcon Button");
//		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "Search output").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(5);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waitAndLog(5);

		if (verifyElementIsEnabled(settings_Payment_Pages.msg_payment_billDes_InPaymentPage)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_billDes_InPaymentPage));
			textbox(settings_Payment_Pages.msg_payment_billDes_InPaymentPage).isEnabled();
			logger.info("Bill description is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false,
					verifyElementIsEnabled(settings_Payment_Pages.msg_payment_billDes_InPaymentPage));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(settings_Payment_Pages.msg_payment_back)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_back));
			textbox(settings_Payment_Pages.msg_payment_back).isEnabled();
			logger.info("Back is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_back));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		logger.info("Tips not added");
		waitAndLog(2);
		link(settings_Payment_Pages.msg_payment_back, "Search output").click();
		logger.info("click on msg_payment_back");
		implicitwaitAndLog(10);

		settings_Payment_Pages.msg_payment_Cancel_msg.getText();
		logger.info(settings_Payment_Pages.msg_payment_Cancel_msg.getText());
		if (settings_Payment_Pages.msg_payment_Cancel_msg.getText().equalsIgnoreCase("Payment cancelled or failed")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_Cancel_msg.getText(), "Payment cancelled or failed",
					"msg_payment_Cancel_msg are matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Cancel_msgCardTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_Cancel_msg.getText(),
					"Payment cancelled or failed", "msg_payment_Cancel_msg are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Cancel_msgCardTestCase");// ScreenShot capture

		}
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));
		waitAndLog(2);
//		link(settings_Payment_Pages.settings, "link").click();
//		logger.info("click on settings Button");
//		implicitwaitAndLog(5);
//
//		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
//		logger.info("click on setting_mysellerAcct Button");
//		implicitwaitAndLog(5);
//
//		settings_Payment_Pages.sellerAcct_tips.click();
//		logger.info("click on sellerAcct_tips Button");
//		waitAndLog(2);
//
//		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
//		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}

	public void payment_successfull_msg() throws Exception {

		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");

		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_email).sendKeys("auto1@yopmail.com");
		logger.info("Input Text for email");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_cardNumber).sendKeys("4242424242424242");
		logger.info("Input Text for Card Number");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_cardExpiry).sendKeys("1234");
		logger.info("Input Text for Expiry");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_cardcvc).sendKeys("1234");
		logger.info("Input Text for cvc");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_nameOnCard).sendKeys("AutoTest");
		logger.info("Input Text for cvc");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_billingCountry, "click").click();
		logger.info("click on billing country");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_billingCountry_optionUS, "click").click();
		logger.info("click on billing country US");
		waitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_billingPostalCode).sendKeys("40003");
		logger.info("Input Text for Postal Code");
		waitAndLog(5);

		JSClick(settings_Payment_Pages.msg_payment_submit);
		logger.info("click on submit");
		waitAndLog(5);
//
//		button(settings_Payment_Pages.msg_payment_submit, "click").click();
//		logger.info("click on submit");
//		waitAndLog(5);

		settings_Payment_Pages.payment_Successfull_Msg.getText();
		logger.info(settings_Payment_Pages.payment_Successfull_Msg.getText());
		if (settings_Payment_Pages.payment_Successfull_Msg.getText().equalsIgnoreCase("Payment was successful!")) {
			Assert.assertEquals(settings_Payment_Pages.payment_Successfull_Msg.getText(), "Payment was successful!",
					"Payment was successful! is matching");
			logger.info("Tips are not added");
			TestUtil.takeScreenshotAtEndOfTest("Payment was successful!TestCase");// ScreenShot
																					// capture
		} else {
			Assert.assertNotEquals(settings_Payment_Pages.payment_Successfull_Msg.getText(), "Payment was successful!",
					"Payment was successful! is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Payment was successful!TestCase");// ScreenShot
																					// capture
		}
		driver.close();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));

		waitAndLog(5);

		settings_Payment_Pages.Payment_Msg.getText();
		logger.info(settings_Payment_Pages.Payment_Msg.getText());

		settings_Payment_Pages.Pinmessage_amount.getText();
		logger.info(settings_Payment_Pages.Pinmessage_amount.getText());

	}

	public void Settings_click_linkToredirect_paymentPage_tips_added_or_not() throws Exception {

		waitAndLog(2);

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(5);

		button(settings_Payment_Pages.composebutton, "click on compose button").click();
		logger.info("click on Compose Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		implicitwaitAndLog(5);

		if (verifyElementIsEnabled(settings_Payment_Pages.msg_payment_addTip)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_addTip));
			textbox(settings_Payment_Pages.msg_payment_addTip).isEnabled();
			logger.info("Add Tip is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_addTip));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}

	public void Settings_tips_display() throws Exception {

		waitAndLog(2);
		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		implicitwaitAndLog(5);

		String tip = settings_Payment_Pages.tips_10.getText();
		logger.info("Tip = " + tip);

		String tip1 = settings_Payment_Pages.tips_15.getText();
		logger.info("Tip = " + tip1);

		String tip2 = settings_Payment_Pages.tips_20.getText();
		logger.info("Tip = " + tip2);

		String tip3 = settings_Payment_Pages.NoTip.getText();
		logger.info("Tip = " + tip3);

		implicitwaitAndLog(2);

		driver.close();

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);
		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}

	public void Settings_tips_display_totalAmount_withTip() throws Exception {

		waitAndLog(2);
		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		implicitwaitAndLog(5);

		String total_Amount = settings_Payment_Pages.Total_withTip.getText();
		logger.info("Total Payment with 15% Tip ---> " + total_Amount);

		waitAndLog(2);

		driver.close();

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		waitAndLog(2);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}

	public void Settings_tips_display_totalAmount_with_10PercentTip() throws Exception {

		waitAndLog(2);

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		waitAndLog(3);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		implicitwaitAndLog(5);

		button(settings_Payment_Pages.tips_10, "tips_10").click();
		logger.info("click on tips_10 Button");
		implicitwaitAndLog(5);

		String total_Amount = settings_Payment_Pages.Total_withTip.getText();
		logger.info("Total Payment with 10% Tip ---> " + total_Amount);

		if (verifyElementIsEnabled(settings_Payment_Pages.pay)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay));
			textbox(settings_Payment_Pages.pay).isEnabled();
			logger.info("payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);

		driver.close();

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}

	public void Settings_tips_display_totalAmount_with_customTip() throws Exception {

		waitAndLog(2);

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		implicitwaitAndLog(5);

		button(settings_Payment_Pages.CustomTip, "tips_10").click();
		logger.info("click on CustomTip Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.CustomTip_text).sendKeys("1.22");
		logger.info("Input Text for contact Search");
		implicitwaitAndLog(5);

		String total_Amount = settings_Payment_Pages.Total_withTip.getText();
		logger.info("Total Payment with custom Tip ---> " + total_Amount);

		if (verifyElementIsEnabled(settings_Payment_Pages.pay)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay));
			textbox(settings_Payment_Pages.pay).isEnabled();
			logger.info("payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);

		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);
	}

	public void Settings_tips_display_totalAmount_with_NoTip() throws Exception {

		waitAndLog(2);
		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		waitAndLog(2);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		waitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		waitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		waitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		waitAndLog(2);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		implicitwaitAndLog(5);

		button(settings_Payment_Pages.NoTip, "tips_10").click();
		logger.info("click on NoTip Button");
		implicitwaitAndLog(5);

		String total_Amount = settings_Payment_Pages.Total_withTip.getText();
		logger.info("Total Payment with custom Tip ---> " + total_Amount);

		if (verifyElementIsEnabled(settings_Payment_Pages.pay)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay));
			textbox(settings_Payment_Pages.pay).isEnabled();
			logger.info("payment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		implicitwaitAndLog(10);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}

	public void tips_payment_successfull_msg() throws Exception {

		waitAndLog(2);
		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(5);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(5);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waitAndLog(5);

		button(settings_Payment_Pages.tips_submit, "click").click();
		logger.info("click on tips_submit");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_email).sendKeys("auto1@yopmail.com");
		logger.info("Input Text for email");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_cardNumber).sendKeys("4242424242424242");
		logger.info("Input Text for Card Number");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_cardExpiry).sendKeys("1234");
		logger.info("Input Text for Expiry");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_cardcvc).sendKeys("1234");
		logger.info("Input Text for cvc");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_nameOnCard).sendKeys("AutoTest");
		logger.info("Input Text for cvc");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_billingCountry, "click").click();

		logger.info("click on billing country");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_billingCountry_optionUS, "click").click();
		logger.info("click on billing country US");
		waitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_billingPostalCode).sendKeys("40003");
		logger.info("Input Text for Postal Code");
		waitAndLog(5);

		JSClick(settings_Payment_Pages.msg_payment_submit);
		logger.info("click on submit");
		waitAndLog(3);

		settings_Payment_Pages.payment_Successfull_Msg.getText();
		logger.info(settings_Payment_Pages.payment_Successfull_Msg.getText());
		if (settings_Payment_Pages.payment_Successfull_Msg.getText().equalsIgnoreCase("Payment was successful!")) {
			Assert.assertEquals(settings_Payment_Pages.payment_Successfull_Msg.getText(), "Payment was successful!",
					"Payment was successful! is matching");
			logger.info("Tips are not added");
			TestUtil.takeScreenshotAtEndOfTest("Payment was successful!TestCase");// ScreenShot
																					// capture
		} else {
			Assert.assertNotEquals(settings_Payment_Pages.payment_Successfull_Msg.getText(), "Payment was successful!",
					"Payment was successful! is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Payment was successful!TestCase");// ScreenShot
																					// capture
		}

		waitAndLog(3);
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(1));

		waitAndLog(5);

		settings_Payment_Pages.payment_already_Paid.getText();
		logger.info(settings_Payment_Pages.payment_already_Paid.getText());
		if (settings_Payment_Pages.payment_already_Paid.getText().equalsIgnoreCase("Payment was paid already!")) {
			Assert.assertEquals(settings_Payment_Pages.payment_already_Paid.getText(), "Payment was paid already!",
					"Payment was paid already! is matching");
			TestUtil.takeScreenshotAtEndOfTest("Payment was paid already!TestCase");// ScreenShot
																					// capture
		} else {
			Assert.assertNotEquals(settings_Payment_Pages.payment_already_Paid.getText(), "Payment was paid already!",
					"Payment was paid already! is not matching");
			TestUtil.takeScreenshotAtEndOfTest("Payment was paid already!TestCase");// ScreenShot
																					// capture
		}
		driver.close();
		driver.switchTo().window(tabs3.get(0));
		waitAndLog(2);
	}

	public void tips_ON_OFF() throws Exception {

		waitAndLog(3);
		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(3);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waitAndLog(3);

		if (verifyElementIsEnabled(settings_Payment_Pages.msg_payment_addTip)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_addTip));
			textbox(settings_Payment_Pages.msg_payment_addTip).isEnabled();
			logger.info("Add Tip is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.msg_payment_addTip));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		waitAndLog(5);

		logger.info("Back is Enabled");

		logger.info("Tips Off");
	}

	public void Send_payment_Link() throws Exception {

		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1.23");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(3);

		logger.info("Successfully Payment Link Send");
	}

	public void none_options_enabled() throws Exception {

		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		waitAndLog(3);

		if (verifyElementIsEnabled(settings_Payment_Pages.pin_attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pin_attachment));
			textbox(settings_Payment_Pages.pin_attachment).isEnabled();
			logger.info("pin attachment is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pin_attachment));
			logger.info("pin attachment is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Settings_roles_Amount_ErrorMsg_for_5Digits() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1000005");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(2);

		if (settings_Payment_Pages.msg_payment_Amount_Maxerror.getText()
				.equalsIgnoreCase("Amount cannot exceed $100000.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror are matching");
			logger.info(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText());
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorerrorCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorCvvTestCase");// ScreenShot capture

		}
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_crossmark, "click").click();
		logger.info("click on msg_payment_crossmark Button");

	}

	public void Settings_roles_Amount_ErrorMsg_For_Morethan_100000() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(2);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("1000005");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(2);

		if (settings_Payment_Pages.msg_payment_Amount_Maxerror.getText()
				.equalsIgnoreCase("Amount cannot exceed $100000.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror are matching");
			logger.info(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText());
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorerrorCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorCvvTestCase");// ScreenShot capture

		}
		waitAndLog(2);
		button(settings_Payment_Pages.msg_payment_crossmark, "click").click();
		logger.info("click on msg_payment_crossmark Button");

	}

	public void Settings_bill_send_payment_link_with_100000_No_error_redirects_to_PaymentPage() throws Exception {

		waitAndLog(2);
		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("100000");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(5);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));
		waitAndLog(3);
		driver.close();
		driver.switchTo().window(tabs1.get(0));

		waitAndLog(3);
	}

	public void tips_ON_GiveAmount_100000_Send_PaymentLink_SelectTip_and_CustomTip_as_1_Pay_Button_In_disabled()
			throws Exception {

		waitAndLog(3);
		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("100000");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(3);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waitAndLog(3);
		button(settings_Payment_Pages.tips_10, "tips_10").click();
		logger.info("click on tips_10 Button");

		if (verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			textbox(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page).isEnabled();
			logger.info("Pay in Tips Payment Page is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			logger.info("Pay in Tips Payment Page is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText()
				.equalsIgnoreCase("Amount cannot exceed $100000.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror_in_TipPage are matching");
			logger.info(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText());
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorerrorCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror_in_TipPage are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorCvvTestCase");// ScreenShot capture

		}

		button(settings_Payment_Pages.CustomTip, "tips_10").click();
		logger.info("click on CustomTip Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.CustomTip_text).sendKeys("1");
		logger.info("Input Text for contact Search");
		implicitwaitAndLog(5);

		if (verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			textbox(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page).isEnabled();
			logger.info("Pay in Tips Payment Page is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			logger.info("Pay in Tips Payment Page is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText()
				.equalsIgnoreCase("Amount cannot exceed $100000.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror_in_TipPage are matching");
			logger.info(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText());
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorerrorCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror_in_TipPage are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorCvvTestCase");// ScreenShot capture

		}
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}

	public void tips_ON_GiveAmount_99999_Send_PaymentLink_SelectTip_Pay_Button_In_disabled_CustomTip_as_1_NoTip_enable()
			throws Exception {

		waitAndLog(3);
		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(2);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

		button(settings_Payment_Pages.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

		button(settings_Payment_Pages.msg_payment_Icon, "click").click();
		logger.info("click on msg_payment_Icon Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.msg_payment_BillDescription).sendKeys("Test");
		logger.info("Enter data in msg_payment_BillDescription ");
		implicitwaitAndLog(3);

		textbox(settings_Payment_Pages.msg_payment_Amount).sendKeys("99999");
		logger.info("Enter data in msg_payment_Amount ");
		implicitwaitAndLog(3);

		button(settings_Payment_Pages.msg_payment_send, "click").click();
		logger.info("click on msg_payment_send Button");
		waitAndLog(3);

		link(settings_Payment_Pages.msg_payment_link, "click").click();
		logger.info("click on msg_payment_link");
		implicitwaitAndLog(10);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		waitAndLog(3);
		button(settings_Payment_Pages.tips_10, "tips_10").click();
		logger.info("click on tips_10 Button");

		if (verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			textbox(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page).isEnabled();
			logger.info("Pay in Tips Payment Page is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			logger.info("Pay in Tips Payment Page is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText()
				.equalsIgnoreCase("Amount cannot exceed $100000.")) {
			Assert.assertEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror_in_TipPage are matching");
			logger.info(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText());
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorerrorCvvTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(settings_Payment_Pages.msg_payment_Amount_Maxerror_in_TipPage.getText(),
					"Amount cannot exceed $100000.", "msg_payment_Amount_Maxerror_in_TipPage are not  matching");
			TestUtil.takeScreenshotAtEndOfTest("msg_payment_Amount_MaxerrorCvvTestCase");// ScreenShot capture

		}

		button(settings_Payment_Pages.CustomTip, "tips_10").click();
		logger.info("click on CustomTip Button");
		implicitwaitAndLog(5);

		textbox(settings_Payment_Pages.CustomTip_text).sendKeys("1");
		logger.info("Input Text for contact Search");
		implicitwaitAndLog(5);

		if (verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			textbox(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page).isEnabled();
			logger.info("Pay in Tips Payment Page is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			logger.info("Pay in Tips Payment Page is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(settings_Payment_Pages.NoTip, "NoTip").click();
		logger.info("click on NoTip Button");
		implicitwaitAndLog(5);

		if (verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page)) {
			Assert.assertEquals(true, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			textbox(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page).isEnabled();
			logger.info("Pay in Tips Payment Page is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(settings_Payment_Pages.pay_in_Tips_Enabled_Payment_page));
			logger.info("Pay in Tips Payment Page is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		driver.close();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));

		link(settings_Payment_Pages.settings, "link").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(5);

		button(settings_Payment_Pages.setting_mysellerAcct, "setting_roles").click();
		logger.info("click on setting_mysellerAcct Button");
		implicitwaitAndLog(5);

		settings_Payment_Pages.sellerAcct_tips.click();
		logger.info("click on sellerAcct_tips Button");
		waitAndLog(3);

		button(settings_Payment_Pages.setting_crossIcon, "setting_crossIcon").click();
		logger.info("click on setting_crossIcon Button");
		waitAndLog(2);

	}
}
