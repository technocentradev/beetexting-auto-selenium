package com.beetext.qa.Workflows;

import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.ChangeaPasswordWFCM;
import com.beetext.qa.pages.ChangePassword;
import com.beetext.qa.util.TestUtil;

public class CurrentPasswordWF extends ChangeaPasswordWFCM {

	ChangePassword password = new ChangePassword();

	public void changepassword_success_case() {
		
		change_Password_Page();
		enterCurrent_pass();
		enterNewPass();
		cofirm_new_Pass();
		save_Button();
		waits(2);
		if (password.toaster_message.getText().equalsIgnoreCase("Password Changed successfully.")) {
			Assert.assertEquals(password.toaster_message.getText(), "Password Changed successfully.",
					"Password Changed successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfullySuccessfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.toaster_message.getText(), "Password Changed successfully.",
					"Password Changed successfully.");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfully is failed");// ScreenShot capture
		}
		enterCurrent_password();
		enterNewPassword();
		cofirm_new_Password();
		save_Button();
		
	}

	public void confirmpassword_invalid_case() {
		
		change_Password_Page();
		enterCurrent_password();
		password.new_password.sendKeys("tech12346");
		password.confirm_new_password.sendKeys("tech12346");
		save_Button();
		waits(2);
		if (password.current_password_wrong_toaster_message.getText().equalsIgnoreCase(
				"The password you entered is not your current password. Try again or logout to reset your password.")) {
			Assert.assertEquals(password.current_password_wrong_toaster_message.getText(),
					"The password you entered is not your current password. Try again or logout to reset your password.",
					"The password you entered is not your current password. Try again or logout to reset your password.");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfullySuccessfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.current_password_wrong_toaster_message.getText(),
					"The password you entered is not your current password. Try again or logout to reset your password.",
					"The password you entered is not your current password. Try again or logout to reset your password.");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfully is failed");// ScreenShot capture
		}

	}

	public void currentpassword_required_validation_case() {
		waits(1);
		threedotLink();
		profileLink();
		change_Password_Page();

		password.current_password.clear();
		password.current_password.sendKeys("");
		password.new_password.click();

		if (password.current_password_required.getText().equalsIgnoreCase("Current password is required.")) {
			Assert.assertEquals(password.current_password_required.getText(), "Current password is required.",
					"Current password is required.");
			TestUtil.takeScreenshotAtEndOfTest("Current password is required validation");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.current_password_required.getText(), "Current password is required.",
					"Current password is required.");
			TestUtil.takeScreenshotAtEndOfTest("'Current password is required. is failed");// ScreenShot capture
		}
		password.notification_link.click();

	}

	public void currentpassword_required_min_8_char_validation_case() {

		change_Password_Page();
		password.current_password.clear();
		password.current_password.sendKeys("1234");

		if (password.current_password_min_8_char.getText()
				.equalsIgnoreCase("Current password must be at least 8 characters.")) {
			Assert.assertEquals(password.current_password_min_8_char.getText(),
					"Current password must be at least 8 characters.",
					"Current password must be at least 8 characters.");
			TestUtil.takeScreenshotAtEndOfTest("Current password is min 8 char required validation");// ScreenShot
																										// capture

		} else {
			Assert.assertNotEquals(password.current_password_min_8_char.getText(),
					"Current password must be at least 8 characters.",
					"Current password must be at least 8 characters.");
			TestUtil.takeScreenshotAtEndOfTest("'Current password is min 8 char required. is failed");// ScreenShot
																										// capture
		}
		password.notification_link.click();
	}

	public void newpassword_required_validation_case() {
		change_Password_Page();
		password.new_password.clear();
		password.new_password.sendKeys("");
		password.confirm_new_password.click();

		if (password.new_password_required.getText().equalsIgnoreCase("New password is required.")) {
			Assert.assertEquals(password.new_password_required.getText(), "New password is required.",
					"New password is required.");
			TestUtil.takeScreenshotAtEndOfTest("New password is required.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.new_password_required.getText(), "New password is required.",
					"New password is required.");
			TestUtil.takeScreenshotAtEndOfTest("New password is required.");// ScreenShot capture
		}
		password.notification_link.click();

	}

	public void newpassword_required_min_8_char_validation_case() {

		change_Password_Page();
		password.new_password.clear();
		password.new_password.sendKeys("1234");

		if (password.new_password_min_8_char.getText()
				.equalsIgnoreCase("New password must be at least 8 characters.")) {
			Assert.assertEquals(password.new_password_min_8_char.getText(),
					"New password must be at least 8 characters.", "New password must be at least 8 characters.");
			TestUtil.takeScreenshotAtEndOfTest("New password must be at least 8 characters.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.new_password_min_8_char.getText(),
					"New password must be at least 8 characters.", "New password must be at least 8 characters.");
			TestUtil.takeScreenshotAtEndOfTest("New password must be at least 8 characters. is failed");// ScreenShot
																										// capture
		}
		password.notification_link.click();

	}

	public void confirm_newpassword_required_min_8_char_validation_case() {

		change_Password_Page();
		password.confirm_new_password.clear();
		password.confirm_new_password.sendKeys("123");

		if (password.confirm_new_password_min_8_char.getText()
				.equalsIgnoreCase("Re-Type password must be at least 8 characters.")) {
			Assert.assertEquals(password.confirm_new_password_min_8_char.getText(),
					"Re-Type password must be at least 8 characters.",
					"Re-Type password must be at least 8 characters.");
			TestUtil.takeScreenshotAtEndOfTest("New password must be at least 8 characters.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.confirm_new_password_min_8_char.getText(),
					"Re-Type password must be at least 8 characters.",
					"Re-Type password must be at least 8 characters.");
			TestUtil.takeScreenshotAtEndOfTest("New password must be at least 8 characters. is failed");// ScreenShot
																										// capture
		}
		password.notification_link.click();
	}

	public void confirm_newpassword_required_validation_case() {

		change_Password_Page();
		password.confirm_new_password.clear();
		password.confirm_new_password.sendKeys("");

		password.new_password.click();

		if (password.confirm_new_password_required.getText().equalsIgnoreCase("Re-Type password is required.")) {
			Assert.assertEquals(password.confirm_new_password_required.getText(), "Re-Type password is required.",
					"'Re-Type password is required.");
			TestUtil.takeScreenshotAtEndOfTest("Re-Type password is required.");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.confirm_new_password_required.getText(), "Re-Type password is required.",
					"Re-Type password is required.");
			TestUtil.takeScreenshotAtEndOfTest("Re-Type password is required. is failed");// ScreenShot capture
		}
		password.notification_link.click();

	}

	public void newpassword_confirmpassword_mismatch_case() {
		change_Password_Page();
		enterCurrent_password();
		enterNewPass();
		cofirm_new_Password();
		save_Button();
		waits(1);
		if (password.confirm_password_wrong_toaster_message.getText()
				.equalsIgnoreCase("Your passwords do not match. Please retype them an")) {
			Assert.assertEquals(password.confirm_password_wrong_toaster_message.getText(),
					"Your passwords do not match. Please retype them an",
					"Your passwords do not match. Please retype them an");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfullySuccessfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.confirm_password_wrong_toaster_message.getText(),
					"Your passwords do not match. Please retype them an",
					"Your passwords do not match. Please retype them an");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfully is failed");// ScreenShot capture
		}

	}
	
	public void clickon_cancel_buttoncase() throws Exception {
		change_Password_Page();
		password.cancel_Button.click();
		
		waits(1);
		if (verifyElementIsEnabled(password.threedots)) {
			Assert.assertEquals(true, verifyElementIsEnabled(password.threedots));
			button(password.threedots, "button").isDisplayed();
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case passed");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(password.threedots));
			
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case failed");// ScreenShot capture
		}
		

	}
	
	public void clickon_close_prfilepopup_icon_case() throws Exception {
		threedotLink();
		profileLink();
		change_Password_Page();
		password.close_button.click();
		
		waits(1);
		if (verifyElementIsEnabled(password.threedots)) {
			Assert.assertEquals(true, verifyElementIsEnabled(password.threedots));
			button(password.threedots, "button").isDisplayed();
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case passed");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(password.threedots));
			
			TestUtil.takeScreenshotAtEndOfTest("cancel button click case failed");// ScreenShot capture
		}
		

	}
	public void newpassword_matchingwith_currentpassword_case() {
		threedotLink();
		profileLink();
		change_Password_Page();
		enterCurrent_password();
		enterCurrent_pass();
		enterNewPassword();
		cofirm_new_Password();
		save_Button();
		waits(1);
		if (password.wrong_toaster_message.getText()
				.equalsIgnoreCase("New password is matching with current password, please change")) {
			Assert.assertEquals(password.wrong_toaster_message.getText(),
					"New password is matching with current password, please change",
					"New password is matching with current password, please change");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfullySuccessfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(password.wrong_toaster_message.getText(),
					"New password is matching with current password, please change",
					"New password is matching with current password, please change");
			TestUtil.takeScreenshotAtEndOfTest("Password Changed successfully is failed");// ScreenShot capture
		}

	}

}
