package com.beetext.qa.Workflows.broadcast;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_BM_Gif_Emoji_WF extends Broadcast {
	BroadCast_Pages BM_Pages = new BroadCast_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void Multiple_GifCases() throws Exception {

		logger.info(
				" Combination of GifDisplay-Gif Popup, Select1Gif, SearchGif with Some Text and Select Gif, Search Gif with Invalid Keyword and text CloseMark, Gif viewsTerms ,outsideclick gifClose");

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);
		if (verifyElementIsEnabled(BM_Pages.tools_BM_Gif1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Gif1));
			textbox(BM_Pages.tools_BM_Gif1).isEnabled();
			logger.info("Gif 1 Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Gif1));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Gif2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Gif2));
			textbox(BM_Pages.tools_BM_Gif2).isEnabled();
			logger.info("Gif 2 Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Gif2));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Gif3)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Gif3));
			textbox(BM_Pages.tools_BM_Gif3).isEnabled();
			logger.info("Gif 3 Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Gif3));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		logger.info("Gif's are Dispayed");

		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1 icon");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Gif_display)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Gif_display));
			textbox(BM_Pages.tools_BM_Gif_display).isEnabled();
			logger.info("Gif Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Gif_display));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		logger.info("Gif Displayed");

		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		textbox(BM_Pages.gif_search).sendKeys("goodmorning");
		logger.info("Enter the text to search gif");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1 icon");
		waitAndLog(2);

		logger.info("Gif Selected");

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		textbox(BM_Pages.gif_search).sendKeys("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		logger.info("Enter the text to search gif");
		waitAndLog(2);

		BM_Pages.tools_BM_Gif_ErrorMsg.getText();
		logger.info(BM_Pages.tools_BM_Gif_ErrorMsg.getText());
		if (BM_Pages.tools_BM_Gif_ErrorMsg.getText().equalsIgnoreCase("We didn't find any matches.")) {
			Assert.assertEquals(BM_Pages.tools_BM_Gif_ErrorMsg.getText(), "We didn't find any matches.",
					"tools_BM_Gif_ErrorMsg is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_Gif_ErrorMsgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_Gif_ErrorMsg.getText(), "We didn't find any matches.",
					"tools_BM_Gif_ErrorMsg is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_Gif_ErrorMsgTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_Gif_text_close, "button").click();
		logger.info("click on tools_BM_Gif_text_close");
		waitAndLog(2);
		logger.info("wait for 32sec");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Gif1)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Gif1));
			textbox(BM_Pages.tools_BM_Gif1).isEnabled();
			logger.info("Gif 1 Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Gif1));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Gif2)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Gif2));
			textbox(BM_Pages.tools_BM_Gif2).isEnabled();
			logger.info("Gif 2 Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Gif2));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Gif3)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Gif3));
			textbox(BM_Pages.tools_BM_Gif3).isEnabled();
			logger.info("Gif 3 Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Gif3));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);

		button(BM_Pages.giphy_viewsTerms, "button").click();
		logger.info("click on giphy_viewsTerms");
		waitAndLog(2);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));
		driver.close();

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1 icon");
		waitAndLog(2);

		button(BM_Pages.giphy_outside, "button").click();
		logger.info("click on giphy_outside");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_GifIcon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_GifIcon));
			textbox(BM_Pages.tools_BM_GifIcon).isEnabled();
			logger.info("Giphy Closes");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_GifIcon));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_Gif_SlectGif_SendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMgif();
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_SendBM();

		Auto1_Created_Toaster();
	}

	public void tools_BM_Gif_SlectGif_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMgif();

		BMCreate_Schedule();
		BM_Terms_Conditions();
		BM_ScheduleBroadCast();

		Auto1_Created_Toaster();

	}

	public void tools_BM_Gif_SlectGif_BM_SaveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMgif();

		BM_Terms_Conditions();

		button(BM_Pages.tools_BM_Past_Save_Draft, "button").click();
		logger.info("Click on Draft button in BroadCast");
		wait(3);

		Draft_Toaster();

	}

	public void tools_BM_Gif_SelectGif_upcoming_ChangeTitle_add_Gif() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming BroadCast");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");
		waitAndLog(2);

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		BM_Edit_Schedule_with_2min();

		BM_ScheduleBroadCast();
		Auto1_Updated_Toaster();

	}

	public void tools_BM_Gif_SelectGif_Past_Reuse_ChangeTitle_add_Gif() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

		Auto1_Created_Toaster();
	}

	public void tools_BM_Gif_SelectGif_Draft_ChangeTitle_add_Gif() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("Click on Draft button in BroadCast");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on tools_BM_Draft_manage button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");

		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

		BM_Edit_Schedule_with_2min();

		BM_ScheduleBroadCast();
		Auto1_Updated_Toaster();
	}

	public void tools_BM_Emoji_SelectEmoji_SendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMEmoji();

		BM_Terms_Conditions();
		BM_SendBM();

		Auto1_Created_Toaster();
	}

	public void tools_BM_Emoji_SelectEmoji_BM_SaveDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMEmoji();

		BM_Terms_Conditions();

		button(BM_Pages.tools_BM_Past_Save_Draft, "button").click();
		logger.info("Click on Draft button in BroadCast");

		Draft_Toaster();
	}

	public void tools_BM_Emoji_SelectEmoji_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMEmoji();

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji1, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji2, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji3, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji4, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji5, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji6, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji7, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji8, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji9, "button").click();
		logger.info("click on emoji");

		BM_Edit_Schedule_with_2min();

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

		Auto1_Created_Toaster();

	}

	public void tools_BM_Emoji_upcoming_SelectEmoji_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMEmoji();

		BM_Edit_Schedule_with_2min();

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming BroadCast");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji, "button").click();
		logger.info("click on emoji");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		BM_Edit_Schedule_with_2min();

		BM_ScheduleBroadCast();

		Auto1_Created_Toaster();

	}

	public void tools_BM_Emoji_SelectEmoji_Past_Reuse_ChangeTitle_add_emoji() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji1, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji2, "button").click();
		logger.info("click on emoji");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow ");
		waitAndLog(2);

		BM_SendBM();

		Auto1_Created_Toaster();

	}

	public void tools_BM_emoji_SelectEmoji_Draft_ChangeTitle_add_emoji() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		BMEmoji();

		button(BM_Pages.tools_BM_Past_Save_Draft, "button").click();
		logger.info("Click on Draft button in BroadCast");
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("Click on Draft button in BroadCast");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on tools_BM_Draft_manage button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji1, "button").click();
		logger.info("click on emoji");

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");

		button(BM_Pages.emoji2, "button").click();
		logger.info("click on emoji");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		BM_SendBM();

		Auto1_Updated_Toaster();
	}

}
