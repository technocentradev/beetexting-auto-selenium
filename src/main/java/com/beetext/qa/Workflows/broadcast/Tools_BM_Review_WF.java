package com.beetext.qa.Workflows.broadcast;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_BM_Review_WF extends Broadcast {

	String url, username1, username2, password;
	BroadCast_Pages BM_Pages = new BroadCast_Pages();
	String randomtitle = RandomStringUtils.randomAlphabetic(5);
	public static String currentDir = System.getProperty("user.dir");

	public void tools_BM_Click_On_Review_ReviewMsg_updated() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_ReviewIcon, "button").click();
		logger.info("Click on review icon");
		waitAndLog(2);

		String Reviewtext = BM_Pages.tools_BM_message.getText();
		logger.info("Review Text --->  " + Reviewtext);

		logger.info("Review Message is Updated");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_attachment)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_attachment));
			button(BM_Pages.tools_BM_create_attachment, "Attachment Button").isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_attachment));
			logger.info("Attachment is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_GifIcon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_GifIcon));
			button(BM_Pages.tools_BM_GifIcon, "Gif Icon Button").isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_GifIcon));
			logger.info("Gif Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_EmojiIcon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_EmojiIcon));
			button(BM_Pages.tools_BM_create_attachment, "Emoji Icon Button").isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_EmojiIcon));
			logger.info("Emoji Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_ReviewIcon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_ReviewIcon));
			button(BM_Pages.tools_BM_ReviewIcon, "Review Icon Button").isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_ReviewIcon));
			logger.info("Review Icon is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		BM_Terms_Conditions();
		BM_SendBM();
	}

	public void tools_BM_upcoming_Click_On_Review_ReviewMsg_updated() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_Recurring();

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_ReviewIcon, "button").click();
		logger.info("Click on review icon");

		BM_SendBM();
	}

	public void tools_BM_upcoming_already_msg_ReviewMsg_icons_disabled() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_ReviewIcon, "button").click();
		logger.info("Click on review icon");

		BMCreate_Schedule();

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		BM_Terms_Conditions();
		BM_SendBM();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		logger.info("Already Review Content Message in Message box ");

		BM_SendBM();

	}

	public void tools_BM_past_Click_On_Review_ReviewMsg_updated_icons_Disabled() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_BM();
		waitAndLog(2);
		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_ReviewIcon, "button").click();
		logger.info("Click on review icon");
		waitAndLog(2);

		String Reviewtext = BM_Pages.tools_BM_message.getText();
		logger.info("Review Text --->  " + Reviewtext);

		logger.info("Review Message is Updated");

		BM_SendBM();
		waitAndLog(2);
	}

	public void tools_BM_past_already_msg_ReviewMsg_icons_disabled() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_ReviewIcon, "button").click();
		logger.info("Click on review icon");
		waitAndLog(2);

		BM_Terms_Conditions();
		BM_SendBM();
		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		logger.info("Already Review Content Message in Message box ");

		BM_SendBM();

	}

	public void tools_BM_SaveAsDraft_already_msg_ReviewMsg_icons_disabled() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("a");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_title).sendKeys("Review - Broadcast Message");
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_ReviewIcon, "button").click();
		logger.info("Click on review icon");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Past_Save_Draft, "button").click();
		logger.info("click on past Save Draft button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Draft_manage, "button").click();
		logger.info("click on Draft manage Bm ");

		logger.info("Already Review Content Message in Message box ");

		BM_SendBM();
		waitAndLog(2);

	}

}
