package com.beetext.qa.Workflows.broadcast;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.util.TestUtil;

public class Tools_BM_Draft_WF extends Broadcast {

	BroadCast_Pages BM_Pages = new BroadCast_Pages();
	String randomtitle = RandomStringUtils.randomAlphabetic(5);
	public static String currentDir = System.getProperty("user.dir");

	public void tools_BM_page_Draft_display_list() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		implicitwaitAndLog(2);

		String title = BM_Pages.tools_BM_upcoming_list_title.getText();
		String DateandTime = BM_Pages.tools_BM_Draft_noDateSelected.getText();
		String manage = BM_Pages.tools_BM_Draft_manage.getText();
		String delete = BM_Pages.tools_BM_Draft_Delete.getText();

		logger.info(title + "" + DateandTime + "" + manage + "" + delete);

		String title1 = BM_Pages.tools_BM_upcoming_list_title1.getText();
		String DateandTime1 = BM_Pages.tools_BM_Draft_noDateSelected1.getText();
		String manage1 = BM_Pages.tools_BM_Draft_manage1.getText();
		String delete1 = BM_Pages.tools_BM_Draft_Delete1.getText();

		logger.info(title1 + "" + DateandTime1 + "" + manage1 + "" + delete1);

		String title2 = BM_Pages.tools_BM_upcoming_list_title2.getText();
		String DateandTime2 = BM_Pages.tools_BM_Draft_noDateSelected2.getText();
		String manage2 = BM_Pages.tools_BM_Draft_manage2.getText();
		String delete2 = BM_Pages.tools_BM_Draft_Delete2.getText();

		logger.info(title2 + "" + DateandTime2 + "" + manage2 + "" + delete2);

		String title3 = BM_Pages.tools_BM_upcoming_list_title3.getText();
		String DateandTime3 = BM_Pages.tools_BM_Draft_noDateSelected3.getText();
		String manage3 = BM_Pages.tools_BM_Draft_manage3.getText();
		String delete3 = BM_Pages.tools_BM_Draft_Delete3.getText();

		logger.info(title3 + "" + DateandTime3 + "" + manage3 + "" + delete3);

	}

	public void tools_BM_page_Draft_ChangeInBM_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Draft_manage, "button").click();
		logger.info("click on Draft manage Bm ");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_Draft_Save_Draft, "click on button").click();
		logger.info("Clear the Save As Draft");

		Draft_Toaster();
	}

	public void tools_BM_page_Draft_deleteBM_saveAsDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Draft_Delete, "button").click();
		logger.info("click on Draft - Delete ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Draft_Delete_discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_Delete_discard));
			textbox(BM_Pages.tools_BM_Draft_Delete_discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_Delete_discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Draft_Delete_Cancel)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_Delete_Cancel));
			textbox(BM_Pages.tools_BM_Draft_Delete_Cancel).isEnabled();
			logger.info("PopUp Opens with Cancel");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_Delete_Cancel));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_Draft_Delete_discard, "button").click();
		logger.info("click on Discard button");

	}

	public void tools_BM_page_Draft_deleteBM_cancel() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_Draft_Delete, "button").click();
		logger.info("click on Draft - Delete ");

		button(BM_Pages.tools_BM_Draft_Delete_Cancel, "button").click();
		logger.info("click on Cancel button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("BroadCast Successfully cancelled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Draft_manage_removetag_sendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Remove tag for BM button");

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow ");
		waitAndLog(2);

		BM_SendBM();

		Draft_Toaster();

	}

	public void tools_BM_page_Draft_manage_changeTitle_removeMSG_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule BroadCast");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");

		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);
		BM_ScheduleBroadCast();

		Auto1_Updated_Toaster();

	}

	public void tools_BM_page_Draft_manage_changeTitle_click_NavigatonLink_saveBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();

		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys("randomtitle");
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save button");

		Draft_Toaster();

	}

	public void tools_BM_page_Draft_manage_changeTitle_click_NavigatonLink_DiscardBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();

		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);
		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Draft_manage_DeleteBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft_manage_delete, "button").click();
		logger.info("click on Draft Manage Delete BroadCast");
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft_manage_delete, "button").click();
		logger.info("click on Draft Manage Delete BroadCast");

	}

	public void tools_BM_page_Draft_manage_fillAllDetails_sendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow ");
		implicitwaitAndLog(2);
		BM_SendBM();

		Auto1_Updated_Toaster();

	}

	public void tools_BM_page_Draft_manage_fillAllDetails_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		BM_ScheduleBroadCast();

	}

	public void tools_BM_page_Draft_manage_addAttachment_ClickSM_saveBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save button");

		Draft_Toaster();
		draft_Delete();

	}

	public void tools_BM_page_Draft_manage_addAttachment_ClickSM_DiscardBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			textbox(BM_Pages.tools_BM_create_create_SM).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Draft_manage_addAttachment_ClickTemplates_saveBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save button");

		Draft_Toaster();
	}

	public void tools_BM_page_Draft_manage_addAttachment_ClickTemplates_DiscardBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			textbox(BM_Pages.tools_BM_create_create_New_Template).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Draft_manage_Select_recurringIcon_ClickTemplates_saveBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save button");
		Draft_Toaster();

		draft_Delete();

	}

	public void tools_BM_page_Draft_manage_Select_recurringIcon_ClickTemplates_DiscardBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			textbox(BM_Pages.tools_BM_create_create_New_Template).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Draft_manage_Select_recurringIcon_ClickSM_saveBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save button");

		Draft_Toaster();
		draft_Delete();

	}

	public void tools_BM_page_Draft_manage_Select_recurringIcon_ClickSM_DiscardBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			textbox(BM_Pages.tools_BM_create_create_SM).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Draft_manage_Scheduletime_updated_withDateTime() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Draft_BM_Tag_Msg();
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime));
			textbox(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime).isEnabled();
			logger.info("Schedule Date and time is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Draft_manage_Scheduletime_updated_No_dateSelected() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");

		button(BM_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Clear the Save As Draft");
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send button");
		waitAndLog(2);

		BM_SendBM();

		String Date_Time = BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime.getText();
		logger.info("Date and Time ---> " + Date_Time);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime));
			textbox(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime).isEnabled();
			logger.info("No Date Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Draft_manage_Schedule_DateAndTime));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

}
