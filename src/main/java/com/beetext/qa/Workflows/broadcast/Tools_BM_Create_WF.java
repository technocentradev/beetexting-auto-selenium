package com.beetext.qa.Workflows.broadcast;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_BM_Create_WF extends Broadcast {
	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	BroadCast_Pages BM_Pages = new BroadCast_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void Comnbination_Of_Cases() throws Exception {

		logger.info(
				"Tools Close Popup using cross Bar, Tools available after Message, opens popup, display allmodules");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.broadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.broadCast));
			textbox(BM_Pages.broadCast).isEnabled();
			logger.info("By Defaultly Broadcast is Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.broadCast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_crossBtn, "Click on cross Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.mesage_tab)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.mesage_tab));
			textbox(BM_Pages.mesage_tab).isEnabled();
			logger.info("Message Tab");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {

			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.mesage_tab));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("Tools Button is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Tools Button is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		logger.info("Tools Popup Closed.");

		button(BM_Pages.tools_bttn, "Click on tools Button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.broadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.broadCast));
			textbox(BM_Pages.broadCast).isEnabled();
			logger.info("On Popup BroadCast is opened");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.broadCast));
			logger.info("BroadCast PopUp not Opened");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_SM));
			textbox(BM_Pages.tools_BM_create_SM).isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_SM));
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_Templates)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_Templates));
			textbox(BM_Pages.tools_Templates).isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_Templates));
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.toolsTags)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.toolsTags));
			textbox(BM_Pages.toolsTags).isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.toolsTags));
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_Automations)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_Automations));
			textbox(BM_Pages.tools_Automations).isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_Automations));
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_Advocate)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_Advocate));
			textbox(BM_Pages.tools_Advocate).isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_Advocate));
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		if (verifyElementIsEnabled(BM_Pages.Tools_Reviews)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.Tools_Reviews));
			textbox(BM_Pages.Tools_Reviews).isEnabled();
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.Tools_Reviews));
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_upcoming_past_draft_enabled_Default_Messages() throws Exception {

		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("AgentAutomation@yopmail.com");
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.broadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.broadCast));
			textbox(BM_Pages.broadCast).isEnabled();
			logger.info("By Defaultly Broadcast is Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.broadCast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_upcoming)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming));
			textbox(BM_Pages.tools_BM_upcoming).isEnabled();
			logger.info("upcoming is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_upcoming, "click on upcoming").click();

		if (BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText()
				.equalsIgnoreCase("All your upcoming broadcasts will appear here.")) {
			Assert.assertEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is matching");
			logger.info(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_past)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_past));
			textbox(BM_Pages.tools_BM_past).isEnabled();
			logger.info("Past is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_past));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_past, "Click on Past").click();

		if (BM_Pages.tools_BM_No_past_BM_msgs.getText().equalsIgnoreCase("You have no past broadcasts.")) {
			Assert.assertEquals(BM_Pages.tools_BM_No_past_BM_msgs.getText(), "You have no past broadcasts.",
					"Message is matching");
			logger.info(BM_Pages.tools_BM_No_past_BM_msgs.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_past_BM_msgsTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_No_past_BM_msgs.getText(), "You have no past broadcasts.",
					"Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_past_BM_msgsTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_draft));
			textbox(BM_Pages.tools_BM_draft).isEnabled();
			logger.info("Draft is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on draft button in BroadCast ");

		if (BM_Pages.tools_BM_NO_draft_msg.getText().equalsIgnoreCase("You can save your draft broadcasts.")) {
			Assert.assertEquals(BM_Pages.tools_BM_NO_draft_msg.getText(), "You can save your draft broadcasts.",
					"Message is matching");
			logger.info(BM_Pages.tools_BM_NO_draft_msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_NO_draft_msgsTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_NO_draft_msg.getText(), "You can save your draft broadcasts.",
					"Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_NO_draft_msgTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("auto1@yopmail.com");
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		waitAndLog(2);
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		waitAndLog(2);

	}

	public void tools_BM_click_on_createBM_Select_Dept_View_Reciepents_relatedCases() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		List<WebElement> allOptions = driver
				.findElements(By.xpath("//select[@class='form-control ng-pristine ng-valid ng-touched']"));
		logger.info(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Auto1")) {

				allOptions.get(i).click();
				break;
			}

		}

		logger.info("Health Screen Department is not shown");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients));
			textbox(BM_Pages.tools_BM_viewrecipients).isEnabled();
			logger.info("View Recipient Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients));
			logger.info("Tools BM View Recipients is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_message)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_message));
			textbox(BM_Pages.tools_BM_message).isEnabled();
			logger.info("Message enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_message));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_title)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_title));
			textbox(BM_Pages.tools_BM_title).isEnabled();
			logger.info("Title enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_title));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_input_To_searchTags)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_input_To_searchTags));
			textbox(BM_Pages.tools_BM_input_To_searchTags).isEnabled();
			logger.info("Input search tags is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_input_To_searchTags));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		String searchtag = BM_Pages.tools_BM_create_SearchTag1.getText();
		logger.info("Search Tag name--->" + searchtag);

		String searchtag1 = BM_Pages.tools_BM_create_SearchTag2.getText();
		logger.info("Search Tag name--->" + searchtag1);

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String recipientCount = BM_Pages.tools_BM_recipentCount.getText();
		logger.info("Recipient Count--->" + recipientCount);
		waitAndLog(2);
		button(BM_Pages.tools_BM_viewrecipients, "button").click();
		logger.info("click on View Recipients");
		waitAndLog(2);

		String recipients = BM_Pages.tools_BM_create_recipients_list.getText();
		logger.info("Recipients List---->  " + recipients);

		button(BM_Pages.tools_BM_create_viewrecipients_back, "button").click();
		logger.info("click on Back");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Cross Mark");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients));
			textbox(BM_Pages.tools_BM_viewrecipients).isEnabled();
			logger.info("Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients));
			logger.info("Tools BM View Recipients is Disabled");
			logger.info("No Tag is Selected - Recipient Count is not shown");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_create_Select_2Tags_moreTarget_and_lessTarget() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("ab");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		button(BM_Pages.tools_BM_create_moretarget, "button").click();
		logger.info("click on More Target");

		String recipientCount_more_target = BM_Pages.tools_BM_recipentCount.getText();
		logger.info("Recipient Count--->" + recipientCount_more_target);

		button(BM_Pages.tools_BM_create_lesstarget, "button").click();
		logger.info("click on More Target");

		String recipientCount_lessCount = BM_Pages.tools_BM_recipentCount.getText();
		logger.info("Recipient Count--->" + recipientCount_lessCount);

	}

	public void tools_BM_page_Finding_Errors_for_Title_Message() throws Exception {

		logger.info("title_max50Char, 3Char errormsg,message 1000char optout errormsg, 700char optout noerrormsg");
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(50);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		logger.info("Title name should restrict to max 50 Char ");

		textbox(BM_Pages.tools_BM_title).clear();
		textbox(BM_Pages.tools_BM_title).sendKeys("hn");
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

		BM_Pages.tools_BM_create_title3Char_errormsg.getText();
		logger.info(BM_Pages.tools_BM_create_title3Char_errormsg.getText());
		if (BM_Pages.tools_BM_create_title3Char_errormsg.getText()
				.equalsIgnoreCase("Title must be at least 3 characters.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_title3Char_errormsg.getText(),
					"Title must be at least 3 characters.", "tools_BM_create_title3Char_errormsg is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_title3Char_errormsgTestCase");// ScreenShot
																								// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_title3Char_errormsg.getText(),
					"Title must be at least 3 characters.", "tools_BM_create_title3Char_errormsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_title3Char_errormsgTestCase");// ScreenShot
																								// capture
		}

		waitAndLog(2);
		String message1 = RandomStringUtils.randomAlphabetic(700);
		textbox(BM_Pages.tools_BM_message).clear();

		checkbox(BM_Pages.tools_BM_create_msg_optout_checkbox, "checkbox").click();
		logger.info("click on optout checkbox");

		textbox(BM_Pages.tools_BM_message).sendKeys(message1);
		logger.info(" Enter your messeage");

		checkbox(BM_Pages.tools_BM_create_msg_optout_checkbox, "checkbox").click();
		logger.info("click on optout checkbox");

		logger.info("No Error Msg after clicking Optout because 700 Char message only");

	}

	public void tools_BM_page_add_Attachment() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		logger.info("Attachment File added Successfully");

	}

	public void tools_BM_page_add_10Attachment() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.jpeg");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\file.gif");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmp4.mp4");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\dropmov.mov");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SampleExcel.xlsx");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.txt");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\welcome.doc");

		waitAndLog(5);

		logger.info("10 Attachments added Successfully");

	}

	public void tools_BM_page_termsandCond_errormsg() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

		if (BM_Pages.tools_BM_create_termsandCond_errormsg.getText()
				.equalsIgnoreCase("Please read the terms of service and verify that you agree.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_termsandCond_errormsg.getText(),
					"Please read the terms of service and verify that you agree.",
					"tools_BM_create_termsandCond_errormsg is matching");
			logger.info(BM_Pages.tools_BM_create_termsandCond_errormsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_termsandCond_errormsgTestCase");// ScreenShot
																								// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_termsandCond_errormsg.getText(),
					"Please read the terms of service and verify that you agree.",
					"tools_BM_create_termsandCond_errormsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_termsandCond_errormsgTestCase");// ScreenShot
																								// capture
		}

	}

	public void tools_BM_page_fill_allDetails_SendNow() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		create_BM();

	}

	public void tools_BM_page_fill_allDetails_ScheduleLater() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

	}

	public void tools_BM_page_DateAndTime_errorMsg() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");

		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

		if (BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText(),
					"Please select a date and time in the future.",
					"tools_BM_create_select_Future_DateandTime_errorMsg is matching");
			logger.info(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_select_Future_DateandTime_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText(),
					"Please select a date and time in the future.",
					"tools_BM_create_select_Future_DateandTime_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_select_Future_DateandTime_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

	public void tools_BM_page_recurring() throws Exception {

		logger.info(" Tools BM page recurring repeat, Display_All recurring, "
				+ "Select all recurring and show Dates, Select less than Current date");
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions = driver.findElements(By.xpath("(//select[1]//option)[1]"));
		logger.info(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Daily at this time")) {

				allOptions.get(i).click();
				break;

			}
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_daily_atThis_time_recurring)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_daily_atThis_time_recurring));
			textbox(BM_Pages.tools_BM_create_daily_atThis_time_recurring).isEnabled();
			logger.info("Daily at this time is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_daily_atThis_time_recurring));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[1]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Daily at this time")) {

				allOptions1.get(i).click();
				break;

			}
		}

		logger.info("Daily at this time");

		String DateandTime = BM_Pages.tools_BM_create_recurring_appDateTime.getAttribute("value");
		logger.info(" Date and Time --->" + DateandTime);

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions2.get(i).click();
				break;

			}
		}

		logger.info("Weekly on (Selected day)");
		String DateandTime_Weekly = BM_Pages.tools_BM_create_recurring_appDateTime.getAttribute("value");
		logger.info(" Date and Time --->" + DateandTime_Weekly);

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions3.get(i).click();
				break;

			}
		}

		logger.info("Monthly on the (Date)");

		String DateandTime_Monthly = BM_Pages.tools_BM_create_recurring_appDateTime.getAttribute("value");
		logger.info(" Date and Time --->" + DateandTime_Monthly);

		List<WebElement> allOptions4 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions4.size());

		for (int i = 0; i <= allOptions4.size() - 1; i++) {

			if (allOptions4.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions4.get(i).click();
				break;

			}
		}

		logger.info("Yearly on (Date) (Month)");

		String DateandTime_Yearly = BM_Pages.tools_BM_create_recurring_appDateTime.getAttribute("value");
		logger.info(" Date and Time --->" + DateandTime_Yearly);

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_recurring_appDateTime, "button").click();
		logger.info("click on recurring App Date and Time button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime_DecreaseMonth, "button").click();
		logger.info("click on recurring App Date and Time Decrease Month button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime_SelectDate, "button").click();
		logger.info("click on recurring Select App Date and Time button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

		BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText();
		logger.info(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText());
		if (BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

	public void tools_BM_page_validDetails_BM_toaster() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_BM();

	}

	public void tools_BM_page_dont_giveMsg_BM_sent() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		button(BM_Pages.tools_BM_create_lesstarget, "button").click();
		logger.info("click on More Target");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		BM_SendBM();
	}

	public void tools_BM_page_Click_onSM_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_page_Click_onSM_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			textbox(BM_Pages.tools_BM_create_create_SM).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Click_on_Templates_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_page_Click_onTemplates_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			textbox(BM_Pages.tools_BM_create_create_New_Template).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Click_on_Tags_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

		Draft_Toaster();
		draft_Delete();

	}

	public void tools_BM_page_Click_onTags_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			textbox(BM_Pages.tools_BM_create_create_New_Tag).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Click_on_Automations_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

	}

	public void tools_BM_page_Click_onAutomations_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			textbox(BM_Pages.tools_BM_create_create_New_Automation).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Click_on_Advocate_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");
	}

	public void tools_BM_page_Click_onAdvocate_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);
		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			textbox(BM_Pages.tools_BM_create_create_Advocate_gotoReflection).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Click_on_Reviews_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

	}

	public void tools_BM_page_Click_onReviews_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			textbox(BM_Pages.tools_BM_create_create_ADDReview).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Click_on_BroadCastLink_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_broadCast_Link, "button").click();
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

	}

	public void tools_BM_page_Click_onBroadCastLink_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_broadCast_Link, "button").click();
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_dontFillDetails_Click_on_outside_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools Button").click();
		waitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_page_dont_fillDetailsClick_Outside_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools Button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_Fill_AllDetails_Click_on_outside_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		BM_Terms_Conditions();

		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");
		Draft_Toaster();
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools Button").click();
		waitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_page_Fill_AllDetails_Click_Outside_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		BM_Terms_Conditions();
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools Button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_PastTime_errorMsg() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
		implicitwaitAndLog(2);

		if (BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText(),
					"Please select a date and time in the future.",
					"tools_BM_create_select_Future_DateandTime_errorMsg is matching");
			logger.info(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_select_Future_DateandTime_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText(),
					"Please select a date and time in the future.",
					"tools_BM_create_select_Future_DateandTime_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_select_Future_DateandTime_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

	public void tools_BM_page_Fill_AllDetails_dontAgreeTermsCondClick_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

		Draft_Toaster();
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools button").click();
		draft_Delete();
	}

	public void tools_BM_page_Fill_AllDetails_dontAgreeTermsCondClick_Click_Outside_PopupOpens_Click_onDiscard()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "Click on tools").click();
		waitAndLog(2);

	}

	public void tools_BM_page_Fill_AllDetails_SaveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");

		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_page_add_only_Attachment_SaveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");
		Draft_Toaster();
		draft_Delete();

	}

	public void tools_BM_page_AddTag_attachmentClick_on_outside_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

		Draft_Toaster();
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools Bttn").click();
		draft_Delete();

	}

	public void tools_BM_page_AddTag_Attachment_Click_Outside_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "Click on tools button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_viewRecipients_Click_on_outside_PopupOpens_Click_onDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);
		button(BM_Pages.tools_BM_viewrecipients, "button").click();
		logger.info("click on View Recipients");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_viewRecipients_Click_Outside_PopupOpens_Click_onDiscard() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		button(BM_Pages.tools_BM_viewrecipients, "button").click();
		logger.info("click on View Recipients");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_FillAlDetails_selectRecurring_Click_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft on PopUp ");

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools Button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_FillAlDetails_selectRecurring_Click_Click_Outside_PopupOpens_Click_onDiscard()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_selectRecurring_Selectweekday_BM_shown_in_past() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		button(BM_Pages.tools_BM_create_recurring_su, "button").click();
		logger.info("click on recurring Sun button");

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

	}

	public void tools_BM_page_upcoming_listDetails() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming BroadCast");

		String Title = tools_BM_create_upcoming_list_Title.getText();
		logger.info("Title name " + Title);

		String AppandDate = tools_BM_create_upcoming_list_DateAndTime.getText();
		logger.info("Title name " + AppandDate);

	}

	public void tools_BM_page_selectRecurring_Select_weeklyOnSelectedDate_BM_shown_in_past() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

		Auto1_Created_Toaster();

	}

	public void tools_BM_page_weekly_upcoming_listDetails() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");

		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming BroadCast");

		String Title = tools_BM_create_upcoming_list_Title.getText();
		logger.info("Title name " + Title);

		String AppandDate = tools_BM_create_upcoming_list_DateAndTime.getText();
		logger.info("Title name " + AppandDate);

	}

	public void tools_BM_page_selectRecurring_Select_MonthlyOnSelectedDate_BM_shown_in_past() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

		Auto1_Created_Toaster();

	}

	public void tools_BM_page_monthly_upcoming_listDetails() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming BroadCast");
		waitAndLog(2);

		String Title = tools_BM_create_upcoming_list_Title.getText();
		logger.info("Title name " + Title);

		String AppandDate = tools_BM_create_upcoming_list_DateAndTime.getText();
		logger.info("Title name " + AppandDate);
	}

	public void tools_BM_page_selectRecurring_Select_yearlyOnSelectedDate_BM_shown_in_past() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");
	}

	public void tools_BM_page_yearly_upcoming_listDetails() throws Exception {

		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming BroadCast");
		implicitwaitAndLog(2);

		String Title = tools_BM_create_upcoming_list_Title.getText();
		logger.info("Title name " + Title);

		String AppandDate = tools_BM_create_upcoming_list_DateAndTime.getText();
		logger.info("Title name " + AppandDate);

	}

	public void tools_BM_page_Save_as_draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");

		Draft_Toaster();

		draft_Delete();
	}

	public void tools_BM_page_create_pastTime_errormsg() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();
		button(BM_Pages.tools_BM_create_time, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

		if (BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText()
				.equalsIgnoreCase("Please select a date and time in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText(),
					"Please select a date and time in the future.",
					"tools_BM_create_select_Future_DateandTime_errorMsg is matching");
			logger.info(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_select_Future_DateandTime_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_select_Future_DateandTime_errorMsg.getText(),
					"Please select a date and time in the future.",
					"tools_BM_create_select_Future_DateandTime_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_select_Future_DateandTime_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

}
