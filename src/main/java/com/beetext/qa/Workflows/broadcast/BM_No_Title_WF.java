package com.beetext.qa.Workflows.broadcast;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.BM_Optout_WFCM;
import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.util.TestUtil;;

public class BM_No_Title_WF extends BM_Optout_WFCM {

	String url, username1, username2, password;
	BroadCast_Pages BM_Pages = new BroadCast_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void createBM_with_NoTitle_Past_noTitle() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		BM_Terms_Conditions();
		waitAndLog(2);
		BM_SendBM();
		BM_Automations_Toater_Verification();
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");

		logger.info(BM_Pages.tools_BM_past_list_Title.getText());
		logger.info(BM_Pages.tools_BM_past_list_DateAndTime.getText());

	}

	public void createBM_ScheduleLater_with_NoTitle_Past_noTitle() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		BMCreate_Schedule();

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

		BM_Automations_Toater_Verification();
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");

		logger.info(BM_Pages.tools_BM_past_list_Title.getText());
		logger.info(BM_Pages.tools_BM_past_list_DateAndTime.getText());

	}

	public void past_View_Display_View_Page() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);
		button(BM_Pages.past_View1, "button").click();
		logger.info("click on past_View1 ");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients));
			textbox(BM_Pages.tools_BM_viewrecipients).isEnabled();
			logger.info("View Recipient Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_viewrecipients));
			logger.info("Tools BM View Recipients is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_message)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_message));
			textbox(BM_Pages.tools_BM_message).isEnabled();
			logger.info("Message enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_message));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_title)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_title));
			textbox(BM_Pages.tools_BM_title).isEnabled();
			logger.info("Title enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_title));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_input_To_searchTags)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_input_To_searchTags));
			textbox(BM_Pages.tools_BM_input_To_searchTags).isEnabled();
			logger.info("Input search tags is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_input_To_searchTags));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void past_Reuse_NoTitleBM_AddTitle_SendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on tools_BM_create_upcoming_Reuse ");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		BM_SendBM();
		BM_Automations_Toater_Verification();

	}

	public void past_Reuse_NoTitleBM_AddTitle_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on tools_BM_create_upcoming_Reuse ");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		driver.findElement(By.xpath("(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[1]")).click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		implicitwaitAndLog(2);

		BM_ScheduleBroadCast();

		BM_Automations_Toater_Verification();

	}

	public void create_DraftBM_with_NoTitle_Past_noTitle() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		BM_Terms_Conditions();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Click the Save As Draft");
		BM_Automations_Draft_Toaster_Verification();
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");

		logger.info(BM_Pages.tools_BM_past_list_Title.getText());
		logger.info(BM_Pages.tools_BM_past_list_DateAndTime.getText());

	}

	public void create_DraftBM_ScheduleLater_with_NoTitle_Past_noTitle() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		BMCreate_Schedule();

		BM_Terms_Conditions();

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Click the Save As Draft");
		BM_Automations_Draft_Toaster_Verification();
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");

		logger.info(BM_Pages.tools_BM_past_list_Title.getText());
		logger.info(BM_Pages.tools_BM_past_list_DateAndTime.getText());

	}

	public void createBM_with_Title() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		BM_Terms_Conditions();
		waitAndLog(2);
		BM_SendBM();
		BM_Automations_Toater_Verification();
	}

	public void past_Reuse_clearTitle_SendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on tools_BM_create_upcoming_Reuse ");

		BM_Pages.tools_BM_title.clear();
		logger.info(" Clear the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		BM_SendBM();
		BM_Automations_Toater_Verification();
		waitAndLog(2);

	}

	public void createBM_with_Title_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		BM_Terms_Conditions();
		waitAndLog(2);
		BM_ScheduleBroadCast();
		BM_Automations_Toater_Verification();
	}

	public void Upcoming_Manage_ClearTitle_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		waitAndLog(2);
		BM_Pages.tools_BM_title.clear();
		waitAndLog(2);
		BM_ScheduleBroadCast();
		waitAndLog(2);
		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		logger.info(BM_Pages.tools_BM_past_list_Title.getText());
		logger.info(BM_Pages.tools_BM_create_upcoming_list_DateAndTime.getText());
	}

	public void create_DraftBM_with_Title_SendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		BM_Terms_Conditions();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Click the Save As Draft");
		BM_Automations_Draft_Toaster_Verification();

	}

	public void Draft_Manage_ClearTitle_SendBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on tools_BM_draft ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Draft_manage, "button").click();
		logger.info("click on tools_BM_create_Draft_manage ");
		BM_Pages.tools_BM_title.clear();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		BM_SendBM();
	}

	public void create_DraftBM_with_Title_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		BM_Terms_Conditions();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Click the Save As Draft");
		BM_Automations_Draft_Toaster_Verification();

	}

	public void Draft_Manage_ClearTitle_ScheduleBM() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on tools_BM_draft ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Draft_manage, "button").click();
		logger.info("click on tools_BM_create_Draft_manage ");
		BM_Pages.tools_BM_title.clear();
		waitAndLog(2);
		BM_ScheduleBroadCast();
	}

}
