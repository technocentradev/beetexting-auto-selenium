package com.beetext.qa.Workflows.broadcast;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_BM_Edit_WF extends Broadcast {
	BroadCast_Pages BM_Pages = new BroadCast_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void tools_BM_upcoming_Edit_monthly_recurring() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);

		logger.info(" Enter the title name");
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		implicitwaitAndLog(2);
		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		if (verifyElementIsEnabled(BM_Pages.tools_BM_upcoming_list_recurring_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming_list_recurring_icon));
			textbox(BM_Pages.tools_BM_upcoming_list_recurring_icon).isEnabled();
			logger.info("Tools BM Recurring Icon is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming_list_recurring_icon));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_upcoming_Edit_uncheckrecurring_shownInPast() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("Click on Past button in BroadCast");

		String Title = tools_BM_past_list_Title.getText();
		logger.info("Title name---> " + Title);

		String AppandDate = tools_BM_past_list_DateAndTime.getText();
		logger.info("Title name " + AppandDate);

	}

	public void tools_BM_upcoming_Edit_removeTag_savedasDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Cross Mark");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

		draft_Delete();
	}

	public void tools_BM_upcoming_Edit_removeMSG_addAttahment_shownInPast() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		textbox(BM_Pages.tools_BM_message).clear();
		waitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");
		waitAndLog(5);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("Click on Past button in BroadCast");

		String Title = tools_BM_past_list_Title.getText();
		logger.info("Title name---> " + Title);

		String AppandDate = tools_BM_past_list_DateAndTime.getText();
		logger.info("Title name " + AppandDate);

	}

	public void tools_BM_upcoming_Edit_removeMSG_addAttahment_scheduleLater_shownInupcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		textbox(BM_Pages.tools_BM_message).clear();
		waitAndLog(2);
		textbox(BM_Pages.tools_BM_message).sendKeys("Meet");
		logger.info(" Enter the Msg ");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();

		waitAndLog(3);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");
		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_upcoming_Edit_removeTag_weeklyRecurring_savedasDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Cross Mark");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		implicitwaitAndLog(2);

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void tools_BM_upcoming_Edit_unSelectRecurring_schedule_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

	}

	public void tools_BM_upcoming_Edit_Recurring_DailyToweekly_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		implicitwaitAndLog(2);

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_upcoming_Edit_message_ShowninUPcoming() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info("clear your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_removemessage_adAttachment_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info("clear your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_removeMSg_removeAttachment_savedasDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_Recurring_With_Attachment();

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void tools_BM_upcoming_Edit_removeTag_MSg_removeAttachment_savedasDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_Recurring_With_Attachment();

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Cross Mark");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info("clear your messeage");
		implicitwaitAndLog(2);

		BM_Pages.tools_BM_remove_attachment.click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

	}

	public void tools_BM_upcoming_Edit_Recurring_DailyTo_other_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		implicitwaitAndLog(2);

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_Recurring_monthly_Select_endDate_lessthan_today_errorDisplay() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on tools_BM_create_make_this_broadCast_recurring manage button");
		waitAndLog(2);

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime, "button").click();
		logger.info("click on tools_BM_create_recurring_appDateTime Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_month, "button").click();
		logger.info("click on tools_BM_create_recurring_month Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_year, "button").click();
		logger.info("click on tools_BM_create_recurring_year Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_lessMonth1, "button").click();
		logger.info("click on tools_BM_create_recurring_lessMonth Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime_SelectDate, "button").click();
		logger.info("click on recurring Select App Date and Time button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

		if (BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is matching");
			logger.info(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

	public void tools_BM_upcoming_Edit_Remove_Recurring_Showninpast() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info("clear your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_Recurring_yearlyTo_other_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_upcoming_Edit_Recurring_yearly_changeMsg_ShowninUPcoming() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Hi All Techno");
		logger.info(" Enter your messeage");

		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_Recurring_yearly_AddAttachment_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_YearlyRecurring_removemessage_adAttachment_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(3);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_upcoming_Edit_yearlyRecurring_removeMSg_removeAttachment_savedasDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring_With_Attachment();

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");
	}

	public void tools_BM_upcoming_Edit_Recurring_yearly_To_other_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_Recurring_yearly_Select_endDate_lessthan_today_errorDisplay() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime, "button").click();
		logger.info("click on tools_BM_create_recurring_appDateTime Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_month, "button").click();
		logger.info("click on tools_BM_create_recurring_month Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_year, "button").click();
		logger.info("click on tools_BM_create_recurring_year Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_lessMonth, "button").click();
		logger.info("click on tools_BM_create_recurring_lessMonth Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime_SelectDate, "button").click();
		logger.info("click on recurring Select App Date and Time button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

		if (BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is matching");
			logger.info(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

	public void tools_BM_upcoming_Edit_NonRecurring_click_SaveDraft_ShowninUPcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on past button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past reuse button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save as Draft ");
	}

	public void tools_BM_upcoming_Edit_NonRecurring_click_breadcumLink_SaveDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past reuse button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_upcoming_Edit_NonRecurring_RemoveMsg_click_breadcumLink_SaveDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on past button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past reuse button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_upcoming_Edit_NonRecurring_RemoveMsg_addAttachment_click_breadcumLink_SaveDraft()
			throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past reuse button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_upcoming_Edit_weekly_Remove_Recurring_Showninpast() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_changetoweekly_Remove_Recurring_Showninpast() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_weekly_changeMsg_Showninpast() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		button(BM_Pages.tools_BM_create_lesstarget, "button").click();
		logger.info("click on More Target");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Hi All People");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_weekly_changeMsg_addAttachment() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Hi All People");
		logger.info(" Enter your messeage");
		waitAndLog(3);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on sendNow Later button");
		implicitwaitAndLog(2);
		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_upcoming_Edit_weekly_RemoveMsg_addAttachment() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on sendNow Later button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_upcoming_Edit_weekly_RemoveMsg_RemoveAttachment_ShownInDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(5);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on send Now Later button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_weeklyRecuring_upcoming_Edit_other_updated() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_upcoming_Edit_Recurring_weekly_Select_endDate_lessthan_today_errorDisplay() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_recurring_appDateTime, "button").click();
		logger.info("click on tools_BM_create_recurring_appDateTime Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_month, "button").click();
		logger.info("click on tools_BM_create_recurring_month Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_year, "button").click();
		logger.info("click on tools_BM_create_recurring_year Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_lessMonth, "button").click();
		logger.info("click on tools_BM_create_recurring_lessMonth Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime_SelectDate, "button").click();
		logger.info("click on recurring Select App Date and Time button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

		if (BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is matching");
			logger.info(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

	public void tools_BM_weeklyRecuring_upcoming_Edit_uncheckRecurring_updatedInPast() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_NonRecuring_upcoming_Edit_weeklyRecurring_updatedInupcoming() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_upcoming_Edit_monthly_changeMsg_Showninpast() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		BMCreate_Schedule();
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Hi All People");
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_upcoming_Edit_monthly_changeMsg_addAttachment() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Hi All People");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		implicitwaitAndLog(2);
		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_upcoming_Edit_monthly_RemoveMsg_addAttachment() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		BM_Edit_Schedule_with_2min();

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_upcoming_Edit_monthly_RemoveMsg_RemoveAttachment_ShownInDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_Recurring_With_Attachment();

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_monthlyRecuring_upcoming_Edit_other_updated() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Weekly on (Selected day)")) {

				allOptions1.get(i).click();
				break;

			}
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_upcoming_Edit_Recurringyearly_Select_endDate_lessthan_today_errorDisplay() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		List<WebElement> allOptions3 = driver.findElements(By.xpath("(//select[1]//option)[4]"));
		logger.info(allOptions3.size());

		for (int i = 0; i <= allOptions3.size() - 1; i++) {

			if (allOptions3.get(i).getText().contains("Yearly on (Date) (Month)")) {

				allOptions3.get(i).click();
				break;

			}
		}

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime, "button").click();
		logger.info("click on tools_BM_create_recurring_appDateTime Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_month, "button").click();
		logger.info("click on tools_BM_create_recurring_month Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_year, "button").click();
		logger.info("click on tools_BM_create_recurring_year Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_recurring_lessMonth, "button").click();
		logger.info("click on tools_BM_create_recurring_lessMonth Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_recurring_appDateTime_SelectDate, "button").click();
		logger.info("click on recurring Select App Date and Time button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

		if (BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText()
				.equalsIgnoreCase("Please select a date in the future.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is matching");
			logger.info(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_recurring_appDateTime_inFuture_errorMsg.getText(),
					"Please select a date in the future.",
					"tools_BM_create_recurring_appDateTime_inFuture_errorMsg is not matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_create_recurring_appDateTime_inFuture_errorMsgTestCase");// ScreenShot
			// capture
		}

	}

	public void tools_BM_NonRecuring_past_Edit_SaveDraft_updatedInDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");
		Draft_Toaster();
		waitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_breadcumlink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_NonRecuring_past_Edit_removeTag_breadcumlink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_removeMsg_removeAttachment_breadcumlink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_BM_With_Attachment();
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_addTag_breadcumlink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_Msg_breadcumlink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Meet");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_NonRecuring_past_Edit_manageAttachments_breadcumlink_clickSave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waitAndLog(5);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_NonRecuring_past_Edit_clickotherLink_clickSave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_NonRecuring_past_Edit_anyField_clickotherLink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_RemoveTag_clickotherLink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_NonRecuring_past_Edit_removeMsg_Attachment_clickotherLink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" remove messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_NonRecuring_past_Edit_addTag_recurring_clickotherLink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_msg_clickotherLink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("winner");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_editAttachment_clickotherLink_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("winner");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		draft_Delete();

	}

	public void tools_BM_NonRecuring_past_Edit_outsideClick_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("winner");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
	}

	public void tools_BM_NonRecuring_past_Edit_changeAnyField_outsideClick_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("winner");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
	}

	public void tools_BM_NonRecuring_past_Edit_RemoveTag_outsideClick_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_RemoveMsg_Attachment_outsideClick_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_title).sendKeys("Winner");
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		Draft_Toaster();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_Add_Attachment_outsideClick_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(5);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waitAndLog(5);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		Draft_Toaster();
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		waitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_msg_outsideClick_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_message).sendKeys(" - Meeting ");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		Draft_Toaster();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		implicitwaitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_NonRecuring_past_Edit_Attachment_outsideClick_clickSave() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");

		textbox(BM_Pages.tools_BM_message).sendKeys(" - Meeting ");
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waitAndLog(5);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		Draft_Toaster();
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_Recuring_upcoming_Edit_SaveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_Past_Save_Draft, "button").click();
		logger.info("click on past Save Draft button");
		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_Recuring_upcoming_Edit_breadcumLink_Save() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_addAttachment_breadcumLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_breadcumLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		BMCreate_Schedule();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_addTwoTags_breadcumLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_Recuring_upcoming_Edit_Msg_breadcumLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Hi All GM");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_Attachment_breadcumLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring_With_Attachment();

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waitAndLog(5);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_click_onOtherLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).sendKeys("HEllo");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_AddTag_Msg_click_onOtherLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("HEllo");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveTag_Msg_click_onOtherLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Remove tag for BM button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_onOtherLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(5);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		BMCreate_Schedule();

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_AddMoreTags_click_onOtherLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		wait(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_Msg_click_onOtherLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Hello Everyone");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_Recuring_upcoming_Edit_Attachment_click_onOtherLink_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring_With_Attachment();

		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_Recuring_upcoming_Edit_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);

	}

	public void tools_BM_Recuring_upcoming_Edit_AddMoreTags_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);

	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveTags_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String title = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(5);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		waitAndLog(2);
	}

	public void tools_BM_Recuring_SM_upcoming_Edit_RemoveMsg_AddAttachment_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);

	}

	public void tools_BM_Recuring_upcoming_Edit_Msg_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Team");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
	}

	public void tools_BM_Recuring_upcoming_Edit_Attachment_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String title = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		BMCreate_Schedule();
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\jpg_file.jpg");

		waitAndLog(5);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools").click();
		waitAndLog(2);
	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveMsg_RemoveAttachment_click_outside_Save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String title = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		BMCreate_Schedule();
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);

	}

	public void tools_BM_Recuring_upcoming_Edit_SaveDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");

		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_Recuring_upcoming_Edit_removeMsg_title_SendBroadCast() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_ScheduleBroadcast() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring_With_Attachment();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_Recuring_upcoming_Edit_deselectRecurring_SaveDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");

		Draft_Toaster();

		draft_Delete();
	}

	public void tools_BM_Recuring_upcoming_Edit_deselectRecurring_removeTags_SaveDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");

		Draft_Toaster();

		draft_Delete();
	}

	public void tools_BM_Recuring_upcoming_Edit_Save_Draft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");
	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveTags_click_ScheduleBroadcast() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		implicitwaitAndLog(2);
		BM_Edit_Schedule_with_2min();
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_Recuring_upcoming_Edit_RemoveMsg_attachment_click_ScheduleBroadcast() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring_With_Attachment();
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_Recuring_upcoming_Edit_deselectRecurring_Save_Draft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");
		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Schedule_BM() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");
	}

	public void tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_addAttachment_Schedule_BM()
			throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_Recurring();
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		BM_Edit_Schedule_with_2min();

		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

	}

	public void tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Attachment_Schedule_BM() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring_With_Attachment();
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

	}

	public void tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveTags_Attachment_SaveDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		create_Recurring_With_Attachment();
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");

		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save as Draft ");

	}

	public void tools_BM_Non_Recuring_Past_Edit_SaveDraft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save as Draft ");

		Draft_Toaster();
		draft_Delete();

	}

	public void tools_BM_Non_Recuring_Past_Edit_removeTags_Schedule_BM() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

		Draft_Toaster();
		draft_Delete();
	}

	public void tools_BM_Non_Recuring_Past_Edit_removemsg_Attachment_Schedule_BM() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		String title = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_remove_attachment, "button").click();
		logger.info("click on button to remove attachment");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");
	}

	public void tools_BM_Draft_Edit_Save_Draft() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Draft_manage, "button").click();
		logger.info("click on Draft - Manage ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Past_Save_Draft, "button").click();
		logger.info("click on BroadCast Save as Draft");

		draft_Delete();
	}

	public void tools_BM_Draft_Edit_manage_delete() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		draft_Delete();

	}

	public void tools_BM_upcoming_Edit_Reuse_AddTag_ChangeTitle_msg_updated_created() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming reuse button");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on send BroadCast");

	}

	public void tools_BM_past_Edit_Reuse_Add_Two_Tag_ChangeTitle_msg_updated_created() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

	}

	public void tools_BM_past_Edit_Reuse_ChangeTitle_click_BroadcastLink_save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_past_Edit_Reuse_ChangeTitle_click_BroadcastLink_Discard() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);
		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_past_Edit_Reuse_ChangeTitle_click_otherLink_save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");
		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		draft_Delete();
	}

	public void tools_BM_past_Edit_Reuse_ChangeTitle_click_otherLink_Discard() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			textbox(BM_Pages.tools_BM_create_create_SM).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_past_Edit_Reuse_click_outside_save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		Draft_Toaster();
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools").click();
		waitAndLog(2);
		draft_Delete();
	}

	public void tools_BM_past_Edit_Reuse_click_outside_Discard() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools").click();
		waitAndLog(2);
	}

	public void tools_BM_past_Edit_Reuse_ChangeTitle_click_outside_save() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		Draft_Toaster();
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools bttn").click();
		waitAndLog(2);
	}

	public void tools_BM_past_Edit_Reuse_changeTitle_click_outside_Discard() throws Exception {
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String title1 = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(title1);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tools bttn").click();
		waitAndLog(2);

	}

}
