package com.beetext.qa.Workflows.broadcast;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.BM_Optout_WFCM;
import com.beetext.qa.pages.BM_Optout_Pages;
import com.beetext.qa.util.TestUtil;;

public class BM_Opt_Out_WF extends BM_Optout_WFCM {

	String url, username1, username2, password;
	BM_Optout_Pages BM_Opt_Pages = new BM_Optout_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void Create_BM_with_OptOut() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Opt_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Opt_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Opt_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Opt_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Opt_Pages.increase_1hr_ScheduleTime, "button").click();
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

	}

	public void upcoming_Mange_Optout_Enable() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			button(BM_Opt_Pages.BM_Optout_Checkbox, "Click on BM_Opt_Pages.BM_Optout_Checkbox").isEnabled();
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Disabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture
		}

	}

	public void upcoming_Mange_Disable_Optout_Click_on_OtherTools_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_Templates, "button").click();
		logger.info("click on tools_Templates ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void upcoming_Manage_Disable_Optout_Click_on_outside_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(3);
		link(BM_Opt_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(3);

	}

	public void upcoming_Mange_Disable_Optout_Click_on_Breadcumlink_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);
		JSClick(BM_Opt_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void create_DraftBM_without_Msg() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Opt_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Opt_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Opt_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

	}

	public void manage_DraftBM_Add_Message_and_Update() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_draft, "button").click();
		logger.info("click on tools_BM_draft Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Draft_manage, " Click on button").click();
		logger.info("Enter data in search tags ");
		waitAndLog(2);
		textbox(BM_Opt_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		BM_ScheduleBroadCast();
		if (BM_Opt_Pages.BM_Updated_Toaster_Automations.getText()
				.equalsIgnoreCase("Your broadcast has been successfully created for Automations.")) {
			Assert.assertEquals(BM_Opt_Pages.BM_Updated_Toaster_Automations.getText(),
					"Your broadcast has been successfully created for Automations.", "message is matching");
			logger.info(BM_Opt_Pages.BM_Updated_Toaster_Automations.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Opt_Pages.BM_Updated_Toaster_Automations.getText(),
					"Your broadcast has been successfully created for Automations.", "message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void manage_DraftBM_unCheck_optout_and_Update() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Opt_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Opt_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Opt_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Opt_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();
		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_draft, "button").click();
		logger.info("click on tools_BM_draft Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Draft_manage, " Click on button").click();
		logger.info("Enter data in search tags ");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		BM_ScheduleBroadCast();
		if (BM_Opt_Pages.BM_Updated_Toaster_Automations.getText()
				.equalsIgnoreCase("Your broadcast has been successfully created for Automations.")) {
			Assert.assertEquals(BM_Opt_Pages.BM_Updated_Toaster_Automations.getText(),
					"Your broadcast has been successfully created for Automations.", "message is matching");
			logger.info(BM_Opt_Pages.BM_Updated_Toaster_Automations.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Opt_Pages.BM_Updated_Toaster_Automations.getText(),
					"Your broadcast has been successfully created for Automations.", "message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void manage_DraftBM_unCheck_optout_and_SaveDraft() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Opt_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Opt_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Opt_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Opt_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();
		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_draft, "button").click();
		logger.info("click on tools_BM_draft Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Draft_manage, " Click on button").click();
		logger.info("Enter data in search tags ");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Clear the Save As Draft");

	}

	public void manage_DraftBM_unCheck_optout_Click_Outside_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Opt_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Opt_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Opt_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Opt_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();
		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_draft, "button").click();
		logger.info("click on tools_BM_draft Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Draft_manage, " Click on button").click();
		logger.info("Enter data in search tags ");
		waitAndLog(2);
		button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
		logger.info("click on BM_Optout_Checkbox button");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(3);
		link(BM_Opt_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(3);

	}

	public void past_reuse_OptoutMsg_Click_breadcumlink_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			button(BM_Opt_Pages.BM_Optout_Checkbox, "Click on BM_Opt_Pages.BM_Optout_Checkbox").isEnabled();
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Disabled");
			button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture
		}

		waitAndLog(2);
		JSClick(BM_Opt_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void past_reuse_OptoutMsg_Click_otherlink_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			button(BM_Opt_Pages.BM_Optout_Checkbox, "Click on BM_Opt_Pages.BM_Optout_Checkbox").isEnabled();
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Disabled");
			button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_Templates, "button").click();
		logger.info("click on tools_Templates ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void past_view_reuse_OptoutMsg_Click_breadcumlink_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Past_view, "button").click();
		logger.info("click on Past view in Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			button(BM_Opt_Pages.BM_Optout_Checkbox, "Click on BM_Opt_Pages.BM_Optout_Checkbox").isEnabled();
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Disabled");
			button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		JSClick(BM_Opt_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void past_view_reuse_OptoutMsg_Click_otherlink_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Past_view, "button").click();
		logger.info("click on Past view in Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			button(BM_Opt_Pages.BM_Optout_Checkbox, "Click on BM_Opt_Pages.BM_Optout_Checkbox").isEnabled();
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Disabled");
			button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_Templates, "button").click();
		logger.info("click on tools_Templates ");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

		waitAndLog(2);

	}

	public void past_view_reuse_OptoutMsg_Click_outside_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_Past_view, "button").click();
		logger.info("click on Past view in Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			button(BM_Opt_Pages.BM_Optout_Checkbox, "Click on BM_Opt_Pages.BM_Optout_Checkbox").isEnabled();
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Disabled");
			button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(3);
		link(BM_Opt_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(3);
	}

	public void past_reuse_OptoutMsg_Click_outside_Save() throws Exception {

		waitAndLog(2);

		button(BM_Opt_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Opt_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			button(BM_Opt_Pages.BM_Optout_Checkbox, "Click on BM_Opt_Pages.BM_Optout_Checkbox").isEnabled();
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.BM_Optout_Checkbox));
			logger.info("BM_Opt_Pages.BM_Optout_Checkbox is Disabled");
			button(BM_Opt_Pages.BM_Optout_Checkbox, "button").click();
			TestUtil.takeScreenshotAtEndOfTest("BM_Opt_Pages.BM_Optout_Checkbox");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Opt_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Opt_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(5);

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Opt_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Opt_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Opt_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		button(BM_Opt_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(3);
		link(BM_Opt_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(3);

	}

}
