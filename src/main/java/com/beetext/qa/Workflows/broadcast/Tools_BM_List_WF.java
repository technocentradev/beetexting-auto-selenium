package com.beetext.qa.Workflows.broadcast;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.util.TestUtil;

public class Tools_BM_List_WF extends Broadcast {
	String url, username1, username2, password;
	BroadCast_Pages BM_Pages = new BroadCast_Pages();
	String randomtitle = RandomStringUtils.randomAlphabetic(5);
	public static String currentDir = System.getProperty("user.dir");

	public void tools_BM_list_BM_upcoming_past_draft_enabled() throws Exception {

		wait(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.broadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.broadCast));
			textbox(BM_Pages.broadCast).isEnabled();
			logger.info("By Defaultly Broadcast is Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.broadCast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String upcoming = BM_Pages.tools_BM_upcoming.getText();
		logger.info(upcoming);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_upcoming)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming));
			textbox(BM_Pages.tools_BM_upcoming).isEnabled();
			logger.info("upcoming is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String past = BM_Pages.tools_BM_past.getText();
		logger.info(past);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_past)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_past));
			textbox(BM_Pages.tools_BM_past).isEnabled();
			logger.info("Past is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_past));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String Draft = BM_Pages.tools_BM_draft.getText();
		logger.info(Draft);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_draft));
			textbox(BM_Pages.tools_BM_draft).isEnabled();
			logger.info("Draft is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_list_Display_list() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");

		implicitwaitAndLog(2);

		List<WebElement> allOptions = driver.findElements(By.xpath("(//select[1]//option)[1]"));
		logger.info(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Auto1")) {

				allOptions.get(i).click();
				break;

			}
		}

		logger.info("Auto1");

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		for (int i = 0; i <= allOptions1.size() - 1; i++) {

			if (allOptions1.get(i).getText().contains("Auto2")) {

				allOptions1.get(i).click();
				break;

			}
		}

		logger.info("Auto2");

	}

	public void tools_BM_list_upcoming_Display_manage_Reuse_options() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		String upcoming = BM_Pages.tools_BM_upcoming.getText();
		logger.info(upcoming);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		String upcoming_manage = BM_Pages.tools_BM_create_upcoming_manage.getText();
		logger.info(upcoming_manage);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_manage)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_manage));
			textbox(BM_Pages.tools_BM_create_upcoming_manage).isEnabled();
			logger.info("Tools BM in upcoming manage is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_manage));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String upcoming_Reuse = BM_Pages.tools_BM_create_upcoming_Reuse.getText();
		logger.info(upcoming_Reuse);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_Reuse)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_Reuse));
			textbox(BM_Pages.tools_BM_create_upcoming_Reuse).isEnabled();
			logger.info("Tools BM In upcoming Re-use is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_Reuse));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_list_Past_Display_view_Reuse_options() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		String Past = BM_Pages.tools_BM_past.getText();
		logger.info(Past);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		String Past_view = BM_Pages.tools_BM_create_Past_view.getText();
		logger.info(Past_view);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_Past_view)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_Past_view));
			textbox(BM_Pages.tools_BM_create_Past_view).isEnabled();
			logger.info("Tools BM In Past View is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_Past_view));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String past_Reuse = BM_Pages.tools_BM_create_past_Reuse.getText();
		logger.info(past_Reuse);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_past_Reuse)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_past_Reuse));
			textbox(BM_Pages.tools_BM_create_past_Reuse).isEnabled();
			logger.info("Tools BM In Past Re-use is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_past_Reuse));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_list_Draft_Display_Manage_Delete_options() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		String Draft = BM_Pages.tools_BM_draft.getText();
		logger.info(Draft);

		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on Draft button");
		implicitwaitAndLog(2);

		String Draft_manage = BM_Pages.tools_BM_draft.getText();
		logger.info(Draft_manage);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_Draft_manage)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_Draft_manage));
			textbox(BM_Pages.tools_BM_create_Draft_manage).isEnabled();
			logger.info("Tools BM In Draft Manage is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_Draft_manage));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		String Draft_delete = BM_Pages.tools_BM_create_Draft_Delete.getText();
		logger.info(Draft_delete);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_Draft_Delete)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_Draft_Delete));
			textbox(BM_Pages.tools_BM_create_Draft_Delete).isEnabled();
			logger.info("Tools BM In Draft Delete is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_Draft_Delete));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_list_upcoming_listwith_RecurringIcon() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");

		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		String title = BM_Pages.tools_BM_upcoming_list_title.getText();
		String DateAndTime = BM_Pages.tools_BM_upcoming_list_DateTime.getText();
		logger.info(title + " " + DateAndTime);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_upcoming_list_recurring_icon)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming_list_recurring_icon));
			textbox(BM_Pages.tools_BM_upcoming_list_recurring_icon).isEnabled();
			logger.info("Tools BM Recurring Icon is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming_list_recurring_icon));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_list_upcoming_list() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		String title = BM_Pages.tools_BM_upcoming_list_title.getText();
		String DateAndTime = BM_Pages.tools_BM_upcoming_list_DateTime.getText();
		logger.info(title + " " + DateAndTime);

	}

	public void tools_BM_list_upcoming_clickOn_manage() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_Send_BroadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_Send_BroadCast));
			textbox(BM_Pages.tools_BM_create_Send_BroadCast).isEnabled();
			logger.info("Manage Send BroadCast is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_Send_BroadCast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_manage_delete)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_manage_delete));
			textbox(BM_Pages.tools_BM_create_upcoming_manage_delete).isEnabled();
			logger.info("Manage Delete is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_upcoming_manage_delete));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_list_upcoming_clickOn_manage_changeAnyField_SendBroadCast() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

		Auto1_Updated_Toaster();

	}

	public void tools_BM_list_upcoming_clickOn_manage_removeTag_AddNewTag_SendBroadCast() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on tools_BM_create_tag_crossmark");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("b");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");

		draft_Delete();
	}

	public void tools_BM_page_List_createBM_Recurring_Manage_withoutRecurring() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

		upcoming_Manage_Delete();
	}

	public void tools_BM_page_List_createBM_withoutRecurring_Manage_withRecurring() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past Re-use button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);

		BM_ScheduleBroadCast();
	}

	public void tools_BM_list_upcoming_clickOn_manage_DeleteBM() throws Exception {

		waitAndLog(2);

		upcoming_Manage_Delete();
	}

	public void tools_BM_list_upcoming_clickOn_manage_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Save_Draft, "button").click();
		logger.info("click on Save Draft button");

		Draft_Toaster();

		draft_Delete();
	}

	public void tools_BM_list_upcoming_clickOn_manage_addTwoTags_ScheduleLater() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		;
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		;
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();

		BM_ScheduleBroadCast();

		Auto1_Updated_Toaster();

		upcoming_Manage_Delete();

	}

	public void tools_BM_list_upcoming_clickOn_manage_RemoveMsg_addAttachment() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys("Hello Tech");
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);

		BM_SendBM();
		Auto1_Updated_Toaster();

	}

	public void tools_BM_page_Upcoming_manage_Click_onSM_PopupOpens_Click_onsave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_page_Upcoming_manage_Click_onSM_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			textbox(BM_Pages.tools_BM_create_create_SM).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_manage_Click_on_Templates_PopupOpens_Click_onSave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_page_Upcoming_manage_Click_onTemplates_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			textbox(BM_Pages.tools_BM_create_create_New_Template).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_manage_changeMsg_ScheduleLater_Click_on_SM_PopupOpens_Click_onSave()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_page_Upcoming_manage_Click_onTags_PopupOpens_Click_onsave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_page_Upcoming_manage_Click_oTags_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			textbox(BM_Pages.tools_BM_create_create_New_Tag).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_manage_Click_onAutomations_PopupOpens_Click_onsave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_page_Upcoming_manage_Click_onAutomations_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			textbox(BM_Pages.tools_BM_create_create_New_Automation).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_manage_Click_onAdvocate_PopupOpens_Click_onsave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_page_Upcoming_manage_Click_onAdvocate_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			textbox(BM_Pages.tools_BM_create_create_Advocate_gotoReflection).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_manage_Click_onReviews_PopupOpens_Click_onsave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_page_Upcoming_manage_Click_onReviews_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_message).clear();
		logger.info(" Enter your messeage");

		textbox(BM_Pages.tools_BM_message).sendKeys("Techies");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			textbox(BM_Pages.tools_BM_create_create_ADDReview).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_manage_Click_onHealth_Screeings_PopupOpens_Click_onsave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Health_Screenings, "button").click();
		logger.info("click on Health Screnings ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_page_Upcoming_manage_Click_onHealth_Screenings_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Health_Screenings, "button").click();
		logger.info("click on Health Screnings ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate));
			textbox(BM_Pages.tools_BM_Health_Screening_Activate).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_manage_change_title_msg_click_outside_PopupOpens_Click_onSave()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);
	}

	public void tools_BM_page_Upcoming_manage_Click_onOutside_PopupOpens_Click_onDiscard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_Upcoming_manage_AddAttachmentChange_DateAndTime_Click_onOutside_PopupOpens_Click_onDiscard()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(3);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(3);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		BM_Edit_Schedule_with_2min();

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_Upcoming_manage_change_title_msg_click_BMLink_PopupOpens_Click_onSave() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_page_Upcoming_manage_change_title_msg_click_BMLink_PopupOpens_Click_onDiscard()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.broadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.broadCast));
			textbox(BM_Pages.broadCast).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.broadCast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_manage_change_title_IMG_ScheduleLater_click_BMLink_PopupOpens_Click_onSave()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(3);

		List<WebElement> allOptions1 = driver.findElements(By.xpath("(//select[1]//option)[2]"));
		logger.info(allOptions1.size());

		waitAndLog(2);
		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");
	}

	public void tools_BM_page_Upcoming_manage_change_title_IMG_ScheduleLater_click_BMLink_PopupOpens_Click_onDiscard()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);

		BM_Edit_Schedule_with_2min();

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(3);

		waitAndLog(2);
		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.broadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.broadCast));
			textbox(BM_Pages.broadCast).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.broadCast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_Reuse_BM_page() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_title)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_title));
			textbox(BM_Pages.tools_BM_title).isEnabled();
			logger.info("Reuse Page is Opened");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_title));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Tag cross button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag2, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
//		draft_Delete( );
	}

	public void tools_BM_page_Upcoming_Reuse_BM_cancel() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse_cancel, "button").click();
		logger.info("click on Upcoming Reuse Cancel button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("Create BroadCast is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_reuse_change_title_IMG_monthlyScheduleLater_click_BMLink_PopupOpens_Click_onSave()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		BM_Edit_Schedule_with_2min();
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		List<WebElement> allOptions2 = driver.findElements(By.xpath("(//select[1]//option)[3]"));
		logger.info(allOptions2.size());

		for (int i = 0; i <= allOptions2.size() - 1; i++) {

			if (allOptions2.get(i).getText().contains("Monthly on the (Date)")) {

				allOptions2.get(i).click();
				break;

			}
		}

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow_() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		create_Recurring();
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Tag cross button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag2, "button").click();
		logger.info("click on tag");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");
	}

	public void tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow_addAttachment() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_tag_crossmark, "button").click();
		logger.info("click on Tag cross button");

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag2, "button").click();
		logger.info("click on tag");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send Bm ");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_dont_change_anyFields_click_outside_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);
	}

	public void tools_BM_page_Upcoming_Reuse_BM_dont_change_anyFields_click_outside_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(3);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard button");

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Successfully Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);
	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_outside_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(3);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);
	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_outside_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(3);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard button");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Successfully Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);
	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_SM_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_SM_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			textbox(BM_Pages.tools_BM_create_create_SM).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Templates_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Templates_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard button");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			textbox(BM_Pages.tools_BM_create_create_New_Template).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Tags_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Tags_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			textbox(BM_Pages.tools_BM_create_create_New_Tag).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Automations_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Automations_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			textbox(BM_Pages.tools_BM_create_create_New_Automation).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Advocate_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Advocate_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			textbox(BM_Pages.tools_BM_create_create_Advocate_gotoReflection).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_review_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Review_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			textbox(BM_Pages.tools_BM_create_create_ADDReview).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Health_Screenings_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Health_Screenings, "button").click();
		logger.info("click on Health Screnings ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_Reuse_BM_change_title_click_Health_Screenings_Save_As_Discard()
			throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Health_Screenings, "button").click();
		logger.info("click on Health Screnings ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate));
			textbox(BM_Pages.tools_BM_Health_Screening_Activate).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_Manage_BM_click_Navigation_link_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Upcoming_manage_BM_click_navigationLink_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on Upcoming manage button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(3);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(3);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Upcoming_Reuse_BM_click_Navigation_link_Save_As_Draft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(3);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_page_Upcoming_Reuse_BM_click_navigationLink_Save_As_Discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		create_Recurring();
		waitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on Upcoming button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_Reuse, "button").click();
		logger.info("click on Upcoming Reuse button");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(3);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(5);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_display_list_of_pastMsg() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");

		String title = BM_Pages.tools_BM_upcoming_list_title.getText();
		String DateandTime = BM_Pages.tools_BM_Past_list_DateTime.getText();
		String View = BM_Pages.tools_BM_create_Past_view.getText();
		String Reuse = BM_Pages.tools_BM_create_past_Reuse.getText();

		logger.info(title + "" + DateandTime + "" + View + "" + Reuse);

		String title1 = BM_Pages.tools_BM_upcoming_list_title1.getText();
		String DateandTime1 = BM_Pages.tools_BM_Past_list_DateTime.getText();
		String View1 = BM_Pages.tools_BM_create_Past_view1.getText();
		String Reuse1 = BM_Pages.tools_BM_create_past_Reuse1.getText();

		logger.info(title1 + "" + DateandTime1 + "" + View1 + "" + Reuse1);

		String title2 = BM_Pages.tools_BM_upcoming_list_title2.getText();
		String DateandTime2 = BM_Pages.tools_BM_Past_list_DateTime.getText();
		String View2 = BM_Pages.tools_BM_create_Past_view2.getText();
		String Reuse2 = BM_Pages.tools_BM_create_past_Reuse2.getText();

		logger.info(title2 + "" + DateandTime2 + "" + View2 + "" + Reuse2);

		String title3 = BM_Pages.tools_BM_upcoming_list_title3.getText();
		String DateandTime3 = BM_Pages.tools_BM_Past_list_DateTime.getText();
		String View3 = BM_Pages.tools_BM_create_Past_view3.getText();
		String Reuse3 = BM_Pages.tools_BM_create_past_Reuse3.getText();

		logger.info(title3 + "" + DateandTime3 + "" + View3 + "" + Reuse3);

	}

	public void tools_BM_page_Past_click_on_view_opens_viewPage() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_past_Reuse)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_past_Reuse));
			textbox(BM_Pages.tools_BM_create_past_Reuse).isEnabled();
			logger.info("Re-use is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_past_Reuse));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_send() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past view - reuse in Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

	}

	public void tools_BM_page_Past_view_reuse_UpdateInBm_send() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Past_view, "button").click();
		logger.info("click on Past view in Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past view - reuse in Bm ");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on tools_BM_create_sendNow ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Send BroadCast");

		Auto1_Created_Toaster();

	}

	public void tools_BM_page_Past_view_reuse_cancel() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_Past_view, "button").click();
		logger.info("click on Past view in Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past view - reuse in Bm ");

		textbox(BM_Pages.tools_BM_title).clear();
		logger.info("Clear the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_Reuse_cancel, "button").click();
		logger.info("click on Cancel BroadCast");

	}

	public void tools_BM_page_Past_view_reuse_UpdateInBm_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		button(BM_Pages.tools_BM_Past_Save_Draft, "button").click();
		logger.info("click on BroadCast Save as Draft");

		Draft_Toaster();
	}

	public void tools_BM_page_Past_view_reuse_click_navigationLink_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		past_reuse();

		waitAndLog(2);

		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		Draft_Toaster();
	}

	public void tools_BM_page_Past_view_reuse_click_navigationLink_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");

		past_reuse();
		waitAndLog(2);
		JSClick(BM_Pages.tools_BM_create_broadCastLink);
		logger.info("click on tools_BM_create_broadCast_Link ");
		waitAndLog(3);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.create_broadcast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.create_broadcast));
			textbox(BM_Pages.create_broadcast).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.create_broadcast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_click_outside_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

		waitAndLog(2);
		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_Past_view_reuse_click_outside_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		waitAndLog(3);

		if (verifyElementIsEnabled(BM_Pages.tools_bttn)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_bttn));
			textbox(BM_Pages.tools_bttn).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_bttn));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_bttn, "click on tool button").click();
		waitAndLog(2);

	}

	public void tools_BM_page_Past_view_reuse_click_SM_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
	}

	public void tools_BM_page_Past_view_reuse_click_SM_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SM, "button").click();
		logger.info("click on SM ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			textbox(BM_Pages.tools_BM_create_create_SM).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_SM));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_click_Templates_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Past_view_reuse_click_Templates_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Templates, "button").click();
		logger.info("click on Templates ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			textbox(BM_Pages.tools_BM_create_create_New_Template).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Template));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_click_Tags_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");
		draft_Delete();

	}

	public void tools_BM_page_Past_view_reuse_click_Tags_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Tags, "button").click();
		logger.info("click on Tags ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			textbox(BM_Pages.tools_BM_create_create_New_Tag).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Tag));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_click_Automations_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Past_view_reuse_click_Automations_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Automations, "button").click();
		logger.info("click on Automations ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			textbox(BM_Pages.tools_BM_create_create_New_Automation).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_New_Automation));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_click_Advocate_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Past_view_reuse_click_Advocate_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Advocate, "button").click();
		logger.info("click on Advocate ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			textbox(BM_Pages.tools_BM_create_create_Advocate_gotoReflection).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_Advocate_gotoReflection));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_click_Review_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");

		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Past_view_reuse_click_Review_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_create_Review, "button").click();
		logger.info("click on Reviews ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			textbox(BM_Pages.tools_BM_create_create_ADDReview).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_create_ADDReview));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void tools_BM_page_Past_view_reuse_click_HealthScreenings_saveAsDraft() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_Health_Screenings, "button").click();
		logger.info("click on Health Screnings ");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			textbox(BM_Pages.tools_BM_create_PopUp_Draft).isEnabled();
			logger.info("PopUp Opens with Draft");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_create_PopUp_Draft, "button").click();
		logger.info("click on Draft button");

	}

	public void tools_BM_page_Past_view_reuse_click_healthScreenings_discard() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);

		past_reuse();

		waitAndLog(2);

		button(BM_Pages.tools_BM_Health_Screenings, "button").click();
		logger.info("click on Health Screnings ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_PopUp_Discard, "button").click();
		logger.info("click on Discard on PopUp ");
		implicitwaitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate));
			textbox(BM_Pages.tools_BM_Health_Screening_Activate).isEnabled();
			logger.info("BroadCast Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_Health_Screening_Activate));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public void tools_BM_list_SelectDisplay_Corresponding_Upcoming_Past_draft() throws Exception {

		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("AgentAutomation@yopmail.com");
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.broadCast)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.broadCast));
			textbox(BM_Pages.broadCast).isEnabled();
			logger.info("By Defaultly Broadcast is Selected");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.broadCast));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_upcoming)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming));
			textbox(BM_Pages.tools_BM_upcoming).isEnabled();
			logger.info("upcoming is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_upcoming));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_upcoming, "click on upcoming").click();

		if (BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText()
				.equalsIgnoreCase("All your upcoming broadcasts will appear here.")) {
			Assert.assertEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is matching");
			logger.info(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_past)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_past));
			textbox(BM_Pages.tools_BM_past).isEnabled();
			logger.info("Past is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_past));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "Click on Past").click();

		if (BM_Pages.tools_BM_No_past_BM_msgs.getText().equalsIgnoreCase("You have no past broadcasts.")) {
			Assert.assertEquals(BM_Pages.tools_BM_No_past_BM_msgs.getText(), "You have no past broadcasts.",
					"Message is matching");
			logger.info(BM_Pages.tools_BM_No_past_BM_msgs.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_past_BM_msgsTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_No_past_BM_msgs.getText(), "You have no past broadcasts.",
					"Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_past_BM_msgsTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_draft)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_draft));
			textbox(BM_Pages.tools_BM_draft).isEnabled();
			logger.info("Draft is Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_draft));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on draft button in BroadCast ");

		if (BM_Pages.tools_BM_NO_draft_msg.getText().equalsIgnoreCase("You can save your draft broadcasts.")) {
			Assert.assertEquals(BM_Pages.tools_BM_NO_draft_msg.getText(), "You can save your draft broadcasts.",
					"Message is matching");
			logger.info(BM_Pages.tools_BM_NO_draft_msg.getText());
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_NO_draft_msgsTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_NO_draft_msg.getText(), "You can save your draft broadcasts.",
					"Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_NO_draft_msgTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("auto1@yopmail.com");
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		waitAndLog(2);

	}

	public void tools_BM_list_scheduleTime_completed_notShown_inUpcoming() throws Exception {

		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");

		waitAndLog(120);
		logger.info("wait for 120 sec");

		driver.navigate().refresh();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		waitAndLog(2);

		BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText();
		logger.info(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText());
		if (BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText()
				.equalsIgnoreCase("All your upcoming broadcasts will appear here.")) {
			Assert.assertEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture
		}

		waitAndLog(2);
		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("auto1@yopmail.com");
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		waitAndLog(2);

	}

	public void tools_BM_page_Upcoming_manage_AddAttachment_Change_DateAndTime_click_outside_PopupOpens_Click_onSave()
			throws Exception {

		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		create_BM();

		waitAndLog(2);

		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on past re-use button");
		waitAndLog(2);
		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Scheduled Later ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		button(BM_Pages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		if (verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			textbox(BM_Pages.tools_BM_manage_PopUp_save).isEnabled();
			logger.info("PopUp Opens with Save");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_manage_PopUp_save));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			textbox(BM_Pages.tools_BM_create_PopUp_Discard).isEnabled();
			logger.info("PopUp Opens with Discard");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.tools_BM_create_PopUp_Discard));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(BM_Pages.tools_BM_manage_PopUp_save, "button").click();
		logger.info("click on Save on PopUp ");

		waitAndLog(120);
		logger.info("wait for 120 sec");

		driver.navigate().refresh();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		waitAndLog(2);
		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		waitAndLog(2);

		BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText();
		logger.info(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText());
		if (BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText()
				.equalsIgnoreCase("All your upcoming broadcasts will appear here.")) {
			Assert.assertEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_No_upcomingBroadcasts_msg.getText(),
					"All your upcoming broadcasts will appear here.", "Message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("tools_BM_No_upcomingBroadcasts_msgTestCase");// ScreenShot capture
		}
		waitAndLog(2);
		button(BM_Pages.tools_crossBtn, "Click on tools_crossBtn button").click();
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);

		textbox(BroadCast_Pages.user_id).sendKeys("auto1@yopmail.com");
		textbox(BroadCast_Pages.Passsword_ID).sendKeys("tech1234");
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);
		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		waitAndLog(2);

	}

}
