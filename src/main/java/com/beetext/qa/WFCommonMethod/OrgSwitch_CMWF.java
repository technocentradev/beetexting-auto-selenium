package com.beetext.qa.WFCommonMethod;

import java.util.Map;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Org_Switch_Pages;
import com.beetext.qa.pages.Repository;

public class OrgSwitch_CMWF extends CommonMethods {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Org_Switch_Pages org_Switch_Pages = new Org_Switch_Pages();
	public static String currentDir = System.getProperty("user.dir");

	public void auto1_Login(Map<String, String> map) throws Exception {

		username1 = (String) map.get("username1");
		username2 = (String) map.get("username2");
		password = (String) map.get("password");

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(5);
		logger.info("wait for 5 sec");

	}

	public void click_AddORG_automationsLogin(Map<String, String> map) throws Exception {

		wait(5);
		button(org_Switch_Pages.add_org, "Arrow").click();
		logger.info("click on add_org Button");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:");
		waits(2);
		logger.info("wait for 2 sec");

		button(org_Switch_Pages.letsgobttn, "Arrow").click();
		logger.info("click on letsgobttn Button");
		wait(5);
		logger.info("wait for 5 sec");

	}

}
