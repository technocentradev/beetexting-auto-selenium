package com.beetext.qa.WFCommonMethod;

import java.util.Properties;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.VCF;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.util.WebEventListener;

public class VCF_WFCM extends CommonMethods {
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");
	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	VCF vcf_Pages = new VCF();

	public void VCard_Auto1_Login() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.auto1_Manage, "click on auto1_Manage button").click();
		logger.info("click on auto1_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

	}

	public void VCard_Automations() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.Numbers_automations_Manage, "click on Numbers_automations_Manage button").click();
		logger.info("click on Numbers_automations_Manage Button");
		implicitwaitAndLog(2);
		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

	}

	public void VCard_Automations_Fill_All_Details_With_SpecialCharacters() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.Vcard_NamePrefix).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_NamePrefix).sendKeys("Mr.@$#1213");
		logger.info("Text the Details of Vcard_NamePrefix ");

		textbox(vcf_Pages.Vcard_MiddleName).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_MiddleName).sendKeys("Auto1@!21");
		logger.info("Text the Details of Vcard_MiddleName ");

		textbox(vcf_Pages.Vcard_LastName).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_LastName).sendKeys("QA!@#!21");
		logger.info("Text the Details of Vcard_LastName ");

		textbox(vcf_Pages.Vcard_NameSuffix).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_NameSuffix).sendKeys("Sr.231!@#");
		logger.info("Text the Details of Vcard_NameSuffix");

		textbox(vcf_Pages.Vcard_CompanyName).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_CompanyName).sendKeys("Techno!#@1");
		logger.info("Text the Details of Vcard_CompanyName");

		textbox(vcf_Pages.Vcard_Description).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_Description).sendKeys("Automation Testing 1321!@#!@#");
		logger.info("Text the Details of Vcard_NamePrefix ");
		waitAndLog(2);

	}

	public void VCard_Automations_Fill_All_Details() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.Vcard_NamePrefix).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_NamePrefix).sendKeys("Mr.");
		logger.info("Text the Details of Vcard_NamePrefix ");

		textbox(vcf_Pages.Vcard_MiddleName).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_MiddleName).sendKeys("Auto1");
		logger.info("Text the Details of Vcard_MiddleName ");

		textbox(vcf_Pages.Vcard_LastName).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_LastName).sendKeys("QA");
		logger.info("Text the Details of Vcard_LastName ");

		textbox(vcf_Pages.Vcard_NameSuffix).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_NameSuffix).sendKeys("Sr.");
		logger.info("Text the Details of Vcard_NameSuffix");

		textbox(vcf_Pages.Vcard_Email).clear();
		logger.info("Clear the Details of Vcard_Email");
		textbox(vcf_Pages.Vcard_Email).sendKeys("auto1@yopmail.com");
		logger.info("Text the Details of Vcard_Email");

		textbox(vcf_Pages.Vcard_CompanyName).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_CompanyName).sendKeys("Techno");
		logger.info("Text the Details of Vcard_CompanyName");

		textbox(vcf_Pages.Vcard_WebsiteURL).clear();
		logger.info("Clear the Details of Vcard_WebsiteURL");
		textbox(vcf_Pages.Vcard_WebsiteURL).sendKeys("www.automation.com");
		logger.info("Text the Details of Vcard_WebsiteURL");

		textbox(vcf_Pages.Vcard_Description).clear();
		logger.info("Clear the Details of Vcard_NamePrefix ");
		textbox(vcf_Pages.Vcard_Description).sendKeys("Automation Testing");
		logger.info("Text the Details of Vcard_NamePrefix ");
		waitAndLog(2);

	}

	public void Vcard_Agent_Automations_Dummy_Account() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "agentautomation@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.settings, "click on settings button").click();
		logger.info("click on settings Button");
		waitAndLog(2);
		button(vcf_Pages.settings_Numbers, "click on settings_Numbers button").click();
		logger.info("click on settings_Numbers Button");
		waitAndLog(2);
		button(vcf_Pages.test_Manage, "click on test_Manage button").click();
		logger.info("click on test_Manage Button");
		waitAndLog(2);

		button(vcf_Pages.VCard, "click on VCard button").click();
		logger.info("click on VCard Button");
		waitAndLog(2);

	}

	public void VCard_Auto1_Automations_Contact() throws Exception {

		// username1 login
		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.Automation_Contact, "click on Automation_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waitAndLog(2);

	}

	public void VCF_nominee_Login_For_VCFOff_Cases() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("nominee1@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(5);

	}

	public void VCard_Automations_Compose_Auto1_Contact() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(8);

		button(vcf_Pages.composebutton, "click on composebutton button").click();
		logger.info("click on composebutton Button");
		waitAndLog(3);

		textbox(vcf_Pages.contactSearchInputTxt).sendKeys("Auto1");
		logger.info("text the Number - Automation");
		button(vcf_Pages.contactSearch_OP_Txt1, "click on contactSearch_OP_Txt1 button").click();
		logger.info("click on contactSearch_OP_Txt1 Button");
		waitAndLog(2);

	}

	public void AgentAutomation_Dummy_Agent_Account() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("automationdummy@yopmail.com");
		waitAndLog(2);
		logger.info("Email id :" + "automationdummy@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

	}

	public void AgentAutomation_Admin_Account() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("agentautomation@yopmail.com");
		logger.info("Email id :" + "automationdummy@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

	}

	public void auto1() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

	}

	public void AgentAutomation_Dummy_Agent_TestDept() throws Exception {

		waitAndLog(5);
		button(vcf_Pages.department_Head, "department_Head").click();
		logger.info("click on department_Head Button");
		waitAndLog(2);

		button(vcf_Pages.test_Dept, "test_Dept").click();
		logger.info("click on test_Dept Button");
		waitAndLog(2);

	}

	public void auto1_TollFree() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

	}

	public void selectTollFreeDept() throws Exception {

		waitAndLog(5);
		button(vcf_Pages.department_Head, "department_Head").click();
		logger.info("click on department_Head Button");
		waitAndLog(2);

		button(vcf_Pages.TollFree_Button_department, "TollFree_Button_department").click();
		logger.info("click on TollFree_Button_department Button");
		waitAndLog(2);

		button(vcf_Pages.TollFree_department, "TollFree_department").click();
		logger.info("click on TollFree_department Button");
		waitAndLog(2);

	}

	public void TollFreeDept_to_LocalDept() throws Exception {

		waitAndLog(5);
		button(vcf_Pages.department_Head, "department_Head").click();
		logger.info("click on department_Head Button");
		waitAndLog(2);

		button(vcf_Pages.LocalDept, "LocalDept").click();
		logger.info("click on LocalDept Button");
		waitAndLog(2);

		button(vcf_Pages.Auto1_Dept, "Auto1_Dept").click();
		logger.info("click on Auto1_Dept Button");
		waitAndLog(2);

	}

	public void auto1_to_TollFree_Dept() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(5);

		button(vcf_Pages.department_Head, "department_Head").click();
		logger.info("click on department_Head Button");
		waitAndLog(2);

		button(vcf_Pages.TollFree_Button_department, "TollFree_Button_department").click();
		logger.info("click on TollFree_Button_department Button");
		waitAndLog(2);

		button(vcf_Pages.TollFree_department, "TollFree_department").click();
		logger.info("click on TollFree_department Button");
		waitAndLog(2);

	}

	public void VCard_Automations_Auto1_Contact() throws Exception {

		waitAndLog(2);
		validateSignOutLink();
		validateSignOut();

		waitAndLog(2);

		textbox(vcf_Pages.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(vcf_Pages.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.Auto1_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Auto1_Contact Button");
		waitAndLog(2);

	}

	public void vcard_Icon_in_Conversation() throws Exception {

		button(vcf_Pages.Vcard_Icon, "click on Vcard_Icon button").click();
		logger.info("click on Vcard_Icon Button");
		waitAndLog(2);

	}

	public void settingsclose() throws Exception {

		waitAndLog(2);
		button(vcf_Pages.Settings_Close, "click on Settings_Close button").click();
		logger.info("click on Settings_Close Button");
		waitAndLog(2);

	}

	public void VCard_Message_Validation() throws Exception {

		waitAndLog(2);
		if (vcf_Pages.message.getText().equalsIgnoreCase("Hello Team")) {
			Assert.assertEquals(vcf_Pages.message.getText(), "Hello Team", "message is matching");
			logger.info("message is verified");
			logger.info(vcf_Pages.message.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(vcf_Pages.message.getText(), "Hello Team", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

	}

	public void gif_Validation() throws Exception {

		if (verifyElementIsEnabled(vcf_Pages.verification_Gif)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.verification_Gif));
			button(vcf_Pages.verification_Gif, "buton").isEnabled();
			logger.info("Gif Received");
			TestUtil.takeScreenshotAtEndOfTest("verification_Gif");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.verification_Gif));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("VCFReceivedSide");// ScreenShot capture
		}
	}

	public void Image_Validation() throws Exception {

		if (verifyElementIsEnabled(vcf_Pages.image_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.image_verification));
			button(vcf_Pages.image_verification, "buton").isEnabled();
			logger.info("Image Received");
			TestUtil.takeScreenshotAtEndOfTest("verification_Gif");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.image_verification));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("VCFReceivedSide");// ScreenShot capture
		}
	}

	public void Video_Validation() throws Exception {

		if (verifyElementIsEnabled(vcf_Pages.video_verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.video_verification));
			textbox(vcf_Pages.video_verification).isEnabled();
			logger.info("Video file Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.video_verification));
			logger.info("Video file not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void audio_File_Validation() throws Exception {

		if (verifyElementIsEnabled(vcf_Pages.audio_File_Verification)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.audio_File_Verification));
			textbox(vcf_Pages.audio_File_Verification).isEnabled();
			logger.info("audio_File_Verification Recevied");
			TestUtil.takeScreenshotAtEndOfTest("audio_File_Verification");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.audio_File_Verification));
			logger.info("audio_File_Verification not Recevied");
			TestUtil.takeScreenshotAtEndOfTest("audio_File_Verification");// ScreenShot capture
		}

	}

	public void VCard_Validation() throws Exception {

		waitAndLog(2);
		if (verifyElementIsEnabled(vcf_Pages.VCFReceivedSide)) {
			Assert.assertEquals(true, verifyElementIsEnabled(vcf_Pages.VCFReceivedSide));
			button(vcf_Pages.VCFReceivedSide, "buton").isEnabled();
			logger.info("VCard Received");
			TestUtil.takeScreenshotAtEndOfTest("VCFReceivedSide");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(vcf_Pages.VCFReceivedSide));
			logger.info(" Disabled");
			TestUtil.takeScreenshotAtEndOfTest("VCFReceivedSide");// ScreenShot capture
		}
		waitAndLog(2);
	}

	public void VCard_Auto1_Login_tools_Button() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(vcf_Pages.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);
	}

	public void VCard_Automationblock_Login_tools_Button() throws Exception {

		waitAndLog(2);
		textbox(vcf_Pages.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(vcf_Pages.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);

		button(vcf_Pages.loginbttn, "login submit").click();
		logger.info("click on Login Button");
		waitAndLog(2);

		button(vcf_Pages.tools_bttn, "click on tools_bttn ").click();
		logger.info("click on tools_bttn ");
		waitAndLog(2);
	}

	public void VCF_Automations_Delete() throws Exception {

		waitAndLog(2);
		button(vcf_Pages.tools_Automations, "Click on tools_Automations").click();
		logger.info("click on tools_Automations");
		waitAndLog(2);
		button(vcf_Pages.manage_1, "manage_1").click();
		logger.info("click on manage_1");
		waitAndLog(2);

		button(vcf_Pages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(vcf_Pages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
	}

	public void VCF_Tools_SM_DeleteSM() throws Exception {

		waitAndLog(2);
		button(vcf_Pages.Tools_SM, "Click on Tools_SM").click();
		logger.info("click on Tools_SM");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Upcoming, "Tools_SM_Upcoming").click();
		logger.info("click on Tools_SM_Upcoming");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage, "Tools_SM_Manage").click();
		logger.info("click on Tools_SM_Manage");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);
		button(vcf_Pages.Tools_SM_Manage_Delete, "Tools_SM_Manage_Delete").click();
		logger.info("click on Tools_SM_Manage_Delete");
		waitAndLog(2);
	}

	public void Click_on_QA10_Contact() throws Exception {

		button(vcf_Pages.QA10_Contact, "Click on QA10_Contact").click();
		logger.info("click on QA10_Contact");
		waitAndLog(2);

	}

	public void Click_on_Conv_SM_Increase_1Min_Time() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Conv_SM, "Conv_SM ").click();
		logger.info("click on Conv_SM Button");
		waitAndLog(2);

		button(vcf_Pages.increase_1Min_Time_in_Conv_SM, "Click on increase_1Min_Time_in_Conv_SM").click();
		logger.info("click on increase_1Min_Time_in_Conv_SM");
		waitAndLog(2);

		button(vcf_Pages.conv_SM_Time_Set_button, "conv_SM_Time_Set_button ").click();
		logger.info("click on conv_SM_Time_Set_button Button");
		waitAndLog(2);

	}

	public void Click_on_Conv_SM_Increase_2Min_Time() throws Exception {

		waitAndLog(2);

		button(vcf_Pages.Conv_SM, "Conv_SM ").click();
		logger.info("click on Conv_SM Button");
		waitAndLog(2);

		button(vcf_Pages.increase_1Min_Time_in_Conv_SM, "Click on increase_1Min_Time_in_Conv_SM").click();
		logger.info("click on increase_1Min_Time_in_Conv_SM");
		waitAndLog(1);
		button(vcf_Pages.increase_1Min_Time_in_Conv_SM, "Click on increase_1Min_Time_in_Conv_SM").click();
		logger.info("click on increase_1Min_Time_in_Conv_SM");
		waitAndLog(2);

		button(vcf_Pages.conv_SM_Time_Set_button, "conv_SM_Time_Set_button ").click();
		logger.info("click on conv_SM_Time_Set_button Button");
		waitAndLog(2);

	}

}
