package com.beetext.qa.WFCommonMethod;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Template;
import com.beetext.qa.util.TestUtil;

public class TemplatesWFCM extends CommonMethods {
	String url, username1, username2, password;
	Repository repository = new Repository();
	TestUtil testUtil = new TestUtil();
	Template template=new Template();
	TestBase testBase= new TestBase();
	public static String currentDir = System.getProperty("user.dir");
	
	
	public void logins() throws Exception {
		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id: automation@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password: tech1234");

		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		waits(5);
		logger.info("wait for 7 sec");
	}
	
	public void clickonTemplateIcon() throws Exception {
		button(template.temlate_icon, "button").click();
		logger.info("clicked on template icon");
		waits(2);
		logger.info("wait for 5 sec");

	}
	
	public void sendButton() throws Exception {
		button(repository.chatInputsendButton2, "button").click();
		logger.info("send button is clicked");
		wait(5);
		logger.info("wait for 5 sec");

	}
	
	public void logout() {
		validateSignOutLink();
		logger.info("clicked on three dots");
		validateSignOut();
		logger.info("clicked on sign out link");
	}
	
	public void selectConversation() throws Exception {
		button(template.select_conversations12, "button").click();
		logger.info("Conversation selected");
		wait(5);
		logger.info("wait for 5 sec");
	}
	public void closetoolspopups() throws Exception {
		   button(template.close_tools_popup, "login submit").click();
			logger.info("clicked on close tools popup link");
			waits(5);
			
	}

}
