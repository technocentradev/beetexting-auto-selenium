package com.beetext.qa.WFCommonMethod;

import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.WebEventListener;

public class Signup extends CommonMethods {
	// WebDriver driver;
	// public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	String org, workmail, confirmEmail, mobileNumber, retypePassword, otptext, Email_otptext, user_id,
			Passsword_ID;

	String Password = "Bee@1234";
	String Name = "Hyderabad";

	public static String currentDir = System.getProperty("user.dir");
	String url, username1, username2, password;

	public void Signup_login(Map<String, String> map) throws Exception {

		String org = CommonMethods.exportData("Org", 1);
		map.put("org", org);

		String Workmail = CommonMethods.exportData("WorkEmail", 1);
		map.put("Workmail", Workmail);

		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys(org);
		logger.info("enter the org name");

		textbox(Repository.workEmail).sendKeys(Workmail);
		logger.info("enter workmail");

		textbox(Repository.name).sendKeys(Name);
		logger.info("enter the name");

		textbox(Repository.signuppassword).sendKeys(Password);
		logger.info("enter the signup pasword");
		waitAndLog(2);
		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup checkbox");
		waitAndLog(2);
		button(Repository.letsgobtn, "button").click();
		logger.info("click on letsgo button");

	}

	public void Signup_login_orgnameAlreadyExist(Map<String, String> map) throws Exception {

		String org = CommonMethods.exportData("Org", 1);
		map.put("org", org);
		String Workmail = CommonMethods.exportData("WorkEmail", 2);
		map.put("Workmail", Workmail);

		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys(org);
		logger.info("enter the org name");

		textbox(Repository.workEmail).sendKeys(Workmail);
		logger.info("enter workmail");

		textbox(Repository.name).sendKeys(Name);
		logger.info("enter the name");

		textbox(Repository.signuppassword).sendKeys(Password);
		logger.info("enter the signup pasword");
		waitAndLog(2);
		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup checkbox");
		waitAndLog(2);

		button(Repository.letsgobtn, "button").click();
		logger.info("click on letsgo button");

	}

	public void Signup_login_emailAlreadyExist(Map<String, String> map) throws Exception {

		String org = CommonMethods.exportData("Org", 2);
		map.put("org", org);

		String Workmail = CommonMethods.exportData("WorkEmail", 1);
		map.put("Workmail", Workmail);

		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys(org);
		logger.info("enter the org name");

		textbox(Repository.workEmail).sendKeys(Workmail);
		logger.info("enter workmail");

		textbox(Repository.name).sendKeys(Name);
		logger.info("enter the name");

		textbox(Repository.signuppassword).sendKeys(Password);
		logger.info("enter the signup pasword");
		waitAndLog(2);
		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup checkbox");
		waitAndLog(2);

		button(Repository.letsgobtn, "button").click();
		logger.info("click on letsgo button");

	}

	public void Signup_login_allValid(Map<String, String> map) throws Exception {

		String org = CommonMethods.exportData("Org", 3);
		map.put("org", org);

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		textbox(Repository.orgName).sendKeys(org);
		logger.info("enter the org name");

		textbox(Repository.workEmail).sendKeys(Workmail);
		logger.info("enter workmail");

		textbox(Repository.name).sendKeys(Name);
		logger.info("enter the name");

		textbox(Repository.signuppassword).sendKeys(Password);
		logger.info("enter the signup pasword");
		waitAndLog(2);
		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup checkbox");
		waitAndLog(2);
		button(Repository.letsgobtn, "button").click();
		logger.info("click on letsgo button");

	}

	public void Signup_Randomlogin(Map<String, String> map) throws Exception {

		link(Repository.signupPage, "signuplink").click();
		logger.info("click on signup link");

		org = randomestring();
		logger.info("enter org");

		workmail = randomestring() + "@yopmail.com";
		logger.info("workmail");

		textbox(Repository.orgName).sendKeys(org);
		logger.info("enter org name");

		textbox(Repository.workEmail).sendKeys(workmail);
		logger.info("enter workmail");

		Repository.name.sendKeys(Name);
		logger.info("enter name");

		Repository.signuppassword.sendKeys(Password);
		logger.info("enter signup password");
		waitAndLog(2);
		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("click on signup checkbox");
		waitAndLog(2);
		button(Repository.letsgobtn, "button").click();
		logger.info("click on letsgo bttn");

	}

}
