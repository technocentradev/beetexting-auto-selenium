package com.beetext.qa.WFCommonMethod;

import com.beetext.qa.pages.ChangePassword;
import com.beetext.qa.pages.CommonMethods;

public class ChangeaPasswordWFCM extends CommonMethods {
	
	ChangePassword password=new ChangePassword();
	
	public void threedotLink() {
		password.threedots.click();
	}
	public void profileLink() {
		password.profile_link.click();
	}
	public void change_Password_Page() {
		password.change_password_link.click();
	}
	public void enterCurrent_password() {
		password.current_password.clear();
		password.current_password.sendKeys("tech12345");
	}
	public void enterCurrent_pass() {
		password.current_password.clear();
		password.current_password.sendKeys("tech1234");
	}
	public void enterNewPassword() {
		password.new_password.clear();
		password.new_password.sendKeys("tech1234");
	}
	public void enterNewPass() {
		password.new_password.clear();
		password.new_password.sendKeys("tech12345");
	}
	public void cofirm_new_Password() {
		password.confirm_new_password.clear();
		password.confirm_new_password.sendKeys("tech1234");
	}
	public void cofirm_new_Pass() {
		password.confirm_new_password.clear();
		password.confirm_new_password.sendKeys("tech12345");
	}
	public void enterCurrentPass_required() {
		password.current_password_required.getText();
	}
	public void enterCurrentPass_min_8char_validation() {
		password.current_password_min_8_char.getText();
	}
	
	public void newPass_required() {
		password.new_password_required.getText();
	}
	public void newPass_min_8char_validation() {
		password.new_password_min_8_char.getText();
	}
	public void confirm_New_Pass_min_8_char_required() {
		password.confirm_new_password_min_8_char.getText();
	}
	public void save_Button() {
		password.save_Button.click();
	}
}
