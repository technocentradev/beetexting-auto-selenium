package com.beetext.qa.WFCommonMethod;



import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Edit_contact;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.UploadNonMediaAttachment;
import com.beetext.qa.util.TestUtil;

public class EditContactWFCM extends CommonMethods {

	String url, username1, username2, password;
	TestUtil testUtil = new TestUtil();
	Edit_contact create = new Edit_contact();
	Schedule_Message sm = new Schedule_Message();
	UploadNonMediaAttachment pinMessage = new UploadNonMediaAttachment();
	TestBase testBase = new TestBase();

	

	public void selectconversations() throws Exception {
		logger.info("Trying to select conversation");
		button(create.select_conversation, "button").click();
		logger.info("selected conversation");
		wait(5);
		logger.info("wait for 5 sec");
		button(create.edit_contact_button, "button").click();
		logger.info("Contact info page , click on  edit icon ");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void selectconversation() throws Exception {
		logger.info("Trying to select conversation");
		button(create.select_conversation, "button").click();
		logger.info("selected conversation");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void searchQA123contact() throws Exception {
		waits(5);
		button(sm.click_compose_icon, "button").click();
		logger.info("Clicked on compose icon");
		wait(5);
		logger.info("wait for 5 sec");
		String mobile = "5553847984";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("compose new search box entering" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact4, "button").click();
		logger.info("selceted  contact ");
		wait(5);
		logger.info("wait for 5 sec");
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		button(pinMessage.postButton, "button").click();
		waits(3);
		button(create.edit_contact_button, "button").click();
		logger.info("Contact info page , click on  edit icon ");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void searchHellocontact() throws Exception {
		waits(5);
		button(sm.click_compose_icon, "button").click();
		logger.info("Clicked on compose icon");
		wait(5);
		logger.info("wait for 5 sec");
		String mobile = "hello";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("compose new search box entering" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact3, "button").click();
		logger.info("selceted  contact ");
		wait(5);
		logger.info("wait for 5 sec");
		String message = randomestring();
		textbox(pinMessage.input_box).sendKeys(message);
		wait(2);
		button(pinMessage.postButton, "button").click();
		waits(3);
		button(create.edit_contact_button, "button").click();
		logger.info("Contact info page , click on  edit icon ");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void select50tag_conversation() throws Exception {
		logger.info("Trying to select conversation");
		button(create.select_50tag_conversations, "button").click();
		logger.info("converation selected");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void savebutton() throws Exception {
		button(create.edit_save_button, "button").click();
		logger.info("Edit Contact page click on save button");
		wait(5);

	}

	public void editName() throws Exception {
		button(create.edit_contact_button, "button").click();
		logger.info("contact info page , click on edit icon");
		wait(5);
		textbox(create.edit_name).clear();
		logger.info("edit contact page , clearing name");
		wait(5);
		textbox(create.edit_name).sendKeys("QA123@$#%&*?_()");
		logger.info("edit contact page , Name entering QA123");
		wait(5);

		button(create.edit_save_button, "button").click();
		logger.info("edit contact page , click on save button");
		wait(5);

	}

	public void clearName() throws Exception {
		textbox(create.edit_name).clear();
		logger.info("Edit Contact name is clearing ");
		wait(2);
	}

	public void clearCompanyName() throws Exception {
		textbox(create.edit_company_name).clear();
		logger.info("Edit contact page , company name clearing");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void clickonaddtagTextbox() throws Exception {
		textbox(create.type_tag).click();
		logger.info("click on create tag text box");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void typeNote() throws Exception {
		textbox(create.type_note).click();
		logger.info("click on create note text box");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void clickoncrossmark() throws Exception {
		button(create.crossmark, "button").click();
		logger.info("click on cross mark");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void avatharlogo() throws Exception {
		button(create.edit_avathar_logo, "button").click();
		waits(5);
		logger.info("wait for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_1MB.jpg");
		logger.info("avathar logo giving valid format image");
		waits(20);
		logger.info("wait for 20 sec");
	}

	public void invalidavatharlogo() throws Exception {
		button(create.edit_avathar_logo, "button").click();
		logger.info("avathar icon is clicked");
		waits(5);
		logger.info("wait for 5 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info("invalid avathar is uploaded");
		waits(5);
	}

	public void addTagbutton() throws Exception {
		button(create.add_tag, "button").click();
		logger.info("click on add tag button");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void deletetag() throws Exception {

		button(create.confirm_delete_tag, "button").click();
		logger.info("click on confrom delete tag button ");
	}

	public void addTag() throws Exception {
		create.type_tag.click();
		logger.info("click on create tag text box");
		wait(2);
		create.type_tag.sendKeys("43");
		logger.info("create tag text box entering 43");
		waits(1);
		create.add_tag.click();
		logger.info("click on ADD button");
		waits(1);
		logger.info("wait for 5 sec");

	}

	public void addNoteButton() throws Exception {
		button(create.add_note, "button").click();
		logger.info("click on add note button");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void savenotebutton() throws Exception {
		button(create.save_note, "button").click();
		logger.info("click on save button");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void deleteNote() throws Exception {
		textbox(create.edit_note).click();
		logger.info("click on edit note text box");
		wait(5);
		logger.info("wait for 5 sec");

		button(create.click_delete_icon, "button").click();
		logger.info("click on note delete icon");
		wait(5);
		logger.info("wait for 5 sec");
		button(create.confirm_delete_note, "button").click();
		logger.info("click on note confirm delete icon");

	}

	public void addNote() throws Exception {
		textbox(create.type_note).click();
		logger.info("click on add note text box");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.type_note).sendKeys("helo");
		logger.info("click on note text box entered helo");
		wait(5);
		logger.info("wait for 5 sec");
		button(create.add_note, "button").click();
		logger.info("click on add note button");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void clearNote() throws Exception {
		textbox(create.edit_type_note).clear();
		logger.info("edit note text box entering helo");
		wait(5);
		logger.info("wait for 5 sec");
	}

}
