package com.beetext.qa.WFCommonMethod;

import java.util.Map;

import org.openqa.selenium.By;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Edit_contact;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.Template;
import com.beetext.qa.pages.UploadNonMediaAttachment;
import com.beetext.qa.util.TestUtil;

public class HealthscreenWFCM extends CommonMethods {

	String url, username1, username2, password;
	TestUtil testUtil = new TestUtil();
	Edit_contact create = new Edit_contact();
	Schedule_Message sm = new Schedule_Message();
	Template t = new Template();
	UploadNonMediaAttachment pinMessage = new UploadNonMediaAttachment();
	TestBase testBase = new TestBase();

	public void oneAgentLogin(Map<String, String> map) throws Exception {

		textbox(Repository.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id :" + username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password" + password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button clicked");
		waits(10);
		logger.info("wait for 10 sec");

		driver.findElement(By.xpath("//div[@id='hoverSelectDepartment']")).click();
		waits(5);
		driver.findElement(By.xpath("//span[@id='Health Screen_Department']")).click();
		waits(5);
	}

	public void auto1Login(Map<String, String> map) throws Exception {
		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("userid" + username1);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login bttn");
		waits(8);
		logger.info("wait for 8 sec");
	}

	public void automationsLogin(Map<String, String> map) throws Exception {
		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(5);
		logger.info("wait for 5 sec");
	}

	public void logout() {
		validateSignOutLink();
		logger.info("clicked on three dots");
		validateSignOut();
		logger.info("clicked on sign out link");
	}

	public void closetoolspopups() throws Exception {
		button(t.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup link");
		waits(5);

	}

	public void closesettingspopup() throws Exception {
		button(t.close_settings_popup, "login submit").click();
		logger.info("clicked on close tools popup link");
		waits(5);

	}
}
