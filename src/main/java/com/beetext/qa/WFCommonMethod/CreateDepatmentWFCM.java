package com.beetext.qa.WFCommonMethod;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.CreateDepartment;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class CreateDepatmentWFCM extends CommonMethods {
	String org, workmail, confirmEmail, name, mobileNumber, signuppassword, retypePassword;
	Repository repository = new Repository();
	TestUtil testUtil = new TestUtil();
	CreateDepartment createDept = new CreateDepartment();
	TestBase testBase= new TestBase();

    public void singuppagedetails() throws Exception {
    	wait(10);
		logger.info("wait for 10 sec");
		link(Repository.signupPage, "signuplink").click();
		logger.info("clicked on signup link");
		wait(10);
		logger.info("wait for 10 sec");
		String org = randomestring();
		wait(5);
		String workmail = randomestring() + "@yopmail.com";
		wait(5);
		String name = randomestring();
		wait(5);
		signuppassword = "tech1234";
		logger.info("password entererd:"+signuppassword);
		wait(5);
		logger.info("wait for 5 sec");
		textbox(Repository.orgName).sendKeys(org);
		logger.info("organization name enterd"+org);
		wait(5);
		logger.info("wait for 5 sec");
		textbox(Repository.workEmail).sendKeys(workmail);
		logger.info("Email Entered:"+workmail);
		wait(5);
		logger.info("wait for 5 sec");
		Repository.name.sendKeys(name);
		logger.info("agent name:"+name);
		wait(5);
		logger.info("wait for 5 sec");
		Repository.signuppassword.sendKeys(signuppassword);
		logger.info("password"+signuppassword);
		wait(5);
		logger.info("wait for 5 sec");

		checkbox(Repository.sighupcheckbox, "checkbox").click();
		logger.info("clicked on terms and conditions check box");

		wait(5);
		logger.info("wait for 5 sec");
		button(Repository.letsgobtn, "button").click();
		logger.info("clicked on letsgo button");
		waits(10);
		logger.info("wait for 30 sec");
    }
    
    public void phonenumberpage() throws Exception {
    	String mobile = "555" + randomeNum();
		textbox(Repository.mobileNumber).sendKeys(mobile);
		logger.info("Mobile number entered:"+mobile);
		wait(5);
		logger.info("wait for 5 sec");
		button(Repository.send_verfity_code, "button").click();
		logger.info("clicked on send verify code button");
		waits(5);
		logger.info("wait for 30 sec");
    }
    
    public void mobileotp() throws Exception {
    	waits(1);
    	Repository.mobile_otp1.sendKeys("1");
		waits(1);
		Repository.mobile_otp2.sendKeys("2");
		waits(1);
		Repository.mobile_otp3.sendKeys("3");
		waits(1);
		Repository.mobile_otp4.sendKeys("4");
		waits(1);
		Repository.mobile_otp5.sendKeys("5");
		waits(1);
		Repository.mobile_otp6.sendKeys("6");
		logger.info("otp entered:123456");
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.verify_bttn, "button").click();
		logger.info("Clicked on verify button");
		wait(30);
    }
    
    public void emailotp() throws Exception {
    	logger.info("wait for 10 sec");
    	waits(1);
		Repository.mobile_otp1.sendKeys("1");
		waits(1);
		Repository.mobile_otp2.sendKeys("2");
		waits(1);
		Repository.mobile_otp3.sendKeys("3");
		waits(1);
		Repository.mobile_otp4.sendKeys("4");
		wait(1);
		Repository.mobile_otp5.sendKeys("5");
		waits(1);
		Repository.mobile_otp6.sendKeys("6");
		logger.info("mobile otp entered:123456");
		waits(2);
		logger.info("wait for 2 sec");
    }
    
    public void creditcardDetails() throws Exception {
    	waits(5);
    	textbox(Repository.Cname).sendKeys("abcdefg");
		logger.info("Customer card name entered:abcdefg");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(Repository.CardNumber).sendKeys("4242424242424242");
		logger.info("card number entered:4242424242424242");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(Repository.ExpiryDate).sendKeys("0630");
		logger.info("ExpiryDate:0630");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(Repository.CC_Code).sendKeys("1234");
		logger.info("CC code entered: 1234");
		wait(5);
		logger.info("wait for 5 sec");
		checkbox(Repository.Accept_autopayments, "checkbox").click();
		logger.info("Accept autopayments radio button clicked");
		wait(5);
		logger.info("wait for 5 sec");
		button(Repository.Next_PaymentInformation, "button").click();
		logger.info("clicked on next button");
		wait(30);
		logger.info("wait for 30 sec");
    }
    
    public void createNewNumberButton() throws Exception {
    	button(createDept.createNewNum, "button").click();
		logger.info("clicked on create new number button");
		wait(30);
		logger.info("wait for 30 sec");

    }
    
    public void txtenabledNumber() throws Exception {
    	button(createDept.btnTxtEnableExisting, "button").click();
		wait(30);
		logger.info("wait for 30 sec");
    }
    
    public void phoneNumberinfoDetails() throws Exception {
    	String phone_Number = "555" + randomeNum();
		textbox(createDept.phoneNumber).sendKeys(phone_Number);
		wait(10);

		textbox(createDept.custName).sendKeys("sample");
		wait(10);

		String street = randomestring();

		textbox(createDept.street).sendKeys(street);
		wait(5);

		String city = randomestring();

		textbox(createDept.city).sendKeys(city);
		wait(5);

		textbox(createDept.zipcode).sendKeys("12345");
		wait(10);

		textbox(createDept.state).click();
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='existingNumberState']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Guam")) {

				allOptions.get(i).click();
				break;

			}
		}

		checkbox(createDept.radioButton, "checkbox").click();

		wait(5);
    }
    
    
    public void callmeButton() throws Exception {
    	button(createDept.callme_Button, "button").click();
    }
    
    public void verficationCode() throws Exception {
    	waits(5);
		logger.info("wait for 30 sec");
		Repository.mobile_otp1.sendKeys("1");
		waits(1);
		Repository.mobile_otp2.sendKeys("2");
		waits(1);
		Repository.mobile_otp3.sendKeys("3");
		waits(1);
		Repository.mobile_otp4.sendKeys("4");
		wait(1);
		Repository.mobile_otp5.sendKeys("5");
		waits(1);
		Repository.mobile_otp6.sendKeys("6");
		waits(2);
    }
    
    public void invalidOtp() throws Exception {
    	waits(1);
		Repository.mobile_otp1.sendKeys("1");
		waits(1);
		Repository.mobile_otp2.sendKeys("2");
		waits(1);
		Repository.mobile_otp3.sendKeys("3");
		waits(1);
		Repository.mobile_otp4.sendKeys("4");
		waits(1);

		Repository.mobile_otp5.sendKeys("5");
		waits(1);
		Repository.mobile_otp6.sendKeys("8");
		waits(1);
    }
    
    public void callmeagain() throws Exception {
    	button(createDept.callme_Again_Button, "button").click();
		waits(2);

    }
}
