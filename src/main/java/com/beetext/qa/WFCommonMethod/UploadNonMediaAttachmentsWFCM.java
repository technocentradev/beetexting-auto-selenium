package com.beetext.qa.WFCommonMethod;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.UploadNonMediaAttachment;
import com.beetext.qa.util.TestUtil;

public class UploadNonMediaAttachmentsWFCM extends CommonMethods {
	String url, username1, username2, password;
	Repository repository = new Repository();
	UploadNonMediaAttachment pinMessage= new UploadNonMediaAttachment();
	TestUtil testUtil = new TestUtil();
	public static String currentDir = System.getProperty("user.dir");
	String message = randomestring();
	
	public void login() throws Exception {
		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		wait(2);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		wait(2);
		button(Repository.loginbttn, "login submit").submit();
		waits(5);
		
		button(pinMessage.select_conversations12, "button").click();
		waits(2);
		// System.out.println( repository.select_conversation.getText() );
	}
	
	
	public void login2() throws Exception {
		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		wait(2);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		wait(2);
		button(Repository.loginbttn, "login submit").submit();
		waits(5);
	}
	
	public void login2conversation() throws Exception {
		button(pinMessage.select_conversations, "button").click();
		waits(2);
	}
	public void randomMessage() throws Exception {
		
		
	}
	
	public void attachmenticon() throws Exception {
		button(repository.pin_attachment, "file attachment").click();
		waits(5);
	}

	public void sendButton() throws Exception {
		button(pinMessage.postButton, "button").click();
		waits(3);

	}
	
	public void logout() {
		validateSignOutLink();
		validateSignOut();
	}
}
