package com.beetext.qa.WFCommonMethod;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.SettingsAgents;
import com.beetext.qa.util.TestUtil;

public class SettingsAgentinviteWFCM extends CommonMethods {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	SettingsAgents agent = new SettingsAgents();
	String agentName = randomestring() + "@yopmail.com";
	public static String currentDir = System.getProperty("user.dir");

	public void automations_login() throws Exception {
		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password :" + password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void auto2_login() throws Exception {
		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id :" + username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password :" + password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void login() throws Exception {
		button(agent.agent_link, "login submit").click();
		logger.info("agents link is clicked");
		wait(5);
		logger.info("wait for 10 sec");
		button(agent.add_agent_btn, "login submit").click();
		logger.info("clicked on add agent button");
		wait(5);
		logger.info("wait for 10 sec");
	}

	public void agent_links() throws Exception {
		button(agent.agent_link, "login submit").click();
		logger.info("agents link is clicked");
		wait(5);
		logger.info("wait for 10 sec");
	}

	public void selectrole() throws Exception {
		textbox(agent.agent_role).click();
		logger.info("clicked on agent role text box");
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='manageAgentRole']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("ADMIN")) {

				allOptions.get(i).click();
				break;

			}
		}

		wait(5);
		logger.info("wait for 5 sec");
	}

	public void selectuserrole() throws Exception {
		textbox(agent.agent_role).click();
		logger.info("clicked on agent role text box");
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='manageAgentRole']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("USER")) {

				allOptions.get(i).click();
				break;

			}
		}

		wait(5);
		logger.info("wait for 5 sec");
	}

	public void selectUserrole() throws Exception {
		textbox(agent.agent_role).click();
		logger.info("clicked on agent role text box");
		wait(5);

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='manageAgentRole']//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("USER")) {

				allOptions.get(i).click();
				break;

			}
		}

		waits(3);
		logger.info("wait for 5 sec");
	}

	public void clickonsendButton() throws Exception {
		button(agent.send_button, "login submit").click();
		logger.info("clicked on send button");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void logins() throws Exception {
		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id :" + username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password :" + password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void deleteConfirmagent() throws Exception {
		button(agent.Delete_agent, "login submit").click();

		button(agent.Confirm_Delete_agent, "login submit").click();
		logger.info("clicked on confirm delete  button");
	}

	public void manageButton() throws Exception {
		button(agent.manage_btn, "login submit").click();
		logger.info("clicked on Manage button");
	}

	public void deleteagenttoaster() {
		logger.info(agent.Delete_Toaster.getText());
		if (agent.Delete_Toaster.getText().equalsIgnoreCase("sampleagent has been deleted from the organization.")) {
			Assert.assertEquals(agent.Delete_Toaster.getText(), "sampleagent has been deleted from the organization.");
			logger.info("sampleagent has been deleted from the organization.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.Delete_Toaster.getText(),
					"sampleagent has been deleted from the organization.",
					"sampleagent has been deleted from the organization.");
			logger.info("sampleagent has been deleted from the organization.");
			TestUtil.takeScreenshotAtEndOfTest("sampleagent has been deleted from the organization..");// ScreenShot
																										// capture
		}

	}

	public void agentLink() throws Exception {
		button(agent.agent_link, "login submit").click();
		logger.info("clicked on agent link");
	}

	public void sampleAgent() throws Exception {
		textbox(agent.agent_name).sendKeys("sampleagent");
		logger.info("agent name : sampleagent");

		String agentName = randomestring() + "@yopmail.com";
		textbox(agent.agent_email).sendKeys(agentName);
		logger.info("Email: auto9999@yopmail.com");
		selectrole();

		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected radio button");
		clickonsendButton();
	}

	public void sampleAgents() throws Exception {
		waits(2);
		textbox(agent.agent_name).sendKeys("sampleagent");
		logger.info("agent name : sampleagent");

		String agentName = "sampleagent@yopmail.com";
		textbox(agent.agent_email).sendKeys(agentName);
		logger.info("Email:sampleagent@yopmail.com ");
		selectrole();

		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected radio button");
		clickonsendButton();
	}

	public void usersampleAgents() throws Exception {
		textbox(agent.agent_name).sendKeys("sampleagent");
		logger.info("agent name : sampleagent");

		String agentName = "sampleagent@yopmail.com";
		textbox(agent.agent_email).sendKeys(agentName);
		logger.info("Email:sampleagent@yopmail.com ");
		selectuserrole();
		button(agent.select_auto1_dept, "login submit").click();
		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected radio button");
		clickonsendButton();
	}

	public void automations_usersampleAgents() throws Exception {
		textbox(agent.agent_name).sendKeys("sampleagent");
		logger.info("agent name : sampleagent");

		String agentName = "sampleagent@yopmail.com";
		textbox(agent.agent_email).sendKeys(agentName);
		logger.info("Email:sampleagent@yopmail.com ");
		selectuserrole();
		button(agent.select_Automations_dept, "login submit").click();
		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected radio button");
		clickonsendButton();
	}

	public void sampleagentinvitetoastermessage() {
		logger.info(agent.agent_invite_toaster.getText());
		if (agent.agent_invite_toaster.getText().equalsIgnoreCase("Your invitation to sampleagent has been sent.")) {
			Assert.assertEquals(agent.agent_invite_toaster.getText(), "Your invitation to sampleagent has been sent.");
			logger.info("Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.agent_invite_toaster.getText(),
					"Your invitation to sampleagent has been sent.", "Your invitation to sampleagent has been sent.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture
		}
	}

	public void sampleagentdeletemessage() {
		logger.info(agent.Delete_Toaster.getText());
		if (agent.Delete_Toaster.getText().equalsIgnoreCase("sampleagent has been deleted from the organization.")) {
			Assert.assertEquals(agent.Delete_Toaster.getText(), "sampleagent has been deleted from the organization.");
			logger.info("sampleagent has been deleted from the organization.");
			TestUtil.takeScreenshotAtEndOfTest("Your invitation to sampleagent has been sent.");// ScreenShot
																								// capture

		} else {
			Assert.assertNotEquals(agent.Delete_Toaster.getText(),
					"sampleagent has been deleted from the organization.",
					"sampleagent has been deleted from the organization.");
			logger.info("sampleagent has been deleted from the organization.");
			TestUtil.takeScreenshotAtEndOfTest("sampleagent has been deleted from the organization..");// ScreenShot
																										// capture
		}
	}

	public void sentsampleagentinvitation() throws Exception {
		textbox(agent.agent_name).sendKeys("sampleagent");
		logger.info("agent name : sampleagent");
		textbox(agent.agent_email).sendKeys(agentName);
		logger.info("Email: " + agentName);

		selectUserrole();
		button(agent.select_auto1_dept, "login submit").click();
		logger.info("Auto1 department is selected");

		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected radio button");
		clickonsendButton();
	}

	public void agentName() throws Exception {
		textbox(agent.email).sendKeys(agentName);
		logger.info("yopmailmail Entered: auto9999@yopmail.com");
	}

	public void addsameagentName() throws Exception {
		textbox(agent.email).sendKeys("auto14");
		logger.info("yopmailmail Entered: auto9999@yopmail.com");
	}

	public void settingsagentslink() throws Exception {
		button(agent.Settings_link, "login submit").click();
		logger.info("clicked on settings link");
		waits(10);
		logger.info("wait for 10 sec");
		button(agent.agent_link, "login submit").click();
		logger.info("clicked on agent link");
		waits(5);
		logger.info("wait for 10 sec");
	}

	public void enteralreadyexistingemail() throws Exception {
		textbox(agent.agent_name).sendKeys("sample");
		logger.info("agent name typed: sample");
		textbox(agent.agent_email).sendKeys("auto2@yopmail.com");
		logger.info("email text box typed: auto2@yopmail.com");
		selectrole();

		button(agent.emailradiobutton, "login submit").click();
		logger.info("selected the email option");
		clickonsendButton();
	}
}
