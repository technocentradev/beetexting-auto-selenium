package com.beetext.qa.WFCommonMethod;

import org.testng.Assert;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Opt_out_pages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Template;
import com.beetext.qa.util.TestUtil;

public class Opt_out_WFCM extends CommonMethods {

	String url, username1, username2, password;
	Repository repository = new Repository();
	TestBase testBase = new TestBase();
	Template template=new Template();
	Opt_out_pages Optoutpages = new Opt_out_pages();
	
	
	public void auto1Login() throws Exception {
		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + username2);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waits(1);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(3);
		logger.info("wait for 5 sec");

	}
	
	public void automationLogin() throws Exception {
		
		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waits(1);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(3);
		logger.info("wait for 5 sec");

	}
	public void autocontact() throws Exception {
		button(Optoutpages.autoContact, "Click on autoContact").click();
		logger.info("autoContact clicked");
		waits(1);
		logger.info("wait for 2 sec");
	}
	
	public void sendStopText() throws Exception {
		 textbox(repository.chatInputTextareaTxt).sendKeys("Stop");
			logger.info("stop");
			waits(2);
			logger.info("wait for 2 sec");
	}
	
	public void subscribeText() throws Exception {
		textbox(repository.chatInputTextareaTxt).sendKeys("Subscribe");
		logger.info("stop");
		waits(2);
		logger.info("wait for 2 sec");
	}
	
	public void startText() throws Exception {
		textbox(repository.chatInputTextareaTxt).sendKeys("Start");
		logger.info("stop");
		waits(2);
		logger.info("wait for 2 sec");
	}
	public void sendButton() throws Exception {
		JSClick(repository.chatInputsendButton);
		logger.info("click on send");
		waits(2);
		logger.info("wait for 5 sec");

	}
	
	public void disabledToasterMessage() {
		if (repository.message.getText().equalsIgnoreCase(
				"You have successfully disabled text messages from Technocentra. Reply START to receive messages again.")) {
			Assert.assertEquals(repository.message.getText(),
					"You have successfully disabled text messages from Technocentra. Reply START to receive messages again.",
					"message is matching");
			System.out.println(repository.message.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(),
					"You have successfully disabled text messages from Technocentra. Reply START to receive messages again.",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
	}
	
	
	public void enabledToasterMessage() {
		if (repository.message.getText().equalsIgnoreCase(
				"You have successfully enabled text messages from Technocentra. Text back STOP to disable text messages again.")) {
			Assert.assertEquals(repository.message.getText(),
					"You have successfully enabled text messages from Technocentra. Text back STOP to disable text messages again.",
					"message is matching");
			System.out.println(repository.message.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(),
					"You have successfully enabled text messages from Technocentra. Text back STOP to disable text messages again.",
					"message is not  matching");
			System.out.println("failed");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
	}
	
	public void optMessage() throws Exception {
		textbox(Optoutpages.message).getText();
		textbox(Optoutpages.message1).getText();
		textbox(Optoutpages.message2).getText();
		textbox(Optoutpages.message3).getText();
	}
	public void closetoolspopups() throws Exception {
		   button(template.close_tools_popup, "login submit").click();
			logger.info("clicked on close tools popup link");
			waits(2);
			
	}

}
