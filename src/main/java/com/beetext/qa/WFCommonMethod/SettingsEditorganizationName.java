package com.beetext.qa.WFCommonMethod;

import org.testng.Assert;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.SettingsOrganizationNameEdit;
import com.beetext.qa.util.TestUtil;

public class SettingsEditorganizationName extends CommonMethods {
	String url, username1, username2, password;
	Repository repository = new Repository();
	TestBase testBase =new TestBase();
	SettingsOrganizationNameEdit editOrg=new SettingsOrganizationNameEdit();
	
	
	/*public void login() throws Exception {
		username1 = (String) map.get("username1");
		password = (String) map.get("password");
		System.out.println("username1-->" + username1);
		wait(5);
		textbox(repository.user_id).sendKeys(username1);
		logger.info("Email:"+username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(repository.Passsword_ID).sendKeys(password);
		logger.info("Password:"+password);
		wait(2);
		logger.info("wait for 2 sec");
		button(repository.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		wait(10);
		logger.info("wait for 10 sec");
	}*/
	
	public void settingsLink() throws Exception {
		button(editOrg.Settings_link,"button").click();
		logger.info("Settings link is clicked");
		waits(10);
		logger.info("wait for 10 sec");
	}
	
	public void editorganizationLink() throws Exception {
		button(editOrg.organization_link,"button").click();
		logger.info("organization link is clicked");
		waits(5);
		logger.info("wait for 2 sec");
	}
	
	public void clearorganizationname() throws Exception {
		textbox(editOrg.organization_name_edit).clear();
		logger.info("clearing the organization name");
		wait(2);
		logger.info("wait for 2 sec");
	}
	public void threecharrequired() {
		if (editOrg.organization_name_min_3char.getText()
				.equalsIgnoreCase("Organization Name must be at least 3 characters.")) {
			Assert.assertEquals(editOrg.organization_name_min_3char.getText(),
					"Organization Name must be at least 3 characters.",
					"Organization Name must be at least 3 characters.");
			logger.info("Organization Name must be at least 3 characters.");
			TestUtil.takeScreenshotAtEndOfTest("Settings organization Name min 3 chars validation checking");// ScreenShot
																												// capture

		} else {
			Assert.assertNotEquals(editOrg.organization_name_min_3char.getText(),
					"Organization Name must be at least 3 characters.",
					"Organization Name must be at least 3 characters.");
			logger.info("Organization Name Min 3 characters validation case field");
			TestUtil.takeScreenshotAtEndOfTest("SettingsOrganization Name Min 3 characters validation case");// ScreenShot
																												// capture
		}
	}
	public void saveButton() throws Exception {
		button(editOrg.saveButton, "login submit").submit();
		logger.info("Save button is clicked");
	}
}
