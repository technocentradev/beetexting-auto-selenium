package com.beetext.qa.WFCommonMethod;

import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.ToolsTemplate;
import com.beetext.qa.util.TestUtil;

public class ToolsSharedTemplateWFCM extends CommonMethods {
	String url, username1, username2, password;
	Repository repository = new Repository();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	ToolsTemplate template = new ToolsTemplate();

	/*
	 * public void login() throws Exception {
	 * 
	 * textbox(repository.user_id).sendKeys(username1); logger.info("Email id :" +
	 * username1); wait(2); logger.info("wait for 2 sec");
	 * textbox(repository.Passsword_ID).sendKeys(password); logger.info("Password :"
	 * + password); wait(2); logger.info("wait for 2 sec");
	 * button(repository.loginbttn, "login submit").submit();
	 * logger.info("Login button is clicked"); waits(3);
	 * logger.info("wait for 2 min"); }
	 */
	public void logins() throws Exception {

		textbox(template.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id :" + username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(template.Passsword_ID).sendKeys("tech1234");
		logger.info("Password :" + password);
		wait(2);
		logger.info("wait for 2 sec");
		button(template.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		waits(3);
		logger.info("wait for 2 min");
	}

	public void toolslink() throws Exception {
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		waits(5);
		logger.info("wait for 10 sec");
	}

	public void openTemplatepages() throws Exception {
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		waits(5);
		logger.info("wait for 10 sec");
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(3);
		logger.info("wait for 10 sec");

	}

	public void openTemplatepage() throws Exception {
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(3);
		logger.info("wait for 10 sec");

		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on add template button");
		waits(2);
		logger.info("wait for 10 sec");
	}

	public void openaddTemplatePage() throws Exception {
		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on add template button");
		waits(2);
		logger.info("wait for 10 sec");

	}

	public void enterTemplatTitle() throws Exception {
		textbox(template.Template_title).sendKeys("shared");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void enterTemplatTitle2char() throws Exception {
		textbox(template.Template_title).sendKeys("sh");
		logger.info("Title: shared");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void enterTemplateMessage() throws Exception {
		textbox(template.Template_message).sendKeys("shared template creation checking");
		logger.info("Message: shared template creation checking");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void selectsharedoption() throws Exception {
		button(template.template_shared_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(3);
		logger.info("wait for 5 sec");
	}

	public void selectpersonaloption() throws Exception {
		button(template.template_personal_option, "login submit").click();
		logger.info("clicked on shared button");
		waits(3);
		logger.info("wait for 5 sec");
	}

	public void selectonlyoneDept() throws Exception {
		button(template.select_specifif_department, "login submit").click();
		logger.info("clicked on select one departments ");
		waits(2);
		logger.info("wait for 5 sec");

	}

	public void selectallDept() throws Exception {
		button(template.selectall_departments, "login submit").click();
		logger.info("clicked on select all departments ");
		waits(2);
		logger.info("wait for 5 sec");
	}

	public void clickoncreateButton() throws Exception {
		button(template.create_Template_btn, "login submit").click();
		logger.info("clicked on create template button");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void drafttemplateToastermessage() throws Exception {
		button(template.draft_template_toaster_messages, "login submit").click();
		logger.info("draft toaster message validated");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void createTemplateToastermessage() {
		System.out.println(template.shared_template_toaster_message.getText());
		if (template.shared_template_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been created.")) {
			Assert.assertEquals(template.shared_template_toaster_message.getText(),
					"Your template shared has been created.");
			System.out.println("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_toaster_message.getText(),
					"Your template personal has been created.", "Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

	}

	public void notitledraftemplate() throws Exception {
		button(template.Manage_draft_nodata, "login submit").click();
		logger.info("clicked on manage template button");
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void selectpersonaltab() throws Exception {
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(2);
		logger.info("wait for 10 sec");
	}

	public void selectsharedtab() throws Exception {
		button(template.shared_tab, "login submit").click();
		logger.info("clicked on shared tab");
		waits(2);
		logger.info("wait for 10 sec");
	}

	public void selectDrafttab() throws Exception {
		button(template.draft_tab, "login submit").click();
		logger.info("clicked on draft tab");
		waits(2);
		logger.info("wait for 10 sec");
	}

	public void manageSharedTemplate() throws Exception {
		button(template.Manage_shared_template, "login submit").click();
		logger.info("clicked on manage template button");
		waits(3);
		logger.info("wait for 10 sec");
	}

	public void manageSharedTemplate2char() throws Exception {
		button(template.Manage_shared_templates, "login submit").click();
		logger.info("clicked on manage template button");
		waits(3);
		logger.info("wait for 10 sec");
	}

	public void clickondeletetemplateButton() throws Exception {
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		waits(3);
		logger.info("wait for 5 sec");

	}

	public void clickonconfirmdelete_template() throws Exception {
		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
		waits(1);
	}

	public void deleteTemplateToastermessage() {

		System.out.println(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			System.out.println("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}
	}

	public void notitletemplatedeltetoastermessage() {
		System.out.println(template.no_draft_template_delete_toaster_messages.getText());
		if (template.no_draft_template_delete_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been deleted.")) {
			Assert.assertEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template has been deleted.");
			System.out.println("Your template has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.no_draft_template_delete_toaster_messages.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been deleted.'");// ScreenShot
																								// capture
		}
	}

	public void addAttachments() throws Exception {
		button(repository.pin_attachment, "file attachment").click();
		waits(7);

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");

		waits(20);
	}

	public void addGIFAttachments() throws Exception {
		button(template.attachment_Gif_link, "file attachment").click();
		waits(7);
		button(template.select_Gif_attachment, "file attachment").click();
		waits(30);
	}

	public void emoji() throws Exception {
		button(template.emoji_link, "file attachment").click();
		waits(5);
		button(template.emoji_link_icon, "file attachment").click();
		waits(3);
	}

	public void saveDraft() throws Exception {
		button(template.save_draft, "login submit").click();
		logger.info("clicked on savedraft template button");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void draftToastermessage() {
		System.out.println(template.draft_template_toaster_message.getText());
		if (template.draft_template_toaster_message.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.");
			System.out.println("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_message.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}
	}

	public void closetoolspopup() throws Exception {
		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup link");
		waits(5);

	}

	public void savedraft() throws Exception {
		button(template.savedraft_button, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);

	}

	public void clicktoolslink() throws Exception {
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void opentemplatepage() throws Exception {
		waits(5);
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void discardbuttons() throws Exception {
		button(template.discard_button, "login submit").click();
		logger.info("clicked on template link");
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void discardbutton() throws Exception {
		System.out.println("button is enbaled");
		if (verifyElementIsEnabled(template.discard_draft)) {
			button(template.discard_draft, "button").click();
			System.out.println("Discard button is clicked");

			TestUtil.takeScreenshotAtEndOfTest("discard button is clliked");// ScreenShot
																			// capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(template.discard_draft));
			System.out.println("TestEnable Existing button  is disbaled");
			TestUtil.takeScreenshotAtEndOfTest("discard button is");// ScreenShot
																	// capture
		}

	}

	public void breadscrumlink() throws Exception {
		button(template.breadscrumlink, "login submit").click();
		logger.info("clicked on breadscrum link");
		waits(5);
	}

	public void breadscrumsavedraft() throws Exception {
		button(template.save_draft, "login submit").click();
		logger.info("clicked on save draft button");
		wait(2);
	}

	public void breadscrumtoastermessage() {
		System.out.println(template.draft_template_toaster_messages.getText());
		if (template.draft_template_toaster_messages.getText()
				.equalsIgnoreCase("Your template has been saved as a draft.")) {
			Assert.assertEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.");
			System.out.println("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.draft_template_toaster_messages.getText(),
					"Your template has been saved as a draft.", "Your template has been saved as a draft.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

	}

	public void tagslink() throws Exception {
		button(template.tags_link, "login submit").click();
		logger.info("clicked on tags link");
		waits(3);
	}

	public void templatelink() throws Exception {

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link tab");
		waits(3);
		logger.info("wait for 10 sec");
	}

	public void clearTitle() throws Exception {
		/*
		 * String s = Keys.chord(Keys.CONTROL, "a");
		 * template.Template_title.sendKeys(s); // sending DELETE key
		 * template.Template_title.sendKeys(Keys.DELETE);
		 * 
		 * logger.info("title  is cleared"); wait(2); logger.info("wait for 2 sec");
		 * String s2 = Keys.chord(Keys.CONTROL, "a");
		 * template.Template_message.sendKeys(s2); // sending DELETE key
		 * template.Template_message.sendKeys(Keys.DELETE);
		 * 
		 * logger.info("message  is cleared"); wait(2); logger.info("wait for 2 sec");
		 */

		template.Template_title.sendKeys(Keys.CONTROL + "a");
		template.Template_title.sendKeys(Keys.DELETE);
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);
	}

	public void clearMessage() throws Exception {
		/*
		 * String s2 = Keys.chord(Keys.CONTROL, "a");
		 * template.Template_message.sendKeys(s2); // sending DELETE key
		 * template.Template_message.sendKeys(Keys.DELETE);
		 * 
		 * logger.info("message  is cleared"); wait(2); logger.info("wait for 2 sec");
		 */
		template.Template_message.sendKeys(Keys.CONTROL + "a");
		template.Template_message.sendKeys(Keys.DELETE);

	}

	public void updateTemplate() throws Exception {
		waits(10);
		button(template.update_template, "login submit").click();
		logger.info("clicked on update template button");
		wait(10);
		logger.info("wait for 10 sec");
		wait(10);
	}

	public void addseconddept() throws Exception {
		button(template.select_2nd_department, "login submit").click();
		logger.info("clicked on select 2nd department  button");
		waits(2);
		logger.info("wait for 10 sec");
	}

	public void personaltab() throws Exception {
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on select personal tab");
		waits(3);
		logger.info("wait for 10 sec");

	}

	public void savebutton() throws Exception {
		button(template.save_button, "login submit").click();
		logger.info("clicked on save  button");
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void deleteSharedtemplatetoastermessage() {

		System.out.println(template.shared_template_delete_toaster_message.getText());
		if (template.shared_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template shared has been deleted.")) {
			Assert.assertEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.");
			System.out.println("Your template personal has been created.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been created.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.shared_template_delete_toaster_message.getText(),
					"Your template shared has been deleted.", "Your template shared has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("'Your template personal has been created.");// ScreenShot
																							// capture
		}

	}

	public void clickondeleteButton() throws Exception {
		waits(1);
		button(template.deletebutton, "login submit").click();
		logger.info("clicked on delete  button");
		wait(10);
		logger.info("wait for 10 sec");
	}

	public void logout() {
		validateSignOutLink();
		logger.info("clicked on three dots");
		validateSignOut();
		logger.info("clicked on sign out link");
	}
}
