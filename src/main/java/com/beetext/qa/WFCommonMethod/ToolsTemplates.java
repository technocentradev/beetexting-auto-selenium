package com.beetext.qa.WFCommonMethod;

import org.testng.Assert;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.pages.ToolsTemplate;
import com.beetext.qa.util.TestUtil;

public class ToolsTemplates extends CommonMethods {

	String url, username1, username2, password;
	Repository repository = new Repository();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	ToolsTemplate template = new ToolsTemplate();
	public static String currentDir = System.getProperty("user.dir");

	public void opencreatetemplatepage() throws Exception {

		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(2);
		logger.info("wait for 10 sec");
		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on  add template button");
		waits(3);
		logger.info("wait for 10 sec");
	}

	public void toolslink() throws Exception {
		button(template.tools_link, "login submit").click();
		logger.info("clicked on tools link");
		waits(3);
		logger.info("wait for 10 sec");
	}

	public void login() throws Exception {
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(2);
		logger.info("wait for 10 sec");
		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on  add template button");
		waits(3);
		logger.info("wait for 10 sec");
	}

	public void templatelink() throws Exception {
		button(template.templateLink, "login submit").click();
		logger.info("clicked on template link");
		waits(2);
		logger.info("wait for 10 sec");

	}

	public void addtemplate() throws Exception {
		button(template.addTemplate_btn, "login submit").click();
		logger.info("clicked on  add template button");
		waits(3);
		logger.info("wait for 10 sec");
	}

	public void managepersonal() throws Exception {
		button(template.personal_tab, "login submit").click();
		logger.info("clicked on manage template button");
		wait(2);
		logger.info("wait for 2 sec");
		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on manage template button");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void managepersonaltemp() throws Exception {
		button(template.Manage_personal_template, "login submit").click();
		logger.info("clicked on manage template button");
		wait(2);
		logger.info("wait for 2 sec");
	}

	

	public void logout() {
		validateSignOutLink();
		logger.info("clicked on three dots");
		validateSignOut();
		logger.info("clicked on sign out link");
	}

	public void closetoolspopup() throws Exception {
		button(template.close_tools_popup, "login submit").click();
		logger.info("clicked on close tools popup link");
		waits(5);

	}
	public void deleteconfirmdelete() throws Exception {
		button(template.delete_template, "login submit").click();
		logger.info("clicked on delete button");
		wait(5);
		logger.info("wait for 5 sec");

		button(template.confirm_delete_template, "login submit").click();
		logger.info("clicked on confirm delete template button");
	}
	public void deleteToastermessage() {
		logger.info(template.personal_template_delete_toaster_message.getText());
		if (template.personal_template_delete_toaster_message.getText()
				.equalsIgnoreCase("Your template personal has been deleted.")) {
			Assert.assertEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.");
			logger.info("Your template personal has been deleted.");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deleted.");// ScreenShot
																							// capture

		} else {
			Assert.assertNotEquals(template.personal_template_delete_toaster_message.getText(),
					"Your template personal has been deleted.", "Your template personal has been deletion failed");
			TestUtil.takeScreenshotAtEndOfTest("Your template personal has been deletion failed");// ScreenShot
																									// capture
		}
	}
	
	public void draftTab() throws Exception {
		button(template.draft_tab, "login submit").click();
		logger.info("Draft tab is  clicked");
		waits(5);
		logger.info("wait for 10 sec");
	}
}
