package com.beetext.qa.WFCommonMethod;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Create_Contact;
import com.beetext.qa.pages.Repository;

public class CreateContactWFCM extends CommonMethods {
	Repository repository = new Repository();

	Create_Contact create = new Create_Contact();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void CreatecontatCode() throws Exception {
		String name = randomestring();
		wait(5);
		logger.info("wait for 5 sec");
		String mobileNumber = "555" + randomeNum();
		wait(5);
		String email = randomestring() + "@yopmail.com";
		logger.info("Email id:" + email);
		wait(5);
		logger.info("wait for 5 sec");

		button(create.create_contact_Link, "button").click();
		logger.info("creaate contact link is clicked");
		wait(5);
		logger.info("wait for 5 sec");

		button(create.create_contact_icon, "button").click();
		logger.info("create contact icon is clicked");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contact_Name).sendKeys(name);
		logger.info("Contact name Entered:" + name);
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contact_phonenum).sendKeys(mobileNumber);
		logger.info("Mobile number entered" + mobileNumber);
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contac_Email).sendKeys(email);
		logger.info("Email entered:" + email);
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void openCreateContact() throws Exception {

		button(create.create_contact_Link, "button").click();
		logger.info("Contact link is clicked");
		wait(5);
		logger.info("wait for 5 sec");

		button(create.create_contact_icon, "button").click();
		logger.info("Clicked on create contact icon");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void enterEmailandPhonenumber() throws Exception {
		waits(2);
		String mobileNumber = "555" + randomeNum();
		wait(5);
		String email = randomestring() + "@yopmail.com";
		wait(5);
		textbox(create.create_contact_phonenum).sendKeys(mobileNumber);
		logger.info("Mobile number :" + mobileNumber);
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contac_Email).sendKeys(email);
		logger.info("Email:" + email);
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void onlyMobileNumber() throws Exception {
		String mobileNumber = "555" + randomeNum();
		wait(5);

		textbox(create.create_contact_phonenum).sendKeys(mobileNumber);
		logger.info("Mobile number :" + mobileNumber);
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void onlyName() throws Exception {
		waits(2);
		textbox(create.create_contact_Name).sendKeys("sample");
		logger.info("Name:sample");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void comapanynameandTagsAndNotes() throws Exception {
		textbox(create.create_contac_companyName).sendKeys("technocentra");
		logger.info("company Name: technocentra");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contac_tags).sendKeys(
				"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,123456789012345678901234567890,@$%&*/-");
		logger.info("tag text box entered 50 tags");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(create.create_contac_notes).sendKeys("sample note 123456789");
		logger.info("Note text box entered sample note 123456789");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void createButton() throws Exception {
		button(create.create_contac_button, "button").click();
		logger.info("clicked on create contact button");
		wait(5);
		logger.info("wait for 5 sec");

	}

	public void closeicon() throws Exception {
		button(create.close_button, "button").click();
		logger.info("clicked on create contact button");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void validavatharlogo() throws Exception {
		button(create.create_avathar_logo, "button").click();
		logger.info("click on avathar logo ");
		waits(3);
		logger.info("wait for 5 sec");

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\JPG_2.5mb.jpg");
		logger.info("avatar image is added");
		waits(25);
		logger.info("wait for 25 sec");
	}

	public void invalidavatharlogo() throws Exception {

		button(create.create_avathar_logo, "button").click();
		logger.info("click on avathar logo");
		waits(5);
		logger.info("wait for 5 sec");

		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		logger.info("added invalid avathar logo");
		waits(10);
		logger.info("wait for 5 sec");
	}

	public void createcontactdetails() throws Exception {
		String mobileNumber = "555" + randomeNum();
		wait(5);
		String email = randomestring() + "@yopmail.com";
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contact_Name).sendKeys("1");
		logger.info("Name:1");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contact_phonenum).sendKeys(mobileNumber);
		logger.info("Mobile Number:" + mobileNumber);
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contac_Email).sendKeys(email);
		logger.info("Email:" + email);
		wait(5);
		logger.info("wait for 5 sec");

		textbox(create.create_contac_companyName).sendKeys("technocentra");
		logger.info("company Name: technocentra");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(create.create_contac_tags).sendKeys(
				"1,2,3,4,5,6,7,8,9,10,ABCDEFGHIJKLMN,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,123456789012345678901234567890,@$%&*/-,89");
		logger.info("tag text box entered tags");
		wait(5);
		logger.info("wait for 5 sec");

		textbox(create.create_contac_notes).sendKeys("sample note 123456789");
		logger.info("note text box entered sample note 123456789");
		wait(5);
		logger.info("wait for 5 sec");

		button(create.create_contac_button, "button").click();
		logger.info("click on create contact button");
		wait(10);
		logger.info("wait for 5 sec");
	}

	public void createcontactNameandMobilenumber() throws Exception {
		textbox(create.create_contact_Name).sendKeys("sample");
		logger.info("Name: sample");
		wait(5);
		logger.info("wait for 5 sec");
		textbox(create.create_contact_phonenum).sendKeys("3068016736");
		logger.info("Mobile number : 3068016736");
		wait(5);
		logger.info("wait for 5 sec");
	}
}
