package com.beetext.qa.WFCommonMethod;

import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.util.WebEventListener;

public class Conversation extends CommonMethods {

	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");
	String url, username1, username2, password;
	Repository repository = new Repository();

	public void autologin(Map<String, String> map) throws Exception {
		username1 = (String) map.get("username1");
		username2 = (String) map.get("username2");
		password = (String) map.get("password");

		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waits(5);
		logger.info("wait for 5 sec");

	}

	public void automationBlock_login(Map<String, String> map) throws Exception {

		textbox(Repository.user_id).sendKeys("automationblock@yopmail.com");
		logger.info("Email id :" + "automationblock@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on Login Button");
		waits(5);
		logger.info("wait for 5 sec");

	}

	public void Auto1_Login_Automation_Contact(Map<String, String> map) throws Exception {

		button(repository.Automation_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Automation_Contact Button");
		waits(2);
		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Enter the message ");
		waits(2);
		logger.info("wait for 2 sec");

	}

	public void SendButton(Map<String, String> map) throws Exception {

		button(repository.chatInputsendButton2, "button").click();
		waits(2);
		logger.info("wait for 2 sec");
	}

	public void changing_Login1_to_Login2(Map<String, String> map) throws Exception {

		validateSignOutLink();
		validateSignOut();

		username1 = (String) map.get("username1");
		username2 = (String) map.get("username2");
		password = (String) map.get("password");

		textbox(Repository.user_id).sendKeys(username2);
		logger.info("Email id :" + username2);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		button(Repository.loginbttn, "button").click();
		logger.info("click on letsgobttn Button");
		waits(5);
		logger.info("wait for 5 sec");
	}

	public void user2_ReceivingSide_message_Validation(Map<String, String> map) throws Exception {

		waits(2);
		logger.info("wait for 2 sec");

		button(repository.Auto1_Contact, "click on Auto1_Contact button").click();
		logger.info("click on Auto1_Contact Button");
		waits(2);

		if (repository.message.getText().equalsIgnoreCase("Hello Team")) {
			Assert.assertEquals(repository.message.getText(), "Hello Team", "message is matching");
			System.out.println(repository.message.getText());
			System.out.println("message is verified");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(repository.message.getText(), "Hello Team", "message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}
	}

	public void Pin_Attachment(Map<String, String> map) throws Exception {

		button(repository.pin_attachment, "file attachment").click();
		waits(3);
	}

}
