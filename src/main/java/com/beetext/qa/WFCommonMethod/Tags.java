package com.beetext.qa.WFCommonMethod;

import java.util.Properties;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.WebEventListener;

public class Tags extends CommonMethods {
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Repository repository = new Repository();

	public void tags_login() throws Exception {

		textbox(Repository.user_id).sendKeys(username1);
		logger.info(username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("password");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login");
		waitAndLog(2);
		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("click on tools link");
		waitAndLog(3);

	}
}
