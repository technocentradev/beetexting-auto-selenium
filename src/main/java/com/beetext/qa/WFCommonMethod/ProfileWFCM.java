package com.beetext.qa.WFCommonMethod;

import org.openqa.selenium.Keys;

import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Profileupdate;
import com.beetext.qa.pages.Repository;

public class ProfileWFCM extends CommonMethods {
	
	Profileupdate profile=new Profileupdate();
	
	
	public void login() throws Exception {
		textbox(Repository.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id: oneAgent@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password : tech1234");
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		waits(5);
		logger.info("wait for 5 sec");
	}
    public void clickonthreedotslink() {
    	profile.threedots.click();
    }
    public void profileLink() {
    	profile.profile_link.click();
    }
    public void profilepage() {
    	profile.profile_page_link.click();
    }
    public void profile_Name_clear() {
    	profile.profile_name_required.sendKeys(Keys.CONTROL + "a");
		profile.profile_name_required.sendKeys(Keys.DELETE);
    }
    public void profile_name() {
    	profile.profile_name.clear();
    	profile.profile_name.sendKeys("agentname");
    }
    public void signature_Clear() {
    	profile.signature_text.sendKeys(Keys.CONTROL + "a");
		profile.signature_text.sendKeys(Keys.DELETE);
    }
    public void signature() {
    	profile.signature_text.clear();
    	profile.signature_text.sendKeys("Regards Thank you...");
    }
    
    public void signature_100_char() {
    	profile.signature_text.sendKeys(Keys.CONTROL + "a");
		profile.signature_text.sendKeys(Keys.DELETE);
    	profile.signature_text.sendKeys("Regards Thank you...Regards Thank you...Regards Thank you...Regards Thank you...Regards Thank you...");
    }
    
    public void toggle_Buttons() {
    	profile.toggle_button.click();
    }
    public void save_Button() {
    	profile.saveButton.click();
    }
    public void toaster_Message() {
    	profile.toaster_message.getText();
    }
}
