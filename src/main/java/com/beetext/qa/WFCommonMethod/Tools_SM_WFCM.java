package com.beetext.qa.WFCommonMethod;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Create_Contact;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Reviews_pages;
import com.beetext.qa.pages.Tools_SM_pages;

public class Tools_SM_WFCM extends CommonMethods {
	Repository repository = new Repository();

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Tools_SM_pages SM = new Tools_SM_pages();
	Reviews_Workflow_CM RCM = new Reviews_Workflow_CM();
	Reviews_pages RP = new Reviews_pages();
	Create_Contact create = new Create_Contact();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void Login_Tools_SM() throws Exception {

		textbox(Repository.user_id).sendKeys(username2);
		logger.info("Email id :" + username2);
		wait(2);
		logger.info("Wait for 2 sec");
		textbox(Repository.password).sendKeys(password);
		logger.info("Password :" + password);
		wait(2);
		logger.info("Wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("clickd on login submit button");
		wait(5);
		logger.info("wait for 5 sec");

		button(SM.Tools_button, "click on Tools").click();
		logger.info("clicked on Tools ");
		waits(3);
		logger.info("wait for 10 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("clicked on sceduled message tab");
		waits(2);
		logger.info("waited for 2 sec");

	}

	public void Login2_Tools_SM() throws Exception {

		waits(2);
		textbox(Repository.user_id).sendKeys("oneagent@yopmail.com");
		logger.info("Email id :" + username1);
		wait(2);
		logger.info("Wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password :" + password);
		waits(2);
		logger.info("Wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("clickd on login submit button");
		waits(5);
		logger.info("wait for 5 sec");

		button(SM.Tools_button, "click on Tools").click();
		logger.info("clicked on Tools ");
		waits(2);
		logger.info("wait for 10 sec");
		button(SM.Scheduled_Message_Tab_button, "click on scheduled messages tab button").click();
		logger.info("clicked on sceduled message tab");
		waits(3);

	}

	public void DeleteSM() throws Exception {

		button(SM.First_contact_manage_button, "click on manage button").click();
		logger.info("clicked on Manage button");
		wait(5);
		logger.info("wait for 5 seconds");
		waits(2);
		button(SM.Edit_page_Delete_Scheduled_Message_button, "click on delete SM button in edit page").click();
		logger.info("clicked on delete sceduled button");
		wait(5);
		logger.info("wait for 5 seconds");
		button(SM.Confirm_delete_SM_button, "click on confirm delete SM button in edit page").click();
		logger.info("clicked on confirm delete button");
		waits(4);
		logger.info("wait for 4 seconds");

	}

	public void delete_draft() throws Exception {

		waits(3);
		button(SM.Delete_button_under_Drafts_list, "Clicked on Delete button").click();
		logger.info("Clicked on Delete  button");
		logger.info("popup is opened with options Discard and Cancel");
		waits(4);
		button(SM.Discard_button_in_conformation_Drafts_popup, "Clicked on Discard button in popup").click();
		logger.info("Clicked on Discard button in popup page");
		waits(5);

	}

	public void Click_on_CreateScheduledMessage_button() throws Exception {

		button(SM.Create_Scheduled_Message_button, "click on create scheduled message button").click();
		logger.info("clicked on create scheduled message button");
		waits(5);
		logger.info("wait for 2 sec");
	}

	public void Enter_Contact_number() throws Exception {

		textbox(SM.Message_sent_to_textbox).sendKeys("5555555551");
		logger.info("Entered contact number to message sent text field");
		waits(2);
		logger.info("wait for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added  contact number to message sent field");
		waits(2);
		logger.info("wait for 2 seconds");
	}

	public void Enter_Contact_number_2() throws Exception {

		textbox(SM.Message_sent_to_textbox).sendKeys("5555555552");
		logger.info("Entered contact number to message sent text field");
		waits(2);
		logger.info("wait for 5 seconds");
		driver.findElement(By.id("inputScheduleContactData")).sendKeys(Keys.ENTER);
		logger.info("Added  contact number to message sent field");
		waits(2);
		logger.info("wait for 2 seconds");
	}

	public void Enter_message() throws Exception {

		textbox(SM.Create_SM_Message_textbox).sendKeys("test message please ignore");
		logger.info("entered text into message textbox");
		wait(5);
		logger.info("wait for 5 seconds");
	}

	public void Click_Recurring_button() throws Exception {

		button(SM.recurring_check_button, "click on recurring check button").click();
		logger.info("Selected recurring button");
		wait(2);
		logger.info("wait for 2 seconds");
	}

	public void Create_Review() throws Exception {
		RCM.Click_on_Reviews();
		RCM.Click_on_AddReviewSite();
		RCM.Click_on_SelectSite_DD();

		List<WebElement> allOptions = driver.findElements(By.xpath("//select[contains(@id,'definedTitle')]//option"));
		System.out.println(allOptions.size());

		for (int i = 0; i <= allOptions.size() - 1; i++) {

			if (allOptions.get(i).getText().contains("Yelp")) {

				allOptions.get(i).click();
				break;
			}

		}
		logger.info("Selected Yelp");
		textbox(RP.Create_Review_Link_textbox).sendKeys("testing");
		logger.info("Entered link into link field");
		wait(5);
		logger.info("wait for 5 seconds");
		RCM.CRP_Click_on_Create_button();
	}

	public void delete_Review() throws Exception {
		RCM.Click_on_Reviews();
		waits(2);
		button(RP.Yelp_manage_button, "click on Yelp manage button").click();
		logger.info("Clicked on manage button");
		waits(2);
		WebElement element = driver.findElement(By.xpath("//button[@id='delete-review-message']"));
		JavascriptExecutor jr = (JavascriptExecutor) driver;
		jr.executeScript("arguments[0].click()", element);
		logger.info("Clickedon Delete button");
		waits(2);
		WebElement element1 = driver.findElement(By.xpath("//button[contains(text(),'Confirm Delete Review ')]"));
		JavascriptExecutor jr1 = (JavascriptExecutor) driver;
		jr1.executeScript("arguments[0].click()", element1);
		logger.info("Clicked on confirm delete button");

	}

	public void logout() throws Exception {
		waits(2);
		button(SM.Logout_3_dots, "click on 3 dots").click();
		logger.info("click on 3 dotts");
		waits(2);
		button(SM.SignOut_button, "Click on signout button").click();
		logger.info("clicked on signout button");
		waits(2);
	}

	public void Close_Tools_page() throws Exception {
		waits(2);
		WebElement element = driver.findElement(By.xpath("//*[@class='cross-icons']"));
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("arguments[0].click()", element);
	}
}
