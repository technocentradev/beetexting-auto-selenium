package com.beetext.qa.WFCommonMethod;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.SettingsCreateDepartment;
import com.beetext.qa.pages.Template;

public class SettingsCreateDepartmentWFCM  extends CommonMethods {
	String url, username1, username2, password;
	Repository repository = new Repository();
	SettingsCreateDepartment createDept=new SettingsCreateDepartment();
	TestBase testBase =new TestBase();
    Template template=new Template();
	
	public void login() throws Exception {
		
		button(createDept.Numbers_link,"button").click();
		logger.info("clicked on Numbers link");
		waits(5);
		logger.info("wait for 5 sec");
		
	}
	
	public void selectcreatenumberoption() throws Exception {
		button(createDept.Create_Number,"button").click();
		logger.info("clicked on create number button");
		wait(5);
		logger.info("wait for 5 sec");
		
	}
	
	public void createbutton() throws Exception {
		button(createDept.createNewNumber_Btn,"button").click();
		logger.info("Clicked on create new number button");
		wait(5);
		logger.info("wait for 5 sec");
	}
	
	public void selectTxtenabledNumberoption() throws Exception {
		button(createDept.txtEnabled_Existing_Number_Btn,"button").click();
		logger.info("clicked on textenabledexisting numebr button");
		wait(5);
		logger.info("wait for 5 sec");
	}
	
	public void departmentName() throws Exception {
		textbox(createDept.DeptName).sendKeys("sample1");
		logger.info("Department Name: sample");
		wait(2);
		logger.info("wait for 2 sec");
	}
	
	public void sampledepartmentName() throws Exception {
		textbox(createDept.DeptName).sendKeys("sample");
		logger.info("Department Name: sample");
		wait(2);
		logger.info("wait for 2 sec");
	}
	
	public void sample2departmentName() throws Exception {
		textbox(createDept.DeptName).sendKeys("sample2");
		logger.info("Department Name: sample");
		wait(2);
		logger.info("wait for 2 sec");
	}
	
	
	public void phonenumberinfopagedetails() throws Exception {
		
		button(createDept.NextButton,"button").click();
		logger.info("Clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		String mobile="555"+randomeNum();
		textbox(createDept.phonenumber).sendKeys(mobile);
		logger.info("phone number entered"+mobile);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.cname).sendKeys("sample");
		logger.info("Customer Name:sample");
		wait(2);
		logger.info("wait for 2 sec");
		
		textbox(createDept.cstreet).sendKeys("dummy");
		logger.info("street Name: dummy");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.ccity).sendKeys("dummy");
		logger.info("City Name: dummy");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.zipcode).sendKeys("89045");
		logger.info("Zip code entered: 89045");
		wait(2);
		logger.info("wait for 2 sec");
	}
	
	
	public void phonenumberinfopagedetailsinvalid() throws Exception {
		textbox(createDept.DeptName).sendKeys("sample");
		logger.info("Department Name: sample");
		wait(2);
		logger.info("wait for 2 sec");
		button(createDept.NextButton,"button").click();
		logger.info("Clicked on next button");
		wait(5);
		logger.info("wait for 5 sec");
		
		textbox(createDept.phonenumber).sendKeys("555");
		logger.info("phone number entered 555");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.cname).sendKeys("sample");
		logger.info("Customer Name:sample");
		wait(2);
		logger.info("wait for 2 sec");
		
		textbox(createDept.cstreet).sendKeys("dummy");
		logger.info("street Name: dummy");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.ccity).sendKeys("dummy");
		logger.info("City Name: dummy");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(createDept.zipcode).sendKeys("89045");
		logger.info("Zip code entered: 89045");
		wait(2);
		logger.info("wait for 2 sec");
	}
	
	public void selectStage() throws Exception {
		textbox(createDept.state).click();
		logger.info("clicked on state drop down text box");
		waits(3);
		
		  List<WebElement> allOptions = driver.findElements(By.xpath("//select[@id='existingNumberState']//option"));
	        System.out.println(allOptions.size());
	         
	                 
	                 
	        for(int i = 0; i<=allOptions.size()-1; i++) {
	             
	             
	            if(allOptions.get(i).getText().contains("Guam")) {
	                 
	                allOptions.get(i).click();
	                break;
	                 
	            }
	        }
	}
	
	public void Auto2Logindetails() throws Exception {
		
		textbox(Repository.user_id).sendKeys("auto2@yopmail.com");
		logger.info("Email id:oneAgent@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("password:"+password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		waits(5);
		logger.info("wait for 10 sec");
		
		button(createDept.Settings_link,"button").click();
		logger.info("clicked on settings link");
		waits(5);
		logger.info("wait for 10 sec");
		button(createDept.Numbers_link,"button").click();
		logger.info("clicked on numbers link");
		waits(5);
	}
	public void oneAgentLogindetails() throws Exception {
	
		textbox(Repository.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id:oneAgent@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("password:"+password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		waits(5);
		logger.info("wait for 10 sec");
		
		button(createDept.Settings_link,"button").click();
		logger.info("clicked on settings link");
		waits(5);
		logger.info("wait for 10 sec");
		button(createDept.Numbers_link,"button").click();
		logger.info("clicked on numbers link");
		waits(5);
	}
	public void logout() {
		validateSignOutLink();
		logger.info("clicked on three dots");
		validateSignOut();
		logger.info("clicked on sign out link");
	}

	public void closetoolspopups() throws Exception {
		button(createDept.close_settingspopup, "login submit").click();
		logger.info("clicked on close settings popup link");
		waits(5);

	}

}
