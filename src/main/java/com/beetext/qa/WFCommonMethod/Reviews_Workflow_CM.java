package com.beetext.qa.WFCommonMethod;

//import javax.xml.xpath.XPath;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Reviews_pages;
import com.beetext.qa.util.TestUtil;

public class Reviews_Workflow_CM extends CommonMethods {

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	Repository repository = new Repository();
	Reviews_pages reviews = new Reviews_pages();
	TestUtil testUtil = new TestUtil();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	public void login2() throws Exception

	{
		textbox(Repository.user_id).sendKeys(username2);
		logger.info("Email_ID:" + username2);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password :" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		logger.info("Clicked on login submit button");
		Click_on_Tools();
	}

	public void login_Automations() throws Exception

	{
		textbox(Repository.user_id).sendKeys(username2);
		logger.info("Email_ID:" + username2);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password :" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		logger.info("Clicked on login submit button");
	}

	public void login1() throws Exception

	{
		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email_ID:" + username1);
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password :" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		logger.info("Clicked on login submit button");
		waitAndLog(2);

	}

	public void Click_on_Tools() throws Exception

	{
		waitAndLog(2);
		button(reviews.Tools_link_button, "Click on Tools").click();
		logger.info("Clicked on Tools");
		waitAndLog(2);

	}

	public void Click_on_Reviews() throws Exception

	{
		waitAndLog(2);
		button(reviews.reviews_Tab, "Verify Review tab then click").click();
		logger.info("Clicked on Review tab");

	}

	public void Click_on_AddReviewSite() throws Exception

	{
		waitAndLog(2);
		button(reviews.Review_AddReviewSite_button, "Click on Add Review Site button").click();
		logger.info("Clicked on Add review site button");

	}

	public void Click_on_SelectSite_DD() throws Exception

	{
		waitAndLog(2);
		textbox(reviews.Create_Review_Select_Site_DD).click();
		logger.info("Clicked on Select site DD");

	}

	public void Click_on_Edit_SelectSite_DD() throws Exception

	{
		waitAndLog(2);
		textbox(reviews.Edit_Review_SelectSite_DD).click();
		logger.info("Clicked on Select site DD");

	}

	public void ERP_Save_button() throws Exception

	{
		waitAndLog(2);
		button(reviews.Edit_Review_page_Save_Button, "click on save button").click();
		logger.info("Clicked on Save button");

	}

	public void CRP_Click_on_Create_button() throws Exception

	{
		waitAndLog(2);
		button(reviews.Create_Review_Create_button, "click on create button").click();
		logger.info("Clicked on Create button");

	}

	public void Click_on_Link() throws Exception

	{
		waitAndLog(2);
		link(reviews.Learnhowtosetupyourreviewsites_link, "click on Learn how to set up your review sites link")
				.click();
		logger.info("Clicked on Link");

	}

	public void ManagePage_Delete_Method() throws Exception {

		waitAndLog(2);
		button(reviews.Manage_review_page_Delete_Review_button, "click on delete button").click();
		logger.info("Clickedon Delete button");
		waitAndLog(2);
		button(reviews.Manage_review_page_Confirm_Delete_Review_button, "click on confirm delete button").click();
		logger.info("Clicked on confirm delete button");
		waitAndLog(2);
	}
}
