package com.beetext.qa.WFCommonMethod;

import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import com.beetext.qa.pages.BroadCast_Pages;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.util.WebEventListener;

public class BM_Optout_WFCM extends CommonMethods {
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");

	String username1 = "auto1@yopmail.com";
	String username2 = "automations@yopmail.com";
	String password = "tech1234";
	BroadCast_Pages BM_Pages = new BroadCast_Pages();

	public void BM_tools_login() throws Exception {

		textbox(BroadCast_Pages.user_id).sendKeys(username1);
		logger.info("Email id :" + username1);
		textbox(BroadCast_Pages.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
	}

	public void BM_tools_login_automations() throws Exception {

		textbox(BroadCast_Pages.user_id).sendKeys(username2);
		logger.info("Email id :" + username2);
		textbox(BroadCast_Pages.Passsword_ID).sendKeys(password);
		logger.info("Password:" + password);
		waitAndLog(2);
		button(BroadCast_Pages.loginbttn, "login submit").submit();
		waitAndLog(2);

		link(BM_Pages.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
	}

	public void create_BM() throws Exception {
		waitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		implicitwaitAndLog(2);

		BM_Terms_Conditions();

		BM_SendBM();
	}

	public void create_BM_With_Attachment() throws Exception {
		waitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(5);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_SendBM();
	}

	public void create_Draft_BM() throws Exception {

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(2);

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		BM_Terms_Conditions();

		button(BM_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Click the Save As Draft");

		Draft_Toaster();

	}

	public void create_Draft_BM_Tag_Msg() throws Exception {

		waitAndLog(2);
		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		BM_Terms_Conditions();

		button(BM_Pages.tools_BM_create_Save_Draft, "click on button").click();
		logger.info("Clear the Save As Draft");

		Draft_Toaster();

	}

	public void create_Recurring() throws Exception {

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

	}

	public void create_Recurring_With_Attachment() throws Exception {

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		implicitwaitAndLog(2);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);
		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_attachment, "button").click();
		logger.info("click on pin Attachment ");
		waitAndLog(2);

		Runtime.getRuntime()
				.exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\SamplePng.Png");

		waitAndLog(5);

		button(BM_Pages.tools_BM_create_make_this_broadCast_recurring, "button").click();
		logger.info("click on recurring button");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule Later button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		waitAndLog(2);

		BM_Terms_Conditions();

		BM_ScheduleBroadCast();

	}

	public void past_reuse() throws Exception {
		waitAndLog(2);
		button(BM_Pages.tools_BM_past, "button").click();
		logger.info("click on Past Bm ");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_past_Reuse, "button").click();
		logger.info("click on Past reuse in Bm ");
		implicitwaitAndLog(2);
		textbox(BM_Pages.tools_BM_title).clear();
		logger.info(" Enter the title name");

		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");
		waitAndLog(2);
		button(BM_Pages.tools_BM_create_sendNow, "button").click();
		logger.info("click on Send now button");
		waitAndLog(2);

	}

	public void upcoming_Manage_Delete() throws Exception {

		waitAndLog(2);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast button");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_upcoming, "button").click();
		logger.info("click on upcoming Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_create_upcoming_manage, "button").click();
		logger.info("click on upcoming manage Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage_delete, "button").click();
		logger.info("click on upcoming manage delete Bm ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_upcoming_manage_delete, "button").click();
		logger.info("click on upcoming manage delete Bm ");
		waitAndLog(2);

	}

	public void draft_Delete() throws Exception {

		waitAndLog(3);

		button(BM_Pages.broadCast, "button").click();
		logger.info("click on broadCast ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_draft, "button").click();
		logger.info("click on tools_BM_draft Bm ");
		implicitwaitAndLog(2);
		button(BM_Pages.tools_BM_Draft_Delete, "button").click();
		logger.info("click on tools_BM_Draft_Delet ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_Draft_Delete_Discard, "button").click();
		logger.info("click on tools_BM_create_PopUp_Discard ");
		waitAndLog(2);

	}

	public void Draft_Toaster() throws Exception {

		BM_Pages.BM_Draft_Toaster.getText();
		logger.info(BM_Pages.BM_Draft_Toaster.getText());
		if (BM_Pages.BM_Draft_Toaster.getText().equalsIgnoreCase("Your broadcast text has been saved as a draft.")) {
			Assert.assertEquals(BM_Pages.BM_Draft_Toaster.getText(),
					"Your broadcast text has been saved as a draft.", "message is matching");
			logger.info(BM_Pages.BM_Draft_Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.BM_Draft_Toaster.getText(),
					"Your broadcast text has been saved as a draft.", "message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Auto1_Draft_Deleted_Toaster() throws Exception {

		BM_Pages.Draft_Deleted_Toaster.getText();
		logger.info(BM_Pages.Draft_Deleted_Toaster.getText());
		if (BM_Pages.Draft_Deleted_Toaster.getText().equalsIgnoreCase("Broadcast draft has been deleted.")) {
			Assert.assertEquals(BM_Pages.Draft_Deleted_Toaster.getText(), "Broadcast draft has been deleted.",
					"message is matching");
			logger.info(BM_Pages.Draft_Deleted_Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.Draft_Deleted_Toaster.getText(), "Broadcast draft has been deleted.",
					"message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		}

	}

	public void Auto1_Created_Toaster() throws Exception {

		if (BM_Pages.tools_BM_create_toaster.getText()
				.equalsIgnoreCase("Your broadcast has been successfully created for Auto1.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_toaster.getText(),
					"Your broadcast has been successfully created for Auto1.", "message is matching");
			logger.info(BM_Pages.tools_BM_create_toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_toaster.getText(),
					"Your broadcast has been successfully created for Auto1.", "message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Auto2_Created_Toaster() throws Exception {

		if (BM_Pages.tools_BM_create_toaster.getText()
				.equalsIgnoreCase("Your broadcast has been successfully created for Auto2.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_toaster.getText(),
					"Your broadcast has been successfully created for Auto2.", "message is matching");
			logger.info(BM_Pages.tools_BM_create_toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_toaster.getText(),
					"Your broadcast has been successfully created for Auto2.", "message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Automations_Created_Toaster() throws Exception {

		if (BM_Pages.tools_BM_create_toaster.getText()
				.equalsIgnoreCase("Your broadcast has been successfully created for Automations.")) {
			Assert.assertEquals(BM_Pages.tools_BM_create_toaster.getText(),
					"Your broadcast has been successfully created for Automations.", "message is matching");
			logger.info(BM_Pages.tools_BM_create_toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.tools_BM_create_toaster.getText(),
					"Your broadcast has been successfully created for Automations.", "message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Updated_Toaster() throws Exception {

		if (BM_Pages.Updated_Toaster.getText().equalsIgnoreCase("Your broadcast has been successfully updated.")) {
			Assert.assertEquals(BM_Pages.Updated_Toaster.getText(), "Your broadcast has been successfully updated.",
					"message is matching");
			logger.info(BM_Pages.Updated_Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.Updated_Toaster.getText(), "Your broadcast has been successfully updated.",
					"message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void Auto1_Updated_Toaster() throws Exception {

		BM_Pages.BM_Updated_Toaster_Auto1.getText();
		logger.info(BM_Pages.BM_Updated_Toaster_Auto1.getText());
		if (BM_Pages.BM_Updated_Toaster_Auto1.getText()
				.equalsIgnoreCase("Your broadcast has been successfully updated for Auto1.")) {
			Assert.assertEquals(BM_Pages.BM_Updated_Toaster_Auto1.getText(),
					"Your broadcast has been successfully updated for Auto1.", "message is matching");
			logger.info(BM_Pages.BM_Updated_Toaster_Auto1.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(BM_Pages.BM_Updated_Toaster_Auto1.getText(),
					"Your broadcast has been successfully updated for Auto1.", "message is not  matching");
			logger.info("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void BMgif() throws Exception {

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(3);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_GifIcon, "button").click();
		logger.info("click on Gif icon");

		button(BM_Pages.tools_BM_Gif1, "button").click();
		logger.info("click on tools_BM_Gif1");
		waitAndLog(2);

	}

	public void BMEmoji() throws Exception {

		button(BM_Pages.create_broadcast, "button").click();
		logger.info("click on create BroadCast button");
		waitAndLog(3);

		textbox(BM_Pages.tools_BM_input_To_searchTags).sendKeys("BM");
		logger.info("Enter data in search tags ");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_SearchTag1, "button").click();
		logger.info("click on tag");
		String randomtitle = RandomStringUtils.randomAlphabetic(5);

		textbox(BM_Pages.tools_BM_title).sendKeys(randomtitle);
		logger.info(" Enter the title name");

		textbox(BM_Pages.tools_BM_message).sendKeys("Technocentra");
		logger.info(" Enter your messeage");
		waitAndLog(2);

		button(BM_Pages.tools_BM_EmojiIcon, "button").click();
		logger.info("click on tools_BM_EmojiIcon");
		waitAndLog(2);

		button(BM_Pages.emoji, "button").click();
		logger.info("click on emoji");
		waitAndLog(2);

	}

	public void BMCreate_Schedule() throws Exception {

		waitAndLog(2);
		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule later button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		implicitwaitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		implicitwaitAndLog(2);
	}

	public void BM_Edit_Schedule_with_2min() throws Exception {

		button(BM_Pages.tools_BM_create_ScheduleLater, "button").click();
		logger.info("click on Schedule later button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_DateandTime_button, "button").click();
		logger.info("click on Date and Time Button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();
		button(BM_Pages.tools_BM_create_time_upArrow, "button").click();

		waitAndLog(2);

		button(BM_Pages.tools_BM_create_select_DateandTime_set, "button").click();
		logger.info("click on Date and Time set Button");
		implicitwaitAndLog(2);
	}

	public void CreateReview_For_BM() throws Exception {

		button(BM_Pages.Tools_Reviews, "button").click();
		logger.info("click on Review button");

		button(BM_Pages.tools_BM_create_Review, "button").click();
		logger.info("click on create Review in BM button");

		textbox(BM_Pages.tools_BM_create_Review_link).sendKeys("https://automation.beetexting.com/");
		logger.info("Enter text to tools_BM_create_Review_link");

		button(BM_Pages.tools_BM_create_Review_Create, "button").click();
		logger.info("click on tools_BM_create_Review_link");

	}

	public void Review_Delete() throws Exception {

		button(BM_Pages.Tools_Reviews, "button").click();
		logger.info("click on Review button");
		waitAndLog(2);

		button(BM_Pages.tools_BM_create_Review_GoogleReview_Manage, "button").click();
		logger.info("click on tools_BM_create_Review_GoogleReview_Manage button");

		button(BM_Pages.tools_BM_create_Review_Manage_DeleteReview, "button").click();
		logger.info("click on tools_BM_create_Review_Manage_DeleteReview button");

		button(BM_Pages.tools_BM_create_Review_Manage_DeleteReview, "button").click();
		logger.info("click on tools_BM_create_Review_Manage_DeleteReview button");
		waitAndLog(2);

	}

	public void BM_Terms_Conditions() throws Exception {

		button(BM_Pages.tools_BM_create_agree_termsandCond, "button").click();
		logger.info("click on Agree Terms and Conditions");
		waitAndLog(2);

	}

	public void BM_SendBM() throws Exception {

		button(BM_Pages.tools_BM_create_Send_BroadCast, "button").click();
		logger.info("click on Upcoming Send BroadCast button");
	}

	public void BM_ScheduleBroadCast() throws Exception {

		button(BM_Pages.tools_BM_create_Schedule_BroadCast, "button").click();
		logger.info("click on Schedule BroadCast");

	}

	public void BM_Automations_Toater_Verification() throws Exception {

		if (verifyElementIsEnabled(BM_Pages.Automations_created_Toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.Automations_created_Toaster));
			textbox(BM_Pages.Automations_created_Toaster).isEnabled();
			logger.info(BM_Pages.Automations_created_Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.Automations_created_Toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public void BM_Automations_Draft_Toaster_Verification() throws Exception {

		if (verifyElementIsEnabled(BM_Pages.BM_Draft_Toaster)) {
			Assert.assertEquals(true, verifyElementIsEnabled(BM_Pages.BM_Draft_Toaster));
			textbox(BM_Pages.BM_Draft_Toaster).isEnabled();
			logger.info(BM_Pages.BM_Draft_Toaster.getText());
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(BM_Pages.BM_Draft_Toaster));
			logger.info("Disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}
}
