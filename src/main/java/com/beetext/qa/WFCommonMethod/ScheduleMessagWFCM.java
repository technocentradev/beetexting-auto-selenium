package com.beetext.qa.WFCommonMethod;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.Schedule_Message;
import com.beetext.qa.util.TestUtil;

public class ScheduleMessagWFCM extends CommonMethods {

	String url, username1, username2, password;
	Repository repository = new Repository();
	TestUtil testUtil = new TestUtil();
	Schedule_Message sm = new Schedule_Message();
	TestBase testBase = new TestBase();
	public static String currentDir = System.getProperty("user.dir");

	/*
	 * public void login() throws Exception { username1 = (String)
	 * map.get("username1"); password = (String) map.get("password");
	 * System.out.println("username1-->"+username1); wait(5);
	 * textbox(repository.user_id).sendKeys(username1);
	 * logger.info("Email id:"+username1); wait(2); logger.info("wait for 2 sec");
	 * textbox(repository.Passsword_ID).sendKeys(password);
	 * logger.info("password:"+password); wait(2); logger.info("wait for 2 sec");
	 * 
	 * button(repository.loginbttn, "login submit").submit();
	 * logger.info("login button is clicked"); waits(120);
	 * logger.info("wait for 2 min"); }
	 */
	public void CreateSM() throws Exception {

		button(sm.select_conversation12, "button").click();
		logger.info("Conversaiton selected");
		waits(2);
		logger.info("wait for 5 sec");
		button(sm.schedule_icon, "button").click();
		logger.info("clicked on schedule icon");
		waits(1);
		logger.info("wait for 5 sec");
		button(sm.increasehour, "button").click();
		logger.info("clicked on schedule icon");
		waits(1);
		logger.info("wait for 5 sec");
		button(sm.set_date_time, "button").click();
		logger.info("Date and time is selected");
		waits(3);
		logger.info("wait for 5 sec");
	}

	public void selectScheduleoption() throws Exception {
		button(sm.schedule_icon, "button").click();
		logger.info("Clicked on scheduel icon");
		waits(3);
		logger.info("wait for 5 sec");

		button(sm.set_date_time, "button").click();
		logger.info("Date and time is selected");
		waits(2);
		logger.info("wait for 5 sec");
	}

	public void select_conversation12() throws Exception {
		button(sm.select_conversation12, "button").click();
		logger.info("conversation  selected");
		waits(2);
		logger.info("wait for 5 sec");
		selectScheduleoption();

	}

	public void entermessage() throws Exception {
		String message = Char50_randomestring();
		wait(2);
		textbox(sm.input_box).sendKeys(message);
		logger.info("Message is typing " + message);
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void ScheduleButton() throws Exception {
		button(sm.schedule_button, "button").click();
		logger.info("Schedule button is clicked");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void clickonComposeIcon() throws Exception {
		button(sm.click_compose_icon, "button").click();
		logger.info("Clicked on compose icon");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void selectQA10_Conversation() throws Exception {
		String mobile = "QA10";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("compose new search box typing mobile number");
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");
	}

	public void composeNewcluckonSM() throws Exception {

		button(sm.schedule_icon, "button").click();
		logger.info("Schedule button is clicked");
		waits(2);
		logger.info("wait for 5 sec");
		button(sm.increasehour, "button").click();
		logger.info("Schedule button is clicked");
		waits(2);
		logger.info("wait for 5 sec");
		
		button(sm.set_date_time, "button").click();
		logger.info("date and time is selected");
		waits(1);
	}

	public void composenew_groupSM() throws Exception {

		button(sm.click_compose_icon, "button").click();
		logger.info("Clicked on compose ");
		waits(2);
		logger.info("wait for 5 sec");
	}

	public void selectgroupcontacts() throws Exception {
		String mobile = "QA10";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("compose new search box entering" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact, "button").click();
		logger.info("selceted  contact ");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile2 = "QA123";
		textbox(sm.click_compose_search_contact).sendKeys(mobile2);
		logger.info("compose new adding 2nd contact" + mobile2);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact2, "button").click();
		logger.info("second contact is selected");
		wait(5);
		logger.info("wait for 5 sec");
		button(sm.schedule_icon, "button").click();
		logger.info("Clicked on schedule icon");
		waits(5);
		logger.info("wait for 5 sec");
		button(sm.increasehour, "button").click();
		logger.info("Schedule button is clicked");
		waits(2);
		logger.info("wait for 5 sec");
		button(sm.set_date_time, "button").click();
		logger.info("date and time is selected");
		wait(2);
		logger.info("wait for 2 sec");
	}

	public void attachments() throws Exception {
		button(repository.pin_attachment, "file attachment").click();
		logger.info("upload attachment icon is clicked");
		waits(5);
		logger.info("wait for 7 sec");
		Runtime.getRuntime().exec(currentDir + "\\Autoit\\fileupload.exe" + " " + currentDir + "\\Files\\pdf.pdf");
		System.out.println(currentDir + "\\Files\\SampleVideo.mp4");
		logger.info("file selected");
		waits(5);
		logger.info("wait for 5 sec");
	}

	public void selectgroupconversation() throws Exception {

		button(sm.select_group_conversation, "button").click();
		logger.info("Conversation selected");
		wait(5);
		logger.info("wait for 5  sec");
		button(sm.schedule_icon, "button").click();
		logger.info("scheudle icon is clicked");
		waits(5);
		logger.info("wait for 5 sec");
		button(sm.increasehour, "button").click();
		logger.info("scheudle hour increate icon is clicked");
		waits(5);
		logger.info("wait for 5 sec");
		
		button(sm.set_date_time, "button").click();
		logger.info("Date and time is selected");
		waits(5);
		logger.info("wait for 5 sec");
	}

	public void composeGroup() throws Exception {
		button(sm.click_compose_icon, "button").click();
		logger.info("compose icon is clicked");
		wait(5);
		logger.info("wait for 5 sec");
		String mobile = "QA10";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("search for mobile number" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 2 sec");

		String mobile1 = "QA123";
		textbox(sm.click_compose_search_contact).sendKeys(mobile1);
		logger.info("selected contact");
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact2, "button").click();
		logger.info("second contact is selected");
		waits(2);
		logger.info("wait for 5 sec");

		button(sm.schedule_icon, "button").click();
		logger.info("click on schedule icon");
		waits(3);
		logger.info("wait for 5 sec");
		button(sm.increasehour, "button").click();
		logger.info("scheudle hour increate icon is clicked");
		waits(2);
		logger.info("wait for 5 sec");
		button(sm.set_date_time, "button").click();
		logger.info("selected date and time");
		waits(1);
		logger.info("wait for 5 sec");
	}

	public void composenew() throws Exception {
		waitAndLog(10);
		button(sm.click_compose_icon, "button").click();
		logger.info("compose icon is clicked");
		wait(5);
		String mobile = "QA10";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("search contact" + mobile);
		waits(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact, "button").click();
		logger.info("contact selected");
		wait(5);
		logger.info("wait for 5 sec");

		button(sm.schedule_icon, "button").click();
		logger.info("click on schedule icon");
		waits(3);
		logger.info("wait for 5 sec");
		button(sm.reducehour, "button").click();
		waits(2);
		button(sm.set_date_time, "button").click();
		//waits(120);

		String message = Char50_randomestring();
		wait(3);
		logger.info("wait for 3 sec");
		textbox(sm.input_box).sendKeys(message);
		logger.info("message typed" + message);
		wait(2);
		logger.info("wait for 2 sec");

	}

	public void composenew_group() throws Exception {
		button(sm.click_compose_icon, "button").click();
		logger.info("clicked on compose new icon");
		wait(5);
		String mobile = "QA10";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("search the contacts" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact, "button").click();
		logger.info("contact selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile1 = "QA123";
		textbox(sm.click_compose_search_contact).sendKeys(mobile1);
		logger.info("search the contact" + mobile1);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact2, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");
		button(sm.schedule_icon, "button").click();
		logger.info("scheudle icon is clicked");
		waits(5);
		logger.info("wait for 5 sec");
		button(sm.reducehour, "button").click();
		logger.info("hour reduce icon is clicked");
		waits(5);
		logger.info("wait for 5 sec");
        
		button(sm.set_date_time, "button").click();
		logger.info("selected date and time");
		//waits(120);
		logger.info("wait for 2 min");
	}

	public void composenew_group_maxlimit_Contacts_create_SM() throws Exception {
		button(sm.click_compose_icon, "button").click();
		logger.info("clicked on compose new icon");
		wait(5);
		String mobile = "QA10";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("search the contacts" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact, "button").click();
		logger.info("contact selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile1 = "QA123";
		textbox(sm.click_compose_search_contact).sendKeys(mobile1);
		logger.info("search the contact" + mobile1);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact2, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile2 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile2);
		logger.info("search the contact" + mobile2);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile3 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile3);
		logger.info("search the contact" + mobile3);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile4 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile4);
		logger.info("search the contact" + mobile4);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile5 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile5);
		logger.info("search the contact" + mobile5);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile6 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile6);
		logger.info("search the contact" + mobile6);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile7 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile7);
		logger.info("search the contact" + mobile7);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile8 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile8);
		logger.info("search the contact" + mobile8);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		button(sm.schedule_icon, "button").click();
		logger.info("scheudle icon is clicked");
		waits(5);
		logger.info("wait for 5 sec");
		button(sm.increasehour, "button").click();
		logger.info("scheudle hour increate icon is clicked");
		waits(5);
		logger.info("wait for 5 sec");
		button(sm.set_date_time, "button").click();
		logger.info("selected date and time");
		waits(5);
		logger.info("wait for 2 min");
	}

	public void composenew_group_maxlimit_Contacts_erorrmessage_checking() throws Exception {
		button(sm.click_compose_icon, "button").click();
		logger.info("clicked on compose new icon");
		wait(5);
		String mobile = "QA10";
		textbox(sm.click_compose_search_contact).sendKeys(mobile);
		logger.info("search the contacts" + mobile);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact, "button").click();
		logger.info("contact selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile1 = "QA123";
		textbox(sm.click_compose_search_contact).sendKeys(mobile1);
		logger.info("search the contact" + mobile1);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.click_compose_select_contact2, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile2 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile2);
		logger.info("search the contact" + mobile2);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile3 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile3);
		logger.info("search the contact" + mobile3);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile4 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile4);
		logger.info("search the contact" + mobile4);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile5 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile5);
		logger.info("search the contact" + mobile5);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile6 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile6);
		logger.info("search the contact" + mobile6);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile7 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile7);
		logger.info("search the contact" + mobile7);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

		String mobile8 = "555" + randomeNum();
		textbox(sm.click_compose_search_contact).sendKeys(mobile8);
		logger.info("search the contact" + mobile7);
		wait(2);
		logger.info("wait for 2 sec");
		button(sm.selectNewcontact, "button").click();
		logger.info("contact is selected");
		wait(5);
		logger.info("wait for 5 sec");

	}

}
