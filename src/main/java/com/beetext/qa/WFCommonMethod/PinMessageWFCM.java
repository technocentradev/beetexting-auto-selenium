package com.beetext.qa.WFCommonMethod;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Pin_Messages;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;

public class PinMessageWFCM extends CommonMethods {

	String url, username1, username2, password;
	Pin_Messages pinMessage = new Pin_Messages();
	TestUtil testUtil = new TestUtil();
	TestBase testBase = new TestBase();

	public void selectConversation() throws Exception {
		logger.info("Trying to select conversation");
		button(pinMessage.select_conversations12, "button").click();
		logger.info("conversation selected");
		wait(5);
		logger.info("wait for 5 sec");
		// System.out.println( repository.select_conversation.getText() );
		button(pinMessage.pinIcon, "button").click();
		logger.info("clicked on pin icon");
		wait(3);
		logger.info("wait for 3 sec");
	}

	public void logout() {
		waits(2);
		validateSignOutLink();
		logger.info("clicked on three dot's");
		waits(5);
		validateSignOut();
		logger.info("clicked on signout link");
	}

	public void noAgent() throws Exception {
		
		waits(2);
		textbox(Repository.user_id).sendKeys("oneAgent@yopmail.com");
		logger.info("Email id: oneAgent@yopmail.com");
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password : tech1234");
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button is clicked");
		waits(5);
		logger.info("wait for 5 sec");
//		button(pinMessage.hoverdepartmentdropdown, "button").click();
//		logger.info("selected conversation");
//		waits(3);
//		logger.info("wait for 3 sec");
//		button(pinMessage.selectsample2dept, "button").click();
//		logger.info("selected conversation");
//		waits(5);
//		logger.info("wait for 3 sec");

		button(pinMessage.noagentconversion, "button").click();
		logger.info("selected conversation");
		wait(5);
		logger.info("wait for 4 sec");
		// System.out.println( repository.select_conversation.getText() );
		button(pinMessage.pinIcon, "button").click();
		logger.info("clicked on pin icon ");
		wait(3);
		logger.info("wait for 2 sec");
		textbox(pinMessage.input_box).sendKeys("@");
		logger.info("typing @ symbol");
		wait(2);
		logger.info("wait for 2 sec");
	}
}
