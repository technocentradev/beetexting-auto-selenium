package com.beetext.qa.WFCommonMethod;

import java.util.Properties;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import com.beetext.qa.pages.Automations_pages;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.util.WebEventListener;

public class AutomatiosWFCM extends CommonMethods {
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");
	String url, username1, username2, password;
	Repository repository = new Repository();
	Automations_pages automationspages = new Automations_pages();

	public void Automations_autologin() throws Exception {

		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(2);

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
	}

	public void toolsAutomations_Button() throws Exception {

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
	}

	public void click_on_Tools_CrossButton() throws Exception {

		button(automationspages.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waits(2);
	}

	public void Automations_Username2() throws Exception {

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + "automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(2);
		logger.info("wait for 2 sec");

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
	}

	public void automations_block_for_DefaultMessages() throws Exception {

		textbox(Repository.user_id).sendKeys("automationblock@yopmail.com");
		logger.info("Email id :" + username1);
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waits(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waits(3);
		logger.info("wait for 2 sec");

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");
		wait(3);
		logger.info("wait for 2 sec");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("notification");
	}

	public void Create_Automations_Contains_Phrase() throws Exception {
		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + "automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(automationspages.Auto1_Contact, "Click on Auto1 Contact").click();
		logger.info("click on Auto1_Contact button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("stop");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "click on send Button").click();
		logger.info("click on send");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		if (automationspages.message1.getText().equalsIgnoreCase("Hello - Technocentra")) {
			Assert.assertEquals(automationspages.message1.getText(), "Hello - Technocentra", "message is matching");
			System.out.println(automationspages.message1.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.message1.getText(), "Hello - Technocentra",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);
		Automations_autologin();
		waitAndLog(2);
		button(automationspages.manage_1, "manage_1").click();
		logger.info("click on manage_1");
		waitAndLog(2);

		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
	}

	public void Create_Automations_MSG_username1() throws Exception {
		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.Automation_Contact, "Click on Automation_Contact").click();
		logger.info("click on Automation_Contact button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("stop");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "Click on Send Button").click();
		logger.info("click on send");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		if (automationspages.message1.getText().equalsIgnoreCase("Hello - Technocentra")) {
			Assert.assertEquals(automationspages.message1.getText(), "Hello - Technocentra", "message is matching");
			System.out.println(automationspages.message1.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.message1.getText(), "Hello - Technocentra",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);
		Automations_Username2();
		waitAndLog(2);
		button(automationspages.manage_1, "manage").click();
		logger.info("click on manage");
		waitAndLog(2);

		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
	}

	public void Create_Automations_delay_Response() throws Exception {
		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.Auto1_Contact, "Click on Auto1_Contact").click();
		logger.info("click on Auto1_Contact button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello Team");
		logger.info("Hello");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "Click on Send Button").click();
		logger.info("click on send");
		waitAndLog(100);
		logger.info("wait for 100 sec");

		if (automationspages.message1.getText().equalsIgnoreCase("Hello - Technocentra")) {
			Assert.assertEquals(automationspages.message1.getText(), "Hello - Technocentra", "message is matching");
			System.out.println(automationspages.message1.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.message1.getText(), "Hello - Technocentra",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);
		Automations_autologin();
		waitAndLog(2);
		button(automationspages.manage_1, "manage_1").click();
		logger.info("click on manage_1");
		waitAndLog(2);

		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
	}

	public void Create_Automations_Exactly_Phrase() throws Exception {
		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.Auto1_Contact, "Click on Auto1_Contact").click();
		logger.info("click on Auto1_Contact button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello");
		logger.info("stop");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "Click on Send Button").click();
		logger.info("click on send");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		if (automationspages.message1.getText().equalsIgnoreCase("Hello - Technocentra")) {
			Assert.assertEquals(automationspages.message1.getText(), "Hello - Technocentra", "message is matching");
			System.out.println(automationspages.message1.getText());
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(automationspages.message1.getText(), "Hello - Technocentra",
					"message is not  matching");
			TestUtil.takeScreenshotAtEndOfTest("messageTestCase");// ScreenShot capture
		}

		validateSignOutLink();
		validateSignOut();
		waitAndLog(2);
		Automations_autologin();
		waitAndLog(2);
		button(automationspages.manage_1, "manage_1").click();
		logger.info("click on manage_1");
		waitAndLog(2);

		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
	}

	public void delete_Automations() throws Exception {
		waitAndLog(2);
		button(automationspages.manage_1, "manage_1").click();
		logger.info("click on manage_1");
		waitAndLog(2);

		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);

	}

	public void delete_Automations_manage() throws Exception {
		waitAndLog(2);
		button(automationspages.manage_1, "manage_1").click();
		logger.info("click on manage");
		waitAndLog(2);

		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);

	}

	public void delete_Automations_From_tools() throws Exception {
		waitAndLog(2);
		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
		waitAndLog(2);
		button(automationspages.manage_1, "manage").click();
		logger.info("click on manage");
		waitAndLog(2);

		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);
		button(automationspages.delete_autoResponse, "delete_autoResponse").click();
		logger.info("click on delete_autoResponse");
		waitAndLog(2);

	}

	public void Automations_From_tools_tag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
		waitAndLog(2);
		textbox(automationspages.Conversation_TagInput).sendKeys("a");
		textbox(automationspages.Conversation_TagInput).sendKeys(Keys.ENTER);
		logger.info("Enter data in search tags : a ");
		waitAndLog(2);

		if (verifyElementIsEnabled(automationspages.Conversation_A_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			textbox(automationspages.Conversation_A_Tag).isEnabled();
			System.out.println("Automations a Tag Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(automationspages.Conversation_ABC_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			textbox(automationspages.Conversation_ABC_Tag).isEnabled();
			System.out.println("Automations abc tag  Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);
		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);

	}

	public void Automations_From_tools_Delayed_tag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
		waitAndLog(2);
		textbox(automationspages.Conversation_TagInput).sendKeys("a");
		textbox(automationspages.Conversation_TagInput).sendKeys(Keys.ENTER);
		logger.info("Enter data in search tags : a ");
		waitAndLog(100);

		if (verifyElementIsEnabled(automationspages.Conversation_A_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			textbox(automationspages.Conversation_A_Tag).isEnabled();
			System.out.println("Automations a Tag Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		if (verifyElementIsEnabled(automationspages.Conversation_ABC_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			textbox(automationspages.Conversation_ABC_Tag).isEnabled();
			System.out.println("Automations abc tag  Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);
		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);

	}

	public void Automations_Tag_Add_to_Remove() throws Exception {

		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
		waitAndLog(2);
		textbox(automationspages.Conversation_TagInput).sendKeys("abcde");
		textbox(automationspages.Conversation_TagInput).sendKeys(Keys.ENTER);
		logger.info("Enter data in search tags : a ");
		waitAndLog(2);

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
	}

	public void Automations_Tag_Add_to_Remove_username2() throws Exception {

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + "automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(2);
		logger.info("wait for 2 sec");

		waitAndLog(2);
		button(automationspages.Auto1_Contact, "Auto1_Contact").click();
		logger.info("click on Auto1_Contact");
		waitAndLog(2);
		textbox(automationspages.Conversation_TagInput).sendKeys("abc");
		textbox(automationspages.Conversation_TagInput).sendKeys(Keys.ENTER);
		logger.info("Enter data in search tags : a ");
		waitAndLog(2);

		link(repository.tools_bttn, "Click on Tools link").click();
		logger.info("Lets go button clicked");

		button(automationspages.tools_Automations, "tools_Automations").click();
		logger.info("tools_Automations");
	}

	public void Automations_From_tools_Delayed_RemoveTag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
		waitAndLog(2);
		textbox(automationspages.Conversation_TagInput).sendKeys("a");
		textbox(automationspages.Conversation_TagInput).sendKeys(Keys.ENTER);
		logger.info("Enter data in search tags : a ");
		waitAndLog(100);

		if (verifyElementIsEnabled(automationspages.Conversation_A_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			textbox(automationspages.Conversation_A_Tag).isEnabled();
			System.out.println("Automations a Tag Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);
		System.out.println("abcde Tag Deleted");

	}

	public void Automations_From_tools_Immediately_RemoveTag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
		waitAndLog(2);
		textbox(automationspages.Conversation_TagInput).sendKeys("a");
		textbox(automationspages.Conversation_TagInput).sendKeys(Keys.ENTER);
		logger.info("Enter data in search tags : a ");
		waitAndLog(2);

		if (verifyElementIsEnabled(automationspages.Conversation_A_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			textbox(automationspages.Conversation_A_Tag).isEnabled();
			System.out.println("Automations a Tag Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_A_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);
		System.out.println("abcde Tag Deleted");

	}

	public void Automations_MSG_Tag_Delayed_tag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.Auto1_Contact, "Click on Auto1_Contact").click();
		logger.info("click on Auto1_Contact button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello");
		logger.info("stop");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "Click on Send Button").click();
		logger.info("click on send");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(100);
		button(automationspages.Contact_Tab, "Contact_Tab").click();
		logger.info("click on Contact_Tab");
		waitAndLog(2);
		button(automationspages.Conversation_Tab, "Conversation_Tab").click();
		logger.info("click on Conversation_Tab");
		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");

		if (verifyElementIsEnabled(automationspages.Conversation_ABC_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			textbox(automationspages.Conversation_ABC_Tag).isEnabled();
			System.out.println("Automations abc tag  Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);

	}

	public void Automations_MSG_Remove_Tag_Delayed_tag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.Automation_Contact, "Click on Automation_Contact").click();
		logger.info("click on Automation_Contact button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello");
		logger.info("stop");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "Click on Send Button").click();
		logger.info("click on send");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("Email id :" + "automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(100);

	}

	public void Automations_MSG_Tag_Immediately_tag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info(password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);

		button(repository.Auto1_Contact, "Click on Auto1_Contact").click();
		logger.info("click on Auto1_Contact button");
		waitAndLog(2);

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello");
		logger.info("stop");
		waitAndLog(2);

		button(repository.chatInputsendButton, "Click on Send Button").click();
		logger.info("click on send");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + "tech1234");
		waitAndLog(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(2);
		button(automationspages.Contact_Tab, "Contact_Tab").click();
		logger.info("click on Contact_Tab");
		waitAndLog(2);
		button(automationspages.Conversation_Tab, "Conversation_Tab").click();
		logger.info("click on Conversation_Tab");
		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");

		if (verifyElementIsEnabled(automationspages.Conversation_ABC_Tag)) {
			Assert.assertEquals(true, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			textbox(automationspages.Conversation_ABC_Tag).isEnabled();
			System.out.println("Automations abc tag  Enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, verifyElementIsEnabled(automationspages.Conversation_ABC_Tag));
			System.out.println("Failed");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		button(automationspages.Conversation_Tag_Close, "Conversation_Tag_Close").click();
		logger.info("click on Conversation_Tag_Close");
		waitAndLog(2);
		button(automationspages.Conversation_Remove_Tag, "Conversation_Remove_Tag").click();
		logger.info("click on Conversation_Remove_Tag");
		waitAndLog(2);

	}

	public void Automations_MSG_RemoveTag_Immediately_tag_close() throws Exception {

		waitAndLog(2);
		button(repository.tools_crossBtn, "button").click();
		logger.info("click on cross button");
		waitAndLog(2);

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("automations@yopmail.com");
		logger.info("automations@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("tech1234");
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		logger.info("click on login button");
		waitAndLog(2);

		button(repository.Auto1_Contact, "Click on Auto1_Contact").click();
		logger.info("click on Auto1_Contact button");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		textbox(repository.chatInputTextareaTxt).sendKeys("Hello");
		logger.info("stop");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		button(repository.chatInputsendButton, "Click on Send Button").click();
		logger.info("click on send");
		waitAndLog(2);
		logger.info("wait for 2 sec");

		validateSignOutLink();
		validateSignOut();

		textbox(Repository.user_id).sendKeys("auto1@yopmail.com");
		logger.info("Email id :" + "auto1@yopmail.com");
		textbox(Repository.Passsword_ID).sendKeys("tech1234");
		logger.info("Password:" + password);
		waitAndLog(2);

		button(Repository.loginbttn, "login submit").submit();
		waitAndLog(2);
		button(automationspages.Contact_Tab, "Contact_Tab").click();
		logger.info("click on Contact_Tab");
		waitAndLog(2);
		button(automationspages.Conversation_Tab, "Conversation_Tab").click();
		logger.info("click on Conversation_Tab");
		waitAndLog(2);
		button(automationspages.Automation_Contact, "Automation_Contact").click();
		logger.info("click on Automation_Contact");
	}

}
