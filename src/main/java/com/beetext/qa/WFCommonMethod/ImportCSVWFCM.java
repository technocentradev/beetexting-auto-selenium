package com.beetext.qa.WFCommonMethod;

import java.util.Map;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;
import com.beetext.qa.pages.Repository;
import com.beetext.qa.pages.importCSV;
import com.beetext.qa.util.TestUtil;

public class ImportCSVWFCM extends CommonMethods {
	
	String url, username1, username2, password;
	Repository repository = new Repository();
	importCSV importCSV= new importCSV();
	TestUtil testUtil = new TestUtil();
	TestBase testBase =new TestBase();

	
	public void login(Map<String,String> map) throws Exception {
		username1 = (String) map.get("username1");
		password = (String) map.get("password");
		System.out.println("username1-->" + username1);
		wait(5);
		textbox(Repository.user_id).sendKeys(username1);
		logger.info("Email id :"+ username1);
		wait(2);
		logger.info("wait for 2 sec");
		textbox(Repository.Passsword_ID).sendKeys(password);
		logger.info("Password"+password);
		wait(2);
		logger.info("wait for 2 sec");
		button(Repository.loginbttn, "login submit").submit();
		logger.info("Login button clicked");
		waits(5);
		logger.info("wait for 2 sec");
		
	}
	
	public void clickonimportcsvicon() throws Exception {
		button(importCSV.contact_tab, "contact tab").click();
		logger.info("clicked on contact tab");
		waits(7);
		logger.info("wait for 10 sec");
		
		button(importCSV.import_svg, "import SVG").click();
		logger.info("clicked on import csv icon");
		waits(5);
		logger.info("wait for 10 sec");
		button(importCSV.select_file, "import SVG").click();
		logger.info("clicked on select file button");
		waits(10);
		logger.info("wait for 10 sec");
	}
}
