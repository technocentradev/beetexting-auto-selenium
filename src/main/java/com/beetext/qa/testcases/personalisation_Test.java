package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.PersonalisationWF;
import com.beetext.qa.base.TestBase;

public class personalisation_Test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		PersonalisationWF PWF = new PersonalisationWF();
		PWF.auto1_Login();
	}

	@Test(priority = 1)
	public void PM_290_Persoanlisation_BM() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.Persoanlisation_BM();

	}

	@Test
	public void PM_291_Persoanlisation_BM_title_Below_PM() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.Persoanlisation_BM_title_Below_PM();

	}

	@Test
	public void PM_292_Persoanlisation_questionMark_Content() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.Persoanlisation_questionMark_Content();

	}

	@Test
	public void PM_293_294_Persoanlisation_firstname_Companyname_Content() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.Persoanlisation_firstname_Companyname_Content();

	}

	@Test
	public void PM_295_PM_BM_doesnot_first_Comapany() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_BM_doesnot_first_Comapany();

	}

	@Test
	public void PM_296_PM_BM_first_Comapany() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_BM_first_Comapany();

	}

	@Test
	public void PM_297_PM_BM_onlyfirst() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_BM_onlyfirst();

	}

	@Test
	public void PM_298_PM_BM_onlycompanyname() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_BM_onlycompanyname();

	}

	@Test
	public void PM_299_PM_firstname_SpecialChar_Comapany() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_firstname_SpecialChar_Comapany();

	}

	@Test
	public void PM_300_Persoanlisation_Msg_Templates() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.Persoanlisation_Msg_Templates();

	}

	@Test
	public void PM_301_Persoanlisation_Templates_title_Below_PM() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.Persoanlisation_Templates_title_Below_PM();

	}

	@Test
	public void PM_302_PM_Templates_In_Conversation_firstname_Comapany() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_Templates_In_Conversation_firstname_Comapany();

	}

	@Test
	public void PM_303_PM_Templates_In_Single_Conversation_firstname_Comapany() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_Templates_In_Single_Conversation_firstname_Comapany();

	}

	@Test
	public void PM_304_PM_Group_Conversation_PM_Template_disabled() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_Group_Conversation_PM_Template_disabled();

	}

	@Test
	public void PM_305_PM_compose_add_2Contacts_PM_Template_disabled() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_compose_add_2Contacts_PM_Template_disabled();

	}

	@Test
	public void PM_306_PM_doesnot_first_Comapany() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_doesnot_first_Comapany();

	}

	@Test
	public void PM_307_PM_first_Comapany() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_first_Comapany();

	}

	@Test
	public void PM_308_PM_only_firstname() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_only_firstname();

	}

	@Test
	public void PM_309_PM_only_Companyname() throws Exception {

		PersonalisationWF PWF = new PersonalisationWF();
		PWF.PM_only_Companyname();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
