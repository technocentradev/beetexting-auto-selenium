package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Reviews_Workflow_CM;
import com.beetext.qa.Workflows.Review_Page_WF;
import com.beetext.qa.base.TestBase;

public class Review_List_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Reviews_Workflow_CM RCM = new Reviews_Workflow_CM();
		RCM.login2();

//	   Login();   for login

	}

	@Test(priority = 1) // 19
	public void TC_234_Manage_review_page_click_on_Review_Tab_from_layout_popup() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Functionality_of_Review_tab_in_manage_Layout_popup();

	}

	@Test(priority = 2) // 20
	public void TC_235_Verify_ADDREVIEWSITE_button_Isenabled_in_Review_list_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_ADDREVIEWSITE_button_Isenabled_in_Review_list_page();

	}

	@Test(priority = 3) // 21
	public void TC_237_Title_Toagle_Manage_buttons_Isavailable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Title_Toagle_Manage_buttons_Isavailable();

	}

	@Test(priority = 4) // 22
	public void TC_238_SelectAll_toggle_should_Turn_off_by_click_on_toggle_button() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.selectAll_toggle_should_Turn_off_by_click_on_any_toggle_button();

	}

	@Test(priority = 5) // 23
	public void TC_239_Verify_record_toggle_button_isdisabled_by_click_on_SelectAll_toggle_button() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.functionality_of_SelectAll_toggle_button();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
