package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.UploadNonMediaAttachmentsWF;
import com.beetext.qa.base.TestBase;

public class UploadNonMediaAttachment extends TestBase {

	// @BeforeMethod
	@BeforeClass
	public void setUp() {
		initialization();
		// Login(); //for login

	}

	@Test(priority=31)
	public void Tc_MessageandVideo_and_Document_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Video_plus_Document_combination();

	}

	@Test(priority=32)
	public void Tc_MessageandVideo_and_PDF_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Video_plus_PDF_combination();

	}

	@Test(priority=33)
	public void Tc_MessageandVideo_and_XLS_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Video_plus_XLS_combination();

	}

	@Test(priority=34)
	public void Tc_MessageandVideo_and_CSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Video_plus_CSV_combination();

	}

	@Test(priority=35)
	public void Tc_MessageandVideo_and_Text_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Video_plus_Text_combination();

	}

	@Test(priority=36)
	public void Tc_MessageandAudio_and_Document_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Audio_plus_Document_combination();

	}

	@Test(priority=37)
	public void Tc_MessageandAudio_and_PDF_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Audio_plus_PDF_combination();

	}

	@Test(priority=38)
	public void Tc_MessageandAudio_and_XLS_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Audio_plus_XLS_combination();

	}

	@Test(priority=39)
	public void Tc_MessageandAudio_and_CSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Audio_plus_CSV_combination();

	}

	@Test(priority=40)
	public void Tc_MessageandAudio_and_Text_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Audio_plus_Text_combination();

	}

	@Test(priority=41)
	public void Tc_MessageandMP3_Audio() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_MP3_Audio();

	}

	@Test(priority=42)
	public void Tc_Messageand3GP_Audio() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_3GP_Audio();

	}

	@Test(priority=43)
	public void Tc_MessageandAMR_Audio() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_AMR_Audio();

	}

	@Test(priority=44)
	public void Tc_MessageandWEBM_Audio() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_WEBM_Audio();

	}

	@Test(priority=45)
	public void Tc_MessageandMP3_Audio_plus_3gp_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_mp3_Audio_plus_3gp_combination();

	}

	@Test(priority=46)
	public void Tc_MessageandMP3_Audio_plus_AMR_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_mp3_Audio_plus_AMR_combination();

	}

	@Test(priority=47)
	public void Tc_MessageandMP3_Audio_plus_WEBM_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_mp3_Audio_plus_WEBM_combination();

	}

	@Test(priority=48)
	public void Tc_Messageand3gp_Audio_plus_MP3_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_3gp_Audio_plus_MP3_combination();

	}

	@Test(priority=49)
	public void Tc_Messageand3gp_Audio_plus_AMR_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_3Gp_Audio_plus_AMR_combination();

	}

	@Test(priority=50)
	public void Tc_Messageand3gp_Audio_plus_WEBM_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_3gp_Audio_plus_WEBM_combination();

	}

	@Test(priority=51)
	public void Tc_MessageandAMR_Audio_plus_MP3_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_AMR_Audio_plus_MP3_combination();

	}

	@Test(priority=52)
	public void Tc_MessageandAMR_Audio_plus_3GP_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_AMR_Audio_plus_3GP_combination();

	}

	@Test(priority=53)
	public void Tc_MessageandAMR_Audio_plus_WEBM_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_AMR_Audio_plus_WEBM_combination();

	}

	@Test(priority=54)
	public void Tc_MessageandWEBM_Audio_plus_MP3_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_WEBM_Audio_plus_MP3_combination();

	}

	@Test(priority=55)
	public void Tc_MessageandWEBM_Audio_plus_3GP_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_WEBM_Audio_plus_3GP_combination();

	}

	

	@AfterMethod
	public void logout() throws Exception {
		VCard_Signout();
	}
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
