package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.Workflows.broadcast.Tools_BM_Draft_WF;
import com.beetext.qa.base.TestBase;

public class Tools_BM_Draft_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {

		initialization();
		Broadcast BM = new Broadcast();
		BM.BM_tools_login();

	}

	@Test(priority = 1)
	public void tools_505_tools_BM_page_Draft_display_list() throws Exception {

		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_display_list();
	}

	@Test(priority = 2)
	public void tools_506_tools_BM_page_Draft_ChangeInBM_saveAsDraft() throws Exception {

		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_ChangeInBM_saveAsDraft();
	}

	@Test(priority = 3)
	public void tools_507_508_tools_BM_page_Draft_deleteBM_saveAsDiscard() throws Exception {

		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_deleteBM_saveAsDiscard();
	}

	@Test(priority = 4)
	public void tools_509_tools_BM_page_Draft_deleteBM_cancel() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_deleteBM_cancel();
	}

	@Test(priority = 5)
	public void tools_510_tools_BM_page_Draft_manage_removetag_sendBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_removetag_sendBM();
	}

	@Test(priority = 6)
	public void tools_511_tools_BM_page_Draft_manage_changeTitle_removeMSG_ScheduleBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_changeTitle_removeMSG_ScheduleBM();
	}

	@Test(priority = 7)
	public void tools_512_513_tools_BM_page_Draft_manage_changeTitle_click_NavigatonLink_saveBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_changeTitle_click_NavigatonLink_saveBM();
	}

	@Test(priority = 8)
	public void tools_514_tools_BM_page_Draft_manage_changeTitle_click_NavigatonLink_DiscardBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_changeTitle_click_NavigatonLink_DiscardBM();
	}

	@Test(priority = 9)
	public void tools_515_tools_BM_page_Draft_manage_DeleteBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_DeleteBM();
	}

	@Test(priority = 10)
	public void tools_516_tools_BM_page_Draft_manage_fillAllDetails_sendBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_fillAllDetails_sendBM();
	}

	@Test(priority = 11)
	public void tools_517_tools_BM_page_Draft_manage_fillAllDetails_ScheduleBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_fillAllDetails_ScheduleBM();
	}

	@Test(priority = 12)
	public void tools_518_519_tools_BM_page_Draft_manage_addAttachment_ClickSM_saveBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_addAttachment_ClickSM_saveBM();
	}

	@Test(priority = 13)
	public void tools_520_tools_BM_page_Draft_manage_addAttachment_ClickSM_DiscardBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_addAttachment_ClickSM_DiscardBM();
	}

	@Test(priority = 14)
	public void tools_521_522_tools_BM_page_Draft_manage_addAttachment_ClickTemplates_saveBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_addAttachment_ClickTemplates_saveBM();
	}

	@Test(priority = 15)
	public void tools_523_tools_BM_page_Draft_manage_addAttachment_ClickTemplates_DiscardBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_addAttachment_ClickTemplates_DiscardBM();
	}

	@Test(priority = 16)
	public void tools_524_525_tools_BM_page_Draft_manage_Select_recurringIcon_ClickTemplates_saveBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_Select_recurringIcon_ClickTemplates_saveBM();
	}

	@Test(priority = 17)
	public void tools_526_tools_BM_page_Draft_manage_Select_recurringIcon_ClickTemplates_DiscardBM() throws Exception {

		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_Select_recurringIcon_ClickTemplates_DiscardBM();
	}

	@Test(priority = 18)
	public void tools_527_528_tools_BM_page_Draft_manage_Select_recurringIcon_ClickSM_saveBM() throws Exception {

		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_Select_recurringIcon_ClickSM_saveBM();
	}

	@Test(priority = 19)
	public void tools_529_tools_BM_page_Draft_manage_Select_recurringIcon_ClickSM_DiscardBM() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_Select_recurringIcon_ClickSM_DiscardBM();
	}

	@Test(priority = 20)
	public void tools_530_tools_BM_page_Draft_manage_Scheduletime_updated_withDateTime() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_Scheduletime_updated_withDateTime();
	}

	@Test(priority = 21)
	public void tools_531_tools_BM_page_Draft_manage_Scheduletime_updated_No_dateSelected() throws Exception {
		Tools_BM_Draft_WF Tools_BM_Draft_WF = new Tools_BM_Draft_WF();
		Tools_BM_Draft_WF.tools_BM_page_Draft_manage_Scheduletime_updated_No_dateSelected();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
