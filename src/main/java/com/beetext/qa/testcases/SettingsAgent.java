package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.settings.SettingsAgentsWF;
import com.beetext.qa.base.TestBase;

public class SettingsAgent extends TestBase {
	Map<String, String> map = new HashMap<String, String>();

	@BeforeClass
	public void setUp() {
		initialization();
		Login(); // for login
		driver.findElement(By.xpath("//span[@class='s-title']")).click();

	}

	@Test(priority = 1)
	public void Settings_080_agent_name_required_validation() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_name_required_validation();

	}

	@Test(priority = 2)
	public void Settings_082_agent_name_required_3char_validation() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_name_required_3char_validation();

	}

	@Test(priority = 3)
	public void _Settings_083_agent_email_required_validation() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_email_required_validation();
	}

	@Test(priority = 4)
	public void Settings_085_agent_email_duplicate_validation() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_email_duplicate_validation();

	}

	@Test(priority = 5)
	public void Settings_084_agent_email_not_valid() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_email_not_valid();

	}

	@Test(priority = 6)
	public void Settings_090_agent_email_check_uncheck_validation() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_uncheck_email();

	}

	@Test(priority = 7)
	public void Settings_087_088_089_agent_invite_send_button_enabled() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_send_button_enabled();

	}

	@Test(priority = 8)
	public void Settings_091_92_93_98_agent_invitation_send_reinvitation_delete_agent() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending();

	}

	@Test(priority = 9)
	public void Settings_091_mange_agent_check_uncheck_department() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Manage_Agent_check_uncheck_department();

	}

	@Test(priority = 10)
	public void Settings_099_mange_agent_clickon_delete_and_then_click_on_cancel() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Manageagent_click_agent_delete_and_then_click_on_cancel_button();

	}

	@Test(priority = 11)
	public void Settings_100_sending_user_agent_invitation() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.User_Agent_invite_sending();

	}

	@Test(priority = 12)
	public void Settings_086_sending_agent_invitation_mobile_number_validation_checking() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_phone_number_validation();

	}

	@Test(priority = 13)
	public void Settings_086_sending_agent_invitation_Accept() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.User_Agent_invite_sending_accept_the_invitation();

	}

	@Test(priority = 14)
	public void Settings_086_sending_agent_invitation_Reject() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.User_Agent_invite_sending_reject_the_invitation();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
