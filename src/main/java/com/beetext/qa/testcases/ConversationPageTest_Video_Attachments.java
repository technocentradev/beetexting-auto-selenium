package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.ConversationWF_Video_Attachments;
import com.beetext.qa.base.TestBase;

public class ConversationPageTest_Video_Attachments extends TestBase {
	Map<String, String> usercredentials = new HashMap<String, String>();

	@BeforeClass
	public void setUp() {
		initialization();
		String username1 = prop.getProperty("username1");
		String username2 = prop.getProperty("username2");
		String password = prop.getProperty("password");
		usercredentials.put("username1", username1);
		usercredentials.put("username2", username2);
		usercredentials.put("password", password);
	}

	@Test(priority = 1)
	public void message_avifile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_avifile(usercredentials);
	}

	@Test(priority = 2)
	public void message_mp4file() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mp4file(usercredentials);
	}

	@Test(priority = 3)
	public void message_movfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_movfile(usercredentials);
	}

	@Test(priority = 4)
	public void message_mpgfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpgfile(usercredentials);
	}

	@Test(priority = 5)
	public void message_mpegfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpegfile(usercredentials);
	}

	@Test(priority = 6)
	public void message_avifile_mp4file() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_avifile_mp4file(usercredentials);
	}

	@Test(priority = 7)
	public void message_avifile_movfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_avifile_movfile(usercredentials);
	}

	@Test(priority = 8)
	public void message_avifile_mpgfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_avifile_mpgfile(usercredentials);
	}

	@Test(priority = 9)
	public void message_avifile_mpegfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_avifile_mpegfile(usercredentials);
	}

	@Test(priority = 10)
	public void message_mp4file_avifile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mp4file_avifile(usercredentials);
	}

	@Test(priority = 11)
	public void message_mp4file_movfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mp4file_movfile(usercredentials);
	}

	@Test(priority = 12)
	public void message_mp4file_mpgfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mp4file_mpgfile(usercredentials);
	}

	@Test(priority = 13)
	public void message_mp4file_mpegfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mp4file_mpegfile(usercredentials);
	}

	@Test(priority = 14)
	public void message_movfile_avifile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_movfile_avifile(usercredentials);
	}

	@Test(priority = 15)
	public void message_movfile_mp4file() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_movfile_mp4file(usercredentials);
	}

	@Test(priority = 16)
	public void message_movfile_mpgfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_movfile_mpgfile(usercredentials);
	}

	@Test(priority = 17)
	public void message_movfile_mpegfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_movfile_mpegfile(usercredentials);
	}

	@Test(priority = 18)
	public void message_mpgfile_avifile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpgfile_avifile(usercredentials);
	}

	@Test(priority = 19)
	public void message_mpgfile_mp4file() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpgfile_mp4file(usercredentials);
	}

	@Test(priority = 20)
	public void message_mpgfile_movfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpgfile_movfile(usercredentials);
	}

	@Test(priority = 21)
	public void message_mpgfile_mpegfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpgfile_mpegfile(usercredentials);
	}

	@Test(priority = 22)
	public void message_mpegfile_avifile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpegfile_avifile(usercredentials);
	}

	@Test(priority = 23)
	public void message_mpegfile_mp4file() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpegfile_mp4file(usercredentials);
	}

	@Test(priority = 24)
	public void message_mpegfile_movfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpegfile_movfile(usercredentials);

	}

	@Test(priority = 25)
	public void message_mpegfile_mpgfile() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpegfile_mpgfile(usercredentials);
	}

	@Test(priority = 26)
	public void message_avifile_image() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_avifile_image(usercredentials);
	}

	@Test(priority = 27)
	public void message_mp4file_image() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mp4file_image(usercredentials);
	}

	@Test(priority = 28)
	public void message_movfile_image() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_movfile_image(usercredentials);
	}

	@Test(priority = 29)
	public void message_mpgfile_image() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpgfile_image(usercredentials);
	}

	@Test(priority = 30)
	public void message_mpegfile_image() throws Exception {

		ConversationWF_Video_Attachments conversationwf = new ConversationWF_Video_Attachments();
		conversationwf.message_mpegfile_image(usercredentials);
	}

	@AfterMethod
	public void validate_Signout() throws Exception {
		VCard_Signout();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
