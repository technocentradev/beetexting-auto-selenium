package com.beetext.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.signup.CreateDepartmentWF;
import com.beetext.qa.base.TestBase;

public class CreateDepartments extends TestBase {

	

	@BeforeMethod
	public void setUp() {
		initialization();
		// Login();//for login

	}

	@Test()
	public void Signup_070_createNewNumber() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Enabled_CreateDepartment_Button();

	}

	@Test()
	public void Signup_068_DepartmentNameRequired() throws Exception {
		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Error_Dept_Name();

	}

	@Test()
	public void Signup_067_DepartmentNameminimum3char() throws Exception {


		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Error_Min3_Char_Dept_Name();

	}

	@Test()
	public void Signup_069_NextButtonEnabled() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Next_Button_Enabled();

	}

	@Test
	public void CreateButton_Enabled() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.CreateBtn_Enabled();

	}

	@Test
	public void Signup_072_invalid_Area_code() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.inValidarea_Code();

	}

	@Test
	public void Err_required_Area_code() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.areaCode_Required();

	}

	@Test
	public void Signup_071_valid_Area_Code() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Validarea_Code();

	}

	@Test
	public void Signup_073_Enable_TextEnable_Exsting() throws Exception {


		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Enabled_TextEnabledExistingNumber_Button();

	}

	@Test
	public void Signup_074_Err_required_DepartmentName() throws Exception {


		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Error_TextenableDept_Name();

	}

	@Test
	public void Signup_075_Err_reaquired3Char_DepartmentName() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.Error_Min3Char_Dept_Name();

	}

	@Test
	public void Signup_077_phoneNumber_infopage_verifying() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.getPhone_Numberinfo_page();

	}

	@Test
	public void Signup_080_phoneNumber_infopage_CustName3characters_validation() throws Exception {


		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.getPhone_Numberinfo_CustName();

	}

	@Test
	public void Signup_082_phoneNumber_infopage_zipcode_Errorcase_validation() throws Exception {

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.error_zipCode();

	}

	@Test
	public void Signup_085_phoneNumber_infopage_NextButton_Enabled() throws Exception {


		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.NextButtonEnabled();

	}

	@Test
	public void Signup_094_enter_valid_verificatio_code() throws Exception {

		

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.phoneNumberinfo_validData();

	}

	@Test
	public void Signup_092_clickon_callme_Again_Button() throws Exception {

	
		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.phoneNumberinfo_callme_Again();

	}

	@Test
	public void Signup_099_phoneNumber_Already_Exist() throws Exception {

		

		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.phoneNumberinfo_mobilealrady_exist();

	}

	@Test
	public void Signup_phone_info_page_uncheck_terms_conditions() throws Exception {


		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.phoneNumberinfo_uncheck_terms_conditions_check_Mark();

	}

	@Test
	public void Signup_phone_info_page_cityName_not_allowed_special_Char() throws Exception {

		
		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.phoneNumberinfo_cityName_notallow_special_Char();

	}

	@Test
	public void Signup_phone_info_page_required_validation_cityName() throws Exception {

	
		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.phoneNumberinfo_required_validation_checking();

	}

	@Test
	public void Signup_087_back_buttoncase() throws Exception {

		
		CreateDepartmentWF createDept = new CreateDepartmentWF();
		createDept.backto_PhoneNumber_info();

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
