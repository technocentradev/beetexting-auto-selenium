package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.templates.QuickTemplates;
import com.beetext.qa.base.TestBase;

public class Templates extends TestBase {
	private Map<String, String> map = new HashMap<String, String>();

	@BeforeClass
	public void setUp() {
		initialization();
		Login();

	}

	@Test(priority = 1)
	public void Tc_158_send_PersonalTemplate() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_personal_Template();

	}

	@Test(priority = 2)
	public void Tc_159_send_PersonalTemplate_ModifyText() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_personal_Temp_modify_text();

	}

	@Test(priority = 3)
	public void Tc_164_send_personal_template_with_attachmets_modified_text() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_personal_Template_with_attachment_modify_text();

	}

	@Test(priority = 4)
	public void Tc_172_send_sharedTemplate_with_attachments_modify_text() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_shared_Template_with_attachment_modify_text();

	}

	@Test(priority = 5)
	public void Tc_174_create_personal_template() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.create_personal_Template();

	}

	@Test(priority = 6)
	public void Tc_175_create_shared_template() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.create_shared_Template();

	}

	@Test(priority = 7)
	public void Tc_166_send_sharedTemplate() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_shared_Template();

	}

	@Test(priority = 8)
	public void Tc_167_send_Modify_sharedTemplate() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_shared_Temp_modify_text();

	}

	@Test(priority = 9)
	public void Tc_160send_PersonalTemplate_Add_attachment() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_personal_Temp_add_Attachment();

	}

	@Test(priority = 10)
	public void Tc_171_send_sharedTemplate_Add_attachment() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.sending_shared_Temp_add_Attachment();

	}

	@Test(priority = 11)
	public void Tc_163_select_textattachment_personal_Template_Add_attachment() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.selecting_text_oneattach_personal_Temp_addone_more_Attachment();

	}

	@Test(priority = 12)
	public void Tc_171_select_textattachment_shared_Template_Add_attachment() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.selecting_text_oneattach_shared_Temp_addone_more_Attachment();

	}

	@Test(priority = 13)
	public void Tc_180_select_10attachment_shared_Template() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.selecting_10attachments_shared_Template();

	}

	@Test(priority = 14)
	public void Tc_177_select_10attachment_personal_Template() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.selecting_10attachments_personal_Template();

	}

	@Test(priority = 15)
	public void Tc_181_select_morethan10attachment_shared_Template() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.selecting_addmorethan10attachments_shared_Template();

	}

	@Test(priority = 16)
	public void Tc_178_select_morethan10attachment_personal_Template() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.selecting_morethan10attachments_personal_Template();

	}

	@Test(priority = 17)
	public void Tc_183_quicktemplate_popup_open() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.quick_template_popup_opening_headding_validating();

	}

	@Test(priority = 18)
	public void Tc_190_quicktemplate_add_attachments() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.quick_template_select_template_modify_attachments();

	}

	@Test(priority = 19)
	public void Tc_191_quicktemplate_morethan_10_attachment() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.quick_template_select_template_add_morethan_10_attachments();

	}

	@Test(priority = 20)
	public void Tc_189_quicktemplate_modify_text() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.quick_template_select_template_modify_text();

	}

	@Test(priority = 21)
	public void Tc_176_personal_template_add_attachemnt_giffy_emoji() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.quick_template_select_template__add_gif_attachment();

	}

	@Test(priority = 22) // below three cases are priority cases
	public void Tc_184_quicktemplate_no_quick_template() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.quick_template_no_quick_template();

	}

	@Test(priority = 23)
	public void Tc_155_template_personal_default_Message() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.template_creation_defaulttemplate_Message();

	}

	@Test(priority = 24)
	public void Tc_155_1_template_shared_default_Message() throws Exception {
		QuickTemplates QuickTemplates = new QuickTemplates();
		QuickTemplates.template_creation_shareddefaulttemplate_Message();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
