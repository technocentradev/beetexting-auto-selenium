package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.Workflows.scheduled.Tools_SM_WF;
import com.beetext.qa.base.TestBase;

public class Tools_SM_Edit_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Tools_SM_WFCM SMCM = new Tools_SM_WFCM();
		SMCM.Login2_Tools_SM();

		// Login(); for login
	}

	// Edit cases started

	@Test(priority = 1)

	public void TC_158_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_158_UI_Error_message_for_max_Contacts();
	}

	@Test(priority = 2)
	public void TC_159_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_159_verify_error_message_for_multiple_contacts_for_TollFree();
	}

	@Test(priority = 3)
	public void TC_160_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_160_Functtionality_of_Delete_icon_of_contact_in_Manage_page();
	}

	@Test(priority = 4)
	public void TC_161_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_161_Validation_message_for_1000_char_in_message_textbox_for_Edit_case();
	}

	@Test(priority = 5)
	public void TC_162_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_162_Allows_Max_10_attachments();
	}

	@Test(priority = 6)
	public void TC_166_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_166_Allows_Max_10_attachments();
	}

	@Test(priority = 7)
	public void TC_167_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_167_Verify_SM_scheduled_with_attachment();
	}

	@Test(priority = 8)
	public void TC_168_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_168_FeatureTime_Date_Warning_message();
	}

	@Test(priority = 9)
	public void TC_169_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_169_Functionality_of_SM_button_add_recipent_in_Manage_page();
	}

	@Test(priority = 79)
	public void TC_170_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_170_Verify_Edited_message();
	}

	@Test(priority = 11)
	public void TC_172_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_172_Functionality_of_Recurring_check_button_in_Manage_page();
	}

	@Test(priority = 12)
	public void TC_173_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_173_verify_Recurring_icon_in_upcoming_list_for_Edit_record();
	}

	@Test(priority = 13)
	public void TC_174_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_174_Verify_Recurring_Record_as_normal_record_in_upcoming_list_for_Edit_record();
	}

	@Test(priority = 14)
	public void TC_176_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_176_Verify_Edit_message_for_Saved_record_for_Edit_record();
	}

	@Test(priority = 15)
	public void TC_177_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_177_Verify_edited_attachment();
	}

	@Test(priority = 16)
	public void TC_178_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_178_Verifying_Updated_recurring_record();
	}

	@Test(priority = 17)
	public void TC_179_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_179_Verify_edited_record_information();
	}

	@Test(priority = 18)
	public void TC_180_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_180_Verifying_Updated_recurring_record();
	}

	@Test(priority = 19)
	public void TC_181_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_181_Verify_validation_message_for_previous_date();
	}

	@Test(priority = 20)
	public void TC_183_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_183_Verifying_Updated_recurring_record();
	}

	@Test(priority = 21)
	public void TC_184_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_184_Verifying_Updated_recurring_record();
	}

	@Test(priority = 22)
	public void TC_185_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_185_Verifying_Updated_text_message_in_manage_page();
	}

	@Test(priority = 23)
	public void TC_186_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_186_Verify_edited_attachment();
	}

	@Test(priority = 24)
	public void TC_187_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_187_Verifying_Updated_text_message_recipent_in_manage_page();
	}

	@Test(priority = 25)
	public void TC_188_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_188_Verify_edited_record_information();
	}

	@Test(priority = 26)
	public void TC_189_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_189_Verifying_Updated_recurring_record();
	}

	@Test(priority = 27)
	public void TC_190_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_190_Verify_validation_message_for_previous_date();
	}

	@Test(priority = 28)
	public void TC_191_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_191_Verifying_Updated_recurring_record();
	}

	@Test(priority = 29)
	public void TC_192_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_192_Verifying_Updated_recurring_record();
	}

	@Test(priority = 30)
	public void TC_193_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_193_Verifying_Updated_text_message_in_manage_page();
	}

	@Test(priority = 31)
	public void TC_194_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_194_Verify_edited_attachment();
	}

	@Test(priority = 32)
	public void TC_195_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_195_Verifying_Updated_text_message_recipent_in_manage_page();
	}

	@Test(priority = 33)
	public void TC_196_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_196_Verify_edited_record_information();
	}

	@Test(priority = 34)
	public void TC_197_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_197_Verifying_Updated_recurring_record();
	}

	@Test(priority = 35)
	public void TC_198_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_198_Verify_validation_message_for_previous_date();
	}

	@Test(priority = 36)
	public void TC_199_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_199_Verifying_Updated_recurring_record();
	}

	@Test(priority = 37)
	public void TC_200_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_200_Verifying_Updated_recurring_record();
	}

	@Test(priority = 38)
	public void TC_201_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_201_Verifying_Updated_text_message_in_manage_page();
	}

	@Test(priority = 39)
	public void TC_202_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_202_Verify_edited_attachment();
	}

	@Test(priority = 40)
	public void TC_203_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_203_Verifying_Updated_text_message_recipent_in_manage_page();
	}

	@Test(priority = 41)
	public void TC_204_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_204_Verify_edited_record_information();
	}

	@Test(priority = 42)
	public void TC_205_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_205_Verifying_Updated_recurring_record();
	}

	@Test(priority = 43)
	public void TC_206_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_206_Verify_validation_message_for_previous_date();
	}

	@Test(priority = 44)
	public void TC_207_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_207_Functionality_of_SaveDraft_button_in_manage_page_for_nonrecurring_record();
	}

	@Test(priority = 45)
	public void TC_208_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_208_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 46)
	public void TC_209_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_209_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 47)
	public void TC_210_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_210_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 48)
	public void TC_211_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_211_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 49)
	public void TC_212_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_212_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 50)
	public void TC_213_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_213_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 51)
	public void TC_214_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_214_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 52)
	public void TC_216_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_216_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 53)
	public void TC_217_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_217_Functionality_of_Save_button_in_popup_for_Other_links();
	}

	@Test(priority = 54)
	public void TC_218_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_218_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 55)
	public void TC_219_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_219_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 56)
	public void TC_220_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_220_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 57)
	public void TC_228_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_228_Functionality_of_SaveDraft_button_in_manage_page();
	}

	@Test(priority = 58)
	public void TC_229_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_229_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 59)
	public void TC_230_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_230_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 60)
	public void TC_232_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_232_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 61)
	public void TC_233_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_233_Functionality_of_Save_button_in_popup_for_breadcrum_link();
	}

	@Test(priority = 62)
	public void TC_235_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_235_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 63)
	public void TC_237_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_237_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 64)
	public void TC_239_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_239_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 65)
	public void TC_240_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_240_Functionality_of_Save_button_in_popup_for_any_other_link();
	}

	@Test(priority = 66)
	public void TC_251_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_251_Functionality_of_SM_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 67)
	public void TC_253_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_253_Functionality_of_Save_Draft_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 68)
	public void TC_254_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_254_Functionality_of_Save_Draft_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 69)
	public void TC_259_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_259_Functionality_of_SM_button_for_NonRecurring_in_manage_page();
	}

	@Test(priority = 70)
	public void TC_261_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_261_Functionality_of_Save_Draft_button_for_NonRecurring_in_manage_page();
	}

	@Test(priority = 71)
	public void TC_262_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_262_Functionality_of_SM_button_for_NonRecurring_in_manage_page();
	}

	@Test(priority = 72)
	public void TC_264_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_264_Functionality_of_Save_Draft_button_for_NonRecurring_in_manage_page();
	}

	@Test(priority = 73)
	public void TC_252_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_252_Functionality_of_SM_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 74)
	public void TC_255_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_255_Functionality_of_SM_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 75)
	public void TC_257_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_257_Functionality_of_SaveDraft_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 76)
	public void TC_260_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_260_Functionality_of_SM_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 77)
	public void TC_263_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_263_Functionality_of_SM_button_for_Recurring_in_manage_page();
	}

	@Test(priority = 78)
	public void TC_265_Tools_SM_Edit_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_265_Functionality_of_SaveDraft_button_for_Recurring_in_manage_page();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
