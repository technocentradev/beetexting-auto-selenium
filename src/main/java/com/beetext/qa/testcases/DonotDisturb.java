package com.beetext.qa.testcases;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.util.TestUtil;

public class DonotDisturb extends TestBase {
	public static WebDriver driver;

	public static void waits(int d) {
		try {
			Thread.sleep((long) (d * 1000));
		} catch (Exception e) {

		}
	}

	public static void openCreateContact() throws InterruptedException {
		// a[@id='contactTab']
		driver.findElement(By.xpath("//a[@id='contactTab']")).click();
		waits(3);

		driver.findElement(By.xpath("//*[@src='assets/grey/plus-icon.svg']//*[name()='svg']")).click();
		waits(5);
	}

	public static void openCreateContacts() throws InterruptedException {

		driver.findElement(By.xpath("//*[@src='assets/grey/plus-icon.svg']//*[name()='svg']")).click();
		waits(5);
	}

	public static void create_avathar_logo() throws InterruptedException {
		driver.findElement(By.xpath("//app-avatar[@class='profile']//div[@class='profile-img editmode-profile']"))
				.click();
		waits(2);
	}

	public static String invalid_avathar() {
		return driver.findElement(By.xpath("//span[contains(text(),'File format is not supported.')]")).getText();
	}

	public static void createContact_Name(String name) {
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys(name);
	}

	public static void create_contact_name_required() {
		driver.findElement(By.xpath("//div[contains(text(),'A new contact must have a name.')]"));
	}

	public static void create_contact_phonenum(String pnum) {
		driver.findElement(By.xpath("//input[@id='contactNumberId']")).sendKeys(pnum);
	}

	public static String removeSpace_end() {
		return driver.findElement(By.xpath("//div[contains(text(),'Please remove space(s) at the end.')]")).getText();
	}

	public static String phonenumber_required() {
		return driver.findElement(By.xpath("//div[contains(text(),'Phone number is required.')]")).getText();
	}

	public static void Enter_email(String email) {
		driver.findElement(By.xpath("//input[@id='contactEmailId']")).sendKeys(email);
	}

	public static String invalid_Email() {
		return driver.findElement(By.xpath("//div[contains(text(),'Enter valid email.')]")).getText();
	}

	public static void company_name(String company) {
		driver.findElement(By.xpath("//input[@id='companyNameId']")).sendKeys(company);
	}

	public static void tags(String tag) {
		driver.findElement(By.xpath("//input[@id='contactTagsId']")).sendKeys(tag);

	}

	public static String create_contac_morethan_50tags() {
		return driver
				.findElement(By.xpath("//div[contains(text(),'This contact has reached the maximum number of tag')]"))
				.getText();
	}

	public static String special_char_notallowed() {
		return driver.findElement(By.xpath("//div[contains(text(),'Special characters not allowed.')]")).getText();
	}

	public static String duplicate_tags() {
		return driver.findElement(By.xpath("//div[contains(text(),'Duplicate tags are not allowed.')]")).getText();
	}

	public static void create_note(String note) {
		driver.findElement(By.xpath("//textarea[@id='contactNotesId']")).sendKeys(note);
	}

	public static void save_Button() {
		// button[@id='contactSaveButton']
		driver.findElement(By.xpath("//button[@id='saveButton']")).click();
	}

	public static void edit_save_Button() {

		driver.findElement(By.xpath("//button[@id='contactSaveButton']")).click();
	}

	public static String createcontact_toaster() {
		// div[@aria-label='Created contact successfully.']
		return driver.findElement(By.xpath("//div[contains(text(),'Created contact successfully.')]")).getText();
	}

	public static String create_contac_phonenum_alreayExist() {
		return driver
				.findElement(By.xpath("//div[contains(text(),'Contact with same phone number already exists in o')]"))
				.getText();
	}

	public static void close_button() {
		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
	}

	public static void toggle() {
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		waits(3);
	}

	public static boolean toggles() {
		return driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).isDisplayed();
	}

	public String Char1000_randomestring() {
		String generatedstring = RandomStringUtils.randomAlphabetic(1000);
		return (generatedstring);
	}

	public void searchQA123contacts() throws InterruptedException {

		driver.findElement(By.xpath("//h4[normalize-space()='QA123@$#%&*?_()']")).click();
		waits(3);
	}

	public void searchQA123contact() throws InterruptedException {

		driver.findElement(By.xpath("//h4[normalize-space()='QA123@$#%&*?_()']")).click();
		waits(3);
		// *[@id='contactPencilIcon']//*[local-name()='svg']
		driver.findElement(By.xpath("//*[@id='contactPencilIcon']//*[name()='svg']")).click();
		waits(3);

	}

	public static void edit_Icon() {
		driver.findElement(By.xpath("//*[@id='contactPencilIcon']//*[name()='svg']")).click();
		waits(3);
	}

	public void searchQA10() throws InterruptedException {
		driver.findElement(By.xpath("//h4[normalize-space()='QA10']")).click();
		waits(3);
		driver.findElement(By.xpath("//*[@id='contactPencilIcon']//*[local-name()='svg']")).click();
		waits(3);

	}

	public String contact_updated_toastermessage() {
		return driver.findElement(By.xpath("//div[contains(text(),'Contact successfully updated.')]")).getText();
	}

	public void editconatct_name(String name) {
		driver.findElement(By.xpath("//input[@id='firstName']")).clear();
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys(name);
	}

	public void editconatct_email(String email) {
		driver.findElement(By.xpath("//input[@id='email-0']")).clear();
		driver.findElement(By.xpath("//input[@id='email-0']")).sendKeys(email);
	}

	public String editconatct_invalidemail() {
		return driver.findElement(By.xpath("//div[contains(text(),'Enter valid email.')]")).getText();
	}

	public void editconatct_companyName(String company) {
		driver.findElement(By.xpath("//input[@id='cName']")).sendKeys(company);
	}

	public void editconatct_companyName_clear() {
		driver.findElement(By.xpath("//input[@id='cName']")).clear();
	}

	public void savebutton() throws InterruptedException {
		waits(2);
		driver.findElement(By.xpath("//button[@id='contactSaveButton']")).click();
	}

	public static boolean createContactIcon() {
		return driver.findElement(By.xpath("//*[@src='assets/grey/plus-icon.svg']//*[name()='svg']")).isDisplayed();
	}

	public static boolean message() {
		waits(5);
		return driver.findElement(By.xpath(" //span[contains(text(),'DonNotSentBMMessage')]")).isDisplayed();

	}

	public static boolean messages() {
		waits(5);
		return driver.findElement(By.xpath(" //span[contains(text(),'DonNotSentBMMessages123')]")).isDisplayed();

	}

	public String randomestring() {
		String generatedstring = RandomStringUtils.randomAlphabetic(8);
		return (generatedstring);
	}

	public static void CreateBM() {
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
		waits(5);
		driver.findElement(By.xpath("//a[@id='broadcastTab']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='createBroadcastButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//input[@id='inputBroadcastTagData']")).sendKeys("dontdistrub");
		waits(3);
		driver.findElement(By.xpath("//div[@class='contact-name d-flex']")).click();
		waits(3);
		driver.findElement(By.xpath("//input[@id='broadcast_name']")).sendKeys("DONotSentBM");
		waits(2);
		driver.findElement(By.xpath("//textarea[@id='broadcast_message']")).sendKeys("DonNotSentBMMessage");
		waits(5);
		driver.findElement(By.xpath("//input[@id='broadcast_sendNow']")).click();
		waits(5);
		driver.findElement(By.xpath("//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='broadcast_SendButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
		waits(5);
	}

	public static void CreateBMs() {
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
		waits(5);
		driver.findElement(By.xpath("//a[@id='broadcastTab']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='createBroadcastButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//input[@id='inputBroadcastTagData']")).sendKeys("dontdistrub");
		waits(3);
		driver.findElement(By.xpath("//div[@class='contact-name d-flex']")).click();
		waits(3);
		driver.findElement(By.xpath("//input[@id='broadcast_name']")).sendKeys("DONotSentBM");
		waits(2);
		driver.findElement(By.xpath("//textarea[@id='broadcast_message']")).sendKeys("DonNotSentBMMessages123");
		waits(5);
		driver.findElement(By.xpath("//input[@id='broadcast_sendNow']")).click();
		waits(5);
		driver.findElement(By.xpath("//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='broadcast_SendButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
		waits(5);
	}

	public String randomeNum() {
		String generatedString2 = RandomStringUtils.randomNumeric(7);
		return (generatedString2);
	}

	@BeforeClass
	public void setUp() throws Exception {
		driver = init("chrome");
		waits(10);
		driver.get("http://automation.beetexting.com/login");
		waits(10);
		driver.manage().window().maximize();
		// Logins();
		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys("auto1@yopmail.com");
		waits(2);
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys("tech1234");
		waits(2);
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		waits(10);

	}

	@Test(priority = 1)
	public void Editcontactpage_Donotdisturb_toggle_available() throws InterruptedException {
		searchQA123contact();
		if (toggles()) {
			Assert.assertEquals(true, toggles());
			toggles();
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, toggles());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(3);
		driver.findElement(By.xpath("//*[@class='cancel-icon']//*[name()='svg']")).click();
		waits(5);
	}

	@Test(priority = 2)
	public void Editcontact_enter_name_email_company_turnon_toggle_save_contact() throws Exception {
		waits(2);
		searchQA123contact();
		editconatct_name("QA123@$#%&*?_()");
		editconatct_companyName("@#$^%$%^&%^&%&^%&");
		editconatct_email("abc@yopmail.com");
		toggle();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}

	}

	@Test(priority = 3)
	public void Editcontact_enter_name_email_company_turnoff_toggle_save_contact() throws Exception {
		waits(2);
		searchQA123contact();
		editconatct_name("QA123@$#%&*?_()");
		editconatct_companyName("@#$^%$%^&%^&%&^%&");
		editconatct_email("abc@yopmail.com");
		toggle();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}

	}

	@Test(priority = 4)
	public void Blocked_contact_Editcontact_enter_name_email_company_turnOn_toggle_save_contact() throws Exception {
		waits(2);
		searchQA123contacts();
		driver.findElement(By.xpath("//*[@id='blockContact']//*[name()='svg']")).click();
		waits(5);
		edit_Icon();
		editconatct_name("QA123@$#%&*?_()");
		editconatct_companyName("@#$^%$%^&%^&%&^%&");
		editconatct_email("abc@yopmail.com");
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}

		waits(5);

		driver.findElement(By.xpath("//*[@id='unblockContact']//*[name()='svg']")).click();
		waits(5);
	}

	@Test(priority = 5)
	public void createBM_for_DoNotDistrub_turnOn_toggle_contact() throws Exception {
		waits(2);
		searchQA123contact();
		editconatct_name("QA123@$#%&*?_()");
		editconatct_companyName("@#$^%$%^&%^&%&^%&");
		editconatct_email("abc@yopmail.com");
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully not updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}
		CreateBM();
		searchQA123contacts();
		if (message()) {
			Assert.assertEquals(true, message());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, message());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	// @Test(priority=6)
	public void createBM_for_DoNotDistrub_turnoff_toggle_contact() throws Exception {
		waits(2);
		searchQA123contact();
		editconatct_name("QA123@$#%&*?_()");
		editconatct_companyName("@#$^%$%^&%^&%&^%&");
		editconatct_email("abc@yopmail.com");
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}
		CreateBM();
		searchQA123contacts();
		if (message()) {
			Assert.assertEquals(true, message());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, message());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	// @Test(priority=7)
	public void createBM_for_DoNotDistrub_turnOn_toggle_contact_anothercontact_toggle_off_contact() throws Exception {
		waits(2);
		searchQA123contact();
		editconatct_name("QA123@$#%&*?_()");
		editconatct_companyName("@#$^%$%^&%^&%&^%&");
		editconatct_email("abc@yopmail.com");
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}
		CreateBMs();
		searchQA123contacts();
		if (messages()) {
			Assert.assertEquals(false, messages());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(true, messages());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

		waits(2);
		searchQA10();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}
		CreateBMs();
		searchQA10();
		if (messages()) {
			Assert.assertEquals(true, messages());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, messages());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public static boolean editicon() {
		return driver.findElement(By.xpath("//*[@id='contactPencilIcon']//*[name()='svg']")).isDisplayed();
	}

	@Test(priority = 8)
	public void Editcontact_DoNotDistrub_turnOn_toggle_clickon_crossmark() throws Exception {
		waits(2);
		searchQA123contact();
		editconatct_name("QA123@$#%&*?_()");
		editconatct_companyName("@#$^%$%^&%^&%&^%&");
		editconatct_email("abc@yopmail.com");
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		driver.findElement(By.xpath("//*[@class='cancel-icon']//*[name()='svg']")).click();
		waits(2);

		if (editicon()) {
			Assert.assertEquals(true, editicon());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, editicon());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	@Test(priority = 9)
	public void Editcontact_DoNotDistrub_turnOn_toggle_clickon_save_button() throws Exception {
		waits(2);
		searchQA123contact();
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		edit_save_Button();
		waits(2);
		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}

	}

	@Test(priority = 10)
	public void createContact_present_donnotdisturb_toggle() throws Exception {
		waits(2);
		openCreateContact();
		waits(2);
		if (toggles()) {
			Assert.assertEquals(true, toggles());
			toggles();
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, toggles());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
		waits(5);
		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
	}

	@Test(priority = 11)
	public void createContact_fill_all_fields_turnon_and_off_donotdisturb_toggle() throws Exception {
		waits(2);
		openCreateContacts();
		waits(2);
		createContact_Name("sample");
		waits(2);
		String mobileNumber = "555" + randomeNum();
		waits(2);
		String email = randomestring() + "@yopmail.com";
		create_contact_phonenum(mobileNumber);
		waits(2);
		Enter_email(email);
		waits(2);
		company_name("technocentra");

		tags("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,123456789012345678901234567890,@$%&*/-");
		// create_note("sample");
		Thread.sleep(3000);
		toggle();
		save_Button();
		waits(2);

		if (createcontact_toaster().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(createcontact_toaster(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createcontact_toaster(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");//

		}

	}

	@Test(priority = 12)
	public void createContact_fill_required_fields_turnon_and_off_donotdisturb_toggle() throws Exception {
		waits(2);
		openCreateContacts();
		waits(2);
		createContact_Name("sample");
		waits(2);

		String mobileNumber = "555" + randomeNum();
		create_contact_phonenum(mobileNumber);
		waits(2);
		toggle();
		save_Button();
		waits(1);

		if (createcontact_toaster().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(createcontact_toaster(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createcontact_toaster(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");//

		}

	}

	@Test(priority = 13)
	public void createContact_page_click_on_cross_mark_close_create_contactpage() throws Exception {
		waits(2);
		openCreateContacts();
		waits(2);

		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
		waits(2);
		if (createContactIcon()) {
			Assert.assertEquals(true, createContactIcon());
			createContactIcon();
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, createContactIcon());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	@Test(priority = 14)
	public void createContact_fill_all_fields_turnon_and_off_donotdisturb_toggle_close_createcontactpage()
			throws Exception {
		waits(2);
		openCreateContacts();
		waits(2);
		createContact_Name("sample");
		waits(2);
		String mobileNumber = "555" + randomeNum();
		waits(2);
		String email = randomestring() + "@yopmail.com";
		create_contact_phonenum(mobileNumber);
		waits(2);
		Enter_email(email);
		waits(2);
		company_name("technocentra");

		tags("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,123456789012345678901234567890,@$%&*/-");
		create_note("sample");
		Thread.sleep(3000);
		toggle();
		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
		waits(2);

		if (createContactIcon()) {
			Assert.assertEquals(true, createContactIcon());
			createContactIcon();
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, createContactIcon());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	@Test(priority = 15)
	public void createContact_fill_nameand_mobilenum_turnon_and_off_donotdisturb_toggle_close_createcontactpage()
			throws Exception {
		waits(2);
		openCreateContact();
		waits(2);
		createContact_Name("sample");
		waits(2);
		String mobileNumber = "555" + randomeNum();
		waits(2);
		create_contact_phonenum(mobileNumber);
		toggle();
		save_Button();
		waits(2);

		if (createcontact_toaster().equalsIgnoreCase("Created contact successfully.")) {
			Assert.assertEquals(createcontact_toaster(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("NewContactiscreated Successfully");// ScreenShot capture

		} else {
			Assert.assertNotEquals(createcontact_toaster(), "Created contact successfully.",
					"Created contact successfully.");
			TestUtil.takeScreenshotAtEndOfTest("new contact creation is failed");//

		}
	}

	// @Test(priority=16)
	public void createContact_fill_nameand_mobilenum_turnon_donotdisturb_toggle__clickon_outSide() throws Exception {
		waits(2);
		openCreateContact();
		waits(2);
		createContact_Name("sample");
		waits(2);
		String mobileNumber = "555" + randomeNum();
		waits(2);
		create_contact_phonenum(mobileNumber);
		toggle();
		driver.findElement(By.xpath("//div[@class='model-bg ng-star-inserted']")).click();
		waits(5);
		if (createContactIcon()) {
			Assert.assertEquals(true, createContactIcon());
			createContactIcon();
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, createContactIcon());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	public static void logout() {

		driver.findElement(By.xpath("//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")).click();
		waits(5);
		driver.findElement(By.xpath("//a[@id='logoutId']")).click();
		waits(5);
	}

	public static boolean Bmmessage() {
		waits(5);
		return driver.findElement(By.xpath(" //*[@class ='messages ng-star-inserted']")).isDisplayed();

	}

	public static boolean Bmmessages() {
		waits(5);
		return driver.findElement(By.xpath("//*[@class ='messages ng-star-inserted']")).isDisplayed();

	}

	@Test(priority = 17)
	public void CreateBM_for_turnoff_Donotdistrub_contacts_send_receive_messages() throws Exception {
		waits(2);
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
		waits(5);
		driver.findElement(By.xpath("//a[@id='broadcastTab']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='createBroadcastButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//input[@id='inputBroadcastTagData']")).sendKeys("valid");
		waits(3);
		driver.findElement(By.xpath("//div[@class='contact-name d-flex']")).click();
		waits(3);
		driver.findElement(By.xpath("//input[@id='broadcast_name']")).sendKeys("valid Message");
		waits(2);
		driver.findElement(By.xpath("//textarea[@id='broadcast_message']")).sendKeys("valid message");
		waits(5);
		driver.findElement(By.xpath("//input[@id='broadcast_sendNow']")).click();
		waits(5);
		driver.findElement(By.xpath("//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='broadcast_SendButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
		waits(5);

		logout();
		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys("automations@yopmail.com");
		waits(2);
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys("tech1234");
		waits(2);
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		waits(16);

		driver.findElement(By.xpath("//h4[normalize-space()='Auto1']")).click();
		waits(5);
		if (Bmmessage()) {
			Assert.assertEquals(true, Bmmessage());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, Bmmessage());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	@Test(priority = 18)
	public void CreateBM_for_turnon_Donotdistrub_contacts_send_receive_messages() throws Exception {

		logout();
		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys("auto1@yopmail.com");
		waits(2);
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys("tech1234");
		waits(2);
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		waits(16);

		waits(2);
		driver.findElement(By.xpath("//h4[normalize-space()='Automation']")).click();
		waits(5);
		edit_Icon();
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		edit_save_Button();
		waits(2);

		if (contact_updated_toastermessage().equalsIgnoreCase("Contact successfully updated.")) {
			Assert.assertEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("contact name with special characters case working fine");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case working fine.");

		} else {
			Assert.assertNotEquals("Contact successfully updated.", "Contact successfully updated.");
			logger.info("edit contact name allow spectialchar case not working");
			TestUtil.takeScreenshotAtEndOfTest("edit contact name allow spectialchar case not working");

		}
		waits(5);

		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
		waits(5);
		driver.findElement(By.xpath("//a[@id='broadcastTab']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='createBroadcastButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//input[@id='inputBroadcastTagData']")).sendKeys("valid");
		waits(3);
		driver.findElement(By.xpath("//div[@class='contact-name d-flex']")).click();
		waits(3);
		driver.findElement(By.xpath("//input[@id='broadcast_name']")).sendKeys("valid Message1");
		waits(2);
		driver.findElement(By.xpath("//textarea[@id='broadcast_message']")).sendKeys("valid message1");
		waits(5);
		driver.findElement(By.xpath("//input[@id='broadcast_sendNow']")).click();
		waits(5);
		driver.findElement(By.xpath("//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='broadcast_SendButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//*[@class='cross-icons']//*[name()='svg']")).click();
		waits(5);

		logout();
		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys("automations@yopmail.com");
		waits(2);
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys("tech1234");
		waits(2);
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		waits(16);

		driver.findElement(By.xpath("//h4[normalize-space()='Auto1']")).click();
		waits(5);
		if (Bmmessages()) {
			Assert.assertEquals(true, Bmmessages());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, Bmmessages());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}
	}

	public static boolean pastBM() {
		return driver.findElement(By.xpath("//span[normalize-space()='past']")).isDisplayed();
	}

	@Test(priority = 19)
	public void CreateBM_for_turnon_Donotdistrub_BM_displayin_pastlist() throws Exception {

		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
		waits(5);
		driver.findElement(By.xpath("//a[@id='broadcastTab']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='createBroadcastButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//input[@id='inputBroadcastTagData']")).sendKeys("valid");
		waits(3);
		driver.findElement(By.xpath("//div[@class='contact-name d-flex']")).click();
		waits(3);
		driver.findElement(By.xpath("//input[@id='broadcast_name']")).sendKeys("past");
		waits(2);
		driver.findElement(By.xpath("//textarea[@id='broadcast_message']")).sendKeys("valid message1");
		waits(5);
		driver.findElement(By.xpath("//input[@id='broadcast_sendNow']")).click();
		waits(5);
		driver.findElement(By.xpath("//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[@id='broadcast_SendButton']")).click();
		waits(5);
		driver.findElement(By.xpath("//button[normalize-space()='Past']")).click();
		waits(5);
		if (pastBM()) {
			Assert.assertEquals(true, pastBM());
			logger.info("toggle button is enabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture

		} else {
			Assert.assertEquals(false, pastBM());
			logger.info("toggle button is disabled");
			TestUtil.takeScreenshotAtEndOfTest("verifyElementIsEnabledTestCase");// ScreenShot capture
		}

	}

	@AfterClass()
	public void tearDown() {
		driver.quit();
	}

}
