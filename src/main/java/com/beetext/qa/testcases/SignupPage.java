package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.signup.SignupWF;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.pages.CommonMethods;

public class SignupPage extends TestBase {

	Map<String, String> map = new HashMap<String, String>();

	@BeforeMethod
	public void setUp() {
		initialization();
	}

	@Test(priority = 1)
	public void Sign_001_signup_allvalid() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.signup_allvalid(map);

	}

	@Test(priority = 2)
	public void Sign_002_OrgErrorValidation() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.orgErrorValidation(map);
	}

	@Test(priority = 3)
	public void Sign_003_Orgnamealreadyexist() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Orgnamealreadyexist(map);
	}

	@Test(priority = 4)
	public void Sign_004_org2charerr() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.org2charerr(map);

	}

	@Test(priority = 6)
	public void Sign_006_email_alreadyexisterrt() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.email_alreadyexisterr(map);
	}

	@Test(priority = 7)
	public void Sign_007_workEmail_ErrorValidation() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.workEmailErrorValidation(map);
	}

	@Test(priority = 8)
	public void Sign_008_emailvalidation() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.emailvalidation(map);
	}

	@Test(priority = 12)
	public void Sign_012_signupPassword_ErrorValidation() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.SignuppasswordErrorValidation(map);
	}

	@Test(priority = 13)
	public void Sign_013_Passwordmustcharacters() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Passwordmustcharacters(map);
	}

	@Test(priority = 15)
	public void Sign_015_Namemust3char() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Namemust3char(map);

	}

	@Test(priority = 18)
	public void Sign_018_Plzreadtermsandcond() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Plzreadtermsandcond(map);
	}

	@Test(priority = 19)
	public void Sign_019_termsCond_Privacypolicy() throws Exception {

		String org = CommonMethods.exportData("Org", 1);
		map.put("org", org);

		String Workmail = CommonMethods.exportData("WorkEmail", 1);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.termsCond_Privacypolicy(map);

	}

	@Test(priority = 20)
	public void Sign_020_letsgobttn_isenabled() throws Exception {
		SignupWF signupwf = new SignupWF();
		signupwf.letsgobttn_isenabled(map);

	}

	@Test(priority = 21)
	public void Sign_021_Allvalid_details() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Allvalid_details(map);
	}

	@Test(priority = 22)
	public void Sign_022_RedirecttoEmaNum() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.RedirecttoEmaNum(map);
	}

	@Test(priority = 23)
	public void Sign_023_verifybttndisable() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.verifybttndisable(map);
	}

	@Test(priority = 24)
	public void Sign_024_verifybttnEnable() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.verifybttnEnable(map);

	}

	@Test(priority = 26)
	public void Sign_026_OtpInvalidErr() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.OtpInvalidErr(map);
	}

	@Test(priority = 27)
	public void Sign_027_Resendotp() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Resendotp(map);
	}

	@Test(priority = 28)
	public void Sign_028_MobOtp_VerifiedToaster() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.MobOtp_VerifiedToaster(map);
	}

	@Test(priority = 29)
	public void Sign_029_Otp_Allowonly6digits() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Dontallowbelow6digits(map);
	}

	@Test(priority = 30)
	public void Sign_030_Email_verifyDisable() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Email_verifyDisable(map);
	}

	@Test(priority = 31)
	public void Sign_031_Email_verifyEnable() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Email_verifyEnable(map);
	}

	@Test(priority = 32)
	public void Sign_032_email_Verification() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.email_Verification(map);
	}

	@Test(priority = 34)
	public void Sign_034_Emailotp_Dontallowbelow6digits() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Emailotp_Dontallowbelow6digits(map);
	}

	@Test(priority = 35)
	public void Sign_035_Email_OtpInvalidErr() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Email_OtpInvalidErr(map);
	}

	@Test(priority = 36)
	public void Sign_036_EmailOtp_VerifiedToaster() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.EmailOtp_VerifiedToaster(map);
	}

	@Test(priority = 37)
	public void Sign_037_Email_ResendotpToaster() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Email_ResendotpToaster(map);
	}

	@Test(priority = 38)
	public void Sign_038_loginwith_usercredentialwithotppending() throws Exception {

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.loginwith_usercredentialwithotppending(map);
	}

	@Test(priority = 39)
	public void Sign_039_loginwith_usercredentialwithotppending_Refresh() throws Exception {

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.loginwith_usercredentialwithotppending_Refresh(map);
	}

	@Test(priority = 42)
	public void Sign_042_loginwith_usercredentialwithaccesscodeppending() throws Exception {

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.loginwith_usercredentialwithaccesscodeppending(map);
	}

	@Test(priority = 43)
	public void Sign_043_loginwith_usercredentialwithaccesscodeppending_refresh() throws Exception {

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.loginwith_usercredentialwithaccesscodeppending_refresh(map);
	}

	@Test(priority = 46)
	public void Sign_46_NextDiable_PaymentInformation() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.NextDiable_PaymentInformation(map);

	}

	@Test(priority = 47)
	public void Sign_47_valid_CrediCardDetails() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.valid_CreditCardDetails(map);

	}

	@Test(priority = 48)
	public void Sign_48_Invalid_CreditCardNumber() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Invalid_CreditCardNumber(map);

	}

	@Test(priority = 49)
	public void Sign_49_Invalid_ExpiryDate() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Invalid_ExpiryDate(map);

	}

	@Test(priority = 50)
	public void Sign_50_Invalid_CvvCode() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Invalid_CvvCode(map);

	}

	@Test(priority = 51)
	public void Sign_51_Cvv_Mustbe3char() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Cvv_Mustbe3char(map);

	}

	@Test(priority = 52)
	public void Sign_52_uncheck_autopayments() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.uncheck_autopayments(map);

	}

	@Test(priority = 53)
	public void Sign_53_Validdetail_NextEnablePayments() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.Validdetail_NextEnablePayments(map);

	}

	@Test(priority = 100)
	public void Login_100_101_validate_forgotpassword_enteremail() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.validate_forgotpassword_enteremail(map);

	}

	@Test(priority = 102)
	public void Login_102_validate_forgotpasswordsendemail_invalidtoaster() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.validate_forgotpasswordsendemail_invalidtoaster(map);

	}

	@Test(priority = 103)
	public void Login_103_validate_forgotpasswordsendemail_invalidmail() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.validate_forgotpasswordsendemail_invalidmail(map);

	}

	@Test(priority = 105)
	public void Login_105_logindetailwithinvalidpassword() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.logindetailwithinvalidpassword(map);

	}

	@Test(priority = 106)
	public void Login_106_login_withInvaliddetails() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.login_withInvaliddetails(map);

	}

	@Test(priority = 107)
	public void Login_107_loginwithNull_letsgo_disable() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.loginwithNull_letsgo_disable(map);

	}

	@Test(priority = 108)
	public void Login_108_login_withsinglechar_error() throws Exception {

		SignupWF signupwf = new SignupWF();
		signupwf.login_withsinglechar_error(map);

	}

	@Test(priority = 109)
	public void login_109_loginwith_pendingpayment() throws Exception {

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.loginwith_pendingpayment(map);

	}

	@Test(priority = 110)
	public void Login_110_loginwith_deptpending() throws Exception {

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.loginwith_deptpending(map);
	}

	@Test(priority = 111)
	public void Login_111_loginwithvaliddetails_conversationpage() throws Exception {

		String Workmail = CommonMethods.exportData("WorkEmail", 3);
		map.put("Workmail", Workmail);

		SignupWF signupwf = new SignupWF();
		signupwf.loginwithvaliddetails_conversationpage(map);

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
