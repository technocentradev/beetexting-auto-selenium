package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.Workflows.vcf.VCF_Conv_SM_Msg_Combinaion_WF;
import com.beetext.qa.base.TestBase;

public class VCF_Conv_SM_Combination_Test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		VCF_WFCM vcfwf = new VCF_WFCM();
		vcfwf.auto1();
	}

	@Test(priority = 1)
	public void VCF_1_Conv_SM_Msg_VCF() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Conv_SM_Msg_VCF();
	}

	@Test(priority = 2)
	public void VCF_2_Conv_SM_VCF_Gif() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Conv_SM_VCF_Gif();
	}

	@Test(priority = 3)
	public void VCF_3_Conv_SM_VCF_Msg_Gif() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Conv_SM_VCF_Msg_Gif();
	}

	@Test(priority = 4)
	public void VCF_4_Conv_SM_VCF_Msg_Emoji() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Conv_SM_VCF_Msg_Emoji();
	}

	@Test(priority = 5)
	public void VCF_5_Con_SM_VCF_Emoji() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Emoji();
	}

	@Test(priority = 6)
	public void VCF_6_Con_SM_VCF_Msg_Image() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_Image();
	}

	@Test(priority = 7)
	public void VCF_7_Con_SM_VCF_Image() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Image();
	}

	@Test(priority = 8)
	public void VCF_8_Con_SM_VCF_Msg_Video() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_Video();
	}

	@Test(priority = 9)
	public void VCF_9_Con_SM_VCF_Msg_audio() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_audio();
	}

	@Test(priority = 10)
	public void VCF_10_Con_SM_VCF_Video() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Video();
	}

	@Test(priority = 11)
	public void VCF_11_Con_SM_VCF_audio() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_audio();
	}

	@Test(priority = 12)
	public void VCF_12_Con_SM_VCF_Msg_pdf() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_pdf();
	}

	@Test(priority = 13)
	public void VCF_13_Con_SM_VCF_pdf() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_pdf();
	}

	@Test(priority = 14)
	public void VCF_14_Con_SM_VCF_Msg_Excel() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_Excel();
	}

	@Test(priority = 15)
	public void VCF_15_Con_SM_VCF_Excel() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Excel();
	}

	@Test(priority = 16)
	public void VCF_16_Con_SM_VCF_Msg_WordDoc() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_WordDoc();
	}

	@Test(priority = 17)
	public void VCF_17_Con_SM_VCF_WordDoc() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_WordDoc();
	}

	@Test(priority = 18)
	public void VCF_18_Con_SM_VCF_Msg_TxtDoc() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_TxtDoc();
	}

	@Test(priority = 19)
	public void VCF_19_Con_SM_VCF_TxtDoc() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_TxtDoc();
	}

	@Test(priority = 20)
	public void VCF_20_Con_SM_VCF_Msg_MOV() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_MOV();
	}

	@Test(priority = 21)
	public void VCF_21_Con_SM_VCF_MOV() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_MOV();
	}

	@Test(priority = 22)
	public void VCF_22_Con_SM_VCF_Msg_WEBM() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_WEBM();
	}

	@Test(priority = 23)
	public void VCF_23_Con_SM_VCF_WEBM() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_WEBM();
	}

	@Test(priority = 24)
	public void VCF_24_Con_SM_VCF_Msg_GIF_Emoji_MP3_MOV_PDF_Image_Video() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_GIF_Emoji_MP3_MOV_PDF_Image_Video();
	}

	@Test(priority = 25)
	public void VCF_25_Con_SM_VCF_Msg_Mp3() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Msg_Mp3();
	}

	@Test(priority = 26)
	public void VCF_26_Con_SM_VCF_Mp3() throws Exception {

		VCF_Conv_SM_Msg_Combinaion_WF VCFWF = new VCF_Conv_SM_Msg_Combinaion_WF();
		VCFWF.Con_SM_VCF_Mp3();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
