package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.contacts.CreateContactWF;
import com.beetext.qa.base.TestBase;

public class CreateContact extends TestBase {

	// @BeforeMethod
	@BeforeClass
	public void setUp() {
		initialization();
		Login(); // for login
	}

	@Test(priority = 1)
	public void createContact_001_validcase() throws Exception {
		CreateContactWF createDept = new CreateContactWF();
		createDept.Createcontat();
	}

	@Test(priority = 2)
	public void createContact_002_NameAllow_Special_Char() throws Exception {
		CreateContactWF createDept = new CreateContactWF();
		createDept.CreatecontatName_allowSpecialChar();
	}

	@Test(priority = 3)
	public void createContact_003_NameAllow_Single_Char() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.CreatecontatName_allowSingleChar();
	}

	@Test(priority = 4)
	public void createContact_011_Only_Contact_upload_valid_avathar_logo() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.Createcontat_valid_avathar_image_upload();
	}

	@Test(priority = 5)
	public void createContact_034_Only_Contact_upload_invalid_avathar_logo() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.Createcontat_invalid_avathar_image_upload();
	}

	@Test(priority = 6)
	public void createContact_004_Only_ContactNameandMobileNumber() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.CreatecontatName_nameandMobilenumber();
	}

	@Test(priority = 7)
	public void createContact_023_invalidEmail() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.Createcontat_invalidEmail();
	}

	@Test(priority = 8)
	public void createContact_032_morethan50tags() throws Exception {
		CreateContactWF createDept = new CreateContactWF();
		createDept.CreatecontatName_Morethan50tags();
	}

	@Test(priority = 9)
	public void createContact_035_MobileNumber_Already_Exist() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.CreatecontatName_MobileNumber_Already_Exist();
	}

	@Test(priority = 10)
	public void createContact_038_tags_invalidcase() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.Createcontat_Tagsnotallowing_Special_Characters();
	}

	@Test(priority = 11)
	public void createContact_029_duplicate_Tags() throws Exception {
		CreateContactWF createDept = new CreateContactWF();
		createDept.Createcontat_DuplicateTAGS();
	}

	@Test(priority = 12)
	public void createContact_027_Name_removespaceEnd() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.CreatecontatName_givespaceatend();
	}

	@Test(priority = 13)
	public void createContact_027_1_Name_removespaceEnd() throws Exception {

		CreateContactWF createDept = new CreateContactWF();
		createDept.CreatecontatCompanyName_givespaceatend();
	}

	// @AfterMethod
	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		// VCard_Signout();
	}
}
