package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.UploadNonMediaAttachmentsWF;
import com.beetext.qa.base.TestBase;

public class UploadNonMediaAttachmentss extends TestBase {

	// @BeforeMethod
	@BeforeClass
	public void setUp() {
		initialization();
		// Login(); //for login

	}

	

	@Test(priority=56)
	public void Tc_MessageandWEBM_Audio_plus_AMR_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_WEBM_Audio_plus_AMR_combination();

	}

	@Test(priority=57)
	public void Tc_MessageandImage_plus_MP3_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_Image_plus_MP3_combination();

	}

	@Test(priority=58)
	public void Tc_MessageandImage_plus_3GP_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_Image_plus_3Gp_combination();

	}

	@Test(priority=59)
	public void Tc_MessageandImage_plus_AMR_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_Image_plus_Amr_combination();

	}

	@Test(priority=60)
	public void Tc_MessageandImage_plus_WEBM_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_Image_plus_Webm_combination();

	}

	@Test(priority=61)
	public void Tc_MessageandVideo_plus_MP3_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_video_plus_MP3_combination();

	}

	@Test(priority=62)
	public void Tc_MessageandVideo_plus_3gp_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_video_plus_3gp_combination();

	}

	@Test(priority=63)
	public void Tc_MessageandVideo_plus_WEBM_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_video_plus_WEBM_combination();

	}

	@Test(priority=64)
	public void Tc_MessageandVideo_plus_AMR_combination() throws Exception {
		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandJpg_video_plus_AMR_combination();

	}

	@AfterMethod
	public void logout() throws Exception {
		VCard_Signout();
	}
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
