package com.beetext.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.deeplink.DeeplinkWF;
import com.beetext.qa.base.TestBase;

public class Deeplink_test extends TestBase {

	@BeforeMethod
	public void setUp() throws Exception {
		initialization();
	}

	@Test(priority = 1)
	public void Deeplink_312_Redirect_Same_User_Org() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.Redirect_Same_User_Org();

	}

	@Test(priority = 2)
	public void Deeplink_313_MultiOrg_Redirect_Same_User_Org() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.MultiOrg_Redirect_Same_User_Org();

	}

	@Test(priority = 3)
	public void Deeplink_314_invalid_deeplink() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.invalid_deeplink();

	}

	@Test(priority = 4)
	public void Deeplink_315_Redirect_OneOne_Conversation() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.Redirect_OneOne_Conversation();

	}

	@Test(priority = 5)
	public void Deeplink_316_MultiOrg_Redirect_Same_User_Auto1_1stuser() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.MultiOrg_Redirect_Same_User_Auto1_1stuser();

	}

	@Test(priority = 6)
	public void Deeplink_317_Dept2_Redirect_to_Dept1() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.Dept2_Redirect_to_Dept1();

	}

	@Test(priority = 7)
	public void Deeplink_319_Redirect_TollFreeDept_To_Auto1Dept() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.Redirect_TollFreeDept_To_Auto1Dept();

	}

	@Test(priority = 8)
	public void Deeplink_321_MultiOrg_Active_In_TollFree_2nd_Redirect_Same_User_Auto1_1stuser_() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.MultiOrg_Active_In_TollFree_2nd_Redirect_Same_User_Auto1_1stuser_();

	}

	@Test(priority = 9)
	public void Deeplink_323_PinMsg_Dept_Switch() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.PinMsg_Dept_switch();

	}

	@Test(priority = 10)
	public void Deeplink_324_PinMsg_org_switch() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.PinMsg_org_switch();

	}

	@Test(priority = 11)
	public void Deeplink_326_PinMsg_org_switch_from_tollFree() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.PinMsg_org_switch_from_tollFree();

	}

	@Test(priority = 12)
	public void Deeplink_327_PinMsg_DepartmentRemoved() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.PinMsg_DepartmentRemoved();

	}

	@Test(priority = 13)
	public void Deeplink_328_PinMsg_DepartmentDeleted() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.PinMsg_DepartmentDeleted();

	}

	@Test(priority = 14)
	public void Deeplink_329_MultiOrg_PinMsg_DepartmentRemoved() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.MultiOrg_PinMsg_DepartmentRemoved();

	}

	@Test(priority = 15)
	public void Deeplink_330_MultiOrg_PinMsg_DepartmentDeleted() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.MultiOrg_PinMsg_DepartmentDeleted();

	}

	@Test(priority = 16)
	public void Deeplink_331_Msg_DepartmentRemoved() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.Msg_DepartmentRemoved();

	}

	@Test(priority = 17)
	public void Deeplink_332_Msg_DepartmentDeleted() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.MultiOrg_PinMsg_DepartmentDeleted();

	}

	@Test(priority = 18)
	public void Deeplink_335_Msg_received_Block_Contact() throws Exception {

		DeeplinkWF DLWF = new DeeplinkWF();
		DLWF.Msg_received_Block_Contact();

	}

//	@AfterMethod
//	public void validate_Signout() throws Exception {
//		WebElement signoutlink = driver.findElement(By.xpath("//li[6]/div[1]/a[1]/app-svg-icon[1]/span[1]"));
//		WebElement signout = driver.findElement(By.xpath("//a[@id='logoutId']"));
//		signoutlink.click();
//		signout.click();
//	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
