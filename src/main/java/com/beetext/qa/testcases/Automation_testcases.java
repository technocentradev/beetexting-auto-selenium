package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.automations.Automations_WF;
import com.beetext.qa.base.TestBase;

public class Automation_testcases extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {

		initialization();
		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_autologin();
	}

	@Test
	public void Features_210_NO_Automations_Created() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.NO_Automations_Created();

	}

	@Test
	public void Features_211_212_automations_Dept() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.automations_Dept();

	}

	@Test
	public void Features_213_create_automations_Open_with_AllFields() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Open_with_AllFields();

	}

	@Test
	public void Features_213_create_automations_title_must() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_title_must();

	}

	@Test
	public void Features_215_create_automations_title_3Char_error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_title_3Char_error();

	}

	@Test
	public void Features_216_create_automations_title_already_exists() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_title_already_exists();

	}

	@Test
	public void Features_217_Default_Dept() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Default_Dept();

	}

	@Test
	public void Features_218_Selecting_Another_Dept() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Selecting_Another_Dept();

	}

	@Test
	public void Features_219_manage_Case_Dept_Disabled() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.manage_Case_Dept_Disabled();

	}

	@Test
	public void Features_220_create_automations_with_Msg_received_Contains_pharse() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_with_Msg_received_Contains_pharse();

	}

	@Test
	public void Features_221_create_automations_with_Msg_received_Contains_pharse_CaseSensitive() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_with_Msg_received_Contains_pharse_CaseSensitive();

	}

	@Test
	public void Features_222_create_automations_with_Msg_received_Exactly_pharse() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_with_Msg_received_Exactly_pharse();

	}

	@Test
	public void Features_223_create_automations_with_Msg_received_Exactly_pharse_CaseSensitive() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_with_Msg_received_Exactly_pharse_CaseSensitive();

	}

	@Test
	public void Features_224_create_automations_Search_term_required_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Search_term_required_Error();

	}

	@Test
	public void Features_225_create_automations_withIn_Create() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_withIn_Create();

	}

	@Test
	public void Features_226_create_automations_Weekday_required_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Weekday_required_Error();

	}

	@Test
	public void Features_227_create_automations_OutsideOf_Create() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_OutsideOf_Create();

	}

	@Test
	public void Features_228_create_automations_select_All_Days() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_select_All_Days();

	}

	@Test
	public void Features_229_create_automations_Deselect_All_Days() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Deselect_All_Days();

	}

	@Test
	public void Features_230_create_automations_Start_Item_Should_be_LessThan_EndTime() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Start_Item_Should_be_LessThan_EndTime();

	}

	@Test
	public void Features_231_create_automations_Tag_Added_to_Contact() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Tag_Added_to_Contact();

	}

	@Test
	public void Features_232_Automations_Manage_Tag_Added_to_Contact() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Manage_Tag_Added_to_Contact();

	}

	@Test
	public void Features_233_create_automations_Tag_Added_to_Contact_Immediately_Respective_Action() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Tag_Added_to_Contact_Immediately_Respective_Action();

	}

//	@Test
//	public void Features_234_create_automations_Tag_Added_to_Contact_Delayed_Respective_Action() throws Exception {
//
//		Automations_WF automationsWF = new Automations_WF();
//		automationsWF.create_automations_Tag_Added_to_Contact_Delayed_Respective_Action();
//
//	}

	@Test
	public void Features_235_create_automations_RemoveTag_to_Contact_Immediately_Respective_Action() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_RemoveTag_to_Contact_Immediately_Respective_Action();

	}

//	@Test
//	public void Features_236_create_automations_RemoveTag_to_Contact_Delayed_Respective_Action() throws Exception {
//
//		Automations_WF automationsWF = new Automations_WF();
//		automationsWF.create_automations_RemoveTag_to_Contact_Delayed_Respective_Action();
//
//	}

	@Test
	public void Features_238_Automations_ON_OFF() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_ON_OFF();

	}

	@Test
	public void Features_239_240_Automations_Condition_For_Tag_toBe_Added_Contains() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Condition_For_Tag_toBe_Added_Contains();

	}

	@Test
	public void Features_241_Automations_IF_Condition_Close_Show_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_IF_Condition_Close_Show_Error();

	}

	@Test
	public void Features_242_Automations_Manage_Existing_Condition_Shown() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Manage_Existing_Condition_Shown();

	}

	@Test
	public void Features_243_Automations_Tag_List_Shown() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Tag_List_Shown();

	}

	@Test
	public void Features_244_245Automations_oneTag_Allowed_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_oneTag_Allowed_Error();

	}

	@Test
	public void Features_246_Automations_IF_Action_Close_Show_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_IF_Action_Close_Show_Error();

	}

	@Test
	public void Features_247_Automations_Multiple_Tags_Action() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Multiple_Tags_Action();

	}

	@Test
	public void Features_248_Automations_Action_Tag_Already_Added_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Action_Tag_Already_Added_Error();

	}

	@Test
	public void Features_249_Automations_Action_Tag_List_Shown() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Action_Tag_List_Shown();

	}

	@Test
	public void Features_250_Automations_Tag_Required_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.Automations_Tag_Required_Error();

	}

	@Test
	public void Features_253_create_automations_Tag_Added_to_Contact_Immediately_Action_Respective_Action()
			throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Tag_Added_to_Contact_Immediately_Action_Respective_Action();

	}

//	@Test
//	public void Features_254_create_automations_Tag_Added_to_Contact_Delayed_Action_Respective_Action()
//			throws Exception {
//
//		Automations_WF automationsWF = new Automations_WF();
//		automationsWF.create_automations_Tag_Added_to_Contact_Delayed_Action_Respective_Action();
//
//	}

	@Test
	public void Features_255_256_create_automations_for_Delay_Response_Shows_Min_Hr_Days() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_for_Delay_Response_Shows_Min_Hr_Days();

	}

	@Test
	public void Features_257_create_automations_If_we_Give_100_Delay_shows_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_If_we_Give_100_Delay_shows_Error();

	}

	@Test
	public void Features_258_create_automations_RemoveTag_to_Contact_Immediately_Action_Respective_Action()
			throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_RemoveTag_to_Contact_Immediately_Action_Respective_Action();

	}

//	@Test
//	public void Features_259_create_automations_RemoveTag_to_Contact_Delayed_Action_Respective_Action()
//			throws Exception {
//
//		Automations_WF automationsWF = new Automations_WF();
//		automationsWF.create_automations_RemoveTag_to_Contact_Delayed_Action_Respective_Action();
//
//	}

	@Test
	public void Features_260_create_automations_Delayed_Msg_Cancel_If_Contact_Replies() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Delayed_Msg_Cancel_If_Contact_Replies();

	}

//	@Test
//	public void Features_261_create_automations_Tag_Added_to_Contact_Delayed_response_With_Minute() throws Exception {
//
//		Automations_WF automationsWF = new Automations_WF();
//		automationsWF.create_automations_Tag_Added_to_Contact_Delayed_response_With_Minute();
//
//	}
//
	@Test
	public void Features_264_272_create_automations_Message_Tag_Added_to_Contact_Delayed() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Message_Tag_Added_to_Contact_Delayed();

	}

	@Test
	public void Features_265_274_create_automations_Message_Remove_Tag_Delayed() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Message_Remove_Tag_Delayed();

	}

	@Test
	public void Features_266_create_automations_Tag_Added_to_Contact_Multiple_Actions() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Tag_Added_to_Contact_Multiple_Actions();

	}

	@Test
	public void Features_267_create_automations_Multiple_Actions() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Multiple_Actions();

	}

	@Test
	public void Features_268_create_automations_Validate_Case_With_Multiple_Actions() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Validate_Case_With_Multiple_Actions();

	}

	@Test
	public void Features_269_create_automations_with_Msg_received_Immediate_Response() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_with_Msg_received_Immediate_Response();

	}

	@Test
	public void Features_270_create_automations_with_Msg_received_Delayed_Response() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_with_Msg_received_Delayed_Response();

	}

	@Test
	public void Features_271_create_automations_Message_Tag_Added_to_Contact_Immediate() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Message_Tag_Added_to_Contact_Immediate();

	}

	@Test
	public void Features_273_create_automations_Message_Tag_Remove_to_Contact_Immediate() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_Message_Tag_Remove_to_Contact_Immediate();

	}

	@Test
	public void Features_275_create_automations_withIn_MSG_Immediate() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_withIn_MSG_Immediate();

	}

	@Test
	public void Features_277_create_automations_withIn_Tag_Add_to_Contact_Immediate() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_withIn_Tag_Add_to_Contact_Immediate();

	}

	@Test
	public void Features_279_create_automations_withIn_Tag_Remove_to_Contact_Immediate() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.create_automations_withIn_Tag_Remove_to_Contact_Immediate();

	}

	@Test
	public void Features_280_WithIn_Weekday_required_Error() throws Exception {

		Automations_WF automationsWF = new Automations_WF();
		automationsWF.WithIn_Weekday_required_Error();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
