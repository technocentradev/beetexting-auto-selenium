package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.templates.ToolsSharedTemplatesWF;
import com.beetext.qa.base.TestBase;

public class ToolsTemplatesSharedpart2 extends TestBase {

	@BeforeClass
	public void setUp() {
		initialization();
		Login(); // for login
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();

	}

	@Test(priority = 1)
	public void Tools__createtemplatepageenteronlytitle_message_closetoolswindow_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_message_close_toolswindow_clickon_savedraft_button();
	}

	@Test(priority = 2)
	public void Tools__createtemplatepageenteronlytitle_breadscrumlink_clickon_savedraft_button() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_breadscrumlink_clickon_savedraft_button();
	}

	@Test(priority = 3)
	public void Tools__createtemplatepageenteronlytitle_message_breadscrumlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_message_breadscrumlink_clickon_savedraft_button();
	}

	@Test(priority = 4)
	public void Tools__createtemplate_nodata_clickon_create_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_nodata_clickon_create_button();
	}

	@Test(priority = 5)
	public void Tools__createtemplate_nodata_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_nodata_clickon_savedraft_button();
	}

	@Test(priority = 6)
	public void Tools__743_createtemplate_nodata_close_toolspopup_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_nodata_close_toolspopup_clickon_savedraft_button();
	}

	@Test(priority = 7)
	public void Tools__744_createtemplate_nodata_clickon_breadscrumlink_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_nodata_clickon_breadscrumlink_clickon_savedraft_button();
	}

	@Test(priority = 8)
	public void Tools__745_createtemplate_selectsharedoption_nodept_clickon_create_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_selectsharedoption_nodept_clickon_create_button();
	}

	@Test(priority = 9)
	public void Tools__746_createtemplate_title_message_selectsharedoption_nodept_clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_message_selectsharedoption_nodept_clickon_create_button();
	}

	@Test(priority = 10)
	public void Tools__747_createtemplate_title_selectsharedoption_nodept_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_selectsharedoption_nodept_clickon_saveDraft_button();
	}

	@Test(priority = 11)
	public void Tools__748_createtemplate_title_message_selectsharedoption_nodept_clickon_savedraft_button()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_message_selectsharedoption_nodept_clickon_saveDraft_button();
	}

//6-12-2021
	@Test(priority = 12)
	public void Tools__749_createtemplate_title_selectsharedoption_nodept_closetoolspopup_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_selectsharedoption_nodept_close_window_clickon_saveDraft_button();
	}

	@Test(priority = 13)
	public void Tools__750_createtemplate_title_message_selectsharedoption_nodept_closetoolspopup_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_message_selectsharedoption_nodept_close_window_clickon_saveDraft_button();
	}

	@Test(priority = 14)
	public void Tools__751_createtemplate_title_message_selectsharedoption_selectdept_closetoolspopup_clickon_savedraft_button()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_message_selectsharedoption_selectdepts_close_window_clickon_saveDraft_button();
	}

	@Test(priority = 15)
	public void Tools__752_createtemplate_title_selectsharedoption_selectdept_closetoolspopup_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_selectsharedoption_selectdepts_close_window_clickon_saveDraft_button();
	}

	@Test(priority = 16)
	public void Tools__753_createtemplate_title_selectsharedoption_nodept_closetoolspopup_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_message_selectsharedoption_selectdeptss_close_window_clickon_saveDraft_button();
	}

	@Test(priority = 17)
	public void Tools__753_createtemplate_title_selectsharedoption_nodepts_closetoolspopup_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_selectsharedoptions_nodept_close_window_clickon_saveDraft_button();
	}

	// 03-1-22

	@Test(priority = 18)
	public void Tools__757_createtemplate_title_selectsharedoption_nodepts_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_selectsharedoptions_nodept_clickon_saveDraft_button();
	}

	@Test(priority = 19)
	public void Tools__1198_draftlist_clickondeletebutton_open_confirmationpopup() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.draftlist_clickon_deletebutton_open_confirmationpopup();
	}

	@Test(priority = 20)
	public void Tools__1187_entertitle_message_clickon_savedraft_dispalyin_draftlist() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_entertitle_message_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 21)
	public void Tools__1188_entertitle_message_sleect_personal_clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_message_sleect_personal_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	// 10-1-2022

	@Test(priority = 22)
	public void Tools__1189_entertitle_message_sleect_shared_nodept_clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_message_sleect_shared_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 23)
	public void Tools__1190_entertitle_message_sleect_shared_selectalldept_clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_message_sleect_shared_select_alldept_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 24)
	public void Tools__1191_entertitle_message_sleect_shared_selectsomedept_clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_message_sleect_shared_select_somedepat_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 25)
	public void Tools__1192_entertitle_attachment_sleect_personal_clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_attachment_sleect_personaloption_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 26)
	public void Tools__1193_entertitle_attachment_sleect_shared_selectalldept_clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_attachment_sleect_sharedoption_alldept_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 27)
	public void Tools__1194_entertitle_attachment_sleect_shared_selectsomedept_clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle_attachment_sleect_sharedoption_select_somedept_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 28)
	public void Tools__1195_entertitle2char_message_sleect_personalopiton__clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Createtemplatepage_entertitle2char_message_sleect_personaloption_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	// 12-01-22

	@Test(priority = 29)
	public void Tools__1196_draftlist_opencreatetemplatepage_entertitle2char_message_sleect_personalopiton__clickon_savedraft_dispalyin_draftlist()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Draftlist_Createtemplatepage_entertitle2char_message_sleect_sharedoption_clickon_savedraft_button_tempalte_display_indraftlist();
	}

	@Test(priority = 30)
	public void Tools__1197_draftlist_openmanagetetemplatepage_clickon_delete_confirm_delete_buttons()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.draftlist_managetemplatepage_clickon_delete_confirmedelete_buttons();
	}

	@Test(priority = 31)
	public void Tools__1186_Drafttempalte_default_message() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.DraftTemplate_defaultmessage();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();

	}

}
