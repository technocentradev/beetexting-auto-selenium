package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.OrgSwitch_CMWF;
import com.beetext.qa.Workflows.Org_Switch_WF;
import com.beetext.qa.base.TestBase;

public class Org_Switch_test extends TestBase {
	Map<String, String> userCredentials = new HashMap<String, String>();

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		String username1 = prop.getProperty("username1");
		String username2 = prop.getProperty("username2");
		String password = prop.getProperty("password");
		userCredentials.put("username1", username1);
		userCredentials.put("username2", username2);
		userCredentials.put("password", password);
		OrgSwitch_CMWF orgSwitch = new OrgSwitch_CMWF();
		orgSwitch.auto1_Login(userCredentials);
	}

	@Test(priority = 1)
	public void features_001_Org_Switch_Add_Org_Arrow_Enabled() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_Arrow_Enabled(userCredentials);

	}

	@Test(priority = 2)
	public void features_002_Org_Switch_Add_Org() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org(userCredentials);

	}

	@Test(priority = 3)
	public void features_003_Org_Switch_Add_Org_text() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_text(userCredentials);

	}

	@Test(priority = 4)
	public void features_004_Org_Switch_Add_Org_add() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_add(userCredentials);

	}

	@Test(priority = 5)
	public void features_005_Org_Switch_Add_Org_letsgo_disabled() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_letsgo_disabled(userCredentials);

	}

	@Test(priority = 6)
	public void features_006_Org_Switch_Add_Org_productname() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_productname(userCredentials);

	}

	@Test(priority = 7)
	public void features_008_Org_Switch_Add_Org_back_to_MsgPage() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_back_to_MsgPage(userCredentials);

	}

	@Test(priority = 8)
	public void features_009_Org_Switch_Add_Org_validdetails() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_validdetails(userCredentials);

	}

	@Test(priority = 9)
	public void features_010_Org_Switch_Add_Org_Invalid_Passworddetails() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_Invalid_Passworddetails(userCredentials);

	}

	@Test(priority = 10)
	public void features_011_Org_Switch_Add_Org_Invalid_Emaildetails() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_Invalid_Emaildetails(userCredentials);

	}

	@Test(priority = 11)
	public void features_012_Org_Switch_Add_Org_AlreadySignIN_errorMsg() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_AlreadySignIN_errorMsg(userCredentials);

	}

	@Test(priority = 12)
	public void features_013_Org_Switch_Add_Org_Only_OneOrgAllowed() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_Only_OneOrgAllowed(userCredentials);

	}

	@Test(priority = 13)
	public void features_014_Org_Switch_Add_Org_validdetails_logout_shows_PreviousOrg() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_validdetails_logout_shows_PreviousOrg(userCredentials);

	}

	@Test(priority = 14)
	public void features_015_Org_Switch_Add_Org_OrgList() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_OrgList(userCredentials);

	}

	@Test(priority = 15)
	public void features_016_Org_Switch_Add_Org_radioButton_underAddButton() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_radioButton_underAddButton(userCredentials);

	}

	@Test(priority = 16)
	public void features_017_Org_Switch_Add_Org_updateOrg_DeleteOrg() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Org_Switch_Add_Org_updateOrg_DeleteOrg(userCredentials);

	}

	@Test(priority = 17)
	public void features_023_Batch_Count() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.Batch_Count(userCredentials);

	}

	@Test(priority = 18)
	public void features_024_login_logout_sameSession() throws Exception {
		Org_Switch_WF org_SwitchWF = new Org_Switch_WF();
		org_SwitchWF.login_logout_sameSession(userCredentials);

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
