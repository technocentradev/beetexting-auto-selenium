package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.Workflows.broadcast.Tools_BM_Edit_WF;
import com.beetext.qa.base.TestBase;

public class Tools_BM_Edit_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Broadcast BM = new Broadcast();
		BM.BM_tools_login();

	}

	@Test(priority = 1)
	public void tools_532_533_tools_BM_upcoming_Edit_monthly_recurring() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_monthly_recurring();
	}

	@Test(priority = 2)
	public void tools_534_tools_BM_upcoming_Edit_uncheckrecurring_shownInPast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_uncheckrecurring_shownInPast();
	}

	@Test(priority = 3)
	public void tools_535_tools_BM_upcoming_Edit_removeTag_savedasDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_removeTag_savedasDraft();
	}

	@Test(priority = 4)
	public void tools_536_tools_BM_upcoming_Edit_removeMSG_addAttahment_shownInPast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_removeMSG_addAttahment_shownInPast();
	}

	@Test(priority = 5)
	public void tools_537_tools_BM_upcoming_Edit_removeMSG_addAttahment_scheduleLater_shownInupcoming()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_removeMSG_addAttahment_shownInPast();
	}

	@Test(priority = 6)
	public void tools_538_tools_BM_upcoming_Edit_removeTag_weeklyRecurring_savedasDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_removeTag_weeklyRecurring_savedasDraft();
	}

	@Test(priority = 7)
	public void tools_539_540_tools_BM_upcoming_Edit_unSelectRecurring_schedule_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_unSelectRecurring_schedule_ShowninUPcoming();
	}

	@Test(priority = 8)
	public void tools_541_tools_BM_upcoming_Edit_Recurring_DailyToweekly_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_DailyToweekly_ShowninUPcoming();
	}

	@Test(priority = 9)
	public void tools_542_tools_BM_upcoming_Edit_message_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_message_ShowninUPcoming();
	}

	@Test(priority = 10)
	public void tools_543_tools_BM_upcoming_Edit_removemessage_adAttachment_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_removemessage_adAttachment_ShowninUPcoming();
	}

	@Test(priority = 11)
	public void tools_544_tools_BM_upcoming_Edit_removeMSg_removeAttachment_savedasDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_removeMSg_removeAttachment_savedasDraft();
	}

	@Test(priority = 12)
	public void tools_545_tools_BM_upcoming_Edit_removeTag_MSg_removeAttachment_savedasDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_removeTag_MSg_removeAttachment_savedasDraft();
	}

	@Test(priority = 13)
	public void tools_546_tools_BM_upcoming_Edit_Recurring_DailyTo_other_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_DailyTo_other_ShowninUPcoming();
	}

	@Test(priority = 14)
	public void tools_547_tools_BM_upcoming_Edit_Recurring_monthly_Select_endDate_lessthan_today_errorDisplay()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_monthly_Select_endDate_lessthan_today_errorDisplay();
	}

	@Test(priority = 15)
	public void tools_548_tools_BM_upcoming_Edit_Remove_Recurring_Showninpast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Remove_Recurring_Showninpast();
	}

	@Test(priority = 16)
	public void tools_549_tools_BM_upcoming_Edit_Recurring_yearlyTo_other_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_yearlyTo_other_ShowninUPcoming();
	}

	@Test(priority = 17)
	public void tools_550_tools_BM_upcoming_Edit_Recurring_yearly_changeMsg_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_yearly_changeMsg_ShowninUPcoming();
	}

	@Test(priority = 18)
	public void tools_551_tools_BM_upcoming_Edit_Recurring_yearly_AddAttachment_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_yearly_AddAttachment_ShowninUPcoming();
	}

	@Test(priority = 19)
	public void tools_552_tools_BM_upcoming_Edit_YearlyRecurring_removemessage_adAttachment_ShowninUPcoming()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_YearlyRecurring_removemessage_adAttachment_ShowninUPcoming();
	}

	@Test(priority = 20)
	public void tools_553_tools_BM_upcoming_Edit_yearlyRecurring_removeMSg_removeAttachment_savedasDraftg()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_yearlyRecurring_removeMSg_removeAttachment_savedasDraft();
	}

	@Test(priority = 21)
	public void tools_554_tools_BM_upcoming_Edit_Recurring_yearly_To_other_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_yearly_To_other_ShowninUPcoming();
	}

	@Test(priority = 22)
	public void tools_555_tools_BM_upcoming_Edit_Recurring_yearly_Select_endDate_lessthan_today_errorDisplay()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_yearly_Select_endDate_lessthan_today_errorDisplay();
	}

	@Test(priority = 23)
	public void tools_556_tools_BM_upcoming_Edit_NonRecurring_click_SaveDraft_ShowninUPcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_NonRecurring_click_SaveDraft_ShowninUPcoming();
	}

	@Test(priority = 24)
	public void tools_557_tools_BM_upcoming_Edit_NonRecurring_click_breadcumLink_SaveDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_NonRecurring_click_breadcumLink_SaveDraft();
	}

	@Test(priority = 25)
	public void tools_558_tools_BM_upcoming_Edit_NonRecurring_RemoveMsg_click_breadcumLink_SaveDraft()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_NonRecurring_RemoveMsg_click_breadcumLink_SaveDraft();
	}

	@Test(priority = 26)
	public void tools_559_tools_BM_upcoming_Edit_NonRecurring_RemoveMsg_addAttachment_click_breadcumLink_SaveDraft()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_NonRecurring_RemoveMsg_addAttachment_click_breadcumLink_SaveDraft();
	}

	@Test(priority = 27)
	public void tools_560_tools_BM_upcoming_Edit_weekly_Remove_Recurring_Showninpast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_weekly_Remove_Recurring_Showninpast();
	}

	@Test(priority = 28)
	public void tools_561_tools_BM_upcoming_Edit_changetoweekly_Remove_Recurring_Showninpast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_changetoweekly_Remove_Recurring_Showninpast();
	}

	@Test(priority = 29)
	public void tools_562_tools_BM_upcoming_Edit_weekly_changeMsg_Showninpast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_weekly_changeMsg_Showninpast();
	}

	@Test(priority = 30)
	public void tools_563_tools_BM_upcoming_Edit_weekly_changeMsg_addAttachment() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_weekly_changeMsg_Showninpast();
	}

	@Test(priority = 31)
	public void tools_564_tools_BM_upcoming_Edit_weekly_RemoveMsg_addAttachment() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_weekly_RemoveMsg_addAttachment();
	}

	@Test(priority = 32)
	public void tools_565_tools_BM_upcoming_Edit_weekly_RemoveMsg_RemoveAttachment_ShownInDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_weekly_RemoveMsg_RemoveAttachment_ShownInDraft();
	}

	@Test(priority = 33)
	public void tools_566_tools_BM_weeklyRecuring_upcoming_Edit_other_updated() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_weeklyRecuring_upcoming_Edit_other_updated();
	}

	@Test(priority = 34)
	public void tools_567_tools_BM_upcoming_Edit_Recurring_weekly_Select_endDate_lessthan_today_errorDisplay()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurring_weekly_Select_endDate_lessthan_today_errorDisplay();
	}

	@Test(priority = 35)
	public void tools_568_tools_BM_weeklyRecuring_upcoming_Edit_uncheckRecurring_updatedInPast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_weeklyRecuring_upcoming_Edit_uncheckRecurring_updatedInPast();
	}

	@Test(priority = 36)
	public void tools_569_tools_BM_NonRecuring_upcoming_Edit_weeklyRecurring_updatedInupcoming() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_upcoming_Edit_weeklyRecurring_updatedInupcoming();
	}

	@Test(priority = 37)
	public void tools_570_tools_BM_upcoming_Edit_monthly_changeMsg_Showninpast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_monthly_changeMsg_Showninpast();
	}

	@Test(priority = 38)
	public void tools_571_tools_BM_upcoming_Edit_monthly_changeMsg_addAttachment() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_monthly_changeMsg_addAttachment();
	}

	@Test(priority = 39)
	public void tools_572_tools_BM_upcoming_Edit_monthly_RemoveMsg_addAttachment() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_monthly_RemoveMsg_addAttachment();
	}

	@Test(priority = 40)
	public void tools_573_tools_BM_upcoming_Edit_monthly_RemoveMsg_RemoveAttachment_ShownInDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_monthly_RemoveMsg_RemoveAttachment_ShownInDraft();
	}

	@Test(priority = 41)
	public void tools_574_tools_BM_monthlyRecuring_upcoming_Edit_other_updated() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_monthlyRecuring_upcoming_Edit_other_updated();
	}

	@Test(priority = 42)
	public void tools_575_tools_BM_upcoming_Edit_Recurringyearly_Select_endDate_lessthan_today_errorDisplay()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Recurringyearly_Select_endDate_lessthan_today_errorDisplay();
	}

	@Test(priority = 43)
	public void tools_576_tools_BM_NonRecuring_past_Edit_SaveDraft_updatedInDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_SaveDraft_updatedInDraft();
	}

	@Test(priority = 44)
	public void tools_577_tools_BM_NonRecuring_past_Edit_breadcumlink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_breadcumlink_clickSave();
	}

	@Test(priority = 45)
	public void tools_578_tools_BM_NonRecuring_past_Edit_removeTag_breadcumlink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_removeTag_breadcumlink_clickSave();
	}

	@Test(priority = 46)
	public void tools_579_tools_BM_NonRecuring_past_Edit_removeMsg_removeAttachment_breadcumlink_clickSave()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_removeMsg_removeAttachment_breadcumlink_clickSave();
	}

	@Test(priority = 47)
	public void tools_580_tools_BM_NonRecuring_past_Edit_addTag_breadcumlink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_addTag_breadcumlink_clickSave();
	}

	@Test(priority = 48)
	public void tools_581_tools_BM_NonRecuring_past_Edit_Msg_breadcumlink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_Msg_breadcumlink_clickSave();
	}

	@Test(priority = 49)
	public void tools_582_tools_BM_NonRecuring_past_Edit_manageAttachments_breadcumlink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_manageAttachments_breadcumlink_clickSave();
	}

	@Test(priority = 50)
	public void tools_583_tools_BM_NonRecuring_past_Edit_clickotherLink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_clickotherLink_clickSave();
	}

	@Test(priority = 51)
	public void tools_584_tools_BM_NonRecuring_past_Edit_anyField_clickotherLink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_anyField_clickotherLink_clickSave();
	}

	@Test(priority = 52)
	public void tools_585_tools_BM_NonRecuring_past_Edit_RemoveTag_clickotherLink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_RemoveTag_clickotherLink_clickSave();
	}

	@Test(priority = 53)
	public void tools_586_tools_BM_NonRecuring_past_Edit_removeMsg_Attachment_clickotherLink_clickSave()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_removeMsg_Attachment_clickotherLink_clickSave();
	}

	@Test(priority = 54)
	public void tools_587_tools_BM_NonRecuring_past_Edit_addTag_recurring_clickotherLink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_addTag_recurring_clickotherLink_clickSave();
	}

	@Test(priority = 55)
	public void tools_588_tools_BM_NonRecuring_past_Edit_msg_clickotherLink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_msg_clickotherLink_clickSave();
	}

	@Test(priority = 56)
	public void tools_589_tools_BM_NonRecuring_past_Edit_editAttachment_clickotherLink_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_editAttachment_clickotherLink_clickSave();
	}

	@Test(priority = 57)
	public void tools_590_tools_BM_NonRecuring_past_Edit_outsideClick_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_outsideClick_clickSave();
	}

	@Test(priority = 58)
	public void tools_591_tools_BM_NonRecuring_past_Edit_changeAnyField_outsideClick_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_changeAnyField_outsideClick_clickSave();
	}

	@Test(priority = 59)
	public void tools_592_tools_BM_NonRecuring_past_Edit_RemoveTag_outsideClick_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_RemoveTag_outsideClick_clickSave();
	}

	@Test(priority = 60)
	public void tools_593_tools_BM_NonRecuring_past_Edit_RemoveTag_outsideClick_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_RemoveMsg_Attachment_outsideClick_clickSave();
	}

	@Test(priority = 61)
	public void tools_594_tools_BM_NonRecuring_past_Edit_Add_Attachment_outsideClick_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_Add_Attachment_outsideClick_clickSave();
	}

	@Test(priority = 62)
	public void tools_595_tools_BM_NonRecuring_past_Edit_msg_outsideClick_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_msg_outsideClick_clickSave();
	}

	@Test(priority = 63)
	public void tools_596_tools_BM_NonRecuring_past_Edit_Attachment_outsideClick_clickSave() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_NonRecuring_past_Edit_Attachment_outsideClick_clickSave();
	}

	@Test(priority = 64)
	public void tools_597_tools_BM_Recuring_upcoming_Edit_SaveAsDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_SaveAsDraft();
	}

	@Test(priority = 65)
	public void tools_598_tools_BM_Recuring_upcoming_Edit_breadcumLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_breadcumLink_Save();
	}

	@Test(priority = 66)
	public void tools_599_tools_BM_Recuring_upcoming_Edit_addAttachment_breadcumLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_addAttachment_breadcumLink_Save();
	}

	@Test(priority = 67)
	public void tools_600_tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_breadcumLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_breadcumLink_Save();
	}

	@Test(priority = 68)
	public void tools_601_tools_BM_Recuring_upcoming_Edit_addTwoTags_breadcumLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_addTwoTags_breadcumLink_Save();
	}

	@Test(priority = 69)
	public void tools_602_tools_BM_Recuring_upcoming_Edit_Msg_breadcumLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_Msg_breadcumLink_Save();
	}

	@Test(priority = 70)
	public void tools_603_tools_BM_Recuring_upcoming_Edit_Attachment_breadcumLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_Attachment_breadcumLink_Save();
	}

	@Test(priority = 71)
	public void tools_604_tools_BM_Recuring_upcoming_Edit_click_onOtherLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_click_onOtherLink_Save();
	}

	@Test(priority = 72)
	public void tools_605_tools_BM_Recuring_upcoming_Edit_AddTag_Msg_click_onOtherLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_AddTag_Msg_click_onOtherLink_Save();
	}

	@Test(priority = 73)
	public void tools_606_tools_BM_Recuring_upcoming_Edit_RemoveTag_Msg_click_onOtherLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveTag_Msg_click_onOtherLink_Save();
	}

	@Test(priority = 74)
	public void tools_607_tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_onOtherLink_Save()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_onOtherLink_Save();
	}

	@Test(priority = 75)
	public void tools_608_tools_BM_Recuring_upcoming_Edit_AddMoreTags_click_onOtherLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_AddMoreTags_click_onOtherLink_Save();
	}

	@Test(priority = 76)
	public void tools_609_tools_BM_Recuring_upcoming_Edit_Msg_click_onOtherLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_Msg_click_onOtherLink_Save();
	}

	@Test(priority = 77)
	public void tools_610_tools_BM_Recuring_upcoming_Edit_Attachment_click_onOtherLink_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_Attachment_click_onOtherLink_Save();
	}

	@Test(priority = 78)
	public void tools_611_tools_BM_Recuring_upcoming_Edit_click_outside_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_click_outside_Save();
	}

	@Test(priority = 79)
	public void tools_612_tools_BM_Recuring_upcoming_Edit_AddMoreTags_click_outside_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_AddMoreTags_click_outside_Save();
	}

	@Test(priority = 80)
	public void tools_613_tools_BM_Recuring_upcoming_Edit_RemoveTags_click_outside_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveTags_click_outside_Save();
	}

	@Test(priority = 81)
	public void tools_614_tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_outside_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_outside_Save();
	}

	@Test(priority = 82)
	public void tools_615_tools_BM_Recuring_SM_upcoming_Edit_RemoveMsg_AddAttachment_click_outside_Save()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_SM_upcoming_Edit_RemoveMsg_AddAttachment_click_outside_Save();
	}

	@Test(priority = 83)
	public void tools_616_tools_BM_Recuring_upcoming_Edit_Msg_click_outside_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_Msg_click_outside_Save();
	}

	@Test(priority = 84)
	public void tools_617_tools_BM_Recuring_upcoming_Edit_Attachment_click_outside_Save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_Attachment_click_outside_Save();
	}

	@Test(priority = 85)
	public void tools_618_tools_BM_Recuring_upcoming_Edit_RemoveMsg_RemoveAttachment_click_outside_Save()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveMsg_RemoveAttachment_click_outside_Save();
	}

	@Test(priority = 86)
	public void tools_619_tools_BM_Recuring_upcoming_Edit_SaveDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_SaveDraft();
	}

	@Test(priority = 87)
	public void tools_620_tools_BM_Recuring_upcoming_Edit_removeMsg_title_SendBroadCast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_removeMsg_title_SendBroadCast();
	}

	@Test(priority = 88)
	public void tools_621_tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_ScheduleBroadcast()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveMsg_Attachment_click_ScheduleBroadcast();
	}

	@Test(priority = 89)
	public void tools_622_tools_BM_Recuring_upcoming_Edit_deselectRecurring_SaveDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_deselectRecurring_SaveDraft();
	}

	@Test(priority = 90)
	public void tools_623_tools_BM_Recuring_upcoming_Edit_deselectRecurring_removeTags_SaveDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_deselectRecurring_removeTags_SaveDraft();
	}

	@Test(priority = 91)
	public void tools_624_tools_BM_Recuring_upcoming_Edit_Save_Draft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_Save_Draft();
	}

	@Test(priority = 92)
	public void tools_625_tools_BM_Recuring_upcoming_Edit_RemoveTags_click_ScheduleBroadcast() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveTags_click_ScheduleBroadcast();
	}

	@Test(priority = 93)
	public void tools_626_tools_BM_Recuring_upcoming_Edit_RemoveMsg_attachment_click_ScheduleBroadcast()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_RemoveMsg_attachment_click_ScheduleBroadcast();
	}

	@Test(priority = 94)
	public void tools_627_tools_BM_Recuring_upcoming_Edit_deselectRecurring_Save_Draft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_deselectRecurring_Save_Draft();
	}

	@Test(priority = 95)
	public void tools_628_tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Schedule_BM() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Schedule_BM();
	}

	@Test(priority = 96)
	public void tools_629_tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_addAttachment_Schedule_BM()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_addAttachment_Schedule_BM();
	}

	@Test(priority = 97)
	public void tools_630_tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Attachment_Schedule_BM()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Attachment_Schedule_BM();
	}

	@Test(priority = 98)
	public void tools_631_tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Attachment_Schedule_BM()
			throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Recuring_upcoming_Edit_deselectRecurring_RemoveMsg_Attachment_Schedule_BM();
	}

	@Test(priority = 99)
	public void tools_632_tools_BM_Non_Recuring_Past_Edit_SaveDraft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Non_Recuring_Past_Edit_SaveDraft();
	}

	@Test(priority = 100)
	public void tools_633_tools_BM_Non_Recuring_Past_Edit_removeTags_Schedule_BM() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Non_Recuring_Past_Edit_removeTags_Schedule_BM();
	}

	@Test(priority = 101)
	public void tools_634_tools_BM_Non_Recuring_Past_Edit_removemsg_Attachment_Schedule_BM() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Non_Recuring_Past_Edit_removemsg_Attachment_Schedule_BM();
	}

	@Test(priority = 102)
	public void tools_635_tools_BM_Draft_Edit_Save_Draft() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Draft_Edit_Save_Draft();
	}

	@Test(priority = 103)
	public void tools_636_tools_BM_Draft_Edit_manage_delete() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_Draft_Edit_manage_delete();
	}

	@Test(priority = 104)
	public void tools_637_tools_BM_upcoming_Edit_Reuse_AddTag_ChangeTitle_msg_updated_created() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_upcoming_Edit_Reuse_AddTag_ChangeTitle_msg_updated_created();
	}

	@Test(priority = 105)
	public void tools_638_tools_BM_past_Edit_Reuse_Add_Two_Tag_ChangeTitle_msg_updated_created() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_Add_Two_Tag_ChangeTitle_msg_updated_created();
	}

	@Test(priority = 106)
	public void tools_639_640_tools_BM_past_Edit_Reuse_ChangeTitle_click_BroadcastLink_save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_ChangeTitle_click_BroadcastLink_save();
	}

	@Test(priority = 107)
	public void tools_641_tools_BM_past_Edit_Reuse_ChangeTitle_click_BroadcastLink_Discard() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_ChangeTitle_click_BroadcastLink_Discard();
	}

	@Test(priority = 108)
	public void tools_642_643_tools_BM_past_Edit_Reuse_ChangeTitle_click_otherLink_save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_ChangeTitle_click_otherLink_save();
	}

	@Test(priority = 109)
	public void tools_644_tools_BM_past_Edit_Reuse_ChangeTitle_click_otherLink_Discard() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_ChangeTitle_click_otherLink_Discard();
	}

	@Test(priority = 110)
	public void tools_645_646_tools_BM_past_Edit_Reuse_click_outside_save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_click_outside_save();
	}

	@Test(priority = 111)
	public void tools_647_tools_BM_past_Edit_Reuse_click_outside_Discard() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_click_outside_Discard();
	}

	@Test(priority = 112)
	public void tools_648_649_tools_BM_past_Edit_Reuse_ChangeTitle_click_outside_save() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_ChangeTitle_click_outside_save();
	}

	@Test(priority = 113)
	public void tools_650_tools_BM_past_Edit_Reuse_changeTitle_click_outside_Discard() throws Exception {
		Tools_BM_Edit_WF Tools_BM_Edit_WF = new Tools_BM_Edit_WF();
		Tools_BM_Edit_WF.tools_BM_past_Edit_Reuse_changeTitle_click_outside_Discard();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
