package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.ProfileWFCM;
import com.beetext.qa.Workflows.ProfileWF;
import com.beetext.qa.base.TestBase;

public class ProfileUpdate_test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		ProfileWFCM profile=new ProfileWFCM();
		profile.login(); //for login
		

	}
	@Test(priority=1)
	public void update_agent_profile() {
		ProfileWF profile=new ProfileWF();
		profile.successfully_update_agent_profile_details();
	}
	@Test(priority=2)
	public void update_agent_name() {
		ProfileWF profile=new ProfileWF();
		profile.update_agent_profile_name();
	}
	@Test(priority=3)
	public void update_agent_name_3char_required_validationo() {
		ProfileWF profile=new ProfileWF();
		profile.update_agent_profile_name_3char_required_validation();
	}
	@Test(priority=4)
	public void update_agent_signature() {
		ProfileWF profile=new ProfileWF();
		profile.update_agent_profile_singature_enable();
	}
	@Test(priority=5)
	public void update_agent_Mobile_number() {
		ProfileWF profile=new ProfileWF();
		profile.update_agent_profile_Moblile_number();
	}
	@Test(priority=6)
	public void update_agent_Mobile_number_invalid_error() {
		ProfileWF profile=new ProfileWF();
		profile.update_agent_profile__Mobile_Number_invalid_validation();
	}
	@Test(priority=7)
	public void update_agent_signature_with_100_char_signature() {
		ProfileWF profile=new ProfileWF();
		profile.update_agent_profile_100_char_singature_enable();
	}
	@Test(priority=9)
	public void Close_profile_popup() throws Exception {
		ProfileWF profile=new ProfileWF();
		profile.clickon_Close_profile_popup_button();
	}
	@Test(priority=8)
	public void clickon_cancel_button() throws Exception {
		ProfileWF profile=new ProfileWF();
		profile.clickon_Cancel_button();
	}
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
