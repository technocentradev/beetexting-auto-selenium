package com.beetext.qa.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.base.TestBase;

public class tagsScrollCases extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	public static WebDriver driver;
	static String username1 = "auto1@yopmail.com";
	static String username2 = "automations@yopmail.com";
	static String password = "tech1234";

	public static void waitAndLog(int d) {
		try {
			Thread.sleep((long) (d * 1000));
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public static void implicitwaitAndLog(int d) {
		try {
			driver.manage().timeouts().implicitlyWait(d * 10, TimeUnit.SECONDS);
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public static void email(String email) throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys(email);
		waitAndLog(1);
	}

	public static void password(String password) throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys(password);
		waitAndLog(1);
	}

	public static void loginButton() throws InterruptedException {
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		waitAndLog(2);

	}

	public static void tools_bttn() throws Exception {

		driver.findElement(By.xpath("//a[@id='toolsMainTab']")).click();
	}

	public static void tools_tags_bttn() throws Exception {

		waitAndLog(2);
		driver.findElement(By.xpath("//a[@id='tagsTab']")).click();
		waitAndLog(2);
	}

	public static void tools_tags_createtag() throws Exception {

		driver.findElement(By.xpath("//button[@id='createTagButton']")).click();

	}

	public static void tools_tags_createtag_search_contact(String value) throws Exception {
		waitAndLog(2);
		driver.findElement(By.xpath("//input[@id='tag_searchContact']")).sendKeys(value);
		waitAndLog(2);
	}

	public static void tools_tags_createtag_taginput(String value) throws Exception {

		driver.findElement(By.xpath("(//*[@id='tagInput'])[1]")).sendKeys(value);

	}

	@BeforeClass
	public void setUp() throws Exception {

		driver = init("chrome");
		driver.manage().window().maximize();
		waitAndLog(2);
		driver.get("http://automation.beetexting.com");
		waitAndLog(5);
		email(username1);
		password(password);
		waitAndLog(2);
		loginButton();
		waitAndLog(5);

		tools_bttn();
		logger.info("Click on tools_bttn");
		waitAndLog(2);

	}

	@Test(priority = 1)
	public void ScrollTags_unselectallContacts() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		waitAndLog(2);

		tools_tags_createtag();
		logger.info("Click on tools_tags_createtag");
		implicitwaitAndLog(10);

		for (int i = 1; i < 200; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			implicitwaitAndLog(3);
		}

		waitAndLog(10);
	}

	@Test(priority = 2)
	public void ScrollTags_selectallContacts() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		waitAndLog(2);

		tools_tags_createtag();
		logger.info("Click on tools_tags_createtag");
		implicitwaitAndLog(10);

		for (int i = 2; i < 200; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			implicitwaitAndLog(3);
		}

		waitAndLog(5);
	}

	@Test(priority = 3)
	public void ScrollTags_unselectallContacts_addTagName() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		waitAndLog(2);

		tools_tags_createtag();
		logger.info("Click on tools_tags_createtag");
		implicitwaitAndLog(10);

		tools_tags_createtag_taginput("Scroll");
		logger.info("Click on tools_tags_createtag_taginput"
				+ "");
		waitAndLog(2);

		for (int i = 1; i < 25; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			waitAndLog(2);
		}

		waitAndLog(5);
	}

	@Test(priority = 4)
	public void ScrollTags_selectallContacts_add_TagName() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		waitAndLog(2);

		tools_tags_createtag();
		logger.info("Click on tools_tags_createtag");
		implicitwaitAndLog(10);

		tools_tags_createtag_taginput("Scroll");
		logger.info("Click on tools_tags_createtag_taginput");
		waitAndLog(2);

		for (int i = 2; i < 200; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			implicitwaitAndLog(3);
		}

		waitAndLog(5);
	}

	@Test(priority = 5)
	public void ScrollTags_EditTag_unselectallContacts() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		implicitwaitAndLog(10);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("Scroll");
		implicitwaitAndLog(3);

		driver.findElement(By.xpath("//button[@id='scroll caseTagManage']")).click();
		implicitwaitAndLog(15);

		for (int i = 3; i < 50; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			implicitwaitAndLog(3);
		}

		waitAndLog(10);
	}

	@Test(priority = 6)
	public void ScrollTags_EditTag_selectallContacts() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		implicitwaitAndLog(10);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("Scroll");
		implicitwaitAndLog(3);

		driver.findElement(By.xpath("//button[@id='scroll caseTagManage']")).click();
		implicitwaitAndLog(15);

		for (int i = 3; i < 50; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			implicitwaitAndLog(3);
		}

		waitAndLog(5);
	}

	@Test(priority = 7)
	public void ScrollTags_EditTag_deselecta_SomeContacts() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		implicitwaitAndLog(10);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("Scroll");
		implicitwaitAndLog(3);

		driver.findElement(By.xpath("//button[@id='scroll caseTagManage']")).click();
		implicitwaitAndLog(15);

		tools_tags_createtag_taginput("Scroll");
		logger.info("Click on tools_tags_createtag_taginput");
		waitAndLog(2);

		for (int i = 3; i < 50; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			implicitwaitAndLog(3);
		}

		waitAndLog(5);
	}

	@Test(priority = 8)
	public void ScrollTags_EditTag_select_SomeContacts() throws Exception {

		waitAndLog(2);

		tools_tags_bttn();
		logger.info("Click on tools_tags_bttn");
		implicitwaitAndLog(10);

		driver.findElement(By.xpath("//input[@placeholder='Search tags']")).sendKeys("Scroll");
		implicitwaitAndLog(3);

		driver.findElement(By.xpath("//button[@id='scroll caseTagManage']")).click();
		implicitwaitAndLog(10);
		waitAndLog(2);

		for (int i = 8; i < 40; i++) {

			driver.findElement(By.xpath("(//label[@class='checkbox-inline']//*[@class='checkmark'])" + "[" + i + "]"))
					.click();
			implicitwaitAndLog(3);
		}

		waitAndLog(5);
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
