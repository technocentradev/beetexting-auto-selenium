package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.Workflows.scheduled.Tools_SM_Gif_WF;
import com.beetext.qa.base.TestBase;

public class Tools_SM_GIF_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();

		Tools_SM_WFCM SMCM = new Tools_SM_WFCM();
		SMCM.Login_Tools_SM();
	}

	@Test(priority = 1)
	public void TC_001_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_001_Verify_Giphy_popup();
	}

	@Test(priority = 2)
	public void TC_002_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_002_Verify_Giphy_Attachment();
	}

	@Test(priority = 3)
	public void TC_003_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_003_Verify_Giphy_images_Using_Search();
	}

	@Test(priority = 4)
	public void TC_004_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_004_Verify_Giphy_error_message_for_invalid_Search();
	}

	@Test(priority = 5)
	public void TC_005_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_005_Verify_Functionality_of_Gif_Close_icon();
	}

	@Test(priority = 6)
	public void TC_006_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_006_Verify_Functionality_of_Gif_ViewTerms();
	}

	@Test(priority = 7)
	public void TC_007_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_007_Verify_Functionality_of_Gif_popup_by_OutsideClick();
	}

	@Test(priority = 8)
	public void TC_008_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_008_Verify_Record_with_Gif_attachments_under_upcoming();
	}

	@Test(priority = 9)
	public void TC_009_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_009_Verify_Record_with_Gif_attachments_under_upcoming();
	}

	@Test(priority = 10)
	public void TC_010_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_010_Verify_Record_with_Gif_attachments_under_upcoming();
	}

	@Test(priority = 11)
	public void TC_011_SM_GIF() throws Exception {

		Tools_SM_Gif_WF Gif = new Tools_SM_Gif_WF();
		Gif.TC_011_Verify_Record_with_Gif_attachments_under_upcoming();
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}

}
