package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.Internal_NotificatinsWF;
import com.beetext.qa.base.TestBase;

public class Internal_notifications_testcases extends TestBase {

	Map<String, String> userCredentials = new HashMap<String, String>();

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		String username1 = prop.getProperty("username1");
		String username2 = prop.getProperty("username2");
		String password = prop.getProperty("password");
		userCredentials.put("username1", username1);
		userCredentials.put("username2", username2);
		userCredentials.put("password", password);
		Internal_NotificatinsWF inpWF = new Internal_NotificatinsWF();
		inpWF.Notifications_CM(userCredentials);
	}

	@Test(priority = 1)
	public void Features_195_Notification_All_States_Enable() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_All_States_Enable(userCredentials);

	}

	@Test(priority = 2)
	public void Features_196_Notification_ON_Off_Update_Toaster() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_ON_Off_Update_Toaster(userCredentials);

	}

	@Test(priority = 3)
	public void Features_197_Notification_Email_WebBanner_Both_ON() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Email_WebBanner_Both_ON(userCredentials);

	}

	@Test(priority = 4)
	public void Features_198_Notification_Email_WebBanner_Both_OFF() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Email_WebBanner_Both_OFF(userCredentials);

	}

	@Test(priority = 5)
	public void Features_199_Notification_Email_ON_WebBanner_OFF() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Email_ON_WebBanner_OFF(userCredentials);

	}

	@Test(priority = 6)
	public void Features_200_Notification_Email_OFF_WebBanner_ON() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Email_OFF_WebBanner_ON(userCredentials);

	}

	@Test(priority = 7)
	public void Features_201_Notification_Internal_Mentions_Both_ON() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Internal_Mentions_Both_ON(userCredentials);

	}

	@Test(priority = 8)
	public void Features_202_Notification_Internal_Mentions_Both_OFF() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Internal_Mentions_Both_OFF(userCredentials);

	}

	@Test(priority = 9)
	public void Features_203_Notification_Internal_Mentions_Email_ON_WebBanner_OFF() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Internal_Mentions_Email_ON_WebBanner_OFF(userCredentials);

	}

	@Test(priority = 10)
	public void Features_204_Notification_Internal_Mentions_Email_OFF_WebBanner_ON() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Internal_Mentions_Email_OFF_WebBanner_ON(userCredentials);

	}

	@Test(priority = 11)
	public void Features_208_Notification_Conversation_trasfered_ON() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Conversation_trasfered_ON(userCredentials);

	}

	@Test(priority = 12)
	public void Features_209_Notification_Conversation_trasfered_OFF() throws Exception {

		Internal_NotificatinsWF INWF = new Internal_NotificatinsWF();
		INWF.Notification_Conversation_trasfered_OFF(userCredentials);

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
