package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.deeplink.Deeplink_withoutLoginWF;
import com.beetext.qa.base.TestBase;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.util.WebEventListener;

public class Deeplink_WithoutLogin_test extends TestBase {
	Map<String,String> map = new HashMap<String,String>();

	@BeforeClass
	public void setUp() {

		logger = Logger.getLogger("Beetexting Test Excution Logs report");
		PropertyConfigurator.configure("Log4j.properties");

		String browserName = prop.getProperty("browser");

		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					currentDir + "\\BrowserDrivers\\chromedriver_win32\\chromedrivers.exe");

			driver = new ChromeDriver();

			logger.info("Chrome Browser is launched ");
		} else if (browserName.equals("FF")) {
			System.setProperty("webdriver.gecko.driver", "C:/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			logger.info("Firefox  Browser is launched ");
		}
		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with
		// EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;

		driver.manage().window().maximize();
		logger.info("Browser window is maximized ");
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS); // create util class
																									// for timeunit
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);

		driver.get("https://www.yopmail.com/");

	}

	@Test(priority = 1)
	public void Deepplink_310_without_login() throws Exception {

		Deeplink_withoutLoginWF DLWF = new Deeplink_withoutLoginWF();
		DLWF.Deepplink_without_login();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
