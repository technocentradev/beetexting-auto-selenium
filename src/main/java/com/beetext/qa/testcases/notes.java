package com.beetext.qa.testcases;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.util.TestUtil;

public class notes extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	public static WebDriver driver;
	static String username1 = "auto1@yopmail.com";
	static String username2 = "automations@yopmail.com";
	static String password = "tech1234";

	public static void waitAndLog(int d) {
		try {
			Thread.sleep((long) (d * 1000));
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public static void implicitwaitAndLog(int d) {
		try {
			driver.manage().timeouts().implicitlyWait(d * 10, TimeUnit.SECONDS);
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public static void email(String email) throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys(email);
		waitAndLog(1);
	}

	public static void password(String password) throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys(password);
		waitAndLog(1);
	}

	public static void loginButton() throws InterruptedException {
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		waitAndLog(2);

	}

	public static void notification() {

		driver.findElement(By.xpath("//a[@id='notification']")).click();
	}

	public void validateSignOutLink() {
		driver.findElement(By.xpath("//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")).click();
	}

	public void signuppagelink() {
		driver.findElement(By.id("signupPage")).click();
	}

	public void validateSignOut() {
		driver.findElement(By.xpath("//a[@id='logoutId']")).click();
	}

	public void validateUser() {
		driver.findElement(By.xpath("//*[@class='dropdown-menu show']//a[contains(text(),'AutoTech')]")).click();
	}

	public void validatetoggle() {
		driver.findElement(By.xpath("//*[@class='mat-slide-toggle-thumb']")).click();
	}

	public void contactsTab() {

		waitAndLog(2);
		driver.findElement(By.xpath("//a[@id='contactTab']")).click();

	}

	public void conversationTab() {

		waitAndLog(2);
		driver.findElement(By.xpath("//a[@id='conversationTab']")).click();

	}

	public void contactsTab_numberlink() {

		driver.findElement(By.xpath("//a[@class='contact-info-link cursor-pointer']")).click();

	}

	public void contactsTab_numberlink2() {

		driver.findElement(By.xpath("(//a[@class='contact-info-link cursor-pointer'])[2]")).click();

	}

	public void contactInfo() {

		String text = driver.findElement(By.xpath("//h5[contains(text(),' Contact Info ')]")).getText();
		logger.info(text);
	}

	public void contactInfo_CloseButton() {

		driver.findElement(By.xpath("//button[contains(text(),'Close')]")).click();

	}

	public void contactInfo_ContactDetails() {

		String text = driver.findElement(By.xpath("//div[contains(@class,'offset')]")).getText();
		logger.info(text);
	}

	public static void Automation_Contact() throws Exception {

		driver.findElement(By.xpath("//h4[normalize-space()='Automation']")).click();

	}

	public static void number_inContactsTab() throws Exception {

		driver.findElement(By.xpath("//h4[normalize-space()='(203) 208-6377']")).click();

	}

	public static void groupCOnversation() throws Exception {

		driver.findElement(By.xpath("//h4[normalize-space()='Automation, QA10']")).click();

	}

	public static void department_Head() throws Exception {

		driver.findElement(By.xpath("//div[@id='hoverSelectDepartment']")).click();

	}

	public static void LocalDept() throws Exception {

		driver.findElement(By.xpath("//button[normalize-space()='Local']")).click();

	}

	public static void Auto1_Dept() throws Exception {

		driver.findElement(By.xpath("//span[@id='Auto1_Department']")).click();

	}

	public static void TollFree_department() throws Exception {

		driver.findElement(By.xpath("//span[normalize-space()='Automation TollFree']")).click();

	}

	public static void TollFree_Button_department() throws Exception {

		driver.findElement(By.xpath("(//button[contains(text(),'Toll-Free')])[1]")).click();

	}

	public static void note_input_Area(String value) throws Exception {

		driver.findElement(By.xpath("//textarea[@id='noteinput']")).sendKeys(value);

	}

	public static void note_input_addButton() throws Exception {

		driver.findElement(By.xpath("//button[@id='noteInputAddBtn']")).click();

	}

	public static void note_DeleteButton() throws Exception {

		driver.findElement(By.xpath("//svg-icon[@id='notesEditDelete']")).click();

	}

	public static void note_DeletePopup_DeleteButton() throws Exception {

		driver.findElement(By.xpath("//button[@id='deleteNoteButton']")).click();

	}

	public static void note_DeletePopup_CancelButton() throws Exception {

		driver.findElement(By.xpath("//button[contains(text(),'CANCEL')]")).click();

	}

	public static void note_saveButton() throws Exception {

		driver.findElement(By.xpath("//button[@id='notesEditSave']")).click();

	}

	public static void QAContact() throws Exception {

		driver.findElement(By.xpath("//h4[normalize-space()='QA10']")).click();

	}

	public static void createcontactclose() throws Exception {

		driver.findElement(By.xpath("//a[@id='createContactClose']")).click();

	}

	@BeforeClass
	public void setUp() throws Exception {

		driver = init("chrome");
		driver.manage().window().maximize();
		waitAndLog(10);
		driver.get("http://automation.beetexting.com");
		waitAndLog(10);
		email(username2);
		password(password);
		waitAndLog(2);
		loginButton();
		waitAndLog(5);

	}

	@Test(priority = 1)
	public void notes_01_contactTab_allows_upto_500Char() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		String notes = RandomStringUtils.randomAlphabetic(500);

		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);

	}

	@Test(priority = 2)
	public void notes_02_contactTab_allows_upto_500Char_Error_textMsg() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		String notes = RandomStringUtils.randomAlphabetic(500);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);

		String notes_error_text = driver
				.findElement(By.xpath("//div[contains(text(),'Limit Note length to 500 characters.')]")).getText();

		logger.info(notes_error_text);

		if (notes_error_text.equalsIgnoreCase("Limit Note length to 500 characters.")) {
			Assert.assertEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is matching");
			logger.info(notes_error_text);
			TestUtil.takeScreenshotAtEndOfTest("resendemailotpTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is no matching");
			logger.info(notes_error_text + " -----> Failed");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotptestcase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	@Test(priority = 3)
	public void notes_03_CreateContact_Notes_allows_upto_500Char() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		driver.findElement(By.xpath("//a[@id='messages-createContact']")).click();
		logger.info("Click on add symbol to create a contact");
		waitAndLog(2);
		String notes = RandomStringUtils.randomAlphabetic(500);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='contactNotesId']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);

		String notes_error_text = driver
				.findElement(By.xpath("//div[contains(text(),'Limit Note length to 500 characters.')]")).getText();

		logger.info(notes_error_text);

		if (notes_error_text.equalsIgnoreCase("Limit Note length to 500 characters.")) {
			Assert.assertEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is matching");
			logger.info(notes_error_text);
			TestUtil.takeScreenshotAtEndOfTest("resendemailotpTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is no matching");
			logger.info(notes_error_text + " -----> Failed");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotptestcase");// ScreenShot capture
		}
		waitAndLog(2);
		createcontactclose();
		waitAndLog(2);

	}

	@Test(priority = 4)
	public void notes_04_ConversationTab_allows_upto_500Char() throws Exception {

		waitAndLog(2);
		conversationTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		String notes = RandomStringUtils.randomAlphabetic(500);

		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);

	}

	@Test(priority = 5)
	public void notes_05_ConversationTab_upto_500Char_Error_textMsg() throws Exception {

		waitAndLog(2);
		conversationTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		String notes = RandomStringUtils.randomAlphabetic(500);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);

		String notes_error_text = driver
				.findElement(By.xpath("//div[contains(text(),'Limit Note length to 500 characters.')]")).getText();

		logger.info(notes_error_text);

		if (notes_error_text.equalsIgnoreCase("Limit Note length to 500 characters.")) {
			Assert.assertEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is matching");
			logger.info(notes_error_text);
			TestUtil.takeScreenshotAtEndOfTest("resendemailotpTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is no matching");
			logger.info(notes_error_text + " -----> Failed");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotptestcase");// ScreenShot capture
		}
		waitAndLog(2);

	}

	@Test(priority = 6)
	public void notes_06_ContactsTab_CreateContcat_400Char_EditWith_100Char_Error_textMsg() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		number_inContactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(400);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		number_inContactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		String notes1 = RandomStringUtils.randomAlphabetic(100);
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();

		logger.info("Click on 1st Note");
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='notesEditInput']")).sendKeys(notes1);
		logger.info("Enter text in notes box -- > " + notes1);
		waitAndLog(2);

		String notes_error_text = driver
				.findElement(By.xpath("//div[contains(text(),'Limit Note length to 500 characters.')]")).getText();

		logger.info(notes_error_text);

		if (notes_error_text.equalsIgnoreCase("Limit Note length to 500 characters.")) {
			Assert.assertEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is matching");
			logger.info(notes_error_text);
			TestUtil.takeScreenshotAtEndOfTest("resendemailotpTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is no matching");
			logger.info(notes_error_text + " -----> Failed");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotptestcase");// ScreenShot capture
		}
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@Test(priority = 7)
	public void notes_07_ConversationTab_CreateContcat_400Char_EditWith_100Char_Error_textMsg() throws Exception {

		waitAndLog(2);
		conversationTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		QAContact();
		logger.info("Click on QAContact");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(400);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);
		conversationTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		QAContact();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		String notes1 = RandomStringUtils.randomAlphabetic(100);
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();

		logger.info("Click on 1st Note");
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='notesEditInput']")).sendKeys(notes1);
		logger.info("Enter text in notes box -- > " + notes1);
		waitAndLog(2);

		String notes_error_text = driver
				.findElement(By.xpath("//div[contains(text(),'Limit Note length to 500 characters.')]")).getText();

		logger.info(notes_error_text);

		if (notes_error_text.equalsIgnoreCase("Limit Note length to 500 characters.")) {
			Assert.assertEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is matching");
			logger.info(notes_error_text);
			TestUtil.takeScreenshotAtEndOfTest("resendemailotpTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is no matching");
			logger.info(notes_error_text + " -----> Failed");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotptestcase");// ScreenShot capture
		}
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@Test(priority = 8)
	public void notes_08_CreateContcat_400Char_EditWith_100Char_Error_textMsg() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);
		driver.findElement(By.xpath("//a[@id='messages-createContact']")).click();
		logger.info("Click on add symbol to create a contact");
		waitAndLog(2);
		String notes = RandomStringUtils.randomAlphabetic(400);
		waitAndLog(2);

		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("CreateContact");
		waitAndLog(2);
		String number = RandomStringUtils.randomNumeric(7);
		driver.findElement(By.xpath("//input[@id='contactNumberId']")).sendKeys("555" + number);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='contactNotesId']")).sendKeys(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);

		driver.findElement(By.xpath("//button[@id='saveButton']")).click();
		waitAndLog(2);
		conversationTab();
		waitAndLog(2);
		contactsTab();
		waitAndLog(2);
		waitAndLog(5);

		driver.findElement(By.xpath("//*[@id='composenewBtn']")).click();

		waitAndLog(2);
		driver.findElement(By.xpath("//*[@id='contactSearchInput']")).sendKeys("CreateContact");

		waitAndLog(2);
		driver.findElement(By.xpath("//Span[@class='col-6 contact-pipe inline-display']")).click();
		waitAndLog(2);
		String notes1 = RandomStringUtils.randomAlphabetic(100);
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();

		logger.info("Click on 1st Note");
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='notesEditInput']")).sendKeys(notes1);
		logger.info("Enter text in notes box -- > " + notes1);
		waitAndLog(2);

		String notes_error_text = driver
				.findElement(By.xpath("//div[contains(text(),'Limit Note length to 500 characters.')]")).getText();

		logger.info(notes_error_text);

		if (notes_error_text.equalsIgnoreCase("Limit Note length to 500 characters.")) {
			Assert.assertEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is matching");
			logger.info(notes_error_text);
			TestUtil.takeScreenshotAtEndOfTest("resendemailotpTestCase");// ScreenShot capture

		} else {
			Assert.assertNotEquals(notes_error_text, "Limit Note length to 500 characters.",
					"Limit Note length to 500 characters is no matching");
			logger.info(notes_error_text + " -----> Failed");
			TestUtil.takeScreenshotAtEndOfTest("resendemailotptestcase");// ScreenShot capture
		}
		waitAndLog(3);

		driver.findElement(By.xpath("//svg-icon[@id='contactPencilIcon']")).click();
		waitAndLog(3);
		String generatedstring = RandomStringUtils.randomAlphabetic(8);
		driver.findElement(By.xpath("//input[@id='firstName']")).clear();
		waitAndLog(3);
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys(generatedstring);
		waitAndLog(3);
		driver.findElement(By.xpath("//button[@id='contactSaveButton']")).click();
		waitAndLog(3);
	}

	@Test(priority = 9)
	public void notes_9_ContactsTab_multiple500Char_notes() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		number_inContactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(500);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@Test(priority = 10)
	public void notes_10_ConversationTab_multiple500Char_notes() throws Exception {

		waitAndLog(2);
		conversationTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		QAContact();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(500);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@Test(priority = 11)
	public void notes_12_ContactsTab_500Char_notes_Delete() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		number_inContactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(500);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@Test(priority = 12)
	public void notes_13_ConversationTab_500Char_notes_Delete() throws Exception {

		waitAndLog(2);
		conversationTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		QAContact();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(400);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@Test(priority = 13)
	public void notes_14_ContactsTab_500Char_notes_cancel() throws Exception {

		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		number_inContactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(500);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);
		waitAndLog(2);
		contactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		number_inContactsTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_CancelButton();
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@Test(priority = 14)
	public void notes_15_ConversationTab_500Char_notes_Delete() throws Exception {

		waitAndLog(2);
		conversationTab();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		QAContact();
		logger.info("Click on contactsTab");
		waitAndLog(2);

		String notes = RandomStringUtils.randomAlphabetic(400);
		waitAndLog(2);
		driver.findElement(By.xpath("//textarea[@id='noteinput']")).clear();
		waitAndLog(2);
		note_input_Area(notes);
		logger.info("Enter text in notes box -- > " + notes);
		waitAndLog(2);
		note_input_addButton();
		waitAndLog(2);
		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_CancelButton();
		waitAndLog(2);

		driver.findElement(By.xpath("//p[@class='note form-control ng-star-inserted']")).click();
		logger.info("Click on 1st Note");
		waitAndLog(2);
		note_DeleteButton();
		waitAndLog(3);
		note_DeletePopup_DeleteButton();
		waitAndLog(3);
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
