package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Reviews_Workflow_CM;
import com.beetext.qa.Workflows.Review_Page_WF;
import com.beetext.qa.base.TestBase;

public class Review_Edit_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Reviews_Workflow_CM RCM = new Reviews_Workflow_CM();
		RCM.login2();

//	   Login();   for login

	}

	@Test(priority = 3) // 24
	public void TC_240_Manage_button_For_Review_Record_isEnable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Review_list_Page_Mange_button_isEnable();

	}

	@Test(priority = 4) // 25
	public void TC_241_Click_on_Manage_Button_Navigate_to_Edit_Manage_review_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.TC_241_Review_Page_click_MangeButton_Navigates_ManageReviewPage();

	}

	@Test(priority = 5) // 26
	public void TC_242_Edit_Manage_review_page_Review_record_Related_AllData_Isavailable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Edit_Review_Page_Review_record_AllData_isavailable();

	}

	@Test(priority = 2) // 15
	public void TC_243_Verify_all_options_in_SelectSite_DDList_In_edit_IsAvailable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.verify_All_options_in_SelectSite_DD_Edit_case();

	}

	@Test(priority = 1) // 8
	public void TC_244_Edit_Manage_review_page_AddSite_Isavailable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Edit_Review_Page_Add_Site_textField_isEnable();

	}

	@Test(priority = 6) // 28
	public void TC_245_Verify_record_site_information_in_edit_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.verify_site_information_for_record_in_edit_page();

	}

	@Test(priority = 14) // 47
	public void TC_249_Edit_Review_page_AddSite_Duplicate_Message() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Edit_Review_page_AddSite_Duplicate_text_message();

	}

	@Test(priority = 7) // 29
	public void TC_251_Link_text_field_in_Edit_page_Allows_only_5000_char() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Edit_review_Link_Textbox_allows_5000_char();

	}

	@Test(priority = 15) // 49
	public void TC_252_Edit_Review_page_mandatory_text_message_for_Link_textbox() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Edit_Review_page_mandatory_text_Message_for_Link_Textbox();

	}

	@Test(priority = 8) // 30
	public void TC_253_Edit_Review_page_save_button_IsEnabled() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.edit_Review_page_save_button_isEnable();

	}

	@Test(priority = 9) // 31
	public void TC_254_Functionality_of_link_in_Edit_review_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.functionality_of_link_in_Edit_reviews_page();

	}

	@Test(priority = 10) // 32
	public void TC_256_Edit_Review_page_Click_on_save_redirects_to_Reviews_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.edit_Review_page_redirects_to_Reviews_page_by_click_on_Save_button();

	}

	@Test(priority = 11) // 33
	public void TC_257_Verify_toaster_message_for_Updated_review_record() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_Toaster_message_for_updated_review_record();

	}

	@Test(priority = 12) // 34
	public void TC_258_Edit_record_under_list_isavailable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Edited_review_record_Shows_under_review_list();

	}

	@Test(priority = 13) // 35
	public void TC_259_Edit_record_toggle_button_under_list_ischecked() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Edited_review_record_verify_toggle_button_isSelected();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
