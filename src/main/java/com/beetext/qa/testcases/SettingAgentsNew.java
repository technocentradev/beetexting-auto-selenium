package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.settings.SettingsAgentsWF;
import com.beetext.qa.base.TestBase;

public class SettingAgentsNew extends TestBase {
	Map<String, String> map = new HashMap<String, String>();

	@BeforeMethod()
	public void setUp() {
		initialization();
		Login(); // for login
		driver.findElement(By.xpath("//span[@class='s-title']")).click();

	}

	@Test(priority = 1)
	public void Settings_087_invite_aleary_existing_agent_email() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agentemail_duplicate_validation();

	}

	@Test(priority = 2)
	public void Settings_087_delete_existing_agent_add_same_agent() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.delete_existing_agent_add_same_email();

	}

	@Test(priority = 3)
	public void deleteagent_try_to_accept() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_delete_agent_try_to_accept();
	}

	@Test(priority = 4)
	public void deleteagent_try_to_reject() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_delete_agent_try_to_reject();
	}

	@Test(priority = 5)
	public void inactive_agent_add_same_agentemail_one_more_time() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_is_inactive_tryto_add_same_agent_email_one_more_time();
	}

	@Test(priority = 6)
	public void alreadydeleted_agent_add_same_agentemail_one_more_time_admin_role() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_is_inactive_tryto_add_same_agent_email_one_more_time();
	}

	@Test(priority = 7)
	public void alreadydeleted_agent_add_same_agentemail_one_more_time_user_role() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.alreadydeletedagent_add_same_agent_email_one_more_time_user_role();
	}

	@Test(priority = 8)
	public void multiorg_oneorg_invite_agent_try_to_add_same_agent_inother_org() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.mulitorg_send_agent_invitation_otherorg_try_to_add_same_agent();
	}

	@Test(priority = 9)
	public void multiorg_oneorg_invite_agent_accept_try_to_add_same_agent_inother_org() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_accept_the_invitation_otherorg_try_to_add_same_agent();
	}

	@Test(priority = 10)
	public void multiorg_oneorg_invite_agent_delete_try_to_add_same_agent_inother_org() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_delete_agent_otherorg_try_to_add_same_agent();
	}

	@Test(priority = 11)
	public void multiorg_oneorg_invite_useragent_delete_try_to_add_same_agent_inother_org() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.userAgent_invite_sending_delete_agent_otherorg_try_to_add_same_agent();
	}

	@Test(priority = 12)
	public void Settings_087_delete_existing_agent_delete_invite_same_agent_accept() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_accept_the_invitation();

	}

	@Test(priority = 13)
	public void Settings_088_delete_existing_agent_delete_invite_same_agent_reject() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_reject_the_invitation_add_same_agent();

	}

	@Test(priority = 14)
	public void Settings_087_invite_agent_same_agent_invite_onemore_time() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_again_add_same_agent_email();

	}

	@Test(priority = 15)
	public void Settings_087_invite_agent_reject_the_invitation_add_same_agent_one_more_time() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.Agent_invite_sending_reject_the_invitation_add_same_agent_one_more_time();

	}

	@Test(priority = 16)
	public void rejectedagent_add_one_more_time() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.addrejectemail_id();
	}

	@Test(priority = 17)
	public void multiorg_oneorg_invite_agent_reject_try_to_add_same_agent_inother_org() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.mulitorg_send_agent_invitation_reject_otherorg_try_to_add_same_agent();
	}

	@Test(priority = 18)
	public void same_agent_add_inother_org() throws Exception {
		SettingsAgentsWF agentWF = new SettingsAgentsWF();
		agentWF.addsameagent();
	}

	@AfterMethod()
	public void teardown() {
		driver.quit();
	}
}
