package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.templates.ToolsTemplateWF;
import com.beetext.qa.base.TestBase;

public class ToolsTemplatesPart3 extends TestBase {


	@BeforeClass
	public void setUp() {
		initialization();
		Login();
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
	}

	@Test(priority = 1)
	public void Tools_Template_943_944_Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_navigationlink_clickon_savedtemplate_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_navigationlink_clickon_save_template_button(
						);
	}

	@Test(priority = 2)
	public void Tools_Template_945_Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_navigationlink_clickon_discardtemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_navigationlink_clickon_discard_template_button(
						);
	}

	@Test(priority = 3)
	public void Tools_Template_946_947_Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_navigationlink_clickon_savetemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_anyotherlink_clickon_save_template_button(
						);
	}

	@Test(priority = 4)
	public void Tools_Template_948_Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_navigationlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_anyohterlink_clickon_discard_template_button(
						);
	}

	@Test(priority = 5)
	public void Tools_Template_949_950Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_close_toolspopup_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_closetoolspopupicon_clickon_save_template_button(
						);
	}

	@Test(priority = 6)
	public void Tools_Template_951_Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_close_toolspopup_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_closetoolspopupicon_clickon_discard_template_button(
						);
	}

	@Test(priority = 7)
	public void Tools_Template_969_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_update_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_Update_template_button(
						);
	}

	@Test(priority = 8)
	public void Tools_Template_970_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_savedraft_template_button(
						);
	}

	@Test(priority = 9)
	public void Tools_Template_971_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_delete_confirm_delete_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_delete_confirmdelete_template_button(
						);
	}

	@Test(priority = 10)
	public void Tools_Template_972_973_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_anyotherlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_anyohterlink_clickon_save_template_button(
						);
	}

	@Test(priority = 11)
	public void Tools_Template_974_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_anyotherlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_anyohterlink_clickon_discard_template_button(
						);
	}

	@Test(priority = 12)
	public void Tools_Template_975_976_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_breadscrumlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon__clickonbreadscrumlink_save_template_button(
						);
	}

	@Test(priority = 13)
	public void Tools_Template_977_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_breadscrumlink_clickon_discard_template_button(
						);
	}

	@Test(priority = 14)
	public void Tools_Template_978_979__Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_CLOSETOOLSPOPUP_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon__close_toolspopup_clickon_save_template_button(
						);
	}

	@Test(priority = 15)
	public void Tools_Template_980_Managepersonaltemplate_edit_template_title_remove_message_addemoji_clickon_closeToolspopup_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_removetmessage_add_emoji_clickon_closetooslpopup_clickon_discard_template_button(
						);
	}

	@Test(priority = 16)
	public void Tools_Template_984_Managepersonaltemplate_remove_template_title_add_message_addemoji_clickon_update_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_update_template_button();
	}

	@Test(priority = 17)
	public void Tools_Template_985_Managepersonaltemplate_remove_template_title_add_message_addemoji_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_savedraft_template_button(
						);
	}

	@Test(priority = 18)
	public void Tools_Template_986_Managepersonaltemplate_remove_template_title_add_message_addemoji_clickon_delete_confirm_delete_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_delete_confirmdelete_template_button(
						);
	}

	@Test(priority = 19)
	public void Tools_Template_987_988_Managepersonaltemplate_remove_template_title_add_message_addemoji_clckon_breadscrumlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_navigationlink_clickon_save_template_button(
						);
	}

	@Test(priority = 20)
	public void Tools_Template_989_Managepersonaltemplate_remove_template_title_add_message_addemoji_clckon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_navigationlink_clickon_discard_template_button(
						);
	}

	@Test(priority = 21)
	public void Tools_Template_990_991_Managepersonaltemplate_remove_template_title_add_message_addemoji_clckon_navigationlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_anyotherlink_clickon_save_template_button(
						);
	}

	@Test(priority = 22)
	public void Tools_Template_992_Managepersonaltemplate_remove_template_title_add_message_addemoji_clckon_navigationlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_anyotherlink_clickon_discard_template_button(
						);
	}

	// 08/09/2021

	@Test(priority = 23)
	public void Tools_Template_993_994_Managepersonaltemplate_remove_template_title_add_message_addemoji_close_toolspopup_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_clickon_closetoolspopup_clickon_save_template_button(
						);
	}

	@Test(priority = 24)
	public void Tools_Template_995_Managepersonaltemplate_remove_template_title_add_message_addemoji_close_toolspopup_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_add_message_add_emoji_close_tooslpopup_clickon_discard_template_button(
						);
	}

	@Test(priority = 25)
	public void Tools_Template_1000_Managesharedtemplate_remove_template_title_clickon_update_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managesharedtemplate_selectsharedopton_removetitle_clickon_Update_template_button();
	}

	@Test(priority = 26)
	public void Tools_Template_1001_Managesharedtemplate_remove_template_title_clickon_saveDraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managesharedtemplate_selectsharedopton_removetitle_clickon_savedraft_template_button();
	}

	@Test(priority = 27)
	public void Tools_Template_1002_Managesharedtemplate_remove_template_title_clickon_delete_confirm_delete_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managesharedtemplate_selectsharedopton_removetitle_clickon_delete_confirm_delete_template_button();
	}

	@Test(priority = 28)
	public void Tools_Template_1003_1004_Managesharedtemplate_remove_template_title_clickon_breadscrumlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectsharedopton_removetitl_clickonbreadscrumlink_clickon_save_template_button();
	}

	@Test(priority = 29)
	public void Tools_Template_1005_Managesharedtemplate_remove_template_title_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectsharedoption_removetitle_clickon_breadscrumlink_clickon_discard_template_button(
						);
	}

	@Test(priority = 30)
	public void Tools_Template_1006_1007_Managesharedtemplate_remove_template_title_clickon_navigationlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectsharedopton_removetitl_clickonnavigationlink_clickon_save_template_button();
	}

	@Test(priority = 31)
	public void Tools_Template_1008_Managesharedtemplate_remove_template_title_clickon_navigationlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectsharedoption_removetitle_clickon_navigationlink_clickon_discard_template_button(
						);
	}

	@Test(priority = 32)
	public void Tools_Template_1009_1010_Managesharedtemplate_remove_template_title_close_toolspoppup_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectsharedopton_removetitl_close_tools_popup_clickon_save_template_button();
	}

	@Test(priority = 33)
	public void Tools_Template_1011_Managesharedtemplate_remove_template_title_close_toolspopup_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectsharedoption_removetitle_close_toolspopup_clickon_discard_template_button();
	}

	@Test(priority = 34)
	public void Tools_Template_1011_clickon_sharedtab_display_sharedlist() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_sharedtab_dispalysharedlist();
	}

	@Test(priority = 35)
	public void Tools_Template_1012_clickon_sharedtab_clickon_create_template_button() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_sharedtab_clickon_createtemplate_button();
	}

	@Test(priority = 36)
	public void Tools_Template_1013_shared_createtemplatepage_title_min3char_validation() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_sharedtab_clickon_createtemplate_button_title_min_3_char_validation();
	}

	@Test(priority = 37)
	public void Tools_Template_1014_shared_withouttitle_entermessage_clickon_create_button() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_sharedtab_dont_enter_title_enteronly_message_clickon_createbutton();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();

	}
}
