package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.Workflows.vcf.VCF_off_redirectTo_VCFPage_WF;

public class VCF_off_redirectTo_VCFPage_Test extends VCF_WFCM {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		VCF_WFCM vcfwfcm = new VCF_WFCM();
		vcfwfcm.VCF_nominee_Login_For_VCFOff_Cases();
	}

	@Test(priority = 1)
	public void VCF_1_VCF_off_redircts_to_VCFPage_From_BM_Page() throws Exception {

		VCF_off_redirectTo_VCFPage_WF VCFWF = new VCF_off_redirectTo_VCFPage_WF();
		VCFWF.VCF_off_redircts_to_VCFPage_From_BM_Page();
	}

	@Test(priority = 2)
	public void VCF_2_VCF_off_redircts_to_VCFPage_From_SM_Page() throws Exception {

		VCF_off_redirectTo_VCFPage_WF VCFWF = new VCF_off_redirectTo_VCFPage_WF();
		VCFWF.VCF_off_redircts_to_VCFPage_From_SM_Page();
	}

	@Test(priority = 3)
	public void VCF_3_VCF_off_redircts_to_VCFPage_From_Templates_Page() throws Exception {

		VCF_off_redirectTo_VCFPage_WF VCFWF = new VCF_off_redirectTo_VCFPage_WF();
		VCFWF.VCF_off_redircts_to_VCFPage_From_Templates_Page();
	}

	@Test(priority = 4)
	public void VCF_4_VCF_off_redircts_to_VCFPage_From_Conv_Page() throws Exception {

		VCF_off_redirectTo_VCFPage_WF VCFWF = new VCF_off_redirectTo_VCFPage_WF();
		VCFWF.VCF_off_redircts_to_VCFPage_From_Conv_Page();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
