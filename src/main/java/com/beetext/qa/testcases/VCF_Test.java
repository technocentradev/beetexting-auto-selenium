package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.vcf.VCF_WF;
import com.beetext.qa.base.TestBase;

public class VCF_Test extends TestBase {

	@BeforeClass
	public void setUp() {
		initialization();
	}

	@Test(priority = 1)
	public void VCF_1_Vcard_toggle() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_toggle();
	}

	@Test(priority = 2)
	public void VCF_2_4_7_8_9_Vcard_toggle_Enable_Disable_Click_on_CrossMark() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_toggle_Enable_Disable_Click_on_CrossMark();
	}

	@Test(priority = 3)
	public void VCF_3_Vcard_Available_Department() throws Exception {
		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Available_Department();
	}

	@Test(priority = 4)
	public void VCF_10_22_Vcard_Valid_Details_Names_Description_Allows_Special_Characers_CloseWindow()
			throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Valid_Details_Names_Description_Allows_Special_Characers_CloseWindow();
	}

	@Test(priority = 5)
	public void VCF_12_Vcard_Valid_Details_Click_Save() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Valid_Details_Click_Save();
	}

	@Test(priority = 6)
	public void VCF_13_Vcard_Valid_Details_Click_Cancel() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Valid_Details_Click_Cancel();
	}

	@Test(priority = 7)
	public void VCF_14_Vcard_If_Any_UI_Error_Save_in_Disable() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_If_Any_UI_Error_Save_in_Disable();
	}

	@Test(priority = 8)
	public void VCF_15_Vcard_Valid_Details_Click_Outside() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Valid_Details_Click_Outside();
	}

	@Test(priority = 9)
	public void VCF_16_17_20_Vcard_PhoneNUmber_US_Format_10_Digits() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_PhoneNUmber_US_Format_10_Digits();
	}

	@Test(priority = 10)
	public void VCF_23_Vcard_Valid_Email() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Valid_Email();
	}

	@Test(priority = 11)
	public void VCF_24_Vcard_Email_Error() throws Exception {
		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Email_Error();
	}

	@Test(priority = 12)
	public void VCF_26_27_Vcard_Company_Allows_50_Char_Error() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Company_Allows_50_Char_Error();
	}

	@Test(priority = 13)
	public void VCF_28_Vcard_Company_Allows_Note_message() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Company_Allows_Note_message();
	}

	@Test(priority = 14)
	public void VCF_29_Vcard_Profile_Pic_Upload() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Profile_Pic_Upload();
	}

	@Test(priority = 15)
	public void VCF_30_Vcard_Profile_Pic_Upload_Error() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Profile_Pic_Upload_Error();
	}

	@Test(priority = 16)
	public void VCF_31_Vcard_Admin_Can_Send_Vcard() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Admin_Can_Send_Vcard();
	}

	@Test(priority = 17)
	public void VCF_32_Vcard_Admin_Can_Send_Vcard_Along_With_Msg_Attachment() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Admin_Can_Send_Vcard_Along_With_Msg_Attachment();
	}

	@Test(priority = 18)
	public void VCF_33_Vcard_Admin_Can_Send_Vcard_Attachment() throws Exception {
		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Admin_Can_Send_Vcard_Attachment();
	}

	@Test(priority = 19)
	public void VCF_34_36_Send_Vcard_Validation_Vcard_For_Single_Contact() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Send_Vcard_Validation_Vcard_For_Single_Contact();
	}

	@Test(priority = 20)
	public void VCF_35_VCard_Send_wih_Emoji() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Send_wih_Emoji();
	}

	@Test(priority = 21)
	public void VCF_37_VCard_For_Group_Conversation() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_For_Group_Conversation();
	}

	@Test(priority = 22)
	public void VCF_39_42_VCard_Agent_Can_Sent_Vcard_For_Single_Conversation() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Agent_Can_Sent_Vcard_For_Single_Conversation();
	}

	@Test(priority = 23)
	public void VCF_41_Vcard_Agent_Can_Send_Vcard_Along_With_Msg_Emoji_Attachment() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Agent_Can_Send_Vcard_Along_With_Msg_Emoji_Attachment();
	}

	@Test(priority = 24)
	public void VCF_43_VCard_Agent_Can_Send_Group_Conversation() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Agent_Can_Send_Group_Conversation();
	}

	@Test(priority = 25)
	public void VCF_44_VCard_Manage() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Manage();
	}

	@Test(priority = 26)
	public void VCF_45_Click_On_VCard_Navigate_To_ThatPage() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Click_On_VCard_Navigate_To_ThatPage();
	}

	@Test(priority = 27)
	public void VCF_46_Vcard_By_Default_DeptNumber_taken_as_Firstname() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_By_Default_DeptNumber_taken_as_Firstname();
	}

	@Test(priority = 28)
	public void VCF_47_Vcard_firstname_50_Char_Error() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_firstname_50_Char_Error();
	}

	@Test(priority = 29)
	public void VCF_50_53_Vcard_Alldetails_Cancel() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Alldetails_Cancel();
	}

	@Test(priority = 30)
	public void VCF_54_56_VCard_Agent_Can_See_Vcard_Details_not_able_to_Edit() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Agent_Can_See_Vcard_Details_not_able_to_Edit();
	}

	@Test(priority = 31)
	public void VCF_51_52_Vcard_Alldetails_Save() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Alldetails_Save();
	}

	@Test(priority = 32)
	public void VCF_57_VCard_Enable_in_Conversation() throws Exception {
		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Enable_in_Conversation();
	}

	@Test(priority = 33)
	public void VCF_58_VCard_Selection() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Selection();
	}

	@Test(priority = 34)
	public void VCF_59_60_Vcard_Sending_Validation_at_ReceivingSide() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Sending_Validation_at_ReceivingSide();
	}

	@Test(priority = 35)
	public void VCF_61_Vcard_Msg_validation() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Msg_validation();
	}

	@Test(priority = 36)
	public void VCF_62_VCard_9_Attachments() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_9_Attachments();
	}

	@Test(priority = 37)
	public void VCF_63_VCard_1000_CharMsg() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_1000_CharMsg();
	}

	@Test(priority = 38)
	public void VCF_64_VCard_Emoji() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Emoji();
	}

	@Test(priority = 39)
	public void VCF_65_VCard_For_Group_Conversation_with_attachment() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_For_Group_Conversation_with_attachment();
	}

	@Test(priority = 40)
	public void VCF_66_VCard_Compose_Contact_VCard_send() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Compose_Contact_VCard_send();
	}

	@Test(priority = 41)
	public void VCF_67_VCard_Compose_Contact_VCard_send_msg_Attachment() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Compose_Contact_VCard_send_msg_Attachment();
	}

	@Test(priority = 42)
	public void VCF_68_VCard_Compose_Group_Conversation() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Compose_Group_Conversation();
	}

	@Test(priority = 43)
	public void VCF_69_VCard_Compose_Group_Conversation_with_Msg_attachment() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Compose_Group_Conversation_with_Msg_attachment();
	}

	@Test(priority = 44)
	public void VCF_70_95_VCard_Disable_After_Clicking() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Disable_After_Clicking();
	}

	@Test(priority = 45)
	public void VCF_71_Vcard_Attaches_Department_Only() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Attaches_Department_Only();
	}

	@Test(priority = 46)
	public void VCF_76_Compose_Number_VCard_Msg_Multiple_Attachmens() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Compose_Number_VCard_Msg_Multiple_Attachmens();
	}

	@Test(priority = 47)
	public void VCF_78_Compose_Number_VCard_1000_Char() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Compose_Number_VCard_1000_Char();
	}

	@Test(priority = 48)
	public void VCF_79_Compose_Number_VCard_Emoji() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Compose_Number_VCard_Emoji();
	}

	@Test(priority = 49)
	public void VCF_80_Admin_Group_Conversation_VCard() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Admin_Group_Conversation_VCard();
	}

	@Test(priority = 50)
	public void VCF_81_Admin_Group_Conversation_VCard_Msg_Attachment() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Admin_Group_Conversation_VCard_Msg_Attachment();
	}

	@Test(priority = 51)
	public void VCF_83_84_VCard_Available_In_BM_SM_Templates_Automations() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Available_In_BM_SM_Templates_Automations();
	}

	@Test(priority = 52)
	public void VCF_84_BM_Vcard_Enable() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.BM_Vcard_Enable();
	}

	@Test(priority = 53)
	public void VCF_89_91_Vcard_Prefix_Suffix_10_Char_Error() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Prefix_Suffix_10_Char_Error();
	}

	@Test(priority = 54)
	public void VCF_90_98_Vcard_Company_First_Middle_Last_Allows_50_Char_Error() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_Company_First_Middle_Last_Allows_50_Char_Error();
	}

	@Test(priority = 55)
	public void VCF_93_BM_VCard_9_Attachments() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.BM_VCard_9_Attachments();
	}

	@Test(priority = 56)
	public void VCF_97_BM_Click_Review_VCard_Disable() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.BM_Click_Review_VCard_Disable();
	}

	@Test(priority = 57)
	public void VCF_100_VCF_Conversation_Click_on_VCard_Switch_to_ContactsTab_Andback_to_Conversation()
			throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCF_Conversation_Click_on_VCard_Switch_to_ContactsTab_Andback_to_Conversation();
	}

	@Test(priority = 58)
	public void VCF_101_tollFree_Dept_VCard_Available() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.tollFree_Dept_VCard_Available();
	}

	@Test(priority = 59)
	public void VCF_102_tollFree_Dept_VCard__Emoji_Msg() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.tollFree_Dept_VCard__Emoji_Msg();
	}

	@Test(priority = 60)
	public void VCF_103_106_tollFree_Dept_VCard_Single_Contact() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.tollFree_Dept_VCard_Single_Contact();
	}

	@Test(priority = 61)
	public void VCF_104_tollFree_Dept_VCard_9Attachments() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.tollFree_Dept_VCard_9Attachments();
	}

	@Test(priority = 62)
	public void VCF_107_VCard_Save_Updated_Toggle_in_Same_Position() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_Save_Updated_Toggle_in_Same_Position();
	}

	@Test(priority = 63)
	public void VCF_109_Vcard_BM_VCF_9Attachments_Manage_Unable_add_2ndVCF() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_BM_VCF_9Attachments_Manage_Unable_add_2ndVCF();
	}

	@Test(priority = 64)
	public void VCF_110_Vcard_SM_VCF_9Attachments_Manage_Unable_add_2ndVCF() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Vcard_SM_VCF_9Attachments_Manage_Unable_add_2ndVCF();
	}

	@Test(priority = 65)
	public void VCF_111_Tempalates_Shared_10Attachments_Error_For_11th_Attachment() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Tempalates_Shared_10Attachments_Error_For_11th_Attachment();
	}

	@Test(priority = 66)
	public void VCF_114_Select_VCF_Switch_Other_Contact_then_to_PreviousContact() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Select_VCF_Switch_Other_Contact_then_to_PreviousContact();
	}

	@Test(priority = 67)
	public void VCF_119_Settings_Roles_VCard_Details() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Settings_Roles_VCard_Details();
	}

	@Test(priority = 68)
	public void VCF_120_Converastion_Select_VCard_Use_oneTemplate_VCard_Enabled() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Converastion_Select_VCard_Use_oneTemplate_VCard_Enabled();
	}

	@Test(priority = 69)
	public void VCF_122_Select_VCard_Click_ON_PinIcon_Deselect_PinIcon_VCardIcon_Enable() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.Select_VCard_Click_ON_PinIcon_Deselect_PinIcon_VCardIcon_Enable();
	}

	@Test(priority = 70)
	public void VCF_128_129_VCard_toggle_Off_Save_toaster_toggleON_changeData_click_Outside() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCard_toggle_Off_Save_toaster_toggleON_changeData_click_Outside();
	}

	@Test(priority = 71)
	public void VCF_87_VCF_BM_Dep1_Have_VCard_Dep2_Donthave_VCard_Switch_VCFEnable() throws Exception {

		VCF_WF VCFWF = new VCF_WF();
		VCFWF.VCF_BM_Dep1_Have_VCard_Dep2_Donthave_VCard_Switch_VCFEnable();
	}

	@AfterMethod
	public void validate_Signout() throws Exception {
		VCard_Signout();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
