package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.Workflows.vcf.vcf_SM_WF;
import com.beetext.qa.base.TestBase;

public class vcf_Scheduled_Messages_Test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		VCF_WFCM vcfwf = new VCF_WFCM();
		vcfwf.VCard_Auto1_Login_tools_Button();
	}

	@Test(priority = 1)
	public void vcf_SM_01_VCF_SM_1000_TextMsg_Vcf() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_1000_TextMsg_Vcf();
	}

	@Test(priority = 2)
	public void vcf_SM_02_VCF_9Attachments_Vcf() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_9Attachments_Vcf();
	}

	@Test(priority = 3)
	public void vcf_SM_03_VCF_SM_Attachment_Vcf_Non_Recurring() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_Attachment_Vcf_Non_Recurring();
	}

	@Test(priority = 4)
	public void vcf_SM_04_VCF_SM_Attachment_Vcf_Recurring() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_Attachment_Vcf_Recurring();
	}

	@Test(priority = 5)
	public void vcf_SM_05_VCF_SM_Attachment_Vcf_textMsg_Emoji_Non_Recurring() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_Attachment_Vcf_textMsg_Emoji_Non_Recurring();
	}

	@Test(priority = 6)
	public void vcf_SM_06_VCF_SM_Attachment_Vcf_textMsg_Emoji() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_Attachment_Vcf_textMsg_Emoji();
	}

	@Test(priority = 7)
	public void vcf_SM_07_VCF_SM_Emoji_Vcf() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_Emoji_Vcf();
	}

	@Test(priority = 8)
	public void vcf_SM_08_VCF_SM_Vcf_textMsg_Emoji() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_Vcf_textMsg_Emoji();
	}

	@Test(priority = 9)
	public void vcf_SM_09_10_11_VCF_SM_admin_enable_Disable_VCF_also_For_Mange_Dept_File_Enabled() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_admin_enable_Disable_VCF_also_For_Mange_Dept_File_Enabled();
	}

	@Test(priority = 10)
	public void vcf_SM_12_VCF_SM_After_Selecting_Vcard_it_Should_Be_in_Disabled() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_After_Selecting_Vcard_it_Should_Be_in_Disabled();
	}

	@Test(priority = 11)
	public void vcf_SM_13_VCF_SM_Attachment_Vcf_TextMsg_Emoji() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_Attachment_Vcf_TextMsg_Emoji();
	}

	@Test(priority = 12)
	public void vcf_SM_14_VCF_10Attachments_Vcf_Shows_Error() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_10Attachments_Vcf_Shows_Error();
	}

	@Test(priority = 13)
	public void vcf_SM_15_VCF_SM_VCF_For_group_Conversation() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_VCF_For_group_Conversation();
	}

	@Test(priority = 14)
	public void vcf_SM_16_VCF_SM_VCF_For_TollFree() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_VCF_For_TollFree();
	}

	@Test(priority = 15)
	public void vcf_SM_17_VCF_SM_VCF_For_one_to_one_Conversation() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_VCF_For_one_to_one_Conversation();
	}

	@Test(priority = 16)
	public void vcf_SM_18_VCF_SM_addVCF_manage_SM_Remove_add_Again_VCF() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_addVCF_manage_SM_Remove_add_Again_VCF();
	}

	@Test(priority = 17)
	public void vcf_SM_19_VCF_SM_addVCF_manage_SM_Remove_add_Again_VCF_Emoji_text() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_addVCF_manage_SM_Remove_add_Again_VCF_Emoji_text();
	}

	@Test(priority = 18)
	public void vcf_SM_20_Create_SM_with_1Attachment_and_VCF() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.Creat_SM_with_1Attachment_and_VCF();
	}

	@Test(priority = 19)
	public void vcf_SM_21_VCF_SM_addVCF_manage_SM_try_to_AddVCF_it_isIn_Disabled() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_addVCF_manage_SM_try_to_AddVCF_it_isIn_Disabled();
	}

	@Test(priority = 20)
	public void vcf_SM_22_VCF_SM_addVCF_then_it_Should_Be_in_Disabled() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_addVCF_then_it_Should_Be_in_Disabled();
	}

	@Test(priority = 21)
	public void vcf_SM_23_VCF_SM_addVCF_RemoveVCF_then_it_Should_Be_in_Enable() throws Exception {

		vcf_SM_WF vcfwf = new vcf_SM_WF();
		vcfwf.VCF_SM_addVCF_RemoveVCF_then_it_Should_Be_in_Enable();
	}

	@AfterClass
	public void tearDown() {
		driver.close();
	}
}
