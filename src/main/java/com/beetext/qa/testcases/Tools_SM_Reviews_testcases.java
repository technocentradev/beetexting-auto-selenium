package com.beetext.qa.testcases;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.Workflows.scheduled.Tools_SM_Reviews_WF;
import com.beetext.qa.base.TestBase;

public class Tools_SM_Reviews_testcases extends TestBase {

	@BeforeSuite
	public void setUp() throws Exception {
		initialization();
		Tools_SM_WFCM SMCM = new Tools_SM_WFCM();
		SMCM.Login_Tools_SM();
	}

	@Test(priority = 1)
	public void TC_001_Tools_SM_Reviews_icon() throws Exception

	{

		Tools_SM_Reviews_WF SM_R_WF = new Tools_SM_Reviews_WF();
		SM_R_WF.tc_001_Functionalty_of_Reviews_icon();
	}

	@Test(priority = 2)
	public void TC_002_Tools_SM_Reviews_icon() throws Exception

	{

		Tools_SM_Reviews_WF SM_R_WF = new Tools_SM_Reviews_WF();
		SM_R_WF.tc_002_Verify_all_icons_disable();
	}

	@Test(priority = 3)
	public void TC_003_Tools_SM_Reviews_icon() throws Exception

	{

		Tools_SM_Reviews_WF SM_R_WF = new Tools_SM_Reviews_WF();
		SM_R_WF.tc_003_Verify_all_icons_disable();
	}

	@AfterSuite
	public void tearDown() {
		driver.quit();
	}

}
