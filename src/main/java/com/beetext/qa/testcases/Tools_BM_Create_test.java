package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.Workflows.broadcast.Tools_BM_Create_WF;
import com.beetext.qa.base.TestBase;

public class Tools_BM_Create_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {

		initialization();
		Broadcast BM = new Broadcast();
		BM.BM_tools_login();

	}

	@Test(priority = 1)
	public void tools_271_To_276_Comnbination_Of_Cases() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.Comnbination_Of_Cases();

	}

	@Test(priority = 7)
	public void tools_277_281_282_283_284tools_BM_upcoming_past_draft_enabled_Default_Messages() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_upcoming_past_draft_enabled_Default_Messages();

	}

	@Test(priority = 14)
	public void tools_278_279_280_285_286_to_292_tools_BM_click_on_createBM_Select_Dept_View_Reciepents_relatedCases()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_click_on_createBM_Select_Dept_View_Reciepents_relatedCases();

	}

	@Test(priority = 20)
	public void tools_294_295_296_tools_BM_page_create_Select_2Tags_moreTarget_and_lessTarget() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_create_Select_2Tags_moreTarget_and_lessTarget();

	}

	@Test(priority = 21)
	public void tools_297_299_300_301_302_tools_BM_page_Finding_Errors_for_Title_Message() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Finding_Errors_for_Title_Message();

	}

	@Test(priority = 26)
	public void tools_303_tools_BM_page_add_Attachment() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_add_Attachment();

	}

	@Test(priority = 27)
	public void tools_304_307_tools_BM_page_add_10Attachment() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_add_10Attachment();

	}

	@Test(priority = 28)
	public void tools_308_tools_BM_page_termsandCond_errormsg() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_termsandCond_errormsg();

	}

	@Test(priority = 29)
	public void tools_309_tools_BM_page_fill_allDetails_SendNow() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_fill_allDetails_SendNow();

	}

	@Test(priority = 30)
	public void tools_310_tools_BM_page_fill_allDetails_ScheduleLater() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_fill_allDetails_ScheduleLater();
	}

	@Test(priority = 31)
	public void tools_311_tools_BM_page_DateAndTime_errorMsg() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_DateAndTime_errorMsg();

	}

	@Test(priority = 32)
	public void tools_312_to_317_tools_BM_page_recurring() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_recurring();

	}

	@Test(priority = 38)
	public void tools_318_tools_BM_page_validDetails_BM_toaster() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_validDetails_BM_toaster();

	}

	@Test(priority = 39)
	public void tools_319_tools_BM_page_dont_giveMsg_BM_sent() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_dont_giveMsg_BM_sent();

	}

	@Test(priority = 40)
	public void tools_320_321_tools_BM_page_Click_onSM_PopupOpens_Click_onDraft() throws Exception {
		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onSM_PopupOpens_Click_onDraft();
	}

	@Test(priority = 41)
	public void tools_322_tools_BM_page_Click_onSM_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onSM_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 42)
	public void tools_323_324_tools_BM_page_Click_on_Templates_PopupOpens_Click_onDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_on_Templates_PopupOpens_Click_onDraft();

	}

	@Test(priority = 43)
	public void tools_325_tools_BM_page_Click_onTemplates_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onTemplates_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 44)
	public void tools_326_327_tools_BM_page_Click_on_Tags_PopupOpens_Click_onDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_on_Tags_PopupOpens_Click_onDraft();

	}

	@Test(priority = 45)
	public void tools_328_tools_BM_page_Click_onTags_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onTags_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 46)
	public void tools_329_330_tools_BM_page_Click_on_Automations_PopupOpens_Click_onDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_on_Automations_PopupOpens_Click_onDraft();

	}

	@Test(priority = 47)
	public void tools_331_tools_BM_page_Click_onAutomations_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onAutomations_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 48)
	public void tools_332_333_tools_BM_page_Click_on_Advocate_PopupOpens_Click_onDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_on_Advocate_PopupOpens_Click_onDraft();

	}

	@Test(priority = 49)
	public void tools_334_tools_BM_page_Click_onAdvocate_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onAdvocate_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 50)
	public void tools_335_336_tools_BM_page_Click_on_Reviews_PopupOpens_Click_onDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_on_Reviews_PopupOpens_Click_onDraft();

	}

	@Test(priority = 51)
	public void tools_337_tools_BM_page_Click_onReviews_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onReviews_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 52)
	public void tools_338_339_tools_BM_page_Click_on_BroadCastLink_PopupOpens_Click_onDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_on_BroadCastLink_PopupOpens_Click_onDraft();

	}

	@Test(priority = 53)
	public void tools_340_tools_BM_page_Click_onBroadCastLink_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Click_onBroadCastLink_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 54)
	public void tools_341_342_tools_BM_page_dontFillDetails_Click_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_dontFillDetails_Click_on_outside_PopupOpens_Click_onDraft();

	}

	@Test(priority = 55)
	public void tools_343_tools_BM_page_dont_fillDetailsClick_Outside_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_dont_fillDetailsClick_Outside_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 56)
	public void tools_344_345_tools_BM_page_Fill_AllDetails_Click_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Fill_AllDetails_Click_on_outside_PopupOpens_Click_onDraft();

	}

	@Test(priority = 57)
	public void tools_346_tools_BM_page_Fill_AllDetails_Click_Outside_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Fill_AllDetails_Click_Outside_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 58)
	public void tools_347_tools_BM_page_PastTime_errorMsg() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_PastTime_errorMsg();

	}

	@Test(priority = 59)
	public void tools_348_349_tools_BM_page_Fill_AllDetails_dontAgreeTermsCondClick_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Fill_AllDetails_dontAgreeTermsCondClick_on_outside_PopupOpens_Click_onDraft();

	}

	@Test(priority = 60)
	public void tools_350_tools_BM_page_Fill_AllDetails_dontAgreeTermsCondClick_Click_Outside_PopupOpens_Click_onDiscard()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF
				.tools_BM_page_Fill_AllDetails_dontAgreeTermsCondClick_Click_Outside_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 61)
	public void tools_351_tools_BM_page_Fill_AllDetails_SaveAsDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Fill_AllDetails_SaveAsDraft();

	}

	@Test(priority = 62)
	public void tools_352_tools_BM_page_add_only_Attachment_SaveAsDraft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_add_only_Attachment_SaveAsDraft();

	}

	@Test(priority = 63)
	public void tools_353_354_tools_BM_page_AddTag_attachmentClick_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_AddTag_attachmentClick_on_outside_PopupOpens_Click_onDraft();

	}

	@Test(priority = 64)
	public void tools_355_tools_BM_page_AddTag_Attachment_Click_Outside_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_AddTag_Attachment_Click_Outside_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 65)
	public void tools_356_357_tools_BM_page_viewRecipients_Click_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_viewRecipients_Click_on_outside_PopupOpens_Click_onDraft();

	}

	@Test(priority = 66)
	public void tools_358_tools_BM_page_viewRecipients_Click_Outside_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_viewRecipients_Click_Outside_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 67)
	public void tools_359_360_tools_BM_page_FillAlDetails_selectRecurring_Click_on_outside_PopupOpens_Click_onDraft()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_FillAlDetails_selectRecurring_Click_on_outside_PopupOpens_Click_onDraft();

	}

	@Test(priority = 68)
	public void tools_361_tools_BM_page_FillAlDetails_selectRecurring_Click_Click_Outside_PopupOpens_Click_onDiscard()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_FillAlDetails_selectRecurring_Click_Click_Outside_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 69)
	public void tools_362_363_tools_BM_page_selectRecurring_Selectweekday_BM_shown_in_past() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_selectRecurring_Selectweekday_BM_shown_in_past();

	}

	@Test(priority = 70)
	public void tools_364_tools_BM_page_upcoming_listDetails() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_upcoming_listDetails();

	}

	@Test(priority = 71)
	public void tools_366_367_tools_BM_page_selectRecurring_Select_weeklyOnSelectedDate_BM_shown_in_past()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_selectRecurring_Select_weeklyOnSelectedDate_BM_shown_in_past();

	}

	@Test(priority = 72)
	public void tools_368_tools_BM_page_weekly_upcoming_listDetails() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_weekly_upcoming_listDetails();

	}

	@Test(priority = 73)
	public void tools_369_370_tools_BM_page_selectRecurring_Select_MonthlyOnSelectedDate_BM_shown_in_past()
			throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_selectRecurring_Select_MonthlyOnSelectedDate_BM_shown_in_past();

	}

	@Test(priority = 74)
	public void tools_371_tools_BM_page_monthly_upcoming_listDetails() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_monthly_upcoming_listDetails();

	}

	@Test(priority = 75)
	public void tools_372_373_tools_BM_page_selectRecurring_Select_yearlyOnSelectedDate_BM_shown_in_past()
			throws Exception {
		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_selectRecurring_Select_yearlyOnSelectedDate_BM_shown_in_past();

	}

	@Test(priority = 76)
	public void tools_374_tools_BM_page_yearly_upcoming_listDetails() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_yearly_upcoming_listDetails();

	}

	@Test(priority = 77)
	public void tools_375_tools_BM_page_Save_as_draft() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_Save_as_draft();

	}

	@Test(priority = 78)
	public void tools_376_tools_BM_page_create_pastTime_errormsg() throws Exception {

		Tools_BM_Create_WF Tools_BM_Create_WF = new Tools_BM_Create_WF();
		Tools_BM_Create_WF.tools_BM_page_create_pastTime_errormsg();

	}

	@AfterClass
	public void tearDown() {
		driver.close();
	}

}