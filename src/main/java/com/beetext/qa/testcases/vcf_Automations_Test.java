package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.Workflows.vcf.vcf_AutomationsWF;
import com.beetext.qa.base.TestBase;

public class vcf_Automations_Test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		VCF_WFCM vcfwf = new VCF_WFCM();
		vcfwf.VCard_Auto1_Login_tools_Button();
	}

	@Test(priority = 1)
	public void vcf_Automations_01_Vcf_Available_in_Automations() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Vcf_Available_in_Automations();
	}

	@Test(priority = 2)
	public void vcf_Automations_03_04_VCF_Automations_Admin_Able_to_Enable_Disable() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VCF_Automations_Admin_Able_to_Enable_Disable();
	}

	@Test(priority = 3)
	public void vcf_Automations_05_VcfAutomations_Message_and_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_Message_and_VCF();
	}

	@Test(priority = 4)
	public void vcf_Automations_06_VcfAutomations_Message_Emoji_and_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_Message_Emoji_and_VCF();
	}

	@Test(priority = 5)
	public void vcf_Automations_07_VcfAutomations_Message_Attachment_Emoji_and_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_Message_Attachment_Emoji_and_VCF();
	}

	@Test(priority = 6)
	public void vcf_Automations_08_VcfAutomations_1000Message_and_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_1000Message_and_VCF();
	}

	@Test(priority = 7)
	public void vcf_Automations_09_VcfAutomations_1000Message_Emoji_and_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_1000Message_Emoji_and_VCF();
	}

	@Test(priority = 8)
	public void vcf_Automations_10_VcfAutomations_9Attachments_and_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_9Attachments_and_VCF();
	}

	@Test(priority = 9)
	public void vcf_Automations_11_VcfAutomations_10Attachments_VCF_Shows_Error() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_10Attachments_VCF_Shows_Error();
	}

	@Test(priority = 10)
	public void vcf_Automations_12_Select_VCF_then_VCF_Disable() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Select_VCF_then_VCF_Disable();
	}

	@Test(priority = 11)
	public void vcf_Automations_13_Select_Attachment_VCF_then_VCF_Disable() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Select_Attachment_VCF_then_VCF_Disable();
	}

	@Test(priority = 12)
	public void vcf_Automations_14_VCF_Can_Addin_Multiple_Actions() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VCF_Can_Addin_Multiple_Actions();
	}

	@Test(priority = 13)
	public void vcf_Automations_15_VcfAutomations_9Attachments_and2ndAction_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_9Attachments_and2ndAction_VCF();
	}

	@Test(priority = 14)
	public void vcf_Automations_16_VcfAutomations_9Attachments_and2ndAction_9Attachments_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_9Attachments_and2ndAction_9Attachments_VCF();
	}

	@Test(priority = 15)
	public void vcf_Automations_17_VcfAutomations_Message_9Attachments_and2ndAction_Message_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_Message_9Attachments_and2ndAction_VCF();
	}

	@Test(priority = 16)
	public void vcf_Automations_18_VcfAutomations_Message_9Attachments_and2ndAction_Message_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_Message_9Attachments_and2ndAction_Message_VCF();
	}

	@Test(priority = 17)
	public void vcf_Automations_19_VcfAutomations_Emoji_Message_9Attachments_and2ndAction_Message_VCF()
			throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_Emoji_Message_9Attachments_and2ndAction_Message_VCF();
	}

	@Test(priority = 18)
	public void vcf_Automations_20_VcfAutomations_1000CharMessage_9Attachments_and2ndAction_1000CharMessage_VCF()
			throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_1000CharMessage_9Attachments_and2ndAction_1000CharMessage_VCF();
	}

	@Test(priority = 19)
	public void vcf_Automations_21_VcfAutomations_1000CharMessage_Emoji_9Attachments_and2ndAction_1000CharMessage_VCF()
			throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.VcfAutomations_1000CharMessage_Emoji_9Attachments_and2ndAction_1000CharMessage_VCF();
	}

	@Test(priority = 20)
	public void vcf_Automations_22In_2nd_Action_Select_VCF_then_Disabled() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.In_2nd_Action_Select_VCF_then_Disabled();
	}

	@Test(priority = 21)
	public void vcf_Automations_23Automaations_Manage_removeVCF_AddVCF_AddAction_9Attachments_VCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Automaations_Manage_removeVCF_AddVCF_AddAction_9Attachments_VCF();
	}

	@Test(priority = 22)
	public void vcf_Automations_24Automations_Manage_removeVCF_addVCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Automations_Manage_removeVCF_addVCF();
	}

	@Test(priority = 23)
	public void vcf_Automations_25Automations_Manage_removeVCF_andAttachment_addVCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Automations_Manage_removeVCF_andAttachment_addVCF();
	}

	@Test(priority = 24)
	public void vcf_Automations_26Automations_Manage_removeVCF_Attachmnet_andMsg_addVCF() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Automations_Manage_removeVCF_Attachmnet_andMsg_addVCF();
	}

	@Test(priority = 25)
	public void vcf_Automations_27Automations_Manage_removeVCF_Attachmnet_andMsg_addVCF_WithAttachment()
			throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Automations_Manage_removeVCF_Attachmnet_andMsg_addVCF_WithAttachment();
	}

	@Test(priority = 26)
	public void vcf_Automations_28Automations_Manage_Message_Emoji_and_VCF_deleteAutomations() throws Exception {

		vcf_AutomationsWF vcfwf = new vcf_AutomationsWF();
		vcfwf.Automations_Manage_Message_Emoji_and_VCF_deleteAutomations();
	}

	@AfterClass
	public void tearDown() {
		driver.close();
	}
}
