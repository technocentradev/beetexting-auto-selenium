package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.ExtentReportListener.VideoRecorder;
import com.beetext.qa.Workflows.contacts.importCSVWF;
import com.beetext.qa.base.TestBase;

public class importCSVContacts extends TestBase {

	// @BeforeMethod
	@BeforeClass
	public void setUp() throws Exception {
		VideoRecorder.startRecord("ImportCSVTestRecording");
		initialization();
		Login();
	}



	@Test(priority = 1)
	public void Contacts_importValidCSV() throws Exception {

		importCSVWF importcsv = new importCSVWF();
		importcsv.importValidCSV();

	}

	@Test(priority = 2)
	public void Contacts_import_Empty_CSV() throws Exception {

		importCSVWF importcsv = new importCSVWF();
		importcsv.import_empty_CSV();
	}

	@Test(priority = 3)
	public void Contacts_import_invalid_CSV_and_valid_CSV() throws Exception {

		importCSVWF importcsv = new importCSVWF();
		importcsv.import_inValidCSV_validCSV();
	}

	@Test(priority = 4)
	public void Download_sample_CSV() throws Exception {

		importCSVWF importcsv = new importCSVWF();
		importcsv.DownloadsampleCSV();
	}

	@Test(priority = 5)
	public void Contacts_importinValidCSV() throws Exception {

		importCSVWF importcsv = new importCSVWF();
		importcsv.importinValidCSV();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		//VCard_Signout();
	}

}
