package com.beetext.qa.testcases;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.PinMessagesWF;
import com.beetext.qa.base.TestBase;

public class Pin_Message extends TestBase {

	@BeforeClass
	public void setUp() {
		initialization();
		Login(); //for login

	}

	@Test(priority = 1)
	public void message_205_pinmessage() throws Exception {
		PinMessagesWF PinMessagesWF = new PinMessagesWF();
		PinMessagesWF.Pinmessage();

	}

	@Test(priority = 2)
	public void message_209_pinmessage_agent_Name_select() throws Exception {
		PinMessagesWF PinMessagesWF = new PinMessagesWF();
		PinMessagesWF.Pinmessage_agentselection();

	}

	@Test(priority = 6)
	public void message_207_pinmessage_no_Agent_Found() throws Exception {

		PinMessagesWF PinMessagesWF = new PinMessagesWF();
		PinMessagesWF.Pinmessage_no_Agent_Found();

	}

	@Test(priority = 3)
	public void message_212_pinmessage_with_hyperlink() throws Exception {

		PinMessagesWF PinMessagesWF = new PinMessagesWF();
		PinMessagesWF.Pinmessage_with_hyperLink();

	}

	@Test(priority = 4)
	public void message_213_pinmessage_with_agentName() throws Exception {

		PinMessagesWF PinMessagesWF = new PinMessagesWF();
		PinMessagesWF.Pinmessage_with_agentName();

	}

	@Test(priority = 5)
	public void message_215_pinmessage_with2agentsName() throws Exception {
		PinMessagesWF PinMessagesWF = new PinMessagesWF();
		PinMessagesWF.Pinmessage_select_morethanoneagentname();

	}

	

 @AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		//VCard_Signout();
	}
}
