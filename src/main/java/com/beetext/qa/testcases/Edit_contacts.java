package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.contacts.Edit_contactWF;
import com.beetext.qa.base.TestBase;

public class Edit_contacts extends TestBase {

	// @BeforeMethod
	@BeforeClass
	public void setUp() {
		initialization();
		Login();
	}

	@Test(priority = 1)
	public void editContact__042Name_With_single_char() throws Exception {

		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_Name_With_singe_Char();
	}

	@Test(priority = 2)
	public void editContact__040Name_With_special_char() throws Exception {

		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_Name_With_specialChar();
	}

	@Test(priority = 3)
	public void editContact__043companyName_allow_special_char() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_Companyname_allow_special_char();
	}

	@Test(priority = 4)
	public void editContact__046_valid_avathar_logo() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_vaid_avathar_logo();
	}

	@Test(priority = 5)
	public void editContact__050_invlaid_avathar_logo() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_invaid_avathar_logo();
	}

	@Test(priority = 6)
	public void editContact__056_toastermessage_validation() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_toastermessagevalidation();
	}

	@Test(priority = 7)
	public void editContact__054_invalid_email() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_enter_invalid_email();
	}

	@Test(priority = 9)
	public void editContact__058_add_tag_with_special_char() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_tag_allow_special_Char();
	}

	@Test(priority = 10)
	public void editContact__060_tag_deletepopup_cancel() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_close_delete_tag_popup();
	}

	@Test(priority = 11)
	public void editContact__059_tag_delete() throws Exception {

		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_delete_tag();
	}

	@Test(priority = 8)
	public void editContact__61_edit_50tagcontact_delete_onetag_addnewtag() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_select50tags_contact_deleteone_tag();
	}

	@Test(priority = 12)
	public void editContact__063_tag_delete() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_tag_allow_invalid_special_Char();
	}

	@Test(priority = 13)
	public void editContact__065_tag_duplicate_validation() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_tag_duplicate_tag_validation();
	}

	@Test(priority = 16)
	public void editContact__066_morethan_50tags() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_morethan_50_tag_validating();
	}

	@Test(priority = 14)
	public void editContact__080_tag_duplicate_validation_upper_lower_char() throws Exception {
		Edit_contactWF editContactWorkFlow = new Edit_contactWF();
		editContactWorkFlow.editContact_tag_duplicate_tag_validation_lower_upper_char();
	}

	@Test(priority = 15)
	public void editContact__078_tag_duplicate_validation_upper_char() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact_tag_duplicate_tag_validation_upper_char();
	}

	@Test(priority = 17)
	public void editContact__079_tag_captital_letter() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact__tag_uppercase_letters();
	}

	@Test(priority = 18)
	public void editContact__083_add_note() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact__add_note();
	}

	@Test(priority = 19)
	public void editContact__084_edit_note() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact__edit_note();
	}

	@Test(priority = 20)
	public void editContact__092_edit_dont_update_note_text() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact__edit_not_update_note_text();
	}

	@Test(priority = 21)
	public void editContact__091_edit_note_clickon_crossmark() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact__edit_note_click_cancel_button();
	}

	@Test(priority = 22)
	public void editContact__101_edit_delete_note() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact__delete_Note();
	}

	@Test(priority = 23)
	public void editContact__102_edit_cancel_note() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editContact__cancel_Note();
	}

	@Test(priority = 24)
	public void editContact__051_name_remove_space() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editcontatName_givespaceatend();
	}

	@Test(priority = 25)
	public void editContact__051_1_companyname_remove_space() throws Exception {

		Edit_contactWF createDept = new Edit_contactWF();
		createDept.editcontatCompanyName_givespaceatend();
	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		//VCard_Signout();
	}
}
