package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.ConversationWF_Attachments;
import com.beetext.qa.base.TestBase;

public class ConversationPageTest_Attachments extends TestBase {
	Map<String, String> usercredentials = new HashMap<String, String>();

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() {
		initialization();
		String username1 = prop.getProperty("username1");
		String username2 = prop.getProperty("username2");
		String password = prop.getProperty("password");
		usercredentials.put("username1", username1);
		usercredentials.put("username2", username2);
		usercredentials.put("password", password);
	}

	@Test(priority = 83)
	public void tc_83_134_conversations_text_message() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.conversations_text_message(usercredentials);

	}

	@Test(priority = 84)
	public void tc_84_conversations_1000char_max_text_message() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.conversations_1000char_max_text_message(usercredentials);

	}

	@Test(priority = 85)
	public void tc_85_137_conversations_textmessage_image() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.conversations_textmessage_image(usercredentials);

	}

	@Test(priority = 86)
	public void tc_86_138_conversations_textmessage_audio() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.conversations_textmessage_audio(usercredentials);

	}

	@Test(priority = 87)
	public void tc_87_139_conversations_textmessage_video() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.conversations_textmessage_video(usercredentials);

	}

	@Test(priority = 88)
	public void tc_88_jpg1mb_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.jpg1mb_file(usercredentials);

	}

	@Test(priority = 89)
	public void tc_89_text_jpg2500kb_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.text_jpg2500kb_file(usercredentials);

	}

	@Test(priority = 90)
	public void tc_90_attachments_10files() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.attachments_10files(usercredentials);

	}

	@Test(priority = 91)
	public void tc_91_attachments_morethan_10files_popuperror() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.attachments_morethan_10files_popuperror(usercredentials);

	}

	@Test(priority = 92)
	public void tc_92_148_conversations_msg_morethan_2mbfile() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.conversations_msg_morethan_2mbfile(usercredentials);

	}

	@Test(priority = 93)
	public void tc_93_149_message_gif() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.message_gif(usercredentials);

	}

	@Test(priority = 97)
	public void tc_97_Signature_count() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.Signature_count(usercredentials);

	}

	@Test(priority = 102)
	public void tc_102_fileinprogress_sendbttndisable() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.fileinprogress_sendbttndisable(usercredentials);

	}

	@Test(priority = 103)
	public void tc_103_fileinprogress_sendbttnenable() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.fileinprogress_sendbttnenable(usercredentials);

	}

	@Test(priority = 106)
	public void tc_106_textmessage_emoji() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_emoji(usercredentials);

	}

	@Test(priority = 107)
	public void tc_107_textmessage_emoji_image() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_emoji_image(usercredentials);

	}

	@Test(priority = 108)
	public void tc_108_textmessage_emoji_image_video() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_emoji_image_video(usercredentials);

	}

	@Test(priority = 109)
	public void tc_109_140_emoji() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.emoji(usercredentials);

	}

	@Test(priority = 110)
	public void tc_110_textmessage_emoji_image_pdf() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_emoji_image_pdf(usercredentials);

	}

	@Test(priority = 111)
	public void tc_111_textmessage_hyperlink() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_hyperlink(usercredentials);

	}

	@Test(priority = 112)
	public void tc_112_textmessage_gif() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_gif(usercredentials);

	}

	@Test(priority = 113)
	public void tc_113_textmessage_morethan_onegif() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_morethan_onegif(usercredentials);

	}

	@Test(priority = 114)
	public void tc_114_textmessage_searchgif() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_searchgif(usercredentials);

	}

	@Test(priority = 115)
	public void tc_115_textmessage_search_dirtyword_gif() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textmessage_search_dirtyword_gif(usercredentials);

	}

	@Test(priority = 116)
	public void Tc_116_144_mp3_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.mp3_file(usercredentials);

	}

	@Test(priority = 117)
	public void Tc_117_136_141_mp4_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.mp4_file(usercredentials);

	}

	@Test(priority = 118)
	public void Tc_118_145_mov_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.mov_file(usercredentials);

	}

	@Test(priority = 119)
	public void Tc_119_147_webm_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.webm_file(usercredentials);

	}

	@Test(priority = 120)
	public void Tc_120_142_143pdf_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.pdf_file(usercredentials);

	}

	@Test(priority = 121)
	public void Tc_121_worddocument_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.worddocument_file(usercredentials);

	}

	@Test(priority = 122)
	public void Tc_122_excelsheet_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.excelsheet_file(usercredentials);

	}

	@Test(priority = 123)
	public void Tc_123_146_textfileverification() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.textfileverification(usercredentials);

	}

	@Test(priority = 124)
	public void Tc_124_png_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.png_file(usercredentials);

	}

	@Test(priority = 125)
	public void Tc_125_135_jpeg_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.jpeg_file(usercredentials);

	}

	@Test(priority = 126)
	public void Tc_126_aif_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.aif_file(usercredentials);

	}

	@Test(priority = 127)
	public void Tc_127_avi_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.avi_file(usercredentials);

	}

	@Test(priority = 128)
	public void Tc_128_wmv_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.wmv_file(usercredentials);

	}

	@Test(priority = 129)
	public void Tc_129_mpg_file() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.mpg_file(usercredentials);

	}

	@Test(priority = 130)
	public void Tc_130_file_vob() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.file_vob(usercredentials);

	}

	@Test(priority = 131)
	public void Tc_131_file_mts() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.file_mts(usercredentials);

	}

	@Test(priority = 132)
	public void Tc_132_file_mpeg() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.file_mpeg(usercredentials);

	}

	@Test(priority = 133)
	public void Tc_133_file_3gp() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.file_3gp(usercredentials);

	}

	@Test(priority = 150)
	public void Tc_150_blocked_contact() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.blocked_contact(usercredentials);

	}

	@Test(priority = 151)
	public void Tc_151_unblockcontact_messagewillappear() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.unblockcontact_messagewillappear(usercredentials);

	}

	@Test(priority = 152)
	public void Tc_152_receivingside_downloadbutton_enable() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.receivingside_downloadbutton_enable(usercredentials);

	}

	@Test(priority = 153)
	public void Tc_153_pinmessage_forinternalorg() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.pinmessage_forinternalorg(usercredentials);

	}

	@Test(priority = 154)
	public void Tc_154_receivingside_downloadfile() throws Exception {

		ConversationWF_Attachments conversationwf = new ConversationWF_Attachments();
		conversationwf.receivingside_downloadfile(usercredentials);

	}

	@AfterMethod
	public void validate_Signout() throws Exception {
		VCard_Signout();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
