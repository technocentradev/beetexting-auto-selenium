package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.settings.SettingsCreateDepartmentWF;
import com.beetext.qa.base.TestBase;

public class SettingsCreateDepartments extends TestBase {

	@BeforeClass
	public void setUp() {
		initialization();
		Login(); // for login
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
	}

	@Test(priority = 1)
	public void Settings_037_vaid_otp() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.valid_Verification_Code();

	}

	@Test(priority = 2)
	public void Settings_054_edit_deptname_3charvalidation() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.edit_departmentname_min3char_validation();
	}

	@Test(priority = 3)
	public void Settings_06263_edit_deptname() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.edit_departmentname();
	}

	@Test(priority = 4)
	public void Settings_064_edit_dept_invite_agent() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.edit_departmentpage_inviteagent();
	}

	@Test(priority = 5)
	public void Settings_067_68_edit_dept_remove_agent_add_same_agent() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.edit_departmentpage_remove_agent_add_agian_same_agent();
	}

	@Test(priority = 6)
	public void Settings_050_createDepartment_required_validation() throws Exception {

		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.DeptName_required_validation();
	}

	@Test(priority = 7)
	public void Settings_049_createDepartment_required_3char_validation() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.DeptName_required_3char_validation();
	}

	@Test(priority = 8)
	public void Settings_052_createDepartment_areadcode_required_validation() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.areaCode_required_validation();
	}

	@Test(priority = 9)
	public void Settings_053_createDepartment_areacode_not_valid() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.invalidareaCode_required_validation();
	}

	@Test(priority = 10)
	public void Settings_051_createDepartment_Tollfree_created_succssfully() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.createTollfreeDept();
	}

	@Test(priority = 11)
	public void Settings_048_createDept_Enabled() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.CreateBtn_Enabled();
	}

	@Test(priority = 12)
	public void Settings_014_createDept_Enabled() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.TextEnabledExistingDepartment_CreateBtn_Enabled();
	}

	@Test(priority = 13)
	public void Settings_015_ValidDepartmentName_Next_Enabled() throws Exception {

		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.TextEnabledExistingDepartment_Entervalid_department_Name();
	}

	@Test(priority = 14)
	public void Settings_017_ValidDepartmentName_Required_validation() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.TextEnabledExistingDepartment_Required_validation();
	}

	@Test(priority = 15)
	public void Settings_018_ValidDepartmentName_3char_validation() throws Exception {

		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.TextEnabledExistingDepartment_3charRequired_validation();
	}

	@Test(priority = 16)
	public void Settings_026_phonenumberinfo_required_validation() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.phoneNumberinfo_required_validation();
	}

	@Test(priority = 17)
	public void Settings_027_phonenumberinfo_custname_3char_validation() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.phoneNumberinfo_customername_3charRequired_validation();
	}

	@Test(priority = 18)
	public void Settings_029_phonenumberinfo_zipcode_allow_only_numbers_validation() throws Exception {

		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.phoneNumberinfo_zipcode_allowonly_numbers_validation();
	}

	@Test(priority = 19)
	public void Settings_030_031_phonenumberinfo_checkanduncheck_radio_button_validation() throws Exception {

		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.phoneNumberinfo_checkanduncheck_radiobutton_validation();
	}

	@Test(priority = 20)
	public void Settings_035_phonenumberinfo_nextbutton_enabled() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.phoneNumberinfo_valid_data_nextbutton_enabled();
	}

	@Test(priority = 21)
	public void Settings_036_clickon_callme_button_verifying_otppage_open_or_not() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.phoneNumberinfo_valid_data_clickon_callme_button();
	}

	@Test(priority = 22)
	public void Settings_038_invaid_otp() throws Exception {

		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.invalid_Verification_Code();
	}

	@Test(priority = 23)
	public void Settings_040_callme_Again() throws Exception {

		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.callME_Again();
	}

	@Test(priority = 24)
	public void Settings_041_clickon_back() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.clickon_back_button();
	}

	@Test(priority = 25)
	public void Settings_043_departmentnumber_Already_Exist() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.Alreadyexisting_department_number();
	}

	@Test(priority = 26)
	public void Settings_054_clickon_back_button() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.tollfree_clickon_backbutton();
	}

	@Test(priority = 27)
	public void Settings_056_57_verifystatus_department_clickon_callmeoption_enter_valid_otp() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.verifystatus_department_clickoncallmebutton_entervalid_verification_Code();
	}

	@Test(priority = 28)
	public void Settings_059_verifystatus_department_clickon_callmeoption_enter_valid_otp() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.verifystatus_department_clickoncallmebutton_entervalid_verification_Code();
	}

	@Test(priority = 29)
	public void Settings_071_72_open_ManageAgent_clickon_cancel_button() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.edit_departmentpage_Manage_agent_click_on_cancel_button();
	}

	@Test(priority = 30)
	public void Settings_023_24_Phonenumberinfo_phonenumber_textbox_mustbe_10digits_validation_checking()
			throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.phoneNumberinfo_phonenumber_mustbe_10digits_validation_checking();
	}

	@Test(priority = 31)
	public void Settings_076_delete_department() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.Delete_Department();
	}

	@Test(priority = 32)
	public void Settings_074_delete_department_department_Next_Button_Enabled1() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.Delete_Department_NextButton1_Enabled();
	}

	@Test(priority = 33)
	public void Settings_075_delete_department_Next_Button_Enabled2() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.Delete_Department_NextButton2_Enabled();
	}

	@Test(priority = 34)
	public void Settings_069_update_TollFreedeptName() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.createTollfreeDeptName_update_department_name();
	}

	@Test(priority = 35)
	public void Settings_046_055_058_tollfree_deptName_min_3_char_validation_createtollfree_deletetollfree()
			throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.createTollfreeDeptName_min_3_char_validation();
	}
	
	@Test(priority=36)
	public void  deleteSample_department() throws Exception {
		SettingsCreateDepartmentWF createDept = new SettingsCreateDepartmentWF();
		createDept.deleteSampleDepartment();
		
	}

	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
