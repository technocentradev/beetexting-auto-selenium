package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Reviews_Workflow_CM;
import com.beetext.qa.Workflows.Review_Page_WF;
import com.beetext.qa.base.TestBase;

public class Review_testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Reviews_Workflow_CM RCM = new Reviews_Workflow_CM();
		RCM.login2();
	}

	@Test(priority = 1)
	public void TC_215_Reviews_Tab_IsEnable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.tc_215_Review_Tab_IsEnable_215();

	}

	@Test(priority = 2)
	public void TC_216_Reviews_Text_message_createYourFirstReview() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Review_NoRecords_CreateYourFirstName_Text_Message_216();

	}

	@Test(priority = 9) // 12
	public void TC_217_Reviews_Add_Review_Site_Button_IsEnabled() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Review_AddReviewSite_button_IsEnabled_217();

	}

	@Test(priority = 3) // 4
	public void TC_218_Create_Review_Page_navigation() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.tc_218_Reviewes_Create_Review_Page_Navigation_218();

	}

	@Test(priority = 4) // 5
	public void TC_219_Create_Review_Page_AllFields_available() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.tc_219_Reviewes_Create_Review_Page_All_Fields_isEnabled_219();

	}

	@Test(priority = 5) // 6
	public void TC_220_Verify_all_options_in_SelectSite_DDList_IsAvailable() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.verify_All_options_in_SelectSite_DD_220();

	}

	@Test(priority = 6) // 7
	public void TC_221_Create_Review_Page_AddSite_available() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.tc_221_Create_Review_AddSite_IsDisplay();

	}

	@Test(priority = 10) // 13
	public void TC_224_Verify_Duplicate_Message_For_AddSiteReview_record() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Create_Review_page_AddSite_Duplicate_text_message();

	}

	@Test(priority = 7) // 8
	public void TC_225_Link_text_field_Allows_only_5000_char() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.create_review_Link_Textbox_allows_5000_char_225();

	}

	@Test(priority = 8) // 9
	public void TC_227_Create_Review_Create_button_isEnabled() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Create_Review_Create_Button_IsEnabled();

	}

	@Test(priority = 14) // 18
	public void TC_228_Functionality_of_link_in_create_review_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.functionality_of_link_in_create_reviews_page();

	}

	@Test(priority = 15) // 27
	public void TC_230_create_review_page_to_List_of_Review_page_navigation() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Create_Review_navigate_to_Reviewlist_230();

	}

	@Test(priority = 12) // 16
	public void TC_231_Verify_toaster_message_for_created_review_record() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_Toaster_message_for_Created_review_record_231();

	}

	@Test(priority = 13) // 17
	public void TC_232_created_review_display_in_review_List_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.create_review_display_in_list();

	}

	@Test(priority = 11) // 14
	public void TC_233_create_review_default_turn_on_toggle_button_under_list_is_enabled() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.created_review_in_list_is_defaultly_turnOn_verification();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
