package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.Workflows.broadcast.Tools_BM_Gif_Emoji_WF;
import com.beetext.qa.base.TestBase;

public class Tools_BM_Gif_Emoji_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {

		initialization();
		Broadcast BM = new Broadcast();
		BM.BM_tools_login();
	}

	@Test(priority = 1)
	public void tools_BM_Gif_Emoji_001_to_007_tools_BM_Gif() throws Exception {

		// Combination of 7 Testcases, Covered in 1 Script
		Tools_BM_Gif_Emoji_WF BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		BM_Gif_Emoji_WF.Multiple_GifCases();

	}

	@Test(priority = 2)
	public void tools_BM_Gif_Emoji_008_tools_BM_Gif_SlectGif_SendBM() throws Exception {

		Tools_BM_Gif_Emoji_WF BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		BM_Gif_Emoji_WF.tools_BM_Gif_SlectGif_SendBM();

	}

	@Test(priority = 3)
	public void tools_BM_Gif_Emoji_009_tools_BM_Gif_SlectGif_ScheduleBM() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Gif_SlectGif_ScheduleBM();

	}

	@Test(priority = 4)
	public void tools_BM_Gif_Emoji_010_tools_BM_Gif_SlectGif_BM_SaveAsDraft() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Gif_SlectGif_BM_SaveAsDraft();

	}

	@Test(priority = 5)
	public void tools_BM_Gif_Emoji_011_tools_BM_Gif_SelectGif_upcoming_ChangeTitle_add_Gif() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Gif_SelectGif_upcoming_ChangeTitle_add_Gif();

	}

	@Test(priority = 6)
	public void tools_BM_Gif_Emoji_012_tools_BM_Gif_SelectGif_Past_Reuse_ChangeTitle_add_Gif() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Gif_SelectGif_Past_Reuse_ChangeTitle_add_Gif();

	}

	@Test(priority = 7)
	public void tools_BM_Gif_Emoji_013_tools_BM_Gif_SelectGif_Draft_ChangeTitle_add_Gif() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Gif_SelectGif_Draft_ChangeTitle_add_Gif();

	}

	@Test(priority = 8)
	public void tools_BM_Gif_Emoji_014_tools_BM_Emoji_SelectEmoji_SendBM() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Emoji_SelectEmoji_SendBM();

	}

	@Test(priority = 9)
	public void tools_BM_Gif_Emoji_015_tools_BM_Emoji_SelectEmoji_BM_SaveDraft() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Emoji_SelectEmoji_BM_SaveDraft();

	}

	@Test(priority = 10)
	public void tools_BM_Gif_Emoji_016_tools_BM_Emoji_SelectEmoji_ScheduleBM() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Emoji_SelectEmoji_ScheduleBM();

	}

	@Test(priority = 11)
	public void tools_BM_Gif_Emoji_017_tools_BM_Emoji_upcoming_SelectEmoji_ScheduleBM() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Emoji_upcoming_SelectEmoji_ScheduleBM();

	}

	@Test(priority = 12)
	public void tools_BM_Gif_Emoji_018_tools_BM_Emoji_SelectEmoji_Past_Reuse_ChangeTitle_add_emoji() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_Emoji_SelectEmoji_Past_Reuse_ChangeTitle_add_emoji();

	}

	@Test(priority = 13)
	public void tools_BM_Gif_Emoji_019_tools_BM_emoji_SelectEmoji_Draft_ChangeTitle_add_emoji() throws Exception {

		Tools_BM_Gif_Emoji_WF tools_BM_Gif_Emoji_WF = new Tools_BM_Gif_Emoji_WF();
		tools_BM_Gif_Emoji_WF.tools_BM_emoji_SelectEmoji_Draft_ChangeTitle_add_emoji();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
