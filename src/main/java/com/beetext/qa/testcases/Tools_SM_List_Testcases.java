package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.Workflows.scheduled.Tools_SM_WF;
import com.beetext.qa.base.TestBase;

public class Tools_SM_List_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {

		initialization();

		Tools_SM_WFCM SMCM = new Tools_SM_WFCM();
		SMCM.Login2_Tools_SM();

	}

	// SM List Page Test cases Started

	@Test(priority = 3)

	public void TC_117_Tools_SM_List_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_117_Verify_all_related_department_records_under_list();
	}

	@Test(priority = 1)
	public void TC_123_Tools_SM_List_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_123_Verifying_text_message_for_no_records();
	}

	@Test(priority = 2)
	public void TC_124_Tools_SM_List_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_124_Verifying_text_message_for_no_draft_records();
	}

	@Test(priority = 4)
	public void TC_125_Tools_SM_List_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_125_Verifying_record_under_Upcoming_list();
	}

	@Test(priority = 5)
	public void TC_126_Tools_SM_List_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_126_Verifying_recurring_icon_under_upcoming_list();
	}

	@Test(priority = 6)
	public void TC_127_Tools_SM_List_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_127_Verifying_navigation_to_manage_page();
	}

	@Test(priority = 7)
	public void TC_133_Tools_SM_List_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_133_Verifying_records_under_Drafts_list();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
