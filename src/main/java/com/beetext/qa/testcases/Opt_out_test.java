package com.beetext.qa.testcases;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.Opt_Out_WF;
import com.beetext.qa.base.TestBase;

public class Opt_out_test extends TestBase {
	
	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() {
		initialization();
		/*String username1 = prop.getProperty("username1");
		String username2 = prop.getProperty("username2");
		String password = prop.getProperty("password");
		usercredentials.put("username1", username1);
		usercredentials.put("username2", username2);
		usercredentials.put("password", password);*/
	}

  	@Test(priority = 1)
  	public void Features_174_180_opt_out_stop() throws Exception {
  	
  		Opt_Out_WF OptOutWF = new Opt_Out_WF();
  		OptOutWF.opt_out_stop();
  
  	}
  
  	@Test(priority = 2)
  	public void Features_175_181_opt_out_start() throws Exception {
  		
  		Opt_Out_WF OptOutWF = new Opt_Out_WF();
  		OptOutWF.opt_out_start();
  
  	}
  
  	@Test(priority = 3)
  	public void Features_176_177_stop_Start_Msg_Block_unblock() throws Exception {
  		
  		Opt_Out_WF OptOutWF = new Opt_Out_WF();
  		OptOutWF.stop_Start_Msg_Block_unblock();
  
  	}
  
   	@Test(priority = 4)
   	public void Features_178_179_COntact_BLOCK_UNBLOCK_user_recived_Msg() throws Exception {
   		
   		Opt_Out_WF OptOutWF = new Opt_Out_WF();
   		OptOutWF.COntact_BLOCK_UNBLOCK_user_recived_Msg();
   
   	}
   
   
   
 	@Test(priority = 5)
 	public void Features_183_184_Msgs_received_after_unblock() throws Exception {
 		
 		Opt_Out_WF OptOutWF = new Opt_Out_WF();
 		OptOutWF.Msgs_received_after_unblock();
 
 	}
   
   	@Test(priority = 6)
   	public void Features_186_188_user_Can_Block_Contact_Another_User_Cannot_unblock_Cannot() throws Exception {
   		
   		Opt_Out_WF OptOutWF = new Opt_Out_WF();
   		OptOutWF.user_Can_Block_Contact_Another_User_Cannot_unblock_Cannot();
   
   	}
   
   	@Test(priority = 7)
   	public void Features_187_User_Can_Unblock_Contact() throws Exception {
   	
   		Opt_Out_WF OptOutWF = new Opt_Out_WF();
   		OptOutWF.User_Can_Unblock_Contact();
   
   	}
   
   	@Test(priority = 8)
   	public void Features_189_user_receive_msg_After_Unblock() throws Exception {
   		
   		Opt_Out_WF OptOutWF = new Opt_Out_WF();
   		OptOutWF.user_receive_msg_After_Unblock();
   
   	}
   
   	@Test(priority = 9)
   	public void Features_190_user_Can_Sendstop_BLocked_start_Unblocked() throws Exception {
   		
   		Opt_Out_WF OptOutWF = new Opt_Out_WF();
   		OptOutWF.user_Can_Sendstop_BLocked_start_Unblocked();
   
   	}
   
   	@Test(priority = 10)
   	public void Features_192_193_194_Stop_Filter_Shows_Blocked_contact() throws Exception {
   	
   		Opt_Out_WF OptOutWF = new Opt_Out_WF();
   		OptOutWF.Stop_Filter_Shows_Blocked_contact();
   
   	}
   	
	@Test(priority = 11)
   	public void Features_182_185_opt_out_MSG_BM() throws Exception {
   		
   		Opt_Out_WF OptOutWF = new Opt_Out_WF();
   		OptOutWF.opt_out_MSG_BM();
   
   	}
    /*@AfterMethod
    public void logout() {
    	driver.findElement(By.xpath("//li[6]/div[1]/a[1]/app-svg-icon[1]/span[1]")).click();
    	driver.findElement(By.xpath("//a[@id='logoutId']")).click();
    }*/
   	@AfterClass
	public void tearDown(){
		driver.quit();
	}

}
