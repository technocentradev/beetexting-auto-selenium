package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.settings.SettignsOrganizationNameEditWF;
import com.beetext.qa.base.TestBase;

public class SettingsOrganizationNameEdits extends TestBase {
	

	@BeforeClass
	public void setUp() {
		initialization();
		Login();
		driver.findElement(By.xpath("//span[@class='s-title']")).click();
	}

	@Test(priority=1)
	public void Settings_004_editOrgName_single_char_validaiton() throws Exception {

		SettignsOrganizationNameEditWF editOrg = new SettignsOrganizationNameEditWF();
		editOrg.Settings_OrganizatioName_contain_single_char();
	}

	@Test(priority=2)
	public void Settings_005_editOrgName_single_char_validaiton() throws Exception {

		SettignsOrganizationNameEditWF editOrg = new SettignsOrganizationNameEditWF();
		editOrg.Settings_OrganizatioName_lessthan_3char_validation();
	}

	@Test(priority=3)
	public void Settings_007_editOrgName_required() throws Exception {
		SettignsOrganizationNameEditWF editOrg = new SettignsOrganizationNameEditWF();
		editOrg.Settings_OrganizatioName_required_validation();
	}

	@Test(priority=4)
	public void Settings_006_editOrgName_Update_clickon_save() throws Exception {

		SettignsOrganizationNameEditWF editOrg = new SettignsOrganizationNameEditWF();
		editOrg.Settings_OrganizatioName_Update();
	}

	@Test(priority=5)
	public void Settings_008_editOrgName_Button_disabled() throws Exception {

		SettignsOrganizationNameEditWF editOrg = new SettignsOrganizationNameEditWF();
		editOrg.Settings_OrganizatioName_Button_Disabled();
	}

	@Test(priority=6)
	public void Settings_009_editOrgName_Duplicate_validation_checking() throws Exception {

		SettignsOrganizationNameEditWF editOrg = new SettignsOrganizationNameEditWF();
		editOrg.Settings_OrganizatioName_Duplicate_Validation_checking();
	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		//settingsLogout();
	}

}
