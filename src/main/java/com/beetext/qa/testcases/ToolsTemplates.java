package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.templates.ToolsTemplateWF;
import com.beetext.qa.base.TestBase;

public class ToolsTemplates extends TestBase {

	@BeforeClass
	// @BeforeClass
	public void setUp() throws Exception {
		initialization();
		Login();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();

	}

	@Test(priority = 1) // ADD test case in regression suite
	public void Tools_Template_005_persaonal_template_only_title_click_on_create_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.personaltemplate_only_title_clickon_create_button();
	}

	@Test(priority = 2) // ADD test case in regression suite
	public void Tools_Template_006_shared_template_only_title_click_on_create_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_only_title_clickon_create_button();
	}

	@Test(priority = 3)
	public void Tools_Template_651_personaltemplate_title_3char_validation() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.personaltemplate_title_1_char_validation();
	}

	@Test(priority = 4)
	public void Tools_Template_652_personaltemplate_title_min_3_char_validation() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.personaltemplate_title_3_char_validation();
	}

	@Test(priority = 5)
	public void Tools_Template_654_sharedtemplate_title_min_3_char_validation() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_title_3_char_validation();
	}

	@Test(priority = 6)
	public void Tools_Template_656_sharedtemplate_already_exist_title_validation() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_title_already_exit_validation();
	}

	@Test(priority = 7)
	public void Tools_Template_656_persaonal_template_already_exist_title_validation() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.personaltemplate_title_already_exit_validation();
	}

	@Test(priority = 8)
	public void Tools_Template_681_shared_template_nodata_create_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_no_data_clickon_create_button();
	}

	@Test(priority = 9)
	public void Tools_Template_673_personal_template_nodata_create_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.personaltemplate_no_data_clickon_create_button();
	}

	@Test(priority = 10)
	public void Tools_Template_657_personaltemplate_creation_deletion() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.personaltemplate_creation_deletion();
	}

	@Test(priority = 11)
	public void Tools_Template_655_personaltemplate_creation_deletion() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.personaltemplate_1000char_message_creation_deletion();
	}

	@Test(priority = 12)
	public void Tools_Template_655_sharedtemplate_1000char_message_creation_deletion() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_1000char_message_creation_deletion();
	}

	// writing started date 2-08-2021
	@Test(priority = 13)
	public void Tools_Template_658_sharedltemplate_creation_deletion() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_creation_deletion();
	}

	@Test(priority = 14)
	public void Tools_Template_682_select_sharedoption_without_select_department_and_deletion() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.select_sharedoption_dont_select_dept();
	}

	@Test(priority = 15)
	public void Tools_Template_771_clickedon_sharedtab() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickedon_sharedtab_display_shared_template_list();
	}

	@Test(priority = 16)
	public void Tools_Template_772_select_personaltab() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_personaltab_display_personal_templates();
	}

	@Test(priority = 17)
	public void Tools_Template_773_clickon_drafttab() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.create_draft_template_clickon_draft_tab_display_draft_templates();
	}

	@Test(priority = 18)
	public void Tools_Template_765_sharedtemplate_givetitle_clickon_Breadscrumlink_clickon_Savedraft_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.shared_template_give_title_nodept_clickon_breadscrumlink_clickon_savedraft();
	}

	@Test(priority = 19)
	public void Tools_Template_766_sharedtemplate_givetitle_message_nodept_clickon_Breadscrumlink_clickon_Savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.shared_template_give_title_message_nodept_clickon_breadscrumlink_clickon_savedraft();
	}

	@Test(priority = 20)
	public void Tools_Template_767_sharedtemplate_givetitle_message_selectdept_clickon_Breadscrumlink_clickon_Savedraft_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.shared_template_give_title_message_selectdept_clickon_breadscrumlink_clickon_savedraft();
	}

	@Test(priority = 21)
	public void Tools_Template_768_sharedtemplate_givetitle_nodept_clickon_Savedraft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.shared_template_give_title_nodept_clickon_savedraft_button();
	}

	@Test(priority = 22)
	public void Tools_Template_769_sharedtemplate_givetitle_message_nodept_clickon_Savedraft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.shared_template_give_title_message_nodept_clickon_savedraft_button();
	}

	@Test(priority = 23)
	public void Tools_Template_770_sharedtemplate_givetitle_message_selectdept_clickon_Savedraft_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.shared_template_give_title_message_select_dept_clickon_savedraft_button();
	}

	@Test(priority = 24)
	public void Tools_Template_775_change_selectpersonaltemplate_changeto_shared_template_select_alldepartments()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltemplate_manangepage_changeto_shared_tempalte();
	}

	@Test(priority = 25)
	public void Tools_Template_776_778_779_change_selectpersonaltemplate_changeto_shared_template_select_one_departments()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltemplate_manangepage_changeto_shared_tempalte_select_one_department();
	}

	@Test(priority = 26)
	public void Tools_Template_781_783_manage_sharedtemplate_unselect_departments_save_changes() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_creation_mangepage_unselect_alldepartments();
	}

	@Test(priority = 27)
	public void Tools_Template_782_manage_sharedtemplate_edit_template_title_save_changes() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.sharedtemplate_creation_edit_template();
	}

	@Test(priority = 28)
	public void Tools_Template_784_manage_persoanltemplate_edit_template_title_save_changes() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltemplate_manangepage_edittitle_save_changes();
	}

	// @Test(priority = 29)
	public void Tools_Template_785_manage_persoanltemplate_remove_message_attachment_save_changes() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltemplate_manangepage_remove_message_attachments_save_changes();
	}

	@Test(priority = 30)
	public void Tools_Template_660_createtempalte_entertitle_and_message_clickon_savedraft() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltemplate_manangepage_title_andmessage_clickon_savedarft();
	}

	@Test(priority = 31)
	public void Tools_Template_684_createtempalte_enteronlytitle_selectsharedoption_clickonsavedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_selectct_shared_dont_select_department_clickon_savedarft();
	}

	@Test(priority = 32)
	public void Tools_Template_659_createtempalte_enteronlytitle_selectsharedoption_clickonsavedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltemplate_manangepage_title_andattachments_clickon_create_button();
	}

	@Test(priority = 33)
	public void Tools_Template_685_createtempalte_entertitle_message_selectsharedoption_select_department_clickonsavedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createsharedtemplate_entertitle_message_selectct_shared__dont_select_department_clickon_savedarft();
	}

	@Test(priority = 34)
	public void Tools_Template_686_createtempalte_entertitle_message_selectsharedoption_select_department_clickonsavedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_message_selectct_shared__select_department_clickon_savedarft();
	}

	@Test(priority = 35)
	public void Tools_Template_687_createtempalte_entertitle__selectsharedoption_close_window_clickonsavedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createsharedtemplate_entertitle_selectct_shared_dont_select_department_closewindow_clickon_savedarft();
	}

	@Test(priority = 36)
	public void Tools_Template_689_createtempalte_entertitle_message_selectsharedoption_select_department_close_window_clickonsavedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createsharedtemplate_entertitle_message_selectct_shared__select_department_close_tools_window_clickon_savedarft();
	}

	@Test(priority = 37)
	public void Tools_Template_688_createtempalte_entertitle_message_selectsharedoption_dont_select_department_close_window_clickonsavedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createsharedtemplate_entertitle_message_selectct_shared__dont_select_department_close_tools_window_clickon_savedarft();
	}

	@Test(priority = 38)
	public void Tools_Template_665_createtempalte_entertitle_click_bradscrumlink_clickonsavedraft() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_clickon_breadscrumlik_clickon_savedarft();
	}

	@Test(priority = 39)
	public void Tools_Template_666_createtempalte_entertitle_message_bradscrumlink_clickonsavedraft() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_message_clickon_breadscrumlik_clickon_savedarft();
	}

	@Test(priority = 40)
	public void Tools_Template_666_createtempalte_entertitle_close_tools_window_clickon_discard() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_close_window_clickon_discard();
	}

	@Test(priority = 41)
	public void Tools_Template_667_createtempalte_entertitle_message_close_tools_window_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_message_closewindow_clickon_discard();
	}

	@Test(priority = 42)
	public void Tools_Template_671_createtempalte_entertitle_clickon_bradscrumlink_clickon_discard() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_clickon_breadscrumlink_clickon_discard();
	}

	@Test(priority = 43)
	public void Tools_Template_672_createtempalte_entertitle_message_clickon_bradscrumlink_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_message_clickon_breadscrumlink_clickon_discard();
	}

	// writting staterd:9-08-2021

	@Test(priority = 44)
	public void Tools_Template_675_createtempalte_close_tooslpopuup_clickon_savedraft_button() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.opencreatetemplate_nodata_close_tools_window_clickon_savedarft();
	}

	@Test(priority = 45)
	public void Tools_Template_677_createtempalte_clickon_breadscrumlink_clickon_savedraft_button() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.opencreatetemplate_nodata_clickon_breadscrumlik_clickon_savedarft();
	}

	@Test(priority = 46)
	public void Tools_Template_678_createtempalte_close_toolswindow__clickon_discard_button() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.opencreatetemplate_nodata_close_tools_window_discard_button();
	}

	@Test(priority = 47)
	public void Tools_Template_680_createtempalte_clickon_breadscrumlink_clickon_discard_button() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.opencreatetemplate_nodata_clickon_breadscrumlik_clickon_discard();
	}

	@Test(priority = 48)
	public void Tools_Template_690_createtempalte_enter_title_close_toolspopup_clickon_savedraft() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle__dont_select_department_close_tools_window_clickon_savedarft();
	}

	@Test(priority = 49)
	public void Tools_Template_691_createtempalte_enter_title_message_close_toolspopup_clickon_savedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createsharedtemplate_entertitle_message__dont_select_department_close_tools_window_clickon_savedarft();
	}

	@Test(priority = 50)
	public void Tools_Template_692_createtempalte_enter_title_message_select_dept_close_toolspopup_clickon_savedraft()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_message__select_department_close_tools_window_clickon_savedarft();
	}

	@Test(priority = 51)
	public void Tools_Template_693_createtempalte_enter_title__no_dept_close_toolspopup_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_close_tools_window_clickon_discard();
	}

	@Test(priority = 52)
	public void Tools_Template_694_695_createtempalte_enter_title__message_no_dept_close_toolspopup_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_Message_no_dept_close_tools_window_clickon_discard();
	}

	@Test(priority = 53)
	public void Tools_Template_696_createtempalte_enter_title__message_select_dept_close_toolspopup_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_Message_select_dept_close_tools_window_clickon_discard();
	}

	@Test(priority = 54)
	public void Tools_Template_697_createtempalte_enter_title_nodept_clickon_anyotherlink_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_clickon_any_other_link_clickon_discard();
	}

	@Test(priority = 55)
	public void Tools_Template_698_createtempalte_enter_title_message_nodept_clickon_anyotherlink_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_message_nodept_clickon_any_other_link_clickon_discard();
	}

	@Test(priority = 56)
	public void Tools_Template_699_createtempalte_enter_title_message_selectdept_clickon_anyotherlink_clickon_discard()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_entertitle_message_selectdept_clickon_any_other_link_clickon_discard();
	}

	@Test(priority = 57)
	public void Tools_Template_940_Managepersonaltemplate_remove_template_title_message_add_attachmet_clickon_delete_confirm_deteletemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_textmessage_add_attachment_clickon_delete_confirm_delete_button();
	}

	@Test(priority = 58)
	public void Tools_Template_941_Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_updatetemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_update_template_button();
	}

	@Test(priority = 59)
	public void Tools_Template_942_Managepersonaltemplate_edit_template_title_message_add_1000char_template_clickon_savedrafttemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_change_title_1000char_textmessage_clickon_savedraft_template_button();
	}

	@Test(priority = 60)
	public void Tools_template_828_nodata_clickon_create_button() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtempalte_dont_fill_data_clickon_create_button();
	}

	@Test(priority = 61)
	public void Tools_Template_912_Managepersonaltemplate_update_template_title_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_savedraft_button();
	}

	@Test(priority = 62)
	public void Tools_Template_913_Managepersonaltemplate_update_template_title_clickon_delete_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_deletetemplate_button();
	}

	@Test(priority = 63)
	public void Tools_Template_914_915_Managepersonaltemplate_update_template_title_clickon_breadscrumlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_breadscrumlink_clickon_savedraft_button();
	}

	@Test(priority = 64)
	public void Tools_Template_916_Managepersonaltemplate_update_template_title_clickon_breadscrumlink_clickon_Discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_breadscrumlink_clickon_discard_button();
	}

	@Test(priority = 65)
	public void Tools_Template_917_918_Managepersonaltemplate_update_template_title_clickon_anyotherlink_clickon_save_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_anyotherlink_clickon_save_button();
	}

	@Test(priority = 66)
	public void Tools_Template_919_Managepersonaltemplate_update_template_title_clickon_anyotherlink_clickon_Discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_anyotherlink_clickon_discard_button();
	}

	@Test(priority = 67)
	public void Tools_Template_920_921_Managepersonaltemplate_update_template_title_clickon_close_tools_popup_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_closepopup_clickon_save_button();
	}

	@Test(priority = 68)
	public void Tools_Template_922_Managepersonaltemplate_update_template_title_clickon_close_popup_clickon_Discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_close_tools_popup_clickon_discard_button();
	}

	@Test(priority = 69)
	public void Tools_Template_926_927_Managepersonaltemplate_update_template_title_remove_message_add_attachment_clickon_breadscrumlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_changetitle_removetextmessage_add_attachment_clickon_breadscrumlink_clickon_save_button();
	}

	@Test(priority = 70)
	public void Tools_Template_928_Managepersonaltemplate_update_template_title_remove_message_add_attachmet_clickon_breadscrumlink_clickon_Discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonoption_changetitle_removemessage_add_attachment_clickonbreadscrumlink_clickon_discard_button();
	}

	@Test(priority = 71)
	public void Tools_Template_929_930Managepersonaltemplate_update_template_title_remove_message_add_attachmet_close_popup_clickon_Save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_changetitle_removetextmessage_add_attachment_clickon_closetoolspopup_clickon_save_button();
	}

	@Test(priority = 72)
	public void Tools_Template_931_Managepersonaltemplate_update_template_title_remove_message_add_attachmet_close_popup_clickon_Discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonoption_changetitle_removemessage_add_attachment_clickon_close_tools_popup_clickon_discard_button();
	}

	@Test(priority = 73)
	public void Tools_Template_932_933__Managepersonaltemplate_update_template_title_remove_message_add_attachmet_clickon_anyotherlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_changetitle_removetextmessage_add_attachment_clickon_anyotherlink_clickon_save_button();
	}

	@Test(priority = 74)
	public void Tools_Template_934_Managepersonaltemplate_update_template_title_remove_message_add_attachmet_clickon_anyotherlink_clickon_Discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonoption_changetitle_removemessage_add_attachment_clickon_anyotherlink_clickon_discard_button();
	}

	@Test(priority = 75)
	public void Tools_Template_938_Managepersonaltemplate_remove_template_title_message_add_attachmet_clickon_updatetemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_textmessage_add_attachment_clickon_anyotherlink_clickon_updatetempalte_button();
	}

	@Test(priority = 76)
	public void Tools_Template_939_Managepersonaltemplate_remove_template_title_message_add_attachmet_clickon_savedrafttemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.Managetemplate_selectpersonopton_removetitle_textmessage_add_attachment_clickon_anyotherlink_clickon_savedrafttempalte_button();
	}

	@Test(priority = 77)
	public void Tools_template_825_personaltemplate_defaultmessage_validation() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_personaltab_check_default_message_validation();
	}

	@Test(priority = 78)
	public void Tools_template_826_sharedtemplate_defaultmessage_validation() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_sharedtab_check_default_message_validation();
	}

	@Test(priority = 79)
	public void Tools_template_827_drafttemplate_defaultmessage_validation() throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.clickon_drafttab_check_default_message_validation();
	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.close();
		//Kill_ChromeDriver();
	}

}
