package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.Workflows.vcf.vcf_TemplatesWF;
import com.beetext.qa.base.TestBase;

public class vcf_Templates_Test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		VCF_WFCM vcfwf = new VCF_WFCM();
		vcfwf.VCard_Auto1_Login_tools_Button();
	}

	@Test(priority = 1)
	public void vcf_Templates_01_VCard_Available_in_SharedTemplates() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.VCard_Available_in_SharedTemplates();
	}

	@Test(priority = 2)
	public void vcf_Templates_02_In_SharedTemplates_Bydefault_Vcard_Disable() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.In_SharedTemplates_Bydefault_Vcard_Disable();
	}

	@Test(priority = 3)
	public void vcf_Templates_03_In_SharedTemplates_Select_Dept_Vcard_Enaable() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.In_SharedTemplates_Select_Dept_Vcard_Enable();
	}

	@Test(priority = 4)
	public void vcf_Templates_04_In_SharedTemplates_Select_Dept_and_Vcard_Select_AnotherDept_Vcard_Disable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.In_SharedTemplates_Select_Dept_and_Vcard_Select_AnotherDept_Vcard_Disable();
	}

	@Test(priority = 5)
	public void vcf_Templates_05_In_SharedTemplates_Select_TwoDepts_Vcard_Will_Disable() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.In_SharedTemplates_Select_TwoDepts_Vcard_Will_Disable();
	}

	@Test(priority = 6)
	public void vcf_Templates_06_In_SharedTemplates_Select_AllDepts_Vcard_Will_Disable() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.In_SharedTemplates_Select_AllDepts_Vcard_Will_Disable();
	}

	@Test(priority = 7)
	public void vcf_Templates_07_In_SharedTemplates_Select_no_VCF_Dept_Vcard_Will_Disable() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.In_SharedTemplates_Select_no_VCF_Dept_Vcard_Will_Disable();
	}

	@Test(priority = 8)
	public void vcf_Templates_08_In_SharedTemplates_Select_Dept_and_Vcard_DeSelect_Dept_Vcard_Disable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.In_SharedTemplates_Select_Dept_and_Vcard_DeSelect_Dept_Vcard_Disable();
	}

	@Test(priority = 9)
	public void vcf_Templates_09_Templates_Select_NoVcfDept_selectVcfDept_Deselect_NoVcfDept_VCardEnable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_Select_NoVcfDept_selectVcfDept_Deselect_NoVcfDept_VCardEnable();
	}

	@Test(priority = 10)
	public void vcf_Templates_10_Templates_Select_VcfDept_selectNoVcfDept_Deselect_VcfDept_VCardDisable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_Select_VcfDept_selectNoVcfDept_Deselect_VcfDept_VCardDisable();
	}

	@Test(priority = 11)
	public void vcf_Templates_11_Templates_9Attachments_and_VCF() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_9Attachments_and_VCF();
	}

	@Test(priority = 12)
	public void vcf_Templates_12_Templates_Attachment_Emoji_VCF() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_Attachment_Emoji_VCF();
	}

	@Test(priority = 13)
	public void vcf_Templates_13_Templates_Emoji_VCF() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_Emoji_VCF();
	}

	@Test(priority = 14)
	public void vcf_Templates_14_Templates_Message_VCF() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_Message_VCF();
	}

	@Test(priority = 15)
	public void vcf_Templates_15_Templates_Message_Emoji_VCF() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_Message_Emoji_VCF();
	}

	@Test(priority = 16)
	public void vcf_Templates_16_Templates_Select_Vcard_Click_On_Personal_Vcard_Disable() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_Select_Vcard_Click_On_Personal_Vcard_Disable();
	}

	@Test(priority = 17)
	public void vcf_Templates_17_Templates_manage_Already_Vcf_Available_Vacrd_Willbe_Disable() throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_manage_Already_Vcf_Available_Vacrd_Willbe_Disable();
	}

	@Test(priority = 18)
	public void vcf_Templates_18_Templates_manage_Already_Vcf_Available_Select_NoVCFDept_Vacrd_Willbe_Disable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_manage_Already_Vcf_Available_Select_NoVCFDept_Vacrd_Willbe_Disable();
	}

	@Test(priority = 19)
	public void vcf_Templates_19_Templates_manage_Already_Vcf_Available_Select_AllDept_Vacrd_Willbe_Disable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.Templates_manage_Already_Vcf_Available_Select_AllDept_Vacrd_Willbe_Disable();
	}

	@Test(priority = 20)
	public void vcf_Templates_20_ManageTemplates_Select_NoVcfDept_selectVcfDept_Deselect_NoVcfDept_VCardEnable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.ManageTemplates_Select_NoVcfDept_selectVcfDept_Deselect_NoVcfDept_VCardEnable();
	}

	@Test(priority = 21)
	public void vcf_Templates_21_ManageTemplates_Select_VcfDept_selectNoVcfDept_Deselect_VcfDept_VCardDisable()
			throws Exception {

		vcf_TemplatesWF VCFWF = new vcf_TemplatesWF();
		VCFWF.ManageTemplates_Select_VcfDept_selectNoVcfDept_Deselect_VcfDept_VCardDisable();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
