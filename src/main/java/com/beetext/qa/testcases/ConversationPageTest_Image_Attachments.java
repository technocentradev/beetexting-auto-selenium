package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.ConversationWF_Image_Attachments;
import com.beetext.qa.base.TestBase;

public class ConversationPageTest_Image_Attachments extends TestBase {
	Map<String, String> usercredentials = new HashMap<String, String>();

	@BeforeClass
	public void setUp() {
		initialization();

		String username1 = prop.getProperty("username1");
		String username2 = prop.getProperty("username2");
		String password = prop.getProperty("password");
		usercredentials.put("username1", username1);
		usercredentials.put("username2", username2);
		usercredentials.put("password", password);
	}

	@Test(priority = 1)
	public void conversations_message() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.message(usercredentials);
	}

	@Test(priority = 2)
	public void conversations_message_png() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_png(usercredentials);
	}

	@Test(priority = 3)
	public void conversations_message_bmpFile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_bmpFile(usercredentials);
	}

	@Test(priority = 4)
	public void conversations_message_jpgfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpgfile(usercredentials);
	}

	@Test(priority = 5)
	public void conversations_message_JpegFile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_JpegFile(usercredentials);
	}

	@Test(priority = 6)
	public void conversations_message_GifFile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_GifFile(usercredentials);

	}

	@Test(priority = 7)
	public void conversations_message_tiffFile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_tiffFile(usercredentials);

	}

	@Test(priority = 8)
	public void conversations_message_pngfile_bmpfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_pngfile_bmpfile(usercredentials);

	}

	@Test(priority = 9)
	public void conversations_message_pngfile_jpgfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_pngfile_jpgfile(usercredentials);
	}

	@Test(priority = 10)
	public void conversations_message_pngfile_jpegfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_pngfile_jpegfile(usercredentials);
	}

	@Test(priority = 11)
	public void conversations_message_pngfile_giffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_pngfile_giffile(usercredentials);
	}

	@Test(priority = 12)
	public void conversations_message_pngfile_tifffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_pngfile_tifffile(usercredentials);
	}

	@Test(priority = 13)
	public void conversations_message_bmpfile_pngfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_bmpfile_pngfile(usercredentials);
	}

	@Test(priority = 14)
	public void conversations_message_bmpfile_jpgfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_bmpfile_jpgfile(usercredentials);
	}

	@Test(priority = 15)
	public void conversations_message_bmpfile_jpegfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_bmpfile_jpegfile(usercredentials);
	}

	@Test(priority = 16)
	public void conversations_message_bmpfile_giffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_bmpfile_giffile(usercredentials);
	}

	@Test(priority = 17)
	public void conversations_message_bmpfile_tifffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_bmpfile_tifffile(usercredentials);
	}

	@Test(priority = 18)
	public void conversations_message_jpgfile_pngfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpgfile_pngfile(usercredentials);
	}

	@Test(priority = 19)
	public void conversations_message_jpgfile_Bmpfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpgfile_Bmpfile(usercredentials);
	}

	@Test(priority = 20)
	public void conversations_message_jpgfile_Jpegfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpgfile_Jpegfile(usercredentials);
	}

	@Test(priority = 21)
	public void conversations_message_jpgfile_GIFfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpgfile_GIFfile(usercredentials);
	}

	@Test(priority = 22)
	public void conversations_message_jpgfile_TIFFfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpgfile_TIFFfile(usercredentials);
	}

	@Test(priority = 23)
	public void conversations_message_jpegfile_pngfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpegfile_pngfile(usercredentials);
	}

	@Test(priority = 24)
	public void conversations_message_jpegfile_bmpfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpegfile_bmpfile(usercredentials);
	}

	@Test(priority = 25)
	public void conversations_message_jpegfile_jpgfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpegfile_jpgfile(usercredentials);
	}

	@Test(priority = 26)
	public void conversations_message_jpegfile_giffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpegfile_giffile(usercredentials);
	}

	@Test(priority = 27)
	public void conversations_message_jpegfile_Tifffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_jpegfile_Tifffile(usercredentials);
	}

	@Test(priority = 28)
	public void conversations_message_giffile_pngfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_giffile_pngfile(usercredentials);
	}

	@Test(priority = 29)
	public void conversations_message_giffile_bmpfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_giffile_bmpfile(usercredentials);
	}

	@Test(priority = 30)
	public void conversations_message_giffile_jpgfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_giffile_jpgfile(usercredentials);
	}

	@Test(priority = 31)
	public void conversations_message_giffile_jpegfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_giffile_jpegfile(usercredentials);
	}

	@Test(priority = 32)
	public void conversations_message_giffile_tifffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_giffile_tifffile(usercredentials);
	}

	@Test(priority = 33)
	public void conversations_message_tifffile_pngfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_tifffile_pngfile(usercredentials);
	}

	@Test(priority = 34)
	public void conversations_message_tifffile_bmpfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_tifffile_bmpfile(usercredentials);
	}

	@Test(priority = 35)
	public void conversations_message_tifffile_JPGfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_tifffile_JPGfile(usercredentials);
	}

	@Test(priority = 36)
	public void conversations_message_tifffile_jpegfile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_tifffile_jpegfile(usercredentials);
	}

	@Test(priority = 37)
	public void conversations_message_tifffile_giffile() throws Exception {

		ConversationWF_Image_Attachments conversationwf = new ConversationWF_Image_Attachments();
		conversationwf.conversations_message_tifffile_giffile(usercredentials);
	}

	@AfterMethod
	public void validate_Signout() throws Exception {
		VCard_Signout();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
