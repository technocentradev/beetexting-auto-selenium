package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.VCF_WFCM;
import com.beetext.qa.Workflows.vcf.vcf_BroadcastWF;
import com.beetext.qa.base.TestBase;

public class vcf_Broadcast_Test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		VCF_WFCM vcfwf = new VCF_WFCM();
		vcfwf.VCard_Auto1_Login_tools_Button();
	}

	@Test(priority = 1)
	public void vcf_BM_02_VCF_9Attachments() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_9Attachments();
	}

	@Test(priority = 2)
	public void vcf_BM_03_VCF_10Attachments_Error() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_10Attachments_Error();
	}

	@Test(priority = 3)
	public void vcf_BM_04_VCF_BM_After_Selecting_Vcard_it_Should_Be_in_Disabled() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_After_Selecting_Vcard_it_Should_Be_in_Disabled();
	}

	@Test(priority = 4)
	public void vcf_BM_05_VCF_BM_1000_TextMsg_Vcf() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_1000_TextMsg_Vcf();
	}

	@Test(priority = 5)
	public void vcf_BM_06_VCF_BM_TextMsg_Vcf_Emoji() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_TextMsg_Vcf_Emoji();
	}

	@Test(priority = 6)
	public void vcf_BM_07_VCF_BM_TextMsg_Vcf_Emoji_Attachment() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_TextMsg_Vcf_Emoji_Attachment();
	}

	@Test(priority = 7)
	public void vcf_BM_08_VCF_BM_TextMsg_Vcf_Attachment_Recurring() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_TextMsg_Vcf_Attachment_Recurring();
	}

	@Test(priority = 8)
	public void vcf_BM_09_VCF_BM_TextMsg_Vcf_Attachment_non_Recurring() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_TextMsg_Vcf_Attachment_non_Recurring();
	}

	@Test(priority = 9)
	public void vcf_BM_10_VCF_BM_TextMsg_Vcf_Emoji_Attachment_Non_Recurring() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_TextMsg_Vcf_Emoji_Attachment_Non_Recurring();
	}

	@Test(priority = 10)
	public void vcf_BM_11_VCF_BM_TextMsg_Vcf_Emoji_Attachment_Recurring() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_TextMsg_Vcf_Emoji_Attachment_Recurring();
	}

	@Test(priority = 11)
	public void vcf_BM_12_VCF_BM_Past_Reuse_add_Vcf() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_Past_Reuse_add_Vcf();
	}

	@Test(priority = 12)
	public void vcf_BM_13_VCF_BM_Past_Reuse_text_add_Vcf() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_Past_Reuse_text_add_Vcf();
	}

	@Test(priority = 13)
	public void vcf_BM_14_VCF_BM_Create_BM_with_VCF_Delete_BM() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_Create_BM_with_VCF_Delete_BM();
	}

	@Test(priority = 14)
	public void vcf_BM_15_VCF_BM_Create_BM_click_Outside() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_Create_BM_click_Outside();
	}

	@Test(priority = 15)
	public void vcf_BM_16_VCF_BM_Save_BM_as_Draft() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_Save_BM_as_Draft();
	}

	@Test(priority = 16)
	public void vcf_BM_17_18_19_VCF_BM_admin_enable_Disable_VCF_also_For_Mange_Dept_File_Enabled() throws Exception {

		vcf_BroadcastWF vcfwf = new vcf_BroadcastWF();
		vcfwf.VCF_BM_admin_enable_Disable_VCF_also_For_Mange_Dept_File_Enabled();
	}

	@AfterClass
	public void tearDown() {
		driver.close();
	}
}
