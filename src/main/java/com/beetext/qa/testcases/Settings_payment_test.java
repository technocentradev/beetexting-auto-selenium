package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.settings.Settings_PaymentWF;
import com.beetext.qa.base.TestBase;

public class Settings_payment_test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {

		initialization();
		Settings_PaymentWF settingsPaymentWF = new Settings_PaymentWF();
		settingsPaymentWF.Login_Settings();
	}

	@Test(priority = 1)
	public void Settings_138_Settings_planBilling_defaultPayment() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_defaultPayment();

	}

	@Test(priority = 2)
	public void Settings_139_Settings_planBilling_TwoPayments() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_TwoPayments();

	}

	@Test(priority = 3)
	public void Settings_140_to_148_Settings_planBilling_Select_creditcard_and_Validations() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_Select_creditcard_and_Validations();

	}

	@Test(priority = 4)
	public void Settings_149_Settings_planBilling_creditCard_Back() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_creditCard_Back();

	}

	@Test(priority = 5)
	public void Settings_150_to_160Settings_planBilling_select_Achpayment_and_ACH_Validations() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_select_Achpayment_and_ACH_Validations();

	}

	@Test(priority = 6)
	public void Settings_161_Settings_planBilling_autoPayments() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_autoPayments();

	}

	@Test(priority = 7)
	public void Settings_162_Settings_planBilling_default_Payment() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_default_Payment();

	}

	@Test(priority = 8)
	public void Settings_163_Settings_planBilling_Select_creditcard_AllValidDetails_Save() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_Select_creditcard_AllValidDetails_Save();

	}

	@Test(priority = 9)
	public void Settings_164_Settings_planBilling_Ach_AllValidDetails() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_Ach_AllValidDetails();

	}

	@Test(priority = 10)
	public void Settings_165_Settings_planBilling_manage_primaryPayment() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_manage_primaryPayment();

	}

	@Test(priority = 11)
	public void Settings_166_Settings_planBilling_manage_changeExpiryDate() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_planBilling_manage_changeExpiryDate();

	}

	@Test(priority = 12)
	public void Settings_100_Settings_roles() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles();

	}

	@Test(priority = 13)
	public void Settings_101_Settings_roles_mySellerAccount() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_mySellerAccount();

	}

	@Test(priority = 14)
	public void Settings_102_Settings_roles_mySellerAccount_stripeDetails() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_mySellerAccount_stripeDetails();

	}

	@Test(priority = 15)
	public void Settings_105_Settings_roles_mySellerAccount_mockup_text() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_mySellerAccount_mockup_text();

	}

	@Test(priority = 16)
	public void Settings_106_Settings_roles_mySellerAccount_Click_StripeAcct_updateLink() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_mySellerAccount_Click_StripeAcct_updateLink();

	}

	@Test(priority = 17)
	public void Settings_107_108_Settings_roles_mySellerAccount_enableTips_DisableTips() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_mySellerAccount_enableTips_DisableTips();

	}

	@Test(priority = 18)
	public void Settings_109_Settings_roles_mySellerAccount_dollarSymbol() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_mySellerAccount_dollarSymbol();

	}

	@Test(priority = 19)
	public void Settings_110_Settings_roles_errorMsg_sellerAcct_NotAdded() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_errorMsg_sellerAcct_NotAdded();

	}

	@Test(priority = 20)
	public void Settings_111_Settings_roles_amount_Dollar_Symbol() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_amount_Dollar_Symbol();

	}

	@Test(priority = 21)
	public void Settings_112_Settings_roles_Bill_Description_100Char_ErrorMsg() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_Bill_Description_100Char_ErrorMsg();

	}

	@Test(priority = 22)
	public void Settings_113_Settings_roles_Amount_ErrorMsg() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_Amount_ErrorMsg();

	}

	@Test(priority = 23)
	public void Settings_114_Settings_roles_2DecimalAmount() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_2DecimalAmount();

	}

	@Test(priority = 24)
	public void Settings_115_Settings_billDescription_errorMsg() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_billDescription_errorMsg();

	}

	@Test(priority = 25)
	public void Settings_116_Settings_bill_crossmark() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_bill_crossmark();

	}

	@Test(priority = 26)
	public void Settings_117_Settings_bill_send_payment() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_bill_send_payment();

	}

	@Test(priority = 27)
	public void Settings_118_Settings_bill_send_payment_link_withText() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_bill_send_payment_link_withText();

	}

	@Test(priority = 28)
	public void Settings_119_120_121_122_123_124_125_Settings_click_linkToredirect_paymentPage() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_click_linkToredirect_paymentPage_FailedPayment();

	}

	@Test(priority = 29)
	public void Settings_122_123_124_125payment_successfull_msg() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.payment_successfull_msg();

	}

	@Test(priority = 30)
	public void Settings_126_Settings_click_linkToredirect_paymentPage_tips_added_or_not() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_click_linkToredirect_paymentPage_tips_added_or_not();

	}

	@Test(priority = 31)
	public void Settings_127_Settings_tips_display() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_tips_display();

	}

	@Test(priority = 32)
	public void Settings_128_Settings_tips_display_totalAmount_withTip() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_tips_display_totalAmount_withTip();

	}

	@Test(priority = 33)
	public void Settings_129_Settings_tips_display_totalAmount_with_10PercentTipp() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_tips_display_totalAmount_with_10PercentTip();

	}

	@Test(priority = 34)
	public void Settings_130_Settings_tips_display_totalAmount_with_customTip() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_tips_display_totalAmount_with_customTip();

	}

	@Test(priority = 35)
	public void Settings_131_Settings_tips_display_totalAmount_with_NoTip() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_tips_display_totalAmount_with_NoTip();

	}

	@Test(priority = 36)
	public void Settings_132_134tips_payment_successfull_msg() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.tips_payment_successfull_msg();

	}

	@Test(priority = 37)
	public void Settings_133_tips_ON_OFF() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.tips_ON_OFF();

	}

	@Test(priority = 38)
	public void Settings_135_Send_payment_Link() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Send_payment_Link();

	}

	@Test(priority = 39)
	public void Settings_137_none_options_enabled() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.none_options_enabled();

	}

	@Test(priority = 40)
	public void Settings_167_Settings_roles_Amount_ErrorMsg_for_5Digits() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_Amount_ErrorMsg_for_5Digits();

	}

	@Test(priority = 41)
	public void Settings_168_Settings_roles_Amount_ErrorMsg_For_Morethan_100000() throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_roles_Amount_ErrorMsg_For_Morethan_100000();

	}

	@Test(priority = 42)
	public void Settings_169_173_Settings_bill_send_payment_link_with_100000_No_error_redirects_to_PaymentPage()
			throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF.Settings_bill_send_payment_link_with_100000_No_error_redirects_to_PaymentPage();

	}

	@Test(priority = 43)
	public void Settings_170_171_172_tips_ON_GiveAmount_100000_Send_PaymentLink_SelectTip_and_CustomTip_as_1_Pay_Button_In_disabled()
			throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF
				.tips_ON_GiveAmount_100000_Send_PaymentLink_SelectTip_and_CustomTip_as_1_Pay_Button_In_disabled();

	}

	@Test(priority = 44)
	public void Settings_174_175_176_177_tips_ON_GiveAmount_99999_Send_PaymentLink_SelectTip_Pay_Button_In_disabled_CustomTip_as_1_NoTip_enable()
			throws Exception {

		Settings_PaymentWF settings_PaymentWF = new Settings_PaymentWF();
		settings_PaymentWF
				.tips_ON_GiveAmount_99999_Send_PaymentLink_SelectTip_Pay_Button_In_disabled_CustomTip_as_1_NoTip_enable();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
