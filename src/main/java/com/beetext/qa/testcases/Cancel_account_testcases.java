package com.beetext.qa.testcases;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.util.TestUtil;

public class Cancel_account_testcases extends TestBase {
//public static WebDriver driver;

	public void threedot() {
		driver.findElement(By.xpath("//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")).click();
	}

	public void profiles() {
		driver.findElement(By.xpath("//body/app-root[1]/div[1]/app-layout[1]/main[1]/aside[1]/ul[1]/li[7]/div[1]/ul[1]/li[1]/a[1]")).click();
	}

	public void profile_form() {
		driver.findElement(By.xpath("//a[@id='profileTab']")).click();
	}

	public void cancel_account_links() {
		driver.findElement(By.xpath(" //a[contains(text(),'How to Cancel your Beetexting Account')]")).click();
	}

	public String cancel_account_headdings() {
		return driver.findElement(By.xpath("//h1[contains(text(),'How to Cancel your Beetexting Account')]")).getText();
	}

	public void close() {
		driver.findElement(By.xpath("//*[@class='cross-icon']//*[name()='svg']")).click();
	}

	public void settingslink() {
		driver.findElement(By.xpath("//span[@class='s-title'][normalize-space()='Settings']")).click();
	}

	@BeforeMethod
	public void setUp() throws Exception {
		initialization();
	    Login();
	    Thread.sleep(5000);
	}

	@Test(priority = 1)
	public void account_cancel_through_profile_page() throws InterruptedException {

		threedot();
		Thread.sleep(5000);
		profiles();
		Thread.sleep(5000);
		profile_form();
		Thread.sleep(5000);
		cancel_account_links();
		Thread.sleep(3000);
		//((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		Thread.sleep(5);
		if (cancel_account_headdings().equalsIgnoreCase("How to Cancel your Beetexting Account")) {
			Assert.assertEquals(cancel_account_headdings(), "How to Cancel your Beetexting Account",
					"How to Cancel your Beetexting Account");
			TestUtil.takeScreenshotAtEndOfTest("How to Cancel your Beetexting Account");// ScreenShot capture

		} else {
			Assert.assertNotEquals(cancel_account_headdings(), "How to Cancel your Beetexting Account",
					"How to Cancel your Beetexting Account");
			TestUtil.takeScreenshotAtEndOfTest("How to Cancel your Beetexting Account");// ScreenShot capture
		}
		driver.close();
	}

	@Test(priority = 2)
	public void account_cancel_through_settings_page() throws InterruptedException {
		settingslink();
		Thread.sleep(5000);
		cancel_account_links();
		Thread.sleep(3000);
		//((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab.get(1));

		Thread.sleep(20000);
		if (cancel_account_headdings().equalsIgnoreCase("How to Cancel your Beetexting Account")) {
			Assert.assertEquals(cancel_account_headdings(), "How to Cancel your Beetexting Account",
					"How to Cancel your Beetexting Account");
			TestUtil.takeScreenshotAtEndOfTest("How to Cancel your Beetexting Account");// ScreenShot capture

		} else {
			Assert.assertNotEquals(cancel_account_headdings(), "How to Cancel your Beetexting Account",
					"How to Cancel your Beetexting Account");
			TestUtil.takeScreenshotAtEndOfTest("How to Cancel your Beetexting Account");// ScreenShot capture
		}

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
