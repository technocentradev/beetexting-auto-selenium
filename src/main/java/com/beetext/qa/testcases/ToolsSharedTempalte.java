package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.templates.ToolsSharedTemplatesWF;
import com.beetext.qa.base.TestBase;

public class ToolsSharedTempalte extends TestBase {

	@BeforeClass
	public void setUp() {
		initialization();
		Login(); // for login
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();

	}

	@Test(priority = 1)
	public void Tools_Template_1015_shared_entertitle_entermessage_selectonedept_clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_entertitle_message_selectone_department_clickon_create_template_button();
	}

	@Test(priority = 2)
	public void Tools_Template_1016_shared_title_entermessage_selectalldept_clickon_create_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_entertitle_message_select_all_department_clickon_create_template_button();
	}

	@Test(priority = 3)
	public void Tools_Template_1017_shared_title_entermessage_selectalldept_unselect_one_dept_clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_entertitle_message_select_all_department_unselect_one_dept_clickon_create_template_button();
	}

	@Test(priority = 4)
	public void Tools_Template_1018_shared_entertitle_nomessage_selectalldept__clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_entertitle_dontenter_message_select_all_department_clickon_create_template_button();
	}

	@Test(priority = 5)
	public void Tools_Template_1019_shared_notitle_entermessage_selectalldept__clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_entermessage_dontenter_title_select_all_department_clickon_create_template_button();
	}

	@Test(priority = 6)
	public void Tools_Template_1020_shared_title_addattachment_selectalldept__clickon_create_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_add_attachemnt_select_all_department_clickon_create_template_button();
	}

	@Test(priority = 7)
	public void Tools_Template_1021_shared_title_addattachment_selectonlyonedept__clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_add_attachemnt_select_one_department_clickon_create_template_button();
	}

	@Test(priority = 8)
	public void Tools_Template_1022_shared_title_addGIF_attachment_selectonlyonedept__clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_add_GIF_attachemnt_select_one_department_clickon_create_template_button();
	}

	@Test(priority = 9)
	public void Tools_Template_1023_shared_title_addemoji_attachment_selectonlyonedept__clickon_create_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_add_emoji_attachemnt_select_one_department_clickon_create_template_button();
	}

	@Test(priority = 10)
	public void Tools_Template_1024_shared_title_addGIF_attachment_selectonlyonedept__clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_add_GIF_attachemnt_select_one_department_clickon_savedraft_template_button();
	}

	@Test(priority = 11)
	public void Tools_Template_1025_shared_title_addemoji_attachment_selectonlyonedept__clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_add_emoji_attachemnt_select_one_department_clickon_savedraft_template_button();
	}

	@Test(priority = 12)
	public void Tools_Template_1028_1029_shared_entertitle_entermessage_selectonlyonedept__close_toolspopup_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_close_toolspopu_icon_clickon_savedraft_template_button();
	}

	@Test(priority = 13)
	public void Tools_Template_1030_shared_entertitle_entermessage_selectonlyonedept__close_toolspopup_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_close_toolspopup_clickon_discard_template_button();
	}

	@Test(priority = 14)
	public void Tools_Template_1031_1032_shared_entertitle_entermessage_selectonedept__clickon_breadscrumlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_dept_clickon_breadscrumlink_clickon_savedraft_template_button();
	}

	@Test(priority = 15)
	public void Tools_Template_1033_shared_entertitle_entermessage_select_all_dept_click_breadscrum_link_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_breadscrumlink_clickon_discard_template_button();
	}

	@Test(priority = 16)
	public void Tools_Template_1034_1035_shared_entertitle_entermessage_selectonedept__clickon_anyotherlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_dept_clickon_anyotherlink_clickon_savedraft_template_button();
	}

	@Test(priority = 17)
	public void Tools_Template_1036_shared_entertitle_entermessage_select_all_dept_click_onanyother_link_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_otherlink_clickon_discard_template_button();
	}

	@Test(priority = 18)
	public void Tools_Template_1037_1038_shared_entertitle_entermessage_selectalldept__clickon_anyotherlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_all_dept_clickon_anyotherlink_clickon_savedraft_template_button();
	}

	@Test(priority = 19)
	public void Tools_Template_1039_shared_entertitle_entermessage_select_all_dept_click_onanyother_link_clickon_discard_button()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_all_department_clickon_otherlink_clickon_discard_template_button();
	}

	@Test(priority = 20)
	public void Tools_Template_1040_1041_shared_entertitle_entermessage_selectonlyonedept__clickon_closetoolspopupicon_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_navigationlink_clickon_discard_template_button();
	}

	@Test(priority = 21)
	public void Tools_Template_1042_shared_entertitle_entermessage_selectonlyonedept__closetoolspopup_icon_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_enter_title_message_select_one_department_clickon_navigationlink_clickon_savedraft_template_button();
	}

	@Test(priority = 22)
	public void Tools_Template_1052_1053_notitle_message_clickon_breadscrumlink_clickon_savedraft_button()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_no_title_message_clickon_breadscrumlink_clickon_savedraft_template_button();
	}

	@Test(priority = 23)
	public void Tools_Template_1054_notitle_message_clickon_breadscrumlink_clickon_discard_button() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_no_title_message_clickon_breadscrumlink_clickon_discard_template_button();
	}

	@Test(priority = 24)
	public void Tools_Template_1058_1059_select_some_dept_entertitle_message_clickon_anyotherlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_some_dept_enter_title_message_clickon_anyotherlink_clickon_savedraft_template_button();
	}

	@Test(priority = 25)
	public void Tools_Template_1060_selecct_some_dept_entertitle_message_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_some_dept_enter_title_message_clickon_anyotherlink_clickon_discard_template_button();
	}

	// 20-09-2021
	@Test(priority = 26)
	public void Tools_Template_1055_1056_select_all_dept_entertitle_message_clickon_breadscrumlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_all_dept_enter_title_message_clickon_anyotherlink_clickon_savedraft_template_button();
	}

	@Test(priority = 27)
	public void Tools_Template_1057_selecct_all_dept_entertitle_message_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_all_dept_enter_title_message_clickon_breadscrumlink_clickon_discard_template_button();
	}

	@Test(priority = 28)
	public void Tools_Template_1061_1062_select_some_dept_entertitle_message_clickon_anyotherlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_some_dept_dont_give_title_message_clickon_anyotherlink_clickon_savedraft_template_button();
	}

	@Test(priority = 29)
	public void Tools_Template_1063_selecct_some_dept_entertitle_message_clickon_anyotherlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_some_dept_dontenter_title_message_clickon_anohterlink_clickon_discard_template_button();
	}

	@Test(priority = 30)
	public void Tools_Template_1064_1065_select_all_dept_entertitle_message_clickon_anyotherlink_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_all_dept_dont_give_title_message_clickon_anyotherlink_clickon_savedraft_template_button();
	}

	@Test(priority = 31)
	public void Tools_Template_1066_selecct_all_dept_entertitle_message_clickon_anyotherlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managetemplate_selectsharedoption_select_all_dept_dontenter_title_message_clickon_anohterlink_clickon_discard_template_button();
	}

	@Test(priority = 32)
	public void Tools_1076_manage_shared_Template_remove_title_message_addattacchments_clickon_update_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_update_template_button();
	}

	@Test(priority = 33)
	public void Tools_1077_manage_shared_Template_remove_title_message_addattacchments_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_savedraft_template_button();
	}

	@Test(priority = 34)
	public void Tools_1092_manage_shared_Template_remove_title_message_addattacchments_clickon_delete_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_anyotherlink_clickon_delete_confirm_delete_template_button();
	}

	@Test(priority = 35)
	public void Tools_manage_1078_1079_shared_Template_remove_title_message_addattacchments_clickon_breadscrumlink_clickon_save_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_breadscrumlink_clickon_save_template_button();
	}

	@Test(priority = 36)
	public void Tools_1080_manage_shared_Template_remove_title_message_addattacchments_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_breadscrumlink_clickon_discard_template_button();
	}

	@Test(priority = 37)
	public void Tools_1094_94_manage_shared_Template_remove_title_message_addattacchments_clickon_anyohterink_clickon_save_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_anyotherlink_clickon_save_template_button();
	}

	@Test(priority = 38)

	public void Tools_1096_1097_manage_shared_Template_remove_title_message_addattacchments_close_toolspopup_clickon_save_button()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_anyotherlink_clickon_discard_template_button();
	}

	@Test(priority = 39)
	public void Tools_1098_manage_shared_Template_remove_title_message_addattacchments_clickon_anyohterink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managesharedtemplate__remove_title_message_add_attachemnt_clickon_anyotherlink_clickon_discard_template_button();
	}

	@Test(priority = 40)
	public void Tools_1140_manage_shared_Template_title_message_add_onemore_dept_clickon_update_button()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_title_message_add_onemore_department_clickon_update_template_button();
	}

	@Test(priority = 41)
	public void Tools_1141_manage_shared_Template_title_message_add_onemore_dept_clickon_savedraft_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_title_message_add_onemore_department_clickon_Savedraft_template_button();
	}

	@Test(priority = 42)
	public void Tools_1142_manage_shared_Template_title_message_add_onemore_dept_clickon_delete_confirm_delete_button()
			throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate
				.Managesharedtemplate_title_message_add_onemore_department_clickon_delete_confirm_delete_template_button();
	}

	@Test(priority = 43)
	public void Tools_1143_manage_shared_Template_changeto_personal_template() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changeto_personal_template_button();
	}

	@Test(priority = 44)
	public void Tools_1144_manage_addemoji_clickon_update_button() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addemoji_clickon_update_template_button();
	}

	@Test(priority = 45)
	public void Tools_1145_manage_change_messagecontent_addemoji_clickon_update_button() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addemoji_clickon_update_template_button();
	}

	@Test(priority = 46)
	public void Tools_1146_manage_select_all_dept_unselect_dept_clickon_update_button() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_selectalldept_unselect_alldept_clickon_update_template_button();
	}

	@Test(priority = 47)
	public void Tools_1147_48_manage_unselect_one_dept_close_tools_popup_clickon_savebutton_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_unselectonedept_close_popup_clickon_save_button();
	}

	@Test(priority = 48)
	public void Tools_1149_manage_unselect_one_dept_close_tools_popup_clickon_discard_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_unselectonedept_close_popup_clickon_discard_button();
	}

	@Test(priority = 49)
	public void Tools_1156_57_manage_unselect_one_dept_clickon_anyotherlink_clickon_save_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_unselectonedept_clickon_anyotherlink_clickon_save_button();
	}

	@Test(priority = 50)
	public void Tools_1158_manage_unselect_one_dept_clickon_anyotherlink_clickon_discard_button() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_unselectonedept_clicon_anyotherlink_clickon_discard_button();
	}

	@Test(priority = 51)
	public void Tools_1159_manage_changetitle_addemoji_addgifimage_clickon_update_button() throws Exception {

		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changetitle_addgif_emoji_clickon_update_button();
	}

	@Test(priority = 52)
	public void Tools_1160_1161_manage_changetitle_addemoji_addgifimage_closetoolspopup_clickon_save_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changetitle_addgif_emoji_close_toolspopup_clickon_save_button();
	}

	@Test(priority = 53)
	public void Tools_1162_manage_changetitle_addemoji_addgifimage_closetoolspopup_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changetitle_addgif_emoji_close_toolspopup_clickon_discard_button();
	}

	@Test(priority = 54)
	public void Tools_1166_67_manage_changetitle_addemoji_addgifimage_clickon_navigationlink_clickon_save_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changetitle_addgif_emoji_clickon_navigationlink_clickon_save_button();
	}

	@Test(priority = 55)
	public void Tools_1168_manage_changetitle_addemoji_addgifimage_clickon_navigationlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changetitle_addgif_emoji_clickon_navigationlink_clickon_discard_button();
	}

	@Test(priority = 56) // problem with these script
	public void Tools_1169_70_manage_changetitle_addemoji_addgifimage_clickon_anyotherlink_clickon_save_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changetitle_addgif_emoji_clickon_anyotherlink_clickon_save_button();
	}

	@Test(priority = 57)
	public void Tools_1171_manage_changetitle_addemoji_addgifimage_clickon_anyotherlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_changetitle_addgif_emoji_clickon_anytoherlink_clickon_discard_button();
	}

	@Test(priority = 58)
	public void Tools_1172_73_manage_addimage_removegifimage_closetoolspopup_clickon_save_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addimage_removegif_emoji_close_toolspopup_clickon_save_button();
	}

	@Test(priority = 59)
	public void Tools_1174_manage_addimage_removegifimage_close_toolspopup_clickon_discard_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addimage_removegif_close_toolspopup_clickon_discard_button();
	}

	@Test(priority = 60)
	public void Tools_1178_79_manage_addimage_removegifimage_clickon_breadscrumlink_clickon_save_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addimage_removegif_clickon_breadscrumlink_clickon_save_button();
	}

	@Test(priority = 61)
	public void Tools_1180_manage_addimage_removegifimage_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addimage_removegif_clickon_breadscrumlink_clickon_discard_button();
	}

	@Test(priority = 62)
	public void Tools_1181_82_manage_addimage_removegifimage_clickon_anyohterlink_clickon_save_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addimage_removegif_clickon_anyotherlink_clickon_save_button();
	}

	@Test(priority = 63)
	public void Tools_1183_manage_addimage_removegifimage_clickon_anyohterlink_clickon_discard_button()
			throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Managesharedtemplate_addimage_removegif_clickon_anyotherlink_clickon_discard_button();
	}

	// 29-11-2021

	@Test(priority = 64)
	public void Tools__createtemplatepageenteronlytitle_clickon_create_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_clickon_create_button();
	}

	@Test(priority = 65)
	public void Tools__createtemplatepageenteronlytitle_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_clickon_saveDraft_button();
	}

	@Test(priority = 66)
	public void Tools__createtemplatepageenteronlytitle_message_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_Messageclickon_saveDraft_button();
	}

	@Test(priority = 67)
	public void Tools__createtemplatepageenteronlytitle_message_clickon_create_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_Messageclickon_create_button();
	}

	@Test(priority = 68)
	public void Tools__createtemplatepageenteronlytitle_closetoolswindow_clickon_savedraft_button() throws Exception {
		ToolsSharedTemplatesWF toolstemplate = new ToolsSharedTemplatesWF();
		toolstemplate.Createtemplatepage_enteronly_title_close_toolswindow_clickon_savedraft_button();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();

	}

}
