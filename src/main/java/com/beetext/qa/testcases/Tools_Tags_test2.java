package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.tags.Tools_TagsWF;
import com.beetext.qa.Workflows.tags.Tools_TagsWF2;
import com.beetext.qa.base.TestBase;

public class Tools_Tags_test2 extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Tools_TagsWF tagswf = new Tools_TagsWF();
		tagswf.tags_login();
	}

//	@Test(priority = 1)
//	public void tools_1220_tools_tags_clickon_createtag() throws Exception {
//
//		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
//		tools_tagsWF2.Tags_Apply_All();
//
//	}

	@Test(priority = 2)
	public void tools_1221_Tags_Select_Individual_Contact() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Select_Individual_Contact();

	}

	@Test(priority = 3)
	public void tools_1222_Tags_Select_Individual_Contact_shows_Top() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Select_Individual_Contact_shows_Top();

	}

	@Test(priority = 4)
	public void tools_1223_Tags_Search_With_Text_Apply_All() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Search_With_Text_Apply_All();

	}

	@Test(priority = 5)
	public void tools_1224_Tags_Search_With_Text_Apply_All_Remove_Text_Apply() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Search_With_Text_Apply_All_Remove_Text_Apply();

	}

	@Test(priority = 6)
	public void tools_1225_Tag_Edit_Add_Individual_Contact() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tag_Edit_Add_Individual_Contact();

	}

	@Test(priority = 7)
	public void tools_1226_Tags_Add_Individual_Contact_applyAll_Enable() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Add_Individual_Contact_applyAll_Enable();

	}

	@Test(priority = 8)
	public void tools_1227_Tags_Edit_remove_Individual_Contact() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Edit_remove_Individual_Contact();

	}

	@Test(priority = 9)
	public void tools_1228_Tags_ApplyAll_Edit_remove_Individual_Contact() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_ApplyAll_Edit_remove_Individual_Contact();

	}

	@Test(priority = 10)
	public void tools_1229_Tags_Search_ApplyAll_Edit_remove_Individual_Contact() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_ApplyAll_Edit_remove_Individual_Contact();

	}

	@Test(priority = 11)
	public void tools_1230_Tags_Search_deleteTag() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Search_deleteTag();

	}

	@Test(priority = 12)
	public void tools_1231_Tags_Create_Tag_Delete_in_MainPage() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Create_Tag_Delete_in_MainPage();

	}

	@Test(priority = 13)
	public void tools_1232_Tags_Selected_Contact_Shows_Top() throws Exception {

		Tools_TagsWF2 tools_tagsWF2 = new Tools_TagsWF2();
		tools_tagsWF2.Tags_Selected_Contact_Shows_Top();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
	}

}
