package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.Workflows.scheduled.Tools_SM_Emoji_WF;
import com.beetext.qa.base.TestBase;

public class Tools_SM_Emoji_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Tools_SM_WFCM SMCM = new Tools_SM_WFCM();
		SMCM.Login_Tools_SM();
		// Login(); //for login
	}

	@Test(priority = 1)
	public void TC_001_SM_Emoji() throws Exception {
		Tools_SM_Emoji_WF emoji = new Tools_SM_Emoji_WF();
		emoji.TC_013_Verify_emoji_added_record_under_upcoming();
	}

	@Test(priority = 2)
	public void TC_002_SM_Emoji() throws Exception {
		Tools_SM_Emoji_WF emoji = new Tools_SM_Emoji_WF();
		emoji.TC_014_Verify_emoji_added_record_under_Drafts();
	}

	@Test(priority = 3)
	public void TC_003_SM_Emoji() throws Exception {
		Tools_SM_Emoji_WF emoji = new Tools_SM_Emoji_WF();
		emoji.TC_016_Verify_emoji_added_record_under_upcoming();
	}

	@Test(priority = 4)
	public void TC_004_SM_Emoji() throws Exception {
		Tools_SM_Emoji_WF emoji = new Tools_SM_Emoji_WF();
		emoji.TC_018_Verify_emoji_added_record_under_Drafts();
	}

	@Test(priority = 5)
	public void TC_005_SM_Emoji() throws Exception {
		Tools_SM_Emoji_WF emoji = new Tools_SM_Emoji_WF();
		emoji.TC_019_Verify_emoji_added_record_under_Upcoming();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
