package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.tags.Tools_TagsApplyallWF;
import com.beetext.qa.base.TestBase;

public class Tools_Tags_Applyall extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();

		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys("automationblock@yopmail.com");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys("tech1234");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@id='toolsMainTab']")).click();
		Thread.sleep(2000);

	}

	@Test(priority = 1)
	public void tools_794_tools_tags_clickon_createtag_applyall() throws Exception {

		Tools_TagsApplyallWF tagsWF = new Tools_TagsApplyallWF();
		tagsWF.tools_tags_clickon_createtag_applyall();
	}
	
	@Test(priority = 2)
	public void tools_812_813_tools_tags_Edittag_save_Selct_AllContacts() throws Exception {

		Tools_TagsApplyallWF tagsWF = new Tools_TagsApplyallWF();
		tagsWF.tools_tags_Edittag_save_Selct_AllContacts();
	}

	@Test(priority = 3)
	public void tools_814tools_tags_editTag_Donot_selectcontact_errormsg() throws Exception {

		Tools_TagsApplyallWF tagsWF = new Tools_TagsApplyallWF();
		tagsWF.tools_tags_editTag_Donot_selectcontact_errormsg();
	}

	@Test(priority = 4)
	public void tools_1220_tools_tags_clickon_createtag() throws Exception {

		Tools_TagsApplyallWF tagsWF = new Tools_TagsApplyallWF();
		tagsWF.Tags_Apply_All();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
