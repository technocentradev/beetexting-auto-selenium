package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.Workflows.scheduled.Tools_SM_WF;
import com.beetext.qa.base.TestBase;

public class Tools_SM_testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Tools_SM_WFCM SMCM = new Tools_SM_WFCM();
		SMCM.Login_Tools_SM();

	}

	// Test cases for create SM starts

	@Test(priority = 1)

	public void TC_001_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_001_functionality_of_Create_Scheduled_message_button();
	}

	@Test(priority = 2)
	public void TC_002_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_002_functionality_of_Select_Department_DD();
	}

	@Test(priority = 3)
	public void TC_004_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_004_Should_allow_only_9_contacts_validation();
	}

	@Test(priority = 4)
	public void TC_006_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_006_functionality_of_Close_mark_for_contact();
	}

//	@Test(priority=5)
//	public void TC_007_Tools_SM_Create_page() throws Exception
//
//	{
//		Tools_SM_WF SM_WF = new Tools_SM_WF();
//		SM_WF.tc_007_Validation_message_for_1000_char_in_message_textbox();
//	}

	@Test(priority = 6)
	public void TC_012_Tools_SM_Create_page() throws Exception {

		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_012_Allows_Max_10_attachments();
	}

	@Test(priority = 7)
	public void TC_013_Tools_SM_Create_page() throws Exception

	{

		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_013_create_SM_with_attachment();
	}

	@Test(priority = 8)
	public void TC_014_Tools_SM_Create_page() throws Exception

	{

		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_014_create_SM_with_textmessage();
	}

	@Test(priority = 9)
	public void TC_015_Tools_SM_Create_page() throws Exception

	{

		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_015_create_SM_with_pastdate();
	}

	@Test(priority = 10)
	public void TC_016_Tools_SM_Create_page() throws Exception

	{

		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_016_Save_SM_with_attachments();
	}

	@Test(priority = 11)
	public void TC_017_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_017_functionality_of_Sceduled_button();
	}

	@Test(priority = 12)
	public void TC_018_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_018_functionality_of_Sceduled_button_with_attachments();
	}

	@Test(priority = 13)
	public void TC_019_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_019_Save_SM_with_message_attachments();
	}

	@Test(priority = 14)
	public void TC_020_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_020_verify_SM_without_contact_under_Draft_list();
	}

	@Test(priority = 15)
	public void TC_021_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_021_verify_SM_with_only_contact_under_Draft_list();
	}

	@Test(priority = 16)
	public void TC_022_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_022_Functionality_of_Delete_attachments();
	}

	@Test(priority = 17)
	public void TC_023_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_023_Functionality_of_save_Draft_button_in_CSM();
	}

	@Test(priority = 18)
	public void TC_024_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_024_Functionality_of_save_Draft_button_in_CSM();
	}

	@Test(priority = 19)
	public void TC_025_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_025_Functionality_of_SecheduleMessage_button_in_CSM_without_data();
	}

	@Test(priority = 20)
	public void TC_026_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_026_Functionality_of_SaveDraft_button_in_CSM_without_data();
	}

	@Test(priority = 21)
	public void TC_027_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_027_Functionality_of_ScheduledMessage_button_in_CSM_with_only_Recurring();
	}

@Test(priority = 22)
	public void TC_028_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_028_Functionality_of_ScheduledMessage_button_in_CSM_with_message_and_Recurring();
	}

	@Test(priority = 23)
	public void TC_029_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_029_functionality_of_SaveDraft_with_allData();
	}

	@Test(priority = 24)
	public void TC_030_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_030_Functionality_of_SaveDraft_button_in_CSM_with_only_Recurring();
	}

	@Test(priority = 25)
	public void TC_031_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_031_Functionality_of_SaveDraft_button_in_CSM_with_message_and_Recurring();
	}

	@Test(priority = 26)
	public void TC_032_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_032_functionality_of_ScheduledMessage_button_in_CSM_with_Reciepient();
	}

	@Test(priority = 27)
	public void TC_033_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_033_functionality_of_ScheduledMessage_button_in_CSM_with_message();
	}

	@Test(priority = 28)
	public void TC_034_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_034_functionality_of_ScheduledMessage_button_in_CSM_with_attachment();
	}

	@Test(priority = 29)
	public void TC_036_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_036_functionality_of_ScheduledMessage_button_in_CSM_with_Reciepient_and_Recurring();
	}

	@Test(priority = 30)
	public void TC_038_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_038_functionality_of_ScheduledMessage_button_with_attachment_recurring();
	}

	@Test(priority = 31)
	public void TC_041_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_041_functionality_of_SaveDraft_button_with_attachments();
	}

	@Test(priority = 32)
	public void TC_042_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_042_functionality_of_SaveDraft_button_in_CSM_with_Reciepient_and_Recurring();
	}

	@Test(priority = 33)
	public void TC_043_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_043_functionality_of_SaveDraft_button_in_CSM_with_message_and_Recurring();
	}

	@Test(priority = 34)
	public void TC_044_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_044_functionality_of_SaveDraft_button_in_CSM_with_Attachment_and_Recurring();
	}

	@Test(priority = 35)
	public void TC_045_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_045_Functionality_of_Breadscrum_link_in_CSM();
	}

	@Test(priority = 36)
	public void TC_046_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_046_Functionality_of_SaveDraft_button_in_Confermation_popup();
	}

	@Test(priority = 37)
	public void TC_047_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_047_Functionality_of_Discard_button_conformation_popup();
	}

	@Test(priority = 38)
	public void TC_048_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_048_Scenario_of_close_popup_by_click_on_outside();
	}

	@Test(priority = 39)
	public void TC_049_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_049_Functionality_of_Breadcrum_link_with_Recipient();
	}

	@Test(priority = 40)
	public void TC_050_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_050_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Recipient();
	}

	@Test(priority = 41)
	public void TC_051_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_051_Functionality_of_Discard_button_in_Confermation_popup_with_Recipient();
	}

	@Test(priority = 42)
	public void TC_052_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_052_Scenario_of_close_popup_by_click_on_outside_with_Recipient();
	}

	@Test(priority = 43)
	public void TC_053_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_053_Functionality_of_Breadcrum_link_with_Message();
	}

	@Test(priority = 44)
	public void TC_054_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_054_Functionality_of_SaveDraft_button_in_Confermation_popup_with_message();
	}

	@Test(priority = 45)
	public void TC_055_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_055_Functionality_of_Discard_button_in_Confermation_popup_with_message();
	}

	@Test(priority = 46)
	public void TC_056_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_056_Scenario_of_close_popup_by_click_on_outside_with_message();
	}

	@Test(priority = 47)
	public void TC_057_and_TC_059_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_057_and_tc_059_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment();
	}

	@Test(priority = 48)
	public void TC_058_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_058_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachments();

	}

	@Test(priority = 49)
	public void TC_060_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_060_Functionality_of_Breadcrum_link_with_Message_Recipient();
	}

	@Test(priority = 50)
	public void TC_061_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_061_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Recipient_message();
	}

	@Test(priority = 51)
	public void TC_062_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_062_Functionality_of_Discard_button_in_Confermation_popup_with_Recipient_Message();
	}

	@Test(priority = 52)
	public void TC_063_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_063_Scenario_of_close_popup_by_click_on_outside_with_Recipient_Message();
	}

	@Test(priority = 53)
	public void TC_064_and_TC_066_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_064_and_tc_066_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment_and_Recipent();
	}

	@Test(priority = 55)
	public void TC_067_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_067_Scenario_of_close_popup_by_click_on_outside_with_message();
	}

	@Test(priority = 56)
	public void TC_068_TC_070_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_068_and_tc_070_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment_message_Recipent();
	}

	@Test(priority = 54)
	public void TC_065_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_065_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachments_Recipient();
	}

	@Test(priority = 57)
	public void TC_069_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_069_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachments_message_Recipient();
	}

	@Test(priority = 58)
	public void TC_071_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_071_Scenario_of_close_popup_by_click_on_outside_with_message_recipient_attachment();
	}

	@Test(priority = 59)
	public void TC_072_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_072_Functionality_of_other_link_link_with_Message();
	}

	@Test(priority = 60)
	public void TC_073_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_073_Functionality_of_SaveDraft_button_in_Confermation_popup_with_message();
	}

	@Test(priority = 61)
	public void TC_074_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_074_Functionality_of_Discard_button_in_Confermation_popup_with_Message();
	}

	@Test(priority = 62)
	public void TC_075_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_075_Scenario_of_close_popup_by_click_on_outside_with_Message();
	}

	@Test(priority = 63)
	public void TC_076_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_076_Functionality_of_other_link_link_with_Recipient();
	}

	@Test(priority = 64)
	public void TC_077_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_077_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Recipient();
	}

	@Test(priority = 65)
	public void TC_078_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_078_Functionality_of_Discard_button_in_Confermation_popup_with_Recipient();
	}

	@Test(priority = 66)
	public void TC_079_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_079_Functionality_of_other_link_link_with_Attachments();
	}

	@Test(priority = 67)
	public void TC_080_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_080_Functionality_of_SaveDraft_button_in_Confermation_popup_with_Attachment();
	}

	@Test(priority = 68)
	public void TC_081_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_081_Functionality_of_Discard_button_in_Confermation_popup_with_Attachment();
	}

	@Test(priority = 69)
	public void TC_082_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_082_Functionality_of_other_link_link_with_AllData();
	}

	@Test(priority = 70)
	public void TC_083_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_083_Functionality_of_SaveDraft_button_in_Confermation_popup_with_AllData();
	}

	@Test(priority = 71)
	public void TC_084_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_084_Functionality_of_Discard_button_in_Confermation_popup_with_AllData();
	}

	@Test(priority = 72)
	public void TC_100_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_100_FeatureTime_Date_Warning_message();
	}

	@Test(priority = 73)
	public void TC_101_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_101_Functionality_of_Deselecting_Recurring_button();
	}

	@Test(priority = 74)
	public void TC_103_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_103_Functionality_of_Recurring_Check_button();
	}

	@Test(priority = 75)
	public void TC_104_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_104_Availability_of_daily_once_recurring_option();
	}

	// not completed //@Test(priority=76)

	public void TC_107_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_107_Verifying_Toster_message_for_recurring_record();
	}

	@Test(priority = 77)
	public void TC_108_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_108_Verifying_UI_error_for_previous_Date_Time();
	}

	@Test(priority = 78)
	public void TC_109_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_109_Verifying_UI_error_for_previous_Date();
	}

	@Test(priority = 79)
	public void TC_111_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_111_Verifying_UI_error_for_previous_Date_Time();
	}

	@Test(priority = 80)
	public void TC_112_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_112_Verifying_UI_error_for_previous_Date();
	}

	@Test(priority = 81)
	public void TC_114_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_114_Verifying_UI_error_for_previous_Date_Time();
	}

	@Test(priority = 82)
	public void TC_115_Tools_SM_Create_page() throws Exception

	{

		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_115_Verifying_UI_error_for_previous_Date();
	}

	@Test(priority = 83)
	public void TC_116_Tools_SM_Create_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_116_verify_error_message_for_contacts_for_TollFree();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
