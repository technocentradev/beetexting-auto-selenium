package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Tools_SM_WFCM;
import com.beetext.qa.Workflows.scheduled.Tools_SM_WF;
import com.beetext.qa.base.TestBase;

public class Tools_SM_Drafts_Testcases extends TestBase {

	@BeforeClass

	public void setUp() throws Exception {
		initialization();

		Tools_SM_WFCM SMCM = new Tools_SM_WFCM();
		SMCM.Login2_Tools_SM();
	}

	// SM Drafts Page Test cases Started

	@Test(priority = 1)

	public void TC_134_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_134_Functionality_of_Delete_button_under_Drafts_list();
	}

	@Test(priority = 2)
	public void TC_135_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_135_Verify_deleted_record_under_Drafts_list();
	}

	@Test(priority = 3)
	public void TC_136_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_136_Functionality_of_Cancel_button_in_conformation_popup();
	}

	@Test(priority = 4)
	public void TC_137_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_137_Functionality_of_Manage_button_under_Drafts_list();
	}

	@Test(priority = 5)
	public void TC_138_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_138_Draft_save_to_Upcoming_list();
	}

	@Test(priority = 6)
	public void TC_139_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_139_Verify_updated_record_under_Drafts_list();
	}

	@Test(priority = 7)
	public void TC_140_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_140_Functionality_of_SaveDraft_button_in_manage_page();
	}

	@Test(priority = 8)
	public void TC_141_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_141_Functionality_of_Breadscrum_link_in_manage_page();
	}

	@Test(priority = 9)
	public void TC_142_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_142_Functionality_of_Save_button_in_conformation_popup();
	}

	@Test(priority = 10)
	public void TC_143_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_143_Functionality_of_Breadscrum_link_in_manage_page();
	}

	@Test(priority = 11)
	public void TC_144_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_144_Functionality_of_Save_button_in_conformation_popup();
	}

	@Test(priority = 12)
	public void TC_145_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_145_Functionality_of_Save_button_in_conformation_popup_for_other_Link();
	}

	@Test(priority = 13)
	public void TC_146_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_146_Functionality_of_Delete_button_in_Draft_manage_page();
	}

	@Test(priority = 14)
	public void TC_147_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_147_Functtionality_by_Click_on_other_Links_in_Draft_manage_page();
	}

	@Test(priority = 15)
	public void TC_148_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_148_Functtionality_of_Save_button_by_Click_on_other_Links();
	}

	@Test(priority = 16)
	public void TC_149_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_149_Functtionality_of_Discard_button_by_Click_on_other_Links();
	}

	@Test(priority = 17)
	public void TC_150_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_150_Functionality_of_other_links_from_manage_page();
	}

	@Test(priority = 18)
	public void TC_151_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_151_Functionality_of_Save_button_in_other_links_conformation_popup();
	}

	@Test(priority = 19)
	public void TC_152_Tools_SM_Drafts_page() throws Exception

	{
		Tools_SM_WF SM_WF = new Tools_SM_WF();
		SM_WF.tc_152_Functionality_of_Discard_button_in_other_links_conformation_popup();
	}

	// Draft cases ended

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
