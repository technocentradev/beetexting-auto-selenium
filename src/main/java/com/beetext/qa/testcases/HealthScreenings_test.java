package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.HealthscreenWFCM;
import com.beetext.qa.Workflows.HealthScreen_WF;
import com.beetext.qa.base.TestBase;

public class HealthScreenings_test extends TestBase {
	Map<String, String> map = new HashMap<String, String>();

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();

		HealthscreenWFCM hs = new HealthscreenWFCM();
		hs.oneAgentLogin(map);
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();

	}

	@Test(priority = 1)
	public void Features_057_tools_HealthScreen_Enabled() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Enabled(map);

	}

	@Test(priority = 2)
	public void Features_059_tools_HealthScreen_options() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_options(map);

	}

	@Test(priority = 3)
	public void Features_061_tools_Create_HealthScreen_Enabled() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_Create_HealthScreen_Enabled(map);

	}

	@Test(priority = 4)
	public void Features_062_tools_HealthScreen_preview_settings_create() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_preview_settings_create(map);

	}

	@Test(priority = 5)
	public void Features_063_tools_HealthScreen_preview() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_preview(map);

	}

	@Test(priority = 6)
	public void Features_064_tools_HealthScreen_settings() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_settings(map);

	}

	@Test(priority = 7)
	public void Features_065_tools_HealthScreen_settings_update() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_settings_update(map);

	}

	@Test(priority = 8)
	public void Features_066_tools_HealthScreen_settings_viewPreview_copytext() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_settings_viewPreview_copytext(map);

	}

	@Test(priority = 9)
	public void Features_067_tools_HS_Settings_CopyText_PasteText() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_Settings_CopyText_PasteText(map);

	}

	@Test(priority = 10)
	public void Features_068_tools_HealthScreen_supporting_Dept_HealthScreen() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_supporting_Dept_HealthScreen(map);

	}

	@Test(priority = 11)
	public void Features_069_tools_HealthScreen_recurring_Message() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_recurring_Message(map);

	}

	@Test(priority = 12)
	public void Features_070_tools_HealthScreen_upcoming_Message() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_upcoming_Message(map);

	}

	@Test(priority = 13)
	public void Features_071_tools_HealthScreen_past_Message() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_past_Message(map);

	}

	@Test(priority = 14)
	public void Features_073_tools_HealthScreen_send_Screen_Enabled() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_send_Screen_Enabled(map);

	}

	@Test(priority = 15)
	public void Features_074_tools_HealthScreen_sendScreen() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_sendScreen(map);

	}

	@Test(priority = 16)
	public void Features_075_76tools_HealthScreen_ScheduleLater_Manage_Send_Screen() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_ScheduleLater_Manage_Send_Scree(map);

	}

	@Test(priority = 17)
	public void Features_077_tools_HealthScreen_upcoming_manage() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_upcoming_manage(map);

	}

	@Test(priority = 18)
	public void Features_078_tools_HealthScreen_Edit_HS_Schedule_HSUpdated() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Edit_HS_Schedule_HSUpdated(map);

	}

	@Test(priority = 19)
	public void Features_079_tools_HealthScreen_upcoming_reuse() throws Exception {

		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_upcoming_reuse(map);

	}

	@Test(priority = 20)
	public void Features_080_tools_HealthScreen_Reuse_HS_Schedule_HSUpdated() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Reuse_HS_Schedule_HSUpdated(map);

	}

	@Test(priority = 21)
	public void Features_081_tools_HealthScreen_upcoming_manage_delete() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_upcoming_manage_delete(map);

	}

	@Test(priority = 22)
	public void Features_082_84_tools_HealthScreen_past_list() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_past_list(map);

	}

	@Test(priority = 23)
	public void Features_083_tools_HS_Past_view() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_Past_view(map);

	}

	@Test(priority = 24)
	public void Features_085_tools_HS_Past_Reuse() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_Past_Reuse(map);

	}

	@Test(priority = 25)
	public void Features_086_tools_HS_Past_Reuse_SendHS() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_Past_Reuse_SendHS(map);

	}

	@Test(priority = 26)
	public void Features_087_tools_Hs_Allows_one_one_Group_Converstion() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_Hs_Allows_one_one_Group_Converstion(map);

	}

	@Test(priority = 27)
	public void Features_072_88_tools_HealthScreen_recurring_manage_reuse() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_recurring_manage_reuse(map);

	}

	@Test(priority = 28)
	public void Features_89_tools_HealthScreen_tags_list() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_tags_list(map);

	}

	@Test(priority = 29)
	public void Features_90_tools_HealthScreen_addtag_deleteTag() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_addtag_deleteTag(map);

	}

	@Test(priority = 30)
	public void Features_91_tools_HealthScreen_addtag_receipentCount() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_addtag_receipentCount(map);

	}

	@Test(priority = 31)
	public void Features_92_tools_HealthScreen_Notag_receipentdisable() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Notag_receipentdisable(map);

	}

	@Test(priority = 32)
	public void Features_93_tools_HealthScreen_addtag_viewRecipientsList() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_addtag_viewRecipientsList(map);

	}

	@Test(priority = 33)
	public void Features_94_tools_HealthScreen_addtag_viewRecipientsList_ClickBack() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_addtag_viewRecipientsList_ClickBack(map);

	}

	@Test(priority = 34)
	public void Features_95_tools_HealthScreen_removetag_receipentdisable() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_removetag_receipentdisable(map);

	}

	@Test(priority = 35)
	public void Features_96_97_tools_HealthScreen_addIndividualRecienpent() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_addIndividualRecienpent(map);

	}

	@Test(priority = 36)
	public void Features_98_tools_HealthScreen_addIndividualRecienpent_Count() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_addIndividualRecienpent_Count(map);

	}

	@Test(priority = 37)
	public void Features_99_tools_HealthScreen_addTag_Individual_Number_Recienpent_Count() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_addTag_Individual_Number_Recienpent_Count(map);

	}

	@Test(priority = 38)
	public void Features_100_tools_HealthScreen_Recurring_dateTime() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Recurring_dateTime(map);

	}

	@Test(priority = 39)
	public void Features_101_tools_HealthScreen_Send_Schedule() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Send_Schedule(map);

	}

	@Test(priority = 40)
	public void Features_102_tools_HealthScreen_ScheduleHS() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_ScheduleHS(map);

	}

	@Test(priority = 41)
	public void Features_103_tools_HS_recurring_Manage_Save() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_recurring_Manage_Save(map);

	}

	@Test(priority = 42)
	public void Features_104_tools_HealthScreen_view_Preview() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_view_Preview(map);

	}

	@Test(priority = 44)
	public void Features_109_tools_HealthScreen_tag_Contact_separeted() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_tag_Contact_separeted(map);

	}

	@Test(priority = 45)
	public void Features_110_tools_HealthScreen_tag() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_tag(map);

	}

	@Test(priority = 46)
	public void Features_112_tools_HS_Allows_Optout_feature() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_Allows_Optout_feature(map);

	}

	@Test(priority = 47)
	public void Features_115_116_tools_HealthScreen_Button_inChat() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Button_inChat(map);

	}

	@Test(priority = 48)
	public void Features_118_119_tools_HS_actionBttn_Disabled() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_actionBttn_Disabled(map);

	}

	@Test(priority = 49)
	public void Features_120_122_123_124_125_126_127_tools_HealthScreen_Create_yes() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Create_yes(map);

	}

	@Test(priority = 50)
	public void Features_121_tools_HS_Create_NO() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_Create_NO(map);

	}

	@Test(priority = 51)
	public void Features_128_129_130_131_134_tools_HealthScreen_Create_48Hrs_NO() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Create_48Hrs_NO(map);

	}

	@Test(priority = 52)
	public void Features_132_133_135_136_137_140tools_HealthScreen_Create_48Hrs_14Days_NO() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_Create_48Hrs_14Days_NO(map);

	}

	@Test(priority = 53)
	public void Features_138_139_141_tools_HS_waiting_For_CovidResults() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_waiting_For_CovidResults(map);

	}

	@Test(priority = 54)
	public void Features_142_tools_HS_NO_Pass_Case() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_NO_Pass_Case(map);

	}

	@Test(priority = 55)
	public void Features_145_nolimit_contacts() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.nolimit_contacts(map);

	}

	@Test(priority = 56)
	public void Features_147_tools_HS_fail_Case() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_fail_Case(map);

	}

	@Test(priority = 57)
	public void Features_148_150_tools_HS_report_Pass_Case() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_report_Pass_Case(map);

	}

	@Test(priority = 58)
	public void Features_153_HS_Icon_TextMsg_InChatBox() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.HS_Icon_TextMsg_InChatBox(map);

	}

	@Test(priority = 59)
	public void Features_154_155_Send_Screen_3Invalid_answers() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.Send_Screen_3Invalid_answers(map);

	}

	@Test(priority = 60)
	public void Features_158_Hs_recurring_unselect_currentDate_Error() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.Hs_recurring_unselect_currentDate_Error(map);

	}

	@Test(priority = 61)
	public void Features_159_Hs_wrongAnswer_Accepts_Yes_NO_help() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.Hs_wrongAnswer_Accepts_Yes_NO_help(map);

	}

	@Test(priority = 62)
	public void Features_160_Hs_AdhocRequest_secondMsg_reply_YourName() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.Hs_AdhocRequest_secondMsg_reply_YourName(map);

	}

	@Test(priority = 63)
	public void Features_161_Hs_AdhocRequest_Respondwrong_Give_YourName() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.Hs_AdhocRequest_Respondwrong_Give_YourName(map);

	}

	@Test(priority = 64)
	public void Features_162_HS_Reports() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.HS_Reports(map);

	}

	@Test(priority = 65)
	public void Features_164_HS_System_Stamp() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.HS_System_Stamp(map);

	}

	@Test(priority = 66)
	public void Features_165_166_HS_Report_Contact() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.HS_Report_Contact(map);

	}

	@Test(priority = 67)
	public void Features_167_168_HS_Contact_US_Format() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.HS_Contact_US_Format(map);

	}

	@Test(priority = 68)
	public void Features_169_HS_Report_agent_Stamp() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.HS_Report_agent_Stamp(map);

	}

	@Test(priority = 69)
	public void Features_171_Settings_roles_Admin_HS() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.Settings_roles_Admin_HS(map);

	}

	@Test(priority = 70)
	public void Features_172_tools_HS_sendScreen() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HS_sendScreen(map);

	}

	@Test(priority = 71)
	public void Features_173_HS_Icon_TextMsg_InChatBox_retry() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.HS_Icon_TextMsg_InChatBox_retry(map);

	}

	// @Test(priority = 43)//don't run
	public void Features_105_106_107_tools_HealthScreen_activate_back() throws Exception {
		HealthScreen_WF healthscreen_WF = new HealthScreen_WF();
		healthscreen_WF.tools_HealthScreen_activate_back(map);

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
