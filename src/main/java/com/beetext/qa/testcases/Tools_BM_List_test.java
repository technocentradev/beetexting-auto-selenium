package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.Workflows.broadcast.Tools_BM_List_WF;
import com.beetext.qa.base.TestBase;

public class Tools_BM_List_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {

		initialization();
		Broadcast BM = new Broadcast();
		BM.BM_tools_login();
	}

	@Test(priority = 1)
	public void tools_377_tools_BM_list_BM_upcoming_past_draft_enabled() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_BM_upcoming_past_draft_enabled();
	}

	@Test(priority = 2)
	public void tools_378_tools_BM_list_Display_list() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_Display_list();

	}

	@Test(priority = 4)
	public void tools_380_tools_BM_list_upcoming_Display_manage_Reuse_options() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_Display_manage_Reuse_options();

	}

	@Test(priority = 5)
	public void tools_381_tools_BM_list_Past_Display_view_Reuse_options() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_Past_Display_view_Reuse_options();

	}

	@Test(priority = 6)
	public void tools_382_tools_BM_list_Draft_Display_Manage_Delete_options() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_Draft_Display_Manage_Delete_options();

	}

	@Test(priority = 7)
	public void tools_383_tools_BM_list_upcoming_listwith_RecurringIcon() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_listwith_RecurringIcon();

	}

	@Test(priority = 8)
	public void tools_384_tools_BM_list_upcoming_list() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_list();

	}

	@Test(priority = 9)
	public void tools_385_tools_BM_list_upcoming_clickOn_manage() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_clickOn_manage();

	}

	@Test(priority = 10)
	public void tools_386_tools_BM_list_upcoming_clickOn_manage_changeAnyField_SendBroadCast() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_clickOn_manage_changeAnyField_SendBroadCast();

	}

	@Test(priority = 11)
	public void tools_387_tools_BM_list_upcoming_clickOn_manage_removeTag_AddNewTag_SendBroadCast() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_clickOn_manage_removeTag_AddNewTag_SendBroadCast();

	}

	@Test(priority = 12)
	public void tools_388_tools_BM_page_List_createBM_Recurring_Manage_withoutRecurring() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_List_createBM_Recurring_Manage_withoutRecurring();

	}

	@Test(priority = 13)
	public void tools_389_tools_BM_page_List_createBM_withoutRecurring_Manage_withRecurring() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_List_createBM_withoutRecurring_Manage_withRecurring();

	}

	@Test(priority = 14)
	public void tools_390_tools_BM_list_upcoming_clickOn_manage_DeleteBM() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_clickOn_manage_DeleteBM();
	}

	@Test(priority = 15)
	public void tools_391_tools_BM_list_upcoming_clickOn_manage_Save_As_Draft() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_clickOn_manage_Save_As_Draft();

	}

	@Test(priority = 16)
	public void tools_392_tools_BM_list_upcoming_clickOn_manage_addTwoTags_ScheduleLater() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_clickOn_manage_addTwoTags_ScheduleLater();

	}

	@Test(priority = 18)
	public void tools_394_tools_BM_list_upcoming_clickOn_manage_RemoveMsg_addAttachment() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_upcoming_clickOn_manage_RemoveMsg_addAttachment();

	}

	@Test(priority = 19)
	public void tools_395_396_tools_BM_page_Upcoming_manage_Click_onSM_PopupOpens_Click_onsave() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onSM_PopupOpens_Click_onsave();

	}

	@Test(priority = 20)
	public void tools_397_tools_BM_page_Upcoming_manage_Click_onSM_PopupOpens_Click_onDiscard() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onSM_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 21)
	public void tools_398_399_tools_BM_page_Upcoming_manage_Click_on_Templates_PopupOpens_Click_onSave()
			throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_on_Templates_PopupOpens_Click_onSave();

	}

	@Test(priority = 22)
	public void tools_400_tools_BM_page_Upcoming_manage_Click_onTemplates_PopupOpens_Click_onDiscard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onTemplates_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 23)
	public void tools_401_402_tools_BM_page_Upcoming_manage_changeMsg_ScheduleLater_Click_on_SM_PopupOpens_Click_onSave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_changeMsg_ScheduleLater_Click_on_SM_PopupOpens_Click_onSave();

	}

	@Test(priority = 24)
	public void tools_403_404_tools_BM_page_Upcoming_manage_Click_onTags_PopupOpens_Click_onsave() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onTags_PopupOpens_Click_onsave();

	}

	@Test(priority = 25)
	public void tools_405_tools_BM_page_Upcoming_manage_Click_oTags_PopupOpens_Click_onDiscard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_oTags_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 26)
	public void tools_406_407_tools_BM_page_Upcoming_manage_Click_onAutomations_PopupOpens_Click_onsave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onAutomations_PopupOpens_Click_onsave();

	}

	@Test(priority = 27)
	public void tools_408_tools_BM_page_Upcoming_manage_Click_onAutomations_PopupOpens_Click_onDiscard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onAutomations_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 28)
	public void tools_409_410_tools_BM_page_Upcoming_manage_Click_onAdvocate_PopupOpens_Click_onsave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onAdvocate_PopupOpens_Click_onsave();

	}

	@Test(priority = 29)
	public void tools_411_tools_BM_page_Upcoming_manage_Click_onAdvocate_PopupOpens_Click_onDiscard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onAdvocate_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 30)
	public void tools_412_413_tools_BM_page_Upcoming_manage_Click_onReviews_PopupOpens_Click_onsave() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onReviews_PopupOpens_Click_onsave();

	}

	@Test(priority = 31)
	public void tools_414_tools_BM_page_Upcoming_manage_Click_onReviews_PopupOpens_Click_onDiscard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onReviews_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 32)
	public void tools_415_416_tools_BM_page_Upcoming_manage_Click_onHealth_Screeings_PopupOpens_Click_onsave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onHealth_Screeings_PopupOpens_Click_onsave();

	}

	@Test(priority = 33)
	public void tools_417_tools_BM_page_Upcoming_manage_Click_onHealth_Screenings_PopupOpens_Click_onDiscard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onHealth_Screenings_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 34)
	public void tools_418_419_tools_BM_page_Upcoming_manage_change_title_msg_click_outside_PopupOpens_Click_onSave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_change_title_msg_click_outside_PopupOpens_Click_onSave();

	}

	@Test(priority = 35)
	public void tools_420_tools_BM_page_Upcoming_manage_Click_onOutside_PopupOpens_Click_onDiscard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_Click_onOutside_PopupOpens_Click_onDiscard();

	}

	@Test(priority = 37)
	public void tools_424_tools_BM_page_Upcoming_manage_AddAttachmentChange_DateAndTime_Click_onOutside_PopupOpens_Click_onDiscard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF
				.tools_BM_page_Upcoming_manage_AddAttachmentChange_DateAndTime_Click_onOutside_PopupOpens_Click_onDiscard();
	}

	@Test(priority = 38)
	public void tools_425__426_tools_BM_page_Upcoming_manage_change_title_msg_click_BMLink_PopupOpens_Click_onSave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_change_title_msg_click_BMLink_PopupOpens_Click_onSave();
	}

	@Test(priority = 39)
	public void tools_427_tools_BM_page_Upcoming_manage_change_title_msg_click_BMLink_PopupOpens_Click_onDiscard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_change_title_msg_click_BMLink_PopupOpens_Click_onDiscard();
	}

	@Test(priority = 40)
	public void tools_428_429tools_BM_page_Upcoming_manage_change_title_IMG_weekly_ScheduleLater_click_BMLink_PopupOpens_Click_onSave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF
				.tools_BM_page_Upcoming_manage_change_title_IMG_ScheduleLater_click_BMLink_PopupOpens_Click_onSave();
	}

	@Test(priority = 41)
	public void tools_431_tools_BM_page_Upcoming_manage_change_title_IMG_ScheduleLater_click_BMLink_PopupOpens_Click_onDiscard()
			throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF
				.tools_BM_page_Upcoming_manage_change_title_IMG_ScheduleLater_click_BMLink_PopupOpens_Click_onDiscard();
	}

	@Test(priority = 42)
	public void tools_432_tools_BM_page_Upcoming_Reuse_BM_page() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_page();
	}

	@Test(priority = 43)
	public void tools_433_tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow();
	}

	@Test(priority = 44)
	public void tools_434_tools_BM_page_Upcoming_Reuse_BM_cancel() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_cancel();
	}

	@Test(priority = 45)
	public void tools_435_tools_BM_page_Upcoming_reuse_change_title_IMG_monthlyScheduleLater_click_BMLink_PopupOpens_Click_onSave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF
				.tools_BM_page_Upcoming_reuse_change_title_IMG_monthlyScheduleLater_click_BMLink_PopupOpens_Click_onSave();
	}

	@Test(priority = 46)
	public void tools_437_tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow_() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow_();
	}

	@Test(priority = 47)
	public void tools_438_tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow_addAttachment() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_changeTitle_tag_sendNow_addAttachment();
	}

	@Test(priority = 48)
	public void tools_439_440_tools_BM_page_Upcoming_Reuse_BM_dont_change_anyFields_click_outside_Save_As_Draft()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_dont_change_anyFields_click_outside_Save_As_Draft();
	}

	@Test(priority = 49)
	public void tools_441_tools_BM_page_Upcoming_Reuse_BM_dont_change_anyFields_click_outside_Save_As_Discard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_dont_change_anyFields_click_outside_Save_As_Discard();
	}

	@Test(priority = 50)
	public void tools_442_443_tools_BM_page_Upcoming_Reuse_BM_change_title_click_outside_Save_As_Draftd()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_outside_Save_As_Draft();
	}

	@Test(priority = 51)
	public void tools_444_tools_BM_page_Upcoming_Reuse_BM_change_title_click_outside_Save_As_Discard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_outside_Save_As_Discard();
	}

	@Test(priority = 52)
	public void tools_445_446_tools_BM_page_Upcoming_Reuse_BM_change_title_click_SM_Save_As_Draft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();

		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_SM_Save_As_Draft();
	}

	@Test(priority = 53)
	public void tools_447_tools_BM_page_Upcoming_Reuse_BM_change_title_click_SM_Save_As_Discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_SM_Save_As_Discard();
	}

	@Test(priority = 54)
	public void tools_448_449_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Templates_Save_As_Draft()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Templates_Save_As_Draft();
	}

	@Test(priority = 55)
	public void tools_450_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Templates_Save_As_Discard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Templates_Save_As_Discard();
	}

	@Test(priority = 56)
	public void tools_451_452_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Tags_Save_As_Draft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Tags_Save_As_Draft();
	}

	@Test(priority = 57)
	public void tools_453_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Tags_Save_As_Discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Tags_Save_As_Discard();
	}

	@Test(priority = 58)
	public void tools_454_455_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Automations_Save_As_Draft()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Automations_Save_As_Draft();
	}

	@Test(priority = 59)
	public void tools_456_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Automations_Save_As_Discard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Automations_Save_As_Discard();
	}

	@Test(priority = 60)
	public void tools_457_458_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Advocate_Save_As_Draft()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Advocate_Save_As_Draft();
	}

	@Test(priority = 61)
	public void tools_459_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Advocate_Save_As_Discard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Advocate_Save_As_Discard();
	}

	@Test(priority = 62)
	public void tools_460_461_tools_BM_page_Upcoming_Reuse_BM_change_title_click_review_Save_As_Draft()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_review_Save_As_Draft();
	}

	@Test(priority = 63)
	public void tools_462_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Review_Save_As_Discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Review_Save_As_Discard();
	}

	@Test(priority = 64)
	public void tools_463_464_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Health_Screenings_Save_As_Draft()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Health_Screenings_Save_As_Draft();
	}

	@Test(priority = 65)
	public void tools_465_tools_BM_page_Upcoming_Reuse_BM_change_title_click_Health_Screenings_Save_As_Discard()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_change_title_click_Health_Screenings_Save_As_Discard();
	}

	@Test(priority = 66)
	public void tools_466_467_tools_BM_page_Upcoming_Manage_BM_click_Navigation_link_Save_As_Draft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Manage_BM_click_Navigation_link_Save_As_Draft();
	}

	@Test(priority = 67)
	public void tools_468_tools_BM_page_Upcoming_manage_BM_click_navigationLink_Save_As_Discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_manage_BM_click_navigationLink_Save_As_Discard();
	}

	@Test(priority = 68)
	public void tools_469_470_tools_BM_page_Upcoming_Reuse_BM_click_Navigation_link_Save_As_Draft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_click_Navigation_link_Save_As_Draft();
	}

	@Test(priority = 69)
	public void tools_471_tools_BM_page_Upcoming_Reuse_BM_click_navigationLink_Save_As_Discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Upcoming_Reuse_BM_click_navigationLink_Save_As_Discard();
	}

	@Test(priority = 70)
	public void tools_472_tools_BM_page_Past_display_list_of_pastMsg() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_display_list_of_pastMsg();
	}

	@Test(priority = 71)
	public void tools_473_tools_BM_page_Past_click_on_view_opens_viewPage() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_click_on_view_opens_viewPage();
	}

	@Test(priority = 72)
	public void tools_474_tools_BM_page_Past_view_reuse_send() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_send();
	}

	@Test(priority = 73)
	public void tools_475_tools_BM_page_Past_view_reuse_UpdateInBm_send() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_UpdateInBm_send();
	}

	@Test(priority = 74)
	public void tools_476_tools_BM_page_Past_view_reuse_cancel() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_cancel();
	}

	@Test(priority = 75)
	public void tools_477_tools_BM_page_Past_view_reuse_UpdateInBm_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_UpdateInBm_saveAsDraft();
	}

	@Test(priority = 76)
	public void tools_478_479_tools_BM_page_Past_view_reuse_click_navigationLink_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_navigationLink_saveAsDraft();
	}

	@Test(priority = 77)
	public void tools_480_tools_BM_page_Past_view_reuse_click_navigationLink_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_navigationLink_discard();
	}

	@Test(priority = 78)
	public void tools_481_482_tools_BM_page_Past_view_reuse_click_outside_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_outside_saveAsDraft();
	}

	@Test(priority = 79)
	public void tools_483_tools_BM_page_Past_view_reuse_click_outside_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_outside_discard();
	}

	@Test(priority = 80)
	public void tools_484_485_tools_BM_page_Past_view_reuse_click_SM_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_SM_saveAsDraft();
	}

	@Test(priority = 81)
	public void tools_486_tools_BM_page_Past_view_reuse_click_SM_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_SM_discard();
	}

	@Test(priority = 82)
	public void tools_487_488_tools_BM_page_Past_view_reuse_click_Templates_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Templates_saveAsDraft();
	}

	@Test(priority = 83)
	public void tools_489_tools_BM_page_Past_view_reuse_click_Templates_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Templates_discard();
	}

	@Test(priority = 84)
	public void tools_490_491_tools_BM_page_Past_view_reuse_click_Tags_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Tags_saveAsDraft();
	}

	@Test(priority = 85)
	public void tools_492_tools_BM_page_Past_view_reuse_click_Tags_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Tags_discard();
	}

	@Test(priority = 86)
	public void tools_493_494_tools_BM_page_Past_view_reuse_click_Automations_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Automations_saveAsDraft();
	}

	@Test(priority = 87)
	public void tools_495_tools_BM_page_Past_view_reuse_click_Automations_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Automations_discard();
	}

	@Test(priority = 88)
	public void tools_496_497_tools_BM_page_Past_view_reuse_click_Advocate_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Advocate_saveAsDraft();
	}

	@Test(priority = 89)
	public void tools_498_tools_BM_page_Past_view_reuse_click_Advocate_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Advocate_discard();
	}

	@Test(priority = 90)
	public void tools_499_500_tools_BM_page_Past_view_reuse_click_Review_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Review_saveAsDraft();
	}

	@Test(priority = 91)
	public void tools_501_tools_BM_page_Past_view_reuse_click_Review_discard() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_Review_saveAsDraft();
	}

	@Test(priority = 92)
	public void tools_502_503_tools_BM_page_Past_view_reuse_click_HealthScreenings_saveAsDraft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();

		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_HealthScreenings_saveAsDraft();
	}

	@Test(priority = 93)
	public void tools_504_tools_BM_page_Past_view_reuse_click_healthScreenings_discard() throws Exception {

		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_page_Past_view_reuse_click_healthScreenings_discard();
	}

	@Test(priority = 94)
	public void tools_379_tools_BM_list_SelectDisplay_Corresponding_Upcoming_Past_draft() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_SelectDisplay_Corresponding_Upcoming_Past_draft();

	}

	@Test(priority = 95)
	public void tools_393_tools_BM_list_scheduleTime_completed_notShown_inUpcoming() throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF.tools_BM_list_scheduleTime_completed_notShown_inUpcoming();

	}

	@Test(priority = 96)
	public void tools_421_422_423_tools_BM_page_Upcoming_manage_AddAttachment_Change_DateAndTime_click_outside_PopupOpens_Click_onSave()
			throws Exception {
		Tools_BM_List_WF Tools_BM_List_WF = new Tools_BM_List_WF();
		Tools_BM_List_WF
				.tools_BM_page_Upcoming_manage_AddAttachment_Change_DateAndTime_click_outside_PopupOpens_Click_onSave();

	}
	// Total Count is 93 only

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
