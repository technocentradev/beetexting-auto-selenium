package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.tags.Tools_TagsWF;
import com.beetext.qa.base.TestBase;

public class Tools_Tags_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Tools_TagsWF tagswf = new Tools_TagsWF();
		tagswf.tags_login();
	}

	@Test(priority = 1)
	public void tools_788_tools_tags_clickon_createtag() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag();
	}

	@Test(priority = 2)
	public void tools_789_tools_tags_clickon_createtag_createdisabled() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_createdisabled();
	}

	@Test(priority = 3)
	public void tools_790_tools_tags_clickon_createtag_taglimit_30char_errormsg() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_taglimit_30char_errormsg();
	}

	@Test(priority = 4)
	public void tools_791_tools_tags_clickon_createtag_duplicatetag() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_duplicatetag();
	}

	@Test(priority = 5)
	public void tools_792_tools_tags_clickon_createtag_spaecialChar_allow() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_spaecialChar_allow();
	}

	@Test(priority = 6)
	public void tools_793_tools_tags_clickon_createtag_count() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_count();
	}

//	@Test(priority = 7)
//	public void tools_794_tools_tags_clickon_createtag_applyall() throws Exception {
//
//		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
//		tools_tagsWF.tools_tags_clickon_createtag_applyall();
//	}

	@Test(priority = 8)
	public void tools_795_tools_tags_clickon_createtag_removeall() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_removeall();
	}

	@Test(priority = 9)
	public void tools_796_tools_tags_clickon_createtag_searchContact_displaySearchresults() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_searchContact_displaySearchresults();
	}

	@Test(priority = 10)
	public void tools_797_tools_tags_clickon_createtag_searchcontact_NoContactFound() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_searchcontact_NoContactFound();
	}

	@Test(priority = 11)
	public void tools_798_tools_tags_createtag() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_createtag();
	}

	@Test(priority = 12)
	public void tools_799_tools_tags_clickon_createtag_applyall_nolimit_forselection() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_applyall_nolimit_forselection();
	}

	@Test(priority = 13)
	public void tools_800_tools_tags_clickon_createtag_selectcontact_manually_automatically_applyorremove()
			throws Exception {
		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_selectcontact_manually_automatically_applyorremove();
	}

	@Test(priority = 14)
	public void tools_801_tools_tags_clickon_createtag_searchContact_applyallcontacts() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_searchContact_applyallcontacts();

	}

	@Test(priority = 15)
	public void tools_802_tools_tags_clickon_createtag_searchContacts_selectContact() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_searchContacts_selectContact();
	}

	@Test(priority = 16)
	public void tools_803_tools_tags_clickon_createtag_searchContacts_select_applyall() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_searchContacts_select_applyall();
	}

	@Test(priority = 17)
	public void tools_804_tools_tags_clickon_createtag_createnewtag_redirecttotagPage() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_clickon_createtag_createnewtag_redirecttotagPage();
	}

	@Test(priority = 19)
	public void tools_806_tools_tags_List_listofTags() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_List_listofTags();
	}

	@Test(priority = 20)
	public void tools_807_tools_tags_List_clickonTag_toEdit() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_List_clickonTag_toEdit();
	}

	@Test(priority = 21)
	public void tools_808_tools_tags_EditTag_Taglimit_30char() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_EditTag_Taglimit_30char();
	}

	@Test(priority = 22)
	public void tools_809_tools_tags_edittag_duplicatetag() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_edittag_duplicatetag();
	}

	@Test(priority = 23)
	public void tools_810_tools_tags_edittag_with_specialChar() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_edittag_with_specialChar();
	}

	@Test(priority = 24)
	public void tools_811_tools_tags_edittag_selectContact_ContactsCount() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_edittag_selectContact_ContactsCount();
	}

//	@Test(priority = 25)
//	public void tools_812_813_tools_tags_Edittag_save_Selct_AllContacts() throws Exception {
//
//		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
//		tools_tagsWF.tools_tags_Edittag_save_Selct_AllContacts();
//	}
//
//	@Test(priority = 26)
//	public void tools_814tools_tags_editTag_Donot_selectcontact_errormsg() throws Exception {
//
//		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
//		tools_tagsWF.tools_tags_editTag_Donot_selectcontact_errormsg();
//	}

	@Test(priority = 27)
	public void tools_815_tools_tags_editTag_select_allContacts() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_select_allContacts();
	}

	@Test(priority = 28)
	public void tools_816_tools_tags_editTag_select_allContacts_unselectContact_applyDisable() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_select_allContacts_unselectContact_applyDisable();
	}

	@Test(priority = 29)
	public void tools_817_tools_tags_editTag_select_allContacts_automatically_applyEnable() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_select_allContacts_automatically_applyEnable();
	}

	@Test(priority = 30)
	public void tools_818_tools_tags_editTag_searchContact_clickonapply() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_searchContact_clickonapply();
	}

	@Test(priority = 31)
	public void tools_819_tools_tags_editTag_searchContact_selectOneContact() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_searchContact_selectOneContact();
	}

	@Test(priority = 32)
	public void tools_820_tools_tags_editTag_selectcontact_manually_automaticallyselect() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_selectcontact_manually_automaticallyselect();
	}

	@Test(priority = 33)
	public void tools_821_tools_tags_editTag_searchContact_noContactsFound() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_searchContact_noContactsFound();
	}

	@Test(priority = 34)
	public void tools_822_823_tools_tags_editTag_delete_confirmDeleteText_confirmDelete() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_delete_confirmDeleteText_confirmDelete();

	}

	@Test(priority = 35)
	public void tools_824_tools_tags_editTag_seletedContact_onTop() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_editTag_seletedContact_onTop();

	}

	@Test(priority = 36)
	public void tools_805_tools_tags__list_createyour_firstTag() throws Exception {

		Tools_TagsWF tools_tagsWF = new Tools_TagsWF();
		tools_tagsWF.tools_tags_list_createyour_firstTag();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
