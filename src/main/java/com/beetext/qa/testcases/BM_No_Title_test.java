package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.BM_Optout_WFCM;
import com.beetext.qa.Workflows.broadcast.BM_No_Title_WF;
import com.beetext.qa.base.TestBase;

public class BM_No_Title_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		BM_Optout_WFCM wfcm = new BM_Optout_WFCM();
		wfcm.BM_tools_login_automations();
	}

	@Test(priority = 1)
	public void BM_NoTitle_01_createBM_with_NoTitle_Past_noTitle() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.createBM_with_NoTitle_Past_noTitle();

	}

	@Test(priority = 2)
	public void BM_NoTitle_02_createBM_ScheduleLater_with_NoTitle_Past_noTitle() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.createBM_ScheduleLater_with_NoTitle_Past_noTitle();

	}

	@Test(priority = 3)
	public void BM_NoTitle_03_past_View_Display_View_Page() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.past_View_Display_View_Page();

	}

	@Test(priority = 4)
	public void BM_NoTitle_04_past_Reuse_NoTitleBM_AddTitle_SendBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.past_Reuse_NoTitleBM_AddTitle_SendBM();

	}

	@Test(priority = 5)
	public void BM_NoTitle_05_past_Reuse_NoTitleBM_AddTitle_ScheduleBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.past_Reuse_NoTitleBM_AddTitle_ScheduleBM();

	}

	@Test(priority = 6)
	public void BM_NoTitle_06_create_DraftBM_with_NoTitle_Past_noTitle() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.create_DraftBM_with_NoTitle_Past_noTitle();

	}

	@Test(priority = 7)
	public void BM_NoTitle_07_create_DraftBM_ScheduleLater_with_NoTitle_Past_noTitle() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.create_DraftBM_ScheduleLater_with_NoTitle_Past_noTitle();

	}

	@Test(priority = 8)
	public void BM_NoTitle_08_createBM_with_Title() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.createBM_with_Title();

	}

	@Test(priority = 9)
	public void BM_NoTitle_09_past_Reuse_clearTitle_SendBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.past_Reuse_clearTitle_SendBM();

	}

	@Test(priority = 10)
	public void BM_NoTitle_10_createBM_with_Title_ScheduleBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.createBM_with_Title_ScheduleBM();

	}

	@Test(priority = 11)
	public void BM_NoTitle_11_Upcoming_Manage_ClearTitle_ScheduleBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.Upcoming_Manage_ClearTitle_ScheduleBM();

	}

	@Test(priority = 12)
	public void BM_NoTitle_12_create_DraftBM_with_Title_SendBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.create_DraftBM_with_Title_SendBM();

	}

	@Test(priority = 13)
	public void BM_NoTitle_13_Draft_Manage_ClearTitle_SendBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.Draft_Manage_ClearTitle_SendBM();

	}

	@Test(priority = 14)
	public void BM_NoTitle_14_create_DraftBM_with_Title_ScheduleBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.create_DraftBM_with_Title_ScheduleBM();

	}

	@Test(priority = 15)
	public void BM_NoTitle_15_Draft_Manage_ClearTitle_ScheduleBM() throws Exception {

		BM_No_Title_WF BMNoTitle_WF = new BM_No_Title_WF();
		BMNoTitle_WF.Draft_Manage_ClearTitle_ScheduleBM();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
