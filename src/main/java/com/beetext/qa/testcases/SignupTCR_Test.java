package com.beetext.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.signup.SignupTCR_WF;
import com.beetext.qa.base.TestBase;

public class SignupTCR_Test extends TestBase {

	@BeforeMethod
	public void setUp() throws Exception {

		initialization();
	}

	@Test(priority = 1)
	public void Tcr_1_tcr_Message() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_Message();
	}

	@Test(priority = 2)
	public void Tcr_2_tcr_Broadcast() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_Broadcast();
	}

	@Test(priority = 3)
	public void Tcr_3_tcr_Schedule_Message() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_Schedule_Message();
	}

	@Test(priority = 4)
	public void Tcr_4_tcr_Health_Screening() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_Health_Screening();
	}

	@Test(priority = 5)
	public void Tcr_5_tcr_After_Submitting_Details_Message() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_After_Submitting_Details_Message();
	}

	@Test(priority = 6)
	public void Tcr_6_tcr_After_Submitting_Details_Broadcast() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_After_Submitting_Details_Broadcast();
	}

	@Test(priority = 7)
	public void Tcr_7_tcr_After_Submitting_Details_Schedule_Message() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_After_Submitting_Details_Schedule_Message();
	}

	@Test(priority = 8)
	public void Tcr_8_tcr_After_Submitting_Details_Health_Screening() throws Exception {

		SignupTCR_WF tcr_WF = new SignupTCR_WF();
		tcr_WF.tcr_After_Submitting_Details_Health_Screening();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
