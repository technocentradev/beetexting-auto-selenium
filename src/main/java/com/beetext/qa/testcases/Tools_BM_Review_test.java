package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Broadcast;
import com.beetext.qa.Workflows.broadcast.Tools_BM_Review_WF;
import com.beetext.qa.base.TestBase;

public class Tools_BM_Review_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {

		initialization();
		Broadcast BM = new Broadcast();
		BM.BM_tools_login();
		BM.CreateReview_For_BM();
	}

	@Test(priority = 1)
	public void tools_BM_R_001_tools_BM_Click_On_Review_ReviewMsg_updated() throws Exception {

		Tools_BM_Review_WF tools_BM_Review_WF = new Tools_BM_Review_WF();
		tools_BM_Review_WF.tools_BM_Click_On_Review_ReviewMsg_updated();

	}

	@Test(priority = 2)
	public void tools_BM_R_002_tools_BM_upcoming_Click_On_Review_ReviewMsg_updated() throws Exception {

		Tools_BM_Review_WF tools_BM_Review_WF = new Tools_BM_Review_WF();
		tools_BM_Review_WF.tools_BM_upcoming_Click_On_Review_ReviewMsg_updated();

	}

	@Test(priority = 3)
	public void tools_BM_R_003_tools_BM_upcoming_already_msg_ReviewMsg_icons_disabled() throws Exception {

		Tools_BM_Review_WF tools_BM_Review_WF = new Tools_BM_Review_WF();
		tools_BM_Review_WF.tools_BM_upcoming_already_msg_ReviewMsg_icons_disabled();

	}

	@Test(priority = 4)
	public void tools_BM_R_004_tools_BM_past_Click_On_Review_ReviewMsg_updated_icons_Disabled() throws Exception {

		Tools_BM_Review_WF tools_BM_Review_WF = new Tools_BM_Review_WF();
		tools_BM_Review_WF.tools_BM_past_Click_On_Review_ReviewMsg_updated_icons_Disabled();

	}

	@Test(priority = 5)
	public void tools_BM_R_005_tools_BM_past_already_msg_ReviewMsg_icons_disabled() throws Exception {

		Tools_BM_Review_WF tools_BM_Review_WF = new Tools_BM_Review_WF();
		tools_BM_Review_WF.tools_BM_past_already_msg_ReviewMsg_icons_disabled();

	}

	@Test(priority = 6)
	public void tools_BM_R_006_tools_BM_SaveAsDraft_already_msg_ReviewMsg_icons_disabled() throws Exception {

		Tools_BM_Review_WF tools_BM_Review_WF = new Tools_BM_Review_WF();
		tools_BM_Review_WF.tools_BM_SaveAsDraft_already_msg_ReviewMsg_icons_disabled();

	}

	@AfterClass
	public void tearDown() throws Exception {
		Broadcast BM = new Broadcast();
		BM.Review_Delete();
		driver.quit();
	}
}
