package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.scheduled.Schedule_MessageWF;
import com.beetext.qa.base.TestBase;

public class Schedule_Message extends TestBase {
	

	@BeforeClass
	public void setUp() {
		initialization();
		Login(); // for login

	}

	@Test(priority = 1)
	public void Tc_199_existing_group_conversation_addattachments_textmessage() throws Exception {

		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.create_exisitng_group_Schedule_Messae_add_attachments();

	}

	@Test(priority = 2)
	public void message_199_create_Schdule_Message() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.create_Schedule_Message();

	}

	@Test(priority = 3)
	public void message_195_create_Schdule_Message() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.create_compose_new_Schedule_Message();

	}

	@Test(priority = 4)
	public void message_196_create_Schdule_Message() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.create_compose_new_group_Schedule_Message();

	}

	@Test(priority = 5)
	public void Tc_196_composenew_one_one_conversation_addattachments_textmessage() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.create_compose_new_Schedule_Message_add_attachments();

	}

	@Test(priority = 6)
	public void Tc_197_composenew_group_conversation_addattachments_textmessage() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.create_compose_new_group_Schedule_Message_add_attachments();

	}

	@Test(priority = 7)
	public void Tc_200_onetoone_exisitng_schedulemessage_creation() throws Exception {

		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.create_exisitng_onetoone_Schedule_Messae_add_attachments();

	}

//30-11-21

	@Test(priority = 8)
	public void Tc_newcase_composenew_add9contacts_schedulemessage_creation() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.composenew_add_maxlimit_contacts_createSM();

	}

	@Test(priority = 9)
	public void Tc_newcase_composenew_add9contacts_errormessage_checking() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.composenew_add_maxlimit_contacts_errrormessagecase();

	}

	@Test(priority=10)
	public void message_203_pastdatetime_validationcreate_Schdule_Message() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.pastDate_Time_compose_new_Schedule_Message();

	}

	@Test(priority=11)
	public void message_204_pastdatetime_validationcreate_groupSchdule_Message() throws Exception {
		Schedule_MessageWF Schedule_MessageWF = new Schedule_MessageWF();
		Schedule_MessageWF.pastDate_Time_group_compose_new_Schedule_Message();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
