package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.UploadNonMediaAttachmentsWF;
import com.beetext.qa.base.TestBase;

public class UploadNonMediaAttachments extends TestBase {

	// @BeforeMethod
	@BeforeClass
	public void setUp() {
		initialization();
		// Login(); //for login

	}

	@Test(priority=1)
	public void Tc_MessageandPDF_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandPDFcombination();

	}

	@Test(priority=2)
	public void Tc_MessageandDocument_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandDocumentcombination();

	}

	@Test(priority=3)
	public void Tc_MessageandTextDocument_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandTextDocumentcombination();

	}

	@Test(priority=4)
	public void Tc_MessageandCSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandCSV_combination();

	}

	@Test(priority=5)
	public void Tc_MessageandExcel_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandExcel_combination();

	}

	@Test(priority=6)
	public void Tc_MessageandDocument_and_pdf_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandDocumentplusPDFcombination();

	}

	@Test(priority=7)
	public void Tc_MessageandDocument_and_Excel_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandDocumentplusExcelcombination();

	}

	@Test(priority=8)
	public void Tc_MessageandDocument_and_CSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandDocumentplusCSVcombination();

	}

	@Test(priority=9)
	public void Tc_MessageandDocument_and_Text_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.MessageandDocumentplusText_combination();

	}

	@Test(priority=10)
	public void Tc_MessageandPDF_and_Text_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_PDF_plusText_combination();

	}

	@Test(priority=11)
	public void Tc_MessageandPDF_and_Document_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_PDF_plusDocument_combination();

	}

	@Test(priority=12)
	public void Tc_MessageandPDF_and_Excel_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_PDF_plusExcel_combination();

	}

	@Test(priority=13)
	public void Tc_MessageandPDF_and_CSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_PDF_plusCSV_combination();

	}

	@Test(priority=14)
	public void Tc_MessageandXLS_and_Document_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_XLS_plusDocument_combination();

	}

	@Test(priority=15)
	public void Tc_MessageandXLS_and_PDF_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_XLS_plus_PDF_combination();

	}

	@Test(priority=16)
	public void Tc_MessageandXLS_and_CSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_XLS_plus_CSV_combination();

	}

	@Test(priority=17)
	public void Tc_MessageandXLS_and_Text_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_XLS_plus_Text_combination();

	}

	@Test(priority=18)
	public void Tc_MessageandCSV_and_Document_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_CSV_plus_Document_combination();

	}

	@Test(priority=19)
	public void Tc_MessageandCSV_and_PDF_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_CSV_plus_PDF_combination();

	}

	@Test(priority=20)
	public void Tc_MessageandCSV_and_XLS_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_CSV_plus_XLS_combination();

	}

	@Test(priority=21)
	public void Tc_MessageandCSV_and_Text_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_CSV_plus_Text_combination();

	}

	@Test(priority=22)
	public void Tc_MessageandText_and_Document_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Text_plus_Document_combination();

	}

	@Test(priority=23)
	public void Tc_MessageandText_and_pdf_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Text_plus_PDF_combination();

	}

	@Test(priority=24)
	public void Tc_MessageandText_and_XLS_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Text_plus_XLS_combination();

	}

	@Test(priority=25)
	public void Tc_MessageandText_and_CSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_Text_plus_CSV_combination();

	}

	@Test(priority=26)
	public void Tc_MessageandImage_and_Document_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_image_plus_Doucment_combination();

	}

	@Test(priority=27)
	public void Tc_MessageandImage_and_PDF_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_image_plus_PDF_combination();

	}

	@Test(priority=28)
	public void Tc_MessageandImage_and_XLS_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_image_plus_XLS_combination();

	}

	@Test(priority=29)
	public void Tc_MessageandImage_and_CSV_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_image_plus_CSV_combination();

	}

	@Test(priority=30)
	public void Tc_MessageandImage_and_Text_combination() throws Exception {

		UploadNonMediaAttachmentsWF attachmentWF = new UploadNonMediaAttachmentsWF();
		attachmentWF.Messageand_image_plus_Text_combination();

	}

	
	

	@AfterMethod
	public void logout() throws Exception {
		VCard_Signout();
	}
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
