package com.beetext.qa.testcases;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.conversations.ConversationWF_Compose_filters;
import com.beetext.qa.base.TestBase;

public class ConversationPageTest_Compose_Filters extends TestBase {
	Map<String, String> usercredentials = new HashMap<String, String>();

	@BeforeClass
	public void setUp() {
		initialization();
		String username1 = prop.getProperty("username1");
		String username2 = prop.getProperty("username2");
		String password = prop.getProperty("password");
		usercredentials.put("username1", username1);
		usercredentials.put("username2", username2);
		usercredentials.put("password", password);
	}

	@Test(priority = 1)
	public void Tc_3_6_11oneoneconversation() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.oneoneconversation(usercredentials);

	}

	@Test(priority = 2)
	public void Tc_4_5_7_8_9_group_conversation_maxtencontactsallowed_error() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.group_conversation_maxtencontactsallowed_error(usercredentials);

	}

	@Test(priority = 3)
	public void tc_5blocked_contact_err() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.blocked_contact_err(usercredentials);

	}

	@Test(priority = 5)
	public void Tc_10_17newnumber_display() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.newnumber_display(usercredentials);

	}

	@Test(priority = 6)
	public void Tc_13_existingnumber_alreadyhavemsg() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.existingnumber_alreadyhavemsg(usercredentials);

	}

	@Test(priority = 7)
	public void Tc_16placeholder() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.placeholder(usercredentials);

	}

	@Test(priority = 8)
	public void Tc_19_conversation_transfer_claim() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.conversation_transfer_claim(usercredentials);

	}

	@Test(priority = 9)
	public void Tc_20conversation_claim() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.conversation_claim(usercredentials);

	}

	@Test(priority = 10)
	public void Tc_21conversation_unclaim() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.conversation_unclaim(usercredentials);

	}

	@Test(priority = 11)
	public void Tc_22_23_24_conversation_resolved_reopen() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.conversation_resolved_reopen(usercredentials);

	}

	@Test(priority = 12)
	public void Tc_18_Toll_Not_Allowed_in_Group() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.Toll_Not_Allowed_in_Group(usercredentials);

	}

	@Test(priority = 13)
	public void Tc_25_Export_Contact() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.Export_Contact(usercredentials);

	}

	@Test(priority = 14)
	public void Tc_26_32_Archive_Cotact() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.Archive_Cotact(usercredentials);

	}

	@Test(priority = 15)
	public void Tc_27_Apply_all() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.Apply_all(usercredentials);

	}

	@Test(priority = 16)
	public void Tc_28_ClaimedByMe() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.ClaimedByMe(usercredentials);

	}

	@Test(priority = 17)
	public void Tc_29_UnClaimed() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.UnClaimed(usercredentials);

	}

	@Test(priority = 18)
	public void Tc_30_Unread() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.Unread(usercredentials);

	}

	@Test(priority = 19)
	public void Tc_31_Unresolved() throws Exception {

		ConversationWF_Compose_filters conversationwf = new ConversationWF_Compose_filters();
		conversationwf.Unresolved(usercredentials);

	}

	@AfterMethod
	public void validate_Signout() throws Exception {
		VCard_Signout();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
