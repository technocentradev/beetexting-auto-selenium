package com.beetext.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.templates.ToolsTemplateWF;
import com.beetext.qa.base.TestBase;

public class ToolsTemplatesPart2 extends TestBase {

	
	@BeforeClass
	public void setUp() {
		initialization();
		
		Login(); // for login
		driver.findElement(By.xpath("//span[@class='tools s-title']")).click();
	}

	@Test(priority = 1)
	public void Tools_template_829_shared_nodata_clickon_create_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtempalte_dont_fill_data_clickon_create_button();
	}

	@Test(priority = 2)
	public void Tools_template_830_shared_nodata_clickon_draft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtempalte_dont_fill_data_clickon_draft_template_button();
	}

	@Test(priority = 3)
	public void Tools_template_831_personal_only_message_clickon_draft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltempalte_only_message_data_clickon_draft_template_button();
	}

	@Test(priority = 4)
	public void Tools_template_833_shared_nodata_clickon_draft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtempalte_nodata_clickon_draft_template_button();
	}

	@Test(priority = 5)
	public void Tools_template_834_personal_title_message_clickon_draft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltempalte_title_and_message_clickon_draft_template_button();
	}

	@Test(priority = 6)
	public void Tools_template_835_shared_title_message_clickon_draft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtempalte_fill_title_message_clickon_savedraft_button();
	}

	// 16-08-2021

	@Test(priority = 7)
	public void Tools_Template_836_837_createtempalte_enter_title_message_selectdept_close_popup_clickon_savedraft()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtempalte_fill_title_message_close_popup_clickon_savedraft_button();
	}

	@Test(priority = 8)
	public void Tools_Template_838_createtempalte_nodata_close_popup_clickon_discard() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtempalte_dont_fill_data_close_popup_clickon_discard_button();
	}

	@Test(priority = 9)
	public void Tools_Template_839_840_createpersonaltempalte_enter_title_message_clickon_otherlink_clickon_save_draft()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createpersonaltempalte_fill_title_message_click_anyother_link_clickon_savedraft_button();
	}

	@Test(priority = 10)
	public void Tools_Template_841_createpersonaltempalte_enter_title_message_clickon_otherlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtempalte_fill_data_close_popup_clickon_discard_button();
	}

	@Test(priority = 11)
	public void Tools_Template_842_843_createsharedtempalte_enter_title_message_clickon_otherlink_clickon_savedraft_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtempalte_fill_title_message_click_anyother_link_clickon_savedraft_button();
	}

	@Test(priority = 12)
	public void Tools_Template_844_createsharedtempalte_enter_title_message_clickon_otherlink_clickon_discard_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtempalte_fill_data_close_popup_clickon_discard_button();
	}

	@Test(priority = 13)
	public void Tools_Template_851_852_manage_personal_tempalte_modify_title_message_clickon_otherlink_clickon_save_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managepersonaltempalte_change_title_message_click_anyother_link_clickon_save_button();
	}

	@Test(priority = 14)
	public void Tools_Template_853_manage_personal_tempalte_modify_title_message_clickon_otherlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managepersonaltempalte_change_title_message_click_anyother_link_clickon_discard_button();
	}

	@Test(priority = 15)
	public void Tools_Template_854_855_manage_personal_tempalte_modify_title_message_close_tools_popup_clickon_save_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managepersonaltempalte_change_title_message_close_tools_popup_clickon_save_button();
	}

	@Test(priority = 16)
	public void Tools_Template_856_manage_personal_tempalte_modify_title_message_close_popup_clickon_discard_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managepersonaltempalte_change_title_message_close_popup_clickon_discard_button();
	}

	@Test(priority = 17)
	public void Tools_Template_860_861_manage_shared_tempalte_modify_title_message_change_sharedto_personal_clickon_navigationlink_clickon_save_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managesharedtempalte_changeto_personaltab_click_breadscrumlink_clickon_save_button();
	}

	@Test(priority = 18)
	public void Tools_Template_862_manage_shared_tempalte_modify_title_message_change_sharedto_personal_clickon_navigationlink_clickon_discard_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managesharedtempalte_changeto_personaltab_click_breadscrumlink_clickon_discard_button();
	}

	@Test(priority = 19)
	public void Tools_Template_863_864_manage_shared_tempalte_add_attachmets_clickon_navigationlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managesharedtempalte__addattachments_click_anyotherlink_clickon_save_button();
	}

	@Test(priority = 20)
	public void Tools_Template_865_manage_shared_tempalte_add_attachmets_clickon_navigationlink_clickon_discard_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managesharedtempalte__addattachments_click_anyotherlink_clickon_discard_button();
	}

	@Test(priority = 21)
	public void Tools_Template_866_867_manage_shared_tempalte_remove_attachmets_clickon_navigationlink_clickon_save_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managesharedtempalte__removeattachments_click_anyotherlink_clickon_save_button();
	}

	@Test(priority = 22)
	public void Tools_Template_868_manage_shared_tempalte_remove_attachmets_clickon_navigationlink_clickon_discard_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.managesharedtempalte__removeattachments_click_anyotherlink_clickon_discard_button();
	}

	@Test(priority = 23)
	public void Tools_Template_869_870_create_tempalte_give_title_clickon_navigationlink_clickon_SAVEDRAFT_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_title_click_anyotherlink_clickon_save_button();
	}

	@Test(priority = 24)
	public void Tools_Template_871_create_tempalte_give_title_clickon_navigationlink_clickon_Discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_title_click_anyotherlink_clickon_discard_button();
	}

	@Test(priority = 25)
	public void Tools_Template_872_873create_tempalte_give_title_close_popup_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_title_close_tools_popup_clickon_save_button();
	}

	@Test(priority = 26)
	public void Tools_Template_874_create_tempalte_give_title_close_popup_clickon_discard_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.cratetemplate_give_title_close_tools_popup_clickon_discard_button();
	}

	@Test(priority = 27)
	public void Tools_Template_875_876create_tempalte_give_title__clickon_breadscrumlink_clickon_savedraft_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_title_clickon_breascrumlink_savedraft_button();
	}

	@Test(priority = 28)
	public void Tools_Template_877_create_tempalte_give_title__clickon_breadscrumlink_clickon_discard_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.cratetemplate_give_title_clickon_breadscrumlink_clickon_discard_button();
	}

	@Test(priority = 29)
	public void Tools_Template_878_879_create_sharedtempalte_give_title__closetoolspopup_clickon_savedraft_button()
			throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createsharedtemplate_give_title_closepopup_savedraft_button();
	}

	@Test(priority = 30)
	public void Tools_Template_880_create_sharedtempalte_give_title__closetoolspopup_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.cratetsharedemplate_give_title_closepopup_clickon_discard_button();
}

	@Test(priority = 31)
	public void Tools_Template_881_create_tempalte_give_title_clickon_Create_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_title_clickon_create_button();
	}

	@Test(priority = 32)
	public void Tools_Template_882_create_tempalte_give_title_clickon_savedraft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_title_clickon_savedraft_button();
	}

	@Test(priority = 33)
	public void Tools_Template_883_create_tempalte_give_message_clickon_create_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_message_clickon_create_button();
	}

	// 23/08/2021

	@Test(priority = 34)
	public void Tools_Template_884_create_tempalte_give_onlymessage_clickon_savedraft_button() throws Exception {

		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlymessage_clickon_savedraft_button();
	}

	@Test(priority = 35)
	public void Tools_Template_885_886_create_tempalte_give_onlymessage_clickon_navigationlink_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlymessage_clickon_navigationlink_clickon_savedraft_button();
	}

	@Test(priority = 36)
	public void Tools_Template_887_create_tempalte_give_onlymessage_clickon_navigationlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlymessage_clickon_navigationlink_clickon_discard_button();
	}

	@Test(priority = 37)
	public void Tools_Template_888_889_create_tempalte_give_onlymessage_closepopup_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlymessage_close_popup_clickon_savedraft_button();
	}

	@Test(priority = 38)
	public void Tools_Template_890_create_tempalte_give_onlymessage_closepopup_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlymessage_close_toolspopup_clickon_discard_button();
	}

	@Test(priority = 39)
	public void Tools_Template_894_895_create_tempalte_give_onlymessage_anyotherlink_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlymessage_clickon_anyotherlink_clickon_savedraft_button();
	}

	@Test(priority = 40)
	public void Tools_Template_896_create_tempalte_give_onlymessage_clickon_anyotherlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlymessage_clickon_anyotherlink_clickon_discard_button();
	}

   @Test(priority = 41)
	public void Tools_Template_897_createtemplate_clickon_personla_add_only_attachment_clickon_create_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_selectpersonaloption_add_attachment_clickon_create_button();
	}

	@Test(priority = 42)
	public void Tools_Template_898_createtemplate_clickon_personla_add_only_attachment_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_selectpersonaloption_add_attachment_clickon_saveDraft_button();
	}

	@Test(priority = 43)
	public void Tools_Template_899_901createtemplate_clickon_personla_add_only_attachment_clickon_breadscrumlink_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createtemplate_selectpersonaloption_add_attachment_clickon_breadscrumlink_clickon_savedraft_button(
						);
	}

	@Test(priority = 44)
	public void Tools_Template_900_createtemplate_clickon_personla_add_only_attachment_clickon_breadscrumlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlyattachment_clickon_breadscrumlink_clickon_discard_button();
	}

	@Test(priority = 45)
	public void Tools_Template_902_904_createtemplate_clickon_personla_add_only_attachment_clickon_anyohterlink_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createtemplate_selectpersonaloption_add_attachment_clickon_anyotherlink_clickon_savedraft_button();
	}

	@Test(priority = 46)
	public void Tools_Template_903_createtemplate_clickon_personla_add_only_attachment_clickon_anyohterlink_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlyattachment_clickon_anyotherlink_clickon_discard_button();
	}

	@Test(priority = 47)
	public void Tools_Template_905_907_createtemplate_clickon_personla_add_only_attachment_closetoolspopup_clickon_savedraft_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate
				.createtemplate_selectpersonaloption_add_attachment_close_tools_popup_clickon_savedraft_button();
	}

	@Test(priority = 48)
	public void Tools_Template_908_createtemplate_clickon_personla_add_only_attachment_closetoolspopup_clickon_discard_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.createtemplate_give_onlyattachment_close_toolspopup_clickon_discard_button();
	}

	@Test(priority = 50)
	public void Tools_Template_911_Managepersonaltemplate_update_template_title_clickon_updatetemplate_button()
			throws Exception {
		ToolsTemplateWF toolstemplate = new ToolsTemplateWF();
		toolstemplate.Managetemplate_selectpersonopton_changetitle_clickon_updatetemplate_button();
	}

	
	@AfterClass
	public void tearDown() {
		driver.quit();

	}
}
