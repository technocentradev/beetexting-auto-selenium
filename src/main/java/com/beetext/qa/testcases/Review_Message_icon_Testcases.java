package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Reviews_Workflow_CM;
import com.beetext.qa.Workflows.Review_Page_WF;
import com.beetext.qa.base.TestBase;

public class Review_Message_icon_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();

		Reviews_Workflow_CM RCM = new Reviews_Workflow_CM();
		RCM.login1();

	}

	@Test(priority = 1) // 3
	public void TC_268_Functionality_of_review_icon_without_Review_Records() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.functionality_of_review_icon_without_active_records();

	}

	@Test(priority = 2) // 41
	public void TC_267_Verify_review_icon_In_message_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_Review_icon_isdisplayed_in_message_page();

	}

	@Test(priority = 3) // 42
	public void TC_269_All_toggle_button_under_list_isdeselected() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Click_on_Review_icon_NoActive_Records_navigates_to_Review_page();

	}

	@Test(priority = 4) // 43
	public void TC_270_verify_link_Url_isenable_in_message() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_message_sent_with_url_in_message_chat_area();

	}

	@Test(priority = 5) // 44
	public void TC_271_review_message_with_URL_displays_in_message_textbox() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_Review_message_with_url_in_message_textbox();

	}

	@Test(priority = 6) // 45
	public void TC_272_click_link_in_message_opens_new_window() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.functionality_of_review_link_Url_in_message();

	}

	@Test(priority = 7) // 46 //This case depends on TC_272
	public void TC_274_verify_org_name_in_link_related_page() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_organization_name_in_Url_related_link_page();

	}

	@Test(priority = 8) // 47
	public void TC_276_click_reviews_button_in_link_page_navigates_newWindow() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.functionality_of_review_related_button_in_link_page();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
