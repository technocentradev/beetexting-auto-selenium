package com.beetext.qa.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.base.TestBase;

public class ContactInfo extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	public static WebDriver driver;
	static String username1 = "auto1@yopmail.com";
	static String username2 = "automations@yopmail.com";
	static String password = "tech1234";

	public static void waitAndLog(int d) {
		try {
			Thread.sleep((long) (d * 1000));
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public static void implicitwaitAndLog(int d) {
		try {
			driver.manage().timeouts().implicitlyWait(d * 10, TimeUnit.SECONDS);
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public static void email(String email) throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='email-id']")).sendKeys(email);
		waitAndLog(1);
	}

	public static void password(String password) throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='password-id']")).sendKeys(password);
		waitAndLog(1);
	}

	public static void loginButton() throws InterruptedException {
		driver.findElement(By.xpath("//button[@id='login-submit']")).click();
		waitAndLog(2);

	}

	public static void notification() {

		driver.findElement(By.xpath("//a[@id='notification']")).click();
	}

	public void validateSignOutLink() {
		driver.findElement(By.xpath("//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")).click();
	}

	public void signuppagelink() {
		driver.findElement(By.id("signupPage")).click();
	}

	public void validateSignOut() {
		driver.findElement(By.xpath("//a[@id='logoutId']")).click();
	}

	public void validateUser() {
		driver.findElement(By.xpath("//*[@class='dropdown-menu show']//a[contains(text(),'AutoTech')]")).click();
	}

	public void validatetoggle() {
		driver.findElement(By.xpath("//*[@class='mat-slide-toggle-thumb']")).click();
	}

	public void contactsTab() {

		waitAndLog(2);
		driver.findElement(By.xpath("//a[@id='contactTab']")).click();

	}

	public void conversationTab() {

		waitAndLog(2);
		driver.findElement(By.xpath("//a[@id='conversationTab']")).click();

	}

	public void contactsTab_numberlink() {

		driver.findElement(By.xpath("//a[@class='contact-info-link cursor-pointer']")).click();

	}

	public void contactsTab_numberlink2() {

		driver.findElement(By.xpath("(//a[@class='contact-info-link cursor-pointer'])[2]")).click();

	}

	public void contactInfo() {

		String text = driver.findElement(By.xpath("//h5[contains(text(),' Contact Info ')]")).getText();
		logger.info(text);
	}

	public void contactInfo_CloseButton() {

		driver.findElement(By.xpath("//button[contains(text(),'Close')]")).click();

	}

	public void contactInfo_ContactDetails() {

		String text = driver.findElement(By.xpath("//div[contains(@class,'offset')]")).getText();
		logger.info(text);
	}

	public static void Automation_Contact() throws Exception {

		driver.findElement(By.xpath("//h4[normalize-space()='Automation']")).click();

	}

	public static void groupCOnversation() throws Exception {

		driver.findElement(By.xpath("//h4[normalize-space()='Automation, QA10']")).click();

	}

	public static void department_Head() throws Exception {

		driver.findElement(By.xpath("//div[@id='hoverSelectDepartment']")).click();

	}

	public static void LocalDept() throws Exception {

		driver.findElement(By.xpath("//button[normalize-space()='Local']")).click();

	}

	public static void Auto1_Dept() throws Exception {

		driver.findElement(By.xpath("//span[@id='Auto1_Department']")).click();

	}

	public static void TollFree_department() throws Exception {

		driver.findElement(By.xpath("//span[normalize-space()='Automation TollFree']")).click();

	}

	public static void TollFree_Button_department() throws Exception {

		driver.findElement(By.xpath("(//button[contains(text(),'Toll-Free')])[1]")).click();

	}

	@BeforeClass
	public void setUp() throws Exception {

		driver = init("chrome");
		driver.manage().window().maximize();
		waitAndLog(10);
		driver.get("http://automation.beetexting.com");
		waitAndLog(10);
		email(username1);
		password(password);
		waitAndLog(2);
		loginButton();
		waitAndLog(5);

	}

	@Test(priority = 1)
	public void Contacts_info_ContactsTab() throws Exception {

		waitAndLog(2);

		contactsTab();
		logger.info("Click on Contacts Tab");
		waitAndLog(2);

		contactsTab_numberlink();
		logger.info("Click on contactsTab_numberlink");
		implicitwaitAndLog(10);

		contactInfo();
		logger.info("Get Text of Contacts Info");
		waitAndLog(2);

		contactInfo_ContactDetails();
		logger.info("Get Text of Contact Details Info");
		waitAndLog(2);

		contactInfo_CloseButton();
		logger.info(" Click on Contact Info - Close Button");
		waitAndLog(2);

	}

	@Test(priority = 2)
	public void Contacts_info_Conversation_ContactInfo() throws Exception {

		waitAndLog(2);

		conversationTab();
		logger.info("Click on Contacts Tab");
		waitAndLog(2);

		Automation_Contact();
		logger.info("Click on Automation Contact");
		waitAndLog(2);

		contactsTab_numberlink();
		logger.info("Click on contactsTab_numberlink");
		implicitwaitAndLog(10);

		contactInfo();
		logger.info("Get Text of Contacts Info");
		waitAndLog(2);

		contactInfo_ContactDetails();
		logger.info("Get Text of Contact Details Info");
		waitAndLog(2);

		contactInfo_CloseButton();
		logger.info(" Click on Contact Info - Close Button");
		waitAndLog(2);

	}

	@Test(priority = 3)
	public void Contacts_info_GroupConversation_ContactInfo() throws Exception {

		waitAndLog(2);

		conversationTab();
		logger.info("Click on Contacts Tab");
		waitAndLog(2);

		groupCOnversation();
		logger.info("Click on groupCOnversation Contact");
		waitAndLog(2);

		contactsTab_numberlink();
		logger.info("Click on contactsTab_numberlink");
		implicitwaitAndLog(10);

		contactInfo();
		logger.info("Get Text of Contacts Info");
		waitAndLog(2);

		contactInfo_ContactDetails();
		logger.info("Get Text of Contact Details Info");
		waitAndLog(2);

		contactInfo_CloseButton();
		logger.info(" Click on Contact Info - Close Button");
		waitAndLog(2);

	}

	@Test(priority = 4)
	public void Contacts_info_GroupConversation_for_Each_ContactInfo() throws Exception {

		waitAndLog(2);

		conversationTab();
		logger.info("Click on Contacts Tab");
		waitAndLog(2);

		groupCOnversation();
		logger.info("Click on groupCOnversation Contact");
		waitAndLog(2);

		contactsTab_numberlink();
		logger.info("Click on contactsTab_numberlink");
		implicitwaitAndLog(10);

		contactInfo();
		logger.info("Get Text of Contacts Info");
		waitAndLog(2);

		contactInfo_ContactDetails();
		logger.info("Get Text of Contact Details Info");
		waitAndLog(2);

		contactInfo_CloseButton();
		logger.info(" Click on Contact Info - Close Button");
		waitAndLog(2);

		groupCOnversation();
		logger.info("Click on groupCOnversation Contact");
		waitAndLog(2);

		contactsTab_numberlink2();
		logger.info("Click on contactsTab_numberlink");
		implicitwaitAndLog(10);

		contactInfo();
		logger.info("Get Text of Contacts Info");
		waitAndLog(2);

		contactInfo_ContactDetails();
		logger.info("Get Text of Contact Details Info");
		waitAndLog(2);

		contactInfo_CloseButton();
		logger.info(" Click on Contact Info - Close Button");
		waitAndLog(2);

	}

	@Test(priority = 5)
	public void Contacts_info_TollFree_ContactsTab() throws Exception {

		waitAndLog(5);

		department_Head();
		logger.info("Click on department_Head Tab");
		waitAndLog(2);

		TollFree_Button_department();
		logger.info("Click on TollFree_Button_department Tab");
		waitAndLog(2);

		TollFree_department();
		logger.info("Click on TollFree_department Tab");
		waitAndLog(2);

		contactsTab();
		logger.info("Click on Contacts Tab");
		waitAndLog(2);

		contactsTab_numberlink();
		logger.info("Click on contactsTab_numberlink");
		implicitwaitAndLog(10);

		contactInfo();
		logger.info("Get Text of Contacts Info");
		waitAndLog(2);

		contactInfo_ContactDetails();
		logger.info("Get Text of Contact Details Info");
		waitAndLog(2);

		contactInfo_CloseButton();
		logger.info(" Click on Contact Info - Close Button");
		waitAndLog(5);

		department_Head();
		logger.info("Click on department_Head Tab");
		waitAndLog(2);

		LocalDept();
		logger.info("Click on TollFree_Button_department Tab");
		waitAndLog(2);

		Auto1_Dept();
		logger.info("Click on TollFree_department Tab");
		waitAndLog(2);

	}

	@Test(priority = 6)
	public void Contacts_info_TollFree_Conversation_ContactInfo() throws Exception {

		waitAndLog(5);

		department_Head();
		logger.info("Click on department_Head Tab");
		waitAndLog(2);

		TollFree_Button_department();
		logger.info("Click on TollFree_Button_department Tab");
		waitAndLog(2);

		TollFree_department();
		logger.info("Click on TollFree_department Tab");
		waitAndLog(2);

		conversationTab();
		logger.info("Click on Contacts Tab");
		waitAndLog(2);

		Automation_Contact();
		logger.info("Click on Automation Contact");
		waitAndLog(2);

		contactsTab_numberlink();
		logger.info("Click on contactsTab_numberlink");
		implicitwaitAndLog(10);

		contactInfo();
		logger.info("Get Text of Contacts Info");
		waitAndLog(2);

		contactInfo_ContactDetails();
		logger.info("Get Text of Contact Details Info");
		waitAndLog(2);

		contactInfo_CloseButton();
		logger.info(" Click on Contact Info - Close Button");
		waitAndLog(5);

		department_Head();
		logger.info("Click on department_Head Tab");
		waitAndLog(2);

		LocalDept();
		logger.info("Click on TollFree_Button_department Tab");
		waitAndLog(2);

		Auto1_Dept();
		logger.info("Click on TollFree_department Tab");
		waitAndLog(2);

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
