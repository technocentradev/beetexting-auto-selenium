package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.ProfileWFCM;
import com.beetext.qa.Workflows.CurrentPasswordWF;
import com.beetext.qa.base.TestBase;

public class ChangePassword_Test  extends TestBase {
	
	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		ProfileWFCM profile=new ProfileWFCM();
		profile.login(); //for login
	}
	
	
	@Test(priority=1)
	public void change_currnet_Password_required_validation_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.currentpassword_required_validation_case();
		
	}
	@Test(priority=2)
	public void change_current_Password_min_8char_validation_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.currentpassword_required_min_8_char_validation_case();
		
	}
	@Test(priority=3)
	public void change_new_Password_required_validation_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.newpassword_required_validation_case();
		
	}
	@Test(priority=4)
	public void change_new_Password_min_8char_validation_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.newpassword_required_min_8_char_validation_case();
		
	}
	@Test(priority=5)
	public void change_confirm_new_Password_required_validation_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.confirm_newpassword_required_validation_case();
		
	}
	@Test(priority=6)
	public void change_confirm_new_Password_min_8char_validation_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.confirm_newpassword_required_min_8_char_validation_case();
		
	}
	@Test(priority=7)
	public void change_current_password_invalid_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.confirmpassword_invalid_case();
		
	}
	@Test(priority=8)
	public void change_new_password_confirm_newpassword_mismatch_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.newpassword_confirmpassword_mismatch_case();
		
	}
	@Test(priority=9)
		public void change_Password_case() {
			CurrentPasswordWF password= new CurrentPasswordWF();
			password.changepassword_success_case();
			
		}
	@Test(priority=10)
	public void clickon_cancel_button_cases() throws Exception {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.clickon_cancel_buttoncase();
		
	}
	
	@Test(priority=11)
	public void clickon_close_profilepopup_cases() throws Exception {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.clickon_close_prfilepopup_icon_case();
		
	}
	@Test(priority=12)
	public void change_newPassword_matchingwith_current_password_case() {
		CurrentPasswordWF password= new CurrentPasswordWF();
		password.newpassword_matchingwith_currentpassword_case();
		
	}
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
