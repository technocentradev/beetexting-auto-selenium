package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.broadcast.BM_Opt_Out_WF;
import com.beetext.qa.WFCommonMethod.BM_Optout_WFCM;
import com.beetext.qa.base.TestBase;

public class BM_Opt_out_test extends TestBase {

	public static String currentDir = System.getProperty("user.dir");

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		BM_Optout_WFCM wfcm = new BM_Optout_WFCM();
		wfcm.BM_tools_login_automations();
	}

	@Test(priority = 1)
	public void BM_Optout_01_Create_BM_with_OptOut() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.Create_BM_with_OptOut();

	}

	@Test(priority = 2)
	public void BM_Optout_02_upcoming_Mange_Optout_Enable() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.upcoming_Mange_Optout_Enable();

	}

	@Test(priority = 3)
	public void BM_Optout_03_upcoming_Mange_Disable_Optout_Click_on_OtherTools_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.upcoming_Mange_Disable_Optout_Click_on_OtherTools_Save();

	}

	@Test(priority = 4)
	public void BM_Optout_04_upcoming_Manage_Disable_Optout_Click_on_outside_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.upcoming_Manage_Disable_Optout_Click_on_outside_Save();

	}

	@Test(priority = 5)
	public void BM_Optout_05_upcoming_Mange_Disable_Optout_Click_on_Breadcumlink_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.upcoming_Mange_Disable_Optout_Click_on_Breadcumlink_Save();

	}

	@Test(priority = 6)
	public void BM_Optout_06_create_DraftBM_without_Msg() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.create_DraftBM_without_Msg();

	}

	@Test(priority = 7)
	public void BM_Optout_07_manage_DraftBM_Add_Message_and_Update() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.manage_DraftBM_Add_Message_and_Update();

	}

	@Test(priority = 8)
	public void BM_Optout_08_manage_DraftBM_unCheck_optout_and_Update() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.manage_DraftBM_unCheck_optout_and_Update();

	}

	@Test(priority = 5)
	public void BM_Optout_09_manage_DraftBM_unCheck_optout_and_SaveDraft() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.manage_DraftBM_unCheck_optout_and_SaveDraft();

	}

	@Test(priority = 10)
	public void BM_Optout_10_manage_DraftBM_unCheck_optout_Click_Outside_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.manage_DraftBM_unCheck_optout_Click_Outside_Save();

	}

	@Test(priority = 11)
	public void BM_Optout_12_past_reuse_OptoutMsg_Click_breadcumlink_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.past_reuse_OptoutMsg_Click_breadcumlink_Save();

	}

	@Test(priority = 12)
	public void BM_Optout_13_past_reuse_OptoutMsg_Click_otherlink_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.past_reuse_OptoutMsg_Click_otherlink_Save();

	}

	@Test(priority = 13)
	public void BM_Optout_14_past_view_reuse_OptoutMsg_Click_breadcumlink_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.past_view_reuse_OptoutMsg_Click_breadcumlink_Save();

	}

	@Test(priority = 14)
	public void BM_Optout_15_past_view_reuse_OptoutMsg_Click_otherlink_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.past_view_reuse_OptoutMsg_Click_otherlink_Save();

	}

	@Test(priority = 15)
	public void BM_Optout_16_past_view_reuse_OptoutMsg_Click_outside_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.past_view_reuse_OptoutMsg_Click_outside_Save();

	}

	@Test(priority = 16)
	public void BM_Optout_17_past_reuse_OptoutMsg_Click_outside_Save() throws Exception {

		BM_Opt_Out_WF bmOptOutWF = new BM_Opt_Out_WF();
		bmOptOutWF.past_reuse_OptoutMsg_Click_outside_Save();

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
