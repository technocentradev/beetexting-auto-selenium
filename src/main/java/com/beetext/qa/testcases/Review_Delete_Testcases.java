package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.WFCommonMethod.Reviews_Workflow_CM;
import com.beetext.qa.Workflows.Review_Page_WF;
import com.beetext.qa.base.TestBase;

public class Review_Delete_Testcases extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Reviews_Workflow_CM RCM = new Reviews_Workflow_CM();
		RCM.login2();

//	   Login();   for login

	}
//	  Review delete test cases starts

	@Test(priority = 1) // 36
	public void TC_262_manage_Review_page_DeleteReview_button_IsEnabled() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Manage_review_Page_DeleteReview_button_IsEnabled();

	}

	@Test(priority = 2) // 37
	public void TC_263_manage_Review_page_ConfirmDeleteReview_button_IsEnabled() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.manage_Review_page_Confirm_Delete_Button_IsEnabled();

	}

	@Test(priority = 3) // 38
	public void TC_264_Functionality_of_ConfirmDeleteReview_button() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.ManageReview_page_Click_ConfirmDeleteReview_Button_redirects_Reviews_page();

	}

	@Test(priority = 4) // 39
	public void TC_265_Verify_tosater_message_for_Delete_record() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_Toaster_message_for_Deleted_review_record();

	}

	@Test(priority = 5) // 40
	public void TC_266_Verify_Deleted_Review_record() throws Exception {

		Review_Page_WF reviewwf = new Review_Page_WF();
		reviewwf.Verify_Deleted_record_in_Review_List_page();

	}
//	 Review delete test cases ends

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
