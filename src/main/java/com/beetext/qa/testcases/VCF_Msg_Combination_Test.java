package com.beetext.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.beetext.qa.Workflows.vcf.VCF_Msg_Combination_WF;
import com.beetext.qa.base.TestBase;

public class VCF_Msg_Combination_Test extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {

		initialization();

	}

	@Test(priority = 1)
	public void VCF_26_VCF_Msg() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg();
	}

	@Test(priority = 2)
	public void VCF_27_VCF_Gif() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Gif();
	}

	@Test(priority = 3)
	public void VCF_28_VCF_Msg_Gif() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_Gif();
	}

	@Test(priority = 4)
	public void VCF_29_VCF_Msg_Emoji() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_Emoji();
	}

	@Test(priority = 5)
	public void VCF_30_VCF_Emoji() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Emoji();
	}

	@Test(priority = 6)
	public void VCF_31_VCF_Msg_Image() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_Image();
	}

	@Test(priority = 7)
	public void VCF_32_VCF_Image() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Image();
	}

	@Test(priority = 8)
	public void VCF_33_VCF_Msg_Video() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_Video();
	}

	@Test(priority = 9)
	public void VCF_34_VCF_Msg_audio() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_audio();
	}

	@Test(priority = 10)
	public void VCF_35_VCF_Video() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Video();
	}

	@Test(priority = 11)
	public void VCF_36_VCF_audio() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_audio();
	}

	@Test(priority = 12)
	public void VCF_37_VCF_Msg_pdf() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_pdf();
	}

	@Test(priority = 13)
	public void VCF_38_VCF_pdf() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_pdf();
	}

	@Test(priority = 14)
	public void VCF_39_VCF_Msg_Excel() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_Excel();
	}

	@Test(priority = 15)
	public void VCF_40_VCF_Excel() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Excel();
	}

	@Test(priority = 16)
	public void VCF_41_VCF_Msg_WordDoc() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_WordDoc();
	}

	@Test(priority = 17)
	public void VCF_42_VCF_WordDoc() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_WordDoc();
	}

	@Test(priority = 18)
	public void VCF_43_VCF_Msg_TxtDoc() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_TxtDoc();
	}

	@Test(priority = 19)
	public void VCF_44_VCF_TxtDoc() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_TxtDoc();
	}

	@Test(priority = 20)
	public void VCF_45_VCF_Msg_MOV() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_MOV();
	}

	@Test(priority = 21)
	public void VCF_46_VCF_MOV() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_MOV();
	}

	@Test(priority = 22)
	public void VCF_47_VCF_Msg_WEBM() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_WEBM();
	}

	@Test(priority = 23)
	public void VCF_48_VCF_WEBM() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_WEBM();
	}

	@Test(priority = 24)
	public void VCF_49_VCF_Msg_GIF_Emoji_MP3_MOV_PDF_Image_Video() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_GIF_Emoji_MP3_MOV_PDF_Image_Video();
	}

	@Test(priority = 25)
	public void VCF_50_VCF_Msg_Mp3() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Msg_Mp3();
	}

	@Test(priority = 26)
	public void VCF_51_VCF_Mp3() throws Exception {

		VCF_Msg_Combination_WF VCFWF = new VCF_Msg_Combination_WF();
		VCFWF.VCF_Mp3();
	}

	@AfterMethod
	public void validate_Signout() throws Exception {
		VCard_Signout();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
