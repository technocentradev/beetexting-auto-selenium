package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

 public class UploadNonMediaAttachment extends  TestBase {

	
	public UploadNonMediaAttachment() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}
	
	//attachment link
		@FindBy(xpath ="(//button[@class='btn btn-link btn-chat-link'])[1]")
		public WebElement pin_attachment;
		
		@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
		public WebElement input_box;
		
		@FindBy(xpath = "//button[@id='chatInput-sendButton']")
		public WebElement postButton;
		
		@FindBy(xpath =" //h4[normalize-space()='Automation']") //auto1 login conversation
		public WebElement select_conversations12;
		
		@FindBy(xpath =" //h4[normalize-space()='Auto1']") //automation login conversation
		public WebElement select_conversations;
		
		@FindBy(xpath = "//*[@class ='messages ng-star-inserted']")
		public WebElement pinmessage;
		
		@FindBy(xpath = "//span[@class='media-name']")
		public WebElement image_verification;
     
		
}



