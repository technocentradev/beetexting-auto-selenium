package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Automations_pages extends TestBase {

	public Automations_pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//a[@id='automationsTab']")
	public WebElement tools_Automations;

	@FindBy(xpath = "//div[contains(text(),' Your Auto Response has been successfully added. ')]")
	public WebElement Toaster;

	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement Auto1_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement Automation_Contact;

	@FindBy(xpath = "//*[@class='cross-icons']")
	public WebElement tools_crossBtn;

	@FindBy(xpath = "//button[@id='createAutomationButton']")
	public WebElement create_Automations;

	@FindBy(xpath = "//i[contains(text(),'No automations have been created for this number.')]")
	public WebElement No_Automations_Created;

	@FindBy(xpath = "//select[@id='department']")
	public WebElement department;

	@FindBy(xpath = "//option[contains(text(),'Auto1 ')]")
	public WebElement department_Auto;

	@FindBy(xpath = "//option[contains(text(),'Auto2 ')]")
	public WebElement department_auto2;

	@FindBy(xpath = "//option[contains(text(),'sample')]")
	public WebElement department_Sample;

	@FindBy(xpath = "//option[contains(text(),'sample1')]")
	public WebElement department_Sample2;

	@FindBy(xpath = "//button[@id='addConitionButton']")
	public WebElement add_Condition;

	@FindBy(xpath = "//button[@id='addActionButton']")
	public WebElement add_Action;

	@FindBy(xpath = "//button[@id='saveButton']")
	public WebElement Create;

	@FindBy(xpath = "//div[@id='invalidBlankTitle']")
	public WebElement blank_title;

	@FindBy(xpath = "//div[contains(text(),' Title must be at least 3 characters. ')]")
	public WebElement title_3Char;

	@FindBy(xpath = "//input[@id='title']")
	public WebElement title;

	@FindBy(xpath = "//div[@id='titleDuplicateError']")
	public WebElement title_already_exists;

	@FindBy(xpath = "//input[@id='conditionType_searchTerm_0']")
	public WebElement condition_Keyword;

	@FindBy(xpath = "//textarea[@id='message']")
	public WebElement Reply_message;

	@FindBy(xpath = "//button[@id='manageButton_0']")
	public WebElement manage;

	@FindBy(xpath = "//button[@id='manageButton_1']")
	public WebElement manage_1;

	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[1]")
	public WebElement ON_OFF_1;

	@FindBy(xpath = "//button[@id='delete-autoresponse']")
	public WebElement delete_autoResponse;

	@FindBy(xpath = "//div[@class='mat-slide-toggle-thumb']")
	public WebElement automation_ON_OFF;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted'])[1]")
	public WebElement message1;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted'])[2]")
	public WebElement message2;

	@FindBy(xpath = "//app-check-box[@id='conditionType_matchCase_0']//span[@class='checkmark']")
	public WebElement Case_Sensitive;

	@FindBy(xpath = "//select[@id='conditionType_matchType_0']")
	public WebElement exactlyMatches_Contains_DropDown;

	@FindBy(xpath = "//option[contains(text(),' Exactly Matches')]")
	public WebElement exactlyMatches_DropDown;

	@FindBy(xpath = "//option[contains(text(),' Contains')]")
	public WebElement Contains_DropDown;

	@FindBy(xpath = "//div[contains(text(),' Search term is required. ')]")
	public WebElement Search_Term_required;

	@FindBy(xpath = "//select[@id='automationCondition_conditionType_0']")
	public WebElement Msg_Received_Tag_Time_IS_DropDown;

	@FindBy(xpath = "//option[contains(text(),' Message Received ')]")
	public WebElement Message_Received;

	@FindBy(xpath = "//option[contains(text(),' Time is ')]")
	public WebElement Time_is;

	@FindBy(xpath = "//option[contains(text(),' Tag Added to Contact ')]")
	public WebElement Tag_Added_to_Contact;

	@FindBy(xpath = "//div[contains(text(),' Weekday is required. ')]")
	public WebElement weekday_required;

	@FindBy(xpath = "//select[@id='conditionType_timeRange_0']")
	public WebElement within_outsideOf_DropDown;

	@FindBy(xpath = "//option[contains(text(),' Within')]")
	public WebElement within_DropDown;

	@FindBy(xpath = "//option[contains(text(),' Outside of')]")
	public WebElement outsideOf_DropDown;

	@FindBy(xpath = "//app-check-box[@id='McheckBox_0']")
	public WebElement Monday;

	@FindBy(xpath = "//app-check-box[@id='TcheckBox_0']")
	public WebElement Tuesday;

	@FindBy(xpath = "//app-check-box[@id='WcheckBox_0']")
	public WebElement Wednesday;

	@FindBy(xpath = "//app-check-box[@id='ThcheckBox_0']")
	public WebElement Thursday;

	@FindBy(xpath = "//app-check-box[@id='FcheckBox_0']")
	public WebElement Friday;

	@FindBy(xpath = "//app-check-box[@id='SacheckBox_0']")
	public WebElement Saturday;

	@FindBy(xpath = "//app-check-box[@id='SucheckBox_0']")
	public WebElement Sunday;

	@FindBy(xpath = "//input[@id='conditionType_startTime_0']")
	public WebElement Start_Time;

	@FindBy(xpath = "//input[@id='conditionType_endTime_0']")
	public WebElement End_Time;

	@FindBy(xpath = "//div[contains(text(),' Start time should be less than end time. ')]")
	public WebElement EndTime_Err;

	@FindBy(xpath = "//div[@id='automationCondition_close_0']")
	public WebElement Condition_Close;

	@FindBy(xpath = "//div[@id='automationAction_close_0']")
	public WebElement Action_Close;

	@FindBy(xpath = "//div[contains(text(),' A condition is required ')]")
	public WebElement Condition_required;

	@FindBy(xpath = "//div[contains(text(),' An action is required ')]")
	public WebElement Action_required;

	@FindBy(xpath = "//div[contains(text(),' Only one Tag is allowed. ')]")
	public WebElement one_Tag_Allowed_Error;

	@FindBy(xpath = "//select[@id='type']")
	public WebElement Action_met_UP_Condition;

	@FindBy(xpath = "//option[contains(text(),' Send a reply immediately ')]")
	public WebElement Action_Send_Immediately;

	@FindBy(xpath = "//option[contains(text(),' Send a delayed reply ')]")
	public WebElement Action_Send_Delayed_Reply;

	@FindBy(xpath = "//option[contains(text(),' Add tag to contact immediately ')]")
	public WebElement Action_Add_Tag_Contact_Immediately;

	@FindBy(xpath = "//option[contains(text(),' Add tag to contact delayed ')]")
	public WebElement Action_Add_Tag_Contact_Delayed;

	@FindBy(xpath = "//option[contains(text(),' Remove tag to contact immediately ')]")
	public WebElement Action_Remove_Tag_Contact_Immediately;

	@FindBy(xpath = "//option[contains(text(),' Remove tag to contact delayed ')]")
	public WebElement Action_Remove_Tag_Contact_Delayed;

	@FindBy(xpath = "(//select[@id='type'])[2]")
	public WebElement Action_met_UP_Condition1;

	@FindBy(xpath = "(//option[contains(text(),' Send a reply immediately ')])[2]")
	public WebElement Action_Send_Immediately1;

	@FindBy(xpath = "(//option[contains(text(),' Send a delayed reply ')])[2]")
	public WebElement Action_Send_Delayed_Reply1;

	@FindBy(xpath = "(//option[contains(text(),' Add tag to contact immediately ')])[2]")
	public WebElement Action_Add_Tag_Contact_Immediately1;

	@FindBy(xpath = "(//option[contains(text(),' Add tag to contact delayed ')])[2]")
	public WebElement Action_Add_Tag_Contact_Delayed1;

	@FindBy(xpath = "(//option[contains(text(),' Remove tag to contact immediately ')])[2]")
	public WebElement Action_Remove_Tag_Contact_Immediately1;

	@FindBy(xpath = "(//option[contains(text(),' Remove tag to contact delayed ')])[2]")
	public WebElement Action_Remove_Tag_Contact_Delayed1;

	@FindBy(xpath = "//input[@id='tagInput']")
	public WebElement Conversation_TagInput;

	@FindBy(xpath = "//button[@id='tagAddButton']")
	public WebElement Conversation_TagAdd;

	@FindBy(xpath = "//button[@id='tagRemoveConfirm']")
	public WebElement Conversation_Remove_Tag;

	@FindBy(xpath = "//label[contains(text(),'abc')]")
	public WebElement Conversation_ABC_Tag;

	@FindBy(xpath = "//label[contains(text(),'a')]")
	public WebElement Conversation_A_Tag;

	@FindBy(xpath = "//svg-icon[@class='contact-tag-cross-icon']")
	public WebElement Conversation_Tag_Close;

	@FindBy(xpath = "(//input[@id='inputBroadcastTagData'])[2]")
	public WebElement input_To_searchTags_Action;

	@FindBy(xpath = "(//input[@id='inputBroadcastTagData'])[3]")
	public WebElement input_To_searchTags_Action1;

	@FindBy(xpath = "//button[@id='tagAddButton']")
	public WebElement Select_SearchTag1;

	@FindBy(xpath = "//button[@id='tagAddButton']")
	public WebElement Select_SearchTag2;

	@FindBy(xpath = "//button[@id='tagAddButton']")
	public WebElement Select_SearchTag3;

	@FindBy(xpath = "//input[@id='delayCount']")
	public WebElement Input_Delay;

	@FindBy(xpath = "//input[@id='delayCount']")
	public WebElement Delay_Unit_Min_Hr_Days;

	@FindBy(xpath = "//option[contains(text(),' Minutes ')]")
	public WebElement Delay_Unit_Minutes;

	@FindBy(xpath = "//option[contains(text(),' Hours ')]")
	public WebElement Delay_Unit_Hours;

	@FindBy(xpath = "//option[contains(text(),' Days ')]")
	public WebElement Delay_Unit_Days;

	@FindBy(xpath = "//div[contains(text(),' Tag already exists and cannot be added again. ')]")
	public WebElement Action_Tag_Already_Added_Error;

	@FindBy(xpath = "//*[contains(text(),' Tag is required. ')]")
	public WebElement Action_Tag_Required_Error;

	@FindBy(xpath = "//div[contains(text(),' Delay value cannot exceed 100. ')]")
	public WebElement Delay_value_cannot_exceed_100;

	@FindBy(xpath = "//div[contains(text(),' *Any delayed messages will be canceled if the contact replies. ')]")
	public WebElement Delayed_Msg_Cancel_If_Contact_Replies;

	@FindBy(xpath = "//a[@id='conversationTab']")
	public WebElement Conversation_Tab;

	@FindBy(xpath = "//a[@id='contactTab']")
	public WebElement Contact_Tab;

	@FindBy(xpath = "//p[contains(text(),'No Tags')]")
	public WebElement NO_Tags;

}
