package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class importCSV extends TestBase  {
	public importCSV() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(xpath = "//a[@id='contactTab']")
	public WebElement contact_tab;
	//*[@class='import-icon']//*[local-name()='svg']
	//*[@src='../assets/grey/import.svg']//*[local-name()='svg']
	@FindBy(xpath = "//a[2]//svg-icon[@class='import-icon']")
	public WebElement import_svg;
	
	@FindBy(xpath = "//button[contains(text(),'Select file')]")
	public WebElement select_file;
	
	@FindBy(xpath = "//button[contains(text(),'Import Contacts')]")
	public WebElement importcontacts_button;
	
	@FindBy(xpath = "//div[@aria-label='Please import your contacts using a CSV file.']")
	public WebElement  invalid_CSV;
	
	@FindBy(xpath = "//button[@class='btn csv-format']")
	public WebElement  download_sample_CSV;
	//span[normalize-space()='Total Contacts Imported :']
	
	@FindBy(xpath = "//span[contains(text(),'Total Contacts Imported :')]")
	public WebElement  totalimportscontacts_text;
	//span[normalize-space()='19']
	@FindBy(xpath = "//span[contains(text(),'19')]")
	public WebElement  totalimportscontacts_count;
	@FindBy(xpath = "//span[contains(text(),'0')]")
	public WebElement  totalimportscontacts_count1;
	
	@FindBy(xpath = " //span[normalize-space()='Successfully Imported :']")
	public WebElement  successimportscontacts_text;
	//span[normalize-space()='6']
	//span[normalize-space()='12']
	//div[@class='modal-container text-center']//span[contains(text(),'12')]
	@FindBy(xpath = "//span[normalize-space()='12']")
	public WebElement  successimportscontacts_count;
	@FindBy(xpath = "//span[normalize-space()='0']")
	public WebElement  successimportscontacts_count1;
	
	
	@FindBy(xpath = "//span[contains(text(),'Failed to import :')]")
	public WebElement  failureimportscontacts_text;
	//span[normalize-space()='4']
	 
	@FindBy(xpath = "//span[normalize-space()='7']")
	public WebElement  failureimportscontacts_count;
	@FindBy(xpath = "//span[normalize-space()='0']")
	public WebElement  failureimportscontacts_count1;
	
	
	@FindBy(xpath = "//div[contains(text(),'Import Contacts Report')]")
	public WebElement  import_contact_headder;
	
	
	@FindBy(xpath = " //a[contains(text(),'here')]")
	public WebElement  download_error_CSV;
	
	@FindBy(xpath = "//button[@class='Mt-10 btn btn-primary text-center']")
	public WebElement  closeButton ;
	@FindBy(xpath = "//*[@class='real cross-icon']//*[name()='svg']")
	public WebElement  crossMark ;
	
	
	
	
	
	
	
	
	
}
