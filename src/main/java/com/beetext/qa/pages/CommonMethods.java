package com.beetext.qa.pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.beetext.qa.util.TestUtil;
import com.beetext.qa.util.WebEventListener;
import com.steadystate.css.parser.ParseException;

public class CommonMethods extends Repository {
	// WebDriver driver;
	// public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");
	ResultSet resultset = null;
	Connection connection = null;
	Statement statement = null;
	ArrayList<String> win;
	public static String TESTDATA_SHEET_PATH = currentDir
			+ "\\src\\main\\java\\com\\beetext\\qa\\testdata\\BeeTextTestData.xlsx";
	public static String TestDataSheetName = "SignupPage";

	public CommonMethods() {
		try {

			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					currentDir + "\\src\\main\\java\\com\\beetext\\qa\\config\\config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void initialization() {

		logger = Logger.getLogger("Beetexting Test Excution Logs report");
		PropertyConfigurator.configure("Log4j.properties");
		String browserName = prop.getProperty("browser");

		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					currentDir + "\\BrowserDrivers\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equals("FF")) {
			System.setProperty("webdriver.gecko.driver", "C:/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		e_driver = new EventFiringWebDriver(driver);
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS); // create util class
																									// for timeunit
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);

		driver.get(prop.getProperty("url"));
	}

	// random string generation methods

	public String randomestring() {
		String generatedstring = RandomStringUtils.randomAlphabetic(8);
		return (generatedstring);
	}

	public String randomecapitalstring() {
		String generatedstring = RandomStringUtils.randomAlphabetic(8).toUpperCase();
		return (generatedstring);
	}

	public String Char50_randomestring() {
		String generatedstring = RandomStringUtils.randomAlphabetic(50);
		return (generatedstring);
	}

	public String Char1000_randomestring() {
		String generatedstring = RandomStringUtils.randomAlphabetic(1000);
		return (generatedstring);
	}

	public static String randomeNum() {
		String generatedString2 = RandomStringUtils.randomNumeric(7);
		return (generatedString2);
	}

	public static String exportData(String value, int num) {
		String data = "";
		try {
			// Create FileInput Stream object
			FileInputStream fis = new FileInputStream(TESTDATA_SHEET_PATH);
			// create work book object
			XSSFWorkbook wb1 = new XSSFWorkbook(fis);
			// create work sheet object
			XSSFSheet ws1 = wb1.getSheet(TestDataSheetName);
			// get row count
			int rowcount = ws1.getLastRowNum();
			for (int j = 0; j <= rowcount; j++) {
				String cell = ws1.getRow(j).getCell(0).getStringCellValue();
				if (cell.equals(value)) {
					data = ws1.getRow(j).getCell(num).getStringCellValue();
					System.out.println("data-->" + data);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public WebElement textbox(WebElement webelement) throws Exception {
		// WAIT(webelement);
		HighlightElement(driver, webelement);
		return webelement;
	}

	/***
	 * navigate an url
	 * 
	 * @throws Exception
	 ***/
	public void navigatePage(String appUrl) throws Exception {
		driver.navigate().to(appUrl);
		Thread.sleep(6000);
		System.out.println("Opened the Url-->" + appUrl);
	}

	/*** WebElement--> Link ***/
	public WebElement link(WebElement webelement, String control) throws Exception {
		// WAIT(webelement);
		HighlightElement(driver, webelement);
		System.out.println("clicked on the link_control-->" + control);
		return webelement;
	}

	/*** WebElement--> image ***/
	public WebElement image(WebElement webelement, String control) throws Exception {
		WAIT(webelement);
		HighlightElement(driver, webelement);
		System.out.println("clicked on the image_control-->" + control);
		return webelement;
	}

	/*** WebElement--> textbox ***/
	public WebElement textbox(WebElement webelement, String text, String control) throws Exception {
		WAIT(webelement);
		HighlightElement(driver, webelement);
		// Log.info("Entered the text -->"+text + " on " + control);
		return webelement;
	}

	public WebElement textbutton(WebElement webelement, String control) throws Exception {
		// WAIT(webelement);
		HighlightElement(driver, webelement);
		System.out.println("clicked on the button_control-->" + control);
		return webelement;
	}

	/*** WebElement--> checkbox ***/
	public WebElement checkbox(WebElement webelement, String control) throws Exception {
		// WAIT(webelement);
		HighlightElement(driver, webelement);
		System.out.println("Checked/Unchecked on the control-->" + control);
		return webelement;
	}

	/*** WebElement--> button ***/
	public WebElement button(WebElement webelement, String control) throws Exception {
		// WAIT(webelement);
		HighlightElement(driver, webelement);
		System.out.println("clicked on the button_control-->" + control);
		return webelement;
	}

	/*** WebElement--> selectbox ***/
	public Select selectbox(WebElement webelement, String option) throws Exception {
		WAIT(webelement);
		Select select = new Select(webelement);
		HighlightElement(driver, webelement);
		System.out.println("selected the value as-->" + option + " in the Dropdown ");
		return select;
	}

	public WebElement radiobutton(WebElement webelement, String control) throws Exception {
		WAIT(webelement);
		HighlightElement(driver, webelement);
		System.out.println("Checked/Unchecked on the control-->" + control);
		return webelement;
	}

	public WebElement table(WebElement webelement) throws Exception {
		WAIT(webelement);
		// Log.info("clicked on the button_control-->" + control);
		return webelement;
	}

	public void JSClick(WebElement webelement) throws Exception {
		WAIT(webelement);
		JS().executeScript("arguments[0].click();", webelement);
		System.out.println("clicked on button" + webelement.toString());
	}

	// ***** Window Handling ****//
	public void winhndls(int index) {
		win = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(win.get(index));
		System.out.println("Window Handles");
	}

	public void alert() {
		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
		}

	}

	/*****
	 * Actions
	 * 
	 * @throws InterruptedException
	 ****/
	public Actions actions() throws InterruptedException {
		Actions actObj = new Actions(driver);
		System.out.println("Actions performing");
		return actObj;
	}

	/*****
	 * JavaScript
	 * 
	 * @return
	 * @throws InterruptedException
	 ****/
	public JavascriptExecutor JS() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		System.out.println("JavaScript performing");
		return js;
	}

	/*** Waits for 60 sec for visibility of a WebElement ***/
	public WebElement WAIT(WebElement webelement) throws Exception {
		WebDriverWait w = new WebDriverWait(driver, 120);
		try {
			if (w.until(ExpectedConditions.visibilityOf(webelement)) != null)
				HighlightElement(driver, webelement);
		} catch (Exception e) {
			System.out.println(
					"WebElement shoulb be visible in the application but WebElement is not visible in the application::");
		}
		return webelement;
	}

	public WebElement WAIT_INV(WebElement webelement) throws Exception {
		WebDriverWait w = new WebDriverWait(driver, 240);
		w.until(ExpectedConditions.invisibilityOf(webelement));
		Thread.sleep(1000);
		return webelement;
	}

	/*** HighLights WebElement in WebPage ***/
	public static WebDriver HighlightElement(WebDriver driver, WebElement element) {
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].setAttribute('style', 'background:pink;border:2px solid green;');", element);
		}
		return driver;
	}

	/**
	 * -----------------------------Web table Methods Start-----------------------
	 **/
	/*** Returns RowCount of WebTable ***/
	public int fn_RowCount(WebElement WE, String table) {
		int row_count = 0;
		try {
			WAIT(WE);
			row_count = WE.findElements(By.tagName("tr")).size();
			System.out.println("Row Count of  -->" + table + " is " + row_count);

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return row_count;
	}

	/*** Returns ColCount of WebTable ***/
	public int fn_ColCount(WebElement WE, String table) {
		int col_count = 0;
		try {
			WAIT(WE);
			col_count = WE.findElements(By.xpath(".//*/tr/th")).size();
			System.out.println("Col Count of  -->" + table + " is " + col_count);

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return col_count;
	}

	/*** Returns Cell Value of WebTable ***/
	public String fn_GetCell(int i, int j, WebElement WE) {
		String cellval = "";
		try {
			WAIT(WE);
			WebElement tableRow = WE.findElement(By.xpath(".//tbody/tr[" + i + "]"));
			List<WebElement> tablecol = tableRow.findElements(By.tagName("td"));
			cellval = tablecol.get(j).getText();
			// Log.info("Cell Value of "+i+"th row and "+(j+1)+ "th col is "+cellval);

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return cellval;
	}

	/*** Returns Column Number of given Column in WebTable ***/
	public int fn_getColumnNum(String columnName, WebElement WE) {
		int columnNum = 0;
		try {
			WAIT(WE);
			int columns = WE.findElements(By.tagName("th")).size();
			System.out.println("columnsCount-->" + columns);
			for (int i = 0; i <= columns; i++) {
				String columnName_UI = WE.findElements(By.tagName("th")).get(i).getText();
				if (columnName_UI.trim().equalsIgnoreCase(columnName)) {
					columnNum = i;
					break;
				}
			}
			System.out.println("Col Num of Col Name:: " + columnName + "is" + columnNum);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return columnNum;
	}

	/**
	 * -----------------------------Web table Methods End-----------------------
	 **/
	/** -----------------------------Date Methods Start----------------------- **/

	/*** Returns Current TimeStamp ***/
	public String fn_GetTimeStamp() {
		DateFormat DF = DateFormat.getDateTimeInstance();
		Date dte = new Date();
		String DateValue = DF.format(dte);
		DateValue = DateValue.replaceAll(":", "_");
		DateValue = DateValue.replaceAll(",", "");
		return DateValue;
	}

	/*** Validates InnerText ***/
	public void ValidateControlInnerText(WebElement WE, String strexpected) {
		try {
			WAIT(WE);
			String actual = WE.getAttribute("innerText");
			Assert.assertEquals(actual, strexpected);
			System.out.println(strexpected + " exists as expected");
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	public String addDaysToCurrentDate(int days, String dateFormat) throws Exception {
		SimpleDateFormat dt = new SimpleDateFormat(dateFormat);
		Date dateObj = new Date();
		System.out.println("Today's date and time--->" + dt.format(dateObj));

		Calendar c = Calendar.getInstance();
		c.setTime(dateObj);

		c.add(Calendar.DATE, days);
		Date updatedDate = c.getTime();

		return dt.format(updatedDate);
	}

	public String addDaysToCurrentDate(int days) throws Exception {
		SimpleDateFormat dt = new SimpleDateFormat();
		Date dateObj = new Date();
		System.out.println("Today's date and time--->" + dt.format(dateObj));

		Calendar c = Calendar.getInstance();
		c.setTime(dateObj);
		c.add(Calendar.DATE, days);
		Date updatedDate = c.getTime();

		return dt.format(updatedDate);
	}

	/** -----------------------------Date Methods End----------------------- **/
	/** -----------------------------HTML Reports start----------------------- **/
	/*** PassReport ***/
	/*
	 * public void passReport(String stepDescription, String expectedResult , String
	 * actualResult ) throws Exception { HTMLReport.updateDetailsHTMLReport(
	 * stepDescription,
	 * "<b>"+expectedResult.trim()+"<b>","<font color=green ><b><U>"+actualResult.
	 * trim()+"</U></b></font>"); }
	 * 
	 *//*** FailReport ***/
	/*
	 * public void failReport(String stepDescription, String expectedResult, String
	 * actualResult) throws Exception { HTMLReport.updateDetailsHTMLReport(
	 * stepDescription,
	 * "<b>"+expectedResult.trim()+"<b>","<font color=red ><b><U>"+actualResult.trim
	 * ()+"</U></b></font>"); Assert.fail(actualResult.trim());
	 * //fail(actualResult.trim()); }
	 * 
	 *//*** WarnReport ***//*
							 * public void warnReport(String stepDescription, String expectedResult, String
							 * actualResult) throws Exception {
							 * HTMLReport.updateDetailsHTMLReport(stepDescription,
							 * "<b>"+expectedResult.trim()+"<b>","<font color=yellow ><b><U>"+actualResult.
							 * trim()+"</U></b></font>"); Assert.fail(actualResult.trim()); }
							 */

	/** -----------------------------HTML Reports End----------------------- **/
	/**
	 * -----------------------------Database Methods start-----------------------
	 **/

	@SuppressWarnings("static-access")
	public void connectDB(String dbUrl, String dbUser, String dbPwd, String dbType)
			throws SQLException, ClassNotFoundException {
		System.out.println(dbUrl + "\n" + dbUser + "\n" + dbPwd);
		if (dbType.equalsIgnoreCase("oracle"))
			Class.forName("oracle.jdbc.driver.OracleDriver"); // step1 load the driver class
		if (dbType.equalsIgnoreCase("sql"))
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); // step1 load the driver class
		try {
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPwd); // step2 create the connection object
			System.out.println(dbType + " DB connected");
			// passReport("DB connection should be successful and is successfully connected
			// --> Url::"+dbUrl+"UserID::"+dbUser +"PASSWORD::"+dbPwd);
			statement = connection.createStatement(resultset.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY); // step3
																													// create
																													// the
																													// statement
																													// object
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void disConnectDB() throws Exception {
		if (statement != null) {
			statement.close();
			System.out.println("Trying to Disconncet DB::" + "DB dis-Connected Sucessfully");
		} else
			System.out.println("Data Base Connection not opened..unable to Close the connection");
	}

	public String getColumnValue(String columnName, String Query) throws Exception {
		String colval = null;
		try {
			System.out.println("ExecuteQuery, Query: " + Query + " for Column:" + columnName);
			resultset = statement.executeQuery(Query);
			System.out.println(resultset);
			resultset.next();
			colval = resultset.getString(columnName);
			System.out.println(colval);
			System.out.println("Column value:" + colval + "Succesfully feteched");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Please check the Cloumn values in DB::" + "Column have no values");

			e.printStackTrace();
		}
		return colval;
	}

	public List<String> getDBColumnValMultiple(String Query) throws SQLException, IOException, ParseException {
		System.out.println("Query: " + Query);
		resultset = statement.executeQuery(Query);
		System.out.println(resultset);
		List<String> rowValue = new ArrayList<String>();
		while (resultset.next()) {
			String temp = resultset.getString(1);
			rowValue.add(temp);
		}
		// Log.info(rowValue);
		return rowValue;
	}

	public String getColumnValue(String query, int column) throws Exception {
		resultset = statement.executeQuery(query);
		System.out.println(resultset);
		String temp = null;
		if (resultset.first())
			temp = resultset.getString(column);
		System.out.println(query + "\n" + temp);
		return temp;
	}

	public int getDBRowCount(String query) throws Exception {
		System.out.println("Query: " + query + "");
		resultset = statement.executeQuery(query);
		System.out.println(resultset);
		int count = 0;
		while (resultset.next()) {
			count++;
		}
		return count;
	}

	public void validateDBColumns(String query, String expVal1, String expVal2, int col1, int col2) throws Exception {
		System.out.println("Validate column values as ==> '" + expVal1 + " and " + expVal2 + "' in query==> " + query
				+ " for the columns" + col1 + " and " + col2);
		resultset = statement.executeQuery(query);
		System.out.println(resultset);
		int count = 0;
		String actVal1 = null, actVal2 = null;
		while (resultset.next()) {
			actVal1 = resultset.getString(col1).trim();
			actVal2 = resultset.getString(col2).trim();
			System.out.println("actVal1:: " + actVal1 + " and actVal2:: " + actVal2);
			if (actVal1.equalsIgnoreCase(expVal1.trim()) && actVal2.equalsIgnoreCase(expVal2.trim())) {
				System.out.println("Columns matched -->" + " expVal1:: '" + expVal1 + " actVal1:: " + actVal1
						+ "' and expVal2:: '" + expVal2 + " actVal2:: " + actVal2 + "'");
				System.out.println("Coloumn values must match as expected and Columns matched -->" + " expVal1:: '"
						+ expVal1 + " actVal1:: " + actVal1 + "' and expVal2:: '" + expVal2 + " actVal2:: " + actVal2
						+ "'");
				count++;
				break;
			}
		}
		if (count == 0) {
			System.out.println("Columns did not match -->" + "expVal1::" + expVal1 + "actVal1::" + actVal1
					+ " and expVal2::" + expVal2 + "actVal2::" + actVal2);
			// System.out.println("Coloumn values must match as expected but Columns did not
			// match -->"+" expVal1:: '"+expVal1+" actVal1:: "+actVal1+"' and expVal2:: '"+
			// expVal2+" actVal2:: "+actVal2+"'",false);
		}
	}

	public static void wait(int d) {
		try {
//			 Thread.sleep((long) (d * 1000));
			driver.manage().timeouts().implicitlyWait(d * 10, TimeUnit.SECONDS);

		} catch (Exception e) {

		}
	}

	public static void waits(int d) {
		try {
			Thread.sleep((long) (d * 1000));
		} catch (Exception e) {

		}
	}

	public static void waitAndLog(int d) {
		try {
			Thread.sleep((long) (d * 1000));
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public static void implicitwaitAndLog(int d) {
		try {
			driver.manage().timeouts().implicitlyWait(d * 10, TimeUnit.SECONDS);
			logger.info("Wait for " + Integer.toString(d) + " seconds:");
		} catch (Exception e) {
			// Ignore exception
		}
	}

	public boolean verifyElementIsEnabled(WebElement element) {
		boolean flag;
		if (element.isEnabled()) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

}
