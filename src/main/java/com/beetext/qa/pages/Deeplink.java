package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Deeplink extends TestBase {

	public Deeplink() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}
	
		@FindBy(xpath = "//input[@id='login']")
		public WebElement mail_textbar;
		
		@FindBy(xpath = "//button[@title='Check Inbox @yopmail.com']")
		public WebElement mail_Submit;
		
		@FindBy(xpath = "//iframe[@id='ifmail']")
		public WebElement mail_2ndtab;
		
		@FindBy(xpath = "//button[contains(text(),'Reply on Beetexting')]")
		public WebElement reply_Link;
		
		@FindBy(xpath = "(//span[contains(text(),'hitextbee@gmail.com')])[1]")
		public WebElement mail1;
		
		@FindBy(xpath = "(//span[@title='Auto12'])[1]")
		public WebElement Auto1Login;
		
		@FindBy(xpath = "//h4[contains(text(),'Invalid deeplink Id.')]")
		public WebElement Invalid_Deeplink;
		
		@FindBy(xpath = "//div[@id='hoverSelectDepartment']")
		public WebElement department_Head;
		
		@FindBy(xpath = "(//span[@title='Dummy'])[1]")
		public WebElement Dummy_department;
		
		@FindBy(xpath = "//span[@id='Auto1_Department']")
		public WebElement Auto1_department;
		
		@FindBy(xpath = "//span[@id='Auto2_Department']")
		public WebElement Auto2_department;
		
		@FindBy(xpath = "//button[contains(text(),'Toll-Free')]")
		public WebElement TollFree_Button_department;
		
		@FindBy(xpath = "//span[@title='Automation TollFree']")
		public WebElement TollFree_department;
		
		@FindBy(xpath = "//app-svg-icon[@id='pinIcon']")
		public WebElement PinIcon;
		
		@FindBy(xpath = "//li[@id='temp-active-agents-list']//div[contains(text(),'auto2')]")
		public WebElement PinIcon_auto2;
		
		@FindBy(xpath = "//h4[contains(text(),'Department not found.')]")
		public WebElement Dept_Not_Found;
		
		@FindBy(xpath = "//a[@id='settingsClose']")
		public WebElement Settings_Close;
		
		@FindBy(xpath = "//a[@id='settingsMainTab']")
		public static WebElement settings;
		
		@FindBy(xpath = "//a[@id='NumbersTab']")
		public static WebElement settings_Numbers;
		
		@FindBy(xpath = "//a[@id='UsersTab']")
		public static WebElement settings_Agents;
		
		@FindBy(xpath = "//div[@class='agent-name inline-display'][contains(text(),'Automation - Dummy')]")
		public static WebElement AutomationDummy_pin;
		
		@FindBy(xpath = "//button[@id='Automation - DummymanageButton']")
		public static WebElement Automation_dummy_Agent_Manage;
		
		@FindBy(xpath = "//app-check-box[@id='DummycheckBox']")
		public static WebElement Department_Dummy_Checkbox;
		
		@FindBy(xpath = "//button[@id='manageAgentButton']")
		public static WebElement Save_Agent_Changes;
	
}
