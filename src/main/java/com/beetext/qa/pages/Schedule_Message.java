package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Schedule_Message extends TestBase {
	public Schedule_Message() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//h4[normalize-space()='QA10']")
	public WebElement select_conversation12;

	@FindBy(xpath = "//h4[normalize-space()='QA10, hello']")
	public WebElement select_group_conversation;
	// compose new

	@FindBy(xpath = "//*[@class='compose-new-icon']")
	public WebElement click_compose_icon;

	@FindBy(xpath = "//input[@id='contactSearchInput']")
	public WebElement click_compose_search_contact;

	@FindBy(xpath = "//span[contains(text(),'QA10')]")
	public WebElement click_compose_select_contact;
	@FindBy(xpath = "//span[contains(text(),'QA123')]")
	public WebElement click_compose_select_contact2;
	@FindBy(xpath = "//span[contains(text(),'hello')]")
	public WebElement click_compose_select_contact3;
	@FindBy(xpath = "//span[@id='+15553847984searchList']")
	public WebElement click_compose_select_contact4;

	@FindBy(xpath = "//button[@name='button'][6]")
	public WebElement schedule_icon;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement input_box;

	@FindBy(xpath = "//span[normalize-space()='Set']")
	public WebElement set_date_time;

	@FindBy(xpath = " //span[contains(text(),'Cancel')]")
	public WebElement cancel_set_date_time;

	@FindBy(xpath = "//button[@aria-label='Minus a hour']//span[@class='owl-dt-control-button-content']//*[name()='svg']")
	public WebElement reducehour;

	@FindBy(xpath = "//button[@aria-label='Add a hour']//span[@class='owl-dt-control-button-content']//*[name()='svg']")
	public WebElement increasehour;
	@FindBy(xpath = "//button[@id='chatInput-sendButton']")
	public WebElement schedule_button;
	// div[contains(text(),'Your message has been successfully scheduled to QA10.')]
	// div[contains(text(),'Your message for QA10 is scheduled to send at')]
	// div[@aria-label='Your message for QA10 is scheduled to send on ']
	@FindBy(xpath = "//div[contains(text(),'Your text for QA10 is scheduled to send on')]")
	public WebElement schdule_toaster_message;
	// div[contains(text(),'Your message has been successfully scheduled to
	// Group.')]
	// div[contains(text(),'Your message for this Group is scheduled to send at')]
	// div[@aria-label='Your text for this Group is scheduled to send on
	// 11/03/2021.']
	@FindBy(xpath = "//div[contains(text(),'Your text for this Group is scheduled to send on')]")
	public WebElement group_schdule_toaster_message;

	@FindBy(xpath = "//p[contains(text(),'Please select a date and time in the future.')]")
	public WebElement past_DateTime;

	@FindBy(xpath = "//button[contains(text(),'OK')]")
	public WebElement ok_button;
	// attachment link
	@FindBy(xpath = "(//button[@class='btn btn-link btn-chat-link'])[1]")
	public WebElement pin_attachment;

	@FindBy(xpath = "//*[contains(text(),'Maximum 9 contacts are allowed.')]")
	public WebElement maxtencontactsallowed;
	@FindBy(xpath = "(//a[@id='contactSelectNewNumber'])")
	public WebElement selectNewcontact;
	// button[contains(text(),'OK')]
	@FindBy(xpath = "//button[contains(text(),'OK')]")
	public WebElement okbutton;

	// a[@id='contactSelectNewNumber']
}
