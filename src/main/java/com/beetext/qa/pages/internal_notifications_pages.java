package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class internal_notifications_pages extends TestBase {

	public internal_notifications_pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//a[@id='notification']")
	public WebElement notification;
	
	@FindBy(xpath = "//button[contains(text(),'UPDATE NOTIFICATIONS')]")
	public WebElement update_notifications;
	
	@FindBy(xpath = "//div[contains(text(),' Notifications settings updated successfully. ')]")
	public WebElement notification_updated_Successfully;
	
	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[1]")
	public WebElement new_Msgs_email;
	
	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[2]")
	public WebElement new_Msgs_web_banner;
	
	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[3]")
	public WebElement internal_Mentions_email;
	
	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[4]")
	public WebElement internal_mentions_web_banner;
	
	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[5]")
	public WebElement Conversation_transfered_email;
	
	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[6]")
	public WebElement Conversation_transfered_web_banner;
}
