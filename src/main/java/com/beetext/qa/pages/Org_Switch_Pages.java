package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Org_Switch_Pages extends TestBase {

	public Org_Switch_Pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "(//*[@class='org-right-arrow'])[1]")
	public WebElement Arrow;

	@FindBy(xpath = "(//app-svg-icon[@class='add-plus-icon'])[1]")
	public WebElement add_org;

	@FindBy(xpath = "//span[@class='ll-title another-org']")
	public WebElement add_org_text;

	@FindBy(xpath = "//a[@class='org-close ']")
	public WebElement add_org_Close_Button;

	@FindBy(xpath = "//button[@type='submit']")
	public WebElement letsgobttn;

	@FindBy(xpath = "//div[@id='product-owner']")
	public WebElement productname;

	@FindBy(xpath = "//div[contains(text(),'Invalid Email ID and/or Password.')]")
	public WebElement invalid_Details_error;

	@FindBy(xpath = "//div[contains(text(),' Email not valid. ')]")
	public WebElement email_notValid;

	@FindBy(xpath = "//div[contains(text(),' You are already signed-in for this organization on this device. ')]")
	public WebElement alreadySign_in;

	@FindBy(xpath = "//span[contains(text(),'Auto12 ')]")
	public WebElement auto1_login;

	@FindBy(xpath = "(//ul[@class='org-check-list']//span[contains(text(),'Auto12 ')])[1]")
	public WebElement addOrg_auto1_login;

	@FindBy(xpath = "(//ul[@class='org-check-list']//span[contains(text(),'Auto12 ')])[1]")
	public WebElement addOrg_automation_login;

	@FindBy(xpath = "(//span[@class='checkmark'])[1]")
	public WebElement auto1_radiobttn;

	@FindBy(xpath = "(//span[@class='checkmark'])[2]")
	public WebElement automation_radiobttn;

	@FindBy(xpath = "//span[@class='count icon-badge total-org-unreadCount cursorPointer ng-star-inserted']")
	public WebElement batch_count;

}
