package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class SettingsOrganizationNameEdit extends TestBase {
	
	// Initializing the page objects:
			public  SettingsOrganizationNameEdit() {
				PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
			}
			
			
			@FindBy(xpath="//span[@class='s-title']")
			public
			WebElement  Settings_link;
			
			@FindBy(xpath="//a[@id='OrganizationTab']")
			public
			WebElement  organization_link;
			
			@FindBy(xpath=" //input[@id='organizationName']")
			public
			WebElement  organization_name_edit;
			
			@FindBy(xpath="//div[@id='nameUnavailableError']")
			public
			WebElement  organization_name_required;
			
			@FindBy(xpath=" //div[@id='nameMinimumLengthError']")
			public
			WebElement  organization_name_min_3char;
			
			@FindBy(xpath=" //button[@id='organizationSaveBtn']")
			public
			WebElement  saveButton;

			//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']
			
			@FindBy(xpath="//div[contains(text(),'Updated organization successfully.')]")
			public
			WebElement  ToasterMessage;
			
			@FindBy(xpath="//div[contains(text(),'Duplicate Organization Name')]")
			public
			WebElement  Duplicate_orgname;

}
