package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class ChangePassword extends TestBase {

	public ChangePassword() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")
	public WebElement threedots;
	//a[@routerlink='profile']
	@FindBy(xpath = "//body/app-root[1]/div[1]/app-layout[1]/main[1]/aside[1]/ul[1]/li[7]/div[1]/ul[1]/li[1]/a[1]")
	public WebElement profile_link;
	//a[@id='ngb-tab-3']
	@FindBy(xpath = "//a[contains(text(),'Change Password')]")
	public WebElement change_password_link;
	@FindBy(xpath="//a[@id='notification']")
	public WebElement notification_link;
	
	@FindBy(xpath = "//input[@name='currentPassword']")
	public WebElement current_password;
	
	@FindBy(xpath = "//div[contains(text(),'Current password is required.')]")
	public WebElement current_password_required;
	
	@FindBy(xpath = "//input[@name='newPassword']")
	public WebElement new_password;
	
	@FindBy(xpath = "//div[contains(text(),'New password is required.')]")
	public WebElement new_password_required;
	
	@FindBy(xpath = "//input[@name='confirmPassword']")
	public WebElement confirm_new_password;
	@FindBy(xpath = "//div[contains(text(),'Re-Type password is required.')]")
	public WebElement confirm_new_password_required;
	
	
	@FindBy(xpath = "//div[contains(text(),'Current password must be at least 8 characters.')]")
	public WebElement current_password_min_8_char;
	
	@FindBy(xpath = "//div[contains(text(),'New password must be at least 8 characters.')]")
	public WebElement new_password_min_8_char;
	
	@FindBy(xpath = "//div[contains(text(),'Re-Type password must be at least 8 characters.')]")
	public WebElement confirm_new_password_min_8_char;
	
	
	@FindBy(xpath="//button[normalize-space()='Save']")
	public WebElement save_Button;
	
	@FindBy(xpath="//button[normalize-space()='Cancel']")
	public WebElement cancel_Button;
	
	@FindBy(xpath="//div[@aria-label='Password Changed successfully.']")
	public WebElement toaster_message;
	
	@FindBy(xpath="//div[@aria-label='New password is matching with current password, please change.']")
	public WebElement wrong_toaster_message;
	
	
	
	
	@FindBy(xpath="//div[contains(text(),'Your passwords do not match. Please retype them an')]")
	public WebElement confirm_password_wrong_toaster_message;
	
	
	@FindBy(xpath="//div[@aria-label='The password you entered is not your current password. Try again or logout to reset your password.']")
	public WebElement  current_password_wrong_toaster_message;
	
	
	
	@FindBy(xpath="//*[@class='cross-icon']//*[name()='svg']")
	public WebElement  close_button;
	
	


}
