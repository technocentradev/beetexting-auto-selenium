package com.beetext.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.beetext.qa.base.TestBase;
import com.beetext.qa.util.TestUtil;

public class Repository extends TestBase {

	// Page Factory - Object Rep:

	// Page Factory - Object Rep:
	@FindBy(id = "email-id")
	WebElement username;

	@FindBy(id = "password-id")
	public static WebElement password;

	@FindBy(id = "login-submit")
	WebElement loginBtn;

	@FindBy(xpath = "//button[@contains(text(),'Sign Up')]")
	WebElement signupBtn;

	@FindBy(xpath = "//img[contains(@class,'img-responsive')]")
	WebElement crmLogo;

	@FindBy(xpath = "//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")
	public WebElement signOutLink;

	@FindBy(xpath = "//a[@id='logoutId']")
	public WebElement signOut;

	@FindBy(id = "signupPage")
	public static WebElement signupPage;

	@FindBy(id = "orgName")
	public static WebElement orgName;

	@FindBy(id = "workEmail")
	public static WebElement workEmail;

	@FindBy(xpath = "//input[@id='confirmEmail']")
	public static WebElement confirmEmail;

	@FindBy(xpath = "//input[@id='agentName']")
	public static WebElement name;

	@FindBy(xpath = "//input[@name='verificationCode']")
	public static WebElement mobileNumber;

	@FindBy(xpath = "//button[normalize-space()='SEND VERIFICATION CODE']")
	public static WebElement send_verfity_code;

	@FindBy(xpath = "//div/input[1]")
	public static WebElement mobile_otp1;
	@FindBy(xpath = "//div/input[2]")
	public static WebElement mobile_otp2;
	@FindBy(xpath = "//div/input[3]")
	public static WebElement mobile_otp3;
	@FindBy(xpath = "//div/input[4]")
	public static WebElement mobile_otp4;
	@FindBy(xpath = "//div/input[5]")
	public static WebElement mobile_otp5;
	@FindBy(xpath = "//div/input[6]")
	public static WebElement mobile_otp6;

	@FindBy(xpath = "//input[@id='password']")
	public static WebElement signuppassword;

	@FindBy(xpath = "//input[@id='retypePassword']")
	public static WebElement retypePassword;

	@FindBy(xpath = "//*[text()=' Your organization name needs 3 or more characters. ']")
	public static WebElement Orgnameneedmorechar;

	@FindBy(xpath = "//div[contains(text(),'Email not valid. ')]")
	public static WebElement emailvalidation;

	@FindBy(xpath = "//div[contains(text(),'Confirm Work Email not valid. ')]")
	public static WebElement cnfmEmailnotvalid;

	@FindBy(xpath = "//div[contains(text(),'Your Emails do not match. Please retype them and try again!')]")
	public static WebElement retypecnfmmail;

	@FindBy(xpath = "//div[contains(text(),'Name must be at least 3 characters.')]")
	public static WebElement Namemust3char;

	@FindBy(xpath = "//div[contains(text(), ' Password must be at least 8 characters. ')]")
	public static WebElement Passwordmustcharacters;

	@FindBy(xpath = "//div[contains(text(), 'Re-Type Password must be at least 8 characters. ')]")
	public static WebElement ReTypePwdmustchar;

	@FindBy(xpath = "//div[contains(text(),' Your passwords do not match. Please retype them and try again! ')]")
	public static WebElement Repwdnotmath;

	@FindBy(xpath = "//div[contains(text(),'Please read the terms of service and verify that you agree.')]")
	public static WebElement Plzreadtermsandcond;

	@FindBy(xpath = "//div[contains(text(),'Organization Name is required.')]")
	public static WebElement OrgError;

	@FindBy(xpath = "//div[contains(text(),'Work Email is required.')]")
	public static WebElement workEmailError;

	@FindBy(xpath = "//div[contains(text(),' Confirm Work Email is required. ')]")
	public static WebElement CnfmEmailError;

	@FindBy(xpath = "//div[contains(text(),'Name is required.')]")
	public static WebElement nameRequiredError;

	@FindBy(xpath = "//div[contains(text(),'Mobile Number is required.')]")
	public static WebElement mobilNumberError;

	@FindBy(xpath = "//div[contains(text(),'Password is required.')]")
	public static WebElement passwordRequiredError;

	@FindBy(xpath = "//div[contains(text(),'Re-Type Password is required.')]")
	public static WebElement reTypePasswordRequiredError;

	@FindBy(xpath = "//span[@class='checkmark']")
	public static WebElement sighupcheckbox;

	@FindBy(xpath = "//a[contains(text(),'Terms of Service')]")
	public static WebElement termscond;

	@FindBy(xpath = "//a[contains(text(),'terms of service')]")
	public static WebElement TermsofService;

	@FindBy(xpath = "//a[text()='privacy policy']")
	public static WebElement PrivacyPolicy;

	@FindBy(xpath = "//a[text()='Pricing']")
	public static WebElement Pricing;

	@FindBy(xpath = "//*[@id='saveButton']")
	public static WebElement letsgobtn;

	@FindBy(xpath = "//div[contains(text(),'Organization name already exists. Please use another organization name.')]")
	public static WebElement Orgnamealreadyexist;

	@FindBy(xpath = "//div[contains(text(),'Organization created successfully.')]")
	public static WebElement orgcresucctoastermsg;

	@FindBy(xpath = "//div[contains(text(),' This email ID is already in use. Sign in or try a different email ID. ')]")
	public static WebElement EmailalreExisterr;

	@FindBy(xpath = "//*[contains(text(),'RESEND')]")
	public static WebElement resend;

	@FindBy(xpath = "//button[normalize-space()='VERIFY']")
	public static WebElement verify_bttn;

	@FindBy(xpath = "//*[contains(@name,'verificationCode')]")
	public static WebElement otptext;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement OtpInvalidErr;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement MobOtp_ResendToaster;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement MobVerifiedToaster;

	@FindBy(xpath = "//button[normalize-space()='VERIFY']")
	public static WebElement Email_VerifyBttn;

	@FindBy(xpath = "//*[contains(@name,'verificationCode')]")
	public static WebElement Email_otptext;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement Email_OtpInvalidErr;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement Email_ResendToaster;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement Email_VerifiedToaster;

	@FindBy(xpath = "//*[@id='creditCardButton']")
	public static WebElement creditcard_bttn;

	@FindBy(xpath = "//*[@id='achButton']")
	public static WebElement Ach_bttn;

	@FindBy(xpath = "//*[@id='cc-name']")
	public static WebElement name_onthe_card;

	@FindBy(xpath = "//*[@id='cc-name']")
	public static WebElement Cname;
	@FindBy(xpath = "//*[@id='cc-number']")
	public static WebElement CardNumber;

	@FindBy(xpath = "//*[@id='cc-exp-date']")
	public static WebElement ExpiryDate;

	@FindBy(xpath = "//*[@id='cc-cvc']")
	public static WebElement CC_Code;

	@FindBy(xpath = "//*[@id='ccDetailsNext']")
	public static WebElement Next_PaymentInformation;

	@FindBy(xpath = "//span[@class='checkmark']")
	public static WebElement CreditCardPayment_checkbox;

	@FindBy(xpath = "//*[contains(text(),'Card number is invalid.')]")
	public static WebElement CardNum_Invalid;

	@FindBy(xpath = "//*[contains(text(),'Expiration date is invalid.')]")
	public static WebElement ExpDate_Invalid;

	@FindBy(xpath = "//*[contains(text(),'Security code must be at least 3 digits.' )]")
	public static WebElement Cvv_Mustbe3char;

	@FindBy(xpath = "//*[contains(text(),' Security code is invalid.')]")
	public static WebElement Cvv_invalid;

	@FindBy(xpath = "//span[@class='checkmark']")
	public static WebElement Accept_autopayments;

	@FindBy(xpath = "//*[contains(text(),'Create a new phone number' )]")
	public static WebElement Create_Newphonenumber;

	@FindBy(xpath = "//*[@id='deptNewnumberName']")
	public static WebElement Create_dept;

	@FindBy(xpath = "//*[@id='deptNewAreacode']")
	public static WebElement Create_dept_areacode;

	@FindBy(xpath = "//button[@id='deptNewNumberCreateButton']")
	public static WebElement Create_dept_clickon_create;

	@FindBy(xpath = "//*[@id='email-id']")
	public static WebElement user_id;

	@FindBy(xpath = "//*[contains(text(),' Email not valid. ' )]")
	public static WebElement login_invalidmail;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement logindetailsinvalid;

	@FindBy(xpath = "//*[@id='password-id']")
	public static WebElement Passsword_ID;

	@FindBy(xpath = "//*[contains(text(),'Password must be at least 8 characters. ' )]")
	public static WebElement loginpassword_must8char;

	@FindBy(xpath = "//*[@id='login-submit']")
	public static WebElement loginbttn;

	@FindBy(xpath = "//button[contains(text(),'OK')]")
	public static WebElement max_Attachments_Popup_Ok;

	@FindBy(xpath = "//*[@id='ccDetailsBack']")
	public static WebElement backbttn_paymentpage;

	@FindBy(xpath = "//*[@id='achButton']")
	public static WebElement achpaymentbttn;

	@FindBy(xpath = "//span[text()='Continue']")
	public static WebElement achContinue;

	@FindBy(xpath = "//*[text()='Bank of America']")
	public static WebElement ach_bankselection;

	@FindBy(xpath = "//*[@id='username']")
	public static WebElement ach_username;

	@FindBy(xpath = "//*[@id='password']")
	public static WebElement ach_password;

	@FindBy(xpath = "//*[@id='aut-submit-button']")
	public static WebElement ach_submitbttn;

	@FindBy(xpath = "//span[@class='BaseInput-module_note__EmsWz']")
	public static WebElement ach_usernamerequired;

	@FindBy(xpath = "//span[@class='BaseInput-module_note__EmsWz']")
	public static WebElement ach_passworedrequried;

	@FindBy(xpath = "//*[@id='a11y-title']")
	public static WebElement ach_details_incorrect;

	@FindBy(xpath = "//*[contains(text(),'For security reasons, your account may be locked after several unsuccessful attempts' )]")
	public static WebElement ach_invaliddetails_errormsg;

	@FindBy(xpath = "//*[contains(text(),'Retry answer' )]")
	public static WebElement ach_retryanswer;

	@FindBy(xpath = "//*[contains(text(),'Try Another Bank' )]")
	public static WebElement ach_tryanotherbank;

	@FindBy(xpath = "//*[@id='forgotPasswordLink']")
	public static WebElement forgot_password;

	@FindBy(xpath = "//*[@id='forgotPasswordEmail']")
	public static WebElement forgot_passwordemail;

	@FindBy(xpath = "//*[@Id='forgotEmailSendBtn']")
	public static WebElement forgotpasswordemail_sendbttn;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement forgotpassword_emailsenttoaster;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement forgotpasswordemail_invalidmailtoaster;

	@FindBy(xpath = "//*[contains(text(),'Beetexting Support' )]")
	public static WebElement Msg_beetextsupport;

	@FindBy(xpath = "//span[text()='Auto1']")
	public static WebElement uname;

	@FindBy(xpath = "//*[@id='contactSearchInput']")
	public WebElement contactSearchInputTxt;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'Automation')]")
	public WebElement contactSearchOutputTxt;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'Auto1')]")
	public WebElement contactSearchOutputTxt1;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display']")
	public WebElement contactSearch_OP_Txt1;

	@FindBy(xpath = "//button[@id='exportConversation']")
	public WebElement exportConversation;

	@FindBy(xpath = "//a[@id='contactSelectNewNumber']")
	public WebElement contactSearchOutputTxt2;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),' QA10 ')]")
	public WebElement contactSearchOutputTxt3;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'sample')]")
	public WebElement contactSearchOutputTxt4;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'sample')]")
	public WebElement contactSearchOutputTxt5;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'sample')]")
	public WebElement contactSearchOutputTxt6;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'sample')]")
	public WebElement contactSearchOutputTxt7;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'555')]")
	public WebElement contactSearchOutputTxt8;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'890')]")
	public WebElement contactSearchOutputTxt9;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),' 456654 ')]")
	public WebElement contactSearchOutputTxt10;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display'][contains(text(),'QhvvzJhY')]")
	public WebElement contactSearchOutputTxt11;

	@FindBy(xpath = "//*[@id='contactSelectNewNumber']")
	public WebElement contactSearchOutputTxt12;

	@FindBy(xpath = "//div[contains(text(),'Toll Free number is not allowed for group.')]")
	public WebElement Toll_Not_Allowed_in_Group;

	@FindBy(xpath = "//input[@id='contactSearchInput' and @placeholder='Type Name(s) or Number(s) Here ']")
	public WebElement placeholder;

	@FindBy(xpath = "//div[@id='chat-container-scroller']")
	public WebElement starnewconversion;

	@FindBy(xpath = "//*[text()='Automation']")
	public WebElement automation_chat;

	@FindBy(xpath = "//h4[@id='+13068016736_conversation'][contains(text(),'Automation')]")
	public WebElement automation_chat_contact;

	@FindBy(xpath = "//span[contains(text(),'Actions')]")
	public WebElement actionbttninchat;

	@FindBy(xpath = "//button[@id='transferList']")
	public WebElement transferlist_inchat;

	@FindBy(xpath = "//*[contains(text(),' auto2 ')]")
	public WebElement transferlist_changetoautomation;

	@FindBy(xpath = "//*[contains(text(),' Conversation claimed by ')]")
	public WebElement transferlist_conversatinclaimedmsg;

	@FindBy(xpath = "//*[@id='chatActions-ClaimBtn']")
	public WebElement conversation_claimbttn;

	@FindBy(xpath = "//*[contains(text(),' Conversation claimed by ')]")
	public WebElement conversation_claimtxt;

	@FindBy(xpath = "//*[@id='chatActions-UnclaimBtn']")
	public WebElement conversation_unclaimbttn;

	@FindBy(xpath = "//*[contains(text(),' Conversation Unclaimed by ')]")
	public WebElement conversation_unclaimtxt;

	@FindBy(xpath = "//*[@class='checkmark ']")
	public static WebElement conversation_resolved;

	@FindBy(xpath = "//*[contains(text(),' Resolved by ')]")
	public static WebElement conversation_resolvedtxt;

	@FindBy(xpath = "//*[contains(text(),'Reopened by ')]")
	public static WebElement conversation_reopentxt;

	@FindBy(xpath = "//h4[contains(text(),'mvr')]")
	public static WebElement conversation_mvrcontact;

	@FindBy(xpath = "//button[@id='archiveConversation']")
	public static WebElement conversation_Archive_bttn;

	@FindBy(xpath = "//*[contains(text(),' Conversation Archived by ')]")
	public static WebElement conversation_Archivedmsg;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement chatInputTextareaTxt;

	@FindBy(xpath = "//button[@class='btn btn-link emojiSendbtn btn-action-sm']")
	public WebElement chatInputsendButton;

	@FindBy(xpath = "//span[text()='Send']")
	public WebElement chatInputsendButton1;

	@FindBy(xpath = "//*[@id='composenewBtn']")
	public WebElement composebutton;

	@FindBy(id = "chatInput-sendButton")
	public WebElement chatInputsendButton2;

	@FindBy(xpath = "//*[@id='chatInput-sendNowButton']")
	public WebElement chatInputsendnowButton;

	@FindBy(xpath = "//*[@class ='messages ng-star-inserted']")
	public WebElement message;

	@FindBy(xpath = "//*[contains(text(),'Maximum 9 contacts are allowed.')]")
	public WebElement maxtencontactsallowed;

	@FindBy(xpath = "//*[contains(text(),' Contact')]")
	public WebElement contactinChat;

	@FindBy(xpath = "//*[@id='blockContact']")
	public WebElement blockcontact;

	@FindBy(xpath = "(//button[@class='btn btn-link btn-chat-link'])[1]")
	public WebElement pin_attachment;

	@FindBy(xpath = "//img[@class='attachment-image-img ng-star-inserted']")
	public WebElement image_verification;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted'])[1]")
	public WebElement video_verification;

	@FindBy(xpath = "//*[@class='attachments-container attachment-file-types ng-star-inserted']")
	public WebElement audio_verification;

	@FindBy(xpath = "//*[contains(text(),'Attachment can be downloaded from')]")
	public WebElement receviedsidemsg;

	@FindBy(xpath = "//*[@class='icon filter-icon ng-tns-c4-0 ng-star-inserted']")
	public WebElement filterbttn;

	@FindBy(xpath = "(//*[@class='dropdown-menu conversation ng-tns-c6-2 ng-star-inserted show']//*[@class='checkmark '])[1]")
	public WebElement filter_claimedbyme;

	@FindBy(xpath = "//li[contains(text(),' Claimed by Me ')]//span[@class='checkmark ']")
	public WebElement filter_claimed_by_me;

	@FindBy(xpath = "//li[contains(text(),' Unclaimed')]//span[@class='checkmark ']")
	public WebElement unclaimed;

	@FindBy(xpath = "//li[contains(text(),' Unresolved')]//span[@class='checkmark ']")
	public WebElement unresolved;

	@FindBy(xpath = "//li[contains(text(),' Unread')]//span[@class='checkmark ']")
	public WebElement unread;

	@FindBy(xpath = "(//div[@class='btn-group0 show dropdown']//span[@class='check-box'])[5]")
	public WebElement filter_archived;

	@FindBy(xpath = "//*[contains(text(),' Conversation Archived by ')]//*[contains(text(),'You.')]")
	public WebElement conversation_archivedtxt;

	@FindBy(xpath = "//*[@class='btn contact-save']")
	public WebElement filter_apply;

	@FindBy(xpath = "//button[@id='restoreConversation']")
	public WebElement restore_Conversation;

	@FindBy(xpath = "//*[@class='icon emoji-icon']")
	public WebElement emojibttn;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[1]")
	public WebElement emoji_like;

	@FindBy(xpath = "//*[@class='icon giphy-icon emoji-icon']")
	public WebElement giphybttn;

	@FindBy(xpath = "//*[@placeholder='Search']")
	public WebElement gif_search;

	@FindBy(xpath = "//*[@id='giphy-container-body'][1]")
	public WebElement giphyselection;

	@FindBy(xpath = "//*[@id='giphy-container-body'][2]")
	public WebElement giphyselection1;

	@FindBy(xpath = "//*[@id='giphy-container-body'][3]")
	public WebElement giphyselection2;

	@FindBy(xpath = "//div[@id='main-modal']")
	public WebElement giphy_outside;

	@FindBy(xpath = "(//a[contains(text(),'View Terms')])[1]")
	public WebElement giphy_viewsTerms;

	@FindBy(xpath = "//p[text()='Maximum 10 attachments are allowed.']")
	public WebElement max10attachmentspopup;

	@FindBy(xpath = "//*[@placeholder='Drop your files here']")
	public WebElement dropping_area;

	@FindBy(xpath = "//ul[@id='bottom-items']//div[@role='group1']/ul[@class='dropdown-menu show']//a[@title='AutomationUs1']")
	public WebElement clickon_username_validatesignout;

	@FindBy(xpath = "//*[@class='dropdown-menu show']//a[contains(text(),'AutoTech')]")
	public WebElement username_signature;

	@FindBy(xpath = "//*[@class='mat-slide-toggle-thumb']")
	public WebElement signature_toggle_On_Off;

	@FindBy(xpath = "//*[@class='form-control cdk-textarea-autosize ng-pristine ng-valid ng-touched']")
	public WebElement signature_text;

	@FindBy(xpath = "//*[@class='mat-slide-toggle-input cdk-visually-hidden']")
	public WebElement signature_enable_toggle;

	@FindBy(xpath = "//*[@class='btn btn-primary all-action-btns btn-info']")
	public WebElement signature_save;

	@FindBy(xpath = "//button[contains(text(),'Cancel')]")
	public WebElement signature_cancel;

	@FindBy(xpath = "//*[@class='char-count']")
	public WebElement signature_count;

	@FindBy(xpath = "//a[@class='download-icon']")
	public WebElement download_file;

	@FindBy(xpath = "//button[contains(text(),'Close')]")
	public WebElement popup_close;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement pinmessage_textarea;

	@FindBy(xpath = "//*[@id='blockContact']")
	public WebElement contact_blocked;

	@FindBy(xpath = "//li[contains(text(),'Blocked')]//span[@class='checkmark ']")
	public WebElement contact_filter_blocked;

	@FindBy(xpath = "//a[@id='contactTab']")
	public WebElement contact;

	@FindBy(xpath = "//svg-icon[@class='filter-icon']")
	public WebElement contact_filter;

	@FindBy(xpath = "//button[@class='btn btn-action-sm']")
	public WebElement filter_clear;

	@FindBy(xpath = "//div[contains(text(),' No conversation found! ')]")
	public WebElement no_Conversation_in_Filter;

	@FindBy(xpath = "(//app-svg-icon[contains(@class,'icon filter-icon')])[2]")
	public WebElement contact_filterIcon;

	@FindBy(xpath = "//app-svg-icon[contains(@class,'icon filter-icon')]")
	public WebElement Filter_Conversation;

	@FindBy(xpath = "//li[contains(text(),' Archived ')]//span[@class='checkmark ']")
	public WebElement Archived_filter_button;

	@FindBy(xpath = "(//span[@class='checkmark '])[2]")
	public WebElement contact_filterIcon_Blocked;

	@FindBy(xpath = "//div[@class='btn-group0 dropdown']//app-svg-icon[@id='blue-icon']")
	public WebElement C_filter;

	@FindBy(xpath = "//a[@class='filter-link menu-item']")
	public WebElement contact_filter_filter;

	@FindBy(xpath = "//*[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement contact_blocked_toaster;

	@FindBy(xpath = "//*[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement contact_unblockedtoaster;

	@FindBy(xpath = "//span[contains(text(),'Unblock')]")
	public WebElement contact_unblock;

	@FindBy(xpath = "//h4[@class='col-10 p-0 recipient-name ng-star-inserted'][@id='+13068016736_conversation'][contains(text(),'Automation')]")
	public WebElement automation_contact_unblock;

	@FindBy(xpath = "//*[@id='unblockContact']")
	public WebElement contact_unblocked;

	@FindBy(xpath = "//h4[@class='col-10 p-0 recipient-name ng-star-inserted b']")
	public WebElement contactunblock;

	@FindBy(xpath = "//div[@class='message-head d-flex justify-content-between align-content-center bottom-pad']//h4[contains(text(),'Automation')]")
	public WebElement contact_filter_unblock;

	@FindBy(xpath = "//div[contains(text(),'Automation successfully Unblocked.')]")
	public WebElement contact_unblocked_toaster;

	@FindBy(xpath = "//h4[@id='3068016736_conversation']")
	public WebElement clickon_automation_contact;

	// pin message xpath
	@FindBy(xpath = "//h4[contains(text(),'QA10')]")
	public WebElement select_conversation;

	@FindBy(xpath = " //*[@id='pinIcon']")
	public WebElement pinIcon;

	@FindBy(xpath = "//div[contains(text(),'Teammates')]")
	public WebElement Teammates_popup;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement input_box;

	@FindBy(xpath = "//div[contains(text(),'automation')]")
	public WebElement selectAgentName;

	@FindBy(xpath = "//div[contains(text(),'dummy')]")
	public WebElement selectsecondAgentName;

	@FindBy(xpath = "//div[contains(text(),\"We didn't find any matches.\")]")
	public WebElement no_Agent_Found;

	@FindBy(xpath = "//button[@id='chatInput-sendButton']")
	public WebElement postButton;

	@FindBy(xpath = "//*[@class ='messages ng-star-inserted']")
	public WebElement pinmessage;

	@FindBy(xpath = "//a[@id='toolsMainTab']")
	public WebElement tools_bttn;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement tools_tags_bttn;

	@FindBy(xpath = "//button[@id='createTagButton']")
	public WebElement tools_tags_createtag;

	@FindBy(xpath = "//input[@id='tag_searchContact']")
	public WebElement tools_tags_createtag_search_contact;

	@FindBy(xpath = "(//*[@id='tagInput'])[1]")
	public WebElement tools_tags_createtag_taginput;

	@FindBy(xpath = "//*[@id='tag_CreateButton']")
	public WebElement tools_tags_createtag_createbbtn;

	@FindBy(xpath = "//*[@class='mb-0']")
	public WebElement tools_tags_createtag_displaycount;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[1]")
	public WebElement tools_tags_createtag_applyall;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[2]")
	public WebElement tools_tags_createtag_select_contact;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[3]")
	public WebElement tools_tags_createtag_select_contact1;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[4]")
	public WebElement tools_tags_createtag_select_contact2;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[5]")
	public WebElement tools_tags_createtag_select_contact3;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[5]")
	public WebElement tools_tags_createtag_select_contact4;

	@FindBy(xpath = "//div[contains(text(),'Tag already exists and cannot be added again.')]")
	public WebElement tools_tags_createtag_alreadytag_existed;

	@FindBy(xpath = "//div[contains(text(),' Please select contacts from the list to add the tag to. ')]")
	public WebElement tools_tags_createtag_select_contact_error;

	@FindBy(xpath = "//*[contains(text(),' technocentra ')]")
	public WebElement tools_tags_createtag_Contactdisplay1;

	@FindBy(xpath = "//div[contains(text(),'No contacts found!')]")
	public WebElement tools_tags_createtag_contact_notfound;

	@FindBy(xpath = "//div[contains(text(),'Tag name can be max 30 characters.')]")
	public WebElement tools_tags_createtag_taglimit_30char_errormsg;

	@FindBy(xpath = "//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement tools_tags_createtag_tagcreated_toaster;

	@FindBy(xpath = "//button[@id='delete-tag']")
	public WebElement tools_tags_editTag_deleteoption;

	@FindBy(xpath = "//i[contains(text(),'Create your first tag and manage it here.')]")
	public WebElement tools_tags_createyour_firstTag_Msg;

	@FindBy(xpath = "//div[contains(text(),'Select the tag you would like to edit.')]")
	public WebElement tools_tags_select_your_tag_toEdit;

	@FindBy(xpath = "//button[@id='abcTagManage']")
	public WebElement tools_tags_abcTagmanage;

	@FindBy(xpath = "//button[@id='25TagManage']")
	public WebElement tools_tags_25Tagmanage;

	@FindBy(xpath = "//button[@id='@&*/TagManage']")
	public WebElement tools_tags_Special_Char_Tagmanage;

	@FindBy(xpath = "//button[contains(text(),'SAVE')]")
	public WebElement tools_tags_editTag_Save;

	@FindBy(xpath = "//button[contains(text(),'PROCEED')]")
	public WebElement tools_tags_editTag_proceed;

	@FindBy(xpath = "//button[@id='mnopqrTagManage']")
	public WebElement tools_tags_editTag_mnopqr_Tag;

	@FindBy(xpath = "//p[contains(text(),'No tags found!')]")
	public WebElement NO_Tags_MainPage;

	@FindBy(xpath = "//svg-icon[@id='mnopqr_deleteTag']")
	public WebElement mnopqrTag_delete_MainPage;

	@FindBy(xpath = "//input[@placeholder='Search tags']")
	public WebElement Search_Tags_Page;

	@FindBy(xpath = "//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement tools_tags_tagedited_save_toaster;

	// BroadCast Messages

	@FindBy(xpath = "//a[@id='broadcastTab']")
	public WebElement broadCast_msg;

	@FindBy(xpath = "//a[@id='conversationTab']")
	public WebElement conversation_tab;

	@FindBy(xpath = "//a[@id='messagesMainTab']")
	public WebElement mesage_tab;

	@FindBy(xpath = "//button[@id='createBroadcastButton']")
	public WebElement create_broadcast;

	@FindBy(xpath = "//*[@class='cross-icons']")
	public WebElement tools_crossBtn;

	@FindBy(xpath = "//*[@class='model-bg ng-star-inserted']")
	public WebElement outsideclick;

	@FindBy(xpath = "//*[@class='ng-tns-c34-9 ng-star-inserted']//option")
	public WebElement tools_BM_department;

	@FindBy(xpath = "//button[contains(text(),'Upcoming')]")
	public WebElement tools_BM_upcoming;

	@FindBy(xpath = "//button[contains(text(),'Past')]")
	public WebElement tools_BM_past;

	@FindBy(xpath = "//button[contains(text(),'Drafts')]")
	public WebElement tools_BM_draft;

	@FindBy(xpath = "//i[contains(text(),'All your upcoming broadcasts will appear here.')]")
	public WebElement tools_BM_No_upcomingBroadcasts_msg;

	@FindBy(xpath = "//i[contains(text(),'You have no past broadcasts.')]")
	public WebElement tools_BM_No_past_BM_msgs;

	@FindBy(xpath = "//i[contains(text(),'You can save your draft broadcasts.')]")
	public WebElement tools_BM_NO_draft_msg;

	@FindBy(xpath = "//button[@id='broadcast_viewRecipients']")
	public WebElement tools_BM_viewrecipients;

	@FindBy(xpath = "//input[@id='inputBroadcastTagData']")
	public WebElement tools_BM_input_To_searchTags;

	@FindBy(xpath = "//input[@id='broadcast_name']")
	public WebElement tools_BM_title;

	@FindBy(xpath = "//textarea[@id='broadcast_message']")
	public WebElement tools_BM_message;

	@FindBy(xpath = "//h1[@class='mb-0 content-center']")
	public WebElement tools_BM_recipentCount;

	@FindBy(xpath = "//div[@class='contact-name d-flex']")
	public WebElement tools_BM_create_SearchTag1;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[2]")
	public WebElement tools_BM_create_SearchTag2;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[3]")
	public WebElement tools_BM_create_SearchTag3;

	@FindBy(xpath = "//input[@id='broadcastMoreTargeted']")
	public WebElement tools_BM_create_moretarget;

	@FindBy(xpath = "//input[@id='broadcastLessTargeted']")
	public WebElement tools_BM_create_lesstarget;

	@FindBy(xpath = "//*[@class='icon contact-tag-cross-icon']")
	public WebElement tools_BM_create_tag_crossmark;

	@FindBy(xpath = "(//*[@class='col-4 overflow-hidden'])[1]")
	public WebElement tools_BM_create_recipients_list;

	@FindBy(xpath = "//button[@id='viewRecipients_backId']")
	public WebElement tools_BM_create_viewrecipients_back;
	@FindBy(xpath = "//textarea[@id='broadcast_message']")
	public WebElement MessageInput_box;
	@FindBy(xpath = "//input[@id='broadcast_optOut']")
	public WebElement tools_BM_create_msg_optout_checkbox;

	@FindBy(xpath = "//div[contains(text(),'Title must be at least 3 characters.')]")
	public WebElement tools_BM_create_title3Char_errormsg;

	@FindBy(xpath = "//div[contains(text(),'Message char limit reached.')]")
	public WebElement tools_BM_create_msg_optout_checkbox_errormsg;

	@FindBy(xpath = "//button[@id='broadcast_addAttachment']")
	public WebElement tools_BM_create_attachment;

	@FindBy(xpath = "//input[@id='broadcast_sendNow']")
	public WebElement tools_BM_create_sendNow;

	@FindBy(xpath = "//input[@id='broadcast_Schedule']")
	public WebElement tools_BM_create_ScheduleLater;

	@FindBy(xpath = "//app-radio-button[@id='isRecurring']//span[@class='checkmark ']")
	public WebElement tools_BM_create_make_this_broadCast_recurring;

	@FindBy(xpath = "//div[contains(text(),'Please read the terms of service and verify that you agree.')]")
	public WebElement tools_BM_create_termsandCond_errormsg;

	@FindBy(xpath = "//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")
	public WebElement tools_BM_create_agree_termsandCond;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement tools_BM_create_Send_BroadCast;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement tools_BM_create_Schedule_BroadCast;

	@FindBy(xpath = "//button[@id='broadcast_DraftButton']")
	public WebElement tools_BM_create_Save_Draft;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement tools_BM_create_DateandTime_button;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[4]")
	public WebElement tools_BM_create_time;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[3]")
	public WebElement tools_BM_create_time_upArrow;

	@FindBy(xpath = "//div[contains(text(),'Please select a date and time in the future.')]")
	public WebElement tools_BM_create_select_Future_DateandTime_errorMsg;

	@FindBy(xpath = "(//*[@class='owl-dt-control-content owl-dt-control-button-content'])[5]")
	public WebElement tools_BM_create_select_DateandTime_set;

	@FindBy(xpath = "//select[@class='form-control ng-pristine ng-valid ng-touched']")
	public WebElement tools_BM_create_recurring_repeat;

	@FindBy(xpath = "//option[@id='Daily at this time_recurring']")
	public WebElement tools_BM_create_daily_atThis_time_recurring;

	@FindBy(xpath = "//option[@id='Weekly on (Selected day)_recurring']")
	public WebElement tools_BM_create_weekly_on_selectedDay_recurring;

	@FindBy(xpath = "//option[@id='Monthly on the (Date)_recurring']")
	public WebElement tools_BM_create_Monthly_onThe_Date_recurring;

	@FindBy(xpath = "//option[@id='Yearly on (Date) (Month)_recurring']")
	public WebElement tools_BM_create_yearly_onThe_Date_recurring;

	@FindBy(xpath = "//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement tools_BM_create_toaster;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public WebElement tools_BM_toaster;

	@FindBy(xpath = "//a[@id='scheduledMessageTab']")
	public WebElement tools_BM_create_SM;

	@FindBy(xpath = "//button[@id='warningInfoSave']")
	public WebElement tools_BM_create_PopUp_Draft;

	@FindBy(xpath = "//button[@id='warningInfoSave']")
	public WebElement tools_BM_manage_PopUp_save;

	@FindBy(xpath = "//button[contains(text(),'DISCARD')]")
	public WebElement tools_BM_create_PopUp_Discard;

	@FindBy(xpath = "//button[@id='createScheduleButton']")
	public WebElement tools_BM_create_create_SM;

	@FindBy(xpath = "//a[@id='templatesTab']")
	public WebElement tools_BM_create_create_Templates;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement tools_BM_create_create_Tags;

	@FindBy(xpath = "//a[@id='automationsTab']")
	public WebElement tools_BM_create_create_Automations;

	@FindBy(xpath = "//a[@id='advocateTab']")
	public WebElement tools_BM_create_create_Advocate;

	@FindBy(xpath = "//a[@id='reviewsTab']")
	public WebElement tools_BM_create_create_Review;

	@FindBy(xpath = "//button[@id='addReviewSite']")
	public WebElement tools_BM_create_Review;

	@FindBy(xpath = "//textarea[@id='link']")
	public WebElement tools_BM_create_Review_link;

	@FindBy(xpath = "//button[@id='reviewCreate']")
	public WebElement tools_BM_create_Review_Create;

	@FindBy(xpath = "//button[@id='Google_button']")
	public WebElement tools_BM_create_Review_GoogleReview_Manage;

	@FindBy(xpath = "//button[@id='delete-review-message']")
	public WebElement tools_BM_create_Review_Manage_DeleteReview;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[2]")
	public WebElement tools_BM_GifIcon;

	@FindBy(xpath = "(//img[@id='giphy-container-body'])[1]")
	public WebElement tools_BM_Gif1;

	@FindBy(xpath = "//span[@class='subhead-2-name pl-1 attachment-name-maxwidth']")
	public WebElement tools_BM_Gif_display;

	@FindBy(xpath = "(//h6[@class='empty-list'])[1]")
	public WebElement tools_BM_Gif_ErrorMsg;

	@FindBy(xpath = "//app-svg-icon[@class='close-icon pointer-section ']")
	public WebElement tools_BM_Gif_text_close;

	@FindBy(xpath = "(//img[@id='giphy-container-body'])[2]")
	public WebElement tools_BM_Gif2;

	@FindBy(xpath = "(//img[@id='giphy-container-body'])[3]")
	public WebElement tools_BM_Gif3;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[3]")
	public WebElement tools_BM_EmojiIcon;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[1]")
	public WebElement emoji;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[2]")
	public WebElement emoji1;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[3]")
	public WebElement emoji2;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[4]")
	public WebElement emoji3;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[5]")
	public WebElement emoji4;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[6]")
	public WebElement emoji5;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[7]")
	public WebElement emoji6;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[8]")
	public WebElement emoji7;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[9]")
	public WebElement emoji8;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[10]")
	public WebElement emoji9;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[4]")
	public WebElement tools_BM_ReviewIcon;

	@FindBy(xpath = "//a[@id='healthScreeningTab']")
	public WebElement tools_BM_Health_Screenings;

	@FindBy(xpath = "//button[@id='createTemplateButton']")
	public WebElement tools_BM_create_create_New_Template;

	@FindBy(xpath = "//button[@id='createTagButton']")
	public WebElement tools_BM_create_create_New_Tag;

	@FindBy(xpath = "//button[@id='createAutomationButton']")
	public WebElement tools_BM_create_create_New_Automation;

	@FindBy(xpath = "//button[@id='gotoReflection']")
	public WebElement tools_BM_create_create_Advocate_gotoReflection;

	@FindBy(xpath = "//button[@id='addReviewSite']")
	public WebElement tools_BM_create_create_ADDReview;

	@FindBy(xpath = "//button[contains(text(),'Activate')]")
	public WebElement tools_BM_Health_Screening_Activate;

	@FindBy(xpath = "//*[@id='broadcasts_breadcrumId']")
	public WebElement tools_BM_create_broadCast_Link;

	@FindBy(xpath = "//a[@id='broadcasts_breadcrumId']")
	public WebElement tools_BM_create_broadCastLink;

	@FindBy(xpath = "d-modal col-sm-9 col-lg-8 col-xl-7 ng-star-inserted")
	public WebElement tools_BM_create_outsideclick;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement tools_BM_create_recurring_appDateTime;

	@FindBy(xpath = "(//input[@id='appDateTimeId'])[2]")
	public WebElement tools_BM_create_recurring_appDateTime1;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]")
	public WebElement tools_BM_create_recurring_month;

	@FindBy(xpath = "//td[@class='owl-dt-calendar-cell owl-dt-year-2020 ng-star-inserted']")
	public WebElement tools_BM_create_recurring_year;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[12]")
	public WebElement tools_BM_create_recurring_lessMonth1;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[12]")
	public WebElement tools_BM_create_recurring_lessMonth;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]")
	public WebElement tools_BM_create_recurring_appDateTime_Month_Data;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]")
	public WebElement tools_BM_create_recurring_appDateTime_Date_Data;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[1]")
	public WebElement tools_BM_create_recurring_appDateTime_DecreaseMonth;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[15]")
	public WebElement tools_BM_create_recurring_appDateTime_SelectDate;

	@FindBy(xpath = "(//span[contains(text(),'15')])[2]")
	public WebElement tools_BM_create_recurring_appDateTime_SelectDate1;

	@FindBy(xpath = "(//span[contains(text(),'15')])[3]")
	public WebElement tools_BM_create_recurring_appDateTime_SelectDate2;

	@FindBy(xpath = "//div[contains(text(),'Please select a date in the future.')]")
	public WebElement tools_BM_create_recurring_appDateTime_inFuture_errorMsg;

	@FindBy(xpath = "//a[contains(text(),'W')]")
	public WebElement tools_BM_create_recurring_W;

	@FindBy(xpath = "//a[contains(text(),'Th')]")
	public WebElement tools_BM_create_recurring_Th;

	@FindBy(xpath = "//a[contains(text(),'Su')]")
	public WebElement tools_BM_create_recurring_su;

	@FindBy(xpath = "(//span[contains(text(),'Meeting')])[1]")
	public WebElement tools_BM_create_recurring_Past_comparing_Title;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0 inline-display border-bt-text'])[1]")
	public WebElement tools_BM_create_upcoming_list_Title;

	@FindBy(xpath = "(//span[@class='m-title'])[1]")
	public WebElement tools_BM_past_list_Title;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[1]")
	public WebElement tools_BM_create_upcoming_list_DateAndTime;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[1]")
	public WebElement tools_BM_past_list_DateAndTime;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0 inline-display border-bt-text'])[1]")
	public WebElement tools_BM_create_Draft_list_Title;

	@FindBy(xpath = "//div[@class='text-danger mt-2 ng-tns-c37-9 ng-star-inserted']")
	public WebElement tools_BM_select_current_Day;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_upcoming_manage;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_upcoming_manage2;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement tools_BM_create_upcoming_manage_delete;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement tools_BM_create_upcoming_Reuse_cancel;

	@FindBy(xpath = "//button[contains(text(),'Re-Use')]")
	public WebElement tools_BM_create_upcoming_Reuse;

	@FindBy(xpath = "//button[contains(text(),'View')]")
	public WebElement tools_BM_create_Past_view;

	@FindBy(xpath = "(//button[contains(text(),'View')])[2]")
	public WebElement tools_BM_create_Past_view1;

	@FindBy(xpath = "(//button[contains(text(),'View')])[2]")
	public WebElement tools_BM_create_Past_view2;

	@FindBy(xpath = "(//button[contains(text(),'View')])[3]")
	public WebElement tools_BM_create_Past_view3;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])")
	public WebElement tools_BM_create_past_Reuse;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])[2]")
	public WebElement tools_BM_create_past_Reuse1;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])[3]")
	public WebElement tools_BM_create_past_Reuse2;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])[4]")
	public WebElement tools_BM_create_past_Reuse3;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_Draft_manage;

	@FindBy(xpath = "//button[contains(text(),'Delete')]")
	public WebElement tools_BM_create_Draft_Delete;

	@FindBy(xpath = "(//span[@class='m-title'])[1]")
	public WebElement tools_BM_upcoming_list_title;

	@FindBy(xpath = "(//span[@class='m-title'])[2]")
	public WebElement tools_BM_upcoming_list_title1;

	@FindBy(xpath = "(//span[@class='m-title'])[3]")
	public WebElement tools_BM_upcoming_list_title2;

	@FindBy(xpath = "(//span[@class='m-title'])[4]")
	public WebElement tools_BM_upcoming_list_title3;

	@FindBy(xpath = "(//span[@class='m-title'])[5]")
	public WebElement tools_BM_upcoming_list_title4;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[1]")
	public WebElement tools_BM_upcoming_list_DateTime;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[2]")
	public WebElement tools_BM_upcoming_list_DateTime1;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[3]")
	public WebElement tools_BM_upcoming_list_DateTime2;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[4]")
	public WebElement tools_BM_upcoming_list_DateTime3;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[5]")
	public WebElement tools_BM_upcoming_list_DateTime4;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[1]")
	public WebElement tools_BM_upcoming_list_recurring_icon;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[2]")
	public WebElement tools_BM_upcoming_list_recurring_icon1;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[3]")
	public WebElement tools_BM_upcoming_list_recurring_icon2;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[4]")
	public WebElement tools_BM_upcoming_list_recurring_icon3;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[5]")
	public WebElement tools_BM_upcoming_list_recurring_icon4;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[1]")
	public WebElement tools_BM_Past_list_DateTime;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[2]")
	public WebElement tools_BM_Past_list_DateTime1;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[3]")
	public WebElement tools_BM_Past_list_DateTime2;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[4]")
	public WebElement tools_BM_Past_list_DateTime3;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[5]")
	public WebElement tools_BM_Past_list_DateTime4;

	@FindBy(xpath = "//button[contains(text(),'SAVE DRAFT')]")
	public WebElement tools_BM_Past_Save_Draft;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[1]")
	public WebElement tools_BM_Draft_noDateSelected;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[2]")
	public WebElement tools_BM_Draft_noDateSelected1;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[3]")
	public WebElement tools_BM_Draft_noDateSelected2;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[4]")
	public WebElement tools_BM_Draft_noDateSelected3;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[5]")
	public WebElement tools_BM_Draft_noDateSelected4;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[1]")
	public WebElement tools_BM_Draft_manage;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[2]")
	public WebElement tools_BM_Draft_manage1;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[3]")
	public WebElement tools_BM_Draft_manage2;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[4]")
	public WebElement tools_BM_Draft_manage3;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[5]")
	public WebElement tools_BM_Draft_manage4;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[1]")
	public WebElement tools_BM_Draft_Delete;

	@FindBy(xpath = "//button[contains(text(),'Discard')]")
	public WebElement tools_BM_Draft_Delete_Discard;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[2]")
	public WebElement tools_BM_Draft_Delete1;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[3]")
	public WebElement tools_BM_Draft_Delete2;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[4]")
	public WebElement tools_BM_Draft_Delete3;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[5]")
	public WebElement tools_BM_Draft_Delete4;

	@FindBy(xpath = "//button[@id='warningInfoDiscard']")
	public WebElement tools_BM_Draft_Delete_discard;

	@FindBy(xpath = "//button[contains(text(),'CANCEL')]")
	public WebElement tools_BM_Draft_Delete_Cancel;

	@FindBy(xpath = "//button[contains(text(),'SAVE DRAFT')]")
	public WebElement tools_BM_Draft_Save_Draft;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement tools_BM_draft_manage_delete;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[1]")
	public WebElement tools_BM_draft_recurring_icon;

	@FindBy(xpath = "(//span[@class=\"pull-right\"])[5]")
	public WebElement tools_BM_Draft_manage_Schedule_DateAndTime;

	@FindBy(xpath = "//span[@class='close-0']")
	public WebElement tools_BM_remove_attachment;

	@FindBy(xpath = "//span[contains(text(),' Auto2 ')]")
	public WebElement auto2_Contact_IN_Tags;

	@FindBy(xpath = "//span[contains(text(),' QA10 ')]")
	public WebElement QA_Contact_IN_Tags;

	@FindBy(xpath = "//div[contains(text(),' Your broadcast message has been saved as a draft. ')]")
	public WebElement BM_Draft_Toaster;

	@FindBy(xpath = "//div[contains(text(),'Your broadcast has been successfully updated for Auto1.')]")
	public WebElement BM_Updated_Toaster_Auto1;

	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement Auto1_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement Automation_Contact;

	@FindBy(xpath = "(//*[@class='org-right-arrow'])[1]")
	public WebElement Arrow;

	// attachment link

	// attachment link end

	// Initializing the page objects:
	public Repository() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	// Actions:
	public String validateLoginPageTitle() {
		return driver.getTitle();// always use string for getTitle
	}

	public boolean validateCRMImage() {
		return crmLogo.isDisplayed();// always use boolean for isDisplayed
	}

	public CommonMethods login(String UserName, String Password) {
		username.sendKeys(UserName);
		password.sendKeys(Password);
		loginBtn.submit();
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);

		return new CommonMethods();
	}

	public static WebDriver HighlightElement(WebDriver driver, WebElement element) {
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].setAttribute('style', 'background:pink;border:2px solid green;');", element);
		}
		return driver;
	}

	public WebElement WAIT(WebElement webelement) throws Exception {
		WebDriverWait w = new WebDriverWait(driver, 120);
		try {
			if (w.until(ExpectedConditions.visibilityOf(webelement)) != null)
				HighlightElement(driver, webelement);
		} catch (Exception e) {
			System.out.println("WebElement is not visible in the application::");
		}
		return webelement;
	}

	public WebElement textbox(WebElement webelement) throws Exception {
		WAIT(webelement);
		return webelement;
	}

	public void alert() {
		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
		}

	}

	public void validateSignOutLink() {
		signOutLink.click();
	}

	public void signuppagelink() {
		signupPage.click();
	}

	public void validateSignOut() {
		signOut.click();
	}

	public void validateUser() {
		username_signature.click();
	}

	public void validatetoggle() {
		signature_toggle_On_Off.click();
	}

}
