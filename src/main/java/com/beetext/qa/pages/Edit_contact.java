package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Edit_contact extends TestBase {

	public Edit_contact() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	// edit contact

	@FindBy(xpath = "//h4[normalize-space()='QA123@$#%&*?_()']")
	public WebElement select_conversation;

	@FindBy(xpath = "//h4[@id='3068016736_conversation']")
	public WebElement select_conversations;

	@FindBy(xpath = "//h4[normalize-space()='hello']")
	public WebElement select_50tag_conversations;

	@FindBy(xpath = "//*[@id='43_deleteTag']//*[@id='Capa_1']")
	public WebElement delete43_tag;

	@FindBy(xpath = "//div[contains(text(),'43 tag has been deleted.')]")
	public WebElement delete_43tag_toaster;

	@FindBy(xpath = "//div[contains(text(),'This contact has reached the maximum number of tag')]")
	public WebElement validation_50tag_error;

	@FindBy(xpath = "//div[@class='profile-img editmode-profile profile-pic-bg']//img[@class='ng-star-inserted']")
	public WebElement edit_avathar_logo;

	@FindBy(xpath = "//p[contains(text(),'File format is not supported.')]")
	public WebElement edit_invalidavathar_file;

	@FindBy(xpath = "//button[@class='btn btn-primary']")
	public WebElement okbutton;
	@FindBy(xpath = "//*[@id='contactPencilIcon']//*[local-name()='svg']")
	public WebElement edit_contact_button;

	@FindBy(xpath = "//input[@id='firstName']")
	public WebElement edit_name;

	@FindBy(xpath = "//input[@id='email-0']")
	public WebElement edit_email;

	@FindBy(xpath = "//div[contains(text(),'Enter valid email.')]")
	public WebElement edit_invalid_email;

	@FindBy(xpath = "//input[@id='cName']")
	public WebElement edit_company_name;

	@FindBy(xpath = " //button[@id='contactSaveButton']")
	public WebElement edit_save_button;

	@FindBy(xpath = "//div[contains(text(),'Contact successfully updated.')]")
	public WebElement edit_toaster_message;

	@FindBy(xpath = "//input[@id='tagInput']") // type tag 4554
	public WebElement type_tag;

	@FindBy(xpath = "//button[@id='tagAddButton']")
	public WebElement add_tag;

	@FindBy(xpath = "//label[contains(text(),'@$%&*/-')]")
	public WebElement verify_tag_special_char;

	@FindBy(xpath = " //*[@id='hello_deleteTag']//*[@id='Capa_1']")
	public WebElement add_tag_serachresults;

	@FindBy(xpath = "/p[contains(text(),'No Tags')]")
	public WebElement no_tag;

	@FindBy(xpath = "//div[contains(text(),'Special characters not allowed.')]")
	public WebElement invalid_special_char;
	// div[contains(text(),'Duplicate tags not allowed.')]

	@FindBy(xpath = "//div[contains(text(),'Duplicate tags are not allowed.')]")
	public WebElement duplicate_tag_validation;

	@FindBy(xpath = "//*[@id='@$%&*/-_deleteTag']//*[@id='Capa_1']")
	public WebElement crossmark;

	@FindBy(xpath = "//*[@id='hello_deleteTag']//*[@id='Capa_1']")
	public WebElement search_crossmark;
	@FindBy(xpath = "//span[contains(text(),'Do you want to remove this tag from')]")
	public WebElement open_delete_popup;

	@FindBy(xpath = "//button[@id='tagRemoveConfirm']")
	public WebElement confirm_delete_tag;

	@FindBy(xpath = "//div[contains(text(),'@$%&*/- tag has been deleted.')]")
	public WebElement tag_toaster;

	@FindBy(xpath = "//button[contains(text(),'CANCEL')]")
	public WebElement cancel_button;

	@FindBy(xpath = " //textarea[@id='noteinput']")
	public WebElement type_note; // add hello note

	@FindBy(xpath = "//button[@id='noteInputAddBtn']")
	public WebElement add_note;

	@FindBy(xpath = " //p[contains(text(),'No Notes')] ")
	public WebElement no_notes;

	@FindBy(xpath = "//p[@id='helo_note']")
	public WebElement edit_note;

	@FindBy(xpath = "//textarea[@id='notesEditInput']")
	public WebElement edit_type_note;

	@FindBy(xpath = " //button[@id='notesEditSave']")
	public WebElement save_note;

	@FindBy(xpath = "//*[@id='notesEditDelete']//*[local-name()='svg']")
	public WebElement click_delete_icon;

	@FindBy(xpath = "//*[@class='notes-cancel-icon']//*[local-name()='svg']")
	public WebElement cancel_cross_mark;

	@FindBy(xpath = " //button[@id='deleteNoteButton']")
	public WebElement confirm_delete_note;
	@FindBy(xpath = "//button[contains(text(),'CANCEL')]")
	public WebElement click_cancel_button;
	// span[contains(text(),'Do you want to remove this note from')]"

	@FindBy(xpath = "//span[contains(text(),'Do you want to delete this note from ')] ")
	public WebElement pop_headding;

	@FindBy(xpath = "//div[@aria-label='Note has been deleted.']")
	public WebElement delete_note_toaster_message;

	@FindBy(xpath = " //div[contains(text(),'Please remove space(s) at the end.')]")
	public WebElement removeSpace_end;
}
