package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Create_Contact extends TestBase {

	public Create_Contact() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//a[@id='contactTab']")
	public WebElement create_contact_Link;
	//// body/app-root[1]/div[1]/app-layout[1]/main[1]/section[1]/app-messages[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/a[1]/*[1]

	// *[@class='create-contact-icon']
	@FindBy(xpath = "//body/app-root[1]/div[1]/app-layout[1]/main[1]/section[1]/app-messages[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/a[1]/*[1]")
	public WebElement create_contact_icon;

	@FindBy(xpath = "//app-avatar[@class='profile']//div[@class='profile-img editmode-profile']")
	public WebElement create_avathar_logo;

	@FindBy(xpath = " //span[contains(text(),'File format is not supported.')]")
	public WebElement create_invlaid_avathar_logo;

	@FindBy(xpath = "//input[@id='firstName']")
	public WebElement create_contact_Name;

	@FindBy(xpath = " //div[contains(text(),'A new contact must have a name.')]")
	public WebElement create_contact_Name_required;

	@FindBy(xpath = "//input[@id='contactNumberId']")
	public WebElement create_contact_phonenum;

	// div[contains(text(),'Please remove spaces at ends.')]
	@FindBy(xpath = " //div[contains(text(),'Please remove space(s) at the end.')]")
	public WebElement removeSpace_end;

	@FindBy(xpath = "//div[contains(text(),'Phone number is required.')]")
	public WebElement create_contact_phonenum_required;

	@FindBy(xpath = "//input[@id='contactEmailId']")
	public WebElement create_contac_Email;

	@FindBy(xpath = "//div[contains(text(),'Enter valid email.')]")
	public WebElement create_contac_invalid_Email;

	@FindBy(xpath = "//input[@id='companyNameId']")
	public WebElement create_contac_companyName;

	@FindBy(xpath = "//input[@id='contactTagsId']")
	public WebElement create_contac_tags;

	@FindBy(xpath = " //div[contains(text(),'This contact has reached the maximum number of tag')]")
	public WebElement create_contac_morethan_50tags;

	@FindBy(xpath = " //div[contains(text(),'Special characters not allowed.')]")
	public WebElement special_char_notallowed;
	// div[contains(text(),'Duplicate tags not allowed.')]
	@FindBy(xpath = "//div[contains(text(),'Duplicate tags are not allowed.')] ")
	public WebElement duplicate_tags;

	@FindBy(xpath = "//textarea[@id='contactNotesId']")
	public WebElement create_contac_notes;

	@FindBy(xpath = "//button[@id='saveButton']")
	public WebElement create_contac_button;

	@FindBy(xpath = "//div[contains(text(),'Created contact successfully.')]")
	public WebElement create_contact_Successfully;

	@FindBy(xpath = "//div[contains(text(),'Contact with same phone number already exists in o')]")
	public WebElement create_contac_phonenum_alreayExist;

	@FindBy(xpath = "//*[@class='cross-icons']//*[name()='svg']")
	public WebElement close_button;

}
