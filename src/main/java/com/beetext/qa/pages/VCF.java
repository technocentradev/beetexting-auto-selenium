package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class VCF extends TestBase {

	public VCF() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//*[@id='email-id']")
	public WebElement user_id;

	@FindBy(xpath = "//*[@id='password-id']")
	public WebElement Passsword_ID;

	@FindBy(xpath = "//*[@id='login-submit']")
	public WebElement loginbttn;

	@FindBy(xpath = "//button[@id='Auto1ButtonId']")
	public WebElement auto1_Manage;

	@FindBy(xpath = "//a[@id='toolsMainTab']")
	public WebElement tools_bttn;

	@FindBy(xpath = "//a[@id='contactTab']")
	public WebElement ContactsTab;

	@FindBy(xpath = "//a[@id='conversationTab']")
	public WebElement ConversationTab;

	@FindBy(xpath = "//a[@id='settingsMainTab']")
	public WebElement settings;

	@FindBy(xpath = "//a[@id='NumbersTab']")
	public WebElement settings_Numbers;

	@FindBy(xpath = "//button[@id='AutomationsButtonId']")
	public WebElement Numbers_automations_Manage;

	@FindBy(xpath = "//button[contains(text(),' Vcard')]")
	public WebElement VCard;

	@FindBy(xpath = "//div[@class='mat-slide-toggle-thumb']")
	public WebElement VCard_toggle;

	@FindBy(xpath = "//button[@id='testButtonId']")
	public WebElement test_Manage;

	@FindBy(xpath = "//button[@id='Automation TollFreeButtonId']")
	public WebElement TollFree_Manage;

	@FindBy(xpath = "//button[@id='dummyButtonId']")
	public WebElement Mange_Dummy_AutomationBlock;

	@FindBy(xpath = "//a[@id='settingsClose']")
	public WebElement Settings_Close;

	@FindBy(xpath = "//span[contains(@title,'.vcf')]")
	public WebElement VCard_File_Name;

	@FindBy(xpath = "//div[@class='model-bg ng-star-inserted']")
	public WebElement VCard_outsideClick;

	@FindBy(xpath = "//input[@formcontrolname='namePreFix']")
	public WebElement Vcard_NamePrefix;

	@FindBy(xpath = "//input[@formcontrolname='firstName']")
	public WebElement Vcard_FirstName;

	@FindBy(xpath = "//input[@formcontrolname='middleName']")
	public WebElement Vcard_MiddleName;

	@FindBy(xpath = "//input[@formcontrolname='lastName']")
	public WebElement Vcard_LastName;

	@FindBy(xpath = "//input[@formcontrolname='nameSuffix']")
	public WebElement Vcard_NameSuffix;

	@FindBy(xpath = "//input[@formcontrolname='email']")
	public WebElement Vcard_Email;

	@FindBy(xpath = "//input[@formcontrolname='company']")
	public WebElement Vcard_CompanyName;

	@FindBy(xpath = "//input[@formcontrolname='phoneNumber']")
	public WebElement Vcard_PhoneNumber;

	@FindBy(xpath = "//input[@formcontrolname='webSiteURL']")
	public WebElement Vcard_WebsiteURL;

	@FindBy(xpath = "//textarea[@formcontrolname='note']")
	public WebElement Vcard_Description;

	@FindBy(xpath = "//button[contains(text(),'Save')]")
	public WebElement Vcard_Save;

	@FindBy(xpath = "//button[contains(text(),' Cancel')]")
	public WebElement Vcard_Cancel;

	@FindBy(xpath = "//div[contains(text(),' Vcard  successfully updated. ')]")
	public WebElement Vcard_Status_Successfully_Updated_Toaster;

	@FindBy(xpath = "//div[contains(text(),'Vcard successfully created.')]")
	public WebElement Vcard_Created_Toaster;

	@FindBy(xpath = "//div[contains(text(),' Vcard  successfully updated. ')]")
	public WebElement Vcard_Updated_Toaster;

	@FindBy(xpath = "//div[contains(text(),' Email not valid. ')]")
	public WebElement Vcard_Email_Error;

	@FindBy(xpath = "//div[contains(text(),' Limit Company Name length to 50 characters. ')]")
	public WebElement Vcard_Company_Error;

	@FindBy(xpath = "//div[contains(text(),' Limit First Name length to 50 characters. ')]")
	public WebElement Vcard_FirstName_Error;

	@FindBy(xpath = "//div[contains(text(),' Limit Middle Name length to 50 characters. ')]")
	public WebElement Vcard_MiddleName_Error;

	@FindBy(xpath = "//div[contains(text(),' Limit Last Name length to 50 characters. ')]")
	public WebElement Vcard_LastName_Error;

	@FindBy(xpath = "//div[contains(text(),' Limit Name Prefix length to 10 characters. ')]")
	public WebElement Vcard_PrefixName_Error;

	@FindBy(xpath = "//div[contains(text(),' Limit Name Suffix length to 10 characters. ')]")
	public WebElement Vcard_SuffixName_Error;

	@FindBy(xpath = "//span[contains(text(),'File format is not supported.')]")
	public WebElement Vcard_File_Format_Not_Supported;

	@FindBy(xpath = "//app-svg-icon[@class='vcard-icon']")
	public WebElement Vcard_Icon;

	@FindBy(xpath = "//h4[@id='VCard Contact, Dummy_group']")
	public WebElement groupConversation;

	@FindBy(xpath = "//h4[@id='QA, QA10_group']")
	public WebElement automations_groupConversation;

	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement Auto1_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement Automation_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation TollFree']")
	public WebElement Automation_Tollfree_Contact;

	@FindBy(xpath = "//button[@class='btn btn-link emojiSendbtn btn-action-sm']")
	public WebElement chatInputsendButton;

	@FindBy(xpath = "//div[@class='d-flex attachment-layout']")
	public WebElement VCFReceivedSide;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement chatInputTextareaTxt;

	@FindBy(xpath = "//*[@class ='messages ng-star-inserted']")
	public WebElement message;

	@FindBy(xpath = "(//button[@class='btn btn-link btn-chat-link'])[1]")
	public WebElement pin_attachment;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[1]")
	public WebElement BM_pin_attachment;

	@FindBy(xpath = "//img[@class='attachment-image-img ng-star-inserted']")
	public WebElement image_verification;

	@FindBy(xpath = "//*[@class='icon emoji-icon']")
	public WebElement emojibttn;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[1]")
	public WebElement emoji_like;

	@FindBy(xpath = "//Span[@class='col-6 contact-pipe inline-display']")
	public WebElement contactSearch_OP_Txt1;

	@FindBy(xpath = "//*[@id='composenewBtn']")
	public WebElement composebutton;

	@FindBy(xpath = "//*[@id='contactSearchInput']")
	public WebElement contactSearchInputTxt;

	@FindBy(xpath = "//div[@id='hoverSelectDepartment']")
	public WebElement department_Head;

	@FindBy(xpath = "//button[normalize-space()='Local']")
	public WebElement LocalDept;

	@FindBy(xpath = "//span[@id='Auto1_Department']")
	public WebElement Auto1_Dept;

	@FindBy(xpath = "//span[@id='test_Department']")
	public WebElement test_Dept;

	@FindBy(xpath = "//button[@id='testButtonId']")
	public WebElement testdeptManage;

	@FindBy(xpath = "//app-svg-icon[@class='vcard-icon']")
	public WebElement vcf_icon_Disable;

	@FindBy(xpath = "//a[@id='broadcastTab']")
	public WebElement broadCast_msg;

	@FindBy(xpath = "//button[@id='createBroadcastButton']")
	public WebElement create_broadcast;

	@FindBy(xpath = "//input[@id='inputBroadcastTagData']")
	public WebElement tools_BM_input_To_searchTags;

	@FindBy(xpath = "//input[@id='broadcast_name']")
	public WebElement tools_BM_title;

	@FindBy(xpath = "//textarea[@id='broadcast_message']")
	public WebElement tools_BM_message;

	@FindBy(xpath = "//div[@class='contact-name d-flex']")
	public WebElement tools_BM_create_SearchTag1;

	@FindBy(xpath = "//button[@id='broadcast_DraftButton']")
	public WebElement tools_BM_create_Save_Draft;

	@FindBy(xpath = "//button[contains(text(),'Drafts')]")
	public WebElement tools_BM_draft;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[1]")
	public WebElement tools_BM_Draft_manage;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement Tools_BM_Manage_Delete;

	@FindBy(xpath = "//*[@class='cross-icons']")
	public WebElement tools_crossBtn;

	@FindBy(xpath = "//button[contains(text(),'DISCARD')]")
	public WebElement tools_BM_create_PopUp_Discard;

	@FindBy(xpath = "//a[@id='RolesTab']")
	public WebElement setting_roles;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[4]")
	public WebElement tools_BM_ReviewIcon;

	@FindBy(xpath = "(//div[@class='Mt-10'])[8]")
	public WebElement setting_roles_VCard_Details;

	@FindBy(xpath = "(//button[contains(text(),'Toll-Free')])[1]")
	public WebElement TollFree_Button_department;

	@FindBy(xpath = "//span[normalize-space()='Automation TollFree']")
	public WebElement TollFree_department;

	@FindBy(xpath = "//app-svg-icon[@class='icon giphy-icon emoji-icon']")
	public WebElement GifIcon;

	@FindBy(xpath = "//app-svg-icon[@class='icon tools-giphy']")
	public WebElement GifIcon_Tools;

	@FindBy(xpath = "//app-svg-icon[@class='icon tools-giphy']")
	public WebElement GifIconTemplates;

	@FindBy(xpath = "//body//app-root//img[9]")
	public WebElement selectGif;

	@FindBy(xpath = "(//app-svg-icon[@class='icon tools-giphy'])[1]")
	public WebElement tools_BM_GifIcon;

	@FindBy(xpath = "(//img[@id='giphy-container-body'])[1]")
	public WebElement tools_BM_Gif1;

	@FindBy(xpath = "//button[@class='btn btn-primary']")
	public WebElement ok_button;

	@FindBy(xpath = "//a[@id='templatesTab']")
	public WebElement templates;

	@FindBy(xpath = "//button[@id='createTemplateButton']")
	public WebElement create_templates;

	@FindBy(xpath = "//button[@id='personalTemplates']")
	public WebElement Personaltemplates;

	@FindBy(xpath = "//button[@id='sharedTemplates']")
	public WebElement sharedtemplates;

	@FindBy(xpath = "//button[@id='draftTemplates']")
	public WebElement drafttemplates;

	@FindBy(xpath = "//input[@id='template_title']")
	public WebElement templates_title;

	@FindBy(xpath = "//button[@id='vcard_addAttachment']")
	public WebElement vcard_in_Templates;

	@FindBy(xpath = "//app-svg-icon[contains(@class,'vcard-icon')]")
	public WebElement vcard_Icon;

	@FindBy(xpath = "//textarea[@id='template_message']")
	public WebElement templates_message;

	@FindBy(xpath = "//button[@id='template_personal']")
	public WebElement templates_Personal;

	@FindBy(xpath = "//button[@id='template_shared']")
	public WebElement templates_shared;

	@FindBy(xpath = "//app-radio-button[@id='template_selectall']//span[@class='checkmark ']")
	public WebElement selectAll_Dept_in_Templates;

	@FindBy(xpath = "//app-radio-button[@id='nominee123_Department']//span[@class='checkmark ']")
	public WebElement nominee123_Dept_in_Templates;

	@FindBy(xpath = "//app-radio-button[@id='Auto1_Department']//span[@class='checkmark ']")
	public WebElement auto1_Dept_in_Templates;

	@FindBy(xpath = "//app-radio-button[@id='Auto2_Department']//span[@class='checkmark ']")
	public WebElement auto2_Dept_in_Templates;

	@FindBy(xpath = "//button[@id='template_CreateButton']")
	public WebElement create_Template;

	@FindBy(xpath = "//button[contains(text(),'SAVE DRAFT')]")
	public WebElement Draft_Template;

	@FindBy(xpath = "//button[@id='Template VCFButton']")
	public WebElement manageTemplate;

	@FindBy(xpath = "//button[@id='VCFButton']")
	public WebElement Template_For_ManageCase;

	@FindBy(xpath = "//button[@id='delete-template-message']")
	public WebElement DeleteTemplate;

	@FindBy(xpath = "//a[@id='automationsTab']")
	public WebElement tools_Automations;

	@FindBy(xpath = "//button[@id='createAutomationButton']")
	public WebElement create_Automations;

	@FindBy(xpath = "(//button[@id='vcard_addAttachment'])[2]")
	public WebElement VCard_Button2;

	@FindBy(xpath = "//input[@id='conditionType_searchTerm_0']")
	public WebElement condition_Keyword;

	@FindBy(xpath = "//textarea[@id='message']")
	public WebElement Reply_message;

	@FindBy(xpath = "//button[@id='saveButton']")
	public WebElement Create;

	@FindBy(xpath = "//input[@id='title']")
	public WebElement title;

	@FindBy(xpath = "//div[@class='mat-slide-toggle-thumb']")
	public WebElement automation_ON_OFF;

	@FindBy(xpath = "//button[@id='manageButton_0']")
	public WebElement manage;

	@FindBy(xpath = "//button[@id='manageButton_1']")
	public WebElement manage_1;

	@FindBy(xpath = "//button[@id='delete-autoresponse']")
	public WebElement delete_autoResponse;

	@FindBy(xpath = "//div[contains(text(),'Maximum 10 attachments are allowed.')]")
	public WebElement error_for_10Attachments;

	@FindBy(xpath = "//button[@id='addActionButton']")
	public WebElement add_Action;

	@FindBy(xpath = "(//span[@class='close-0'])[2]")
	public WebElement VCF_Close;

	@FindBy(xpath = "(//span[@class='close-0'])[1]")
	public WebElement VCF_Attachent_Close;

	@FindBy(xpath = "(//app-svg-icon[@class='icon paper-clip'])[2]")
	public WebElement VCF_PinAttachent;

	@FindBy(xpath = "//app-svg-icon[@class='icon paper-clip']")
	public WebElement PinAttachent;

	@FindBy(xpath = "//a[@id='scheduledMessageTab']")
	public WebElement Tools_SM;

	@FindBy(xpath = "//button[@id='createScheduleButton']")
	public WebElement Tools_SM_Create;

	@FindBy(xpath = "//button[@id='schedule_DraftButton']")
	public WebElement Tools_SM_SaveDraft;

	@FindBy(xpath = "//input[@id='inputScheduleContactData']")
	public WebElement input_msg_Sent_TO;

	@FindBy(xpath = "//textarea[@id='schedule_message']")
	public WebElement SM_MSg_Textarea;

	@FindBy(xpath = "//h4[normalize-space()='QA10']")
	public WebElement QA10_Contact;

	@FindBy(xpath = "//app-svg-icon[@id='pinIcon']")
	public WebElement PinIcon;

	@FindBy(xpath = "//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]")
	public WebElement Set_button_in_calendar_popup;

	@FindBy(xpath = "//button[@aria-label='Add a hour']//span[@class='owl-dt-control-button-content']//*[local-name()='svg']")
	public WebElement Uparrow_button_Time_hour;

	@FindBy(xpath = "//button[@aria-label='Add a minute']//span[@class='owl-dt-control-button-content']//*[name()='svg']")
	public WebElement Uparrow_button_Time_mint;

	@FindBy(xpath = "//button[@aria-label='Minus a hour']")
	public WebElement Downarrow_button_Time_hour;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement Calendar_icon;

	@FindBy(xpath = "//button[@id='schedule_SaveButton']")
	public WebElement Tools_Sm_Create_SM;

	@FindBy(xpath = "//button[contains(text(),'Upcoming')]")
	public WebElement Tools_SM_Upcoming;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[1]")
	public WebElement Tools_SM_Manage;

	@FindBy(xpath = "//button[@id='delete-scheduled-message']")
	public WebElement Tools_SM_Manage_Delete;

	@FindBy(xpath = "//app-check-box[@id='isRecurring']//span[@class='checkmark']")
	public WebElement Tools_SM_Recurring_Button;

	@FindBy(xpath = "//svg-icon[@class='remove-reciepient']")
	public WebElement Tools_SM_crossMark_of_Contact;

	@FindBy(xpath = "//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")
	public WebElement tools_BM_create_agree_termsandCond;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement tools_BM_create_Send_BroadCast;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[1]")
	public WebElement BM_Pin_Attachment;

	@FindBy(xpath = "//app-radio-button[@id='isRecurring']//span[@class='checkmark ']")
	public WebElement tools_BM_create_make_this_broadCast_recurring;

	@FindBy(xpath = "//button[contains(text(),'Past')]")
	public WebElement tools_BM_past;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])")
	public WebElement tools_BM_create_past_Reuse;

	@FindBy(xpath = "//button[contains(text(),'Upcoming')]")
	public WebElement tools_BM_upcoming;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_upcoming_manage;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_upcoming_manage2;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement tools_BM_create_upcoming_manage_delete;

	@FindBy(xpath = "//input[@id='broadcast_Schedule']")
	public WebElement tools_BM_create_ScheduleLater;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement tools_BM_create_DateandTime_button;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[3]")
	public WebElement tools_BM_create_time_upArrow;

	@FindBy(xpath = "(//*[@class='owl-dt-control-content owl-dt-control-button-content'])[5]")
	public WebElement tools_BM_create_select_DateandTime_set;

	@FindBy(xpath = "//button[@id='warningInfoSave']")
	public WebElement tools_BM_create_PopUp_Draft;

	@FindBy(xpath = "//button[@id='warningInfoSave']")
	public WebElement tools_BM_manage_PopUp_save;

	@FindBy(xpath = "(//button[@class='btn btn-link btn-chat-link'])[4]")
	public WebElement conversation_Templates;

	@FindBy(xpath = "//button[contains(text(),'Personal')]")
	public WebElement conversation_Templates_Personal;

	@FindBy(xpath = "//button[contains(text(),' USE ')]")
	public WebElement conversation_Templates_Select_Template;

	@FindBy(xpath = "//button[@id='VCFAdminButtonId']")
	public WebElement oneAgent_Sample1_Manage;

	@FindBy(xpath = "//button[@id='VCFAdminButtonId']")
	public WebElement automationBlock_Manage;

	@FindBy(xpath = "//span[contains(text(),'.vcf')]")
	public WebElement managePage_Department_VCF_File;

	@FindBy(xpath = "//span[contains(text(),'.vcf')]")
	public WebElement Conversation_Msg_textbox_Department_VCF_File;

	@FindBy(xpath = "//img[@class='attachment-image-img ng-star-inserted']")
	public WebElement verification_Gif;

	@FindBy(xpath = "//a[contains(text(),'.mp4')]")
	public WebElement video_verification;

	@FindBy(xpath = "//a[contains(text(),'.mp3')]")
	public WebElement audio_File_Verification;

	@FindBy(xpath = "//a[contains(text(),'.pdf')]")
	public WebElement pdf_File_Verification;

	@FindBy(xpath = "//a[contains(text(),'.mov')]")
	public WebElement mov_File_Verification;

	@FindBy(xpath = "//a[contains(text(),'.xls')]")
	public WebElement Excel_xls_File_Verification;

	@FindBy(xpath = "//a[contains(text(),'.doc')]")
	public WebElement wordDoc_File_Verification;

	@FindBy(xpath = "//a[contains(text(),'.txt')]")
	public WebElement txt_File_Verification;

	@FindBy(xpath = "(//*[@class='btn btn-link btn-chat-link'])[5]")
	public WebElement Conv_SM;

	@FindBy(xpath = "(//span[@class='owl-dt-control-button-content'])[3]")
	public WebElement increase_1Min_Time_in_Conv_SM;

	@FindBy(xpath = "//span[@class='owl-dt-control-content owl-dt-control-button-content'][contains(text(),'Set')]")
	public WebElement conv_SM_Time_Set_button;

	@FindBy(xpath = "//*[contains(text(),'.webm')]")
	public WebElement webm_File_Verification;

}
