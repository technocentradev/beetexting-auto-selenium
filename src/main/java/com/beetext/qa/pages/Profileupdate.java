package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Profileupdate extends TestBase {

	public Profileupdate() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	// profile message xpath
	@FindBy(xpath = "//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")
	public WebElement threedots;
	//a[@routerlink='profile']
	@FindBy(xpath = "//body/app-root[1]/div[1]/app-layout[1]/main[1]/aside[1]/ul[1]/li[7]/div[1]/ul[1]/li[1]/a[1]")
	public WebElement profile_link;
	
	@FindBy(xpath = "//a[@id='profileTab']")
	public WebElement  profile_page_link;
	@FindBy(xpath = "//input[@name='name']")
	public WebElement profile_name;
	@FindBy(xpath = "//div[contains(text(),'Name is required.')]")
	public WebElement profile_name_required;
	@FindBy(xpath = "//div[contains(text(),'Name must be at least 3 characters.')]")
	public WebElement profile_name_3_char_required;
	
	@FindBy(xpath = "//input[@placeholder='(###) ###-####']")
	public WebElement profile_mobile_num;
	
	@FindBy(xpath = "//div[contains(text(),'Mobile number must be a valid 10-digit phone numbe')]")
	public WebElement profile_mobile_num_required;
	@FindBy(xpath = "//textarea[@placeholder='Signature']")
	public WebElement signature_text;
	@FindBy(xpath = "//div[@class='mat-slide-toggle-thumb']")
	public WebElement toggle_button;
	
	@FindBy(xpath="//button[normalize-space()='Save']")
	public WebElement saveButton;
	
	@FindBy(xpath="//button[normalize-space()='Cancel']")
	public WebElement cancel_Button;
	
	@FindBy(xpath="//div[@aria-label='Profile updated successfully.']")
	public WebElement toaster_message;
	@FindBy(xpath="//*[@class='cross-icon']//*[name()='svg']")
	public WebElement close_profile_popup;
	
	

}
