package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class SettingsAgents extends TestBase {
	public SettingsAgents() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//span[@class='s-title']")
	public WebElement Settings_link;
	@FindBy(xpath = "//a[@id='UsersTab']")
	public WebElement agent_link;
	@FindBy(xpath = "//button[@id='addAgentButton']")
	public WebElement add_agent_btn;
	// input[@id='manageAgentName']
	@FindBy(xpath = "//input[@id='manageAgentName']")
	public WebElement agent_name;
	@FindBy(xpath = "//div[contains(text(),'Name is required.')]")
	public WebElement agent_name_required;

	@FindBy(xpath = "//div[contains(text(),'Name must be at least 3 characters.')]")
	public WebElement agent_name_required_3_char;
	@FindBy(xpath = "//input[@id='manageAgentEmail']")
	public WebElement agent_email;
	@FindBy(xpath = "//div[contains(text(),'Email is required.')]")
	public WebElement agent_email_required;
	// div[contains(text(),'Email already exists.Please try again.')]
	@FindBy(xpath = "//div[@aria-label='Email address already in use. Re-invite the user or try a new email address.']")
	public WebElement agent_email_duplicate;

	@FindBy(xpath = "//div[contains(text(),'Please select how you would like to send your invi')]")
	public WebElement agent_email_check_uncheck;

	@FindBy(xpath = "//div[contains(text(),'Email not valid.')]")
	public WebElement agent_email_not_valid;

	@FindBy(xpath = "//select[@id='manageAgentRole']")
	public WebElement agent_role;

	@FindBy(xpath = "//app-radio-button[@id='inviteAgentEmail']//span[@class='checkmark ']")
	public WebElement emailradiobutton;

	@FindBy(xpath = " //button[normalize-space()='SEND']")
	public WebElement send_button;

	@FindBy(xpath = "//div[@aria-label='Your invitation to sampleagent has been sent.']")
	public WebElement agent_invite_toaster;
	@FindBy(xpath = "//div[@aria-label='Your invitation to deleteagentaddsameagent has been sent.']")
	public WebElement agent_invite_toasters;

	@FindBy(xpath = " //button[@id='sampleagentreInviteButton']")
	public WebElement agent_reviewinvite_button;

	@FindBy(xpath = "//div[@aria-label='User has been re-invited.']")
	public WebElement reinvitation_toaster_message;

	// manage agent

	@FindBy(xpath = " //button[@id='sampleagentmanageButton']")
	public WebElement manage_btn;

	@FindBy(xpath = "//button[@id='deleteagentaddsameagentmanageButton']")
	public WebElement addsame_agent_email_manage_btn;

	@FindBy(xpath = " //button[@id='dummymanageButton']")
	public WebElement manage_btn1;

	@FindBy(xpath = "//button[@id='deleteAgentButton']")
	public WebElement Delete_agent;

	@FindBy(xpath = "//button[@id='confirmDeleteAgentId']")
	public WebElement Confirm_Delete_agent;

	@FindBy(xpath = " //button[normalize-space()='Cancel']")
	public WebElement cancel_button;

	@FindBy(xpath = " //div[contains(text(),'Manage Agent')]")
	public WebElement manage_agent_headding;

	@FindBy(xpath = "//div[@aria-label='sampleagent has been deleted from the organization.']")
	public WebElement Delete_Toaster;
	@FindBy(xpath = "//div[@aria-label='deleteagentaddsameagent has been deleted from the organization.']")
	public WebElement Delete_Toasters;

	@FindBy(xpath = " //app-check-box[@id='Auto1checkBox']//span[@class='checkmark']")
	public WebElement check_uncheck_dept;

	@FindBy(xpath = "//button[normalize-space()='Save']")
	public WebElement save_agent;

	@FindBy(xpath = "//div[@aria-label='User updated successfully.']")
	public WebElement update_toaster_message;

	@FindBy(xpath = "//app-check-box[@id='Auto1checkBox']//span[@class='checkmark']")
	public WebElement select_auto1_dept;
	@FindBy(xpath = "//app-check-box[@id='AutomationscheckBox']//span[@class='checkmark']")
	public WebElement select_Automations_dept;

	@FindBy(xpath = "//div[contains(text(),'Your contact must have a valid 10-digit phone numb')]")
	public WebElement phone_number_validation;

	@FindBy(xpath = "//input[@id='manageAgentMobile']")
	public WebElement phone_number_input_box;

	// yopmail xpath

	@FindBy(xpath = " //input[@id='login']")
	public WebElement email;

	@FindBy(xpath = "//i[@class='material-icons-outlined f36']")
	public WebElement gobutton;

	@FindBy(xpath = " //button[@id='refresh']")
	public WebElement refresh_button;
	// div[@id='e_ZwRjAmV5ZQLkZQZkZQNjZwR4ZQNkZt==']//span[@class='lmf'][normalize-space()='hitextbee@gmail.com']
	@FindBy(xpath = "//div[normalize-space()='auto2 invited you to Beetexting']")
	public WebElement email_receviced;

	@FindBy(xpath = " //a[normalize-space()='Accept']")
	public WebElement accept_button;

	@FindBy(xpath = "//a[normalize-space()='Reject']")
	public WebElement reject_button;

	@FindBy(xpath = " //*[@class='cross-icons']//*[local-name()='svg']")
	public WebElement close_settings_popup;

	// set password

	@FindBy(xpath = "//input[@name='password']")
	public WebElement password;

	@FindBy(xpath = "//input[@name='confirmPassword']")
	public WebElement cpassword;

	@FindBy(xpath = " //button[normalize-space()='Set Password']")
	public WebElement setpassword_button;

	@FindBy(xpath = "//h4[contains(text(),'Your invitation is rejected successfully.')]")
	public WebElement reject_page;

	@FindBy(xpath = "//h4[contains(text(),'Your invite has expired or invalid, please contact your administrator. ')]")
	public WebElement delete_agent_popup;
	
	@FindBy(xpath = "//div[@class='modal-body text-center modal-pad d-block']")
	public WebElement delete_agent_popup1;

}
