package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class BM_Optout_Pages extends TestBase {

	public BM_Optout_Pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(id = "email-id")
	WebElement username;

	@FindBy(id = "password-id")
	public static WebElement password;

	@FindBy(id = "login-submit")
	WebElement loginBtn;

	@FindBy(xpath = "//button[@contains(text(),'Sign Up')]")
	WebElement signupBtn;

	@FindBy(xpath = "//li[6]/div[1]/a[1]/app-svg-icon[1]/span[1]")
	WebElement signOutLink;

	@FindBy(xpath = "//a[@id='logoutId']")
	WebElement signOut;

	@FindBy(xpath = "//*[@id='email-id']")
	public static WebElement user_id;

	@FindBy(xpath = "//*[@id='password-id']")
	public static WebElement Passsword_ID;

	@FindBy(xpath = "//*[@id='login-submit']")
	public static WebElement loginbttn;

	@FindBy(xpath = "//a[@id='toolsMainTab']")
	public WebElement tools_bttn;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement tools_tags_bttn;

	@FindBy(xpath = "//a[@id='broadcastTab']")
	public WebElement broadCast;

	@FindBy(xpath = "//a[@id='conversationTab']")
	public WebElement conversation_tab;

	@FindBy(xpath = "//a[@id='messagesMainTab']")
	public WebElement mesage_tab;

	@FindBy(xpath = "//div[@class='row d-flex justify-content-between']//select[@class='form-control ng-pristine ng-valid ng-touched']")
	public WebElement select_Dept;

	@FindBy(xpath = "//option[contains(text(),'Auto2')]")
	public WebElement Auto2_Dept;

	@FindBy(xpath = "//button[@id='createBroadcastButton']")
	public WebElement create_broadcast;

	@FindBy(xpath = "//*[@class='cross-icons']")
	public WebElement tools_crossBtn;

	@FindBy(xpath = "//*[@class='model-bg ng-star-inserted']")
	public WebElement outsideclick;

	@FindBy(xpath = "//*[@class='ng-tns-c34-9 ng-star-inserted']//option")
	public WebElement tools_BM_department;

	@FindBy(xpath = "//button[contains(text(),'Upcoming')]")
	public WebElement tools_BM_upcoming;

	@FindBy(xpath = "//button[contains(text(),'Past')]")
	public WebElement tools_BM_past;

	@FindBy(xpath = "//button[contains(text(),'Drafts')]")
	public WebElement tools_BM_draft;

	@FindBy(xpath = "//i[contains(text(),'All your upcoming broadcasts will appear here.')]")
	public WebElement tools_BM_No_upcomingBroadcasts_msg;

	@FindBy(xpath = "//i[contains(text(),'You have no past broadcasts.')]")
	public WebElement tools_BM_No_past_BM_msgs;

	@FindBy(xpath = "//i[contains(text(),'You can save your draft broadcasts.')]")
	public WebElement tools_BM_NO_draft_msg;

	@FindBy(xpath = "//button[@id='broadcast_viewRecipients']")
	public WebElement tools_BM_viewrecipients;

	@FindBy(xpath = "//input[@id='inputBroadcastTagData']")
	public WebElement tools_BM_input_To_searchTags;

	@FindBy(xpath = "//input[@id='broadcast_name']")
	public WebElement tools_BM_title;

	@FindBy(xpath = "//textarea[@id='broadcast_message']")
	public WebElement tools_BM_message;

	@FindBy(xpath = "//h1[@class='mb-0 content-center']")
	public WebElement tools_BM_recipentCount;

	@FindBy(xpath = "//div[@class='contact-name d-flex']")
	public WebElement tools_BM_create_SearchTag1;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[2]")
	public WebElement tools_BM_create_SearchTag2;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[3]")
	public WebElement tools_BM_create_SearchTag3;

	@FindBy(xpath = "//input[@id='broadcastMoreTargeted']")
	public WebElement tools_BM_create_moretarget;

	@FindBy(xpath = "//input[@id='broadcastLessTargeted']")
	public WebElement tools_BM_create_lesstarget;

	@FindBy(xpath = "//*[@class='icon contact-tag-cross-icon']")
	public WebElement tools_BM_create_tag_crossmark;

	@FindBy(xpath = "(//*[@class='col-4 overflow-hidden'])[1]")
	public WebElement tools_BM_create_recipients_list;

	@FindBy(xpath = "//button[@id='viewRecipients_backId']")
	public WebElement tools_BM_create_viewrecipients_back;

	@FindBy(xpath = "//input[@id='broadcast_optOut']")
	public WebElement tools_BM_create_msg_optout_checkbox;

	@FindBy(xpath = "//div[contains(text(),'Title must be at least 3 characters.')]")
	public WebElement tools_BM_create_title3Char_errormsg;

	@FindBy(xpath = "//div[contains(text(),'Message length exceeding the character limit.')]")
	public WebElement tools_BM_create_msg_optout_checkbox_errormsg;

	@FindBy(xpath = "//button[@id='broadcast_addAttachment']")
	public WebElement tools_BM_create_attachment;

	@FindBy(xpath = "//input[@id='broadcast_sendNow']")
	public WebElement tools_BM_create_sendNow;

	@FindBy(xpath = "//input[@id='broadcast_Schedule']")
	public WebElement tools_BM_create_ScheduleLater;

	@FindBy(xpath = "//app-radio-button[@id='isRecurring']//span[@class='checkmark ']")
	public WebElement tools_BM_create_make_this_broadCast_recurring;

	@FindBy(xpath = "//div[contains(text(),'Please read the terms of service and verify that you agree.')]")
	public WebElement tools_BM_create_termsandCond_errormsg;

	@FindBy(xpath = "//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")
	public WebElement tools_BM_create_agree_termsandCond;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement tools_BM_create_Send_BroadCast;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement tools_BM_create_Schedule_BroadCast;

	@FindBy(xpath = "//button[@id='broadcast_DraftButton']")
	public WebElement tools_BM_create_Save_Draft;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement tools_BM_create_DateandTime_button;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[4]")
	public WebElement tools_BM_create_time;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[1]")
	public WebElement increase_1hr_ScheduleTime;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[3]")
	public WebElement tools_BM_create_time_upArrow;

	@FindBy(xpath = "//div[contains(text(),'Please select a date and time in the future.')]")
	public WebElement tools_BM_create_select_Future_DateandTime_errorMsg;

	@FindBy(xpath = "(//*[@class='owl-dt-control-content owl-dt-control-button-content'])[5]")
	public WebElement tools_BM_create_select_DateandTime_set;

	@FindBy(xpath = "//select[@class='form-control ng-pristine ng-valid ng-touched']")
	public WebElement tools_BM_create_recurring_repeat;

	@FindBy(xpath = "//option[@id='Daily at this time_recurring']")
	public WebElement tools_BM_create_daily_atThis_time_recurring;

	@FindBy(xpath = "//option[@id='Weekly on (Selected day)_recurring']")
	public WebElement tools_BM_create_weekly_on_selectedDay_recurring;

	@FindBy(xpath = "//option[@id='Monthly on the (Date)_recurring']")
	public WebElement tools_BM_create_Monthly_onThe_Date_recurring;

	@FindBy(xpath = "//option[@id='Yearly on (Date) (Month)_recurring']")
	public WebElement tools_BM_create_yearly_onThe_Date_recurring;

	@FindBy(xpath = "//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement tools_BM_create_toaster;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public WebElement tools_BM_toaster;

	@FindBy(xpath = "//a[@id='scheduledMessageTab']")
	public WebElement tools_BM_create_SM;

	@FindBy(xpath = "//button[@id='warningInfoSave']")
	public WebElement tools_BM_create_PopUp_Draft;

	@FindBy(xpath = "//button[@id='warningInfoSave']")
	public WebElement tools_BM_manage_PopUp_save;

	@FindBy(xpath = "//button[contains(text(),'DISCARD')]")
	public WebElement tools_BM_create_PopUp_Discard;

	@FindBy(xpath = "//button[@id='createScheduleButton']")
	public WebElement tools_BM_create_create_SM;

	@FindBy(xpath = "//a[@id='templatesTab']")
	public WebElement tools_BM_create_create_Templates;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement tools_BM_create_create_Tags;

	@FindBy(xpath = "//a[@id='automationsTab']")
	public WebElement tools_BM_create_create_Automations;

	@FindBy(xpath = "//a[@id='advocateTab']")
	public WebElement tools_BM_create_create_Advocate;

	@FindBy(xpath = "//button[@id='createScheduleButton']")
	public WebElement tools_create_SM;

	@FindBy(xpath = "//a[@id='templatesTab']")
	public WebElement tools_Templates;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement toolsTags;

	@FindBy(xpath = "//a[@id='automationsTab']")
	public WebElement tools_Automations;

	@FindBy(xpath = "//a[@id='advocateTab']")
	public WebElement tools_Advocate;

	@FindBy(xpath = "//a[@id='reviewsTab']")
	public WebElement tools_BM_create_create_Review;

	@FindBy(xpath = "//a[@id='reviewsTab']")
	public WebElement Tools_Reviews;

	@FindBy(xpath = "//button[@id='addReviewSite']")
	public WebElement tools_BM_create_Review;

	@FindBy(xpath = "//textarea[@id='link']")
	public WebElement tools_BM_create_Review_link;

	@FindBy(xpath = "//button[@id='reviewCreate']")
	public WebElement tools_BM_create_Review_Create;

	@FindBy(xpath = "//button[@id='Google_button']")
	public WebElement tools_BM_create_Review_GoogleReview_Manage;

	@FindBy(xpath = "//button[@id='delete-review-message']")
	public WebElement tools_BM_create_Review_Manage_DeleteReview;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[2]")
	public WebElement tools_BM_GifIcon;

	@FindBy(xpath = "//*[@placeholder='Search']")
	public WebElement gif_search;

	@FindBy(xpath = "(//a[contains(text(),'View Terms')])[1]")
	public WebElement giphy_viewsTerms;

	@FindBy(xpath = "(//img[@id='giphy-container-body'])[1]")
	public WebElement tools_BM_Gif1;

	@FindBy(xpath = "//span[@class='subhead-2-name pl-1 attachment-name-maxwidth']")
	public WebElement tools_BM_Gif_display;

	@FindBy(xpath = "(//h6[@class='empty-list'])[1]")
	public WebElement tools_BM_Gif_ErrorMsg;

	@FindBy(xpath = "//app-svg-icon[@class='close-icon pointer-section ']")
	public WebElement tools_BM_Gif_text_close;

	@FindBy(xpath = "(//img[@id='giphy-container-body'])[2]")
	public WebElement tools_BM_Gif2;

	@FindBy(xpath = "(//img[@id='giphy-container-body'])[3]")
	public WebElement tools_BM_Gif3;

	@FindBy(xpath = "//div[@id='main-modal']")
	public WebElement giphy_outside;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[3]")
	public WebElement tools_BM_EmojiIcon;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[1]")
	public WebElement emoji;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[2]")
	public WebElement emoji1;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[3]")
	public WebElement emoji2;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[4]")
	public WebElement emoji3;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[5]")
	public WebElement emoji4;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[6]")
	public WebElement emoji5;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[7]")
	public WebElement emoji6;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[8]")
	public WebElement emoji7;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[9]")
	public WebElement emoji8;

	@FindBy(xpath = "(//span[@class='emoji-mart-emoji ng-star-inserted'])[10]")
	public WebElement emoji9;

	@FindBy(xpath = "(//button[@id='broadcast_addAttachment'])[4]")
	public WebElement tools_BM_ReviewIcon;

	@FindBy(xpath = "//a[@id='healthScreeningTab']")
	public WebElement tools_BM_Health_Screenings;

	@FindBy(xpath = "//button[@id='createTemplateButton']")
	public WebElement tools_BM_create_create_New_Template;

	@FindBy(xpath = "//button[@id='createTagButton']")
	public WebElement tools_BM_create_create_New_Tag;

	@FindBy(xpath = "//button[@id='createAutomationButton']")
	public WebElement tools_BM_create_create_New_Automation;

	@FindBy(xpath = "//button[@id='gotoReflection']")
	public WebElement tools_BM_create_create_Advocate_gotoReflection;

	@FindBy(xpath = "//button[@id='addReviewSite']")
	public WebElement tools_BM_create_create_ADDReview;

	@FindBy(xpath = "//button[contains(text(),'Activate')]")
	public WebElement tools_BM_Health_Screening_Activate;

	@FindBy(xpath = "//*[@id='broadcasts_breadcrumId']")
	public WebElement tools_BM_create_broadCast_Link;

	@FindBy(xpath = "//a[@id='broadcasts_breadcrumId']")
	public WebElement tools_BM_create_broadCastLink;

	@FindBy(xpath = "d-modal col-sm-9 col-lg-8 col-xl-7 ng-star-inserted")
	public WebElement tools_BM_create_outsideclick;

	@FindBy(xpath = "//app-date-time[@class='date-margin']//input[@id='appDateTimeId']")
	public WebElement tools_BM_create_recurring_appDateTime;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement tools_BM_Schedule_appDateTime;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]")
	public WebElement tools_BM_create_recurring_month;

	@FindBy(xpath = "//td[@class='owl-dt-calendar-cell owl-dt-year-2020 ng-star-inserted']")
	public WebElement tools_BM_create_recurring_year;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[12]")
	public WebElement tools_BM_create_recurring_lessMonth1;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[12]")
	public WebElement tools_BM_create_recurring_lessMonth;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]")
	public WebElement tools_BM_create_recurring_appDateTime_Month_Data;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]")
	public WebElement tools_BM_create_recurring_appDateTime_Date_Data;

	@FindBy(xpath = "(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[1]")
	public WebElement tools_BM_create_recurring_appDateTime_DecreaseMonth;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[15]")
	public WebElement tools_BM_create_recurring_appDateTime_SelectDate;

	@FindBy(xpath = "(//span[contains(text(),'15')])[2]")
	public WebElement tools_BM_create_recurring_appDateTime_SelectDate1;

	@FindBy(xpath = "(//span[contains(text(),'15')])[3]")
	public WebElement tools_BM_create_recurring_appDateTime_SelectDate2;

	@FindBy(xpath = "//div[contains(text(),'Please select a date in the future.')]")
	public WebElement tools_BM_create_recurring_appDateTime_inFuture_errorMsg;

	@FindBy(xpath = "//a[contains(text(),'W')]")
	public WebElement tools_BM_create_recurring_W;

	@FindBy(xpath = "//a[contains(text(),'Th')]")
	public WebElement tools_BM_create_recurring_Th;

	@FindBy(xpath = "//a[contains(text(),'Su')]")
	public WebElement tools_BM_create_recurring_su;

	@FindBy(xpath = "(//span[contains(text(),'Meeting')])[1]")
	public WebElement tools_BM_create_recurring_Past_comparing_Title;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0 inline-display border-bt-text'])[1]")
	public WebElement tools_BM_create_upcoming_list_Title;

	@FindBy(xpath = "(//span[@class='m-title'])[1]")
	public WebElement tools_BM_past_list_Title;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[1]")
	public WebElement tools_BM_create_upcoming_list_DateAndTime;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[1]")
	public WebElement tools_BM_past_list_DateAndTime;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0 inline-display border-bt-text'])[1]")
	public WebElement tools_BM_create_Draft_list_Title;

	@FindBy(xpath = "//div[@class='text-danger mt-2 ng-tns-c37-9 ng-star-inserted']")
	public WebElement tools_BM_select_current_Day;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_upcoming_manage;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_upcoming_manage2;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement tools_BM_create_upcoming_manage_delete;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement tools_BM_create_upcoming_Reuse_cancel;

	@FindBy(xpath = "//button[contains(text(),'Re-Use')]")
	public WebElement tools_BM_create_upcoming_Reuse;

	@FindBy(xpath = "//button[contains(text(),'View')]")
	public WebElement tools_BM_create_Past_view;

	@FindBy(xpath = "(//button[contains(text(),'View')])[2]")
	public WebElement tools_BM_create_Past_view1;

	@FindBy(xpath = "(//button[contains(text(),'View')])[2]")
	public WebElement tools_BM_create_Past_view2;

	@FindBy(xpath = "(//button[contains(text(),'View')])[3]")
	public WebElement tools_BM_create_Past_view3;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])")
	public WebElement tools_BM_create_past_Reuse;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])[2]")
	public WebElement tools_BM_create_past_Reuse1;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])[3]")
	public WebElement tools_BM_create_past_Reuse2;

	@FindBy(xpath = "(//button[contains(text(),'Re-Use')])[4]")
	public WebElement tools_BM_create_past_Reuse3;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement tools_BM_create_Draft_manage;

	@FindBy(xpath = "//button[contains(text(),'Delete')]")
	public WebElement tools_BM_create_Draft_Delete;

	@FindBy(xpath = "(//span[@class='m-title'])[1]")
	public WebElement tools_BM_upcoming_list_title;

	@FindBy(xpath = "(//span[@class='m-title'])[2]")
	public WebElement tools_BM_upcoming_list_title1;

	@FindBy(xpath = "(//span[@class='m-title'])[3]")
	public WebElement tools_BM_upcoming_list_title2;

	@FindBy(xpath = "(//span[@class='m-title'])[4]")
	public WebElement tools_BM_upcoming_list_title3;

	@FindBy(xpath = "(//span[@class='m-title'])[5]")
	public WebElement tools_BM_upcoming_list_title4;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[1]")
	public WebElement tools_BM_upcoming_list_DateTime;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[2]")
	public WebElement tools_BM_upcoming_list_DateTime1;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[3]")
	public WebElement tools_BM_upcoming_list_DateTime2;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[4]")
	public WebElement tools_BM_upcoming_list_DateTime3;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-3 col-md-3 col-sm-3 mt-1 px-0  text-right p-0'])[5]")
	public WebElement tools_BM_upcoming_list_DateTime4;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[1]")
	public WebElement tools_BM_upcoming_list_recurring_icon;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[2]")
	public WebElement tools_BM_upcoming_list_recurring_icon1;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[3]")
	public WebElement tools_BM_upcoming_list_recurring_icon2;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[4]")
	public WebElement tools_BM_upcoming_list_recurring_icon3;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[5]")
	public WebElement tools_BM_upcoming_list_recurring_icon4;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[1]")
	public WebElement tools_BM_Past_list_DateTime;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[2]")
	public WebElement tools_BM_Past_list_DateTime1;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[3]")
	public WebElement tools_BM_Past_list_DateTime2;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[4]")
	public WebElement tools_BM_Past_list_DateTime3;

	@FindBy(xpath = "(//span[@class='inline-display dropdown-toggle past-bm-title'])[5]")
	public WebElement tools_BM_Past_list_DateTime4;

	@FindBy(xpath = "//button[contains(text(),'SAVE DRAFT')]")
	public WebElement tools_BM_Past_Save_Draft;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[1]")
	public WebElement tools_BM_Draft_noDateSelected;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[2]")
	public WebElement tools_BM_Draft_noDateSelected1;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[3]")
	public WebElement tools_BM_Draft_noDateSelected2;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[4]")
	public WebElement tools_BM_Draft_noDateSelected3;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[5]")
	public WebElement tools_BM_Draft_noDateSelected4;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[1]")
	public WebElement tools_BM_Draft_manage;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[2]")
	public WebElement tools_BM_Draft_manage1;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[3]")
	public WebElement tools_BM_Draft_manage2;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[4]")
	public WebElement tools_BM_Draft_manage3;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[5]")
	public WebElement tools_BM_Draft_manage4;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[1]")
	public WebElement tools_BM_Draft_Delete;

	@FindBy(xpath = "//button[contains(text(),'Discard')]")
	public WebElement tools_BM_Draft_Delete_Discard;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[2]")
	public WebElement tools_BM_Draft_Delete1;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[3]")
	public WebElement tools_BM_Draft_Delete2;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[4]")
	public WebElement tools_BM_Draft_Delete3;

	@FindBy(xpath = "(//button[contains(text(),'Delete')])[5]")
	public WebElement tools_BM_Draft_Delete4;

	@FindBy(xpath = "//button[@id='warningInfoDiscard']")
	public WebElement tools_BM_Draft_Delete_discard;

	@FindBy(xpath = "//button[contains(text(),'CANCEL')]")
	public WebElement tools_BM_Draft_Delete_Cancel;

	@FindBy(xpath = "//button[contains(text(),'SAVE DRAFT')]")
	public WebElement tools_BM_Draft_Save_Draft;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement tools_BM_draft_manage_delete;

	@FindBy(xpath = "(//app-svg-icon[@class='icon recurring'])[1]")
	public WebElement tools_BM_draft_recurring_icon;

	@FindBy(xpath = "(//span[@class='pull-right'])[5]")
	public WebElement tools_BM_Draft_manage_Schedule_DateAndTime;

	@FindBy(xpath = "//span[@class='close-0']")
	public WebElement tools_BM_remove_attachment;

	@FindBy(xpath = "//span[contains(text(),' Auto2 ')]")
	public WebElement auto2_Contact_IN_Tags;

	@FindBy(xpath = "//span[contains(text(),' QA10 ')]")
	public WebElement QA_Contact_IN_Tags;

	@FindBy(xpath = "//div[contains(text(),'Your broadcast message has been saved as a draft.')]")
	public WebElement BM_Draft_Toaster;

	@FindBy(xpath = "//div[contains(text(),' Your broadcast  has been successfully updated. ')]")
	public WebElement Updated_Toaster;

	@FindBy(xpath = "//div[contains(text(),'Your broadcast has been successfully updated for Auto1.')]")
	public WebElement BM_Updated_Toaster_Auto1;

	@FindBy(xpath = "//div[contains(text(),'Your broadcast has been successfully updated for Automations.')]")
	public WebElement BM_Updated_Toaster_Automations;

	@FindBy(xpath = "//div[contains(text(),'Broadcast draft has been deleted.')]")
	public WebElement Draft_Deleted_Toaster;

	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement Auto1_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement Automation_Contact;

	@FindBy(xpath = "//input[@id='broadcast_optOut']")
	public WebElement BM_Optout_Checkbox;

}
