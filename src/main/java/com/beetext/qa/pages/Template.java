package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Template extends TestBase {

	public Template() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//h4[contains(text(),'QA10')]") // automation@yopmail.com
	public WebElement pin_conversation;
	@FindBy(xpath = " //h4[normalize-space()='QA10']") // auto1 login conversation
	public WebElement select_conversations12;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement input_box;
	
	@FindBy(xpath = "//a[@id='toolsCloseButtonId']")
	public WebElement close_tools_popup;
	@FindBy(xpath = "//header/a[@id='settingsClose']/*[1]/*[1]")
	public WebElement close_settings_popup;


	@FindBy(xpath = "//button[@id='chatInput-sendButton']")
	public WebElement schedule_button;

	@FindBy(xpath = "//button[@name='button'][5]")
	public WebElement temlate_icon;

	@FindBy(xpath = " //body//app-root//img[9]")
	public WebElement select_giff_image;

	@FindBy(xpath = "//button[contains(text(),'OK')]")
	public WebElement okbutton;

	@FindBy(xpath = " //i[contains(text(),'Create your first personal template.')]")
	public WebElement noPersonal_template;
	// Create a template for your departments
	@FindBy(xpath = " //i[contains(text(),'Create a template for your numbers.')]")
	public WebElement noshared_template;

	@FindBy(xpath = "//p[contains(text(),'Maximum 10 attachments are allowed.')]")
	public WebElement morethan_10attachments;

	@FindBy(xpath = "//div[contains(text(),'Quick Templates')]")
	public WebElement quick_template_headding;

	@FindBy(xpath = "//div[contains(text(),'No Templates found!')]")
	public WebElement no_quick_template;

	@FindBy(xpath = " //div[@class='template-name'][normalize-space()='attachment']")
	public WebElement quick_template_10_attachment;

	@FindBy(xpath = "//button[contains(text(),'Shared')]")
	public WebElement shared_template_button;

	@FindBy(xpath = "//div[@class='template-name'][normalize-space()='3232332']")
	public WebElement select_quick_template_list;

	@FindBy(xpath = "//button[contains(text(),'Personal')]")
	public WebElement personal_template_button;
	@FindBy(xpath = " //app-radio-button[@id='template_selectall']//span[contains(@class,'checkmark')]")
	public WebElement shared_template_click_selectall;

	@FindBy(xpath = "//div[@class='col-sm-8 col-md-8 col-lg-6 col-12 h-100 chat-list chat-md-container d-none d-block']//li[10]//div[1]//span[2]")
	public WebElement remove_attachment;

	@FindBy(xpath = "//div[contains(text(),'3232332')]/../parent::div/../div[2]/button[contains(text(),'USE')]") // add
																													// xpath
	public WebElement select_Template;

	@FindBy(xpath = "//div[contains(text(),'attachment')]/../parent::div/../div[2]/button[contains(text(),'USE')]") // add
																													// xpath
	public WebElement select_attachment_Template;

	@FindBy(xpath = "//div[contains(text(),'one attachment')]/../parent::div/../div[2]/button[contains(text(),'USE')]") // add
																														// xpath
	public WebElement select_oneattachment_Template;

	@FindBy(xpath = "//div[contains(text(),'attachments')]/../parent::div/../div[2]/button[contains(text(),'USE')]") // add
																														// xpath
	public WebElement select_shared_attachment_Template;

	@FindBy(xpath = "//div[contains(text(),'shared one attachment')]/../parent::div/../div[2]/button[contains(text(),'USE')]") // add
																																// xpath
	public WebElement select_shared_oneattachment_Template;

	@FindBy(xpath = "//div[contains(text(),'sharedtemplate')]/../parent::div/../div[2]/button[contains(text(),'USE')]") // add
																														// xpath
	public WebElement select_shared_Template;

	@FindBy(xpath = "//span[contains(text(),'Create template')]")
	public WebElement create_template_button;

	@FindBy(xpath = " //input[@id='template_title']")
	public WebElement create_template_title;

	@FindBy(xpath = " //textarea[@id='template_message']")
	public WebElement create_template_message;

	@FindBy(xpath = "//button[@id='template_shared']")
	public WebElement create_shared_template;

	@FindBy(xpath = "//button[normalize-space()='CREATE TEMPLATE']")
	public WebElement create_templates_button;

	@FindBy(xpath = " //button[@id='sampleButton']")
	public WebElement manage_template;

	@FindBy(xpath = " //button[normalize-space()='Delete Template']")
	public WebElement manage_delete_template;

	@FindBy(xpath = " //button[normalize-space()='Confirm Delete Template']")
	public WebElement manage_confirm_delete_template;

	@FindBy(xpath = " //button[@id='sharedButton']")
	public WebElement manage_shared_template;

	@FindBy(xpath = "//button[@class='tp-btn btn-personal'] ")
	public WebElement personal_template_tab;

	@FindBy(xpath = "//button[@id='template_personal']")
	public WebElement personal_option;

	@FindBy(xpath = "//button[@id='sharedTemplates']")
	public WebElement select_shared_list;
	@FindBy(xpath = " //button[normalize-space()='Delete Template']")
	public WebElement manage_shared_delete_template;

	@FindBy(xpath = " //button[normalize-space()='Confirm Delete Template']")
	public WebElement manage_shared_confirm_delete_template;
	// div[contains(text(),'Your template has been successfully created for
	// sample.')]
	@FindBy(xpath = "//div[@aria-label='Your template sample has been created.']")
	public WebElement create_template_toaster_msg;
	// div[contains(text(),'Your template has been successfully created for
	// shared.')]
	@FindBy(xpath = "//div[@aria-label='Your template shared has been created.']")
	public WebElement create_shared_template_toaster_msg;
}
