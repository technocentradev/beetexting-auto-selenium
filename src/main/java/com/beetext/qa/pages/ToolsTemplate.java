package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class ToolsTemplate extends TestBase {

	public ToolsTemplate() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}
	@FindBy(xpath = "//*[@id='email-id']")
	public  WebElement user_id;
	
	@FindBy(xpath = "//*[@id='login-submit']")
	public  WebElement loginbttn;
	@FindBy(xpath = "//*[@id='password-id']")
	public  WebElement Passsword_ID;
	
	@FindBy(xpath = " //span[@class='tools s-title']")
	public WebElement tools_link;
	//i[normalize-space()='You can save your templates as drafts.']
	@FindBy(xpath = "//i[normalize-space()='You can save your templates as drafts.']")
	public WebElement draf_default_message;
	
	
	@FindBy(xpath = "//a[normalize-space()='Templates']")
	public WebElement templateLink;

	@FindBy(xpath = "//button[normalize-space()='Create Template']")
	public WebElement addTemplate_btn;

	@FindBy(xpath = "//input[@id='template_title']")
	public WebElement Template_title;

	@FindBy(xpath = "//div[contains(text(),'Title must be at least 3 characters.')]")
	public WebElement Template_title_min_3_char;

	@FindBy(xpath = "//div[contains(text(),\"Template with name 'sharedtemplate' already exists\")]")
	public WebElement shared_Template_title_alrady_exist;

	@FindBy(xpath = " //div[contains(text(),\"Template with name 'attachments' already exists.\")]")
	public WebElement personal_Template_title_alrady_exist;

	@FindBy(xpath = " //textarea[@id='template_message']")
	public WebElement Template_message;

	@FindBy(xpath = "//button[normalize-space()='CREATE TEMPLATE']")
	public WebElement create_Template_btn;

	@FindBy(xpath = "//button[normalize-space()='SAVE DRAFT']")
	public WebElement draft_Template_btn;

	@FindBy(xpath = "//button[@id='template_shared']")
	public WebElement template_shared_option;

	@FindBy(xpath = "//button[@id='template_personal']")
	public WebElement template_personal_option;

	@FindBy(xpath = "//app-radio-button[@id='template_selectall']//span[contains(@class,'checkmark')]")
	public WebElement selectall_departments;

	@FindBy(xpath = " //app-radio-button[@id='Auto1_Department']//span[contains(@class,'checkmark')]")
	public WebElement select_specifif_department;
	// app-radio-button[@id='Tollfree(1)_Department']//span[contains(@class,'checkmark')]
	@FindBy(xpath = "//app-radio-button[@id='Automation TollFree_Department']//span[contains(@class,'checkmark')]")
	public WebElement select_2nd_department;

	@FindBy(xpath = "//button[@id='template_personal']")
	public WebElement templat_personal_option;

	@FindBy(xpath = "//div[@class='form-group tools-icon-container']//button[1]")
	public WebElement attachment_link;

	@FindBy(xpath = "//*[@class='icon tools-giphy']//*[@id='Capa_1']")
	public WebElement attachment_Gif_link;

	@FindBy(xpath = "//div[@id='main-modal']//img[1]")
	public WebElement select_Gif_attachment;

	@FindBy(xpath = "//a[1]//*[@class='icon emoji-icon']//*[@id='Layer_1']")
	public WebElement emoji_link;

	@FindBy(xpath = "//emoji-category[2]//div[1]//ngx-emoji[1]//span[1]//span[1]")
	public WebElement emoji_link_icon;

	// tabs

	@FindBy(xpath = "//button[@id='personalTemplates']")
	public WebElement personal_tab;

	@FindBy(xpath = "//button[@id='sharedTemplates']")
	public WebElement shared_tab;

	@FindBy(xpath = "//button[@id='draftTemplates']")
	public WebElement draft_tab;

	// manage template

	@FindBy(xpath = "//button[@id='personalButton']")
	public WebElement Manage_personal_template;

	@FindBy(xpath = "//button[@id='sharedButton']")
	public WebElement Manage_shared_template;

	@FindBy(xpath = "//button[@id='shButton']")
	public WebElement Manage_shared_templates;

	@FindBy(xpath = "//button[@id='draftButton']")
	public WebElement Manage_draft_templates_nodata;

	@FindBy(xpath = "//button[@id='personalButton']")
	public WebElement Manage_draft_templates_only_title;

	@FindBy(xpath = "//button[@id='sharedButton']")
	public WebElement Manage_draft_sharedtemplates_only_title;

	@FindBy(xpath = "//button[@id='Button']")
	public WebElement Manage_draft_nodata;

	@FindBy(xpath = "//button[@id='delete-template-message']")
	public WebElement delete_template;

	@FindBy(xpath = " //button[normalize-space()='Confirm Delete Template']")
	public WebElement confirm_delete_template;

	@FindBy(xpath = " //button[@id='template_CreateButton']")
	public WebElement update_template;

	// draft toaster messages

	@FindBy(xpath = "//div[contains(@aria-label,'has been saved as a draft.')]")
	public WebElement draft_template_toaster_messages;
	@FindBy(xpath = "//div[@aria-label='Your template has been saved as a draft.']")
	public WebElement draft_template_toaster_message;

	@FindBy(xpath = "//div[@aria-label='Your template shared has been deleted.']")
	public WebElement draft_template_delete_toaster_message;

	@FindBy(xpath = "//div[@aria-label='Your template personal has been deleted.']")
	public WebElement draft_template_delete_toaster_messages;
	// div[@aria-label=' Your template has been deleted.']

	// div[contains(@aria-label,'Your Template has been deleted.')]
	@FindBy(xpath = "//div[@aria-label='Your template has been deleted.']")
	public WebElement no_draft_template_delete_toaster_messages;

	// personal template toaster

	@FindBy(xpath = "//div[@aria-label='Your template personal has been created.']")
	public WebElement personal_template_toaster_message;

	// div[@aria-label='Your template has been successfully updated.']
	@FindBy(xpath = "//div[@aria-label='Your template personal has been successfully updated.']")
	public WebElement personal_template_update_toaster_message;

	@FindBy(xpath = "//div[@aria-label='Your template has been successfully updated.']")
	public WebElement personal_template_update_toaster_message_clickon_save;

	@FindBy(xpath = "//div[@aria-label='Your template personal has been deleted.']")
	public WebElement personal_template_delete_toaster_message;

	// shared toaster messages

	@FindBy(xpath = "//div[@aria-label='Your template shared has been created.']")
	public WebElement shared_template_toaster_message;
	// div[@aria-label='Your template shared has been updated.

	@FindBy(xpath = "//div[@aria-label='Your template shared has been successfully updated.']")
	public WebElement shared_template_update_toaster_message;

	@FindBy(xpath = "//div[@aria-label='Your template shared has been deleted.']")
	public WebElement shared_template_delete_toaster_message;

	// Breads_scrum_link
	// ol[@class='breadcrumb']
	@FindBy(xpath = "//ol[@class='breadcrumb']")
	public WebElement breadscrum;

	@FindBy(xpath = "//body/app-root[1]/div[1]/app-layout[1]/main[1]/section[1]/div[2]/div[1]/app-tools[1]/div[1]/app-templates[1]/div[1]/nav[1]/ol[1]/li[1]/a[1]")
	public WebElement breadscrumlinks;
	@FindBy(xpath = "//app-tools[1]//div[1]//app-templates[1]//div[1]//nav[1]//ol[1]//li[1]//a[1]")
	public WebElement breadscrumlink;

	@FindBy(xpath = " //button[normalize-space()='SAVE DRAFT']")
	public WebElement save_draft;

	@FindBy(xpath = "//button[normalize-space()='DISCARD']")
	public WebElement discard_draft;

	@FindBy(xpath = " //*[@class='icon paper-clip']//*[@id='Layer_1']")
	public WebElement attachment;

	@FindBy(xpath = "//app-templates[1]//div[1]//div[1]//form[1]//div[1]//div[4]//div[1]//div[2]//ul[1]//li[1]//div[1]//span[2]")
	public WebElement remove_attachment;

	@FindBy(xpath = "//header/a[@id='toolsCloseButtonId']/*[1]/*[1]")
	public WebElement close_tools_popup;

	@FindBy(xpath = "//button[@id='warningInfoSave']")
	public WebElement savedraft_button;

	// div[contains(text(),'Your template has been saved as a draft.')]
	@FindBy(xpath = "//div[@role='alertdialog']")
	public WebElement toastermessage;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement tags_link;

	@FindBy(xpath = "/body/app-root[1]/div[1]/app-layout[1]/main[1]/section[1]")
	public WebElement outsideclick;

	@FindBy(xpath = " //div[@class='text-center pd-10 head-color ng-star-inserted']")
	public WebElement personal_default_message;
	//i[normalize-space()='Create a template for your numbers.']
	@FindBy(xpath = "//div[1]/div[4]/div[1]/div[1]/div[1]/i[1][normalize-space()='Create a template for your numbers.'] ")
	public WebElement shared_default_message;

	@FindBy(xpath = " //i[normalize-space()='You can save your templates as drafts.']")
	public WebElement draft_default_message;

	@FindBy(xpath = " //button[@id='warningInfoSave']")
	public WebElement save_button;

	@FindBy(xpath = "//button[contains(text(),'DISCARD')]")
	public WebElement discard_button;

	@FindBy(xpath = "//div[@aria-label='Your template personal has been successfully updated.']")
	public WebElement update_toaster_message;

	@FindBy(xpath = "//div[@aria-label='Your template shared has been successfully updated.']")
	public WebElement update_toaster_messages;

	// div[contains(@aria-label,'Your Template has been deleted.')]

	@FindBy(xpath = "//span[normalize-space()='[firstname]']")
	public WebElement firstnamedrag;

	@FindBy(xpath = "//span[normalize-space()='[companyname]']")
	public WebElement companynamedrag;

	@FindBy(xpath = "//div[contains(@aria-label,'Your template personal has been deleted.')]")
	public WebElement delete_toaster;

	@FindBy(xpath = "//li[2]//div[1]//span[2]")
	public WebElement remove_gif;
	
	// draft list delete button xpath
	//div[@class='past-broadcast-messages pl-2']//div[1]//div[1]//div[2]//div[1]//div[2]//button[1]
	@FindBy(xpath = "//button[@id='sharedDelete']")
	public WebElement deletebutton;
	@FindBy(xpath = "	//div[@class='tag-font Mt-10 modal-container color-value b ng-star-inserted']")
	public WebElement conformationpop_headding;
	
	@FindBy(xpath = "//button[normalize-space()='CANCEL']")
	public WebElement  cancelButton;

}
