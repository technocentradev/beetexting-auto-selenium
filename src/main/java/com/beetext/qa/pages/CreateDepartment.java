package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class CreateDepartment extends TestBase {

	// Initializing the page objects:
	public CreateDepartment() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	// create new department Number

	@FindBy(xpath = "//button[@id='deptCreateNewNumber']")
	public WebElement createNewNum;

	@FindBy(xpath = "//input[@id='deptNewnumberName']")
	public WebElement departmentName;

	@FindBy(xpath = "//body/app-root[1]/div[1]/app-department-number-name[1]/div[1]/div[1]/div[1]")
	public WebElement outsideClick;

	@FindBy(xpath = " //div[contains(text(),'Please give your number a name.')]")
	public WebElement deptNameRequired;

	@FindBy(xpath = "//div[contains(text(),'Your Number name must contain at least 3 character')] ")
	public WebElement deptName3chars;

	@FindBy(xpath = "//button[@id='deptNewNumberCreateButton']")
	public WebElement nextButtonClick;

	@FindBy(xpath = "//input[@id='deptNewAreacode']")
	public WebElement areaCode;

	@FindBy(xpath = "//div[contains(text(),'Area Code is required.')]")
	public WebElement areaCodeRequited;
	// div[contains(text(),'Please enter a valid three-digit US area code.')]
	@FindBy(xpath = "//div[contains(text(),'Please enter a valid three-digit area code.')]")
	public WebElement onlyDigits;

	@FindBy(xpath = "//button[@id='deptNewNumberCreateButton']")
	public WebElement createBtn;

	@FindBy(xpath = "//div[contains(text(),'Number Created Successfully.')]")
	public WebElement deptCreated;

	// create department textenable number

	@FindBy(xpath = " //button[@id='deptEnableExistingNumber']")
	public WebElement btnTxtEnableExisting;

	@FindBy(xpath = "//input[@id='deptNewnumberName']")
	public WebElement deptName;

	@FindBy(xpath = " //div[contains(text(),'Please give your number a name.')]")
	public WebElement ClerDeptName;

	@FindBy(xpath = "//div[contains(text(),'Your Number name must contain at least 3 character')]")
	public WebElement Chars_3validation;

	@FindBy(xpath = "//button[@id='deptNewnameNextBtn']")
	public WebElement nextButton;

	// create department xpath edning

	// phone number info page xpath starting

	@FindBy(xpath = "//div[contains(text(),'Phone Number Account Info')]")
	public WebElement phone_info;

	@FindBy(xpath = "//input[@id='existingPhoneNumber']")
	public WebElement phoneNumber;

	@FindBy(xpath = "//div[contains(text(),'Phone number is required.')]")
	public WebElement required_phone;

	@FindBy(xpath = "//input[@id='customerName']")
	public WebElement custName;

	@FindBy(xpath = "//div[contains(text(),'Name must be at least 3 characters.')]")
	public WebElement custNamelessthan3;

	@FindBy(xpath = "//input[@id='existingNumberStreet']")
	public WebElement street;

	@FindBy(xpath = "//input[@id='existingNumberCity']")
	public WebElement city;

	@FindBy(xpath = " //input[@id='existingNumberZip']")
	public WebElement zipcode;

	@FindBy(xpath = "//div[contains(text(),'Only numbers are allowed.')]")
	public WebElement zipcode_allowolyNumber;

	@FindBy(xpath = "//select[@id='existingNumberState']")
	public WebElement state;

	@FindBy(xpath = "//div[contains(text(),'State is required.')]")
	public WebElement state_required;

	@FindBy(xpath = "//select[@id='existingNumberState']//option")
	public WebElement options;

	@FindBy(xpath = "//span[@class='checkmark']")
	public WebElement radioButton;

	@FindBy(xpath = "//input[@id='existingNumberSignature']")
	public WebElement signature;

	@FindBy(xpath = " //div[contains(text(),'Min 3 characters')]")
	public WebElement signature_min3;

	@FindBy(xpath = "	//button[@id='deptNewNumberCreateButton']")
	public WebElement next_Button;

	@FindBy(xpath = "//button[contains(text(),'Call me!')]")
	public WebElement callme_Button;

	@FindBy(xpath = "//*[contains(@name,'verificationCode')]")
	public WebElement code;

	@FindBy(xpath = "//div[contains(text(),'The 6 digit verification you entered is invalid, p')]")
	public WebElement invalid_code;

	@FindBy(xpath = "//div[contains(text(),'Only numbers are allowed.')]")
	public WebElement onlyNumbers_code;

	@FindBy(xpath = " //button[contains(text(),'Verify')]")
	public WebElement verify_Button;

	@FindBy(xpath = "//button[contains(text(),'RESEND CODE')]")
	public WebElement callme_Again_Button;

	@FindBy(xpath = "//div[contains(text(),'Number Created Successfully.')]")
	public WebElement dept_created;

	@FindBy(xpath = " //div[contains(text(),'Phone number is already available with other org, ')]")
	public WebElement dept_already_exist;

	@FindBy(xpath = "//button[contains(text(),'Back')]")
	public WebElement back_Button;

	@FindBy(xpath = "//div[contains(text(),'Please read the terms of service and accept.')]")
	public WebElement err_terms_conditons_check;

	@FindBy(xpath = "//div[contains(text(),'Special characters are not allowed.')]")
	public WebElement err_city_name_notallow_specialchar;

	@FindBy(xpath = " //div[contains(text(),'City is required.')]")
	public WebElement err_required_validation;

	@FindBy(xpath = "//div[@aria-label='Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.']")
	public WebElement Numberalready_Exist;

	// phone number info page

	// phone number info page xpath eding

}
