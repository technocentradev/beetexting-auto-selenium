package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Tags_Pages extends TestBase {

	// Page Factory - Object Rep:

	// Page Factory - Object Rep:

	@FindBy(xpath = "//*[@id='email-id']")
	public WebElement user_id;

	@FindBy(xpath = "//*[@id='password-id']")
	public WebElement Passsword_ID;

	@FindBy(xpath = "//*[@id='login-submit']")
	public WebElement loginbttn;

	@FindBy(xpath = "//a[@id='toolsMainTab']")
	public WebElement tools_bttn;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement tools_tags_bttn;

	@FindBy(xpath = "//button[@id='createTagButton']")
	public WebElement tools_tags_createtag;

	@FindBy(xpath = "//input[@id='tag_searchContact']")
	public WebElement tools_tags_createtag_search_contact;

	@FindBy(xpath = "(//*[@id='tagInput'])[1]")
	public WebElement tools_tags_createtag_taginput;

	@FindBy(xpath = "//*[@id='tag_CreateButton']")
	public WebElement tools_tags_createtag_createbbtn;

	@FindBy(xpath = "//*[@class='mb-0']")
	public WebElement tools_tags_createtag_displaycount;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[1]")
	public WebElement tools_tags_createtag_applyall;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[2]")
	public WebElement tools_tags_createtag_select_contact;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[3]")
	public WebElement tools_tags_createtag_select_contact1;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[4]")
	public WebElement tools_tags_createtag_select_contact2;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[5]")
	public WebElement tools_tags_createtag_select_contact3;

	@FindBy(xpath = "(//label[@class='checkbox-inline']//*[@class='checkmark'])[5]")
	public WebElement tools_tags_createtag_select_contact4;

	@FindBy(xpath = "//div[contains(text(),'Tag already exists and cannot be added again.')]")
	public WebElement tools_tags_createtag_alreadytag_existed;

	@FindBy(xpath = "//div[contains(text(),' Please select contacts from the list to add the tag to. ')]")
	public WebElement tools_tags_createtag_select_contact_error;

	@FindBy(xpath = "//*[contains(text(),' technocentra ')]")
	public WebElement tools_tags_createtag_Contactdisplay1;

	@FindBy(xpath = "//div[contains(text(),'No contacts found!')]")
	public WebElement tools_tags_createtag_contact_notfound;

	@FindBy(xpath = "//div[contains(text(),'Limit Tag length to 30 characters.')]")
	public WebElement tools_tags_createtag_taglimit_30char_errormsg;

	@FindBy(xpath = "//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement tools_tags_createtag_tagcreated_toaster;

	@FindBy(xpath = "//button[@id='delete-tag']")
	public WebElement tools_tags_editTag_deleteoption;

	@FindBy(xpath = "//i[contains(text(),'Create your first tag and manage it here.')]")
	public WebElement tools_tags_createyour_firstTag_Msg;

	@FindBy(xpath = "//div[contains(text(),'Select the tag you would like to edit.')]")
	public WebElement tools_tags_select_your_tag_toEdit;

	@FindBy(xpath = "//button[@id='abcTagManage']")
	public WebElement tools_tags_abcTagmanage;

	@FindBy(xpath = "//button[@id='25TagManage']")
	public WebElement tools_tags_25Tagmanage;

	@FindBy(xpath = "//button[@id='@&*/TagManage']")
	public WebElement tools_tags_Special_Char_Tagmanage;

	@FindBy(xpath = "//button[contains(text(),'SAVE')]")
	public WebElement tools_tags_editTag_Save;

	@FindBy(xpath = "//button[contains(text(),'PROCEED')]")
	public WebElement tools_tags_editTag_proceed;

	@FindBy(xpath = "//button[@id='mnopqrTagManage']")
	public WebElement tools_tags_editTag_mnopqr_Tag;

	@FindBy(xpath = "//p[contains(text(),'No tags found!')]")
	public WebElement NO_Tags_MainPage;

	@FindBy(xpath = "//span[contains(text(),' QA10 ')]")
	public WebElement QA_Contact_IN_Tags;

	@FindBy(xpath = "//*[@class='cross-icons']")
	public WebElement tools_crossBtn;

	@FindBy(xpath = "//button[@id='tagRemoveConfirm']")
	public WebElement Conversation_Remove_Tag;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement automation_chat_contact;

	@FindBy(xpath = "//svg-icon[@id='mnopqr_deleteTag']")
	public WebElement mnopqrTag_delete_MainPage;

	@FindBy(xpath = "//input[@placeholder='Search tags']")
	public WebElement Search_Tags_Page;

	@FindBy(xpath = "//span[contains(text(),' Auto2 ')]")
	public WebElement auto2_Contact_IN_Tags;

	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement Auto1_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement Automation_Contact;

	@FindBy(xpath = "//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement tools_tags_tagedited_save_toaster;

	public Tags_Pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

}
