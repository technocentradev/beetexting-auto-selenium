package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Pin_Messages extends TestBase {

	public Pin_Messages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	// pin message xpath
	@FindBy(xpath = "//h4[contains(text(),'QA10')]")
	public WebElement select_conversation;

	@FindBy(xpath = "//div[@class='active-agent-list color-value']")
	public WebElement Team_popup;

	@FindBy(xpath = "//h4[contains(text(),'QA10')]") // automation@yopmail.com
	public WebElement pin_conversation;

	@FindBy(xpath = " //h4[normalize-space()='QA10']") // auto1 login conversation
	public WebElement select_conversations12;

	@FindBy(css = "h4[id='3091212131_conversation']")
	public WebElement select_conversations;

	@FindBy(xpath = "//h4[normalize-space()='QA10']")
	public WebElement noagentconversion;

	@FindBy(xpath = " //*[@id='pinIcon']")
	public WebElement pinIcon;

	@FindBy(xpath = "//div[contains(text(),'Teammates')]")
	public WebElement Teammates_popup;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement input_box;

	@FindBy(xpath = "//div[contains(text(),'auto1')]")
	public WebElement selectAgentName;

	@FindBy(xpath = "//div[contains(text(),'dummyagent')]")
	public WebElement selectsecondAgentName;

	@FindBy(xpath = "//div[contains(text(),\"We didn't find any matches.\")]")
	public WebElement no_Agent_Found;

	@FindBy(xpath = "//button[@id='chatInput-sendButton']")
	public WebElement postButton;

	@FindBy(xpath = "//*[@class ='messages ng-star-inserted']")
	public WebElement pinmessage;

	@FindBy(xpath = "//div[@id='hoverSelectDepartment']")
	public WebElement hoverdepartmentdropdown;

	@FindBy(xpath = "//span[@id='sample(2)_Department']")
	public WebElement selectsample2dept;

}
