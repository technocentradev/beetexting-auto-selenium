package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class HealthScreen_pages extends TestBase {

	public HealthScreen_pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//a[@id='healthScreeningTab']")
	public WebElement healthscreen;

	@FindBy(xpath = "(//button[@class='btn btn-link btn-chat-link ng-star-inserted'])[3]")
	public WebElement healthscreen_inChat;

	@FindBy(xpath = "//button[contains(text(),'Activate')]")
	public WebElement healthscreen_activate;

	@FindBy(xpath = "//button[contains(text(),'Back')]")
	public WebElement activate_back;

	@FindBy(xpath = "//button[contains(text(),'Recurring')]")
	public WebElement recurring;

	@FindBy(xpath = "//i[contains(text(),'All your recurring health screenings will appear here.')]")
	public WebElement recurring_Msg;

	@FindBy(xpath = "//button[contains(text(),'Upcoming')]")
	public WebElement upcoming;

	@FindBy(xpath = "//i[contains(text(),' All your upcoming health screenings will appear here.')]")
	public WebElement upcoming_Msg;

	@FindBy(xpath = "//button[contains(text(),'Past')]")
	public WebElement past;

	@FindBy(xpath = "//i[contains(text(),' All your past health screenings will appear here.')]")
	public WebElement past_msg;

	@FindBy(xpath = "//a[contains(text(),' View Preview')]")
	public WebElement view_preview;

	@FindBy(xpath = "(//a[contains(text(),'Features')])[1]")
	public WebElement view_preview_Features;

	@FindBy(xpath = "//button[contains(text(),'Settings')]")
	public WebElement settings;

	@FindBy(xpath = "//button[@id='createBroadcastButton']")
	public WebElement create_HealthScreen;

	@FindBy(xpath = "//div[contains(text(),'Default Supporting Department')]")
	public WebElement default_supporting_department;

	@FindBy(xpath = "//button[contains(text(),'Update')]")
	public WebElement update;

	@FindBy(xpath = "//button[contains(text(),'Copy Text')]")
	public WebElement copytext;

	@FindBy(xpath = "//input[@id='broadcast_name']")
	public WebElement create_title;

	@FindBy(xpath = "//div[contains(text(),'Please read the terms of service and verify that you agree.')]")
	public WebElement termsandCond_errormsg;

	@FindBy(xpath = "//app-radio-button[@id='broadcast_terms']")
	public WebElement agree_termsandCond;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement Send_Screening;

	@FindBy(xpath = "//input[@id='broadcast_sendNow']")
	public WebElement sendNow;

	@FindBy(xpath = "//input[@id='inputBroadcastTagData']")
	public WebElement input_To_searchTags;

	@FindBy(xpath = "//div[@class='contact-name d-flex']")
	public WebElement SearchTag1;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[2]")
	public WebElement SearchTag2;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[3]")
	public WebElement SearchTag3;

	@FindBy(xpath = "//app-svg-icon[@class='icon contact-tag-cross-icon icon-cursor']")
	public WebElement tag_crossmark;

	@FindBy(xpath = "//div[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement create_toaster;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement DateandTime_button;

	@FindBy(xpath = "//input[@id='broadcast_Schedule']")
	public WebElement ScheduleLater;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[3]")
	public WebElement time_upArrow;

	@FindBy(xpath = "(//*[@class='owl-dt-control-button owl-dt-control-arrow-button'])[1]")
	public WebElement time_hr_upArrow;

	@FindBy(xpath = "(//*[@class='owl-dt-control-content owl-dt-control-button-content'])[5]")
	public WebElement select_DateandTime_set;

	@FindBy(xpath = "(//*[@class='owl-dt-control-content owl-dt-control-button-content'])[2]")
	public WebElement DateandTime_set;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement Schedule_BroadCast;

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement upcoming_manage;

	@FindBy(xpath = "(//button[contains(text(),'Manage')])[2]")
	public WebElement upcoming_manage1;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement upcoming_manage_delete;

	@FindBy(xpath = "//button[contains(text(),'Re-Use')]")
	public WebElement upcoming_Reuse;

	@FindBy(xpath = "//button[contains(text(),'Re-Use')]")
	public WebElement past_Reuse;

	@FindBy(xpath = "(//button[contains(text(),'View')])[1]")
	public WebElement past_view;

	@FindBy(xpath = "//button[@id='delete-broadcast-message']")
	public WebElement upcoming_Reuse_cancel;

	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[1]")
	public WebElement create_recurring;

	@FindBy(xpath = "//span[@class='label-tag']")
	public WebElement tag_text;

	@FindBy(xpath = "//span[@class='selected-contact inline-display']")
	public WebElement Msg_ToSend_text;

	@FindBy(xpath = "(//input[@id='appDateTimeId'])[1]")
	public WebElement recurring_appDate;

	@FindBy(xpath = "(//input[@id='appDateTimeId'])[2]")
	public WebElement recurring_time;

	@FindBy(xpath = "//h1[@class='mb-0 content-center']")
	public WebElement recipentCount;

	@FindBy(xpath = "//button[@id='broadcast_viewRecipients']")
	public WebElement viewrecipients;

	@FindBy(xpath = "(//*[@class='col-4 overflow-hidden'])[1]")
	public WebElement recipients_list;

	@FindBy(xpath = "//button[contains(text(),' Back')]")
	public WebElement viewrecipients_back;

	@FindBy(xpath = "//span[contains(text(),'Add individual recipients')]")
	public WebElement Add_Individual_recipients;

	@FindBy(xpath = "//input[@id='inputScheduleContactData']")
	public WebElement Add_Individual_recipients_input;

	@FindBy(xpath = "//div[@class='contact-name d-flex']")
	public WebElement Searchcontact1;

	@FindBy(xpath = "//a[@class='btn btn-link']")
	public WebElement Searchcontact_Individual;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[2]")
	public WebElement Searchcontact2;

	@FindBy(xpath = "(//*[@class='contact-name d-flex'])[3]")
	public WebElement Searchcontact3;

	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[2]")
	public WebElement recurring_EndDate;

	@FindBy(xpath = "(//input[@id='appDateTimeId'])[1]")
	public WebElement recurring_appDateTime_Start;

	@FindBy(xpath = "(//input[@id='appDateTimeId'])[2]")
	public WebElement recurring_appDateTime_End;

	@FindBy(xpath = "(//input[@id='appDateTimeId'])[3]")
	public WebElement recurring_appDateTime_WhatTime;

	@FindBy(xpath = "(//Span[@class='col-6 contact-pipe inline-display'])[1]")
	public WebElement searchcontact_HealthScreen;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted'])[1]")
	public WebElement HS_Msg;

	@FindBy(xpath = "//div[contains(text(),' Can not delete as screening is in progress. ')]")
	public WebElement HS_Delete_Error_Toaster;

	@FindBy(xpath = "(//span[@class='m-title'])[1]")
	public WebElement HS_Past_list_title;

	@FindBy(xpath = "(//span[@class='m-title'])[2]")
	public WebElement HS_Past_list_title1;

	@FindBy(xpath = "(//span[@class='m-title'])[3]")
	public WebElement HS_Past_list_title2;

	@FindBy(xpath = "(//span[@class='m-title'])[4]")
	public WebElement HS_Past_list_title3;

	@FindBy(xpath = "(//span[@class='m-title'])[5]")
	public WebElement HS_Past_list_title4;

	@FindBy(xpath = "(//span[@class='m-title'])[6]")
	public WebElement HS_Past_list_title5;

	@FindBy(xpath = "(//span[@class='m-title'])[7]")
	public WebElement HS_Past_list_title6;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[1]")
	public WebElement HS_Past_list_DateTime;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[2]")
	public WebElement HS_Past_list_DateTime1;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[3]")
	public WebElement HS_Past_list_DateTime2;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[4]")
	public WebElement HS_Past_list_DateTime3;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[5]")
	public WebElement HS_Past_list_DateTime4;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[6]")
	public WebElement HS_Past_list_DateTime5;

	@FindBy(xpath = "(//div[@class='col-3 col-lg-4 col-md-3 col-sm-3 mt-1 px-0 inline-display text-right sch-date border-bt-text'])[7]")
	public WebElement HS_Past_list_DateTime6;

	@FindBy(xpath = "//div[@class='b']")
	public WebElement Activate_HS_Page_text1;

	@FindBy(xpath = "//div[@class='ng-tns-c35-4']")
	public WebElement Activate_HS_Page_text2;

	@FindBy(xpath = "//a[@class='learn-more']")
	public WebElement Activate_HS_Page_text3;

	@FindBy(xpath = "(//app-radio-button[@class='col-2 p-0 text-center disable-checkbox ng-untouched ng-pristine ng-valid'])[1]")
	public WebElement actionBttn;

	@FindBy(xpath = "(//p[@class ='messages ng-star-inserted'])[1]")
	public WebElement msg;

	@FindBy(xpath = "(//div[@class='Mt-10'])[7]")
	public WebElement Settings_roles_Admin_HS;

	@FindBy(xpath = "(//app-svg-icon[@class='icon filter-blue-icon ng-tns-c5-0 ng-star-inserted'])[2]")
	public WebElement contact_filterIcon;

	@FindBy(xpath = "//h4[contains(text(),'Health Screen')]")
	public WebElement HS_Reports;

	@FindBy(xpath = " //h4[normalize-space()='Auto1']") // automation login conversation
	public WebElement autoContact;

	@FindBy(xpath = "(//p[@class ='messages ng-star-inserted'])[1]")
	public WebElement message;

	@FindBy(xpath = "(//p[@class ='messages ng-star-inserted'])[2]")
	public WebElement message1;

	@FindBy(xpath = "(//p[@class ='messages ng-star-inserted'])[3]")
	public WebElement message2;

	@FindBy(xpath = "(//p[@class ='messages ng-star-inserted'])[4]")
	public WebElement message3;

	@FindBy(xpath = "//div[contains(text(),' Contact is opted out, unblock is not allowed. ')]")
	public WebElement Block_UnblockTosaster;

	@FindBy(xpath = "//h4[contains(text(),'Automation')]")
	public WebElement Automation_Contact;
	//h4[@id='3092043048_conversation']

	@FindBy(xpath = "//h4[contains(text(),'HS- Tech')]")
	public WebElement HSTech_Contact;
	@FindBy(xpath = "//h4[contains(text(),'auto technocentra')]")
	public WebElement auto_techno_contact;

	@FindBy(xpath = "//div[contains(text(),' Please select a date and time in the future. ')]")
	public WebElement recurring_appDateTime_inFuture_errorMsg;

	@FindBy(xpath = "(//span[@class='chat-date-time-style'])[1]")
	public WebElement System_Stamp;

	@FindBy(xpath = "(//span[@class='failed-icon ng-star-inserted'])[1]")
	public WebElement retry;

	@FindBy(xpath = "//h4[contains(text(),'Health Screen')]")
	public WebElement HS_Cont;

	@FindBy(xpath = "//span[@class='item-sub-sub inline-display']")
	public WebElement Dept_Selection;

	@FindBy(xpath = "//span[@id='Health Screen_Department']")
	public WebElement HS_Dept_Selection;
	
	@FindBy(xpath = "(//span[contains(text(),'test')])[1]")
	public WebElement test_Dept_Selection;
}
