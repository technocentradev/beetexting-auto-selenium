package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Personalisation extends TestBase {

	public Personalisation() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//a[@id='broadcastTab']")
	public WebElement BM;

	@FindBy(xpath = "//*[@id='email-id']")
	public static WebElement user_id;

	@FindBy(xpath = "//*[@id='password-id']")
	public static WebElement Passsword_ID;

	@FindBy(xpath = "//*[@id='login-submit']")
	public static WebElement loginbttn;

	@FindBy(xpath = "//a[@id='toolsMainTab']")
	public WebElement tools_bttn;

	@FindBy(xpath = "//a[@id='tagsTab']")
	public WebElement tools_tags_bttn;

	@FindBy(xpath = "//button[@id='createBroadcastButton']")
	public WebElement create_broadcast;
	
	@FindBy(xpath = "//*[@class='cross-icons']")
	public WebElement tools_crossBtn;
	
	@FindBy(xpath = "//a[@id='templatesTab']")
	public WebElement Templates;

	@FindBy(xpath = "(//button[@class='btn btn-link btn-chat-link'])[4]")
	public WebElement Templates_In_Conversation;

	@FindBy(xpath = "//div[@class='template-tabs-container']//button[contains(text(),'Personal')]")
	public WebElement personal_Templates_In_Conversation;

	@FindBy(xpath = "(//button[contains(text(),'USE')])[5]")
	public WebElement Use_personal_Templates_In_Conversation;

	@FindBy(xpath = "//button[@id='createTemplateButton']")
	public WebElement Create_Template;

	@FindBy(xpath = "//span[@class='display-items-flex']")
	public WebElement BM_Persoanlisation_Message;

	@FindBy(xpath = "//span[@class='display-item-flex']")
	public WebElement Templates_Persoanlisation_Message;

	@FindBy(xpath = "//span[contains(text(),'[firstname]')]")
	public WebElement firstname;

	@FindBy(xpath = "//span[contains(text(),'[companyname]')]")
	public WebElement companyname;

	@FindBy(xpath = "//p[contains(text(),' ContentBeetexting ')]")
	public WebElement messagecomparision;

	@FindBy(xpath = "//span[@class='dropdown']")
	public WebElement Question_Mark;

	@FindBy(xpath = "//p[contains(text(),'Personalize your texts with dynamic text. Beetexting will insert the first name or company name of the recipient wherever the placeholder is entered.')]")
	public WebElement Question_Mark_Content1;

	@FindBy(xpath = "//p[contains(text(),'If there is no first name or company name for the recipient, the dynamic text is emitted from that recipient.')]")
	public WebElement Question_Mark_Content2;

	@FindBy(xpath = "//h4[normalize-space()='Content']")
	public WebElement contentContact;

	@FindBy(xpath = "//h4[@id='5550026788_conversation']")
	public WebElement emptyDetails_Contact;

	@FindBy(xpath = "//h4[@id='5550113474_conversation']")
	public WebElement SpecialChar_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Onlyname']")
	public WebElement onlyname_Contact;

	@FindBy(xpath = "//h4[@id='+15550345626_conversation']")
	public WebElement companyname_Contact;

	@FindBy(xpath = "//svg-icon[@class='contact-pencil-icon']")
	public WebElement editcontact;

	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement Auto1_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement Automation_Contact;
	
	@FindBy(xpath = "//input[@id='firstName']")
	public WebElement inputfirstname;

	@FindBy(xpath = "//input[@id='cName']")
	public WebElement inputcompanyname;

	@FindBy(xpath = "//button[@id='contactSaveButton']")
	public WebElement Contact_edit_Save;

	@FindBy(xpath = "//h4[@id='QA10, QA123,  (555) 023-3727,  (555) 422-9386,  (555) 495-5525,  (555) 451-5894,  (555) 267-2610,  (555) 594-2233,  (555) 120-8272_group']")
	public WebElement group_Conversation;
}
