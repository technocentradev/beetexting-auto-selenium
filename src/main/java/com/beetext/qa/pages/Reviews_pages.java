package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.beetext.qa.base.TestBase;

public class Reviews_pages extends TestBase {

	// Review page xpath starts

	@FindBy(xpath = "//span[contains(text(),' Tools')]")
	public WebElement Tools_link_button;

	// h4[normalize-space()='QA10']

	@FindBy(xpath = "//h4[normalize-space()='QA10']")
	public WebElement qa10;

	@FindBy(xpath = "//a[contains(@id,'reviewsTab')]")
	public WebElement reviews_Tab;

	@FindBy(xpath = "//i[contains(text(),'Create your first review.')]")
	public WebElement Reviewes_NoRecords_TextMessage;

	@FindBy(xpath = "//button[contains(text(),' Add Review Site')]")
	public WebElement Review_AddReviewSite_button;

	@FindBy(xpath = "//li[contains(text(),'Create Review')]")
	public WebElement Create_Review;

	@FindBy(xpath = "//select[contains(@id,'definedTitle')]")
	public WebElement Create_Review_Select_Site_DD;

	@FindBy(xpath = "//select[contains(@id,'definedTitle')]//option")
	public WebElement Create_Review_Site_Options;

	@FindBy(xpath = "//textarea[contains(@id,'link')]")
	public WebElement Create_Review_Link_textbox;

	@FindBy(xpath = "//a[contains(text(),'Learn how to set up your review sites')]")
	public WebElement Create_Review_Help_link;

	@FindBy(xpath = "//button[contains(text(),'Create')]")
	public WebElement Create_Review_Create_button;

	@FindBy(xpath = "//input[@id='customTitle']")
	public WebElement Create_Review_Add_Site;

	@FindBy(xpath = "//option[contains(text(),'Google')]")
	public WebElement Reviews_Site_Selec_Google;

	@FindBy(xpath = "//option[contains(text(),'TripAdvisor')]")
	public WebElement Reviews_Site_Selec_TripAdvisor;

	@FindBy(xpath = "//option[contains(text(),'Facebook')]")
	public WebElement Reviews_Site_Selec_Facebook;

	@FindBy(xpath = "//option[contains(text(),'Yelp')]")
	public WebElement Reviews_Site_Selec_Yelp;

	@FindBy(xpath = "//option[contains(text(),'Others')]")
	public WebElement Reviews_Site_Selec_Others;

	@FindBy(xpath = "//div[@class='col-5 col-lg-5 col-md-6 col-sm-6 pl-0 inline-display border-bt-text title-padd']")
	public WebElement Created_Review_name_in_list;

	@FindBy(xpath = "//label[@class='mat-slide-toggle-label']//div[@class='mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin']")
	public WebElement Review_list_record_default_turn_on_toggle_button;

	@FindBy(xpath = "//li[@class='breadcrumb-item']/a[contains(text(),'Reviews')]")
	public WebElement Manage_page_Reviews_button;

	@FindBy(xpath = "//button[contains(text(),' Manage ')]")
	public WebElement Review_Manage_button;

	@FindBy(xpath = "//div[contains(@class,'row pb-2 valign-center')]//div[contains(@class,'col-5 col-lg-5 col-md-6 col-sm-6 valign-center')]")
	public WebElement Review_Page_select_All_lable;

	@FindBy(xpath = "//li[contains(text(),'Manage Review')]")
	public WebElement Manage_Review_Page_lable;

	@FindBy(xpath = "//option[contains(text(),'sample1')]")
	public WebElement Review_record_sample1_related_SiteDD_data;

	@FindBy(xpath = "//option[contains(text(),'sample11')]")
	public WebElement Review_record_sample11_related_SiteDD_data;

	@FindBy(xpath = "//textarea[@id='link']")
	public WebElement Rivew_record_sample1_Link_details;

	@FindBy(xpath = "//textarea[@id='link']")
	public WebElement Rivew_record_sample11_Link_details;

	@FindBy(xpath = "//button[@id='sample1_button']")
	public WebElement Review_record_sample1_manage_button_under_list;

	@FindBy(xpath = "//button[@id='sample11_button']")
	public WebElement Review_record_sample11_manage_button_under_list;

	@FindBy(xpath = "//button[@id='sample8_button']")
	public WebElement edit_review_page_sample8_manage_button;

	@FindBy(xpath = "//button[@id='var1_button']")
	public WebElement edit_review_page_var1_manage_button;

	@FindBy(xpath = "//button[@id='sample9_button']")
	public WebElement edit_review_page_sample9_manage_button;

	@FindBy(xpath = "//div[@id='titleDuplicateError']")
	public WebElement Edit_Review_page_AddSite_textField_duplicate_error_message;

	@FindBy(xpath = "//div[contains(text(),' Title exists already. ')]")
	public WebElement Create_Review_page_AddSite_textField_duplicate_error_message;

	@FindBy(xpath = "//button[@id='reviewSave']")
	public WebElement Edit_Review_page_Save_Button;

	@FindBy(xpath = "//div[contains(text(),' Please input link for review. ')]")
	public WebElement Edit_Review_page_LinkTextField_mandatory_text_message;

	@FindBy(xpath = "//textarea[@id='link']")
	public WebElement Edit_Review_page_Link_textbox;

	@FindBy(xpath = "//label[contains(text(),'Select Site')]")
	public WebElement Review_SelectSite_lable;

	@FindBy(xpath = "//button[@id='sample2_button']")
	public WebElement sample2_review_manage_button;

	@FindBy(xpath = "//button[@id='delete-review-message']")
	public WebElement Manage_review_page_Delete_Review_button;

	@FindBy(xpath = "//button[contains(text(),'Confirm Delete Review ')]")
	public WebElement Manage_review_page_Confirm_Delete_Review_button;

	@FindBy(xpath = "//button[@id='sample3_button']")
	public WebElement sample3_review_manage_button;

	@FindBy(xpath = "//button[@id='sample4_button']")
	public WebElement sample4_review_manage_button;

	@FindBy(xpath = "//button[@id='sample1_button']")
	public WebElement sample1_review_manage_button;

	@FindBy(xpath = "//button[@id='sample5_button']")
	public WebElement sample5_review_manage_button;

	@FindBy(xpath = "//button[contains(@id,'sample6_button')]")
	public WebElement sample6_review_manage_button;

	@FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-layout[1]/main[1]/section[1]/div[2]/div[1]/app-tools[1]/div[1]/app-reviews[1]/div[1]/div[1]/form[1]/div[1]/div[2]/div[2]/input[1]")
	public WebElement Add_Site_Review_button;

	@FindBy(xpath = "//select[@id='definedTitle']")
	public WebElement Review_SelectSite_DD;

	@FindBy(xpath = "//select[@id='definedTitle']")
	public WebElement Edit_Review_SelectSite_DD;

	@FindBy(xpath = "//div[@aria-label='Review create1 has been successfully created.']")
	public WebElement Toaster_Message_For_create_Record;

	@FindBy(xpath = "//div[@aria-label='Review create4 has been successfully updated.']")
	public WebElement Toaster_Message_For_Updated_Record;

	@FindBy(xpath = "//button[@id='create1_button']")
	public WebElement Create1_Manage_button;

	@FindBy(xpath = "//button[@id='create3_button']")
	public WebElement Create3_Manage_button;

	@FindBy(xpath = "//button[@id='create4_button']")
	public WebElement Create4_Manage_button;

	@FindBy(xpath = "//button[@id='save1_button']")
	public WebElement edit_review_page_save1_manage_button;

	@FindBy(xpath = "//button[@id='Red1_button']")
	public WebElement edit_review_page_Red1_manage_button;

	@FindBy(xpath = "//label[contains(@class,'mat-slide-toggle-label')]//div[contains(@class,'mat-slide-toggle-thumb-container')]//div[contains(@class,'mat-slide-toggle-thumb')]")
	public WebElement Select_All_toggle_button;

	@FindBy(xpath = "//div[@class='mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin']")
	public WebElement toggle_On_Off_button_for_All_records_selected;

	@FindBy(xpath = "//button[@id='site1_button']")
	public WebElement site1_manage_button;

	@FindBy(xpath = "//select[@id='definedTitle']")
	public WebElement site1_record_siteDD_data_in_edit_page;

	@FindBy(xpath = "//a[normalize-space()='Learn how to set up your review sites']")
	public WebElement Learnhowtosetupyourreviewsites_link;

	@FindBy(xpath = "//div[@class='textwidget custom-html-widget']//div[@class='logo-wrap flexin']//a//*[local-name()='svg']")
	public WebElement Beetexting_label_in_review_link_help_page;

	@FindBy(xpath = "//button[@id='Delete1_button']")
	public WebElement delete1_Manage_button;

	@FindBy(xpath = "//div[@aria-label='Delete1 has been deleted.']")
	public WebElement Toaster_Message_For_Delete_Record;

	@FindBy(xpath = "//button[@class='btn btn-link btn-chat-link']//*[@class='icon dollar-icon']//*[@id='Layer_1']")
	public WebElement Review_icon_under_message_chat_box;

	@FindBy(xpath = "//button[@id='icon1_button']")
	public WebElement icon1_Manage_button;

	@FindBy(xpath = "//span[@id='charcount']")
	public WebElement create_Review_page_Link_Char_Count;

	@FindBy(xpath = "//button[@id='v1_button']")
	public WebElement v1_Manage_button;

	@FindBy(xpath = "//button[normalize-space()='Manage']")
	public WebElement Yelp_manage_button;

	@FindBy(xpath = "//button[normalize-space()='Manage']")
	public WebElement Facebook_manage_button;

	@FindBy(xpath = "//button[normalize-space()='Manage']")
	public WebElement google_manage_button;

	@FindBy(xpath = "//button[@id='edit1_button']")
	public WebElement edit1_Manage_button;

	@FindBy(xpath = "//button[@id='edit2_button']")
	public WebElement edit2_Manage_button;

	@FindBy(xpath = "//span[normalize-space()='edit1']")
	public WebElement edit1_record_under_list;

	@FindBy(xpath = "//label[@class='mat-slide-toggle-label']//div[@class='mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin']//input[@class='mat-slide-toggle-input cdk-visually-hidden']")
	public WebElement edit2_record_default_turn_on_check_button;

	@FindBy(xpath = "//*[@class='cross-icons']//*[local-name()='svg']")
	public WebElement tools_page_close_icon;

	@FindBy(xpath = "//label[@class='mat-slide-toggle-label']//div[@class='mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin']//input[@class='mat-slide-toggle-input cdk-visually-hidden']")
	public WebElement toggle_On_Off_button_for_All_records_unselected;

	@FindBy(xpath = "//div[@id='chat-input-area']//textarea[@id='chatInputTextarea']")
	public WebElement message_text_box_field_in_messagees_page;

	@FindBy(xpath = "//button[@id='chatInput-sendButton']")
	public WebElement messages_send_button;

	@FindBy(xpath = "//p[contains(@class,'messages ng-star-inserted')]//a[contains(@class,'linkified')]")
	public WebElement link_url_in_message_chatbox;

	@FindBy(xpath = "//h2[normalize-space()='Leave your review on']")
	public WebElement Leave_your_review_on_text;

	@FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-reviews[1]/div[1]/div[2]/button[1]")
	public WebElement Url_related_page_link1_button;

	@FindBy(xpath = "//h2[@class='h2 b mb-4 text-center mg-tp-15px']")
	public WebElement org_name_in_url_related_page;

	@FindBy(xpath = "//button[normalize-space()='link1']")
	public WebElement link1_button_in_link_page;

	@FindBy(xpath = "//p[contains(@class,'messages ng-star-inserted')]//a[contains(@class,'linkified')]")
	public WebElement link_url_in_message_chatbox_274;

	@FindBy(xpath = "//p[@class='messages ng-star-inserted']//a[@class='linkified']")
	public WebElement link_url_in_message_chatbox_276;

	@FindBy(xpath = "//button[@id='edit2_button']")
	public WebElement edit2_manage;

	@FindBy(xpath = "//div[@class='input-types d-flex justify-content-between mb-1 py-2']")
	public WebElement click_outside_of_webelements;

	@FindBy(xpath = "//a[normalize-space()='Learn how to set up your review sites']")
	public WebElement Link_LearnHowToSetUpYourReviewSites;

	@FindBy(xpath = "//div[@class='flexin mobile-view']//a[normalize-space()='Features']")
	public WebElement Features_button_in_link_page;

	@FindBy(xpath = "//button[@id='link_button']")
	public WebElement link_manage_button;

	@FindBy(xpath = "//span[normalize-space()='records']")
	public WebElement records_title_under_list;

	@FindBy(xpath = "//label[@class='mat-slide-toggle-label']//div[@class='mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin']//input[@class='mat-slide-toggle-input cdk-visually-hidden']")
	public WebElement records_toaggle_button;

	@FindBy(xpath = "//button[@id='records_button']")
	public WebElement records_manage_button;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement conversation_Automation;

	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement conversation_Auto1;

	@FindBy(xpath = "//button[@id='link1_button']")
	public WebElement link1_manage_button;

	@FindBy(xpath = "//button[@id='link2_button']")
	public WebElement link2_manage_button;

	@FindBy(xpath = "//button[@id='link3_button']")
	public WebElement link3_manage_button;

	@FindBy(xpath = "//button[normalize-space()='Manage']")
	public WebElement toggle_manage_button;

	@FindBy(xpath = "(//div[@class='mat-slide-toggle-thumb'])[2]")
	public WebElement link5_toggle_button;

	@FindBy(xpath = "//div[@class='row pb-2 valign-center']//label[@class='mat-slide-toggle-label']//div[@class='mat-slide-toggle-thumb']")
	public WebElement Select_All_toggle_off_button;

	@FindBy(xpath = "//button[@id='link5_button']")
	public WebElement link5_manage_button;

	@FindBy(xpath = "//button[normalize-space()='Manage']")
	public WebElement like_manage_button;

	@FindBy(xpath = "//button[@id='delete-review-message']")
	public WebElement delete_button;

	@FindBy(xpath = " //button[normalize-space()='Confirm Delete Review']")
	public WebElement confirm_delete_button;

	@FindBy(xpath = "//div[contains(@aria-label,'sample8 has been deleted.')]")
	public WebElement toastermessage_delete_review;

	// End review xpaths

	// Initializing the page objects:
	public Reviews_pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

}
