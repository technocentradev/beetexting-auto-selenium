package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class SettingsCreateDepartment extends TestBase {

	// Initializing the page objects:
	public SettingsCreateDepartment() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//span[@class='s-title']")
	public WebElement Settings_link;

	@FindBy(xpath = "//a[@id='NumbersTab']")
	public WebElement Numbers_link;

	@FindBy(xpath = "//button[@id='departmentAddNumberId']")
	public WebElement Create_Number;
	// create new number
	@FindBy(xpath = "//button[@id='deptCreateNewNumber']")
	public WebElement createNewNumber_Btn;

	@FindBy(xpath = "//input[@id='deptNewnumberName']")
	public WebElement deptName;

	@FindBy(xpath = " //div[contains(text(),'Please give your number a name.')]")
	public WebElement deptName_required;

	@FindBy(xpath = " //div[contains(text(),'Your Number name must contain at least 3 character')]")
	public WebElement deptName_required_min3_char;

	@FindBy(xpath = " //input[@id='deptNewAreacode']")
	public WebElement areacode;

	@FindBy(xpath = "//div[contains(text(),'Area Code is required.')]")
	public WebElement areacode_required;
	// div[contains(text(),'Please enter a valid three-digit US area code.')]
	@FindBy(xpath = "//div[contains(text(),'Please enter a valid three-digit area code.')]")
	public WebElement areacode_mustbe_letters;

	@FindBy(xpath = "//button[normalize-space()='Create']")
	public WebElement createbtn;

	@FindBy(xpath = "//div[contains(text(),'Number Created Successfully.')]")
	public WebElement deptCreated;

	// text enabled existing number
	@FindBy(xpath = " //button[@id='deptEnableExistingNumber']")
	public WebElement txtEnabled_Existing_Number_Btn;

	@FindBy(xpath = "//input[@id='deptNewnumberName']")
	public WebElement DeptName;

	@FindBy(xpath = "//body/app-root[1]/div[1]/app-layout[1]/main[1]/section[1]/div[2]/div[1]/app-settings[1]/div[1]/app-department-number-name[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]")
	public WebElement outsideClick;

	@FindBy(xpath = " //div[contains(text(),'Please give your number a name.')]")
	public WebElement department_required_valid;

	@FindBy(xpath = "//div[contains(text(),'Your Number name must contain at least 3 character')]")
	public WebElement department_required_3char_valid;

	@FindBy(xpath = " //button[normalize-space()='Next']")
	public WebElement NextButton;

	@FindBy(xpath = "//div[contains(text(),'Phone Number Account Info')]")
	public WebElement phonenumberinfo_page;

	@FindBy(xpath = " //input[@id='existingPhoneNumber']")
	public WebElement phonenumber;

	@FindBy(xpath = "//input[@id='customerName']")
	public WebElement cname;

	@FindBy(xpath = " //div[contains(text(),'Please give your number a name.')]")
	public WebElement cname_required_validation;
	@FindBy(xpath = "//div[contains(text(),'Name must be at least 3 characters.')]")
	public WebElement cname_3char_required_validation;

	@FindBy(xpath = "//input[@id='existingNumberStreet']")
	public WebElement cstreet;

	@FindBy(xpath = "//input[@id='existingNumberCity']")
	public WebElement ccity;

	@FindBy(xpath = " //div[contains(text(),'Special characters are not allowed.')]")
	public WebElement ccity_specialchar_notallowed;

	@FindBy(xpath = "//input[@id='existingNumberZip']")
	public WebElement zipcode;

	@FindBy(xpath = " //div[contains(text(),'Zip is required.')]")
	public WebElement zipcode_required;

	@FindBy(xpath = "//div[contains(text(),'Only numbers are allowed.')]")
	public WebElement zipcode_allow_only_numbers;

	@FindBy(xpath = "//select[@id='existingNumberState']")
	public WebElement state;
	@FindBy(xpath = "//select[@id='existingNumberState']//option[50]")
	public WebElement select_state;

	@FindBy(xpath = "//app-check-box[@id='existingNumberCheckbox']//span[@class='checkmark']")
	public WebElement radioButton;

	@FindBy(xpath = "//div[contains(text(),'Please read the terms of service and accept.')]")
	public WebElement checkradiobtn_validation;

	@FindBy(xpath = "//button[@id='deptNewNumberCreateButton']")
	public WebElement nextButton;

	@FindBy(xpath = "//button[contains(text(),'Call me!')]")
	public WebElement callme_Button;

	@FindBy(xpath = "//span[contains(text(),'Verify Your Number')]")
	public WebElement verify_page;

	@FindBy(xpath = "//button[normalize-space()='VERIFY']")
	public WebElement verify_button;

	@FindBy(xpath = " //button[contains(text(),'VERIFY')]")
	public WebElement verify_button_click;

	@FindBy(xpath = "//div[@aria-label='The 6 digit verification you entered is invalid, please try again.']")
	public WebElement invalid_otp;

	// div[@aria-label='Phone number is already available with other org, please
	// check with Sales Team']
	@FindBy(xpath = "//div[@aria-label='Phone number is already in use elsewhere and cannot be text enabled, contact our support team with any questions or concerns.']")
	public WebElement Numberalready_Exist;

	@FindBy(xpath = " //button[normalize-space()='RESEND CODE']")
	public WebElement resendcode;
	// div[@aria-label='Voice call triggered or initiated for your number.']

	// div[@aria-label='Your phone number text enablement request was successfully
	// created. Keep an eye on your phone, if we need any more information we will
	// reach out to you.']
	@FindBy(xpath = "//div[@aria-label='Your phone number text enablement request was successfully created. Keep an eye on your phone, if we need any more information we will reach out to you.']")
	public WebElement callme_again;

	@FindBy(xpath = "//button[normalize-space()='BACK']")
	public WebElement BackBtn;

	@FindBy(xpath = "//button[@id='deptNewNumberBack']")
	public WebElement BackButton;

	@FindBy(xpath = "//span[contains(text(),'Would you like to create a new phone number or text enable an existing landline?')]")
	public WebElement createNumber;

	// manage Department xpath

	@FindBy(xpath = "//button[@id='sampleButtonId']")
	public WebElement ManageBtn;
	@FindBy(xpath = "//button[@id='sampleeditButtonId']")
	public WebElement ManageEditBtn;
	@FindBy(xpath = "//button[@id='sample2ButtonId']")
	public WebElement sample2ManageBtn;
	@FindBy(xpath = " //button[@id='Auto1ButtonId']")
	public WebElement Auto1ManageBtn;

	@FindBy(xpath = "//button[@id='TollfreeButtonId']")
	public WebElement ManageBtn1;

	@FindBy(xpath = "//button[@id='Tollfree123456ButtonId']")
	public WebElement ManageBtn2;

	@FindBy(xpath = "//button[@id='DummyButtonId']")
	public WebElement DummyBtn1;

	@FindBy(xpath = "//input[@id='departmentNameManage']")
	public WebElement dept_name_edit;

	@FindBy(xpath = "//div[@id='numberNameRequired']")
	public WebElement dept_name_required_validation;

	@FindBy(xpath = "//div[@id='numberNameMinLength']")
	public WebElement dept_name_edit_3char_validation;

	@FindBy(xpath = "//input[@id='forwardCalls']")
	public WebElement fcall;

	@FindBy(xpath = "//button[@id='saveDepartmentButton']")
	public WebElement savebutton;

	@FindBy(xpath = "//button[normalize-space()='confirm']")
	public WebElement confirm_save;

	@FindBy(xpath = "//div[@aria-label='Updated sample successfully.']")
	public WebElement toaster_message;
	@FindBy(xpath = "//div[@aria-label='Updated sampleedit successfully.']")
	public WebElement toaster_message_edit;

	@FindBy(xpath = "//button[contains(text(),'Delete Number')]")
	public WebElement DeleteDept;

	@FindBy(xpath = "//app-check-box[@id='acceptTerm1']//span[@class='checkmark']")
	public WebElement radiobtn1;

	@FindBy(xpath = "//button[normalize-space()='Next']")
	public WebElement NEXTBTN1;

	@FindBy(xpath = "//app-check-box[@class='font-size-12']//span[@class='checkmark']")
	public WebElement radiobtn2;

	@FindBy(xpath = " //app-check-box[@id='checkDept']//span[@class='checkmark']")
	public WebElement radiobtn3;
	@FindBy(xpath = "//app-check-box[@id='I accept I will lose the phone number associated to this account if the phone number was provided by Beetexting.']//span[@class='checkmark']")
	public WebElement radiobtn4;
	@FindBy(xpath = "//app-check-box[@id='I understand that all incoming messages to this department will receive out of service response.']//span[@class='checkmark']")
	public WebElement radiobtn5;
	@FindBy(xpath = "//app-check-box[@id='I understand that all data and conversation history associated with this department will be deleted and will not be recoverable.']//span[@class='checkmark']")
	public WebElement radiobtn6;

	@FindBy(xpath = "//button[normalize-space()='Delete Number']")
	public WebElement confirmDelete;

	@FindBy(xpath = "//div[@aria-label='sample has been deleted.']")
	public WebElement department_deleted;
	@FindBy(xpath = "//button[contains(text(),'Verify')]")
	public WebElement Verify_button;

	// button[@id='fggfghButtonId']
	@FindBy(xpath = " //button[@id='sampleButtonId']")
	public WebElement Verify_button1;
	@FindBy(xpath = " //button[@id='sample2ButtonId']")
	public WebElement Verify_button2;
	@FindBy(xpath = " //button[normalize-space()='Call me!']")
	public WebElement callmeButton;

	@FindBy(xpath = "//button[contains(text(),'Back')]")
	public WebElement Back_Button;

	@FindBy(xpath = "//button[@id='v-delete-number']")
	public WebElement DeleteButton;

	@FindBy(xpath = "//button[@id='v-delete-number']")
	public WebElement CofirmDeleteButton;

	@FindBy(xpath = "//div[@aria-label='sample has been deleted.']")
	public WebElement delete_toaster_message;

	// button[@id='btn-60c1ada640b6e5662d31a1a1']
	// button[@id='btn-61b1958540b6e5730ad3f2e1']
	// agent Remove
	@FindBy(xpath = "	//button[@id='btn-61b1958540b6e5730ad3f2e1']")
	public WebElement department_remvoe_agent;

	@FindBy(xpath = "//button[@id='btn-61b1958540b6e5730ad3f2e1']")
	public WebElement department_confirm_remvoe_agent;

	@FindBy(xpath = "	//div[contains(@aria-label,'sample.All of their claimed threads are now unclaimed within the sample.')]")
	public WebElement department_confirm_remvoe_agent_toastermessage;
	// agent remove end

	// add agent

	@FindBy(xpath = "//button[contains(text(),'Add users')]")
	public WebElement addagentBtn;

	@FindBy(xpath = "//app-check-box[@id='dept-add-agent']//span[@class='checkmark']")
	public WebElement select_radiobutton;

	@FindBy(xpath = " //button[normalize-space()='Add']")
	public WebElement addButton;

	@FindBy(xpath = "//div[@aria-label='Updated Tollfree successfully.']")
	public WebElement tollfreeDept_updated;

	@FindBy(xpath = "//div[contains(@aria-label,'Successfully added user(s).')]")
	public WebElement addagentToaster;
	@FindBy(xpath = "//button[contains(text(),' Invite Users')]")
	public WebElement inviteagent_button;

	@FindBy(xpath = " //input[@id='manageAgentName']")
	public WebElement agent_name;
	@FindBy(xpath = "//input[@id='manageAgentEmail']")
	public WebElement agent_email;

	@FindBy(xpath = "//select[@id='manageAgentRole']")
	public WebElement agent_role;

	@FindBy(xpath = " //app-radio-button[@id='inviteAgentEmail']//span[contains(@class,'checkmark')]")
	public WebElement email_radiobutton;

	@FindBy(xpath = "//button[normalize-space()='SEND']")
	public WebElement sendbutton;

	@FindBy(xpath = "//div[@aria-label='Your invitation to dummy123 has been sent.']")
	public WebElement invite_agent_toaster_message;

	// department manage agent button xpath

	@FindBy(xpath = " //button[@id='automationsManage']")
	public WebElement manageagentButton;

	@FindBy(xpath = "//button[@id='deleteAgentButton']")
	public WebElement managedeleteagentButton;

	@FindBy(xpath = " //button[contains(text(),'Cancel')]")
	public WebElement cancelbutton;

	@FindBy(xpath = "//div[contains(text(),'Manage User')]")
	public WebElement ManageAgent_Headding;
	// end

	// tollfree department manage
	@FindBy(xpath = " //button[@id='TollfreeButtonId']")
	public WebElement Manage_tollfree_Dept;
	@FindBy(xpath = " //div[@id='numberNameMinLength']")
	public WebElement Manage_tollfree_Dept_min_3char_validation;

	@FindBy(xpath = " //div[@id='numberNameRequired']")
	public WebElement Manage_tollfree_Dept_required;

	// end

	// end invite agent xpath
	@FindBy(xpath = " //a[@id='UsersTab']")
	public WebElement agent_link;

	@FindBy(xpath = "//button[@id='dummy123manageButton']")
	public WebElement ManageButton;

	@FindBy(xpath = "//button[@id='deleteAgentButton']")
	public WebElement delete_Agent;

	@FindBy(xpath = "//button[@id='confirmDeleteAgentId']")
	public WebElement confirm_delete_Agent;

	@FindBy(xpath = "//div[@aria-label='dummy123 has been deleted from the organization.']")
	public WebElement delete_Agent_toastermessage;
	// div[contains(@aria-label,'dummy has been removed from Auto1.All of their
	// claimed threads are now unclaimed within the Auto1.')]

	@FindBy(xpath = "//div[contains(@aria-label,'Auto1.All of their claimed threads are now unclaimed within the Auto1.')]")
	public WebElement delete_Agent_toastermessage1;
	// div[contains(text(),'Your contact must have a valid 10-digit phone numb')]
	@FindBy(xpath = "//div[contains(text(),'Your contact must have a valid 10-digit phone numb')]")
	public WebElement phonenumber_10digits_validation;

	@FindBy(xpath = "//button[@id='sampleButtonId']")
	public WebElement Verify_buttons;

	@FindBy(xpath = "//button[@id='v-delete-number']")
	public WebElement click_delete_button;

	@FindBy(xpath = "//button[@id='v-delete-number']")
	public WebElement click_confirm_delete_button;

	@FindBy(xpath = "//button[@id='sample1ButtonId']")
	public WebElement sample1_dept;

	@FindBy(xpath = " //header/a[@id='settingsClose']/*[1]/*[1]")
	public WebElement close_settingspopup;

	@FindBy(xpath = "	//button[normalize-space()='Delete Number']")
	public WebElement delete_button;

}
