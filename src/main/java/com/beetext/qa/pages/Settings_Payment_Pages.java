package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class Settings_Payment_Pages extends TestBase {

	public Settings_Payment_Pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//*[@id='email-id']")
	public static WebElement user_id;

	@FindBy(xpath = "//*[contains(text(),' Email not valid. ' )]")
	public static WebElement login_invalidmail;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public static WebElement logindetailsinvalid;

	@FindBy(xpath = "//*[@id='password-id']")
	public static WebElement Passsword_ID;

	@FindBy(xpath = "//*[contains(text(),'Password must be at least 8 characters. ' )]")
	public static WebElement loginpassword_must8char;

	@FindBy(xpath = "//*[@id='login-submit']")
	public static WebElement loginbttn;

	@FindBy(xpath = "//a[@id='settingsMainTab']")
	public WebElement settings;

	@FindBy(xpath = "//a[@id='settingsClose']")
	public WebElement settings_Close;

	@FindBy(xpath = "//a[@id='planAndBillingTab']")
	public WebElement settings_planBilling;

	@FindBy(xpath = "(//main[@class='App__content']//button[@type='button'])[1]")
	public WebElement ach_Account_Cross;

	@FindBy(xpath = "//button[@id='addPaymentButton']")
	public WebElement planBilling_addPayment;

	@FindBy(xpath = "//button[@id='addPaymentCreditCard']")
	public WebElement planBilling_addPayment_creditCard;

	@FindBy(xpath = "//button[@id='linkButton']")
	public WebElement planBilling_addPayment_AchLink;

	@FindBy(xpath = "//input[@id='cc-number']")
	public WebElement planBilling_creditcard_cardnumber;

	@FindBy(xpath = "//input[@id='cc-exp-date']")
	public WebElement planBilling_creditcard_expDate;

	@FindBy(xpath = "//input[@id='cc-cvc']")
	public WebElement planBilling_creditcard_cvvcode;

	@FindBy(xpath = "//div[@id='invalidCardNumber']")
	public WebElement planBilling_creditcard_invalidCard;

	@FindBy(xpath = "//div[@id='invalidCardExpDate']")
	public WebElement planBilling_creditcard_InvalidExpDate;

	@FindBy(xpath = "//div[@id='invalidSecurityCode']")
	public WebElement planBilling_creditcard_invalidCvv;

	@FindBy(xpath = "//div[contains(text(),' Security code must be at least 3 digits. ' )]")
	public WebElement planBilling_creditcard_3charerror;

	@FindBy(xpath = "//button[@id='saveButtonAddPayment']")
	public WebElement planBilling_creditcard_save;

	@FindBy(xpath = "//app-check-box[@id='primaryAddPayment']")
	public WebElement planBilling_creditcard_PrimaryPayments;

	@FindBy(xpath = "//button[contains(text(),'Back' )]")
	public WebElement planBilling_creditcard_back;

	@FindBy(xpath = "//span[text()='Continue']")
	public static WebElement achContinue;

	@FindBy(xpath = "//input[@id='search-input']")
	public WebElement ach_Bank_Search;

	@FindBy(xpath = "//*[text()='Bank of America']")
	public static WebElement ach_bankselection;

	@FindBy(xpath = "(//*[text()='Bank of America'])[2]")
	public static WebElement ach_bankselection1;

	@FindBy(xpath = "//button[@aria-label='Close']")
	public static WebElement ach_Close;

	@FindBy(xpath = "//input[@id='aut-input-0']")
	public static WebElement ach_username;

	@FindBy(xpath = "//input[@id='aut-input-1']")
	public static WebElement ach_password;

	@FindBy(xpath = "//span[contains(text(),'Submit')]")
	public static WebElement ach_submitbttn;

	@FindBy(xpath = "//li[@id='aut-selection-0']")
	public static WebElement ach_acct1;

	@FindBy(xpath = "(//div[contains(text(),'Phone' )])[1]")
	public static WebElement ach_acct1_Description;

	@FindBy(xpath = "//li[@id='aut-selection-1']")
	public static WebElement ach_acct2;

	@FindBy(xpath = "(//div[contains(text(),'Phone' )])[2]")
	public static WebElement ach_acct2_Description;

	@FindBy(xpath = "(//span[contains(text(),'Invalid input text length')])[1]")
	public static WebElement ach_usernamerequired;

	@FindBy(xpath = "(//span[contains(text(),'Invalid input text length')])[2]")
	public static WebElement ach_passworedrequried;

	@FindBy(xpath = "//*[@id='a11y-title']")
	public static WebElement ach_details_incorrect;

	@FindBy(xpath = "//*[contains(text(),'For security reasons, your account may be locked after several unsuccessful attempts' )]")
	public static WebElement ach_invaliddetails_errormsg;

	@FindBy(xpath = "//*[contains(text(),'Retry answer' )]")
	public static WebElement ach_retryanswer;

	@FindBy(xpath = "//button[contains(text(),'Try Another Bank' )]")
	public static WebElement ach_tryanotherbank;

	@FindBy(xpath = "//input[@id='device-input']")
	public static WebElement ach_secretCode;

	@FindBy(xpath = "//li[@id='aut-selection-0']")
	public static WebElement ach_plaidChecking;

	@FindBy(xpath = "//div[contains(text(),'Plaid Checking')]")
	public static WebElement ach_plaidChecking_Description;

	@FindBy(xpath = "//li[@id='aut-selection-1']")
	public static WebElement ach_plaidsaving;

	@FindBy(xpath = "//div[contains(text(),'Plaid Saving')]")
	public static WebElement ach_plaidsaving_Description;

	@FindBy(xpath = "//span[text()='Submit']")
	public static WebElement ach_secretcode_submit;

	@FindBy(xpath = "//div[@id='payment_col3_0']")
	public static WebElement Auto_payment;

	@FindBy(xpath = "//div[@id='payment_col3_0']")
	public static WebElement default_payment;

	@FindBy(xpath = "//*[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement creditCard_toaster;

	@FindBy(xpath = "//button[@id='manageButton_1']")
	public WebElement manage_billing;

	@FindBy(xpath = "//app-check-box[@id='defaultForCurrency']")
	public WebElement primary_payment;

	@FindBy(xpath = "//button[@id='saveButton']")
	public WebElement save;

	@FindBy(xpath = "//a[@id='RolesTab']")
	public WebElement setting_roles;

	@FindBy(xpath = "//div[@class='subhead']")
	public WebElement setting_roles_roleCapbilities;

	@FindBy(xpath = "(//div[@class='col-6'])[1]")
	public WebElement setting_roles_admin;

	@FindBy(xpath = "(//div[@class='col-6'])[2]")
	public WebElement setting_roles_agent;

	@FindBy(xpath = "//a[@id='mySellerAccountTab']")
	public WebElement setting_mysellerAcct;

	@FindBy(xpath = "//div[@class='Mt-10 h4 form-group text-align-center font-size-16 head-color']")
	public WebElement setting_mysellerAcct_text;

	@FindBy(xpath = "//button[@class='btn btn-action-sm-new']")
	public WebElement setting_Add_mysellerAcct;

	@FindBy(xpath = "//div[@class='mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin']")
	public WebElement sellerAcct_tips;

	@FindBy(xpath = "//div[contains(text(),' My Seller Account ' )]")
	public WebElement sellerAcct_Page;

	@FindBy(xpath = "(//div[@class='subhead-2 ng-tns-c35-4 ng-star-inserted'])[1]")
	public WebElement sellerAcct_Page_text1;

	@FindBy(xpath = "//div[contains(text(),' Enable Tips ' )]")
	public WebElement sellerAcct_Page_enableTips;

	@FindBy(xpath = "(//div[@class='subhead-2 ng-tns-c35-4 ng-star-inserted'])[2]")
	public WebElement sellerAcct_Page_text2;

	@FindBy(xpath = "//a[@class='beetexting-link']")
	public WebElement stripe_Acct_Updatelink;

	@FindBy(xpath = "//*[@class='toast-success toast ng-trigger ng-trigger-flyInOut']")
	public WebElement tips_on_off_toaster;

	@FindBy(xpath = "(//app-svg-icon[@class='icon dollar-icon'])[1]")
	public WebElement msg_payment_Icon;

	@FindBy(xpath = "//h4[@id='SellerAccountUnavailable']")
	public WebElement msg_payment_errorMsg;

	@FindBy(xpath = "//label[@class='amount']")
	public WebElement msg_payment_amounttext;

	@FindBy(xpath = "//div[@id='billDescriptionLengthError']")
	public WebElement msg_payment_billDescription_lengtherror;

	@FindBy(xpath = "//input[@id='invoiceNumber']")
	public WebElement msg_payment_invoice;

	@FindBy(xpath = "//input[@id='billDescription']")
	public WebElement msg_payment_BillDescription;

	@FindBy(xpath = "//input[@id='amount']")
	public WebElement msg_payment_Amount;

	@FindBy(xpath = "//div[@id='billDescriptionRequiredError']")
	public WebElement msg_payment_billDescription_error;

	@FindBy(xpath = "//div[@id='AmountMaximumError']")
	public WebElement msg_payment_Amount_Maxerror;

	@FindBy(xpath = "//div[contains(text(),' Amount cannot exceed $100000. ')]")
	public WebElement msg_payment_Amount_Maxerror_in_TipPage;

	@FindBy(xpath = "(//div[@class='p-2 chat-template ng-star-inserted']//a[@class='pull-right close-icon'])[2]")
	public WebElement msg_payment_crossmark;

	@FindBy(xpath = "//button[@id='sendButton']")
	public WebElement msg_payment_send;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted']//a[@class='linkified'])[1]")
	public WebElement msg_payment_link;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted'])[1]")
	public WebElement msg_payment_textWithLink;

	@FindBy(xpath = "//div[contains(text(),'Payment cancelled or failed')]")
	public WebElement msg_payment_Cancel_msg;

	@FindBy(xpath = "//*[@class='InlineSVG Icon Header-backArrow mr2 Icon--sm']")
	public WebElement msg_payment_back;

	@FindBy(xpath = "//span[@class='ProductSummary-name Text Text-color--gray500 Text-fontSize--16 Text-fontWeight--500']")
	public WebElement msg_payment_billDes_InPaymentPage;

	@FindBy(xpath = "//span[@id='ProductSummary-totalAmount']")
	public WebElement msg_payment_Amount_InPaymentPage;

	@FindBy(xpath = "//div[@class='text-align-center payment-status payment-message-center']")
	public WebElement payment_already_Paid;

	@FindBy(xpath = "//input[@id='email']")
	public WebElement msg_payment_email;

	@FindBy(xpath = "//input[@id='cardNumber']")
	public WebElement msg_payment_cardNumber;

	@FindBy(xpath = "//input[@id='cardExpiry']")
	public WebElement msg_payment_cardExpiry;

	@FindBy(xpath = "//input[@id='cardCvc']")
	public WebElement msg_payment_cardcvc;

	@FindBy(xpath = "//input[@id='billingName']")
	public WebElement msg_payment_nameOnCard;

	@FindBy(xpath = "//select[@id='billingCountry']")
	public WebElement msg_payment_billingCountry;

	@FindBy(xpath = "//option[@value='US']")
	public WebElement msg_payment_billingCountry_optionUS;

	@FindBy(xpath = "//input[@id='billingPostalCode']")
	public WebElement msg_payment_billingPostalCode;

	@FindBy(xpath = "//div[@class='text-align-center payment-status payment-message-center']")
	public WebElement payment_Successfull_Msg;

	@FindBy(xpath = "//div[@class='SubmitButton-IconContainer']")
	public WebElement msg_payment_submit;

	@FindBy(xpath = "//button[@class='btn pay-btn pay']")
	public WebElement tips_submit;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted'])[2]")
	public WebElement Pinmessage_amount;

	@FindBy(xpath = "(//p[@class='messages ng-star-inserted'])[1]")
	public WebElement Payment_Msg;

	@FindBy(xpath = "//h2[contains(text(),'Add a Tip?')]")
	public WebElement msg_payment_addTip;

	@FindBy(xpath = "//svg-icon[@class='cross-icons']")
	public WebElement setting_crossIcon;

	@FindBy(xpath = "(//span[@class='text-btn'])[1]")
	public WebElement tips_10;

	@FindBy(xpath = "(//span[@class='text-btn'])[2]")
	public WebElement tips_15;

	@FindBy(xpath = "(//span[@class='text-btn'])[3]")
	public WebElement tips_20;

	@FindBy(xpath = "(//span[@class='text-btn'])[4]")
	public WebElement NoTip;

	@FindBy(xpath = "//span[@class='text-btn margin-rt-10']")
	public WebElement CustomTip;

	@FindBy(xpath = "//input[@numerictype='currency']")
	public WebElement CustomTip_text;

	@FindBy(xpath = "//div[@class='form-group text-center']")
	public WebElement Total_withTip;

	@FindBy(xpath = "//button[@class='btn pay-btn pay']")
	public WebElement pay;

	@FindBy(xpath = "//button[contains(text(),'Pay')]")
	public WebElement pay_in_Tips_Enabled_Payment_page;
	
	@FindBy(xpath = "//h4[normalize-space()='Auto1']")
	public WebElement Auto1_Contact;

	@FindBy(xpath = "//h4[normalize-space()='Automation']")
	public WebElement Automation_Contact;
	
	@FindBy(xpath = "//*[@id='composenewBtn']")
	public WebElement composebutton;

	@FindBy(id = "chatInput-sendButton")
	public WebElement chatInputsendButton2;

	@FindBy(xpath = "//*[@id='chatInput-sendNowButton']")
	public WebElement chatInputsendnowButton;
	
	@FindBy(xpath = "(//button[@class='btn btn-link btn-chat-link'])[1]")
	public WebElement pin_attachment;

}
