package com.beetext.qa.pages;

import com.beetext.qa.base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Tools_SM_pages extends TestBase {

	@FindBy(xpath = "//span[contains(text(),' Tools')]")
	public WebElement Tools_button;

	@FindBy(xpath = "//a[@id='scheduledMessageTab']")
	public WebElement Scheduled_Message_Tab_button;

	@FindBy(xpath = "//button[@id='createScheduleButton']")
	public WebElement Create_Scheduled_Message_button;

	@FindBy(xpath = "//a[@id='schedule_breadcrumId']")
	public WebElement Scheduled_message_link_button;

	@FindBy(xpath = "//button[normalize-space()='DISCARD']")
	public WebElement popup_Discard_button;

	@FindBy(xpath = "//select[@class='form-control ng-untouched ng-pristine ng-valid']")
	public WebElement select_department_DD;

	@FindBy(xpath = "//input[@id='inputScheduleContactData']")
	public WebElement Message_sent_to_textbox;

	@FindBy(xpath = "//div[contains(text(),'Maximum 9 contacts are allowed.')]")
	public WebElement Max_9_Contact_allowed_text_message;

	@FindBy(xpath = "//*[@class='remove-reciepient']//*[local-name()='svg']")
	public WebElement close_mark_button_for_contact;

	//textarea[@id='schedule_message']
	@FindBy(xpath="//textarea[@id='schedule_message']")
	public WebElement Create_SM_Message_textbox;

	@FindBy(xpath = "//span[contains(text(),'1000/1000')]")
	public WebElement Create_SM_Message_char_count;

	@FindBy(xpath = "//app-check-box[@id='isRecurring']//span[@class='checkmark']")
	public WebElement recurring_check_button;

	@FindBy(id = "schedule_SaveButton")
	public WebElement create_SM_ScheduledMessage_button;

	@FindBy(xpath = "//span[normalize-space()='SS']")
	public WebElement Created_SM_under_upcomming;

	@FindBy(xpath = "//button[normalize-space()='Manage']")
	public WebElement First_contact_manage_button;

	@FindBy(id = "delete-scheduled-message")
	public WebElement Edit_page_Delete_Scheduled_Message_button;

	@FindBy(id = "delete-scheduled-message")
	public WebElement Confirm_delete_SM_button;

	@FindBy(xpath = "//button[normalize-space()='Drafts']")
	public WebElement SM_Drafts_Button;

	@FindBy(xpath = "//div[@class='col-3 mt-1 inline-display px-0 m-title border-bt-text']")
	public WebElement SM_first_saved_draft;

	@FindBy(xpath = "//button[@class='btn btn-primary-danger text-danger']")
	public WebElement Draft_delete_button;

	@FindBy(xpath = "//button[@id='warningInfoDiscard']")
	public WebElement Draft_delete_discard_popup_button;

	@FindBy(id = "schedule_DraftButton")
	public WebElement Save_Draft_button_in_CSM;

	@FindBy(xpath="//a[@id = 'schedule_breadcrumId']")
	public WebElement schedule_breadcrumId_link_CSM;
	
	@FindBy(xpath="//span[@class='close-0']")
	public WebElement Close_icon_of_Attachment;
	
	@FindBy(xpath="//span[@class='subhead-2-name attachment-name-maxwidth']")
	public WebElement Attachment_1;
 
	@FindBy(xpath = "//button[.='DISCARD']")
	public WebElement Breadcrum_popup_discard_button;

	@FindBy(xpath = "//div[@class='tag-font Mt-10 modal-container color-value b ng-star-inserted']")
	public WebElement text_in_Breadcrum_popup_page;

	@FindBy(id = "warningInfoSave")
	public WebElement SaveDraft_button_in_Breadcrum_popup;

	@FindBy(xpath = "//i[@class='empty-msg']")
	public WebElement YouCanSaveDraftScheduledMessages_text_under_Drafts_list;

	@FindBy(xpath = "//div[@class='main-container modal-popup light-theme']")
	public WebElement outside_click;

	@FindBy(id = "templatesTab")
	public WebElement Templates_tab;

	@FindBy(id = "createTemplateButton")
	public WebElement CreateTemplate_button;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement Calendar_icon;

	@FindBy(xpath = "//span[normalize-space()='26']")
	public WebElement Previous_date_in_calendar_popup;

	@FindBy(xpath = "//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]")
	public WebElement Set_button_in_calendar_popup;

	@FindBy(xpath = "//div[contains(text(),' Please select a date and time in the future. ')]")
	public WebElement Error_text_message_for_Date_Time_in_CSM;

	@FindBy(xpath = "//label[contains(text(),'Repeat')]")
	public WebElement Repeat_label_under_Recurring;

	@FindBy(xpath = "//select[contains(@class,'form-control ng-pristine ng-valid')]")
	public WebElement Repeat_DD;

	@FindBy(xpath = "//button[@aria-label='Add a hour']//span[@class='owl-dt-control-button-content']//*[local-name()='svg']")
	public WebElement Uparrow_button_Time_hour;
	
	@FindBy(xpath = "//button[@aria-label='Add a minute']//span[@class='owl-dt-control-button-content']//*[name()='svg']")
	public WebElement Uparrow_button_Time_mint;
	

	@FindBy(xpath = "//button[@aria-label='Minus a hour']")
	public WebElement Downarrow_button_Time_hour;

	@FindBy(xpath = "//div[@aria-label=' Your text for Auto1 is scheduled to send on']")
	public WebElement Toaster_message_Reccuring_SM_record;
	
	@FindBy(xpath = "//app-date-time[@class='date-margin']//input[@id='appDateTimeId']")
	public WebElement Calender_icon_under_reccuring;

	@FindBy(xpath = "//div[contains(text(),'Please select a date and time in the future.')]")
	public WebElement Validation_message_for_previous_date_time;

	@FindBy(xpath = "//select[@class='form-control ng-pristine ng-valid ng-touched']")
	public WebElement Create_SM_Department_DDfield;

	@FindBy(xpath = "//button[@aria-label='Previous month']//span[@class='owl-dt-control-content owl-dt-control-button-content']//*[local-name()='svg']")
	public WebElement Left_arrow_button_in_End_Date_calender;

	@FindBy(xpath = "//span[normalize-space()='15']")
	public WebElement Previous_date_for_End_Date_calender;

	@FindBy(xpath = "//div[normalize-space()='Please select a date in the future.']")
	public WebElement validation_error_message_for_Date_in_EndDate_calender;

	@FindBy(xpath = "//span[@class='owl-dt-control-button-arrow']")
	public WebElement Down_arrow_icon_for_Month_and_Year_under_Recurring;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[5]")
	public WebElement Previous_Year_under_recurring;

	@FindBy(xpath = "(//span[@class='owl-dt-calendar-cell-content'])[8]")
	public WebElement Month_for_previous_year_under_recurring;

	@FindBy(xpath = "//span[normalize-space()='To:']")
	public WebElement Recurring_Record_under_upcoming_list;

	@FindBy(xpath = "//i[@class='empty-msg']")
	public WebElement text_message_for_no_upcoming_records;

	@FindBy(xpath = "//i[@class='empty-msg']")
	public WebElement text_message_for_no_Drafts_records;

	@FindBy(xpath = "//div[@class='col-4 text-right btn-container']/span")
	public WebElement recurring_icon_under_list;
	//button[@class='manage btn btn-action-sm pull-right']
	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	public WebElement recurring_manage_button_under_list;

	@FindBy(xpath = "//button[@id='delete-scheduled-message']")
	public WebElement Delete_SM_button_in_Edit_SM_page;

	@FindBy(xpath = "//button[@id='delete-scheduled-message']")
	public WebElement Confirm_Delete_SM_button_in_Edit_SM_page;

	@FindBy(xpath = "//li[@class='breadcrumb-item active']")
	public WebElement Manage_SM_Breadcrum_button;

	@FindBy(xpath = "//div[@class='col-3 mt-1 inline-display px-0 m-title border-bt-text']")
	public WebElement Record_under_drafts_list;

	@FindBy(xpath = "//button[@class='btn btn-primary-danger text-danger']")
	public WebElement Delete_button_under_Drafts_list;

	@FindBy(xpath = "//button[@id='warningInfoDiscard']")
	public WebElement Discard_button_in_conformation_Drafts_popup;

	@FindBy(xpath = "//div[@class='tag-font Mt-10 modal-container color-value b ng-star-inserted']")
	public WebElement Text_in_Drafts_conformation_popup;

	@FindBy(xpath = "//button[normalize-space()='CANCEL']")
	public WebElement Cancel_button_in_conformation_Drafts_popup;

	@FindBy(xpath = "//button[@class='btn btn-action-sm manage']")
	public WebElement Manage_button_under_drafts_List;
	//button[@class='tab-btn pull-left tab-button1']
	@FindBy(xpath = "//button[contains(text(),'Upcoming')]")
	public WebElement SM_Upcoming_tab;
	//button[@class='tab-btn pull-left tab-button1 tab-active']
	@FindBy(xpath="//button[contains(text(),'Upcoming')]")
	public WebElement SM_selected_Upcoming_button;

	@FindBy(xpath = "//span[normalize-space()='To:']")
	public WebElement Upcoming_Record;

	@FindBy(id = "schedule_SaveButton")
	public WebElement SM_button_in_Manage_page;

	@FindBy(xpath = "//button[@id='schedule_DraftButton']")
	public WebElement SaveDraft_button_in_Manage_page;

	@FindBy(xpath = "//button[@aria-label='Previous month']//span[@class='owl-dt-control-content owl-dt-control-button-content']//*[local-name()='svg']")
	public WebElement left_arrow_for_month_in_calander;

	@FindBy(xpath = "//a[@id='schedule_breadcrumId']")
	public WebElement Breadcrum_link_button;

	@FindBy(xpath = "//div[@class='tag-font Mt-10 modal-container color-value b ng-star-inserted']")
	public WebElement text_in_breadcrum_conformation_popup;

	@FindBy(xpath = "//button[normalize-space()='SAVE']")
	public WebElement Save_button_in_breadcrum_conformation_popup;

	@FindBy(xpath = "//button[normalize-space()='SAVE']")
	public WebElement Save_button_in_other_Link_conformation_popup;

	@FindBy(xpath = "//div[@class='tag-font Mt-10 modal-container color-value b ng-star-inserted']")
	public WebElement text_in_other_Link_conformation_popup;

	@FindBy(xpath = "//button[normalize-space()='DISCARD']")
	public WebElement Discard_button_in_other_Link_conformation_popup;

	@FindBy(xpath = "//button[normalize-space()='Manage']")
	public WebElement Manage_button_under_Upcomming_list;
	//div[@class='template-name'][normalize-space()='3232332']
	//button[normalize-space()='Manage']
	@FindBy(xpath = "//span[normalize-space()='(555) 555-5552']button[normalize-space()='Manage']")
	public WebElement Manage_button;

	@FindBy(xpath = "//*[@class='remove-reciepient']//*[local-name()='svg']")
	public WebElement Cancel_Icon_For_Contact_in_Manage_page;

	@FindBy(xpath = "//div[@class='toast-message ng-star-inserted']")
	public WebElement Toster_message_for_edited_upcomming_record;

	@FindBy(xpath = "//textarea[@id='schedule_message']")
	public WebElement text_in_message_field;

	@FindBy(xpath = "//span[@class='selected-contact inline-display']")
	public WebElement New_recipent_in_manage_page;
	
	
	@FindBy(xpath="//*[@class='icon paper-clip']//span//*[name()='svg']")
	public WebElement Paper_Clip_icon_for_Attachments;
	
	@FindBy(xpath="//button[@class='btn btn-link btn-chat-link mt-1']/..//app-svg-icon[@class='icon dollar-icon']")
	public WebElement Review_link_icon;
	
	@FindBy(xpath="//div[@class='input-types d-flex justify-content-between mb-1 py-2']")
	public WebElement All_icons_of_Attachments;
	
	@FindBy(xpath="//div[@class='input-types d-flex justify-content-between mb-1 py-2']//button[@class='btn btn-link btn-chat-link mt-1']")
	public WebElement SM_reviews_icon;
	
	@FindBy(xpath="//app-svg-icon[@class='icon emoji-icon']")
	public WebElement SM_emoji_icon; 
	
	@FindBy(xpath="//emoji-category[2]//div[1]//ngx-emoji[1]//span[1]//span[1]")
	public WebElement SM_emoji_image1;
	
	@FindBy(xpath="//emoji-category[2]//div[1]//ngx-emoji[1]//span[1]//span[2]")
	public WebElement SM_emoji_image2;
	
	@FindBy(xpath="//emoji-category[2]//div[1]//ngx-emoji[1]//span[1]//span[3]")
	public WebElement SM_emoji_image3;
	
	@FindBy(xpath="//*[@class='icon tools-giphy']//span//*[name()='svg']")
	public WebElement SM_GIF_icon;
	
	@FindBy(xpath="//div[@class='input-group-append poiner-event-none']//button[@class='btn btn-link btn-chat-link'][1]")
	public WebElement SM_pin_icon;
	
	@FindBy(xpath="//*[@placeholder='Search']")
	public WebElement SM_GIF_popup;
	
	@FindBy(xpath="//*[@placeholder='Search']")
	public WebElement SM_Gif_Search;
	
	@FindBy(xpath="//h6[@class='empty-list'][1]")
	public WebElement SM_Gif_Search_Error_Message;
	
	@FindBy(xpath="//*[@class='close-icon pointer-section ']//span//*[name()='svg']")
	public WebElement SM_Gif_Close_icon;
	
	@FindBy(xpath="//a[normalize-space()='View Terms']")
	public WebElement SM_Gif_ViewTerms_link;
	
	@FindBy(xpath="//div[@class='model-bg']")
	public WebElement SM_Gif_OutsideClick;
	
	@FindBy(xpath="//img[@id='giphy-container-body'][1]")
	public WebElement SM_GIF_image1;
	
	@FindBy(xpath="//img[@id='giphy-container-body'][2]")
	public WebElement SM_GIF_image2;
	
	@FindBy(xpath="//img[@id='giphy-container-body'][3]")
	public WebElement SM_GIF_image3;
	
	@FindBy(xpath="//span[@class='subhead-2-name attachment-name-maxwidth']")
	public WebElement SM_GIF_Attachment1;
	
	@FindBy(xpath="//textarea[@id='schedule_message']")
	public WebElement edited_text_message;
	
	@FindBy(xpath="//span[@class='subhead-2-name attachment-name-maxwidth']")
	public WebElement attachment;
	
	@FindBy(xpath="//button[@class='owl-dt-control owl-dt-control-button owl-dt-control-arrow-button'][1]")
	public WebElement previous_Month;
	
	@FindBy(xpath="//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]")
	public WebElement Logout_3_dots;
	
	@FindBy(xpath="//a[@id='logoutId']")
	public WebElement SignOut_button;
	
	@FindBy(xpath="//*[@class='cross-icons']//*[name()='svg']")
	public WebElement close_icon_for_Tools_page;
	
//Initializing the page objects:
	public Tools_SM_pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

}
