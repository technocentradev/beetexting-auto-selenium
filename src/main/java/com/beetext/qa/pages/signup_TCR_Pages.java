package com.beetext.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.beetext.qa.base.TestBase;

public class signup_TCR_Pages extends TestBase {

	public signup_TCR_Pages() {
		PageFactory.initElements(driver, this);// 'this' means current class objects. Can also use LoginPage.class
	}

	@FindBy(xpath = "//button[contains(text(),'Close')]")
	public WebElement tcr_Close;

	@FindBy(xpath = "//button[contains(text(),'Register with TCR')]")
	public WebElement tcr_Register;

	@FindBy(xpath = "//*[@id='email-id']")
	public WebElement user_id;

	@FindBy(xpath = "//*[@id='password-id']")
	public WebElement Passsword_ID;

	@FindBy(xpath = "//*[@id='login-submit']")
	public WebElement loginbttn;

	@FindBy(xpath = "//*[@id='composenewBtn']")
	public WebElement composebutton;

	@FindBy(xpath = "//*[@id='contactSearchInput']")
	public WebElement contactSearchInputTxt;

	@FindBy(xpath = "//*[@id='contactSelectNewNumber']")
	public WebElement contactSearchOutputTxt12;

	@FindBy(xpath = "//button[@class='btn btn-link emojiSendbtn btn-action-sm']")
	public WebElement chatInputsendButton;

	@FindBy(xpath = "//textarea[@id='chatInputTextarea']")
	public WebElement chatInputTextareaTxt;

	@FindBy(xpath = "//a[@id='toolsMainTab']")
	public WebElement tools_bttn;

	@FindBy(xpath = "//*[@class='cross-icons']")
	public WebElement tools_crossBtn;

	@FindBy(xpath = "//a[@id='broadcastTab']")
	public WebElement broadCast_msg;

	@FindBy(xpath = "//button[@id='createBroadcastButton']")
	public WebElement create_broadcast;

	@FindBy(xpath = "//input[@id='inputBroadcastTagData']")
	public WebElement tools_BM_input_To_searchTags;

	@FindBy(xpath = "//input[@id='broadcast_name']")
	public WebElement tools_BM_title;

	@FindBy(xpath = "//textarea[@id='broadcast_message']")
	public WebElement tools_BM_message;

	@FindBy(xpath = "//app-radio-button[@id='broadcast_terms']//span[@class='checkmark ']")
	public WebElement tools_BM_create_agree_termsandCond;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement tools_BM_create_Send_BroadCast;

	@FindBy(xpath = "//div[@class='contact-name d-flex']")
	public WebElement tools_BM_create_SearchTag1;

	@FindBy(xpath = "//a[@id='scheduledMessageTab']")
	public WebElement tools_SM;

	@FindBy(xpath = "//button[@id='createScheduleButton']")
	public WebElement tools_create_SM;

	@FindBy(xpath = "//input[@id='inputScheduleContactData']")
	public WebElement input_msg_Sent_TO;
	
	@FindBy(xpath = "//span[@class='col-6 contact-pipe inline-display']")
	public WebElement select_Contact;

	@FindBy(xpath = "//textarea[@id='schedule_message']")
	public WebElement SM_MSg_Textarea;

	@FindBy(xpath = "//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]")
	public WebElement Set_button_in_calendar_popup;

	@FindBy(xpath = "//button[@aria-label='Add a hour']//span[@class='owl-dt-control-button-content']//*[local-name()='svg']")
	public WebElement Uparrow_button_Time_hour;

	@FindBy(xpath = "//button[@aria-label='Add a minute']//span[@class='owl-dt-control-button-content']//*[name()='svg']")
	public WebElement Uparrow_button_Time_mint;

	@FindBy(xpath = "//button[@aria-label='Minus a hour']")
	public WebElement Downarrow_button_Time_hour;

	@FindBy(xpath = "//input[@id='appDateTimeId']")
	public WebElement Calendar_icon;

	@FindBy(xpath = "//button[@id='schedule_SaveButton']")
	public WebElement Tools_Sm_Create_SM;

	@FindBy(xpath = "//a[@id='healthScreeningTab']")
	public WebElement healthscreen;

	@FindBy(xpath = "//button[@id='createBroadcastButton']")
	public WebElement create_HealthScreen;

	@FindBy(xpath = "//input[@id='broadcast_name']")
	public WebElement create_title;

	@FindBy(xpath = "//div[contains(text(),'Please read the terms of service and verify that you agree.')]")
	public WebElement termsandCond_errormsg;

	@FindBy(xpath = "//app-radio-button[@id='broadcast_terms']")
	public WebElement agree_termsandCond;

	@FindBy(xpath = "//button[@id='broadcast_SendButton']")
	public WebElement Send_Screening;

	@FindBy(xpath = "//input[@id='broadcast_sendNow']")
	public WebElement sendNow;

	@FindBy(xpath = "//input[@id='inputBroadcastTagData']")
	public WebElement input_To_searchTags;

	@FindBy(xpath = "//div[@class='contact-name d-flex']")
	public WebElement SearchTag1;

	@FindBy(xpath = "//p[@class='message text-center']")
	public WebElement tcr_Message1;

	@FindBy(xpath = "//p[@class='message text-center']")
	public WebElement tcr_Message2;

}
