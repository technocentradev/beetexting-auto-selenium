package com.beetext.qa.sendemail;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.mail.EmailException;
import org.testng.annotations.Test;

public class Sendmail {

	public static String currentDir = System.getProperty("user.dir");

	@Test
	public static void Sendemail() throws EmailException {

		final String username = "hitextbee@gmail.com";
		final String password = "BeeText@123";
		String fromEmail = "hitextbee@gmail.com";
		String toEmail = "nageswar@technocentra.com";
		String toEmail1 = "sudarsan.edagotti@technocentra.com";
		String toEmail2 = "siva@technocentra.com";
		String toEmail3 = "rajendra.yerramas@technocentra.com";
		String toEmail4 = "venkata.jayavarapu@technocentra.com";
		String toEmail5 = "chidvilas.avvaru@technocentra.com";

		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		MimeMessage msg = new MimeMessage(session);
		try {

			msg.setFrom(new InternetAddress(fromEmail));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail1));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail2));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail3));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail4));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail5));

			msg.setSubject(" Automation Test Report ");

			Multipart emailContent = new MimeMultipart();

			MimeBodyPart textBodyPart = new MimeBodyPart();
			textBodyPart.setText(" Hi Everyone \n Automation Test Report is Below");

			MimeBodyPart reportAttachment = new MimeBodyPart();
			reportAttachment.attachFile(currentDir + "\\target\\surefire-reports\\Extent.html");

			emailContent.addBodyPart(textBodyPart);
			emailContent.addBodyPart(reportAttachment);

			msg.setContent(emailContent);

			/* msg.setText("Email body text"); */
			Transport.send(msg);
			System.out.println("Sent Message");

		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String args[]) throws EmailException {

		Sendmail.Sendemail();
	}
}
