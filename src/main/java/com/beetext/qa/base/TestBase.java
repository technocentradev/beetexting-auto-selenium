package com.beetext.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.beetext.qa.pages.Repository;
import com.beetext.qa.util.TestUtil;
import com.beetext.qa.util.WebEventListener;


public class TestBase {

	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");
	public static Logger logger;

	// Create a constructor and initialize the variables
	public TestBase() {

		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					currentDir + "\\src\\main\\java\\com\\beetext\\qa\\config\\config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void initialization() {

		logger = Logger.getLogger("Beetexting Test Excution Logs report");
		PropertyConfigurator.configure("Log4j.properties");

		String browserName = prop.getProperty("browser");

		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					currentDir + "\\BrowserDrivers\\chromedriver_win32\\chromedriver.exe");

			driver = new ChromeDriver();

			logger.info("Chrome Browser is launched ");
		} else if (browserName.equals("FF")) {
			System.setProperty("webdriver.gecko.driver", "C:/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			logger.info("Firefox  Browser is launched ");
		}
		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with
		// EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;

		driver.manage().window().maximize();
		logger.info("Browser window is maximized ");
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS); // create util class
																									// for timeunit
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);

		driver.get(prop.getProperty("url"));
		logger.info("Beetexting Login page is launched ");

	}

	public void Login() {

		Repository loginPage;
//		TestUtil testUtil;
//		testUtil = new TestUtil();
		loginPage = new Repository();
		TestBase testBase = new TestBase();
		logger.info("Login page entering Email and Password");
		testBase = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		logger.info("Email id : " + (prop.getProperty("username")));
		logger.info("Password : " + (prop.getProperty("password")));
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		logger.info("page load time ");
		TestUtil.takeScreenshotAtEndOfTest("LogInTestCase");// ScreenShot capture
	}

	public void Kill_ChromeDriver() throws IOException {

		Runtime.getRuntime().exec("taskkill /F /IM chromedrivers.exe /T");

	}

	public WebDriver init(String browser) throws Exception {

		logger = Logger.getLogger("Beetexting Test Excution Logs report");
		PropertyConfigurator.configure("Log4j.properties");

		DesiredCapabilities dc = new DesiredCapabilities();

		if (browser.equals("chrome")) {
			dc.setBrowserName("chrome");
			System.setProperty("webdriver.chrome.driver",
					currentDir + "\\BrowserDrivers\\chromedriver_win32\\chromedriver.exe");

			driver = new ChromeDriver();

			logger.info("Chrome Browser is launched ");

		} else if (browser.equals("firefox")) {
			dc.setBrowserName("firefox");
		} else if (browser.equals("safari")) {
			dc.setBrowserName("safari");
		} else if (browser.equals("opera")) {
			dc.setBrowserName("opera");
		} else if (browser.equals("edge")) {
			dc.setBrowserName("MicrosoftEdge");
		}

		return driver;

	}

	public void VCard_Signout() throws Exception {
		// li[6]/div[1]/a[1]/app-svg-icon[1]/span[1]
		// a[@id='logoutId']

		WebElement signoutlink = driver.findElement(By.xpath("//li[7]/div[1]/a[1]/app-svg-icon[1]/span[1]"));
		WebElement signout = driver.findElement(By.xpath("//a[@id='logoutId']"));
		signoutlink.click();
		Thread.sleep(3000);
		signout.click();
	}

}
